package com.food.order.speedzy.Activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import com.food.order.speedzy.root.BaseActivity;
import com.google.android.material.appbar.AppBarLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.Adapter.DeliveryAddAdapter;
import com.food.order.speedzy.Adapter.PlaceAutocompleteAdapter;
import com.food.order.speedzy.Adapter.Saved_Addresses_Adapter;
import com.food.order.speedzy.Model.CityListModel;
import com.food.order.speedzy.Model.DeliveryAddressModel;
import com.food.order.speedzy.Model.UserAddressModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */


public class LocationActivity extends BaseActivity implements
        AdapterView.OnItemSelectedListener,PlaceAutocompleteAdapter.PlaceAutoCompleteInterface,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {
        EditText search_location;
    GoogleApiClient mGoogleApiClient;
    PlaceAutocompleteAdapter mAdapter;
    DeliveryAddAdapter mdeliveryAddressAdapter;
    Saved_Addresses_Adapter savedAddressAdapter;
    ArrayList<DeliveryAddressModel> deliveryAddressList = new ArrayList<>();
    ArrayList<UserAddressModel.info> savedAddressList = new ArrayList<>();
    private static final LatLngBounds BOUNDS_BERHAMPUR = new LatLngBounds(
            new LatLng(19.2805219,  84.7545107), new LatLng (19.3359684, 84.86200319999999));
    private RecyclerView recent_search_recyclerview,search_Filter_recyclerview,saved_adds_recyclerview;
    LinearLayoutManager llm,llm1,llm2;
    TextView current_address;
    LOcaldbNew lOcaldbNew;
    MySharedPrefrencesData mySharedPrefrencesData;
    String location="",Veg_Flag;
    String userid="";
    TextView add_address;
    private Toolbar toolbar;
    AppBarLayout appbar;
        ProgressBar progressBar1;
        ImageView clear;
    List<CityListModel.cityList> cityList = new ArrayList<>();
    private View viewToAttachDisplayerTo;
    boolean network_status=true;
    Activity context;
    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }
    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        viewToAttachDisplayerTo = findViewById(R.id.displayerAttachableView);

        context=LocationActivity.this;
        lOcaldbNew = new LOcaldbNew(this);
        mySharedPrefrencesData = new MySharedPrefrencesData();
        userid = mySharedPrefrencesData.getUser_Id(this);
        location=mySharedPrefrencesData.getLocation(this);
        Veg_Flag=mySharedPrefrencesData.getVeg_Flag(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        clear = (ImageView) findViewById(R.id.clear);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (location.equalsIgnoreCase("") || location.equalsIgnoreCase(null)) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (Veg_Flag.equalsIgnoreCase("1")){
            statusbar_bg(R.color.red);
        }else {
            statusbar_bg(R.color.red);
        }

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_location.setText("");
                search_Filter_recyclerview.setVisibility(View.GONE);
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        call_api_citylist();
        search_location=(EditText)findViewById(R.id.search_location);
        search_location.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        progressBar1=(ProgressBar) findViewById(R.id.progressBar1);
        search_Filter_recyclerview = (RecyclerView)findViewById(R.id.search_Filter_recyclerview);
        current_address=(TextView) findViewById(R.id.current_address);
        add_address=(TextView) findViewById(R.id.add_address);
        search_Filter_recyclerview.setHasFixedSize(true);
        llm = new LinearLayoutManager(this);
        search_Filter_recyclerview.setLayoutManager(llm);
        mAdapter = new PlaceAutocompleteAdapter(this, R.layout.view_placesearch,
                mGoogleApiClient, BOUNDS_BERHAMPUR, null);
        search_Filter_recyclerview.setAdapter(mAdapter);

        setRecentSearchAddress(userid);
        callapi_get_address();

        search_location.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) { }
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                    private Timer timer=new Timer();
                    private final long DELAY = 1000; // milliseconds

                    @Override
                    public void afterTextChanged(final Editable s) {
                        timer.cancel();
                        timer = new Timer();
                        timer.schedule(
                                new TimerTask() {
                                    @Override
                                    public void run() {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (!s.toString().equalsIgnoreCase("")) {
                                                    apicall(s);
                                                }else {
                                                    search_Filter_recyclerview.setVisibility(View.GONE);
                                                }
                                            }
                                        });
                                    }
                                },
                                DELAY
                        );
                    }
                }
        );
        current_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               location_select_function("", 0);
            }
        });
        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                location_select_function("",1);
            }
        });
    }

    private void call_api_citylist() {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<CityListModel> call = methods.get_city_list();
        Log.d("url", "url=" + call.request().url().toString());
        call.enqueue(new Callback<CityListModel>() {
            @Override
            public void onResponse(Call<CityListModel> call, Response<CityListModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                CityListModel cityListModel = response.body();
                cityList.clear();
                cityList = cityListModel.getCityList();
            }

            @Override
            public void onFailure(Call<CityListModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        appbar.setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,color ));
        }
    }

    private void location_select_function(final String current_address_text, int i) {
        Intent intent=new Intent(context, AddAddresses_Manual.class);
        intent.putExtra("locality",current_address_text);
        intent.putExtra("change_address",false);
        intent.putExtra("flag",String.valueOf(i));
        startActivityForResult(intent, 2);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2)
        {
            if (data!=null) {
                String message = data.getStringExtra("ADD_ADDRESS");
                String city_selected=data.getStringExtra("city_selected");
                String longitude_selected=data.getStringExtra("longitude_selected");
                String latitude_selected=data.getStringExtra("latitude_selected");
                String addressid_selected=data.getStringExtra("addressid_selected");
                String name=data.getStringExtra("name");
                String area=data.getStringExtra("area");
                home_Activity_Intent(city_selected,longitude_selected,latitude_selected,addressid_selected,message, name,area);
            }
        }
    }
    private void apicall(Editable s) {
        if (mAdapter != null) {
            search_Filter_recyclerview.setVisibility(View.VISIBLE);
            recent_search_recyclerview.setVisibility(View.GONE);
            search_Filter_recyclerview.setAdapter(mAdapter);
        }

        if (!s.toString().equals("") && mGoogleApiClient.isConnected()) {
            mAdapter.getFilter().filter(s.toString());
        } else if (!mGoogleApiClient.isConnected()) {
//                    Toast.makeText(getApplicationContext(), Constants.API_NOT_CONNECTED, Toast.LENGTH_SHORT).show();
            Log.e("", "NOT CONNECTED");
        }
    }
    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onPlaceClick(ArrayList<PlaceAutocompleteAdapter.PlaceAutocomplete> mResultList, int position) {
        if(mResultList!=null){
            try {
                final String placeId = String.valueOf(mResultList.get(position).placeId);
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if(places.getCount()>0){
                            //Do the things here on Click.....
                            search_location.setText(places.get(0).getAddress());
                            lOcaldbNew.insertRecentSearchAdd(userid, places.get(0).getAddress().toString(),String.valueOf(places.get(0).getLatLng().latitude),
                                    String.valueOf(places.get(0).getLatLng().longitude));
                            //setRecentSearchAddress(userid);
                            location_select_function(search_location.getText().toString(), 0);
                            //search_location.setText("");
                           // hideKeyboard();
                        } else{
                            Toast.makeText(getApplicationContext(),"Place details not found.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            catch (Exception e){

            }

        }
    }

    private void setRecentSearchAddress(String userid) {
        deliveryAddressList.clear();

        if (userid == null)
            userid = "";
        if (userid.trim().equals(""))
            userid = "";

        ArrayList<DeliveryAddressModel> listOfAdd = lOcaldbNew.getAllRecentSearchAddress(userid);
        Log.e("DB_DATA", String.valueOf(new Gson().toJson(listOfAdd)));

        if (listOfAdd == null)
            listOfAdd = new ArrayList<>();

        deliveryAddressList.addAll(listOfAdd);
        Collections.reverse(deliveryAddressList);

        recent_search_recyclerview= (RecyclerView)findViewById(R.id.recent_search_recyclerview);
        recent_search_recyclerview.setVisibility(View.VISIBLE);
        recent_search_recyclerview.setHasFixedSize(true);
        llm1 = new LinearLayoutManager(this);
        recent_search_recyclerview.setLayoutManager(llm1);
        mdeliveryAddressAdapter = new DeliveryAddAdapter(this,deliveryAddressList);
        recent_search_recyclerview.setAdapter(mdeliveryAddressAdapter);
        mdeliveryAddressAdapter.notifyDataSetChanged();
        mdeliveryAddressAdapter.setOnItemClickListener(new DeliveryAddAdapter.OnItemClickListener(){
            public void onItemClick(String address) {
                location_select_function(address, 0);
            }
        });
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void callapi_get_address() {
        saved_adds_recyclerview = (RecyclerView)findViewById(R.id.saved_adds_recyclerview);
        progressBar1.setVisibility(View.VISIBLE);
        saved_adds_recyclerview.setVisibility(View.GONE);
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<UserAddressModel> call = methods.get_address(userid);
        Log.d("url","url="+call.request().url().toString());
        call.enqueue(new Callback<UserAddressModel>() {
            @Override
            public void onResponse(Call<UserAddressModel> call, Response<UserAddressModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                UserAddressModel response1 = response.body();
                savedAddressList.clear();
                if (response1.getStatus().equalsIgnoreCase("Success")) {
                    savedAddressList.addAll(response1.getInfo_list());
                    Collections.reverse(savedAddressList);

                    saved_adds_recyclerview.setHasFixedSize(true);
                    llm2 = new LinearLayoutManager(context);
                    saved_adds_recyclerview.setLayoutManager(llm2);
                    savedAddressAdapter = new Saved_Addresses_Adapter(context,savedAddressList);
                    saved_adds_recyclerview.setAdapter(savedAddressAdapter);
                    /*savedAddressAdapter.setOnItemClickListener(new Saved_Addresses_Adapter.OnItemClickListener(){
                        public void onItemClick(String cityid, String selected_location, String longi, String lati, String address_id, String name, String area, String s, String toString) {
                           home_Activity_Intent(cityid,longi,lati,address_id,selected_location+","+area,name,area);
                        }
                    });*/
                    progressBar1.setVisibility(View.GONE);
                    saved_adds_recyclerview.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onFailure(Call<UserAddressModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void home_Activity_Intent(String city_selected, String longitude_selected, String latitude_selected, String addressid_selected, String selected_location, String name,String area) {
        if (mySharedPrefrencesData.getLocation(context)==null || mySharedPrefrencesData.getLocation(this).equalsIgnoreCase("")) {
            mySharedPrefrencesData.setLocation(context, selected_location);
            if (city_selected!=null &&
                    !city_selected.equalsIgnoreCase("")&&
                    !city_selected.equalsIgnoreCase("NA")) {
                mySharedPrefrencesData.setSelectCity(context, city_selected);
            }else {
                mySharedPrefrencesData.setSelectCity(context, "FOODCITYBAM");
            }
            mySharedPrefrencesData.setSelectCity_latitude(context, latitude_selected);
            mySharedPrefrencesData.setSelectCity_longitude(context, longitude_selected);
            mySharedPrefrencesData.setSelectAddress_ID(context, addressid_selected);
            mySharedPrefrencesData.setSelectName(context, name);
            mySharedPrefrencesData.setSelectArea(context, area);
            Intent intent = new Intent(context, HomeActivityNew.class);
            intent.putExtra("flag",1);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }else {
            mySharedPrefrencesData.setLocation(context, selected_location);
            if (city_selected!=null &&
                    !city_selected.equalsIgnoreCase("")&&
                    !city_selected.equalsIgnoreCase("NA")) {
                mySharedPrefrencesData.setSelectCity(context, city_selected);
            }else {
                mySharedPrefrencesData.setSelectCity(context, "FOODCITYBAM");
            }
            mySharedPrefrencesData.setSelectCity_latitude(context, latitude_selected);
            mySharedPrefrencesData.setSelectCity_longitude(context, longitude_selected);
            mySharedPrefrencesData.setSelectAddress_ID(context, addressid_selected);
            mySharedPrefrencesData.setSelectName(context, name);
            mySharedPrefrencesData.setSelectArea(context, area);
            Intent intent=new Intent();
            intent.putExtra("SELECTED_LOCATION",selected_location);
            setResult(1,intent);
            Commons.back_button_transition(context);
        }
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
               Commons.back_button_transition(context);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Commons.back_button_transition(context);
    }
   @Override
    public void onResume() {
        super.onResume();
       setRecentSearchAddress(userid);
       callapi_get_address();
    }
}
