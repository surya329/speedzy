package com.food.order.speedzy.Utils;

import android.content.Context;
import com.food.order.speedzy.R;
import com.google.android.gms.maps.model.BitmapDescriptor;

public class IconUtils {

  public static BitmapDescriptor getDropSVGIcon(Context context) {
    return CommonConvertion.bitmapDescriptorFromVector(context,
        R.drawable.ic_location_drop);
  }

  public static BitmapDescriptor getBikeImageSvgIcon(Context context) {
    return CommonConvertion.bitmapDescriptorFromVector(context,
        R.drawable.ic_bike_speedzy);
  }
}
