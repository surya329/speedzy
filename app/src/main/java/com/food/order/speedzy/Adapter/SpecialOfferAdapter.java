package com.food.order.speedzy.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.food.order.speedzy.Model.Delivery_Charge_Response_Model;
import com.food.order.speedzy.R;

import java.util.ArrayList;



/**
 * Created by Sujata Mohanty.
 */

public class SpecialOfferAdapter extends RecyclerView.Adapter<SpecialOfferAdapter.ViewHolder> {

    Context context;
    ArrayList<Delivery_Charge_Response_Model.Offer> newOffersArrayList;

    public SpecialOfferAdapter(Context context, ArrayList<Delivery_Charge_Response_Model.Offer> newOffersArrayList) {
        this.context = context;
        this.newOffersArrayList = newOffersArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        System.out.println("View Holder");
        // set the view's size, margins, paddings and layout parameters
        SpecialOfferAdapter.ViewHolder vh = new SpecialOfferAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Delivery_Charge_Response_Model.Offer offers=newOffersArrayList.get(position);
        holder.txt_radioButton.setText(offers.getOfferDescription());
        holder.priceradio.setText("₹ "+offers.getOfferprice());
    }

    @Override
    public int getItemCount() {
        return newOffersArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_radioButton, priceradio;


        public ViewHolder(View itemView) {
            super(itemView);


            txt_radioButton = (TextView) itemView.findViewById(R.id.check);
            priceradio = (TextView) itemView.findViewById(R.id.price);
            txt_radioButton.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);

        }
    }
}
