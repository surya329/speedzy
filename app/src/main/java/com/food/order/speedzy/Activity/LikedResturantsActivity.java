package com.food.order.speedzy.Activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.food.order.speedzy.Adapter.LikedResturantsActivityAdapter;
import com.food.order.speedzy.Model.ViewOrderModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.api.response.OrderItem;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */

public class LikedResturantsActivity extends AppCompatActivity {
  RecyclerView recyclerView;
  LinearLayoutManager lm;
  LikedResturantsActivityAdapter likedResturantsActivityAdapter;
  String userid, Veg_Flag;
  MySharedPrefrencesData mysharedpref;
  ViewOrderModel viewordermodel = new ViewOrderModel();
  List<OrderItem> orderlist = new ArrayList<>();
  RelativeLayout noorder;
  private Toolbar toolbar;
  LoaderDiloag loaderDiloag;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_my_order);

    mysharedpref = new MySharedPrefrencesData();
    userid = mysharedpref.getUser_Id(this);
    Veg_Flag = mysharedpref.getVeg_Flag(LikedResturantsActivity.this);

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    if (Veg_Flag.equalsIgnoreCase("1")) {
      statusbar_bg(R.color.red);
    } else {
      statusbar_bg(R.color.red);
    }
    loaderDiloag = new LoaderDiloag(this);

    callapiorder();
    lm = new LinearLayoutManager(this);
    noorder = (RelativeLayout) findViewById(R.id.noorder);

    recyclerView = (RecyclerView) findViewById(R.id.recyclerView_order);
    recyclerView.setLayoutManager(lm);
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    recyclerView.setHasFixedSize(true);
  }

  private void statusbar_bg(int color) {
    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
        .getColor(color)));
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
    }
  }

  private void callapiorder() {

    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    Call<ViewOrderModel> call = methods.getvieworders(userid,"1");
    Log.d("url", "url=" + call.request().url().toString());
    loaderDiloag.displayDiloag();
    call.enqueue(new Callback<ViewOrderModel>() {
      @Override
      public void onResponse(Call<ViewOrderModel> call, Response<ViewOrderModel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        viewordermodel = response.body();
        if (viewordermodel.getStatus().equalsIgnoreCase("Success")) {
          orderlist = viewordermodel.getOrder();
          if (orderlist.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            noorder.setVisibility(View.GONE);
            likedResturantsActivityAdapter =
                new LikedResturantsActivityAdapter(LikedResturantsActivity.this, orderlist,
                    viewordermodel.getStatus());
            recyclerView.setAdapter(likedResturantsActivityAdapter);
            likedResturantsActivityAdapter.notifyDataSetChanged();
          } else {
            recyclerView.setVisibility(View.GONE);
            noorder.setVisibility(View.VISIBLE);
          }
        } else {
          recyclerView.setVisibility(View.GONE);
          noorder.setVisibility(View.VISIBLE);
        }
        loaderDiloag.dismissDiloag();
      }

      @Override
      public void onFailure(Call<ViewOrderModel> call, Throwable t) {
        Toast.makeText(getApplicationContext(), "internet not available..connect internet",
            Toast.LENGTH_LONG).show();
        loaderDiloag.dismissDiloag();
      }
    });
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case android.R.id.home:
        Commons.back_button_transition(LikedResturantsActivity.this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    Commons.back_button_transition(LikedResturantsActivity.this);
  }
}
