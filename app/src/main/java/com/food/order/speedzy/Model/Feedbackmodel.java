package com.food.order.speedzy.Model;


/**
 * Created by Sujata Mohanty.
 */

public class Feedbackmodel {


    private String status;
    private String msg;
    private String OrderID;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }
}
