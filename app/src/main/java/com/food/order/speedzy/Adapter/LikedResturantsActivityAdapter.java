package com.food.order.speedzy.Adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.R;
import com.food.order.speedzy.api.response.OrderItem;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class LikedResturantsActivityAdapter
    extends RecyclerView.Adapter<LikedResturantsActivityAdapter.ViewHolder> {
  Context context;
  List<OrderItem> orderlist;
  String status;
  public static int sCorner = 15;
  public static int sMargin = 8;

  public LikedResturantsActivityAdapter(Context context, List<OrderItem> orderlist,
      String status) {
    this.context = context;
    this.orderlist = orderlist;
    this.status = status;
  }

  @Override
  public LikedResturantsActivityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.like_rex_row, parent, false);
    System.out.println("View Holder");
    // set the view's size, margins, paddings and layout parameters
    LikedResturantsActivityAdapter.ViewHolder vh = new LikedResturantsActivityAdapter.ViewHolder(v);
    return vh;
  }

  @Override
  public void onBindViewHolder(final LikedResturantsActivityAdapter.ViewHolder holder,
      int position) {
    final OrderItem orderin = orderlist.get(position);
    String imageurl = orderin.getImage();
    imageurl = imageurl.replace("\\", "/");
    orderin.setImage(imageurl);

    Glide.with(context)
        .load(imageurl)
            .apply(RequestOptions.bitmapTransform(new RoundedCorners(sCorner)))
            .into(holder.itemImage);

    holder.itemTitle.setText(orderin.getRestaurantName());
  }

  @Override
  public int getItemCount() {
    return orderlist.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView itemTitle, status, sectionTitle, totalprice, reorder, order_time;
    ImageView itemImage;
    RelativeLayout relative;

    public ViewHolder(View itemView) {
      super(itemView);
      itemTitle = (TextView) itemView.findViewById(R.id.rex_name);
      itemImage = (ImageView) itemView.findViewById(R.id.rex_img);
    }
  }
}
