package com.food.order.speedzy.api.response.delivery;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Data{

	@SerializedName("listDriverLocation")
	private List<ListDriverLocationItem> listDriverLocation;

	public List<ListDriverLocationItem> getListDriverLocation(){
		return listDriverLocation;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"listDriverLocation = '" + listDriverLocation + '\'' + 
			"}";
		}
}