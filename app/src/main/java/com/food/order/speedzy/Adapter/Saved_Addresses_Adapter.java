package com.food.order.speedzy.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.Activity.AddAddresses;
import com.food.order.speedzy.Model.AddUserAddressModel;
import com.food.order.speedzy.Model.UserAddressModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;

import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Sujata Mohanty.
 */


public class Saved_Addresses_Adapter extends RecyclerView.Adapter<Saved_Addresses_Adapter.MyViewHolder> {

    private ArrayList<UserAddressModel.info> deliveryAddList;
    Activity mContext;
    private Saved_Addresses_Adapter.OnItemClickListener onItemClickListener;
    MySharedPrefrencesData mySharedPrefrencesData;

    public void setOnItemClickListener(Saved_Addresses_Adapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout mParentLayout;
        public TextView type, location, landmark;
        ImageView image1,more;
        private MyViewHolder(View view) {
            super(view);
            mParentLayout = (RelativeLayout) itemView.findViewById(R.id.predictedRow);
            type = (TextView) itemView.findViewById(R.id.type);
            location = (TextView) itemView.findViewById(R.id.location);
            landmark = (TextView) itemView.findViewById(R.id.landmark);
            image1 = (ImageView) itemView.findViewById(R.id.image1);
            more= itemView.findViewById(R.id.more);
        }
    }

    public Saved_Addresses_Adapter(Activity mContext, ArrayList<UserAddressModel.info> deliveryAddList) {
        this.deliveryAddList = deliveryAddList;
        this.mContext = mContext;
    }

    @Override
    public Saved_Addresses_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_recent_search, parent, false);
        mySharedPrefrencesData=new MySharedPrefrencesData();
        return new Saved_Addresses_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final Saved_Addresses_Adapter.MyViewHolder holder, final int position) {
        final UserAddressModel.info deliveryData = deliveryAddList.get(position);
        holder.location.setVisibility(View.VISIBLE);
        holder.type.setVisibility(View.VISIBLE);
        holder.image1.setVisibility(View.VISIBLE);
        if (deliveryData.getAddress_type().equalsIgnoreCase("0")) {
            holder.type.setText("Other");
            holder.image1.setImageResource(R.drawable.ic_location);
        }
        if (deliveryData.getAddress_type().equalsIgnoreCase("1")) {
            holder.type.setText("Home");
            holder.image1.setImageResource(R.drawable.ic_home);
        }
        if (deliveryData.getAddress_type().equalsIgnoreCase("2")) {
            holder.type.setText("Work");
            holder.image1.setImageResource(R.drawable.ic_work_grey);
        }
        holder.location.setText(deliveryData.getComplete_address());

        if (!deliveryData.getLandmark().equalsIgnoreCase("")) {
            holder.landmark.setVisibility(View.VISIBLE);
            holder.landmark.setText(deliveryData.getLandmark());
        } else {
            holder.landmark.setVisibility(View.GONE);
        }
        holder.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(holder.type.getText().toString(),deliveryData.getCityid().toString(),holder.location.getText().toString(),deliveryData.getLongitude().toString(),
                            deliveryData.getLatitude().toString(),deliveryData.getAddress_id().toString(),deliveryData.getName().toString(),deliveryData.getArea().toString(),
                            deliveryData.getPincode().toString(),deliveryData.getAddress_type().toString(),deliveryData.getLandmark().toString());
                }
            }
        });
        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupWindow popup = new PopupWindow(mContext);
                View layout = ((Activity)mContext).getLayoutInflater().inflate(R.layout.popup_more, null);
                popup.setContentView(layout);
                // Set content width and height
                popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                // Closes the popup window when touch outside of it - when looses focus
                popup.setOutsideTouchable(true);
                popup.setFocusable(true);
                // Show anchored to button
                popup.setBackgroundDrawable(new BitmapDrawable());
                popup.showAsDropDown(view);

                TextView edit=(TextView) layout.findViewById(R.id.edit);
                TextView delete=(TextView) layout.findViewById(R.id.delete);
                TextView set_default=(TextView) layout.findViewById(R.id.set_default);

                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, AddAddresses.class);
                        intent.putExtra("change_address", true);
                        intent.putExtra("edit_address", true);
                        intent.putExtra("locality",deliveryAddList.get(position).getComplete_address());
                        intent.putExtra("latitude",deliveryAddList.get(position).getLatitude());
                        intent.putExtra("longitude",deliveryAddList.get(position).getLongitude());
                        intent.putExtra("flag","1");
                        intent.putExtra("address_data",deliveryAddList.get(position));
                        mContext.startActivityForResult(intent, 1);
                        mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        popup.dismiss();
                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callApi(popup,position);
                    }
                });
                set_default.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popup.dismiss();
                    }
                });

            }
        });
    }

    private void callApi(PopupWindow popup, int pos) {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(mContext).create(Apimethods.class);
        Call<AddUserAddressModel> call = methods.delete_address(deliveryAddList.get(pos).getAddress_id());
        Log.d("url","url="+call.request().url().toString());
        call.enqueue(new Callback<AddUserAddressModel>() {
            @Override
            public void onResponse(Call<AddUserAddressModel> call, Response<AddUserAddressModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                AddUserAddressModel response1 = response.body();
                if (response1.getStatus().equalsIgnoreCase("Success")) {
                    if (deliveryAddList.get(pos).getAddress_id().equalsIgnoreCase(mySharedPrefrencesData.getSelectAddress_ID(mContext))) {
                        mySharedPrefrencesData.setLocation(mContext, "");
                        mySharedPrefrencesData.setSelectCity(mContext, "FOODCITYBAM");
                        mySharedPrefrencesData.setSelectCity_latitude(mContext, "");
                        mySharedPrefrencesData.setSelectCity_longitude(mContext, "");
                        mySharedPrefrencesData.setSelectAddress_ID(mContext, "");
                        mySharedPrefrencesData.setSelectArea(mContext, "");
                        mySharedPrefrencesData.setSelectName(mContext, "");
                        mySharedPrefrencesData.setSelectMobile(mContext, mySharedPrefrencesData.get_Party_mobile(mContext));
                        mySharedPrefrencesData.setSelectPincode(mContext, "");
                        mySharedPrefrencesData.setSelectAddresstype(mContext, "");
                        mySharedPrefrencesData.setSelectLandMark(mContext, "");
                    }
                    popup.dismiss();
                    deliveryAddList.remove(pos);
                    notifyItemRemoved(pos);
                    notifyItemRangeChanged(pos,deliveryAddList.size());

                }
            }
            @Override
            public void onFailure(Call<AddUserAddressModel> call, Throwable t) {
                Toast.makeText(mContext, "internet not available..connect internet", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return deliveryAddList.size();
    }

    public interface OnItemClickListener {

        void onItemClick(String type, String cityid, String selected_location, String longi, String lati, String address_id, String name, String area, String pincode, String address_type, String landmark);

    }
}