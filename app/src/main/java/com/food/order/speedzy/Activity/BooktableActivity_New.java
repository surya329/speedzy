package com.food.order.speedzy.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.Adapter.BookTableAdapter;
import com.food.order.speedzy.Adapter.BooktableTimeListAdapter;
import com.food.order.speedzy.Model.Bookmodel;
import com.food.order.speedzy.Model.Restaurant_model;
import com.food.order.speedzy.Model.DateModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.AllValidation;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */

public class BooktableActivity_New extends AppCompatActivity {
    private Toolbar toolbar;
    RecyclerView date_recycler_view_list,time_recycler_view_list;
    BookTableAdapter bookTableAdapter;
    BooktableTimeListAdapter booktableTimeListAdapter;
    List<DateModel> datelist=new ArrayList<>();
    String rest_id,rest_name,rest_street,rest_open_close_cuisine,rest_image;
    EditText name,email,phone,person_no,comment;
    String date_sentto_web="";
    String time_sentto_web="";
    TextView book_table;
    String name1,email1,phone1,person1,comment1,Veg_Flag;
    MySharedPrefrencesData mySharedPrefrencesData;
    String userid,menu_id,nav_type;
    Dialog dialog;
    TextView warn,ok;
    Restaurant_model.PickupSchedule pickupSchedule;
    String[] arrayspinner=new String[0];
    Date starttime_lunch, endtime_lunch, starttime_dinner, endtime_dinner,newtime;
    boolean istimecycle = true;
    TextView text1;
    String start_time_lunch,end_time_lunch,start_time_dinner,end_time_dinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booktable__new);

        mySharedPrefrencesData=new MySharedPrefrencesData();
        Veg_Flag=mySharedPrefrencesData.getVeg_Flag(BooktableActivity_New.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (Veg_Flag.equalsIgnoreCase("1")){
            statusbar_bg(R.color.red);
        }else {
            statusbar_bg(R.color.red);
        }
        dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.order_successful_popup);
        warn=(TextView)dialog.findViewById(R.id.warning);
        ok=(TextView)dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });

        menu_id= getIntent().getStringExtra("menu_id");
        nav_type= getIntent().getStringExtra("nav_type");
        rest_id=getIntent().getStringExtra("res_id");
        rest_image=getIntent().getStringExtra("res_img");
        rest_name=getIntent().getStringExtra("res_name");
        rest_street=getIntent().getStringExtra("rest_street_details");
        start_time_lunch= getIntent().getStringExtra("start_time_lunch");
        end_time_lunch= getIntent().getStringExtra("end_time_lunch");
        start_time_dinner= getIntent().getStringExtra("start_time_dinner");
        end_time_dinner= getIntent().getStringExtra("end_time_dinner");
        rest_open_close_cuisine=getIntent().getStringExtra("rest_close_open_rest_venue_cuisine");
        pickupSchedule = (Restaurant_model.PickupSchedule) getIntent().getSerializableExtra("pickupSchedule");

        name=(EditText) findViewById(R.id.name);
        email=(EditText)findViewById(R.id.email);
        phone=(EditText)findViewById(R.id.mobile);
        person_no=(EditText)findViewById(R.id.no_of_person);
        comment=(EditText) findViewById(R.id.comment);
        book_table=(TextView) findViewById(R.id.book_table);
        text1=(TextView) findViewById(R.id.text1);


        SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE");
        SimpleDateFormat sdf2 = new SimpleDateFormat("MMM dd");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < 4; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            String day = sdf1.format(calendar.getTime());
            String date = sdf2.format(calendar.getTime());
            String sent_to_api_date = inputFormat.format(calendar.getTime());
            DateModel dateModel=new DateModel(day,date,sent_to_api_date);
            datelist.add(dateModel);
        }
        time_adapter_set(0);
        date_recycler_view_list = (RecyclerView) findViewById(R.id.date_recycler_view_list);
        date_recycler_view_list.setHasFixedSize(true);
        date_recycler_view_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        date_recycler_view_list.setNestedScrollingEnabled(false);
        bookTableAdapter = new BookTableAdapter(BooktableActivity_New.this, datelist);
        date_recycler_view_list.setAdapter(bookTableAdapter);

        bookTableAdapter.setOnItemClickListener(new BookTableAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, String return_date, int position) {
                date_sentto_web=return_date;
                time_adapter_set(position);
                booktableTimeListAdapter.notifyDataSetChanged();
            }
        });

        book_table.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name1=name.getText().toString();
                email1=email.getText().toString();
                phone1=phone.getText().toString();
                person1=person_no.getText().toString();
                comment1=comment.getText().toString();
                if (date_sentto_web.equalsIgnoreCase("")) {
                    date_sentto_web = datelist.get(0).getSent_to_api_date();
                }
                if (time_sentto_web.equalsIgnoreCase("")) {
                    time_sentto_web = arrayspinner[0];
                }
                if(AllValidation.book_table_validate(name1,email1,phone1,person1,date_sentto_web,time_sentto_web, "1", "1", BooktableActivity_New.this)){
                    callapi_book();
                }
            }


        });

    }

    private void time_adapter_set(int pos) {
        ArrayList<String> picktime = callfortime_pick(/*pickupSchedule*/start_time_lunch,end_time_lunch,start_time_dinner,end_time_dinner);
        DateFormat dateFormatcuurenttime = new SimpleDateFormat("H:mm");
        String str = dateFormatcuurenttime.format(new Date());
        Date cuurenttime = null;
        try {
            cuurenttime = dateFormatcuurenttime.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArrayList<String> time_new_with_cuurenttime_pick = new ArrayList<>();
        for (int i = 0; i < picktime.size(); i++) {
            Date timecheck = null;
            try {
                timecheck = dateFormatcuurenttime.parse(picktime.get(i));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (pos==0) {
                if (timecheck.compareTo(cuurenttime) > 0){
                    time_new_with_cuurenttime_pick.add(dateFormatcuurenttime.format(timecheck));
                }
            }else {
                time_new_with_cuurenttime_pick.add(dateFormatcuurenttime.format(timecheck));
            }
        }
        if (time_new_with_cuurenttime_pick.size() > 0) {
            time_recycler_view_list = (RecyclerView) findViewById(R.id.time_recycler_view_list);
            time_recycler_view_list.setHasFixedSize(true);
            time_recycler_view_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            time_recycler_view_list.setNestedScrollingEnabled(false);
            arrayspinner = time_new_with_cuurenttime_pick.toArray(new String[0]);
            if (arrayspinner.length>0) {
                time_recycler_view_list.setVisibility(View.VISIBLE);
                text1.setVisibility(View.VISIBLE);
                booktableTimeListAdapter = new BooktableTimeListAdapter(BooktableActivity_New.this, arrayspinner);
                time_recycler_view_list.setAdapter(booktableTimeListAdapter);
                booktableTimeListAdapter.setOnItemClickListener(new BooktableTimeListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        time_sentto_web=arrayspinner[position];
                    }
                });
            }else {
                text1.setVisibility(View.GONE);
                time_recycler_view_list.setVisibility(View.GONE);
            }
        }
    }

    private ArrayList<String> callfortime_pick(/*Restaurant_model.PickupSchedule pickupSchedule*/ String start_time_lunch,String end_time_lunch,
                                                                                                  String start_time_dinner,String end_time_dinner) {

        DateFormat dateFormat = new SimpleDateFormat("H:mm");
        DateFormat dateFormat2 = new SimpleDateFormat("H:mm");

        ArrayList<String> time_todaylist = new ArrayList<>();
        try {

            if (!(start_time_lunch == null)) {
                starttime_lunch = dateFormat.parse(start_time_lunch);
                endtime_lunch = dateFormat.parse(end_time_lunch);
                newtime = starttime_lunch;
                time_todaylist.add(String.valueOf(dateFormat2.format(starttime_lunch)));

                while (newtime.compareTo(endtime_lunch) < 0 && istimecycle == true) {
                    Date timget = covert_time_for_add(newtime, endtime_lunch);
                    time_todaylist.add(String.valueOf(dateFormat2.format(timget)));
                }
            }

            if (!(start_time_dinner == null)) {
                starttime_dinner = dateFormat.parse(start_time_dinner);
                endtime_dinner = dateFormat.parse(end_time_dinner);
                System.out.println("Time: " + dateFormat.format(starttime_dinner));
                newtime = starttime_dinner;
                time_todaylist.add(String.valueOf(dateFormat2.format(starttime_dinner)));

                while (newtime.compareTo(endtime_dinner) < 0 && istimecycle == true) {
                    Date timget = covert_time_for_add(newtime, endtime_dinner);
                    time_todaylist.add(String.valueOf(dateFormat2.format(timget)));
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("lunch_del_arr", time_todaylist + "");
        return time_todaylist;

    }
    public Date covert_time_for_add(Date date, Date dateend) {

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, 30);
        String newTime1 = df.format(cal.getTime());
        Date addedtime = null;
        try {
            if (newTime1.equalsIgnoreCase("00:00")) {
                istimecycle = false;
                newtime = dateend;
            } else {
                addedtime = df.parse(newTime1);
                newtime = addedtime;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newtime;
    }

    private void callapi_book() {
        userid=mySharedPrefrencesData.getUser_Id(this);
        if(userid.length()<1){
            userid="0";
        }else {
            userid=mySharedPrefrencesData.getUser_Id(this);
        }

        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);

        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
        String date_order = dateFormatter.format(Calendar.getInstance().getTime());
        String bookingdate=date_sentto_web+" "+time_sentto_web;

        Call<Bookmodel> call =methods.setBook(userid,rest_id,name1,phone1,person1,email1,comment1,bookingdate,date_order);
        Log.d("url","url="+call.request().url().toString());

        call.enqueue(new Callback<Bookmodel>() {
            @Override
            public void onResponse(Call<Bookmodel> call, Response<Bookmodel> response) {
                int statusCode = response.code();
                Log.d("Response",""+statusCode);
                Log.d("respones",""+response);
                Bookmodel feedback=response.body();
                if(feedback.getStatus().equalsIgnoreCase("Success"))
                {
                    warn.setText("Your Order has been successful.");
                    dialog.show();
                }else
                {
                    Toast.makeText(BooktableActivity_New.this,"Table booked unsuccessfully", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Bookmodel> call, Throwable t) {
                Toast.makeText(BooktableActivity_New.this,"internet not available..connect internet", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,color ));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                if (nav_type.equalsIgnoreCase("3") && menu_id.equalsIgnoreCase("M1014")){
                    Intent intent=new Intent();
                    setResult(1,intent);
            }
                Commons.back_button_transition(BooktableActivity_New.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        if (nav_type.equalsIgnoreCase("3") && menu_id.equalsIgnoreCase("M1014")){
            Intent intent=new Intent();
            setResult(1,intent);
        }
        Commons.back_button_transition(BooktableActivity_New.this);
    }

}
