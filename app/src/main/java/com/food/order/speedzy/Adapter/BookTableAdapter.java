package com.food.order.speedzy.Adapter;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.food.order.speedzy.Model.DateModel;
import com.food.order.speedzy.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



/**
 * Created by Sujata Mohanty.
 */


public class BookTableAdapter extends RecyclerView.Adapter<BookTableAdapter.MyView> {

    private List<DateModel> list;
    private BookTableAdapter.OnItemClickListener mOnItemClickListener;
    Context context;
    int row_index=0;
    private int mYear, mMonth, mDay;
    String sent_to_api_date="";
    public BookTableAdapter(Context context, List<DateModel> list) {
        this.context=context;
        this.list=list;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, String date, int position);
    }

    public void setOnItemClickListener(BookTableAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public class MyView extends RecyclerView.ViewHolder {

        public TextView day;
        public MyView(View view) {
            super(view);
            day = (TextView) view.findViewById(R.id.day);
        }
    }

    @Override
    public BookTableAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.date_item_row, parent, false);

        return new BookTableAdapter.MyView(itemView);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final BookTableAdapter.MyView holder, final int position) {
        DateModel dateModel=list.get(position);
        if (position==0){
            holder.day.setText("Today"+"\n"+dateModel.getDate1());
            holder.day.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        }else if (position==1){
            holder.day.setText("Tomorrow"+"\n"+dateModel.getDate1());
            holder.day.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        }else if (position==2) {
            holder.day.setText(dateModel.getDay()+"\n"+dateModel.getDate1());
            holder.day.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        }else
        {
            holder.day.setText("Choose Date");
            holder.day.setTextColor(context.getResources().getColor(R.color.black_transparent));
            holder.day.setBackground(context.getDrawable(R.drawable.rectangle_border_grey));
           // holder.day.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_calender,0,0);
        }
        holder.day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    if (position==3){
                        datepicker_dialog_show(view,holder.day,position);
                    }else {
                        mOnItemClickListener.onItemClick(view, list.get(position).getSent_to_api_date(),position);
                    }
                    row_index = position;
                    notifyDataSetChanged();
                }
            }
        });
        if(row_index==position){
            holder.day.setTextColor(context.getResources().getColor(R.color.white));
            holder.day.setBackground(context.getDrawable(R.drawable.rectangle_red));
        }else
        {
            holder.day.setTextColor(context.getResources().getColor(R.color.red));
            holder.day.setBackground(context.getDrawable(R.drawable.date_bg_unselct));
        }
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.day.getLayoutParams();
        params.height = 100;
        holder.day.setLayoutParams(params);
    }

    private void datepicker_dialog_show(final View view1, final TextView day1,final int position) {
        final SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE");
        final SimpleDateFormat sdf2 = new SimpleDateFormat("MMM dd");
        final DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String selected_date_str=year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        Date selected_date = null;
                        try {
                            selected_date = inputFormat.parse(selected_date_str);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        sent_to_api_date = inputFormat.format(selected_date);
                        String day = sdf1.format(selected_date);
                        String date = sdf2.format(selected_date);
                        day1.setText(day+"\n"+date);
                        day1.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                        mOnItemClickListener.onItemClick(view1,sent_to_api_date, position);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
