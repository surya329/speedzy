package com.food.order.speedzy.screen.patanjali;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;

import java.util.Random;

public class GrocerySubCategoriesVH extends RecyclerView.ViewHolder {
  private final Callback mCallback;
  private final Context mContext;
  @BindView(R.id.ivExample) ImageView ivExample;
  @BindView(R.id.text1) TextView header;
  @BindView(R.id.text2) TextView footer;
  @BindView(R.id.cardview)
  LinearLayout cardview;
  public static int sCorner = 15;
  public static int sMargin = 8;
  public GrocerySubCategoriesVH(@NonNull View itemView,
      Callback callback) {
    super(itemView);
    mCallback = callback;
    mContext = itemView.getContext();
    ButterKnife.bind(this, itemView);
  }

  public void onBindView(GrocerySubcategoryList data) {
    //DrawableRequestBuilder<String> thumbnailRequest = Glide
    //    .with(mContext)
    //    .load(data.getImage())
    //    .bitmapTransform(new RoundedCorners( sCorner));
    if (!GeneralUtil.isStringEmpty(data.getImage())) {
      Glide.with(mContext)
          .load(data.getImage())
              .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner))
                      .placeholder(R.drawable.ic_product_coming_soon))
          .into(ivExample);
    } else {
      Glide.with(mContext)
          .load(R.drawable.ic_product_coming_soon)
              .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner))
                      .placeholder(R.drawable.ic_product_coming_soon))
          .into(ivExample);
    }
    header.setText(data.getName());
    footer.setText(data.getName());
    cardview.setBackgroundTintList(ColorStateList.valueOf(Commons.getRandomColor(mContext)));
  }

  @OnClick(R.id.cardview) void onClickContainerMain() {
    if (getAdapterPosition() != RecyclerView.NO_POSITION) {
      mCallback.onClickItem(getAdapterPosition());
    }
  }

  public interface Callback {
    void onClickItem(int position);
  }
}
