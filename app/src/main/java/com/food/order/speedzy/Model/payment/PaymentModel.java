package com.food.order.speedzy.Model.payment;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PaymentModel{

	@SerializedName("payment_status")
	private String paymentStatus;

	@SerializedName("status")
	private String status;

	@SerializedName("Payment_type")
	private String paymentType;

	public String getPaymentStatus(){
		return paymentStatus;
	}

	public String getStatus(){
		return status;
	}

	public String getPaymentType(){
		return paymentType;
	}

	@Override
 	public String toString(){
		return 
			"PaymentModel{" + 
			"payment_status = '" + paymentStatus + '\'' + 
			",status = '" + status + '\'' + 
			",payment_type = '" + paymentType + '\'' + 
			"}";
		}
}