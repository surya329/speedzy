package com.food.order.speedzy.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.food.order.speedzy.Activity.HomeActivityNew;
import com.food.order.speedzy.R;

/**
 * Created by Sujata Mohanty.
 */


@SuppressLint("ValidFragment")
public class Help_Support_Fragment extends Fragment {
    String flag;
    LinearLayout activity_help, activity_terms, activity_about;
    View view;

    public Help_Support_Fragment(String flag) {
        this.flag = flag;
    }

    public Help_Support_Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_help__support_, container, false);

        activity_help = (LinearLayout) view.findViewById(R.id.activity_help);
        activity_terms = (LinearLayout) view.findViewById(R.id.activity_terms);
        activity_about = (LinearLayout) view.findViewById(R.id.activity_about);
        HomeActivityNew.current_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        if (flag.equalsIgnoreCase("1")) {
            HomeActivityNew.current_address.setText("Help & Support");
            activity_help.setVisibility(View.VISIBLE);
            activity_terms.setVisibility(View.GONE);
            activity_about.setVisibility(View.GONE);
        } else if (flag.equalsIgnoreCase("2") || flag.equalsIgnoreCase("4")) {
            if (flag.equalsIgnoreCase("2")) {
                HomeActivityNew.current_address.setText("Terms & Conditions");
            } else {
                HomeActivityNew.current_address.setText("Privacy Policies");
            }
            activity_help.setVisibility(View.GONE);
            activity_terms.setVisibility(View.VISIBLE);
            activity_about.setVisibility(View.GONE);
        } else if (flag.equalsIgnoreCase("3")) {
            HomeActivityNew.current_address.setText("About Us");
            activity_help.setVisibility(View.GONE);
            activity_terms.setVisibility(View.GONE);
            activity_about.setVisibility(View.VISIBLE);
        } else {
            HomeActivityNew.current_address.setText("");
            activity_help.setVisibility(View.GONE);
            activity_terms.setVisibility(View.GONE);
            activity_about.setVisibility(View.GONE);
        }
        return view;
    }
}
