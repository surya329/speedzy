package com.food.order.speedzy.api.response.createorder;

import com.google.gson.annotations.SerializedName;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CreateOrdersItem{

	@SerializedName("deliveryGuyId")
	private String deliveryGuyId;

	@SerializedName("orderNumber")
	private String orderNumber;

	@SerializedName("appId")
	private String appId;

	public String getDeliveryGuyId(){
		return deliveryGuyId;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public String getAppId(){
		return appId;
	}

	@Override
 	public String toString(){
		return 
			"UserRegistrationModel{" +
			"deliveryGuyId = '" + deliveryGuyId + '\'' + 
			",orderNumber = '" + orderNumber + '\'' + 
			",appId = '" + appId + '\'' + 
			"}";
		}
}