package com.food.order.speedzy.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Activity.MealCombo_Checkout_Activity;
import com.food.order.speedzy.Activity.PatanjaliProduct_DetailsActivity;
import com.food.order.speedzy.Model.Cakes_Model;
import com.food.order.speedzy.Model.Combo_Response_Model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.michael.easydialog.EasyDialog;

import java.util.List;

/**
 * Created by Sujata Mohanty.
 */


public class MealComboActivityAdapter extends RecyclerView.Adapter<MealComboActivityAdapter.SingleItemRowHolder> {

    private List<Combo_Response_Model.combos.Itemlist> itemsList;
    List<Cakes_Model.Cake> cakelist;
    private Activity mContext;
    public static int sCorner = 15;
    public static int sMargin = 8;
    String app_tittle="";
    Cakes_Model.Restaurant_timings delivery_time_Cake;
    Cakes_Model.open_close_status open_close_status_Cake;
    public MealComboActivityAdapter(Activity mContext, List<Combo_Response_Model.combos.Itemlist> itemlist) {
        this.itemsList = itemlist;
        this.mContext = mContext;
    }

    public MealComboActivityAdapter(Activity mContext, List<Cakes_Model.Cake> cakelist, Cakes_Model.Restaurant_timings delivery_time_Cake, Cakes_Model.open_close_status open_close_status_Cake, String app_tittle) {
        this.mContext=mContext;
        this.cakelist=cakelist;
        this.app_tittle=app_tittle;
        this.delivery_time_Cake=delivery_time_Cake;
        this.open_close_status_Cake=open_close_status_Cake;
    }

    @Override
    public MealComboActivityAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.meal_combo_row, null);
        MealComboActivityAdapter.SingleItemRowHolder mh = new MealComboActivityAdapter.SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(MealComboActivityAdapter.SingleItemRowHolder holder, int i) {
        if (app_tittle.equalsIgnoreCase("Cakes")){
            Cakes_Model.Cake items = cakelist.get(i);
            if (items.getDishDesription() != null && !items.getDishDesription().equalsIgnoreCase("")) {
                holder.tooltip.setVisibility(View.VISIBLE);
            } else {
                holder.tooltip.setVisibility(View.GONE);
            }
            String base_url_thumb_image =
                    "https://storage.googleapis.com/speedzy_data/resources/cakes/thumb_image/";
            String base_url_main_image =
                    "https://storage.googleapis.com/speedzy_data/resources/cakes/main_image/";
            String extension = "_1.png";
            RequestBuilder<Drawable> thumbnailRequest = Glide
                    .with(mContext)
                    .load(base_url_thumb_image+ items.getImagename());
            Glide.with(mContext)
                    .load(base_url_main_image + items.getImagename())
                    .thumbnail(thumbnailRequest)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.combo_placeholder))
                    .into(holder.itemImage);
            holder.name.setText(items.getStDishName());
            holder.price.setText("₹ " + items.getStPrice().get(0).getMenuPrice());
        }else {
            Combo_Response_Model.combos.Itemlist items = itemsList.get(i);
            if (items.getDish_desription() != null && !items.getDish_desription().equalsIgnoreCase("")) {
                holder.tooltip.setVisibility(View.VISIBLE);
            } else {
                holder.tooltip.setVisibility(View.GONE);
            }
            RequestBuilder<Drawable> thumbnailRequest = Glide
                    .with(mContext)
                    .load("https://storage.googleapis.com/speedzy_data/resources/mealcombo/thumb/" + items.getImagename());
            Glide.with(mContext)
                    .load("https://storage.googleapis.com/speedzy_data/resources/mealcombo/main_image/" + items.getImagename())
                    .thumbnail(thumbnailRequest)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.combo_placeholder))
                    .into(holder.itemImage);
            holder.name.setText(items.getStDishName());
            holder.price.setText("₹ " + items.getStPrice().get(0).getMenuPrice());
        }

        holder.tooltip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (app_tittle.equalsIgnoreCase("Cakes")){
                    ToolTipView(holder,cakelist.get( holder.getAdapterPosition()).getDishDesription());
                }else {
                    ToolTipView(holder, itemsList.get( holder.getAdapterPosition()).getDish_desription());
                }
            }
        });
        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (app_tittle.equalsIgnoreCase("Cakes")){
                    Commons.menu_id = SpeedzyConstants.CAKE_MENU_ID;
                    Intent intent = new Intent(mContext, MealCombo_Checkout_Activity.class);
                    intent.putExtra("flag", 3);
                    intent.putExtra("where", 1);
                    intent.putExtra("Cake_Details", cakelist.get(holder.getAdapterPosition()));
                    intent.putExtra("Cake_rest_timings", delivery_time_Cake);
                    intent.putExtra("Cake_res_open_status", open_close_status_Cake);
                    mContext.startActivity(intent);
                    mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }else {
                    Intent intent = new Intent(mContext, MealCombo_Checkout_Activity.class);
                    intent.putExtra("flag", 1);
                    intent.putExtra("Combo_Details",  itemsList.get(holder.getAdapterPosition()));
                    mContext.startActivity(intent);
                    mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
            }
        });
    }

    private void ToolTipView(SingleItemRowHolder holder, String msg) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.custom_tooltip, null);
            TextView amount = view.findViewById(R.id.amount);
            TextView desc = view.findViewById(R.id.desc);
             desc.setText(Html.fromHtml(msg));
            amount.setVisibility(View.GONE);
            new EasyDialog(mContext)
                    .setLayout(view)
                    .setBackgroundColor(mContext.getResources().getColor(R.color.red))
                    .setLocationByAttachedView(holder.tooltip)
                    .setGravity(EasyDialog.GRAVITY_TOP)
                    .setAnimationAlphaShow(100, 0.0f, 1.0f)
                    .setAnimationAlphaDismiss(100, 1.0f, 0.0f)
                    .setTouchOutsideDismiss(true)
                    .setMatchParent(false)
                    .setMarginLeftAndRight(24, 24)
                    .setOutsideColor(Color.TRANSPARENT)
                    .show();
    }

    @Override
    public int getItemCount() {
        if (app_tittle.equalsIgnoreCase("Cakes")){
            return cakelist.size();
        }else {
            return itemsList.size();
        }
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {
        TextView name,price;
        protected ImageView itemImage;
        CardView cardview;
        ImageView tooltip;
        public SingleItemRowHolder(View view) {
            super(view);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
            name= (TextView) view.findViewById(R.id.name);
            price=(TextView) view.findViewById(R.id.price);
            cardview=(CardView)view.findViewById(R.id.cardview);
            tooltip = view.findViewById(R.id.tooltip);
        }
    }
}