package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EventListModelNew implements Serializable {

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("events")
    @Expose
    private List<Event> events = null;
    public class Event implements Serializable {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("event_id")
        @Expose
        private String eventId;
        @SerializedName("event_title")
        @Expose
        private String eventTitle;
        @SerializedName("event_type")
        @Expose
        private String eventType;
        @SerializedName("event_description")
        @Expose
        private String eventDescription;
        @SerializedName("event_picture_name")
        @Expose
        private String eventPictureName;
        @SerializedName("event_picture_path")
        @Expose
        private String eventPicturePath;
        @SerializedName("event_created_by")
        @Expose
        private String eventCreatedBy;
        @SerializedName("city_id")
        @Expose
        private String cityId;
        @SerializedName("logo_image_name")
        @Expose
        private String logoImageName;
        @SerializedName("logo_image_path")
        @Expose
        private String logoImagePath;
        @SerializedName("event_owner_id")
        @Expose
        private String eventOwnerId;
        @SerializedName("event_price")
        @Expose
        private String eventPrice;
        @SerializedName("event_terms")
        @Expose
        private String eventTerms;
        @SerializedName("advance_amount")
        @Expose
        private String advanceAmount;
        @SerializedName("max_persons")
        @Expose
        private String maxPersons;
        @SerializedName("min_persons")
        @Expose
        private String minPersons;
        @SerializedName("additional_price_perperson")
        @Expose
        private String additionalPricePerperson;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEventId() {
            return eventId;
        }

        public void setEventId(String eventId) {
            this.eventId = eventId;
        }

        public String getEventTitle() {
            return eventTitle;
        }

        public void setEventTitle(String eventTitle) {
            this.eventTitle = eventTitle;
        }

        public String getEventType() {
            return eventType;
        }

        public void setEventType(String eventType) {
            this.eventType = eventType;
        }

        public String getEventDescription() {
            return eventDescription;
        }

        public void setEventDescription(String eventDescription) {
            this.eventDescription = eventDescription;
        }

        public String getEventPictureName() {
            return eventPictureName;
        }

        public void setEventPictureName(String eventPictureName) {
            this.eventPictureName = eventPictureName;
        }

        public String getEventPicturePath() {
            return eventPicturePath;
        }

        public void setEventPicturePath(String eventPicturePath) {
            this.eventPicturePath = eventPicturePath;
        }

        public String getEventCreatedBy() {
            return eventCreatedBy;
        }

        public void setEventCreatedBy(String eventCreatedBy) {
            this.eventCreatedBy = eventCreatedBy;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getLogoImageName() {
            return logoImageName;
        }

        public void setLogoImageName(String logoImageName) {
            this.logoImageName = logoImageName;
        }

        public String getLogoImagePath() {
            return logoImagePath;
        }

        public void setLogoImagePath(String logoImagePath) {
            this.logoImagePath = logoImagePath;
        }

        public String getEventOwnerId() {
            return eventOwnerId;
        }

        public void setEventOwnerId(String eventOwnerId) {
            this.eventOwnerId = eventOwnerId;
        }

        public String getEventPrice() {
            return eventPrice;
        }

        public void setEventPrice(String eventPrice) {
            this.eventPrice = eventPrice;
        }

        public String getEventTerms() {
            return eventTerms;
        }

        public void setEventTerms(String eventTerms) {
            this.eventTerms = eventTerms;
        }

        public String getAdvanceAmount() {
            return advanceAmount;
        }

        public void setAdvanceAmount(String advanceAmount) {
            this.advanceAmount = advanceAmount;
        }

        public String getMaxPersons() {
            return maxPersons;
        }

        public void setMaxPersons(String maxPersons) {
            this.maxPersons = maxPersons;
        }

        public String getMinPersons() {
            return minPersons;
        }

        public void setMinPersons(String minPersons) {
            this.minPersons = minPersons;
        }

        public String getAdditionalPricePerperson() {
            return additionalPricePerperson;
        }

        public void setAdditionalPricePerperson(String additionalPricePerperson) {
            this.additionalPricePerperson = additionalPricePerperson;
        }

        public String getEventAddress() {
            return eventAddress;
        }

        public void setEventAddress(String eventAddress) {
            this.eventAddress = eventAddress;
        }

        public String getEventLat() {
            return eventLat;
        }

        public void setEventLat(String eventLat) {
            this.eventLat = eventLat;
        }

        public String getEventLong() {
            return eventLong;
        }

        public void setEventLong(String eventLong) {
            this.eventLong = eventLong;
        }

        public String getDisplaySequence() {
            return displaySequence;
        }

        public void setDisplaySequence(String displaySequence) {
            this.displaySequence = displaySequence;
        }

        public String getEventAcStatus() {
            return eventAcStatus;
        }

        public void setEventAcStatus(String eventAcStatus) {
            this.eventAcStatus = eventAcStatus;
        }

        public String getEventDelStatus() {
            return eventDelStatus;
        }

        public void setEventDelStatus(String eventDelStatus) {
            this.eventDelStatus = eventDelStatus;
        }

        @SerializedName("event_address")
        @Expose
        private String eventAddress;
        @SerializedName("event_lat")
        @Expose
        private String eventLat;
        @SerializedName("event_long")
        @Expose
        private String eventLong;
        @SerializedName("display_sequence")
        @Expose
        private String displaySequence;
        @SerializedName("event_ac_status")
        @Expose
        private String eventAcStatus;
        @SerializedName("event_del_status")
        @Expose
        private String eventDelStatus;
    }
}
