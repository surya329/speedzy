package com.food.order.speedzy.Adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.food.order.speedzy.R;



/**
 * Created by Sujata Mohanty.
 */


public class BooktableTimeListAdapter extends RecyclerView.Adapter<BooktableTimeListAdapter.ViewHolder> {

    Context context;
    String[] timelist;
    int row_index=0;
    private BooktableTimeListAdapter.OnItemClickListener mOnItemClickListener;
    public BooktableTimeListAdapter(Context context, String[] timelist) {
        this.context=context;
        this.timelist=timelist;
    }
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(BooktableTimeListAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @Override
    public BooktableTimeListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.date_item_row, parent, false);
        System.out.println("View Holder");
        // set the view's size, margins, paddings and layout parameters
        BooktableTimeListAdapter.ViewHolder vh = new BooktableTimeListAdapter.ViewHolder(v);
        return vh;
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(BooktableTimeListAdapter.ViewHolder holder, final int position) {
        if (timelist.length>0) {
            holder.time.setText(timelist[position].toString());
            holder.time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view,position);
                        row_index = position;
                        notifyDataSetChanged();
                    }
                }
            });
            if (row_index == position) {
                holder.time.setTextColor(context.getResources().getColor(R.color.white));
                holder.time.setBackground(context.getDrawable(R.drawable.rectangle_red));
            } else {
                holder.time.setTextColor(context.getResources().getColor(R.color.red));
                holder.time.setBackground(context.getDrawable(R.drawable.date_bg_unselct));
            }
        }
    }

    @Override
    public int getItemCount() {
        return timelist.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView time;
        public ViewHolder(View itemView) {
            super(itemView);
            time=(TextView)itemView.findViewById(R.id.day);
        }
    }

}
