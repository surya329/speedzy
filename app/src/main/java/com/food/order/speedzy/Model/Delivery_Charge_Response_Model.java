package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Delivery_Charge_Response_Model implements Serializable {
    @SerializedName("status")
    @Expose
    public String status;

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    @SerializedName("orderno")
    @Expose
    public String orderno;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeliverycharge() {
        return deliverycharge;
    }

    public void setDeliverycharge(String deliverycharge) {
        this.deliverycharge = deliverycharge;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    @SerializedName("deliverycharge")
    @Expose
    public String deliverycharge;
    @SerializedName("Offers")
    @Expose
    public List<Offer> offers = null;
    public class Offer implements Serializable {
        @SerializedName("offer_title")
        @Expose
        public String offerTitle;
        @SerializedName("offer_description")
        @Expose
        public String offerDescription;
        @SerializedName("offer_terms")
        @Expose
        public String offerTerms;
        @SerializedName("st_del_pic_condition")
        @Expose
        public String stDelPicCondition;
        @SerializedName("flg_offer_type")
        @Expose
        public String flgOfferType;
        @SerializedName("second_offer_value")
        @Expose
        public String secondOfferValue;

        private float offerprice;
        public float getOfferprice() {
            return offerprice;
        }

        public void setOfferprice(float offerprice) {
            this.offerprice = offerprice;
        }

        public String getOfferTitle() {
            return offerTitle;
        }

        public void setOfferTitle(String offerTitle) {
            this.offerTitle = offerTitle;
        }

        public String getOfferDescription() {
            return offerDescription;
        }

        public void setOfferDescription(String offerDescription) {
            this.offerDescription = offerDescription;
        }

        public String getOfferTerms() {
            return offerTerms;
        }

        public void setOfferTerms(String offerTerms) {
            this.offerTerms = offerTerms;
        }

        public String getStDelPicCondition() {
            return stDelPicCondition;
        }

        public void setStDelPicCondition(String stDelPicCondition) {
            this.stDelPicCondition = stDelPicCondition;
        }

        public String getFlgOfferType() {
            return flgOfferType;
        }

        public void setFlgOfferType(String flgOfferType) {
            this.flgOfferType = flgOfferType;
        }

        public String getSecondOfferValue() {
            return secondOfferValue;
        }

        public void setSecondOfferValue(String secondOfferValue) {
            this.secondOfferValue = secondOfferValue;
        }

        public String getFirstOfferValue() {
            return firstOfferValue;
        }

        public void setFirstOfferValue(String firstOfferValue) {
            this.firstOfferValue = firstOfferValue;
        }

        public String getThirdOfferValue() {
            return thirdOfferValue;
        }

        public void setThirdOfferValue(String thirdOfferValue) {
            this.thirdOfferValue = thirdOfferValue;
        }

        @SerializedName("first_offer_value")
        @Expose
        public String firstOfferValue;
        @SerializedName("third_offer_value")
        @Expose
        public String thirdOfferValue;
    }
}
