package com.food.order.speedzy.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.Activity.PatanjaliProductActivity;
import com.food.order.speedzy.Activity.PatanjaliProduct_DetailsActivity;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.reyclerviewutils.RecyclerViewUtils;
import com.food.order.speedzy.screen.patanjali.GroceryCategoryList;
import com.food.order.speedzy.screen.patanjali.GrocerySubCategoriesAdapter;
import com.food.order.speedzy.screen.patanjali.GrocerySubcategoryList;

import java.util.ArrayList;
import java.util.List;

public class GrocerySubCategoriesAdapterNew extends RecyclerView.Adapter<GrocerySubCategoriesAdapterNew.GrocerySubCategoriesVHNew> {
    private List<GroceryCategoryList> mData;
    Activity contex;
    GrocerySubCategoriesAdapter adapter2;
    public GrocerySubCategoriesAdapterNew(Activity mcontex, List<GroceryCategoryList> mData) {
        this.mData = mData;
        contex=mcontex;
    }

    @NonNull
    @Override
    public GrocerySubCategoriesVHNew onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view =
                LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.viewhokder_two_new, viewGroup, false);
        return new GrocerySubCategoriesVHNew(view,contex);
    }

    @Override
    public void onBindViewHolder(@NonNull GrocerySubCategoriesVHNew holder, int i) {
        holder.my_recycler_view.setLayoutManager(RecyclerViewUtils.newGridLayoutManager(contex,2));
        holder.my_recycler_view.setNestedScrollingEnabled(false);
        holder.my_recycler_view.setVisibility(View.VISIBLE);
        holder.category.setText(mData.get(i).getName());
        adapter2 = new GrocerySubCategoriesAdapter(new GrocerySubCategoriesAdapter.Callback() {
            @Override public void onClickItem(GrocerySubcategoryList data, String categoryId) {
                goToDetailsPage(data, categoryId);
            }
        });
        adapter2.refreshData(mData.get(i).getSubCategories());
        holder.my_recycler_view.setAdapter(adapter2);
    }

    @Override public int getItemCount() {
        return mData.size();
    }

    public class GrocerySubCategoriesVHNew extends RecyclerView.ViewHolder {
        RecyclerView my_recycler_view;
        TextView category;

        public GrocerySubCategoriesVHNew(@NonNull View itemView, Activity contex) {
            super(itemView);
            my_recycler_view = itemView.findViewById(R.id.sub_cat_list);
            category = itemView.findViewById(R.id.category_name);
        }
    }
    private void goToDetailsPage(GrocerySubcategoryList data,
                                 String categoryId) {
        Intent intent =
                new Intent(contex, PatanjaliProduct_DetailsActivity.class);
        intent.putExtra("app_tittle", "Grocery World");
        intent.putExtra("category_id", categoryId);
        intent.putExtra("subcategory_id", data.getId());
        intent.putParcelableArrayListExtra("child_subcat",
                (ArrayList<? extends Parcelable>) data.getChildSubcat());
        contex.startActivity(intent);
        contex.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
