package com.food.order.speedzy.api.response.home;

import java.io.Serializable;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class AppclosestatusItem implements Serializable {

	@SerializedName("closing_banner")
	private String closingBanner;

	@SerializedName("close_title")
	private String closeTitle;

	@SerializedName("id")
	private String id;

	@SerializedName("close_type")
	private String closeType;

	@SerializedName("close_message")
	private String closeMessage;

	@SerializedName("closing_icon")
	private String closingIcon;

	@SerializedName("city_id")
	private String cityId;

	public void setClosingBanner(String closingBanner){
		this.closingBanner = closingBanner;
	}

	public String getClosingBanner(){
		return closingBanner;
	}

	public void setCloseTitle(String closeTitle){
		this.closeTitle = closeTitle;
	}

	public String getCloseTitle(){
		return closeTitle;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCloseType(String closeType){
		this.closeType = closeType;
	}

	public String getCloseType(){
		return closeType;
	}

	public void setCloseMessage(String closeMessage){
		this.closeMessage = closeMessage;
	}

	public String getCloseMessage(){
		return closeMessage;
	}

	public void setClosingIcon(String closingIcon){
		this.closingIcon = closingIcon;
	}

	public String getClosingIcon(){
		return closingIcon;
	}

	public void setCityId(String cityId){
		this.cityId = cityId;
	}

	public String getCityId(){
		return cityId;
	}

	@Override
 	public String toString(){
		return 
			"AppclosestatusItem{" + 
			"closing_banner = '" + closingBanner + '\'' + 
			",close_title = '" + closeTitle + '\'' + 
			",id = '" + id + '\'' + 
			",close_type = '" + closeType + '\'' + 
			",close_message = '" + closeMessage + '\'' + 
			",closing_icon = '" + closingIcon + '\'' + 
			",city_id = '" + cityId + '\'' + 
			"}";
		}
}