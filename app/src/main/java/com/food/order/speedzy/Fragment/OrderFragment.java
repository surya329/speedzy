package com.food.order.speedzy.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.food.order.speedzy.Adapter.MyOrdersAdapter;
import com.food.order.speedzy.Model.Patanjali_order_deatail_model;
import com.food.order.speedzy.Model.ViewOrderModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.RoomBooking.HotelInnListModelClass;
import com.food.order.speedzy.RoomBooking.UpcomingRecycleAdapter;
import com.food.order.speedzy.SpeedzyRide.RideRecycleAdapter;
import com.food.order.speedzy.Utils.EndlessRecyclerViewScrollListener;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.api.response.OrderItem;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderFragment extends Fragment {
  RecyclerView recyclerView;
  LinearLayoutManager lm;
  MyOrdersAdapter ordersAdapter;
  RelativeLayout noorder;
  LoaderDiloag loaderDiloag;
  MySharedPrefrencesData mySharedPrefrencesData;
  SwipeRefreshLayout simpleSwipeRefreshLayout;
  Context context;
  String userid, address_id;
  ViewOrderModel viewordermodel = new ViewOrderModel();
  Patanjali_order_deatail_model patanjali_order_deatail_model = new Patanjali_order_deatail_model();
  List<OrderItem> food_orderlist = new ArrayList<>();
  List<Patanjali_order_deatail_model.Grocery> Grocerylist = new ArrayList<>();
  int pos = 0;
  boolean load = false;
  int page = 1;
  boolean apiCallBlocler = false;
  private EndlessRecyclerViewScrollListener scrollListener;

  private ArrayList<HotelInnListModelClass> room_booking_list=new ArrayList<>();
  private UpcomingRecycleAdapter roomBookingAdapter;
  String txt[]={"Hotel Sunrise","Hotel Welcome"};
  private ArrayList<HotelInnListModelClass> ride_list=new ArrayList<>();
  private RideRecycleAdapter rideRecycleAdapter;
  String txt1[]={"Tue, jan 14, 03:39PM","Wed, Feb 08, 09:39AM","Fri, Jun 11, 10:39AM"};
  @SuppressLint("ValidFragment")
  @Override
  public void setUserVisibleHint(boolean isFragmentVisible_) {
    super.setUserVisibleHint(isFragmentVisible_);
    if (isFragmentVisible_ && load) {
      page = 1;
      callapimethod(2);
    }
  }

  public OrderFragment() {

  }

  public static OrderFragment newInstance(int pos) {
    OrderFragment myFragment = new OrderFragment();

    Bundle bundle = new Bundle();
    bundle.putInt("pos", pos);
    myFragment.setArguments(bundle);
    return myFragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.activity_my_order_new, container, false);

    context = getActivity();
    if (getArguments() != null) {
      pos = getArguments().getInt("pos", pos);
    }

    mySharedPrefrencesData = new MySharedPrefrencesData();
    userid = mySharedPrefrencesData.getUser_Id(context);
    address_id = mySharedPrefrencesData.getSelectAddress_ID(context);

    simpleSwipeRefreshLayout =
        (SwipeRefreshLayout) view.findViewById(R.id.simpleSwipeRefreshLayout);
    simpleSwipeRefreshLayout.setColorSchemeResources(R.color.red);
    loaderDiloag = new LoaderDiloag(context);
    lm = new LinearLayoutManager(context);
    noorder = (RelativeLayout) view.findViewById(R.id.noorder);

    recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_order);
    recyclerView.setLayoutManager(lm);
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    recyclerView.setHasFixedSize(true);
    simpleSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        simpleSwipeRefreshLayout.setRefreshing(true);
        page = 1;
        callapimethod(2);
        if (pos == 0 || pos == 4) {
          if (!food_orderlist.isEmpty()) {
            ordersAdapter.notifyDataSetChanged();
          }
        }
        if (pos == 1 || pos == 2 || pos == 3) {
          if (!Grocerylist.isEmpty()) {
            ordersAdapter.notifyDataSetChanged();
          }
        }
        if (pos == 5) {
          if (!room_booking_list.isEmpty()) {
            roomBookingAdapter.notifyDataSetChanged();
          }
        }
        if (pos == 6) {
          if (!ride_list.isEmpty()) {
            rideRecycleAdapter.notifyDataSetChanged();
          }
        }
      }
    });

    callapimethod(1);
    scrollListener = new EndlessRecyclerViewScrollListener(lm) {
      @Override
      public void onLoadMore(int page, int totalItemsCount) {
        if (apiCallBlocler) {
          apiCallBlocler = false;
          if (page > 1) {
            callapimethod(2);
          }
        }
      }
    };
    recyclerView.addOnScrollListener(scrollListener);

    return view;
  }

  private void callapimethod(int i) {
    if (pos == 0 || pos == 4) {
      food_orderlist.clear();
      callapiorder(i);
    }
    if (pos == 1 || pos == 2 || pos == 3) {
      Grocerylist.clear();
      callapiorder(i);
    }
    if (pos == 5) {
      room_booking_list.clear();
      for (int j = 0; j < txt.length; j++) {
        HotelInnListModelClass beanClassForRecyclerView_contacts = new HotelInnListModelClass(txt[j]);
        room_booking_list.add(beanClassForRecyclerView_contacts);
      }
      roomBookingAdapter = new UpcomingRecycleAdapter(getActivity(),room_booking_list);
      recyclerView.setAdapter(roomBookingAdapter);
      recyclerView.setVisibility(View.VISIBLE);
      noorder.setVisibility(View.GONE);
      recyclerView.setBackgroundColor(getResources().getColor(R.color.light_gray));
      simpleSwipeRefreshLayout.setRefreshing(false);
    }
    if (pos == 6) {
      ride_list.clear();
      for (int j = 0; j < txt1.length; j++) {
        HotelInnListModelClass beanClassForRecyclerView_contacts = new HotelInnListModelClass(txt1[j]);
        ride_list.add(beanClassForRecyclerView_contacts);
      }
      rideRecycleAdapter = new RideRecycleAdapter(getActivity(),ride_list);
      recyclerView.setAdapter(rideRecycleAdapter);
      recyclerView.setVisibility(View.VISIBLE);
      noorder.setVisibility(View.GONE);
      recyclerView.setBackgroundColor(getResources().getColor(R.color.light_gray));
    }
    simpleSwipeRefreshLayout.setRefreshing(false);
  }

  private void callapiorder(int i) {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(getActivity()).create(Apimethods.class);
    if (pos == 0 || pos == 4) {
      Call<ViewOrderModel> call = methods.getvieworders(userid, String.valueOf(page));
      Log.d("url", "url=" + call.request().url().toString());
      if (i == 1) {
        loaderDiloag.displayDiloag();
      }
      call.enqueue(new Callback<ViewOrderModel>() {
        @Override
        public void onResponse(@NonNull Call<ViewOrderModel> call,
            @NonNull Response<ViewOrderModel> response) {
          int statusCode = response.code();
          apiCallBlocler = true;
          viewordermodel = response.body();
          food_orderlist.clear();
          simpleSwipeRefreshLayout.setRefreshing(false);
          if (i == 1) {
            loaderDiloag.dismissDiloag();
            load = true;
          }
          if (viewordermodel != null
              && !GeneralUtil.isStringEmpty(viewordermodel.getStatus())
              && viewordermodel.getStatus().equalsIgnoreCase("Success")) {
            page++;
            food_orderlist.addAll(viewordermodel.getOrder());
            if (food_orderlist.size() > 0) {
              recyclerView.setVisibility(View.VISIBLE);
              noorder.setVisibility(View.GONE);
              ordersAdapter =
                  new MyOrdersAdapter(getActivity(), food_orderlist, viewordermodel.getStatus(),
                      1);
              recyclerView.setAdapter(ordersAdapter);
              ordersAdapter.notifyDataSetChanged();
            } else {
              recyclerView.setVisibility(View.GONE);
              noorder.setVisibility(View.VISIBLE);
            }
          } else {
            recyclerView.setVisibility(View.GONE);
            noorder.setVisibility(View.VISIBLE);
          }
        }

        @Override
        public void onFailure(Call<ViewOrderModel> call, Throwable t) {
          Toast.makeText(getActivity().getApplicationContext(),
              "internet not available..connect internet",
              Toast.LENGTH_LONG).show();
          loaderDiloag.dismissDiloag();
        }
      });
    } else {
      Call<Patanjali_order_deatail_model> call =
          methods.getpatanjaliorder_details(userid, address_id);
      Log.d("url", "url=" + call.request().url().toString());
      if (i == 1) {
        loaderDiloag.displayDiloag();
      }
      call.enqueue(new Callback<Patanjali_order_deatail_model>() {
        @Override
        public void onResponse(Call<Patanjali_order_deatail_model> call,
            Response<Patanjali_order_deatail_model> response) {
          int statusCode = response.code();
          patanjali_order_deatail_model = response.body();
          simpleSwipeRefreshLayout.setRefreshing(false);
          if (i == 1) {
            loaderDiloag.dismissDiloag();
            load = true;
          }
          Grocerylist.clear();
          if (patanjali_order_deatail_model.getStatus().equalsIgnoreCase("Success")) {
            for (int i = 0; i < patanjali_order_deatail_model.getGrocery().size(); i++) {
                            if (pos==1 && patanjali_order_deatail_model.getGrocery().get(i).getOrderName().equalsIgnoreCase(SpeedzyConstants.GROCERY_ORDER)) {
                                Grocerylist.add(patanjali_order_deatail_model.getGrocery().get(i));
                            }else if (pos==2 && patanjali_order_deatail_model.getGrocery().get(i).getOrderNumber().equalsIgnoreCase(SpeedzyConstants.MEDICINE_ORDER)) {
                                Grocerylist.add(patanjali_order_deatail_model.getGrocery().get(i));
                            }
              //Grocerylist.add(patanjali_order_deatail_model.getGrocery().get(i));
            }
            ordersAdapter = new MyOrdersAdapter(getActivity(), Grocerylist,
                    patanjali_order_deatail_model.getStatus());
            recyclerView.setAdapter(ordersAdapter);
            ordersAdapter.notifyDataSetChanged();
            if (Grocerylist.isEmpty()) {
              recyclerView.setVisibility(View.GONE);
              noorder.setVisibility(View.VISIBLE);
            } else {
              recyclerView.setVisibility(View.VISIBLE);
              noorder.setVisibility(View.GONE);
            }
          }
        }

        @Override
        public void onFailure(Call<Patanjali_order_deatail_model> call, Throwable t) {
          Toast.makeText(getActivity().getApplicationContext(),
              "internet not available..connect internet",
              Toast.LENGTH_LONG).show();
          loaderDiloag.dismissDiloag();
        }
      });
    }
  }
}
