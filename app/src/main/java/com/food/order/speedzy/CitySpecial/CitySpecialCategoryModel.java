package com.food.order.speedzy.CitySpecial;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CitySpecialCategoryModel implements Serializable {
    @SerializedName("status")
    @Expose
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;

    public class Category implements Serializable {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("product_count")
        @Expose
        private String productCount;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProductCount() {
            return productCount;
        }

        public void setProductCount(String productCount) {
            this.productCount = productCount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getParent() {
            return parent;
        }

        public void setParent(String parent) {
            this.parent = parent;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public List<SubCategory> getSubCategories() {
            return subCategories;
        }

        public void setSubCategories(List<SubCategory> subCategories) {
            this.subCategories = subCategories;
        }

        @SerializedName("parent")
        @Expose
        private String parent;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("sub_categories")
        @Expose
        private List<SubCategory> subCategories = null;

        public class SubCategory implements Serializable {
            @SerializedName("id")
            @Expose
            private String id;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getProductCount() {
                return productCount;
            }

            public void setProductCount(String productCount) {
                this.productCount = productCount;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getParent() {
                return parent;
            }

            public void setParent(String parent) {
                this.parent = parent;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public List<Object> getChildSubcat() {
                return childSubcat;
            }

            public void setChildSubcat(List<Object> childSubcat) {
                this.childSubcat = childSubcat;
            }

            @SerializedName("product_count")
            @Expose
            private String productCount;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("parent")
            @Expose
            private String parent;
            @SerializedName("image")
            @Expose
            private String image;
            @SerializedName("child_subcat")
            @Expose
            private List<Object> childSubcat = null;
        }
    }
}
