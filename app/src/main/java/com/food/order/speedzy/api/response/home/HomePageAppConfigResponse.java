package com.food.order.speedzy.api.response.home;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class HomePageAppConfigResponse implements Serializable {

	@SerializedName("open_close_status")
	private OpenCloseStatus openCloseStatus;

	@SerializedName("update-required")
	private int updateRequired;

	@SerializedName("endpoints")
	private List<String> endpoints;

	@SerializedName("Message")
	private String message;

	@SerializedName("rest_details")
	private List<Object> restDetails;

	@SerializedName("alltimings")
	private Alltimings alltimings;

	@SerializedName("image_baseURL")
	private String imageBaseURL;

	@SerializedName("app_close_type")
	private String appCloseType;

	@SerializedName("offer_details")
	private List<Object> offerDetails;

	@SerializedName("sections")
	private List<SectionsItem> sections;

	@SerializedName("app-version")
	private String appVersion;

	@SerializedName("baseURL")
	private String baseURL;

	@SerializedName("app_openstatus")
	private String appOpenstatus;

	@SerializedName("name")
	private String name;

	@SerializedName("type_details")
	private TypeDetails typeDetails;

	@SerializedName("appclosestatus")
	private List<AppclosestatusItem> appclosestatus;

	@SerializedName("status")
	private String status;

	@SerializedName("map_api_key")
	private String mapApiKey;

	public String getMapApiKey() {
		return mapApiKey;
	}

	public void setOpenCloseStatus(OpenCloseStatus openCloseStatus){
		this.openCloseStatus = openCloseStatus;
	}

	public OpenCloseStatus getOpenCloseStatus(){
		return openCloseStatus;
	}

	public void setUpdateRequired(int updateRequired){
		this.updateRequired = updateRequired;
	}

	public int getUpdateRequired(){
		return updateRequired;
	}

	public void setEndpoints(List<String> endpoints){
		this.endpoints = endpoints;
	}

	public List<String> getEndpoints(){
		return endpoints;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setRestDetails(List<Object> restDetails){
		this.restDetails = restDetails;
	}

	public List<Object> getRestDetails(){
		return restDetails;
	}

	public void setAlltimings(Alltimings alltimings){
		this.alltimings = alltimings;
	}

	public Alltimings getAlltimings(){
		return alltimings;
	}

	public void setImageBaseURL(String imageBaseURL){
		this.imageBaseURL = imageBaseURL;
	}

	public String getImageBaseURL(){
		return imageBaseURL;
	}

	public void setAppCloseType(String appCloseType){
		this.appCloseType = appCloseType;
	}

	public String getAppCloseType(){
		return appCloseType;
	}

	public void setOfferDetails(List<Object> offerDetails){
		this.offerDetails = offerDetails;
	}

	public List<Object> getOfferDetails(){
		return offerDetails;
	}

	public void setSections(List<SectionsItem> sections){
		this.sections = sections;
	}

	public List<SectionsItem> getSections(){
		return sections;
	}

	public void setAppVersion(String appVersion){
		this.appVersion = appVersion;
	}

	public String getAppVersion(){
		return appVersion;
	}

	public void setBaseURL(String baseURL){
		this.baseURL = baseURL;
	}

	public String getBaseURL(){
		return baseURL;
	}

	public void setAppOpenstatus(String appOpenstatus){
		this.appOpenstatus = appOpenstatus;
	}

	public String getAppOpenstatus(){
		return appOpenstatus;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setTypeDetails(TypeDetails typeDetails){
		this.typeDetails = typeDetails;
	}

	public TypeDetails getTypeDetails(){
		return typeDetails;
	}

	public void setAppclosestatus(List<AppclosestatusItem> appclosestatus){
		this.appclosestatus = appclosestatus;
	}

	public List<AppclosestatusItem> getAppclosestatus(){
		return appclosestatus;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OpenCloseStatusResponse{" + 
			"open_close_status = '" + openCloseStatus + '\'' + 
			",update-required = '" + updateRequired + '\'' + 
			",endpoints = '" + endpoints + '\'' + 
			",message = '" + message + '\'' + 
			",rest_details = '" + restDetails + '\'' + 
			",alltimings = '" + alltimings + '\'' + 
			",image_baseURL = '" + imageBaseURL + '\'' + 
			",app_close_type = '" + appCloseType + '\'' + 
			",offer_details = '" + offerDetails + '\'' + 
			",sections = '" + sections + '\'' + 
			",app-version = '" + appVersion + '\'' + 
			",baseURL = '" + baseURL + '\'' + 
			",app_openstatus = '" + appOpenstatus + '\'' + 
			",name = '" + name + '\'' + 
			",type_details = '" + typeDetails + '\'' + 
			",appclosestatus = '" + appclosestatus + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}