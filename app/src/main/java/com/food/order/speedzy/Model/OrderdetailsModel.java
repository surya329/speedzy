package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderdetailsModel implements Serializable {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("order")
    @Expose
    public List<Order> order = null;

    public List<Deliveryarea> getDeliveryarea() {
        return deliveryarea;
    }

    public void setDeliveryarea(List<Deliveryarea> deliveryarea) {
        this.deliveryarea = deliveryarea;
    }

    public List<AlldeliveryBoy> getAlldeliveryBoy() {
        return alldeliveryBoy;
    }

    public void setAlldeliveryBoy(List<AlldeliveryBoy> alldeliveryBoy) {
        this.alldeliveryBoy = alldeliveryBoy;
    }

    @SerializedName("deliveryarea")
    @Expose
    public List<Deliveryarea> deliveryarea = null;
    @SerializedName("alldeliveryBoy")
    @Expose
    public List<AlldeliveryBoy> alldeliveryBoy = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Order> getOrder() {
        return order;
    }

    public void setOrder(List<Order> order) {
        this.order = order;
    }

    public class Order implements Serializable {
        public String getInResOwnerId() {
            return inResOwnerId;
        }

        public void setInResOwnerId(String inResOwnerId) {
            this.inResOwnerId = inResOwnerId;
        }

        public String getRestaurantName() {
            return restaurantName;
        }

        public void setRestaurantName(String restaurantName) {
            this.restaurantName = restaurantName;
        }

        public String getResAddress() {
            return resAddress;
        }

        public void setResAddress(String resAddress) {
            this.resAddress = resAddress;
        }

        public String getResMobile() {
            return resMobile;
        }

        public void setResMobile(String resMobile) {
            this.resMobile = resMobile;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getRestaurantId() {
            return restaurantId;
        }

        public void setRestaurantId(String restaurantId) {
            this.restaurantId = restaurantId;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getResLatitude() {
            return resLatitude;
        }

        public void setResLatitude(String resLatitude) {
            this.resLatitude = resLatitude;
        }

        public String getResLongitude() {
            return resLongitude;
        }

        public void setResLongitude(String resLongitude) {
            this.resLongitude = resLongitude;
        }

        public String getUserLatitude() {
            return userLatitude;
        }

        public void setUserLatitude(String userLatitude) {
            this.userLatitude = userLatitude;
        }

        public String getUserLongitude() {
            return userLongitude;
        }

        public void setUserLongitude(String userLongitude) {
            this.userLongitude = userLongitude;
        }

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public List<OrderDetail> getOrderDetail() {
            return orderDetail;
        }

        public void setOrderDetail(List<OrderDetail> orderDetail) {
            this.orderDetail = orderDetail;
        }

        public String getSuburbId() {
            return suburbId;
        }

        public void setSuburbId(String suburbId) {
            this.suburbId = suburbId;
        }

        public String getOrderAmount() {
            return orderAmount;
        }

        public void setOrderAmount(String orderAmount) {
            this.orderAmount = orderAmount;
        }

        public String getDeliveryCharge() {
            return deliveryCharge;
        }

        public void setDeliveryCharge(String deliveryCharge) {
            this.deliveryCharge = deliveryCharge;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getCompanyname() {
            return companyname;
        }

        public void setCompanyname(String companyname) {
            this.companyname = companyname;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStreetnumber() {
            return streetnumber;
        }

        public void setStreetnumber(String streetnumber) {
            this.streetnumber = streetnumber;
        }

        public String getNearestCrossCity() {
            return nearestCrossCity;
        }

        public void setNearestCrossCity(String nearestCrossCity) {
            this.nearestCrossCity = nearestCrossCity;
        }

        public String getWallet() {
            return wallet;
        }

        public void setWallet(String wallet) {
            this.wallet = wallet;
        }

        public Object getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(Object transactionId) {
            this.transactionId = transactionId;
        }

        public Object getMethodOfOrdering() {
            return methodOfOrdering;
        }

        public void setMethodOfOrdering(Object methodOfOrdering) {
            this.methodOfOrdering = methodOfOrdering;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public String getFlgOrderType() {
            return flgOrderType;
        }

        public void setFlgOrderType(String flgOrderType) {
            this.flgOrderType = flgOrderType;
        }

        public String getFlgOrderStatus() {
            return flgOrderStatus;
        }

        public void setFlgOrderStatus(String flgOrderStatus) {
            this.flgOrderStatus = flgOrderStatus;
        }

        public String getOrderStatusMessage() {
            return orderStatusMessage;
        }

        public void setOrderStatusMessage(String orderStatusMessage) {
            this.orderStatusMessage = orderStatusMessage;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getPreOrderDate() {
            return preOrderDate;
        }

        public void setPreOrderDate(String preOrderDate) {
            this.preOrderDate = preOrderDate;
        }

        public String getDicountVal() {
            return dicountVal;
        }

        public void setDicountVal(String dicountVal) {
            this.dicountVal = dicountVal;
        }

        public String getDeliveryboyname() {
            return deliveryboyname;
        }

        public void setDeliveryboyname(String deliveryboyname) {
            this.deliveryboyname = deliveryboyname;
        }

        public String getDeliveryboyno() {
            return deliveryboyno;
        }

        public void setDeliveryboyno(String deliveryboyno) {
            this.deliveryboyno = deliveryboyno;
        }

        @SerializedName("in_res_owner_id")
        @Expose
        public String inResOwnerId;
        @SerializedName("restaurant_name")
        @Expose
        public String restaurantName;
        @SerializedName("res_address")
        @Expose
        public String resAddress;
        @SerializedName("res_mobile")
        @Expose
        public String resMobile;
        @SerializedName("order_no")
        @Expose
        public String orderNo;
        @SerializedName("restaurant_id")
        @Expose
        public String restaurantId;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("res_latitude")
        @Expose
        public String resLatitude;
        @SerializedName("res_longitude")
        @Expose
        public String resLongitude;
        @SerializedName("user_latitude")
        @Expose
        public String userLatitude;
        @SerializedName("user_longitude")
        @Expose
        public String userLongitude;
        @SerializedName("orderid")
        @Expose
        public String orderid;
        @SerializedName("order_detail")
        @Expose
        public List<OrderDetail> orderDetail = null;
        @SerializedName("suburb_id")
        @Expose
        public String suburbId;
        @SerializedName("order_amount")
        @Expose
        public String orderAmount;
        @SerializedName("delivery_charge")
        @Expose
        public String deliveryCharge;
        @SerializedName("total_amount")
        @Expose
        public String totalAmount;
        @SerializedName("fname")
        @Expose
        public String fname;
        @SerializedName("lname")
        @Expose
        public String lname;
        @SerializedName("companyname")
        @Expose
        public String companyname;
        @SerializedName("mobile")
        @Expose
        public String mobile;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("streetnumber")
        @Expose
        public String streetnumber;
        @SerializedName("nearest_cross_city")
        @Expose
        public String nearestCrossCity;
        @SerializedName("wallet")
        @Expose
        public String wallet;
        @SerializedName("transaction_id")
        @Expose
        public Object transactionId;
        @SerializedName("method_of_ordering")
        @Expose
        public Object methodOfOrdering;
        @SerializedName("payment_status")
        @Expose
        public String paymentStatus;
        @SerializedName("flg_order_type")
        @Expose
        public String flgOrderType;
        @SerializedName("flg_order_status")
        @Expose
        public String flgOrderStatus;

        public String getOrder_status_title() {
            return order_status_title;
        }

        public void setOrder_status_title(String order_status_title) {
            this.order_status_title = order_status_title;
        }

        @SerializedName("order_status_title")
        @Expose
        public String order_status_title;
        @SerializedName("order_status_message")
        @Expose
        public String orderStatusMessage;
        @SerializedName("payment_method")
        @Expose
        public String paymentMethod;
        @SerializedName("pre_order_date")
        @Expose
        public String preOrderDate;
        @SerializedName("dicount_val")
        @Expose
        public String dicountVal;
        @SerializedName("Deliveryboyname")
        @Expose
        public String deliveryboyname;
        @SerializedName("Deliveryboyno")
        @Expose
        public String deliveryboyno;

        public class OrderDetail implements Serializable
        {

            @SerializedName("dishid")
            @Expose
            public String dishid;
            @SerializedName("qty")
            @Expose
            public String qty;
            @SerializedName("price")
            @Expose
            public String price;
            @SerializedName("dishname")
            @Expose
            public String dishname;
            @SerializedName("subtotal")
            @Expose
            public String subtotal;

            public String getDishid() {
                return dishid;
            }

            public void setDishid(String dishid) {
                this.dishid = dishid;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDishname() {
                return dishname;
            }

            public void setDishname(String dishname) {
                this.dishname = dishname;
            }

            public String getSubtotal() {
                return subtotal;
            }

            public void setSubtotal(String subtotal) {
                this.subtotal = subtotal;
            }

            public Options getOptions() {
                return options;
            }

            public void setOptions(Options options) {
                this.options = options;
            }

            @SerializedName("options")
            @Expose
            public Options options;

            public class Options implements Serializable
            {

                @SerializedName("Unit")
                @Expose
                public Object unit;

                public Object getUnit() {
                    return unit;
                }

                public void setUnit(Object unit) {
                    this.unit = unit;
                }

                public String getActualPrice() {
                    return actualPrice;
                }

                public void setActualPrice(String actualPrice) {
                    this.actualPrice = actualPrice;
                }

                public Object getExtraItem() {
                    return extraItem;
                }

                public void setExtraItem(Object extraItem) {
                    this.extraItem = extraItem;
                }

                public String getExtraPrice() {
                    return extraPrice;
                }

                public void setExtraPrice(String extraPrice) {
                    this.extraPrice = extraPrice;
                }

                public Object getType() {
                    return type;
                }

                public void setType(Object type) {
                    this.type = type;
                }

                @SerializedName("Actual Price")
                @Expose
                public String actualPrice;
                @SerializedName("Extra Item")
                @Expose
                public Object extraItem;
                @SerializedName("Extra Price")
                @Expose
                public String extraPrice;
                @SerializedName("Type")
                @Expose
                public Object type;

            }

        }
    }
    public class AlldeliveryBoy implements Serializable{

        @SerializedName("DeliveryboyID")
        @Expose
        public String deliveryboyID;
        @SerializedName("st_first_name")
        @Expose
        public String stFirstName;

        public String getDeliveryboyID() {
            return deliveryboyID;
        }

        public void setDeliveryboyID(String deliveryboyID) {
            this.deliveryboyID = deliveryboyID;
        }

        public String getStFirstName() {
            return stFirstName;
        }

        public void setStFirstName(String stFirstName) {
            this.stFirstName = stFirstName;
        }

        public Object getStLastName() {
            return stLastName;
        }

        public void setStLastName(Object stLastName) {
            this.stLastName = stLastName;
        }

        @SerializedName("st_last_name")
        @Expose
        public Object stLastName;

    }
    public class Deliveryarea implements Serializable{

        public String getInSuburbId() {
            return inSuburbId;
        }

        public void setInSuburbId(String inSuburbId) {
            this.inSuburbId = inSuburbId;
        }

        public String getPostcodeId() {
            return postcodeId;
        }

        public void setPostcodeId(String postcodeId) {
            this.postcodeId = postcodeId;
        }

        public String getStSuburb() {
            return stSuburb;
        }

        public void setStSuburb(String stSuburb) {
            this.stSuburb = stSuburb;
        }

        public String getStPostcode() {
            return stPostcode;
        }

        public void setStPostcode(String stPostcode) {
            this.stPostcode = stPostcode;
        }

        public String getStCity() {
            return stCity;
        }

        public void setStCity(String stCity) {
            this.stCity = stCity;
        }

        public String getStDeliveryCharge() {
            return stDeliveryCharge;
        }

        public void setStDeliveryCharge(String stDeliveryCharge) {
            this.stDeliveryCharge = stDeliveryCharge;
        }

        public String getFlgDeliveryStatus() {
            return flgDeliveryStatus;
        }

        public void setFlgDeliveryStatus(String flgDeliveryStatus) {
            this.flgDeliveryStatus = flgDeliveryStatus;
        }

        public String getFlgMenuChoice() {
            return flgMenuChoice;
        }

        public void setFlgMenuChoice(String flgMenuChoice) {
            this.flgMenuChoice = flgMenuChoice;
        }

        @SerializedName("in_suburb_id")
        @Expose
        public String inSuburbId;
        @SerializedName("postcode_id")
        @Expose
        public String postcodeId;
        @SerializedName("st_suburb")
        @Expose
        public String stSuburb;
        @SerializedName("st_postcode")
        @Expose
        public String stPostcode;
        @SerializedName("st_city")
        @Expose
        public String stCity;
        @SerializedName("st_delivery_charge")
        @Expose
        public String stDeliveryCharge;
        @SerializedName("flg_delivery_status")
        @Expose
        public String flgDeliveryStatus;
        @SerializedName("flg_menu_choice")
        @Expose
        public String flgMenuChoice;
    }
}
