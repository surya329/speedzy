package com.food.order.speedzy.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Activity.OrderDetails_New;
import com.food.order.speedzy.Model.Patanjali_order_deatail_model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.api.response.OrderItem;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {
  Activity context;
  List<OrderItem> orderlist;
  String status;
  public static int sCorner = 15;
  public static int sMargin = 8;
  int flag = 0;
  List<Patanjali_order_deatail_model.Grocery> Grocerylist;

  public MyOrdersAdapter(Activity context, List<OrderItem> orderlist, String status,
      int flag) {
    this.context = context;
    this.orderlist = orderlist;
    this.status = status;
    this.flag = flag;
  }

  public MyOrdersAdapter(Activity context,
      List<Patanjali_order_deatail_model.Grocery> Grocerylist, String status) {
    this.context = context;
    this.Grocerylist = Grocerylist;
    this.status = status;
  }

  @NonNull @Override
  public MyOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.ordersadapter_layout_new, parent, false);
    System.out.println("View Holder");
    // set the view's size, margins, paddings and layout parameters
    MyOrdersAdapter.ViewHolder vh = new MyOrdersAdapter.ViewHolder(v);
    return vh;
  }

  @Override
  public void onBindViewHolder(@NonNull final MyOrdersAdapter.ViewHolder holder,
      @SuppressLint("RecyclerView") final int position) {
    if (flag == 1) {
      final OrderItem orderin = orderlist.get(position);
      holder.sectionTitle.setText("Order ID : " + orderin.getOrderid());
      String imageurl = orderin.getImage();
      //     imageurl=imageurl.replace(" ","%20");
      //     imageurl=imageurl.replace("\\\\","/");
      imageurl = imageurl.replace("\\", "/");
      orderin.setImage(imageurl);
     /* Glide.with(context)
          .load(R.drawable.app_logo)
          .asBitmap()
          .centerCrop()
          .into(new BitmapImageViewTarget(holder.itemImage) {
            @Override
            protected void setResource(Bitmap resource) {
              RoundedBitmapDrawable circularBitmapDrawable =
                  RoundedBitmapDrawableFactory.create(context.getResources(), resource);
              circularBitmapDrawable.setCircular(true);
              holder.itemImage.setImageDrawable(circularBitmapDrawable);
            }
          });*/
      String base_url = "https://storage.googleapis.com/speedzy_data/resources/restaurant/";
      String base_url_logo =
          "https://storage.googleapis.com/speedzy_data/resources/restaurantlogo/";

      RequestBuilder<Drawable> thumbnailRequest = Glide
          .with(context)
          .load(base_url + "very_small/" + orderin.getImage());
      // .bitmapTransform(new RoundedCornersTransformation(context, sCorner, sMargin))
      Glide
          .with(context)
          .load(base_url_logo + "big/" + orderin.getImage())
          .thumbnail(thumbnailRequest)
          // .bitmapTransform(new RoundedCornersTransformation(context, sCorner, sMargin))
              .apply(new RequestOptions()
          .placeholder(R.drawable.grid_placeholder)
          .diskCacheStrategy(DiskCacheStrategy.ALL))
          .into(holder.itemImage);

      holder.itemTitle.setText(orderin.getRestaurantName());
      holder.address.setText(orderin.getAddress());
      if (orderin.getPaymentMethod().equalsIgnoreCase("2")) {
        holder.payment_type.setText("COD");
      } else if (orderin.getPaymentMethod().equalsIgnoreCase("1") || orderin.getPaymentMethod()
          .equalsIgnoreCase("3")) {
        holder.payment_type.setText("Card Payment");
      } else if (orderin.getPaymentMethod().equalsIgnoreCase("4")) {
        holder.payment_type.setText("UPI Payment");
      }
      holder.status.setText(orderin.getOrderRunningstatus().getStatusTitle());
      if (orderin.getFlgOrderStatus().equalsIgnoreCase("0")) {
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.GONE);
      } else if (orderin.getFlgOrderStatus().equalsIgnoreCase("1")) {
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.VISIBLE);
        holder.reorder.setText("Track");
      } else if (orderin.getFlgOrderStatus().equalsIgnoreCase("2")) {
        holder.status.setTextColor(context.getResources().getColor(R.color.red));
        holder.reorder.setVisibility(View.GONE);
      } else if (orderin.getFlgOrderStatus().equalsIgnoreCase("3")) {
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.VISIBLE);
        holder.reorder.setText("Reorder");
      } else if (orderin.getFlgOrderStatus().equalsIgnoreCase("4")) {
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.GONE);
      } else if (orderin.getFlgOrderStatus().equalsIgnoreCase("5")) {
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.VISIBLE);
      } else if (orderin.getFlgOrderStatus().equalsIgnoreCase("6")) {
        holder.status.setTextColor(context.getResources().getColor(R.color.red));
        holder.reorder.setVisibility(View.VISIBLE);
        holder.reorder.setVisibility(View.GONE);
      } else if (orderin.getFlgOrderStatus().equalsIgnoreCase("7")) {
        holder.status.setTextColor(context.getResources().getColor(R.color.red));
        holder.reorder.setVisibility(View.GONE);
      }

      //        0=>In process,
      //        1=>Accepted,
      //                2=>Rejected by Restaurant ,
      //                3=> Refund Completed ,
      //        4=>Moved list'
      //        5 => Delivery started ,
      //        6=> Delivery Ended,
      //        7=> user Cancelled

      holder.totalprice.setText("₹ " + orderin.getTotalAmount());
      String deliveryDate = getdate(orderin.getPreOrderDate());
      holder.order_time.setText(deliveryDate);
      holder.reorder.setVisibility(View.GONE);

        /*holder.reorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, CheckoutActivity_New.class));
            }
        });*/

    } else {
      final Patanjali_order_deatail_model.Grocery grocery = Grocerylist.get(position);
      holder.sectionTitle.setText("Order ID : " + grocery.getOrderNumber());

      if (grocery.getOrderName().equalsIgnoreCase(SpeedzyConstants.GROCERY_ORDER)) {
        Glide.with(context)
            .load(R.drawable.cart_icon)
                .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)))
                .into( holder.itemImage);
      } else {
        Glide.with(context)
            .load(R.drawable.med_icon)
                .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)))
                .into( holder.itemImage);
      }
      holder.itemTitle.setText(grocery.getOrderName());

      if (grocery.getOrderStatus().equalsIgnoreCase("1")) {
        holder.status.setText("Order Received");
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.GONE);
      } else if (grocery.getOrderStatus().equalsIgnoreCase("2")) {
        holder.status.setText("Order accepted");
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.GONE);
      } else if (grocery.getOrderStatus().equalsIgnoreCase("3")) {
        holder.status.setText("Rejected");
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.GONE);
      } else if (grocery.getOrderStatus().equalsIgnoreCase("4")) {
        holder.status.setText("Order Picked up");
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.GONE);
      } else if (grocery.getOrderStatus().equalsIgnoreCase("5")) {
        holder.status.setText("Delivery started");
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.GONE);
      } else if (grocery.getOrderStatus().equalsIgnoreCase("6")) {
        holder.status.setText("Delivery end");
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.GONE);
      } else if (grocery.getOrderStatus().equalsIgnoreCase("7")) {
        holder.status.setText("Item change in progress");
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.GONE);
      } else {
        holder.status.setText("Order Placed");
        holder.status.setTextColor(context.getResources().getColor(R.color.black_transparent));
        holder.reorder.setVisibility(View.VISIBLE);
      }
      holder.totalprice.setText("₹ " + grocery.getOrderAmount());
      String deliveryDate = getdate(grocery.getCreated());
      holder.order_time.setText(deliveryDate);
      holder.reorder.setVisibility(View.GONE);
    }

    holder.relative.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(context, OrderDetails_New.class);
        if (flag == 1) {
          intent.putExtra("orderdetails", orderlist.get(holder.getAdapterPosition()));
          intent.putExtra("flag", flag);
        } else {
          intent.putExtra("patanjalidetails", Grocerylist.get(holder.getAdapterPosition()));
          intent.putExtra("flag", 2);
        }
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      }
    });
  }

  private String getdate(String deliveryDate) {
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
    Date d = null;
    String changedDate = "";
    try {
      d = dateFormatter.parse(deliveryDate);
      SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy H:mm:ss");
      changedDate = dateFormat.format(d);
    } catch (ParseException e) {
      e.printStackTrace();
      changedDate = deliveryDate;
    }
    return changedDate;
  }

  @Override
  public int getItemCount() {
    if (flag == 1) {
      return orderlist.size();
    } else {
      return Grocerylist.size();
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView itemTitle, status, sectionTitle, totalprice, reorder, order_time, address,
        payment_type;
    ImageView itemImage;
    RelativeLayout relative;

    public ViewHolder(View itemView) {
      super(itemView);
      itemTitle = (TextView) itemView.findViewById(R.id.itemTitle);
      status = (TextView) itemView.findViewById(R.id.status);
      itemImage = (ImageView) itemView.findViewById(R.id.itemImage);
      sectionTitle = (TextView) itemView.findViewById(R.id.sectionTitle);
      totalprice = (TextView) itemView.findViewById(R.id.totalprice);
      reorder = (TextView) itemView.findViewById(R.id.reorder);
      order_time = (TextView) itemView.findViewById(R.id.order_time);
      relative = (RelativeLayout) itemView.findViewById(R.id.relative);
      address = itemView.findViewById(R.id.address);
      payment_type = itemView.findViewById(R.id.payment_type);
    }
  }
}