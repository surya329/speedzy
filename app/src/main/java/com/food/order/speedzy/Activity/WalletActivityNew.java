package com.food.order.speedzy.Activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.food.order.speedzy.Adapter.WalletAdapter_New;
import com.food.order.speedzy.Model.WalletModel;
import com.food.order.speedzy.Model.WalletNewModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.reyclerviewutils.RecyclerViewUtils;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.root.BaseActivity;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */

public class WalletActivityNew extends BaseActivity {
  private Toolbar toolbar;
  MySharedPrefrencesData mysharedpref;
  List<WalletModel> walletModelArrayList = new ArrayList<>();
  TextView walletamnt1;
  String userid, Veg_Flag;
  LinearLayout linear1, linear2;
  RecyclerView recyclerView;
  LinearLayoutManager lm;
  WalletAdapter_New walletAdapter;
  LoaderDiloag loaderDiloag;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_wallet_new);

    loaderDiloag = new LoaderDiloag(this);
    mysharedpref = new MySharedPrefrencesData();
    userid = mysharedpref.getUser_Id(this);
    Veg_Flag = mysharedpref.getVeg_Flag(this);

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    if (Veg_Flag.equalsIgnoreCase("1")) {
      statusbar_bg();
    } else {
      statusbar_bg();
    }

    walletamnt1 = (TextView) findViewById(R.id.walletamount1);
    linear1 = (LinearLayout) findViewById(R.id.linear1);
    linear2 = (LinearLayout) findViewById(R.id.linear2);

    lm = new LinearLayoutManager(this);
    recyclerView = (RecyclerView) findViewById(R.id.recyclerView_deals);
    recyclerView.setLayoutManager(lm);
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    recyclerView.addItemDecoration(
        RecyclerViewUtils.newVerticalSpacingItemDecoration(this, R.dimen.size_16, true, true));
    recyclerView.setHasFixedSize(true);
    recyclerView.addItemDecoration(
        RecyclerViewUtils.newDividerItemDecoration(this, R.color.light_gray));
    callapiwallet();
  }

  private void statusbar_bg() {
    Objects.requireNonNull(getSupportActionBar())
        .setBackgroundDrawable(new ColorDrawable(getResources()
            .getColor(R.color.red)));
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.red));
    }
  }

  private void callapiwallet() {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    Call<WalletNewModel> call = methods.getwalletdetails(userid);
    Log.d("url", "url=" + call.request().url().toString());

    call.enqueue(new Callback<WalletNewModel>() {
      @Override
      public void onResponse(@NonNull Call<WalletNewModel> call,
          @NonNull Response<WalletNewModel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        WalletNewModel walletNewModel = new WalletNewModel();
        walletNewModel = response.body();

        //walletamnt1.setText("₹ " + walletNewModel.getWallet());
        //walletModelArrayList = walletNewModel.getTransactions();
        if (walletNewModel.getTransactions() != null
            && walletNewModel.getTransactions().size() > 0) {
          linear1.setVisibility(View.GONE);
          linear2.setVisibility(View.VISIBLE);
          walletAdapter =
              new WalletAdapter_New(WalletActivityNew.this, walletNewModel.getTransactions());
          recyclerView.setAdapter(walletAdapter);
        } else {
          linear1.setVisibility(View.VISIBLE);
          linear2.setVisibility(View.GONE);
        }
      }

      @Override
      public void onFailure(@NonNull Call<WalletNewModel> call, @NonNull Throwable t) {
        Log.e("error message", t.getMessage());
        //Toast.makeText(getApplicationContext(),"internet not available..connect internet", Toast.LENGTH_LONG).show();
        showErrorDialog(t.getLocalizedMessage());
      }
    });
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case android.R.id.home:
        Commons.back_button_transition(WalletActivityNew.this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    Commons.back_button_transition(WalletActivityNew.this);
  }
}
