package com.food.order.speedzy.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.food.order.speedzy.Activity.CheckoutActivity_New;
import com.food.order.speedzy.Activity.RestarantDetails_New;
import com.food.order.speedzy.Adapter.MenuAdapter;
import com.food.order.speedzy.Adapter.RestaurantDetailsAdapter;
import com.food.order.speedzy.Adapter.RestaurantDetailsAdapter_New;
import com.food.order.speedzy.Model.Cart_Model;
import com.food.order.speedzy.Model.Dishdetail;
import com.food.order.speedzy.Model.Dishitem;
import com.food.order.speedzy.Model.Preferencemodel;
import com.food.order.speedzy.Model.Restaurant_Dish_Model;
import com.food.order.speedzy.Model.Restaurant_Dish_Model_New;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.AllValidation;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Sujata Mohanty.
 */

@SuppressLint("ValidFragment")
public class MenuFragment extends Fragment {
  RecyclerView recyclerView;
  RestaurantDetailsAdapter adapter;
  Switch veg_switch;
  TextView search_menu,menu,parent_dish_name;
  String userid;
  String pick_del;
  String delivery_fee;
  String res_id;
  String res_name;
  String res_address;
  String postcode;
  String suburb;
  String veg_Flag;
  MySharedPrefrencesData mySharedPrefrencesData;
  Restaurant_Dish_Model_New restmodel;
  //ArrayList<DevliverySuburb> devliverySuburbs;
  ArrayList<Restaurant_Dish_Model.NewOffers> offerArrayList;
  ArrayList<Dishitem> parentDishArrayList;
  ArrayList<Dishdetail> childDishArrayList;
  ArrayList<Dishitem> parentDishArrayListVeg;
  ArrayList<Dishdetail> childDishArrayListVeg;
  ArrayList<Cart_Model.Cart_Details> cd = new ArrayList<>();
  LOcaldbNew lOcaldbNew;
  int quantity;
  private static double totalsum = 0;
  TextView avadadesc;
  LinearLayout avadadesclay;
  String start_time_lunch;
  String end_time_lunch;
  String start_time_dinner;
  String end_time_dinner;
  String app_openstatus;
  static String Flg_ac_status;
  private String mMenuId = "";
  static RestarantDetails_New restarantDetails_new;
  static boolean additem;
  NestedScrollView nestedScrollView;
  public MenuFragment(RestarantDetails_New restarantDetails_new,
                      Restaurant_Dish_Model_New restmodel, String app_openstatus, String start_time_lunch,
                      String end_time_lunch, String start_time_dinner, String end_time_dinner, String res_id,
                      String res_name, String res_address, String suburb, String postcode, String menuId, boolean additem,
                      String Flg_ac_status,NestedScrollView nestedScrollView) {
    this.app_openstatus = app_openstatus;
    this.start_time_lunch = start_time_lunch;
    this.end_time_lunch = end_time_lunch;
    this.start_time_dinner = start_time_dinner;
    this.end_time_dinner = end_time_dinner;
    this.res_id = res_id;
    this.res_name = res_name;
    this.res_address = res_address;
    this.suburb = suburb;
    this.postcode = postcode;
    this.restmodel = restmodel;
    this.restarantDetails_new = restarantDetails_new;
    this.mMenuId = menuId;
    this.additem=additem;
    this.Flg_ac_status=Flg_ac_status;
    this.nestedScrollView=nestedScrollView;
  }

  public MenuFragment() {
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_menu, container, false);

    mySharedPrefrencesData = new MySharedPrefrencesData();
    if (!(mySharedPrefrencesData.getUser_Id(getActivity()) == null)) {

      userid = mySharedPrefrencesData.getUser_Id(getActivity());
    }
    veg_Flag = mySharedPrefrencesData.getVeg_Flag(getActivity());
    lOcaldbNew = new LOcaldbNew(getActivity());
    Commons.restaurant_id = res_id;
    // restapicall();
    recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_restaurant_details);
    avadadesc = (TextView) view.findViewById(R.id.avadadescription);
    avadadesclay = (LinearLayout) view.findViewById(R.id.avadadescriptionlay);
    veg_switch=view.findViewById(R.id.veg_switch);
    search_menu=view.findViewById(R.id.search_menu);
    menu=view.findViewById(R.id.menu);
      parent_dish_name=view.findViewById(R.id.parent_dish_name);
    parentDishArrayList = new ArrayList<>();
    parentDishArrayListVeg= new ArrayList<>();
    childDishArrayList = new ArrayList<>();
    childDishArrayListVeg = new ArrayList<>();
    if (mMenuId.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID) || Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)) {
      cd = lOcaldbNew.getFruitCart_Details();
      quantity = lOcaldbNew.getFruitQuantity(Commons.restaurant_id);
      setPriceDetails(cd, quantity);
    } else {
      cd = lOcaldbNew.getCart_Details();
      quantity = lOcaldbNew.getQuantity(Commons.restaurant_id);
      setPriceDetails(cd, quantity);
    }
    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setHasFixedSize(false);
    recyclerView.setNestedScrollingEnabled(false);
    setData();
    offerArrayList = (ArrayList<Restaurant_Dish_Model.NewOffers>) restmodel.getOffers();
   /* adapter =
        new RestaurantDetailsAdapter(parentDishArrayList, getActivity(), MenuFragment.this,
            res_id, res_name, res_address, suburb, postcode, pick_del,
            delivery_fee, *//*devliverySuburbs,*//* offerArrayList,
            restmodel.getAvg_order_value(),
            restmodel.getOrders_count(),*//*restmodel.getDeliveryAreaList(),*//*7, mMenuId,additem,Flg_ac_status, restmodel.getRestaurant_details().get(0).getRestcharge(),restmodel.getRestaurant_details().get(0).getRestaurant_logo_image());
    recyclerView.setAdapter(adapter);*/
   /* if (adapter.getGroups().size()<=3){
      adapter.onGroupClick(0);
    }*/

    veg_switch.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        final Switch btn = (Switch) v;
        if (!btn.isChecked()) {
          adapter.updateList(parentDishArrayList);
        }else {
          adapter.updateList(parentDishArrayListVeg);
        }
      }
    });
    search_menu.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        showDialogMenuList();
      }
    });
    menu.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
       popupmenu();
      }
    });
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
            if (firstVisibleItemPosition!=0) {
                parent_dish_name.setVisibility(View.VISIBLE);
            }else {
                parent_dish_name.setVisibility(View.GONE);
            }
            if (veg_switch.isChecked()){
                parent_dish_name.setText(parentDishArrayListVeg.get(firstVisibleItemPosition).getCuisineName());
            }else {
                parent_dish_name.setText(parentDishArrayList.get(firstVisibleItemPosition).getCuisineName());
            }
        }
    });
    return view;
  }

  private void popupmenu() {
    PopupWindow popup = new PopupWindow(getActivity());
    View layout = getLayoutInflater().inflate(R.layout.popup_list, null);
    popup.setContentView(layout);
    // Set content width and height
    popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
    popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
    // Closes the popup window when touch outside of it - when looses focus
    popup.setOutsideTouchable(true);
    popup.setFocusable(true);
    // Show anchored to button
    popup.setBackgroundDrawable(new BitmapDrawable());
    popup.showAsDropDown(menu);

    RecyclerView recyclerViewmenu=layout.findViewById(R.id.recyclerView);
    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
    recyclerViewmenu.setLayoutManager(layoutManager);
    recyclerViewmenu.setHasFixedSize(true);
    recyclerViewmenu.setNestedScrollingEnabled(false);
    MenuAdapter menuAdapter;
   /* if (veg_switch.isChecked()) {
      menuAdapter = new MenuAdapter(parentDishArrayListVeg, getActivity(),popup,recyclerView,nestedScrollingView);
    }else {
      menuAdapter = new MenuAdapter(parentDishArrayList, getActivity(),popup,recyclerView,nestedScrollingView);
    }
    recyclerViewmenu.setAdapter(menuAdapter);
    menuAdapter.notifyDataSetChanged();*/
  }

  private void setData() {
    int sizenew = restmodel.getOffers().size();

    if (sizenew > 0) {

      for (int k = 0; k < restmodel.getOffers().size(); k++) {
        childDishArrayList.add(new Dishdetail("",
            "",
            restmodel.getOffers().get(k).getOffer_description(),
            null,
            "",
            "",
            /*null,*/
            "",
            "",
            "",
            ""
        ));
        childDishArrayListVeg.add(new Dishdetail("",
                "",
                restmodel.getOffers().get(k).getOffer_description(),
                null,
                "",
                "",
                /*null,*/
                "",
                "",
                "",
                ""
        ));
      }
      parentDishArrayList.add(0, new Dishitem("Special offers", childDishArrayList));
      parentDishArrayListVeg.add(0, new Dishitem("Special offers", childDishArrayListVeg));
    }
    for (int i = 0; i < restmodel.getCuisines().size(); i++) {
      childDishArrayList = new ArrayList<>();
      childDishArrayListVeg = new ArrayList<>();
      if (!restmodel.getCuisines().get(i).getDishdetails().isEmpty()) {
        for (int j = 0; j < restmodel.getCuisines().get(i).getDishdetails().size(); j++) {

          childDishArrayList.add(
                  new Dishdetail(restmodel.getCuisines().get(i).getDishdetails().get(j).getInDishId(),
                          restmodel.getCuisines().get(i).getDishdetails().get(j).getStDishName(),
                          restmodel.getCuisines().get(i).getDishdetails().get(j).getDishDesription(),
                          restmodel.getCuisines().get(i).getDishdetails().get(j).getStPrice(),
                          restmodel.getCuisines().get(i).getDishdetails().get(j).getFlgAddChoices(),
                          restmodel.getCuisines().get(i).getDishdetails().get(j).getDishCuisineId(),
                          /*null,*/
                          restmodel.getCuisines().get(i).getDishdetails().get(j).getEgg_status(),
                          restmodel.getCuisines().get(i).getDishdetails().get(j).getNon_veg_status(),
                          restmodel.getCuisines().get(i).getDishdetails().get(j).getCake_flg(),
                          restmodel.getCuisines().get(i).getDishdetails().get(j).getMax_qty_peruser()
                  ));
          if (restmodel.getCuisines().get(i).getDishdetails().get(j).getNon_veg_status().equalsIgnoreCase("0")) {
            childDishArrayListVeg.add(
                    new Dishdetail(restmodel.getCuisines().get(i).getDishdetails().get(j).getInDishId(),
                            restmodel.getCuisines().get(i).getDishdetails().get(j).getStDishName(),
                            restmodel.getCuisines().get(i).getDishdetails().get(j).getDishDesription(),
                            restmodel.getCuisines().get(i).getDishdetails().get(j).getStPrice(),
                            restmodel.getCuisines().get(i).getDishdetails().get(j).getFlgAddChoices(),
                            restmodel.getCuisines().get(i).getDishdetails().get(j).getDishCuisineId(),
                            /*null,*/
                            restmodel.getCuisines().get(i).getDishdetails().get(j).getEgg_status(),
                            restmodel.getCuisines().get(i).getDishdetails().get(j).getNon_veg_status(),
                            restmodel.getCuisines().get(i).getDishdetails().get(j).getCake_flg(),
                            restmodel.getCuisines().get(i).getDishdetails().get(j).getMax_qty_peruser()
                    ));
          }
        }
      }else {
        childDishArrayList.add(new Dishdetail("",
                "",
                "No Items",
                null,
                "",
                "",
                /*null,*/
                "",
                "",
                "",
                ""
        ));
        childDishArrayListVeg.add(new Dishdetail("",
                "",
                "No Items",
                null,
                "",
                "",
                /*null,*/
                "",
                "",
                "",
                ""
        ));
      }

      parentDishArrayList.add(
          new Dishitem(restmodel.getCuisines().get(i).getCuisineName(), childDishArrayList));
      if (!childDishArrayListVeg.isEmpty()) {
        parentDishArrayListVeg.add(
                new Dishitem(restmodel.getCuisines().get(i).getCuisineName(), childDishArrayListVeg));
      }
    }
  }

  public static void setPriceDetails(ArrayList<Cart_Model.Cart_Details> listdata, int quantity) {
    int qty = 0;
    if (listdata != null) {
      totalsum = 0.00;
      qty = 0;
      for (int k = 0; k < listdata.size(); k++) {
        final Cart_Model.Cart_Details cart_details = listdata.get(k);
        qty = Integer.parseInt(cart_details.getQuantity());
        if (!GeneralUtil.isStringEmpty(cart_details.getMenu_price())) {
          double dishPrice = Double.parseDouble(cart_details.getMenu_price()) * qty;
          ArrayList<ArrayList<Preferencemodel>> myarray = cart_details.getPreferencelfromdb();
          if (myarray.size() > 1) {

            for (int i = 1; i < myarray.size(); i++) {
              ArrayList<Preferencemodel> mychildarray = cart_details.getPreferencelfromdb().get(i);
              for (int j = 0; j < mychildarray.size(); j++) {
                Preferencemodel prefData = mychildarray.get(j);
                if (prefData.getMenuprice().length() > 2) {
                  double itemPrice = Double.parseDouble(prefData.getMenuprice()) * qty;
                  dishPrice = dishPrice + itemPrice;
                } else {
                  double itemPrice = 0.00;
                  dishPrice = dishPrice + itemPrice;
                }
              }
            }
          }
          totalsum = totalsum + dishPrice;
        }
      }
    }

    if (quantity == 0) {
      try {
        restarantDetails_new.cartaddedlay.setVisibility(View.VISIBLE);
        restarantDetails_new.item.setText("0 Items  |  "+"₹" + String.format("%.2f", 0.00));
      } catch (Exception e) {
      }
    } else {
      try {
        restarantDetails_new.cartaddedlay.setVisibility(View.VISIBLE);
        restarantDetails_new.item.setText(String.valueOf(quantity)+" Items  |  "+"₹" + String.format("%.2f", totalsum));
      } catch (Exception e) {

      }
    }
    if (additem){
      restarantDetails_new.cartaddedlay.setVisibility(View.GONE);
      restarantDetails_new.add_item_layout.setVisibility(View.VISIBLE);
    }else {
      restarantDetails_new.cartaddedlay.setVisibility(View.VISIBLE);
      restarantDetails_new.add_item_layout.setVisibility(View.GONE);
      if (Flg_ac_status.equalsIgnoreCase("2")){
        restarantDetails_new.cartaddedlay.setVisibility(View.GONE);
        restarantDetails_new.add_item_layout.setVisibility(View.GONE);
      }
    }
  }

  private void showDialogMenuList() {
    ArrayList<Dishitem> parentDishArrayList_filter=new ArrayList<>();
    if (veg_switch.isChecked()){
      parentDishArrayList_filter=parentDishArrayListVeg;
    }else {
      parentDishArrayList_filter=parentDishArrayList;
    }
    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
  /*  DialogFragment newFragment = MyDialogFragment.newInstance(parentDishArrayList_filter, getActivity(), MenuFragment.this,
            res_id, res_name, res_address, suburb, postcode, pick_del,
            delivery_fee, offerArrayList,
            restmodel.getAvg_order_value(),
            restmodel.getOrders_count(),7, mMenuId,additem,Flg_ac_status, restmodel.getRestaurant_details().get(0).getRestcharge(),restmodel.getRestaurant_details().get(0).getRestaurant_logo_image());
    newFragment.show(ft, "dialog");*/
  }
}
