package com.food.order.speedzy.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BulletSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.annotation.NonNull;
import com.bumptech.glide.Glide;
import com.food.order.speedzy.Activity.MealComboActivity;
import com.food.order.speedzy.Activity.MealCombo_Checkout_Activity;
import com.food.order.speedzy.Activity.MedicineActivity;
import com.food.order.speedzy.Activity.MyOrderActivity;
import com.food.order.speedzy.Activity.PatanjaliProductActivity;
import com.food.order.speedzy.Activity.PatanjaliProduct_DetailsActivity;
import com.food.order.speedzy.Activity.PaymentWebView_food;
import com.food.order.speedzy.Activity.RestarantDetails;
import com.food.order.speedzy.Activity.RestaurantActivity_NewVersion;
import com.food.order.speedzy.Activity.TechnicalIssueActivity;
import com.food.order.speedzy.Model.Feedbackmodel;
import com.food.order.speedzy.Model.Home_Response_Model;
import com.food.order.speedzy.Model.PaytmModel;
import com.food.order.speedzy.Model.Restaurant_model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.api.response.home.Alltimings;
import com.food.order.speedzy.api.response.home.ItemsItem;
import com.food.order.speedzy.api.response.home.OpenCloseStatus;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.screen.fruits.FruitShopActivity;
import com.food.order.speedzy.screen.ordersuccess.ActivityOrderDataModel;
import com.food.order.speedzy.screen.ordersuccess.ActivityOrderSuccess;
import com.food.order.speedzy.screen.orderupi.OrderUPIDataModel;
import com.food.order.speedzy.screen.orderupi.OrderUPIPaymentActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import okhttp3.ResponseBody;
import org.json.JSONArray;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;

/**
 * Created by Sujata Mohanty.
 */

public class Commons {
  public static String distance = "";
  public static String duration = "";

  public static String voucherid = "voucherid";
  public static String vaoucheramount = "vaoucheramount";
  public static String deviceid;
  public static double latitude;
  public static String latitude_str = "";
  public static double longitude;
  public static String longitude_str = "";
  public static String deliveryArea;
  public static String flag_for_hta = "";
  public static String restaurant_id;
  public static String menu_id="";
  public static String order_type;
  public static String order_MRP_amount;
  public static String resturant_charge_amount;
  public static String order_amount;
  public static String total_amount;
  public static String subtotal_amount;
  public static String dicount_val;
  public static String coupon_id;
  public static String coupon_amount;
  public static String wallet;
  public static String wallet_static_value;
  public static String delivery_charge;
  public static double min_order_rest;
  public static double min_order_grocery;
  public static String d_type;
  public static String order_date_from_cart;
  public static String Preorder_date_from_cart;
  public static String gst_commision;
  public static double gstamount;
  public static String restname;
  public static String restadd;
  public static String restsuburb;
  public static String closedtoday;
  public static ArrayList<String> cuisinearray;
  public static String ratingflag;
  public static String deliveryfee;
  public static String minimumorder;
  public static ArrayList<String> cuisinelist;
  public static ArrayList<Restaurant_model> restlist;
  public static List<Home_Response_Model.sections.items> offer_list = new ArrayList<>();
  public static List<Home_Response_Model.sections.items> insight_list = new ArrayList<>();
  public static String image_baseURL_very_small;
  public static String image_baseURL_small;
  public static String image_baseURL;
  public static String avada_start_time;
  public static String avada_closetime;
  public static int banner_height;

    public static void clicking_event(Activity mContext, ItemsItem items,
      Alltimings alltimings,
      OpenCloseStatus open_close_status) {
    if (items.getLiveStatus().equalsIgnoreCase("0")) {
      Intent i = new Intent(mContext, TechnicalIssueActivity.class);
      i.putExtra("msg", items.getLiveMessage());
      mContext.startActivity(i);
      mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    } else {
      if (items.getNavType().equalsIgnoreCase("1")) {
        Commons.flag_for_hta = "7";
        Commons.restaurant_id = items.getRestId();
        Commons.menu_id = items.getMenuID();
        Intent i = new Intent(mContext, RestarantDetails.class);
        i.putExtra("additem", false);
        i.putExtra("res_id", items.getRestId());
        i.putExtra("menu_id", items.getMenuID());
        i.putExtra("nav_type", items.getNavType());
        i.putExtra("res_type_flag", "Resturant menu");
        i.putExtra("bookflag", "0");
        i.putExtra("start_time_lunch", alltimings.getLunchStart());
        i.putExtra("end_time_lunch", alltimings.getLunchEnd());
        i.putExtra("start_time_dinner", alltimings.getDinnerStart());
        i.putExtra("end_time_dinner", alltimings.getDinnerEnd());
        i.putExtra("app_openstatus", open_close_status.getOpenstatus());
        i.putExtra("Promotion_msg", "");
        i.putExtra("Flg_ac_status","1");
        mContext.startActivity(i);
        mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      } else if (items.getNavType().equalsIgnoreCase("2")) {
        Commons.menu_id = items.getMenuID();
        Commons.restaurant_id = items.getRestId();
        Commons.flag_for_hta = "Patanjali";
        Intent i = new Intent(mContext, PatanjaliProductActivity.class);
        mContext.startActivity(i);
        mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      } else if (items.getNavType().equalsIgnoreCase("3")) {
        if (items.getMenuID()
            .equals(SpeedzyConstants.RESTURANT_MENU_ID)) {
          Commons.flag_for_hta = "7";
          Commons.restaurant_id = items.getRestId();
          Commons.menu_id = items.getMenuID();
          Intent i = new Intent(mContext, RestaurantActivity_NewVersion.class);
          i.putExtra("res_id", items.getRestId());
          i.putExtra("menu_id", items.getMenuID());
          i.putExtra("name", "Restaurants");
          i.putExtra("nav_type", items.getNavType());
          mContext.startActivity(i);
          mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Commons.flag_for_hta = SpeedzyConstants.Fruits;
          Commons.restaurant_id = items.getRestId();
          Commons.menu_id = SpeedzyConstants.FRUITS_MENU_ID;
          Intent i = new Intent(mContext, FruitShopActivity.class);
          i.putExtra("res_id", items.getRestId());
          i.putExtra("menu_id", SpeedzyConstants.FRUITS_MENU_ID);
          i.putExtra("name", "Fruits");
          i.putExtra("nav_type", items.getNavType());
          mContext.startActivity(i);
          mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      } else if (items.getNavType().equalsIgnoreCase("4")) {
        //Commons.flag_for_hta = "Grocerry";
                   /* Intent i = new Intent(mContext, GroceryActivity.class);
                    mContext.startActivity(i);*/
        Commons.menu_id = "M1004";
        Commons.restaurant_id = "";
        Commons.flag_for_hta = "Patanjali";
        Intent i = new Intent(mContext, PatanjaliProductActivity.class);
        mContext.startActivity(i);
        mContext.overridePendingTransition(R.anim.slide_from_right,
            R.anim.slide_to_left);
      } else if (items.getNavType().equalsIgnoreCase("5")) {
        //dialogshow(mContext, Commons.image_baseURL + items.getImagename());
      } else if (items.getNavType().equalsIgnoreCase("6")) {
        Commons.menu_id = items.getMenuID();
        if (items.getComboId() != null) {
          Intent intent = new Intent(mContext, MealCombo_Checkout_Activity.class);
          intent.putExtra("meal_combo", items);
          intent.putExtra("flag", 0);
          mContext.startActivity(intent);
          mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      } else if (items.getNavType().equalsIgnoreCase("7")) {
        Commons.menu_id = items.getMenuID();
        Intent i = new Intent(mContext, RestaurantActivity_NewVersion.class);
        i.putExtra("res_id", "");
        i.putExtra("menu_id", items.getMenuID());
        i.putExtra("nav_type", items.getNavType());
        i.putExtra("name", items.getName());
        mContext.startActivity(i);
        mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      } else if (items.getNavType().equalsIgnoreCase("8")
          && items.getRestId() != null
          && items.getRestId()
          .equalsIgnoreCase("")) {
        Commons.flag_for_hta = "7";
        Commons.restaurant_id = items.getRestId();
        Commons.menu_id = items.getMenuID();
        Intent i = new Intent(mContext, RestarantDetails.class);
        i.putExtra("additem", false);
        i.putExtra("res_id", items.getRestId());
        i.putExtra("menu_id", items.getMenuID());
        i.putExtra("nav_type", items.getNavType());
        i.putExtra("res_type_flag", "Resturant menu");
        i.putExtra("bookflag", "0");
        i.putExtra("start_time_lunch", alltimings.getLunchStart());
        i.putExtra("end_time_lunch", alltimings.getLunchEnd());
        i.putExtra("start_time_dinner", alltimings.getDinnerStart());
        i.putExtra("end_time_dinner", alltimings.getDinnerEnd());
        i.putExtra("app_openstatus", open_close_status.getOpenstatus());
        i.putExtra("Promotion_msg", "");
        i.putExtra("Flg_ac_status","1");
        mContext.startActivity(i);
        mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      } else if (items.getNavType().equalsIgnoreCase("9")) {
        Commons.menu_id = items.getMenuID();
        Intent intent = new Intent(mContext, MealComboActivity.class);
        intent.putExtra("app_tittle", "Meal Combo");
        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      } else if (items.getNavType().equalsIgnoreCase("12")) {
        Commons.flag_for_hta = "City Special";
        Commons.menu_id = items.getMenuID();
        Intent intent = new Intent(mContext, PatanjaliProduct_DetailsActivity.class);
        intent.putExtra("app_tittle", "City Special");
        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      } else if (items.getNavType().equalsIgnoreCase("11") && items.getMenuID()
          .equalsIgnoreCase(SpeedzyConstants.MEDICINE_ORDER_ID)) {
        Intent intent = new Intent(mContext, MedicineActivity.class);
        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      }
    }
  }

  public static void dialogshow(Context mContext, String url) {
    Dialog dialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.setContentView(R.layout.offer_about_dialog);
    TextView offer = (TextView) dialog.findViewById(R.id.offer);
    ImageView itemImage = (ImageView) dialog.findViewById(R.id.itemImage);
    String longDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n\n" +
        "Aenean mattis tortor id ullamcorper efficitur.\n\n" +
        "Duis id urna non erat faucibus pellentesque.\n\n" +
        "Nullam nec justo non diam consequat elementum vel a turpis.\n\n" +
        "Praesent ullamcorper turpis vitae lacus convallis, sit amet egestas mi pellentesque.";
    String arr[] = longDescription.split("\n\n");
    int bulletGap = (int) dp(mContext, 5);
    SpannableStringBuilder ssb = new SpannableStringBuilder();
    for (int i = 0; i < arr.length; i++) {
      String line = arr[i];
      SpannableString ss = new SpannableString(line);
      ss.setSpan(new BulletSpan(bulletGap), 0, line.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
      ssb.append(ss);
      //avoid last "\n"
      if (i + 1 < arr.length) {
        ssb.append("\n");
      }
    }
    offer.setText(ssb);
    Glide.with(mContext)
        .load(url)
        //.bitmapTransform(new RoundedCornersTransformation(mContext,sCorner, sMargin))
        //.placeholder(R.color.red)
        .into(itemImage);
    //show dialog
    if (!((Activity) mContext).isFinishing()) {
      dialog.show();
    }
  }

  public static float dp(Context context, int dp) {
    return context.getResources().getDisplayMetrics().density * dp;
  }

  public static void dialogshow_payment(Activity context, String delivery_type,
                                        double MRPsum, String suburbid, String ordertype, String userid,
                                        final MySharedPrefrencesData mySharedPrefrencesData, LOcaldbNew lOcaldbNew,
                                        String delivery_charge, JSONArray order_detail) {

    BottomSheetDialog dialog_payment = new BottomSheetDialog(context, R.style.SheetDialog);
    dialog_payment.setContentView(R.layout.payment_dialog1);
    TextView card_payment = dialog_payment.findViewById(R.id.upi_payment);
    TextView cash_on_delivery = dialog_payment.findViewById(R.id.cod_payment);

    if (ordertype.equalsIgnoreCase("2")) {
      cash_on_delivery.setVisibility(View.GONE);
    } else {
      cash_on_delivery.setVisibility(View.VISIBLE);
    }
    card_payment.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        orderid_api_call("0", context, dialog_payment, ordertype, userid, MRPsum,
            mySharedPrefrencesData, lOcaldbNew, suburbid, order_detail);
      }
    });
    cash_on_delivery.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        orderid_api_call("4", context, dialog_payment, ordertype, userid, MRPsum,
            mySharedPrefrencesData, lOcaldbNew, suburbid, order_detail);
      }
    });
    dialog_payment.show();
  }

  /*public static void dialogshow_payment_Upi(Activity context) {
    final BottomSheetDialog dialog_payment = new BottomSheetDialog(context, R.style.SheetDialog);
    dialog_payment.setContentView(R.layout.payment_dialog);
    final ImageView google_pay = (ImageView) dialog_payment.findViewById(R.id.google_pay);
    final ImageView phone_pe = (ImageView) dialog_payment.findViewById(R.id.phone_pe);
    final ImageView paytm = (ImageView) dialog_payment.findViewById(R.id.paytm);
    final ImageView bhim = (ImageView) dialog_payment.findViewById(R.id.bhim);
    TextView text_upi=dialog_payment.findViewById(R.id.text_upi);

    text_upi.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.npci.org.in/upi-faq-s"));
        context.startActivity(browserIntent);
      }
    });
    google_pay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        orderid_api_call("1",context, dialog_payment, Commons.dis_ordertype, Commons.dis_userid, Commons.dis_MRPsum,
                Commons.dis_mySharedPrefrencesData, Commons.dis_lOcaldbNew, Commons.dis_suburbid, Commons.dis_order_detail);

      }
    });
    phone_pe.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        orderid_api_call("2", context, dialog_payment, Commons.dis_ordertype, Commons.dis_userid, Commons.dis_MRPsum,
                Commons.dis_mySharedPrefrencesData, Commons.dis_lOcaldbNew, Commons.dis_suburbid, Commons.dis_order_detail);
      }
    });
    paytm.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        orderid_api_call("3", context, dialog_payment, Commons.dis_ordertype, Commons.dis_userid, Commons.dis_MRPsum,
                Commons.dis_mySharedPrefrencesData, Commons.dis_lOcaldbNew, Commons.dis_suburbid, Commons.dis_order_detail);
      }
    });
    bhim.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        orderid_api_call("5", context, dialog_payment, Commons.dis_ordertype, Commons.dis_userid, Commons.dis_MRPsum,
                Commons.dis_mySharedPrefrencesData, Commons.dis_lOcaldbNew, Commons.dis_suburbid, Commons.dis_order_detail);
      }
    });
    dialog_payment.show();
  }*/

  public static void orderid_api_call(String flag, Activity context,
      BottomSheetDialog dialog_payment, String ordertype, String userid, double MRPsum,
      MySharedPrefrencesData mySharedPrefrencesData, LOcaldbNew lOcaldbNew, String suburbid,
      JSONArray order_detail) {
    if (Commons.flag_for_hta.equals("Grocerry")
        || Commons.flag_for_hta.equals("Patanjali")
        || Commons.flag_for_hta.equals("City Special")
        || Commons.flag_for_hta.equals("Drinking Water")) {
      GrocerypaymentApi(flag, context, dialog_payment, ordertype, userid, MRPsum,
          mySharedPrefrencesData, lOcaldbNew);
    } else {
      paymentApi_cash_on_delivery(flag, context, dialog_payment, userid, ordertype, suburbid,
          mySharedPrefrencesData, lOcaldbNew, order_detail);
    }
    if (dialog_payment != null) {
      dialog_payment.dismiss();
    }
  }

  private static void generateCheckSum(final Activity context, final String total_amount,
      final String orderid, final String userid) {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(context).create(Apimethods.class);
    Call<PaytmModel> call = methods.get_checkhash(total_amount, orderid);
    Log.d("url", "url=" + call.request().url().toString());
    call.enqueue(new Callback<PaytmModel>() {
      @Override
      public void onResponse(Call<PaytmModel> call, Response<PaytmModel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        PaytmModel paytmModel = response.body();
        if (!paytmModel.getORDER_ID().equalsIgnoreCase("")) {
          initializePaytmPayment(context, paytmModel.getCHECKSUMHASH(), paytmModel.getORDER_ID(),
              userid, total_amount);
        }
      }

      @Override
      public void onFailure(Call<PaytmModel> call, Throwable t) {
        Toast.makeText(context.getApplicationContext(), "internet not available..connect internet",
            Toast.LENGTH_LONG).show();
      }
    });
  }

  private static void initializePaytmPayment(Activity context, String checksum, String orderid,
      String userid, String total_amount) {
    PaytmPGService Service = PaytmPGService.getProductionService();
    Map<String, String> paramMap = new HashMap<>();
    paramMap.put("MID", "BANsot71704588984181'");
    paramMap.put("ORDER_ID", orderid);
    paramMap.put("CUST_ID", userid);
    paramMap.put("CHANNEL_ID", "WAP");
    paramMap.put("TXN_AMOUNT", total_amount);
    paramMap.put("WEBSITE", "BANsotWAP");
    paramMap.put("CALLBACK_URL",
        "http://foodinns.com/mobile/ordersuccess?type=3&orderid=" + orderid);
    paramMap.put("CHECKSUMHASH", checksum);
    paramMap.put("INDUSTRY_TYPE_ID", "Retail109");

    //creating a paytm order object using the hashmap
    PaytmOrder order = new PaytmOrder(paramMap);

    //intializing the paytm service
    Service.initialize(order, null);

    //finally starting the payment transaction
    Service.startPaymentTransaction(context, true, true, new PaytmPaymentTransactionCallback() {
      /*Call Backs*/
      public void someUIErrorOccurred(String inErrorMessage) {
      }

      public void onTransactionResponse(Bundle inResponse) {
      }

      public void networkNotAvailable() {
      }

      public void clientAuthenticationFailed(String inErrorMessage) {
      }

      public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage,
          String inFailingUrl) {
      }

      public void onBackPressedCancelTransaction() {
      }

      public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
      }
    });
    PaytmPaymentTransactionCallback zfzs = Service.getmPaymentTransactionCallback();
    Log.d("mytag", "Paytm_call_back" + zfzs.toString());
  }

  public static void webviewintentcall(BottomSheetDialog dialog_payment, Activity context,
      String selected_location_delivery, String delivery_type, double MRPsum, String suburbid) {
    if (Commons.order_MRP_amount != null) {
      Commons.order_MRP_amount = String.format("%.2f", MRPsum);
    } else {
      Commons.order_MRP_amount = String.format("%.2f", 0.0);
    }
    SharedPreferenceSingleton.getInstance().init(context);
    SharedPreferenceSingleton.getInstance()
        .writeStringPreference("key_order_amount", Commons.order_amount);
    SharedPreferenceSingleton.getInstance()
        .writeStringPreference("key_order_mrp_amount", Commons.order_MRP_amount);
    SharedPreferenceSingleton.getInstance()
        .writeStringPreference("key_total_amount", Commons.total_amount);
    SharedPreferenceSingleton.getInstance()
        .writeStringPreference("key_discount_value", Commons.dicount_val);
    SharedPreferenceSingleton.getInstance().writeStringPreference("key_wallet", Commons.wallet);
    SharedPreferenceSingleton.getInstance()
        .writeStringPreference("key_delivery_charge", Commons.delivery_charge);
    SharedPreferenceSingleton.getInstance().writeStringPreference("key_suburb_id", suburbid);
    if (Commons.flag_for_hta == "Grocerry"
        || Commons.flag_for_hta == "Patanjali"
        || Commons.flag_for_hta == "City Special") {
      SharedPreferenceSingleton.getInstance().writeStringPreference("key_order_type", "Delivery");
    } else {
      SharedPreferenceSingleton.getInstance()
          .writeStringPreference("key_order_type", delivery_type);
    }
    SharedPreferenceSingleton.getInstance()
        .writeStringPreference("key_street_name", selected_location_delivery);
    SharedPreferenceSingleton.getInstance().writeStringPreference("key_company_name", "NA");
    SharedPreferenceSingleton.getInstance().writeStringPreference("key_comment", "NA");
    dialog_payment.dismiss();
       /* Intent intent = new Intent(context, PaymentWebView_food.class);
        //          i.putExtra("url",url);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);*/
  }

  public static void GrocerypaymentApi(final String flag, final Activity context,
      final BottomSheetDialog dialog_payment, String ordertype, final String userid, double MRPsum,
      MySharedPrefrencesData mySharedPrefrencesData, final LOcaldbNew lOcaldbNew) {
    String[] listArr = new String[0];
    final LoaderDiloag loaderDiloag = new LoaderDiloag(context);
    JSONArray product_detail = null;
    if (Commons.flag_for_hta.equals("Grocerry")) {
      product_detail = lOcaldbNew.getOrderDetails_patanjali();
    } else if (Commons.flag_for_hta.equals("Patanjali")) {
      product_detail = lOcaldbNew.getOrderDetails_patanjali();
    } else if (Commons.flag_for_hta.equals("City Special")) {
      product_detail = lOcaldbNew.getOrderDetails_CitySpecial();
    } else if (Commons.flag_for_hta.equals("Drinking Water")) {
      product_detail = lOcaldbNew.getOrderDetails_Water();
    }
    ordertype = "Delivery";
   /* if (userid.length() < 1) {
      userid = Commons.deviceid;
      Commons.wallet = "0";
      Commons.dicount_val = "0";
    }*/
    int paymentType = 2;
    if (flag.equalsIgnoreCase("0")) {
      paymentType = SpeedzyConstants.PaymentType.CARD;
    } else {
      paymentType = SpeedzyConstants.PaymentType.CASH_ON_DELIVERY;
    }

    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(context).create(Apimethods.class);
    Call<Feedbackmodel> call = methods.getordergrocery(userid,
        "Android",
        "1",
        mySharedPrefrencesData.getSelectName(context),
        mySharedPrefrencesData.getLName(context),
        mySharedPrefrencesData.getPartyemail(context),
        mySharedPrefrencesData.get_Party_mobile(context),
        mySharedPrefrencesData.getLocation(context),
        "NA",
        "NA",
        mySharedPrefrencesData.getSelectCity_latitude(context),
        mySharedPrefrencesData.getSelectCity_longitude(context),
        Commons.delivery_charge,
        "NA",
        "0",
        Commons.wallet,
        Commons.total_amount,
        product_detail,
        mySharedPrefrencesData.getSelectAddress_ID(context),
        Commons.menu_id, "", paymentType);
    ;
    Log.d("url", "url=" + call.request().url().toString());
    //  System.out.print();
    loaderDiloag.displayDiloag();
    call.enqueue(new Callback<Feedbackmodel>() {
      @Override
      public void onResponse(Call<Feedbackmodel> call, Response<Feedbackmodel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        loaderDiloag.dismissDiloag();
        if (response.isSuccessful()) {
          Feedbackmodel feedback = response.body();
          if (feedback.getStatus().equalsIgnoreCase("Success")) {
            String orderid = feedback.getOrderID();

            /*if (flag.equalsIgnoreCase("1")) {
              google_pay_call(1,context,orderid,Commons.total_amount);
            }else   if (flag.equalsIgnoreCase("2")) {
              google_pay_call(2,context,orderid,Commons.total_amount);
            }else   if (flag.equalsIgnoreCase("3")) {
              //generateCheckSum(context,Commons.total_amount,orderid,userid);
              Intent intent = new Intent(context, PaymentWebView_food.class);
              intent.putExtra("order_id", orderid);
              context.startActivity(intent);
              context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }else   if (flag.equalsIgnoreCase("5")) {
              google_pay_call(3,context,orderid,Commons.total_amount);
            }else */
            if (flag.equalsIgnoreCase("4")) {
              success_order_dialog_show(context, dialog_payment, lOcaldbNew);
            } else if (flag.equalsIgnoreCase("0")) {
              if (Commons.flag_for_hta != null) {
                if (Commons.flag_for_hta.equals("Grocerry")
                    || Commons.flag_for_hta.equals("Patanjali") && lOcaldbNew != null) {
                  lOcaldbNew.deleteCart_patanjali();
                } else if (Commons.flag_for_hta.equals("City Special")) {
                  lOcaldbNew.deleteCart_CitySpecial();
                } else if (Commons.flag_for_hta.equals("Drinking Water") && lOcaldbNew != null) {
                  lOcaldbNew.deleteCart_Water();
                } else if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)
                    && lOcaldbNew != null) {
                  lOcaldbNew.deleteFruitCart();
                } else if (lOcaldbNew != null) {
                  lOcaldbNew.deleteCart();
                }
              }
              OrderUPIDataModel orderUPIDataModel = new OrderUPIDataModel();
              orderUPIDataModel.setOrderNumber(orderid);
              orderUPIDataModel.setPrice(total_amount);
              context.finish();
              context.startActivity(OrderUPIPaymentActivity.newIntent(context, orderUPIDataModel));
              context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
          }
        } else {
          try {
            String data = response.errorBody().string();
            Toast.makeText(context, data, Toast.LENGTH_LONG).show();
          } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
          }
        }
      }

      @Override
      public void onFailure(Call<Feedbackmodel> call, Throwable t) {
        t.printStackTrace();
        loaderDiloag.dismissDiloag();
        Toast.makeText(context, "internet not available..connect internet", Toast.LENGTH_LONG)
            .show();
      }
    });
  }

  public static void google_pay_call(int i, Activity context, String orderid, String amount) {
    String PACKAGE_NAME = "";
    int REQUEST_CODE = 0;
    if (i == 1) {
      PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user";
      REQUEST_CODE = 11;
    } else if (i == 2) {
      PACKAGE_NAME = "com.phonepe.app";
      REQUEST_CODE = 22;
    } else if (i == 3) {
      PACKAGE_NAME = "in.org.npci.upiapp";
      REQUEST_CODE = 33;
    }
    else if (i == 4) {
      PACKAGE_NAME = "net.one97.paytm";
      REQUEST_CODE = 44;
    }
    String tn = "Food Order", menu_id = "M1001";
    if (Commons.menu_id.equalsIgnoreCase("M1010") || Commons.menu_id.equalsIgnoreCase("M1004")) {
      tn = "Grocery Order";
      menu_id = Commons.menu_id;
    } else if (Commons.menu_id.equalsIgnoreCase("M1005")) {
      tn = "Fruit Order";
      menu_id = Commons.menu_id;
    } else if (Commons.menu_id.equalsIgnoreCase("M1001")) {
      tn = "Food Order";
      menu_id = Commons.menu_id;
    } else if (Commons.menu_id.equalsIgnoreCase("M1027")) {
      tn = "Medicine Order";
      menu_id = Commons.menu_id;
    } else if (Commons.menu_id.equalsIgnoreCase("M1007")) {
      tn = "Cake Order";
      menu_id = Commons.menu_id;
    } else if (Commons.menu_id.equalsIgnoreCase("M1016")) {
      tn = "City Special Order";
      menu_id = Commons.menu_id;
    } else {
      tn = "Food Order";
      menu_id = "M1001";
    }
    String url =
        "http://35.244.8.156/speedzy/mobile/ordersuccess?menuID=" + menu_id + "&orderid=" + orderid;
    Uri uri =
        new Uri.Builder()
            .scheme("upi")
            .authority("pay")
            .appendQueryParameter("pa", "bharatpe09894621521@yesbankltd")
            .appendQueryParameter("pn", "Speedzy India")
            .appendQueryParameter("tr", orderid)
            .appendQueryParameter("tn", tn)
            .appendQueryParameter("am", amount)
            .appendQueryParameter("cu", "INR")
            .build();
    try {
      Intent intent = new Intent(Intent.ACTION_VIEW);
      intent.setData(uri);
      intent.setPackage(PACKAGE_NAME);
      context.startActivityForResult(intent, REQUEST_CODE);
    } catch (android.content.ActivityNotFoundException anfe) {
      context.startActivity(new Intent(Intent.ACTION_VIEW,
          Uri.parse("https://play.google.com/store/apps/details?id=" + PACKAGE_NAME)));
    }
  }

  public static void paymentApi_cash_on_delivery(final String flag, final Activity context,
      final BottomSheetDialog dialog_payment, final String userid, String ordertype,
      String suburbid, MySharedPrefrencesData mySharedPrefrencesData, final LOcaldbNew lOcaldbNew,
      JSONArray order_detail) {
       /* if(userid.length()<1){
            userid= Commons.deviceid;
            Commons.wallet="NA";
            Commons.dicount_val="NA";
        }*/
    int paymentType = 2;
    if (flag.equalsIgnoreCase("4")) {
      paymentType = SpeedzyConstants.PaymentType.CASH_ON_DELIVERY;
    } else {
      paymentType = SpeedzyConstants.PaymentType.CARD;
    }
    final LoaderDiloag loaderDiloag = new LoaderDiloag(context);
    JSONArray order_data = new JSONArray();
    if (Commons.order_type.equals("1")) {
      ordertype = "Delivery";
    } else {
      ordertype = "Pickup";
    }
    if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)) {
      if (!GeneralUtil.isStringEmpty(Commons.restaurant_id)) {
        order_data = lOcaldbNew.getFruitsOrderDetails(Commons.restaurant_id);
      }
    } else {
      if (!GeneralUtil.isStringEmpty(Commons.restaurant_id)) {
        order_data = lOcaldbNew.getOrderDetails(Commons.restaurant_id);
      }
    }
    Commons.deliveryArea = "";
    Commons.deviceid = "";
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(context).create(Apimethods.class);
    Call<Feedbackmodel> call = methods.setPaymentNew(userid,
        mySharedPrefrencesData.get_Party_mobile(context),
        Commons.restaurant_id,
        mySharedPrefrencesData.getSelectName(context),
        mySharedPrefrencesData.getLName(context),
        mySharedPrefrencesData.getPartyemail(context),
        order_data,
        suburbid,
        Commons.order_amount,
        Commons.delivery_charge,
        Commons.total_amount,
        "NA",
        "NA",
        mySharedPrefrencesData.getLocation(context),
        mySharedPrefrencesData.getSelectArea(context),
        Commons.wallet,
        "",
        Commons.voucherid,
        Commons.vaoucheramount,
        "restspecialoffer",
        Commons.dicount_val,
        ordertype,
        Commons.order_date_from_cart,
        Commons.Preorder_date_from_cart,
        Commons.deviceid,
        "Android",
        Commons.gstamount,
        mySharedPrefrencesData.getSelectCity_latitude(context),
        mySharedPrefrencesData.getSelectCity_longitude(context),
        mySharedPrefrencesData.getSelectAddress_ID(context),
        mySharedPrefrencesData.getSelectCity(context), paymentType,Commons.resturant_charge_amount);
    Log.d("url", "url=" + call.request().url().toString());
    loaderDiloag.displayDiloag();
    call.enqueue(new Callback<Feedbackmodel>() {
      @Override
      public void onResponse(@NonNull Call<Feedbackmodel> call,
          @NonNull Response<Feedbackmodel> response) {
        loaderDiloag.dismissDiloag();
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        if (response.isSuccessful()) {
          Feedbackmodel feedback = response.body();
          if (feedback != null && feedback.getStatus().equalsIgnoreCase("Success")) {
            String orderid = feedback.getOrderID();
            orderid = orderid.substring(1);

            if (flag.equalsIgnoreCase("4")) {
              cash_on_delivery_api(userid, flag, loaderDiloag, context, orderid, dialog_payment,
                  lOcaldbNew,
                  SpeedzyConstants.PaymentType.CASH_ON_DELIVERY);
            } else {
              //generateCheckSum(context,Commons.total_amount,orderid,userid);
              //Intent intent = new Intent(context, PaymentWebView_food.class);
              //intent.putExtra("order_id", orderid);
              //context.startActivity(intent);
              //context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

              //if (Commons.flag_for_hta != null) {
              //  if (Commons.flag_for_hta.equals("Grocerry")
              //      || Commons.flag_for_hta.equals("Patanjali") && lOcaldbNew != null) {
              //    lOcaldbNew.deleteCart_patanjali();
              //  } else if (Commons.flag_for_hta.equals("City Special")) {
              //    lOcaldbNew.deleteCart_CitySpecial();
              //  } else if (Commons.flag_for_hta.equals("Drinking Water") && lOcaldbNew != null) {
              //    lOcaldbNew.deleteCart_Water();
              //  } else if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)
              //      && lOcaldbNew != null) {
              //    lOcaldbNew.deleteFruitCart();
              //  } else if (lOcaldbNew != null) {
              //    lOcaldbNew.deleteCart();
              //  }
              //}
              //OrderUPIDataModel orderUPIDataModel = new OrderUPIDataModel();
              //orderUPIDataModel.setOrderNumber(orderid);
              //context.finish();
              //context.startActivity(OrderUPIPaymentActivity.newIntent(context, orderUPIDataModel));
              //context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
              cash_on_delivery_api(userid, flag, loaderDiloag, context, orderid, dialog_payment,
                  lOcaldbNew,
                  SpeedzyConstants.PaymentType.CARD);
            }
          }
        } else {
          try {
            String data = response.errorBody().string();
            Toast.makeText(context, data, Toast.LENGTH_LONG).show();
          } catch (Exception e) {
            loaderDiloag.dismissDiloag();
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
          }
        }
      }

      @Override
      public void onFailure(@NonNull Call<Feedbackmodel> call, @NonNull Throwable t) {
        t.printStackTrace();
        loaderDiloag.dismissDiloag();
      }
    });
  }

  private static void cash_on_delivery_api(String userid, final String flag,
      final LoaderDiloag loaderDiloag,
      final Activity context,
      String orderid, final BottomSheetDialog dialog_payment, final LOcaldbNew lOcaldbNew,
      int paymentType) {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(context).create(Apimethods.class);
    Call<ResponseBody> call = null;
    String paymentMethod = "2";
    if (paymentType == SpeedzyConstants.PaymentType.CASH_ON_DELIVERY) {
      paymentMethod = "2";
    } else {
      paymentMethod = "4";
    }
    if (Commons.flag_for_hta == "Grocerry"
        || Commons.flag_for_hta == "Patanjali"
        || Commons.flag_for_hta == "City Special"
        || Commons.flag_for_hta == "Drinking Water") {
      call = methods.get_cash_payment_patanjali(paymentMethod, orderid);
    } else {
      call = methods.get_cash_payment(paymentMethod, orderid);
    }
    Log.d("url", "url=" + call.request().url().toString());
    call.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(@NonNull Call<ResponseBody> call,
          @NonNull Response<ResponseBody> response) {
        loaderDiloag.dismissDiloag();
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        if (response.code() == 200 || response.code() == 202) {
          //success_order_dialog_show(context, dialog_payment, lOcaldbNew);
          if (paymentType == SpeedzyConstants.PaymentType.CASH_ON_DELIVERY) {
            if (Commons.flag_for_hta != null) {
              if (Commons.flag_for_hta.equals("Grocerry")
                  || Commons.flag_for_hta.equals("Patanjali") && lOcaldbNew != null) {
                lOcaldbNew.deleteCart_patanjali();
              } else if (Commons.flag_for_hta.equals("City Special")) {
                lOcaldbNew.deleteCart_CitySpecial();
              } else if (Commons.flag_for_hta.equals("Drinking Water") && lOcaldbNew != null) {
                lOcaldbNew.deleteCart_Water();
              } else if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)
                  && lOcaldbNew != null) {
                lOcaldbNew.deleteFruitCart();
              } else if (lOcaldbNew != null) {
                lOcaldbNew.deleteCart();
              }
              ActivityOrderDataModel activityOrderDataModel = new ActivityOrderDataModel();
              activityOrderDataModel.setFlag(Commons.flag_for_hta);
              activityOrderDataModel.setOrderId(orderid);
              context.finish();
              context.startActivity(
                  ActivityOrderSuccess.newIntent(context, activityOrderDataModel));
              context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
          } else {
            if (Commons.flag_for_hta != null && Commons.flag_for_hta.equals("Grocerry")
                || Commons.flag_for_hta.equals("Patanjali") && lOcaldbNew != null) {
              lOcaldbNew.deleteCart_patanjali();
            } else if (Commons.flag_for_hta != null && Commons.flag_for_hta.equals(
                "City Special")) {
              lOcaldbNew.deleteCart_CitySpecial();
            } else if (Commons.flag_for_hta != null
                && Commons.flag_for_hta.equals("Drinking Water")
                && lOcaldbNew != null) {
              lOcaldbNew.deleteCart_Water();
            } else if (Commons.flag_for_hta != null && Commons.flag_for_hta.equalsIgnoreCase(
                SpeedzyConstants.Fruits)
                && lOcaldbNew != null) {
              lOcaldbNew.deleteFruitCart();
            } else if (lOcaldbNew != null) {
              lOcaldbNew.deleteCart();
            }
            OrderUPIDataModel orderUPIDataModel = new OrderUPIDataModel();
            orderUPIDataModel.setOrderNumber(orderid);
            orderUPIDataModel.setPrice(total_amount);
            context.finish();
            context.startActivity(OrderUPIPaymentActivity.newIntent(context, orderUPIDataModel));
            context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            /*else  if (flag.equalsIgnoreCase("1")) {
              google_pay_call(1,context,orderid,Commons.total_amount);
            }else  if (flag.equalsIgnoreCase("2")) {
              google_pay_call(2,context,orderid,Commons.total_amount);
            }else  if (flag.equalsIgnoreCase("3")) {
              //generateCheckSum(context,Commons.total_amount,orderid,userid);
              Intent intent = new Intent(context, PaymentWebView_food.class);
              intent.putExtra("order_id", orderid);
              context.startActivity(intent);
              context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }else   if (flag.equalsIgnoreCase("5")) {
              google_pay_call(3,context,orderid,Commons.total_amount);
            }*/
          }
        }
      }

      @Override
      public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
        loaderDiloag.dismissDiloag();
        t.printStackTrace();
        //Toast.makeText(context, "internet not available..connect internet", Toast.LENGTH_LONG)
        //    .show();
      }
    });
  }

  public static void success_order_dialog_show(final Activity context,
      final BottomSheetDialog dialog_payment, final LOcaldbNew lOcaldbNew) {
    final Dialog dialog = new Dialog(context);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    Objects.requireNonNull(dialog.getWindow())
        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.setContentView(R.layout.order_successful_popup);
    TextView ok = (TextView) dialog.findViewById(R.id.ok);
    ok.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int type = 0;
        if (dialog_payment != null) {
          dialog_payment.dismiss();
        }
        if (Commons.flag_for_hta != null) {
          if (Commons.flag_for_hta.equals("Grocerry")
              || Commons.flag_for_hta.equals("Patanjali") && lOcaldbNew != null) {
            lOcaldbNew.deleteCart_patanjali();
            type = 2;
          } else if (Commons.flag_for_hta.equals("City Special")) {
            lOcaldbNew.deleteCart_CitySpecial();
            type = 2;
          } else if (Commons.flag_for_hta.equals("Drinking Water") && lOcaldbNew != null) {
            lOcaldbNew.deleteCart_Water();
            type = 2;
          } else if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)
              && lOcaldbNew != null) {
            type = 1;
            lOcaldbNew.deleteFruitCart();
          } else if (lOcaldbNew != null) {
            lOcaldbNew.deleteCart();
            type = 1;
          }
          Intent intent = new Intent(context, MyOrderActivity.class);
          intent.putExtra("flag", 1);
          intent.putExtra("type", type);
          context.startActivity(intent);
          context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Intent intent = new Intent(context, MyOrderActivity.class);
          intent.putExtra("flag", 1);
          intent.putExtra("type", type);
          context.startActivity(intent);
          context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
    if (context != null && !context.isFinishing()) {
      //show dialog
      dialog.show();
    }
  }

  public static void back_button_transition(Activity contex) {
    contex.finish();
    contex.overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
  }

  public static float image_height_width(Activity mContext, String height, String width) {
    float height_img = Float.parseFloat(height);
    ;
    float width_img = Float.parseFloat(width);
    DisplayMetrics displayMetrics = new DisplayMetrics();
    ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    float sys_width = displayMetrics.widthPixels;
    System.out.println("actual_height" + (height_img / width_img));
    float cal_height =
        (height_img / width_img) * ((sys_width / displayMetrics.scaledDensity) - 20);
    return cal_height;
  }
    /*private void clicking_event() {
        if ((items.getMenu_ID().equalsIgnoreCase("M1000")&&items.getNav_type().equalsIgnoreCase("1"))){
            Commons.flag_for_hta = "7";
            Commons.restaurant_id = items.getRest_id();
            Intent i = new Intent(mContext, RestarantDetails_New.class);
            i.putExtra("res_id", items.getRest_id());
            i.putExtra("res_type_flag", "Resturant menu");
            i.putExtra("bookflag", "0");
            mContext.startActivity(i);
        } if ((items.getMenu_ID().equalsIgnoreCase("M1008")||items.getMenu_ID().equalsIgnoreCase("M1009"))&&items.getNav_type().equalsIgnoreCase("1")){
            Commons.flag_for_hta = "9";
            Commons.restaurant_id = "1250";
            Intent in=new Intent(mContext, RestarantDetails_New.class);
            in.putExtra("res_type_flag", "Abhada");
            in.putExtra("res_id", "1250");
            mContext.startActivity(in);
        }
        if ((items.getMenu_ID().equalsIgnoreCase("M1013")&&items.getNav_type().equalsIgnoreCase("1"))){
            Commons.flag_for_hta = "8";
            Commons.restaurant_id = "1249";
            Intent in=new Intent(mContext, RestarantDetails_New.class);
            in.putExtra("res_type_flag", "Food on train");
            in.putExtra("res_id", "1249");
            mContext.startActivity(in);
        }
        if ((items.getMenu_ID().equalsIgnoreCase("M1005")&&items.getNav_type().equalsIgnoreCase("3"))){
            Commons.flag_for_hta = "10";
            Commons.restaurant_id = "1262";
            Intent in=new Intent(mContext, RestarantDetails_New.class);
            in.putExtra("res_type_flag", "Fresh fruits");
            in.putExtra("res_id", "1262");
            mContext.startActivity(in);
        }
        if ((items.getMenu_ID().equalsIgnoreCase("M1022")&&items.getNav_type().equalsIgnoreCase("7"))){
            Commons.flag_for_hta = "11";
                   *//* Intent i = new Intent(mContext, CaterService.class);
                    i.putExtra("imageslide", 4);
                    mContext.startActivity(i);*//*
        }
    }*/

  public static void hideSoftKeyboard(View view) {
    try {
      InputMethodManager inputMethodManager =
          (InputMethodManager) view.getContext().getSystemService(INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Shows the soft keyboard
   */
  public static void showSoftKeyboard(View view) {
    InputMethodManager inputMethodManager =
        (InputMethodManager) view.getContext().getSystemService(INPUT_METHOD_SERVICE);
    view.requestFocus();
    inputMethodManager.showSoftInput(view, 0);
  }

  public static double getConvertPriceToDouble(String sale_price) {

    try {
      sale_price = sale_price.replace(",", "");
      return Double.parseDouble(sale_price);
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
    return 0.00;
  }

  public static void placeMedicineOrder(final String flag, final Activity context,
      final BottomSheetDialog dialog_payment, String ordertype, String userid, double MRPsum,
      MySharedPrefrencesData mySharedPrefrencesData, final LOcaldbNew lOcaldbNew,
      String[] imageList) {
    JSONArray product_detail = ApiUtils.getMedcineOrderJsonArray();
    //if (Commons.flag_for_hta.equals("Grocerry")) {
    //  product_detail = lOcaldbNew.getOrderDetails_patanjali();
    //} else if (Commons.flag_for_hta.equals("Patanjali")) {
    //  product_detail = lOcaldbNew.getOrderDetails_patanjali();
    //} else if (Commons.flag_for_hta.equals("City Special")) {
    //  product_detail = lOcaldbNew.getOrderDetails_CitySpecial();
    //} else if (Commons.flag_for_hta.equals("Drinking Water")) {
    //  product_detail = lOcaldbNew.getOrderDetails_Water();
    //}
    ordertype = "Delivery";
    if (userid.length() < 1) {
      userid = Commons.deviceid;
      Commons.wallet = "0";
      Commons.dicount_val = "0";
    } else if (Commons.wallet == null) {
      Commons.wallet = "0";
    }
    int paymentType = 2;
    if (flag.equalsIgnoreCase("0")) {
      paymentType = SpeedzyConstants.PaymentType.CARD;
    } else {
      paymentType = SpeedzyConstants.PaymentType.CASH_ON_DELIVERY;
    }

    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(context).create(Apimethods.class);
    Call<Feedbackmodel> call = methods.getordergrocery(userid,
        "Android",
        "1",
        mySharedPrefrencesData.getSelectName(context),
        mySharedPrefrencesData.getLName(context),
        mySharedPrefrencesData.getPartyemail(context),
        mySharedPrefrencesData.get_Party_mobile(context),
        mySharedPrefrencesData.getLocation(context),
        "NA",
        "NA",
        mySharedPrefrencesData.getSelectCity_latitude(context),
        mySharedPrefrencesData.getSelectCity_longitude(context),
        "0",
        "NA",
        "0",
        Commons.wallet,
        "0",
        product_detail,
        mySharedPrefrencesData.getSelectAddress_ID(context),
        SpeedzyConstants.MEDICINE_ORDER_ID, android.text.TextUtils.join(",", imageList),
        paymentType);
    ;
    Log.d("url", "url=" + call.request().url().toString());
    //  System.out.print();

    call.enqueue(new Callback<Feedbackmodel>() {
      @Override
      public void onResponse(Call<Feedbackmodel> call, Response<Feedbackmodel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        if (response.isSuccessful()) {
          Feedbackmodel feedback = response.body();
          if (feedback.getStatus().equalsIgnoreCase("Success")) {
            String orderid = feedback.getOrderID();
            //orderid=orderid.substring(1);
            if (flag.equalsIgnoreCase("1")) {
              //cash_on_delivery_api(context, orderid, dialog_payment, lOcaldbNew);
              success_order_dialog_show(context, dialog_payment, lOcaldbNew);
            } else {
              //generateCheckSum(context,Commons.total_amount,orderid,userid);
              Intent intent = new Intent(context, PaymentWebView_food.class);
              intent.putExtra("order_id", orderid);
              context.startActivity(intent);
              context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
          }
        } else {
          try {
            assert response.errorBody() != null;
            String data = response.errorBody().string();
            Toast.makeText(context, data, Toast.LENGTH_LONG).show();
          } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
          }
        }
      }

      @Override
      public void onFailure(@NonNull Call<Feedbackmodel> call, @NonNull Throwable t) {
        t.printStackTrace();
        //Toast.makeText(context, "internet not available..connect internet", Toast.LENGTH_LONG)
        //    .show();
      }
    });
  }

  public static void dialogMedicineshow_payment(final Activity context,
      final String delivery_type,
      final double MRPsum, final String suburbid, final String ordertype, final String userid,
      final MySharedPrefrencesData mySharedPrefrencesData, final LOcaldbNew lOcaldbNew,
      String delivery_charge, final JSONArray order_detail, List<String> imageList) {
    final BottomSheetDialog dialog_payment = new BottomSheetDialog(context, R.style.SheetDialog);
    dialog_payment.setContentView(R.layout.payment_dialog);
    dialog_payment.setCancelable(false);
    dialog_payment.setCanceledOnTouchOutside(false);
    final TextView card_payment = (TextView) dialog_payment.findViewById(R.id.card_payment);
    final LinearLayout viewLine = (LinearLayout) dialog_payment.findViewById(R.id.view_payment);
    TextView text_upi = dialog_payment.findViewById(R.id.text_upi);
    text_upi.setVisibility(View.GONE);
    viewLine.setVisibility(View.GONE);
    card_payment.setVisibility(View.GONE);
    final TextView cash_on_delivery =
        (TextView) dialog_payment.findViewById(R.id.cash_on_delivery);

    if (ordertype.equalsIgnoreCase("2")) {
      cash_on_delivery.setVisibility(View.GONE);
    } else {
      cash_on_delivery.setVisibility(View.VISIBLE);
    }
    //card_payment.setOnClickListener(new View.OnClickListener() {
    //  @Override
    //  public void onClick(View v) {
    //    placeMedicineOrder("0", context, dialog_payment, ordertype, userid, MRPsum,
    //        mySharedPrefrencesData, lOcaldbNew, imageList);
    //  }
    //});
    cash_on_delivery.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String[] listArr = new String[imageList.size()];
        listArr = imageList.toArray(listArr);
        placeMedicineOrder("1", context, dialog_payment, ordertype, userid, MRPsum,
            mySharedPrefrencesData, lOcaldbNew, listArr);
      }
    });
    dialog_payment.show();
  }

  public static void dialogGrocery_payment(final Activity context, final String delivery_type,
      final double MRPsum, final String suburbid, final String ordertype, final String userid,
      final MySharedPrefrencesData mySharedPrefrencesData, final LOcaldbNew lOcaldbNew,
      String delivery_charge, final JSONArray order_detail, List<String> imageList) {
    final BottomSheetDialog dialog_payment = new BottomSheetDialog(context, R.style.SheetDialog);
    dialog_payment.setContentView(R.layout.payment_dialog);
    dialog_payment.setCanceledOnTouchOutside(false);
    dialog_payment.setCancelable(false);
    final TextView card_payment = (TextView) dialog_payment.findViewById(R.id.card_payment);
    final LinearLayout viewLine = (LinearLayout) dialog_payment.findViewById(R.id.view_payment);
    TextView text_upi = dialog_payment.findViewById(R.id.text_upi);
    text_upi.setVisibility(View.GONE);
    viewLine.setVisibility(View.GONE);
    card_payment.setVisibility(View.GONE);
    final TextView cash_on_delivery =
        (TextView) dialog_payment.findViewById(R.id.cash_on_delivery);

    if (ordertype.equalsIgnoreCase("2")) {
      cash_on_delivery.setVisibility(View.GONE);
    } else {
      cash_on_delivery.setVisibility(View.VISIBLE);
    }
    //card_payment.setOnClickListener(new View.OnClickListener() {
    //  @Override
    //  public void onClick(View v) {
    //    GroceryImageUploadApi("0", context, dialog_payment, ordertype, userid, MRPsum,
    //        mySharedPrefrencesData, lOcaldbNew, imageList);
    //  }
    //});
    cash_on_delivery.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        GroceryImageUploadApi("1", context, dialog_payment, ordertype, userid, MRPsum,
            mySharedPrefrencesData, lOcaldbNew, imageList);
      }
    });
    dialog_payment.show();
  }

  public static void GroceryImageUploadApi(final String flag, final Activity context,
      final BottomSheetDialog dialog_payment, String ordertype, String userid, double MRPsum,
      MySharedPrefrencesData mySharedPrefrencesData, final LOcaldbNew lOcaldbNew,
      List<String> imageList) {
    final LoaderDiloag loaderDiloag = new LoaderDiloag(context);
    JSONArray product_detail = null;
    //if (Commons.flag_for_hta.equals("Grocerry")) {
    //  product_detail = lOcaldbNew.getOrderDetails_patanjali();
    //} else if (Commons.flag_for_hta.equals("Patanjali")) {
    //  product_detail = lOcaldbNew.getOrderDetails_patanjali();
    //} else if (Commons.flag_for_hta.equals("City Special")) {
    //  product_detail = lOcaldbNew.getOrderDetails_CitySpecial();
    //} else if (Commons.flag_for_hta.equals("Drinking Water")) {
    //  product_detail = lOcaldbNew.getOrderDetails_Water();
    //}
    product_detail = ApiUtils.getMedcineOrderJsonArray();
    ordertype = "Delivery";
    if (userid.length() < 1) {
      userid = Commons.deviceid;
      Commons.wallet = "0";
      Commons.dicount_val = "0";
    } else {
      Commons.wallet = "0";
      Commons.dicount_val = "0";
    }
    int paymentType = 2;
    if (flag.equalsIgnoreCase("0")) {
      paymentType = SpeedzyConstants.PaymentType.CARD;
    } else {
      paymentType = SpeedzyConstants.PaymentType.CASH_ON_DELIVERY;
    }
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(context).create(Apimethods.class);
    String[] listArr = new String[imageList.size()];
    listArr = imageList.toArray(listArr);
    //
    //    String[] output = new String[imageList.size()];
    //
    //    StringBuilder builder;
    //    List<String> imgList = new ArrayList<>();
    //    for(int i=0;i<imageList.size();i++){
    //      builder = new StringBuilder();
    //      builder.append("\"");
    //      builder.append(imageList.get(i));
    //      builder.append("\"");
    //      output[i] = builder.toString();
    //      imgList.add(builder.toString());
    //    }
    //    String[] lArr = new String[imgList.size()];
    //    lArr = imgList.toArray(lArr);

    Call<Feedbackmodel> call = methods.getordergrocery(userid,
        "Android",
        "1",
        mySharedPrefrencesData.getSelectName(context),
        mySharedPrefrencesData.getLName(context),
        mySharedPrefrencesData.getPartyemail(context),
        mySharedPrefrencesData.get_Party_mobile(context),
        mySharedPrefrencesData.getLocation(context),
        "NA",
        "NA",
        mySharedPrefrencesData.getSelectCity_latitude(context),
        mySharedPrefrencesData.getSelectCity_longitude(context),
        "0",
        "NA",
        "0",
        "0",
        "0",
        product_detail,
        mySharedPrefrencesData.getSelectAddress_ID(context),
        SpeedzyConstants.GROCERY_ORDER_ID, android.text.TextUtils.join(",", listArr), paymentType);
    ;
    Log.d("url", "url=" + call.request().url().toString());
    //  System.out.print();
    loaderDiloag.displayDiloag();
    call.enqueue(new Callback<Feedbackmodel>() {
      @Override
      public void onResponse(Call<Feedbackmodel> call, Response<Feedbackmodel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        loaderDiloag.dismissDiloag();
        if (response.isSuccessful()) {
          Feedbackmodel feedback = response.body();
          if (feedback.getStatus().equalsIgnoreCase("Success")) {
            String orderid = feedback.getOrderID();
            //orderid=orderid.substring(1);
            if (flag.equalsIgnoreCase("1")) {
              //cash_on_delivery_api(context, orderid, dialog_payment, lOcaldbNew);
              success_order_dialog_show(context, dialog_payment, lOcaldbNew);
            } else {
              //generateCheckSum(context,Commons.total_amount,orderid,userid);
              Intent intent = new Intent(context, PaymentWebView_food.class);
              intent.putExtra("order_id", orderid);
              context.startActivity(intent);
              context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
          }
        } else {
          try {
            String data = response.errorBody().string();
            Toast.makeText(context, data, Toast.LENGTH_LONG).show();
          } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
          }
        }
      }

      @Override
      public void onFailure(Call<Feedbackmodel> call, Throwable t) {
        t.printStackTrace();
        loaderDiloag.dismissDiloag();
        Toast.makeText(context, "internet not available..connect internet", Toast.LENGTH_LONG)
            .show();
      }
    });
  }

  public static String month(int month) {
    String in_month ="";
    if (month==0){
      in_month="JAN";
    }else if (month==1){
      in_month="FEB";
    }else if (month==2){
      in_month="MAR";
    }else if (month==3){
      in_month="APR";
    }else if (month==4){
      in_month="MAY";
    }else if (month==5){
      in_month="JUN";
    }else if (month==6){
      in_month="JUL";
    }else if (month==7){
      in_month="AUG";
    }else if (month==8){
      in_month="SEP";
    }else if (month==9){
      in_month="OCT";
    }else if (month==10){
      in_month="NOV";
    }else if (month==11){
      in_month="DEC";
    }
    return in_month;
  }

  public static String day(int day) {
    String day_str="";
    if (day==1){
      day_str="Monday";
    }else if (day==2){
      day_str="Tuesday";
    }else if (day==3){
      day_str="Wednesday";
    }else if (day==4){
      day_str="Thursday";
    }else if (day==5){
      day_str="Friday";
    }else if (day==6){
      day_str="Saturday";
    }else if (day==0){
      day_str="Sunday";
    }
    return day_str;
  }
  public static String Time(TimePicker tpSelectedTime) {
    String strTime_leave_on= null;
    String strHour_leave_on = null, strMinute_leave_on = null, strAM_PM_leave_on = null;

    strMinute_leave_on = tpSelectedTime.getCurrentMinute().toString();
    if (tpSelectedTime.getCurrentMinute() < 10) {
      strMinute_leave_on = "0" + strMinute_leave_on;
    }
    if (tpSelectedTime.getCurrentHour() > 12) {
      strHour_leave_on = (tpSelectedTime.getCurrentHour() - 12) + "";
      strAM_PM_leave_on = "PM";
    } else if (tpSelectedTime.getCurrentHour() == 12) {
      strHour_leave_on = tpSelectedTime.getCurrentHour().toString();
      strAM_PM_leave_on = "PM";
    } else if (tpSelectedTime.getCurrentHour() < 12) {
      strHour_leave_on = tpSelectedTime.getCurrentHour().toString();
      strAM_PM_leave_on = "AM";
    }
    if (strHour_leave_on != null && strAM_PM_leave_on != null) {
      strTime_leave_on = strHour_leave_on + ":" + strMinute_leave_on + " " + strAM_PM_leave_on;
    }
    return strTime_leave_on;
  }

    public static int getRandomColor(Context mContext) {
      Random rnd = new Random();
      return Color.argb(25, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }
    public static double getDistance(String start_lati, String start_longi, String end_lati, String end_longi){
        NumberFormat f = NumberFormat.getInstance();
        double distanceInMeters = 0;
        try {
            distanceInMeters = f.parse("0").doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    if (start_lati !=null && start_longi !=null ) {
      Location startPoint = new Location("locationA");
      startPoint.setLatitude(Double.parseDouble(start_lati));
      startPoint.setLongitude(Double.parseDouble(start_longi));


      double end_final_lat = 0,end_final_long=0;
      if (end_lati.equalsIgnoreCase("") || end_longi.equalsIgnoreCase("")){
        try {
          end_final_lat = f.parse("0").doubleValue();
          end_final_long=f.parse("0").doubleValue();
        } catch (ParseException e) {
          e.printStackTrace();
        }
      }else {
        end_final_lat= Double.parseDouble(end_lati);
        end_final_long=Double.parseDouble(end_longi);
      }
      Location endPoint = new Location("locationB");
      endPoint.setLatitude(end_final_lat);
      endPoint.setLongitude(end_final_long);

      distanceInMeters  = startPoint.distanceTo(endPoint);
    }
      return distanceInMeters/1000;
    }

  /*public static void SetGlideImage(Context mContext, String main, String thumb, ImageView imageview) {
    RequestOptions requestOptions = new RequestOptions();
    requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
    Glide
            .with(mContext)
            .load(main)
            .thumbnail(
                    Glide.with(mContext)
                            .load(thumb))
            .apply(requestOptions)
            //.placeholder(R.drawable.combo_placeholder)
            .into(imageview);
  }
  public static void SetGlideImageCircle(Context mContext, String main, String thumb, ImageView imageview) {
    RequestOptions requestOptions = new RequestOptions();
    requestOptions = requestOptions.transforms(new CircleTransform(mContext));
    Glide
            .with(mContext)
            .load(main)
            .thumbnail(
                    Glide.with(mContext)
                            .load(thumb))
            .apply(requestOptions)
            //.placeholder(R.drawable.combo_placeholder)
            .into(imageview);
  }*/
}
