package com.food.order.speedzy.screen.coupon;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.food.order.speedzy.R;
import com.food.order.speedzy.api.response.coupon.CouponItem;
import java.util.ArrayList;
import java.util.List;

public class CouponAdapter extends RecyclerView.Adapter<CouponVH> {
  private final CallBack mCallBack;
  private List<CouponItem> mData;

  public CouponAdapter(CallBack callBack) {
    mData = new ArrayList<>();
    mCallBack = callBack;
  }

  @NonNull @Override public CouponVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_inflate_coupon_row, parent, false);
    return new CouponVH(v, new CouponVH.Callback() {
      @Override public void onClickApplyButton(int position) {
        mCallBack.onClickApplyButton(mData.get(position));
      }
    });
  }

  @Override public void onBindViewHolder(@NonNull CouponVH holder, int position) {
    holder.onBindView(mData.get(position));
  }

  @Override public int getItemCount() {
    return mData.size();
  }

  public void setData(List<CouponItem> data) {
    if (mData.size() > 0) {
      mData.clear();
    }
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public interface CallBack {
    void onClickApplyButton(CouponItem couponItem);
  }
}
