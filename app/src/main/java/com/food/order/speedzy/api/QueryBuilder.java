package com.food.order.speedzy.api;

public class QueryBuilder {

  public static String getOrderStatus(String orderId) {

    return "query{\n"
        + "  listOrderStatus(appId:\"speedzy123\",\n"
        + "  orderNumber:\"" + orderId + "\")\n"
        + "  {\n"
        + "    appId\n"
        + "    deliveryGuyId\n"
        + "    orderNumber\n"
        + "    status\n"
        + "    statusMessage\n"
        + "    deliveryGuyMobileNo\n"
        + "    deliveryGuyName\n"
        + "  }\n"
        + "}";
  }

  public static String createOrder(String orderId) {
    return "mutation{\n"
        + "  createOrders(appId:\"speedzy123\",\n"
        + "  orderNumber:\"" + orderId.replaceAll("#", "").trim() + "\",\n"
        + ")\n"
        + "  {\n"
        + "    appId\n"
        + "    orderNumber\n"
        + "  }\n"
        + "}";
  }

  public static String getDeliveryGuyLastKnownLocation(String deliveryGuyId, String orderNumber) {
    return "query{\n"
        + "  listDriverLocation(deliveryGuyId:\"" + deliveryGuyId + "\",\n"
        + "  orderNumber:\"" + orderNumber + "\")\n"
        + "  {\n"
        + "    location\n"
        + "  }\n"
        + "}";
  }
}
