package com.food.order.speedzy.api.response.grocery;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ChildSubcatItem implements Serializable {

  //@SerializedName("parent")
  //private String parent;

  @SerializedName("image")
  private String image;


  @SerializedName("name")
  private String name;

  @SerializedName("id")
  private String id;

  //public String getParent() {
  //  return parent;
  //}

  public String getImage() {
    return image;
  }


  public String getName() {
    return name;
  }

  public String getId() {
    return id;
  }

  @Override
  public String toString() {
    return
        "ChildSubcatItem{" +
            ",image = '" + image + '\'' +
            ",name = '" + name + '\'' +
            ",id = '" + id + '\'' +
            "}";
  }
}