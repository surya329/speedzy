package com.food.order.speedzy.RoomBooking;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.R;

import java.util.List;


public class HotelListRecycleAdapter extends RecyclerView.Adapter<HotelListRecycleAdapter.MyViewHolder>{

    Activity context;

    private List<HotelListModleClass> OfferList;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView imageView;

        public MyViewHolder(View view) {
            super(view);

            title=(TextView)view.findViewById(R.id.title);

            imageView = (ImageView)view.findViewById(R.id.image);
        }

    }


    public HotelListRecycleAdapter(Activity context, List<HotelListModleClass> offerList) {
        this.OfferList = offerList;
        this.context = context;
    }

    @Override
    public HotelListRecycleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hotel_list, parent, false);


        return new HotelListRecycleAdapter.MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final  int position) {
        HotelListModleClass lists = OfferList.get(position);
        holder.imageView.setImageResource(lists.getImage());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, HotelDetailActivity.class);
                context.startActivity(i);
                context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });

    }
    @Override
    public int getItemCount() {
        return OfferList.size();

    }

}


