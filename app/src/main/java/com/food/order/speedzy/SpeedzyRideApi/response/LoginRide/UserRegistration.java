package com.food.order.speedzy.SpeedzyRideApi.response.LoginRide;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class UserRegistration {

	@SerializedName("data")
	private Data data;

	public Data getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"userRegistration{" +
			"data = '" + data + '\'' + 
			"}";
		}
}