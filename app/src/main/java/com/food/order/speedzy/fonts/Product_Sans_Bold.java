package com.food.order.speedzy.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by Sujata Mohanty.
 */


public class Product_Sans_Bold extends AppCompatTextView {
    public Product_Sans_Bold(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public Product_Sans_Bold(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public Product_Sans_Bold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("ProductSansBold.ttf", context);
        setTypeface(customFont);
    }
}