package com.food.order.speedzy.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Activity.PatanjaliProduct_DetailsActivity;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.api.response.grocery.SubCategoriesItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.food.order.speedzy.Utils.Commons.getRandomColor;

/**
 * Created by Sujata Mohanty.
 */

public class Patanjali_ComplexRecyclerViewAdapter
    extends RecyclerView.Adapter<Patanjali_ComplexRecyclerViewAdapter.ViewHolder> {

  Activity mcontext;
  private List<SubCategoriesItem> items;
  String category_id = "";
  public static int sCorner = 15;
  public static int sMargin = 8;

  //public Patanjali_ComplexRecyclerViewAdapter(Activity mcontext, String category_id,
  //    List<Patanjali_Category_model.categories.sub_categories> items) {
  //  this.items = items;
  //  this.category_id = category_id;
  //  this.mcontext = mcontext;
  //}

  public Patanjali_ComplexRecyclerViewAdapter(Activity mcontext) {
    this.mcontext = mcontext;
    this.items = new ArrayList<>();
  }

  @NonNull @Override
  public Patanjali_ComplexRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {
    View v =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.viewhokder_two, parent, false);
    return new ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(Patanjali_ComplexRecyclerViewAdapter.ViewHolder holder,
      int position) {
    //View2Model view2 = (View2Model) items.get(position);
    //holder.ivExample.setImageResource(items.get(position).getSub_Image());
    RequestBuilder<Drawable> thumbnailRequest = Glide
        .with(mcontext)
        .load(items.get(position).getImage())
            .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder));
    Glide.with(mcontext)
        .load(items.get(position).getImage())
            .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder))
            .into(holder.ivExample);
    holder.header.setText(items.get(position).getName());
    holder.footer.setText(items.get(position).getName());
    holder.cardView.setBackgroundTintList(ColorStateList.valueOf(Commons.getRandomColor(mcontext)));
    holder.cardView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (holder.getAdapterPosition() != RecyclerView.NO_POSITION) {
          Intent intent = new Intent(mcontext, PatanjaliProduct_DetailsActivity.class);
          intent.putExtra("app_tittle", "Grocery World");
          intent.putExtra("category_id", category_id);
          intent.putExtra("subcategory_id", items.get(holder.getAdapterPosition()).getId());
          intent.putExtra("child_subcat",
              (Serializable) items.get(holder.getAdapterPosition()).getChildSubcat());
          mcontext.startActivity(intent);
          mcontext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
  }

  public void setCategory_id(String category_id) {
    this.category_id = category_id;
  }

  public void setData(
      List<SubCategoriesItem> itemList) {
    items.addAll(itemList);
    notifyDataSetChanged();
  }

  public void refreshData(
      List<SubCategoriesItem> itemList) {
    items.clear();
    setData(itemList);
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    ImageView ivExample;
    TextView header, footer;
    LinearLayout cardView;

    public ViewHolder(View itemView) {
      super(itemView);
      ivExample = (ImageView) itemView.findViewById(R.id.ivExample);
      header = (TextView) itemView.findViewById(R.id.text1);
      footer = (TextView) itemView.findViewById(R.id.text2);
      cardView = itemView.findViewById(R.id.cardview);
    }
  }
}
