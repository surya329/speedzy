
package com.food.order.speedzy.Model;


import java.io.Serializable;

/**
 * Created by Sujata Mohanty.
 */


public class StPrice implements Serializable {

    private String id;
    private String price_item;
    private String menu_price;
    private boolean isSelected;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPriceItem() {
        return price_item;
    }

    public void setPriceItem(String priceItem) {
        this.price_item = priceItem;
    }

    public String getMenuPrice() {
        return menu_price;
    }

    public void setMenuPrice(String menuPrice) {
        this.menu_price = menuPrice;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}


