package com.food.order.speedzy.Model;

/**
 * Created by Sujata Mohanty.
 */


public class DateModel {
    private String day;

    public String getSent_to_api_date() {
        return sent_to_api_date;
    }

    public void setSent_to_api_date(String sent_to_api_date) {
        this.sent_to_api_date = sent_to_api_date;
    }

    private String sent_to_api_date;

    public DateModel(String day, String date1, String sent_to_api_date) {
        this.day=day;
        this.date1=date1;
        this.sent_to_api_date=sent_to_api_date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    private String date1;
}
