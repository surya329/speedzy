package com.food.order.speedzy.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.food.order.speedzy.Model.OrderDataModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.api.response.OrderVH;
import java.util.ArrayList;
import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderVH> {
  List<OrderDataModel> mData;

  Context context;
  int flag;

  public OrderAdapter(int flag, Context context) {
    mData = new ArrayList<>();
    this.context = context;
    this.flag = flag;
  }

  public void setData(List<OrderDataModel> data) {
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public void refreshData(List<OrderDataModel> data) {
    mData.clear();
    setData(data);
  }

  @NonNull @Override public OrderVH onCreateViewHolder(@NonNull ViewGroup parent, int i) {
    View v = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.orderdetailheader, parent, false);
    return new OrderVH(v);
  }

  @Override public void onBindViewHolder(@NonNull OrderVH orderVH, int position) {
    orderVH.onBindView(mData.get(position));
  }

  @Override public int getItemCount() {
    return mData.size();
  }
}
