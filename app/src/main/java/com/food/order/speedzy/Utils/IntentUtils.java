package com.food.order.speedzy.Utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.core.content.FileProvider;
import com.food.order.speedzy.BuildConfig;
import java.io.File;

/**
 * Created by surya on 22/12/2017.
 */

public final class IntentUtils {

  private static final String PACKAGE_SCHEME = "package";
  private static final String CALL_DIALER_TELEPHONE = "tel:";
  private static final String FILE_PROVIDER_AUTHORITY =
      BuildConfig.APPLICATION_ID + ".fileprovider";
  public static final int REQUEST_IMAGE_CAPTURE = 1122;
  public static final int REQUEST_IMAGE_GALLERY = 1123;

  public static Intent newSettingsIntent(Context context) {
    Intent intent = new Intent();
    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    Uri uri = Uri.fromParts(PACKAGE_SCHEME, context.getPackageName(), null);
    intent.setData(uri);
    return intent;
  }

  public static Intent takePictureIntent(Context context, Uri photoUri) {
    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
      takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
      takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
      return takePictureIntent;
    }
    return null;
  }

  //public static Intent shareImageIntent(Context context, File imageFile, String chooserTitle) {
  //  Uri imageUri;
  //  // if android version 24+ then should be get uri using FileProvider
  //  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
  //    imageUri = FileProvider.getUriForFile(context, FILE_PROVIDER_AUTHORITY, imageFile);
  //  } else {
  //    imageUri = Uri.parse(imageFile.getPath());
  //  }
  //  Intent intent = new Intent(Intent.ACTION_SEND);
  //  intent.setType("image/*");
  //  intent.putExtra(Intent.EXTRA_STREAM, imageUri);
  //  intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
  //  return Intent.createChooser(intent, chooserTitle);
  //}

  public static Intent openAppInPlayStoreIntent(String appPackageName) {
    try {
      return new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
    } catch (android.content.ActivityNotFoundException e) { // if there is no Google Play available on device
      return new Intent(Intent.ACTION_VIEW,
          Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
    }
  }

  public static Intent openGalleryIntent() {
    Intent pickIntent =
        new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    pickIntent.setType("image/*");
    return pickIntent;
  }

  public static Intent callDialerIntent(String phonenumber) {
    return new Intent(Intent.ACTION_VIEW, Uri.parse(CALL_DIALER_TELEPHONE + phonenumber));
  }

  public static Uri getImageUri(Context mContext, File imageFile) {
    Uri imageUri;
    // if android version 24+ then should be get uri using FileProvider
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      imageUri = FileProvider.getUriForFile(mContext, FILE_PROVIDER_AUTHORITY, imageFile);
    } else {
      //imageUri = Uri.parse(imageFile.getPath());
      imageUri = Uri.fromFile(imageFile);
    }
    return imageUri;
  }

  public static Intent newLocationSettingsIntent(Context context) {
    return new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
  }
}
