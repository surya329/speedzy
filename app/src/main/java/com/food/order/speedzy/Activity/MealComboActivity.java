package com.food.order.speedzy.Activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.food.order.speedzy.Adapter.MealComboListActivityAdapter;
import com.food.order.speedzy.Adapter.Patanjali_Product_Adapter;
import com.food.order.speedzy.Model.Cakes_Model;
import com.food.order.speedzy.Model.Combo_Response_Model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.EndlessRecyclerViewScrollListener;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedyLinearLayoutManager;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */


public class MealComboActivity extends AppCompatActivity {
    private Toolbar toolbar;
    RecyclerView combo_recyclerview;
    ImageView no_product;
    MealComboListActivityAdapter mealComboAdapter;
    MySharedPrefrencesData mySharedPrefrencesData;
    String city_id="",Veg_Flag,app_tittle;
    com.food.order.speedzy.fonts.Product_Sans title;
    LoaderDiloag loaderDiloag;
    List<Cakes_Model.Cake> Cakelist = new ArrayList<>();
    int page = 1;
    private EndlessRecyclerViewScrollListener scrollListener;
    boolean apiCallBlocler = false;
    Cakes_Model.Restaurant_timings delivery_time_Cake;
    Cakes_Model.open_close_status open_close_status_Cake;

    LinearLayout  no_available;
    LinearLayout linear1;
    int egg_flag = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_combo);

        loaderDiloag=new LoaderDiloag(MealComboActivity.this);
        mySharedPrefrencesData=new MySharedPrefrencesData();
        city_id=mySharedPrefrencesData.getSelectCity(MealComboActivity.this);
        Veg_Flag=mySharedPrefrencesData.getVeg_Flag(this);
        app_tittle=getIntent().getStringExtra("app_tittle");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title=(com.food.order.speedzy.fonts.Product_Sans) findViewById(R.id.title);
        linear1=findViewById(R.id.linear1);
        no_available=findViewById(R.id.no_available);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title.setText(app_tittle);

        if (Veg_Flag.equalsIgnoreCase("1")){
            statusbar_bg(R.color.red);
        }else {
            statusbar_bg(R.color.red);
        }

        combo_recyclerview = (RecyclerView) findViewById(R.id.combo_recyclerview);
        no_product= (ImageView) findViewById(R.id.no_product);
        if (app_tittle.equalsIgnoreCase("Cakes")){
            callApi_Cakes();
        }else {
            callApi();
        }
    }
    private void callApi_Cakes() {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<Cakes_Model> call =
                methods.getCakes(city_id, String.valueOf(egg_flag), String.valueOf(page));
        Log.d("url", "url=" + call.request().url().toString());
        loaderDiloag.displayDiloag();
        call.enqueue(new Callback<Cakes_Model>() {
            @Override
            public void onResponse(Call<Cakes_Model> call, Response<Cakes_Model> response) {
                int statusCode = response.code();
                apiCallBlocler = true;
                Cakes_Model cakes_model = response.body();
                if (cakes_model != null) {
                    page++;
                    Cakelist = cakes_model.getCakes();
                    delivery_time_Cake = cakes_model.getRestaurant_timings();
                    open_close_status_Cake = cakes_model.getOpen_close_status();
                    if (cakes_model.getApp_openstatus().equalsIgnoreCase("0")) {
                        linear1.setVisibility(View.GONE);
                        no_available.setVisibility(View.VISIBLE);
                    } else {
                        linear1.setVisibility(View.VISIBLE);
                        no_available.setVisibility(View.GONE);
                        if (Cakelist.size() > 0) {
                            combo_recyclerview.setLayoutManager(new SpeedyLinearLayoutManager(MealComboActivity.this, LinearLayoutManager.VERTICAL, false));
                            combo_recyclerview.setNestedScrollingEnabled(false);
                            combo_recyclerview.setVisibility(View.VISIBLE);
                            no_product.setVisibility(View.GONE);
                            mealComboAdapter = new MealComboListActivityAdapter(MealComboActivity.this,Cakelist,delivery_time_Cake,open_close_status_Cake,app_tittle);
                            combo_recyclerview.setAdapter(mealComboAdapter);
                        } else {
                            combo_recyclerview.setVisibility(View.GONE);
                            no_product.setVisibility(View.VISIBLE);
                        }
                        scrollListener_call(3);
                    }
                }
                loaderDiloag.dismissDiloag();
            }

            @Override
            public void onFailure(Call<Cakes_Model> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet",
                        Toast.LENGTH_LONG).show();
                loaderDiloag.dismissDiloag();
            }
        });
    }
    private void scrollListener_call(final int i) {
        scrollListener = new EndlessRecyclerViewScrollListener(new SpeedyLinearLayoutManager(MealComboActivity.this, LinearLayoutManager.VERTICAL, false)) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (apiCallBlocler) {
                    apiCallBlocler = false;
                    if (page > 1) {
                         if (app_tittle.equalsIgnoreCase("Cakes")) {
                            callApi_Cakes();
                        }
                    }
                }
            }
        };
        combo_recyclerview.addOnScrollListener(scrollListener);
    }
    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,color ));
        }
    }

    private void callApi() {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<Combo_Response_Model> call = methods.get_combo_list(city_id,Veg_Flag);
        call.enqueue(new Callback<Combo_Response_Model>() {
            @Override
            public void onResponse(Call<Combo_Response_Model> call, Response<Combo_Response_Model> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                Combo_Response_Model pm = response.body();
                combo_recyclerview.setHasFixedSize(true);
               combo_recyclerview.setLayoutManager(new SpeedyLinearLayoutManager(MealComboActivity.this, LinearLayoutManager.VERTICAL, false));
               combo_recyclerview.setNestedScrollingEnabled(false);
               if (pm.getCombos().size()>0) {
                   combo_recyclerview.setVisibility(View.VISIBLE);
                   no_product.setVisibility(View.GONE);
                   mealComboAdapter = new MealComboListActivityAdapter(MealComboActivity.this, pm.getCombos());
                   combo_recyclerview.setAdapter(mealComboAdapter);
               }else {
                   combo_recyclerview.setVisibility(View.GONE);
                   no_product.setVisibility(View.VISIBLE);
               }
            }
            @Override
            public void onFailure(Call<Combo_Response_Model> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet", Toast.LENGTH_LONG).show();

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Commons.back_button_transition(MealComboActivity.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Commons.back_button_transition(MealComboActivity.this);
    }
}
