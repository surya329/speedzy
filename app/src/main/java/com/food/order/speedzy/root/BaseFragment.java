package com.food.order.speedzy.root;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import com.afollestad.materialdialogs.MaterialDialog;
import com.food.order.speedzy.R;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import java.util.Objects;

/**
 * Created by EKMYonkusumo on 18/12/2017.
 */
public class BaseFragment extends Fragment {

  private Dialog mProgressDialog;
  private MaterialDialog mErrorDialog;
  private boolean mInBackground;
  private CompositeDisposable compositeDisposable;

  @Override public void onResume() {
    super.onResume();
    mInBackground = false;
  }

  @Override public void onPause() {
    super.onPause();
    mInBackground = true;
  }

  @Override public void onDestroy() {
    super.onDestroy();
    mProgressDialog = null;
    mErrorDialog = null;
  }

  protected boolean isInBackground() {
    return mInBackground;
  }

  protected void showProgressDialog(String message) {
    if (mProgressDialog == null) {
      mProgressDialog = new Dialog(Objects.requireNonNull(getContext()));
      mProgressDialog.setContentView(R.layout.loader_layout);
      mProgressDialog.setCancelable(false);
      Objects.requireNonNull(mProgressDialog.getWindow())
          .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
    if (!isInBackground()) {
      mProgressDialog.show();
    }
  }

  protected void showProgressDialog() {
    showProgressDialog(R.string.general_label_pleasewait);
  }

  protected void showProgressDialog(@StringRes int messageResId) {
    showProgressDialog(getString(messageResId));
  }

  protected void dismissProgressDialog() {
    if (mProgressDialog != null && mProgressDialog.isShowing() && !isRemoving()) {
      mProgressDialog.dismiss();
    }
  }

  protected void showErrorDialog() {
    showErrorDialog(R.string.general_label_error);
  }

  protected void showErrorDialog(String message) {
    if (mErrorDialog == null) {
      mErrorDialog =
          new MaterialDialog.Builder(getContext()).title(getString(R.string.general_label_error))
              .content(message)
              .positiveText(getString(R.string.general_label_ok))
              .build();
    } else {
      mErrorDialog.setContent(message);
    }
    if (!isInBackground()) {
      mErrorDialog.show();
    }
  }

  protected void showErrorDialog(@StringRes int messageResId) {
    showErrorDialog(getString(messageResId));
  }

  protected void dismissErrorDialog() {
    if (mErrorDialog != null && mErrorDialog.isShowing() && !isRemoving()) {
      mErrorDialog.dismiss();
    }
  }

  public void addDisposable(Disposable disposable) {
    this.compositeDisposable.add(disposable);
  }
}
