package com.food.order.speedzy.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Fragment.MenuFragment;
import com.food.order.speedzy.Model.Dishitem;
import com.food.order.speedzy.Model.Patanjali_Productdetails_Model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.FoodinnsMediaPlayer;
import com.food.order.speedzy.api.response.home.Alltimings;
import com.food.order.speedzy.api.response.home.ItemsItem;
import com.food.order.speedzy.api.response.home.OpenCloseStatus;

import java.util.ArrayList;
import java.util.List;

public class MenuAdapter  extends RecyclerView.Adapter<MenuAdapter.SingleItemRowHolder> {

    private ArrayList<Dishitem> itemsList;
    private Context mContext;
    PopupWindow popupWindow;
    RecyclerView recyclerView;
    NestedScrollView nestedScrollingView;
    public MenuAdapter(ArrayList<Dishitem> itemsList, Context context, PopupWindow popupWindow, RecyclerView recyclerView,NestedScrollView nestedScrollingView) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.popupWindow=popupWindow;
        this.recyclerView=recyclerView;
        this.nestedScrollingView=nestedScrollingView;
    }

    @NonNull
    @Override
    public MenuAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_item, null);
        return new MenuAdapter.SingleItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MenuAdapter.SingleItemRowHolder holder, int i) {
        Dishitem dishitem=itemsList.get(i);
        holder.itemName.setText(dishitem.getCuisineName());
        if (!dishitem.getDishdetails().isEmpty()) {
            holder.count.setText(""+dishitem.getDishdetails().size());
        }else {
            holder.count.setText("0");
        }
        holder.itemName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyDataSetChanged();
                popupWindow.dismiss();
                final float y = recyclerView.getChildAt(i).getY();
                nestedScrollingView.post(new Runnable() {
                    @Override
                    public void run() {
                        nestedScrollingView.fling(0);
                        nestedScrollingView.smoothScrollTo(0, (int) y);
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {

        return itemsList.size();
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemName, count;

        public SingleItemRowHolder(View view) {
            super(view);
            this.itemName = view.findViewById(R.id.parent_dish_name);
            this.count = view.findViewById(R.id.count);
        }
    }
}
