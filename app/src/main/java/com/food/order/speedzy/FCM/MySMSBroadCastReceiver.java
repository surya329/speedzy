package com.food.order.speedzy.FCM;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * Created by Sujata Mohanty.
 */

public class MySMSBroadCastReceiver extends BroadcastReceiver {
  private static SmsListener mListener;
  String abcd;

  @Override
  public void onReceive(Context context, Intent intent) {
    final Bundle bundle = intent.getExtras();

    try {
      if (bundle != null) {
        final Object[] pdus = (Object[]) bundle.get("pdus");
        for (int i = 0; i < pdus.length; i++) {
          try {
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
            String messageBody = smsMessage.getMessageBody();
            if (messageBody.equalsIgnoreCase("SpeedZy")) {
              abcd = messageBody.replaceAll("[^0-9]", "");
              mListener.messageReceived(abcd);
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
          break;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void bindListener(SmsListener listener) {
    mListener = listener;
  }
}
