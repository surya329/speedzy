package com.food.order.speedzy.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sujata Mohanty.
 */

public class WalletModel {

  @SerializedName("tranid")
  private String tranid;

  @SerializedName("date")
  private String date;

  @SerializedName("amount")
  private String amount;

  @SerializedName("purpose")
  private String purpose;

  @SerializedName("created")
  private String created;

  @SerializedName("type")
  private String type;

  @SerializedName("userid")
  private String userid;

  @SerializedName("current_amt")
  private String currentAmt;

  @SerializedName("currency")
  private String currency;

  @SerializedName("id")
  private String id;

  @SerializedName("time")
  private String time;

  @SerializedName("paytype")
  private String paytype;

  @SerializedName("desc")
  private String desc;

  public String getTranid() {
    return tranid;
  }

  public String getDate() {
    return date;
  }

  public String getAmount() {
    return amount;
  }

  public String getPurpose() {
    return purpose;
  }

  public String getCreated() {
    return created;
  }

  public String getType() {
    return type;
  }

  public String getUserid() {
    return userid;
  }

  public String getCurrentAmt() {
    return currentAmt;
  }

  public String getCurrency() {
    return currency;
  }

  public String getId() {
    return id;
  }

  public String getTime() {
    return time;
  }

  public String getPaytype() {
    return paytype;
  }

  public String getDesc() {
    return desc;
  }

  @Override
  public String toString() {
    return
        "TransactionsItem{" +
            "tranid = '" + tranid + '\'' +
            ",date = '" + date + '\'' +
            ",amount = '" + amount + '\'' +
            ",purpose = '" + purpose + '\'' +
            ",created = '" + created + '\'' +
            ",type = '" + type + '\'' +
            ",userid = '" + userid + '\'' +
            ",current_amt = '" + currentAmt + '\'' +
            ",currency = '" + currency + '\'' +
            ",id = '" + id + '\'' +
            ",time = '" + time + '\'' +
            ",paytype = '" + paytype + '\'' +
            ",desc = '" + desc + '\'' +
            "}";
  }
}
