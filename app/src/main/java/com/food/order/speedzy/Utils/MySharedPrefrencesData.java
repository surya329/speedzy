package com.food.order.speedzy.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.food.order.speedzy.Model.Restaurant_Dish_Model;
import com.food.order.speedzy.Model.Restaurant_model;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Sujata Mohanty.
 */

public class MySharedPrefrencesData {
  private static final String MAPS_API_KEY = "maps_key";

  public void setVeg_Flag(Context mContext, String Veg_Flag) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("Veg_Flag", Veg_Flag);
    editor.commit(); // commit changes
  }

  public String getVeg_Flag(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("Veg_Flag", "");
  }

  public void setLocation(Context mContext, String location) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("Location", location);
    editor.commit(); // commit changes
  }

  public String getLocation(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("Location", "");
  }

  public void setSelectCity(Context mContext, String city) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("SelectCity", city);
    editor.commit(); // commit changes
  }

  public String getSelectCity(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("SelectCity", "");
  }

  public void setSelectCity_longitude(Context mContext, String city) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("SelectCity_longitude", city);
    editor.commit(); // commit changes
  }

  public String getSelectCity_longitude(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("SelectCity_longitude", "");
  }

  public void setSelectCity_latitude(Context mContext, String city) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("SelectCity_latitude", city);
    editor.commit(); // commit changes
  }

  public String getSelectCity_latitude(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("SelectCity_latitude", "");
  }

  public void setSelectAddress_ID(Context mContext, String city) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("SelectAddress_ID", city);
    editor.commit(); // commit changes
  }

  public String getSelectAddress_ID(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("SelectAddress_ID", "");
  }

  public void setSelectName(Context mContext, String Name) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("SelectName", Name);
    editor.commit(); // commit changes
  }

  public String getSelectName(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("SelectName", "");
  }

  public void setSelectPincode(Context mContext, String Pincode) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("SelectPincode", Pincode);
    editor.commit(); // commit changes
  }

  public String getSelectPincode(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("SelectPincode", "");
  }

  public void setSelectAddresstype(Context mContext, String Addresstype) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("SelectAddresstype", Addresstype);
    editor.commit(); // commit changes
  }

  public String getSelectAddresstype(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("SelectAddresstype", "");
  }

  public void setSelectLandMark(Context mContext, String LandMark) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("SelectLandMark", LandMark);
    editor.commit(); // commit changes
  }

  public String getSelectLandMark(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("SelectLandMark", "");
  }

  public void setSelectMobile(Context mContext, String Mobile) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("SelectMobile", Mobile);
    editor.commit(); // commit changes
  }

  public String getSelectMobile(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("SelectMobile", "");
  }

  public void setSelectArea(Context mContext, String Area) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("SelectArea", Area);
    editor.commit(); // commit changes
  }

  public String getSelectArea(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("SelectArea", "");
  }

  public void setLoginStatus(Context mContext, boolean login) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putBoolean("hasLoggedIn", login);
    editor.commit(); // commit changes
  }

  public boolean getLoginStatus(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getBoolean("hasLoggedIn", false);
  }
  public void setRideLoginStatus(Context mContext, boolean login) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putBoolean("hasLoggedInRide", login);
    editor.commit(); // commit changes
  }

  public boolean getRideLoginStatus(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getBoolean("hasLoggedInRide", false);
  }

  public void setLoginType(Context mContext, String login) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("loginType", login);
    editor.commit(); // commit changes
  }

  public String getLoginType(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("loginType", "");
  }

  public void setUser_Id(Context mContext, String user_id) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("userid", user_id);
    editor.apply(); // commit changes
  }

  public String getUser_Id(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("userid", "");
  }

  public void setFName(Context mContext, String party_name) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("Fname", party_name);
    editor.commit(); // commit changes
  }

  public String getFName(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("Fname", "");
  }

  public void setLName(Context mContext, String party_name) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("Lname", party_name);
    editor.commit(); // commit changes
  }

  public String getLName(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("Lname", "");
  }

  public void set_Party_mobile(Context mContext, String party_mobile) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("party_mobile", party_mobile);
    editor.commit(); // commit changes
  }

  public String get_Party_mobile(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("party_mobile", "");
  }

  public void setPartyEmail(Context mContext, String party_email) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("party_email", party_email);
    editor.commit(); // commit changes
  }

  public String getPartyemail(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("party_email", "");
  }

  public void setReferal(Context mContext, String referal) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("referal", referal);
    editor.commit(); // commit changes
  }

  public String getReferal(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("referal", "");
  }

  public void setDeviceId(Context mContext, String party_name) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("DeviceId", party_name);
    editor.commit(); // commit changes
  }

  public String getDeviceId(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString("DeviceId", "");
  }

  public void clearAllSharedData(Context context) {
    SharedPreferences preferences = context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.clear();
    editor.commit();
  }

  public void set_cart_offerArrayList(Context mContext,
      ArrayList<Restaurant_Dish_Model.NewOffers> offerlist) {
    SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPrefs.edit();
    Gson gson = new Gson();
    String json = gson.toJson(offerlist);
    editor.putString("offerlist", json);
    editor.commit();
  }

  public ArrayList<Restaurant_Dish_Model.NewOffers> get_cart_offerArrayList(Context mContext) {
    SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    Gson gson = new Gson();
    String json = sharedPrefs.getString("offerlist", "");
    Type type = new TypeToken<ArrayList<Restaurant_Dish_Model.NewOffers>>() {
    }.getType();
    ArrayList<Restaurant_Dish_Model.NewOffers> arrayList = gson.fromJson(json, type);
    return arrayList;
  }

  /*  public void set_cart_devliverySuburbArrayList(Context mContext, List<Restaurant_Dish_Model_New.DeliveryAreaList> list) {
        SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("devliverySuburbArrayList", json);
        editor.commit();
    }
    public ArrayList<Restaurant_Dish_Model_New.DeliveryAreaList> get_cart_devliverySuburbArrayList(Context mContext) {
        SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPrefs.getString("devliverySuburbArrayList", "");
        Type type = new TypeToken<ArrayList<Restaurant_Dish_Model_New.DeliveryAreaList>>() {}.getType();
        ArrayList<Restaurant_Dish_Model_New.DeliveryAreaList> arrayList = gson.fromJson(json, type);
        return arrayList;
    }*/
  public void set_cart_deliverySchedule(Context mContext, Restaurant_model.DeliverySchedule list) {
    SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPrefs.edit();
    Gson gson = new Gson();
    String json = gson.toJson(list);
    editor.putString("deliverySchedule", json);
    editor.commit();
  }

  public Restaurant_model.DeliverySchedule get_cart_deliverySchedule(Context mContext) {
    SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    Gson gson = new Gson();
    String json = sharedPrefs.getString("deliverySchedule", "");
    Type type = new TypeToken<Restaurant_model.DeliverySchedule>() {
    }.getType();
    Restaurant_model.DeliverySchedule arrayList = gson.fromJson(json, type);
    return arrayList;
  }

  public void set_cart_tom_deliverySchedule(Context mContext,
      Restaurant_model.DeliverySchedule list) {
    SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPrefs.edit();
    Gson gson = new Gson();
    String json = gson.toJson(list);
    editor.putString("tom_deliverySchedule", json);
    editor.commit();
  }

  public Restaurant_model.DeliverySchedule get_cart_tom_deliverySchedule(Context mContext) {
    SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    Gson gson = new Gson();
    String json = sharedPrefs.getString("tom_deliverySchedule", "");
    Type type = new TypeToken<Restaurant_model.DeliverySchedule>() {
    }.getType();
    Restaurant_model.DeliverySchedule arrayList = gson.fromJson(json, type);
    return arrayList;
  }

  public void set_cart_pickupSchedule(Context mContext, Restaurant_model.PickupSchedule list) {
    SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPrefs.edit();
    Gson gson = new Gson();
    String json = gson.toJson(list);
    editor.putString("pickupSchedule", json);
    editor.commit();
  }

  public Restaurant_model.PickupSchedule get_cart_pickupSchedule(Context mContext) {
    SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    Gson gson = new Gson();
    String json = sharedPrefs.getString("pickupSchedule", "");
    Type type = new TypeToken<Restaurant_model.PickupSchedule>() {
    }.getType();
    Restaurant_model.PickupSchedule arrayList = gson.fromJson(json, type);
    return arrayList;
  }

  public void set_cart_tom_pickupSchedule(Context mContext, Restaurant_model.PickupSchedule list) {
    SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPrefs.edit();
    Gson gson = new Gson();
    String json = gson.toJson(list);
    editor.putString("tom_pickupSchedule", json);
    editor.commit();
  }

  public Restaurant_model.PickupSchedule get_cart_tom_pickupSchedule(Context mContext) {
    SharedPreferences sharedPrefs = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    Gson gson = new Gson();
    String json = sharedPrefs.getString("tom_pickupSchedule", "");
    Type type = new TypeToken<Restaurant_model.PickupSchedule>() {
    }.getType();
    Restaurant_model.PickupSchedule arrayList = gson.fromJson(json, type);
    return arrayList;
  }

  public void setpick_del(Context mContext, String party_name) {
    SharedPreferences pref = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("pick_del", party_name);
    editor.commit(); // commit changes
  }

  public String getpick_del(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    return pref.getString("pick_del", "");
  }

  public void setdeliveryfee(Context mContext, String party_name) {
    SharedPreferences pref = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("deliveryfee", party_name);
    editor.commit(); // commit changes
  }

  public String getdeliveryfee(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    return pref.getString("deliveryfee", "");
  }

  public void setordernum(Context mContext, String party_name) {
    SharedPreferences pref = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("ordernum", party_name);
    editor.commit(); // commit changes
  }

  public String getordernum(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    return pref.getString("ordernum", "");
  }

  public void setavg_order_value(Context mContext, String party_name) {
    SharedPreferences pref = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("avg_order_value", party_name);
    editor.commit(); // commit changes
  }

  public String getavg_order_value(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("cart_MyPref", MODE_PRIVATE);
    return pref.getString("avg_order_value", "");
  }

  public void cart_clearAllSharedData(Context context) {
    SharedPreferences preferences =
        context.getSharedPreferences("cart_MyPref", Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.clear();
    editor.commit();
  }

  public void saveMapsKey(Context mContext, String mapsKey) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString(MAPS_API_KEY, mapsKey);
    editor.apply(); // commit changes
  }

  public String getMapsApiKey(Context mContext) {
    SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
    return pref.getString(MAPS_API_KEY, "");
  }
}
