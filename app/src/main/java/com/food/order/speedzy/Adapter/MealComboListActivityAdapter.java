package com.food.order.speedzy.Adapter;

import android.app.Activity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.order.speedzy.Activity.MealComboActivity;
import com.food.order.speedzy.Model.Cakes_Model;
import com.food.order.speedzy.Model.Combo_Response_Model;
import com.food.order.speedzy.R;

import java.util.List;

public class MealComboListActivityAdapter extends RecyclerView.Adapter<MealComboListActivityAdapter.SingleItemRowHolder> {

    List<Combo_Response_Model.combos> itemsList;
    List<Cakes_Model.Cake> cakelist;
    private Activity mContext;
    public static int sCorner = 15;
    public static int sMargin = 8;
    String app_tittle="";
    Cakes_Model.Restaurant_timings delivery_time_Cake;
    Cakes_Model.open_close_status open_close_status_Cake;
    public MealComboListActivityAdapter(Activity context, List<Combo_Response_Model.combos> combos) {
        this.mContext=context;
        this.itemsList=combos;
    }

    public MealComboListActivityAdapter(Activity context, List<Cakes_Model.Cake> cakelist, Cakes_Model.Restaurant_timings delivery_time_Cake, Cakes_Model.open_close_status open_close_status_Cake, String app_tittle) {
        this.mContext=context;
        this.cakelist=cakelist;
        this.app_tittle=app_tittle;
        this.delivery_time_Cake=delivery_time_Cake;
        this.open_close_status_Cake=open_close_status_Cake;
    }

    @Override
    public MealComboListActivityAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.meal_combo_type, null);
        MealComboListActivityAdapter.SingleItemRowHolder mh = new MealComboListActivityAdapter.SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(MealComboListActivityAdapter.SingleItemRowHolder holder, int i) {
        if (app_tittle.equalsIgnoreCase("Cakes")){
            holder.tittle.setVisibility(View.GONE);
            holder.see_all_location.setVisibility(View.GONE);
            holder.mealrecyclerView.setHasFixedSize(true);
            LinearLayoutManager lm = new GridLayoutManager(mContext, 2);
            holder.mealrecyclerView.setLayoutManager(lm);
            holder.mealrecyclerView.setNestedScrollingEnabled(false);
            holder.see_all_ley.setVisibility(View.GONE);
            MealComboActivityAdapter mealComboAdapter = new MealComboActivityAdapter(mContext, cakelist,delivery_time_Cake,open_close_status_Cake,app_tittle);
            holder.mealrecyclerView.setAdapter(mealComboAdapter);
        }else {
            holder.tittle.setText(itemsList.get(i).getComboTitle());
            holder.see_all_location.setVisibility(View.GONE);
            holder.mealrecyclerView.setHasFixedSize(true);
            LinearLayoutManager lm = new GridLayoutManager(mContext, 2);
            holder.mealrecyclerView.setLayoutManager(lm);
            //holder.mealrecyclerView.setLayoutManager(new SpeedyLinearLayoutManager(mContext, SpeedyLinearLayoutManager.HORIZONTAL, false));
            holder.mealrecyclerView.setNestedScrollingEnabled(false);
            holder.see_all_ley.setVisibility(View.GONE);
       /* LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        params.setMargins(10, 30, 10, 30);
        holder.see_all_ley.setLayoutParams(params);
        holder.see_all_ley.requestLayout();*/
            MealComboActivityAdapter mealComboAdapter = new MealComboActivityAdapter(mContext, itemsList.get(i).getItemlist());
            holder.mealrecyclerView.setAdapter(mealComboAdapter);
        }
    }

    @Override
    public int getItemCount() {
        if (app_tittle.equalsIgnoreCase("Cakes")){
            return 1;
        }else {
            return itemsList.size();
        }
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {
        TextView tittle,see_all_location;
        RecyclerView mealrecyclerView;
        RelativeLayout see_all_ley;
        public SingleItemRowHolder(View view) {
            super(view);
            this.tittle = (TextView) itemView.findViewById(R.id.tittle);
            see_all_location = (TextView) itemView.findViewById(R.id.see_all_location);
            this.mealrecyclerView = (RecyclerView) itemView.findViewById(R.id.mealrecyclerView);
            this.see_all_ley=(RelativeLayout) itemView.findViewById(R.id.see_all_ley);
        }
    }
}
