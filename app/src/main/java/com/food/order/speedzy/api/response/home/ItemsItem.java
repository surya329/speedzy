package com.food.order.speedzy.api.response.home;

import com.food.order.speedzy.Model.StPrice;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ItemsItem implements Serializable {

  @SerializedName("imagename")
  private String imagename;

  @SerializedName("imageURL")
  private String imageURL;

  @SerializedName("live_message")
  private String liveMessage;

  @SerializedName("name")
  private String name;

  @SerializedName("rest_id")
  private String restId;

  @SerializedName("nav_type")
  private String navType;

  @SerializedName("menu_ID")
  private String menuID;

  @SerializedName("live_status")
  private String liveStatus;

  public String getMax_qty_peruser() {
    return max_qty_peruser;
  }

  public void setMax_qty_peruser(String max_qty_peruser) {
    this.max_qty_peruser = max_qty_peruser;
  }

  @SerializedName("max_qty_peruser")
  private String max_qty_peruser;

  @SerializedName("combo_id")
  public String comboId;

  public void setComboId(String comboId) {
    this.comboId = comboId;
  }

  public String getIn_dish_id() {
    return in_dish_id;
  }

  public void setIn_dish_id(String in_dish_id) {
    this.in_dish_id = in_dish_id;
  }

  @SerializedName("in_dish_id")
  @Expose
  public String in_dish_id;

  public String getNon_veg_status() {
    return non_veg_status;
  }

  public void setNon_veg_status(String non_veg_status) {
    this.non_veg_status = non_veg_status;
  }

  @SerializedName("non_veg_status")
  @Expose
  public String non_veg_status;

  public String getCoupon_active() {
    return coupon_active;
  }

  public void setCoupon_active(String coupon_active) {
    this.coupon_active = coupon_active;
  }

  @SerializedName("coupon_active")
  @Expose
  public String coupon_active;

  public String getCake_flg() {
    return cake_flg;
  }

  public void setCake_flg(String cake_flg) {
    this.cake_flg = cake_flg;
  }

  @SerializedName("cake_flg")
  @Expose
  public String cake_flg;

  public List<StPrice> getStPrice() {
    return stPrice;
  }

  public void setStPrice(List<StPrice> stPrice) {
    this.stPrice = stPrice;
  }

  @SerializedName("st_price")
  @Expose
  public List<StPrice> stPrice = null;

  public String getComboId() {
    return comboId;
  }

  public void setImagename(String imagename) {
    this.imagename = imagename;
  }

  public String getImagename() {
    return imagename;
  }

  public void setImageURL(String imageURL) {
    this.imageURL = imageURL;
  }

  public String getImageURL() {
    return imageURL;
  }

  public void setLiveMessage(String liveMessage) {
    this.liveMessage = liveMessage;
  }

  public String getLiveMessage() {
    return liveMessage;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setRestId(String restId) {
    this.restId = restId;
  }

  public String getRestId() {
    return restId;
  }

  public void setNavType(String navType) {
    this.navType = navType;
  }

  public String getNavType() {
    return navType;
  }

  public void setMenuID(String menuID) {
    this.menuID = menuID;
  }

  public String getMenuID() {
    return menuID;
  }

  public void setLiveStatus(String liveStatus) {
    this.liveStatus = liveStatus;
  }

  public String getLiveStatus() {
    return liveStatus;
  }

  @Override
  public String toString() {
    return
        "ItemsItem{" +
            "imagename = '" + imagename + '\'' +
            ",imageURL = '" + imageURL + '\'' +
            ",live_message = '" + liveMessage + '\'' +
            ",name = '" + name + '\'' +
            ",rest_id = '" + restId + '\'' +
            ",nav_type = '" + navType + '\'' +
            ",menu_ID = '" + menuID + '\'' +
            ",live_status = '" + liveStatus + '\'' +
            "}";
  }
}