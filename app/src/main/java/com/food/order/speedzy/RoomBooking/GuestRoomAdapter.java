package com.food.order.speedzy.RoomBooking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.R;

import java.util.List;

public class GuestRoomAdapter extends RecyclerView.Adapter<GuestRoomAdapter.MyViewHolder>{

    Context context;
    private List<GuestRoomModel> list;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_adult,txt_child,room_count;
        LinearLayout sub_adult,add_adult,add_child,sub_child;

        public MyViewHolder(View view) {
            super(view);

            room_count=view.findViewById(R.id.room_count);
            add_adult=view.findViewById(R.id.add_adult);
            sub_adult=view.findViewById(R.id.sub_adult);
            add_child=view.findViewById(R.id.add_child);
            sub_child=view.findViewById(R.id.sub_child);
            txt_adult=view.findViewById(R.id.txt_adult);
            txt_child=view.findViewById(R.id.txt_child);
        }

    }


    public GuestRoomAdapter(Context context, List<GuestRoomModel> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public GuestRoomAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.guest_room, parent, false);
        return new GuestRoomAdapter.MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull final GuestRoomAdapter.MyViewHolder holder, final  int position) {
        GuestRoomModel lists = list.get(position);

        holder.add_adult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add(holder.txt_adult,holder.txt_child,position);
            }
        });
        holder.sub_adult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sub(holder.txt_adult,holder.txt_child,position,1);
            }
        });
        holder.add_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add(holder.txt_child,holder.txt_adult,position);
            }
        });
        holder.sub_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sub(holder.txt_child,holder.txt_adult,position,0);
            }
        });
    }

    private void add(TextView textView1, TextView textView2, int position) {
        int s= Integer.parseInt(textView1.getText().toString());
        if (s<2){
            s++;
            textView1.setText(""+s);
        }
        model_update(s,textView1,position);
    }
    private void sub(TextView textView1, TextView textView2, int position, int i) {
        int s= Integer.parseInt(textView1.getText().toString());
        boolean con;
        if (i==1){
            con=(s>1);
        }else {
            con=(s>=1);
        }
        if (con){
            s--;
            textView1.setText(""+s);
        }
        model_update(s,textView1,position);
    }
    private void model_update(int s, TextView textView2, int position) {
        GuestRoomModel product = new GuestRoomModel();
        product.setAdult(String.valueOf(s));
        product.setChild(textView2.getText().toString());
        list.remove(position);
        list.add(position,product);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}



