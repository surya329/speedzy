package com.food.order.speedzy.Adapter;

import android.app.Activity;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Activity.RestarantDetails;
import com.food.order.speedzy.Activity.RestarantDetails_New;
import com.food.order.speedzy.Model.RestaurantModel;
import com.food.order.speedzy.Model.Restaurant_model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class LikedResturantsActivityAdapter1
    extends RecyclerView.Adapter<LikedResturantsActivityAdapter1.ViewHolder> {
  Activity context;
  List<Restaurant_model> restaurant_models;
  String status;
  public static int sCorner = 15;
  public static int sMargin = 8;
  String start_time_lunch_list, end_time_lunch_list, start_time_dinner_list, end_time_dinner_list,
      app_openstatus;

  public LikedResturantsActivityAdapter1(Activity context, List<Restaurant_model> restaurant_models,
      String start_time_lunch_list, String end_time_lunch_list, String start_time_dinner_list,
      String end_time_dinner_list, String app_openstatus) {
    this.context = context;
    this.restaurant_models = restaurant_models;
    this.start_time_lunch_list = start_time_lunch_list;
    this.end_time_lunch_list = end_time_lunch_list;
    this.start_time_dinner_list = start_time_dinner_list;
    this.end_time_dinner_list = end_time_dinner_list;
    this.app_openstatus = app_openstatus;
  }

  @Override
  public LikedResturantsActivityAdapter1.ViewHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {
    View v =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.search_rex_row, parent, false);
    System.out.println("View Holder");
    // set the view's size, margins, paddings and layout parameters
    LikedResturantsActivityAdapter1.ViewHolder vh =
        new LikedResturantsActivityAdapter1.ViewHolder(v);
    return vh;
  }

  @Override
  public void onBindViewHolder(final LikedResturantsActivityAdapter1.ViewHolder holder,
      int position) {
    final Restaurant_model restaurant_model = restaurant_models.get(position);
    String imageurl = restaurant_model.getImage();
    imageurl = imageurl.replace("\\", "/");
    restaurant_model.setImage(imageurl);

    Glide.with(context)
        .load(imageurl)
            .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)))
            .into(holder.itemImage);
    holder.itemTitle.setText(restaurant_model.getStRestaurantName());
    holder.restrating.setText(restaurant_model.getAvg_rating());
    holder.close_open_flag.setText(restaurant_model.getClose_open_st() + " Now");
    holder.menu.setVisibility(View.GONE);
    holder.relative.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Commons.restname = restaurant_model.getStRestaurantName();
        Commons.restadd = restaurant_model.getStreet_address();
        Commons.restsuburb = restaurant_model.getStSuburb();
        Intent in = new Intent(context, RestarantDetails.class);
        in.putExtra("additem",false);
        in.putExtra("menu_id", "");
        in.putExtra("nav_type", "");
        in.putExtra("res_id", restaurant_model.getInRestaurantId());
        in.putExtra("res_name", restaurant_model.getStRestaurantName());
        in.putExtra("res_image", restaurant_model.getImage());
        in.putExtra("res_add", restaurant_model.getStStreetAddress());
        in.putExtra("res_minorder", restaurant_model.getStMinOrder());
        in.putExtra("res_rating", restaurant_model.getAvg_rating());
        in.putExtra("res_reviews", restaurant_model.getCount_rating());
        in.putExtra("Suburb", restaurant_model.getStSuburb());
        in.putExtra("Promotion_msg", restaurant_model.getPromotion_msg());
        in.putExtra("Flg_ac_status", restaurant_model.getFlg_ac_status());
        in.putExtra("postcode", restaurant_model.getStPostcode());
        in.putExtra("booktableparameter", restaurant_model.getAgree_table_booking());
        in.putExtra("deliverytime", (Serializable) restaurant_model.getDeliverySchedule());
        in.putExtra("pickuptime", (Serializable) restaurant_model.getPickupSchedule());
        in.putExtra("tom_deliverytime", (Serializable) restaurant_model.getTom_delivery_schedule());
        in.putExtra("tom_pickuptime", (Serializable) restaurant_model.getTom_pickup_schedule());
        in.putExtra("inner_image_list", (Serializable) restaurant_model.getInnerimg());
        in.putExtra("stclose_open", restaurant_model.getClose_open_st());
        in.putExtra("venuecuisine", restaurant_model.getStVenueCuisine());
        in.putExtra("start_time_lunch", start_time_lunch_list);
        in.putExtra("end_time_lunch", end_time_lunch_list);
        in.putExtra("start_time_dinner", start_time_dinner_list);
        in.putExtra("end_time_dinner", end_time_dinner_list);
        in.putExtra("app_openstatus", app_openstatus);
        in.putExtra("extra_model", transform(restaurant_model));
        Commons.closedtoday = restaurant_model.getClose_open_st();
        Commons.gst_commision = restaurant_model.getStGstCommission();
        if (!(restaurant_model.getStMinOrder() == null)) {
          Commons.min_order_rest = Double.parseDouble(restaurant_model.getStMinOrder());
        }
        Commons.flag_for_hta = "7";
        context.startActivity(in);
        context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      }
    });
  }

  @Override
  public int getItemCount() {
    return restaurant_models.size();
  }

  private RestaurantModel transform(Restaurant_model restaurant_model) {
    String base_url_logo =
        "https://storage.googleapis.com/speedzy_data/resources/restaurantlogo/";
    RestaurantModel obj = new RestaurantModel();
    obj.setmResPic(base_url_logo + "big/" + restaurant_model.getRestaurant_logo_image());
    obj.setmResName(restaurant_model.getStRestaurantName());
    obj.setmRating(restaurant_model.getCount_rating());
    obj.setmRatingBar(Float.parseFloat(restaurant_model.getAvg_rating()));
    obj.setmMsg(restaurant_model.getPromotion_msg());
    obj.setmInRestaurantId(restaurant_model.getInRestaurantId());
    obj.setmStStreetAddress(restaurant_model.getStStreetAddress());
    obj.setmStSuburb(restaurant_model.getStSuburb());
    //obj.setmMsg();

    if (app_openstatus.toString().equalsIgnoreCase("0")) {
      obj.setmOpenClose("Closed");
    } else if (app_openstatus.toString().equalsIgnoreCase("1")) {
      obj.setmOpenClose("Open");
    }
    /*obj.setmOpenClose(
        restaurant_model.getClose_open_st() != null ? restaurant_model.getClose_open_st() : "Open");*/
    obj.setmMinOrder("Min ₹" + restaurant_model.getStMinOrder());
    //if (nav_type.equalsIgnoreCase("7") || menu_id.equalsIgnoreCase("M1014")) {
    //  obj.setmMenu("Book");
    //} else {
    //  obj.setmMenu("Menu");
    //}
    return obj;
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView itemTitle, restrating, close_open_flag, menu;
    ImageView itemImage;
    RelativeLayout relative;

    public ViewHolder(View itemView) {
      super(itemView);
      itemTitle = (TextView) itemView.findViewById(R.id.rex_name);
      restrating = (TextView) itemView.findViewById(R.id.restrating);
      close_open_flag = (TextView) itemView.findViewById(R.id.close_open_flag);
      menu = (TextView) itemView.findViewById(R.id.menu);
      itemImage = (ImageView) itemView.findViewById(R.id.rex_img);
      relative = (RelativeLayout) itemView.findViewById(R.id.relative);
    }
  }
}
