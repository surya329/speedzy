package com.food.order.speedzy.CitySpecial;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.food.order.speedzy.Activity.PatanjaliProductActivity;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.reyclerviewutils.RecyclerViewUtils;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.features.uploadgrocery.UploadGroceryActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CitySpecialActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.my_recycler_view)
    RecyclerView my_recycler_view;
    CitySpecialCategoryAdapter adapter2;
    List<CitySpecialCategoryModel.Category> list=new ArrayList<>();
    LoaderDiloag loaderDiloag;
    String start_time_lunch_list, end_time_lunch_list, start_time_dinner_list, end_time_dinner_list,
            app_openstatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cityspecial);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(
                    ContextCompat.getColor(CitySpecialActivity.this, R.color.red));
        }
        loaderDiloag=new LoaderDiloag(this);
        app_openstatus = getIntent().getStringExtra("app_openstatus");
    start_time_lunch_list = getIntent().getStringExtra("start_time_lunch");
    end_time_lunch_list = getIntent().getStringExtra("end_time_lunch");
    start_time_dinner_list = getIntent().getStringExtra("start_time_dinner");
    end_time_dinner_list = getIntent().getStringExtra("end_time_dinner");
        my_recycler_view.setLayoutManager(RecyclerViewUtils.newLinearVerticalLayoutManager(this));
        my_recycler_view.setNestedScrollingEnabled(false);
        my_recycler_view.setVisibility(View.VISIBLE);
        adapter2 = new CitySpecialCategoryAdapter(CitySpecialActivity.this,
                list,app_openstatus,start_time_lunch_list, end_time_lunch_list, start_time_dinner_list, end_time_dinner_list);
        my_recycler_view.setAdapter(adapter2);
        callApi();
    }

    private void callApi() {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<CitySpecialCategoryModel> call =methods.getcityspecial();
        Log.d("url", "url=" + call.request().url().toString());
        loaderDiloag.displayDiloag();
        call.enqueue(new Callback<CitySpecialCategoryModel>() {
            @Override
            public void onResponse(@NonNull Call<CitySpecialCategoryModel> call,
                                   Response<CitySpecialCategoryModel> response) {
                loaderDiloag.dismissDiloag();
                int statusCode = response.code();
                CitySpecialCategoryModel epm = response.body();
                if (epm != null) {
                    list.clear();
                    list.addAll(epm.getCategories());
                    adapter2.notifyDataSetChanged();
                }
                loaderDiloag.dismissDiloag();
            }

            @Override
            public void onFailure(Call<CitySpecialCategoryModel> call, Throwable t) {
                loaderDiloag.dismissDiloag();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Commons.back_button_transition(CitySpecialActivity.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(CitySpecialActivity.this);
    }
}
