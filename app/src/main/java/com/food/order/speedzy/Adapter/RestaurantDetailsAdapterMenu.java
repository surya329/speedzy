package com.food.order.speedzy.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.food.order.speedzy.Activity.RestarantDetails;
import com.food.order.speedzy.Fragment.MenuFragment;
import com.food.order.speedzy.Model.Cart_Model;
import com.food.order.speedzy.Model.Dishdetail;
import com.food.order.speedzy.Model.Dishitem;
import com.food.order.speedzy.Model.Restaurant_Dish_Model;
import com.food.order.speedzy.Model.Restaurant_Dish_Model_New;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.database.LOcaldbNew;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RestaurantDetailsAdapterMenu extends RecyclerView.Adapter<RestaurantDetailsAdapterMenu.ViewHolder> {


    private Context ctx;
    List<Dishdetail> parentDishArrayList;
    String rid, res_name, res_address, suburb, postcode, pick_del, delivery_fee, avg_order_value, orders_count, mMenuId, flg_ac_status;
    ArrayList<Restaurant_Dish_Model.NewOffers> offerArrayList;
    int i1;
    boolean additem;
    Restaurant_Dish_Model_New.restaurant_details.restcharge restcharge;
    MySharedPrefrencesData mySharedPrefrencesData;
    ArrayList<Cart_Model.Cart_Details> cd;
    List<String> itemlist = new ArrayList<>();
    LOcaldbNew lOcaldbNew;
    String res_image;
    ImageView gifImage;
    public RestaurantDetailsAdapterMenu(ImageView gifImageview,List<Dishdetail> parentDishArrayList, Context ctx, String res_id, String res_name, String res_address, String suburb, String postcode, String pick_del, String delivery_fee, ArrayList<Restaurant_Dish_Model.NewOffers> offerArrayList, String avg_order_value, String orders_count, int i,String mMenuId, boolean additem, String flg_ac_status, Restaurant_Dish_Model_New.restaurant_details.restcharge restcharge,String res_image) {
        this.ctx = ctx;
        this.gifImage=gifImageview;
        this.res_image=res_image;
        this.parentDishArrayList = parentDishArrayList;
        this.rid=res_id;
        this. res_name=res_name;
        this.res_address=res_address;
        this. suburb=suburb;
        this. postcode=postcode;
        this. pick_del=pick_del;
        this. delivery_fee=delivery_fee;
        this. offerArrayList=offerArrayList;
        this. avg_order_value=avg_order_value;
        this. orders_count=orders_count;
        this. i1=i;
        this. mMenuId=mMenuId;
        this. additem=additem;
        this. flg_ac_status=flg_ac_status;
        this. restcharge=restcharge;
    }

    public void updateList(ArrayList<Dishdetail> list) {
        parentDishArrayList = list;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView childdishname,child_text,add,price;

        public RecyclerView pricerecycle;
        public LinearLayout linearLayout,quantity_linear;
        public AppCompatImageView remove_quantity,add_quantity;
        public TextView quantity_text;
        public ViewHolder(View v) {
            super(v);
            childdishname=(TextView)itemView.findViewById(R.id.txt1);
            child_text=(TextView)itemView.findViewById(R.id.child_text);
            price=(TextView)itemView.findViewById(R.id.txt3);
            //       childspecial=(TextView)itemView.findViewById(R.id.child_text1);
            linearLayout=(LinearLayout)itemView.findViewById(R.id.card_view);
            add=(TextView) itemView.findViewById(R.id.add);
            remove_quantity=(AppCompatImageView) itemView.findViewById(R.id.remove_quantity);
            add_quantity=(AppCompatImageView ) itemView.findViewById(R.id.add_quantity);
            quantity_text=(TextView) itemView.findViewById(R.id.quantity_text);
            quantity_linear=(LinearLayout)itemView.findViewById(R.id.quantity_linear);
        }
    }

    @Override
    public RestaurantDetailsAdapterMenu.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_dishes_new, parent, false);
        System.out.println("View Holder");
        // set the view's size, margins, paddings and layout parameters
        lOcaldbNew = new LOcaldbNew(ctx);
        mySharedPrefrencesData = new MySharedPrefrencesData();
        if (mMenuId.equals(SpeedzyConstants.FRUITS_MENU_ID)) {
            cd = lOcaldbNew.getFruitCart_Details();
            itemlist.clear();
            for (int i = 0; i < cd.size(); i++) {
                if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id())) {
                    itemlist.add(cd.get(i).getIn_dish_id());
                }
            }
        } else {
            cd = lOcaldbNew.getCart_Details();
            itemlist.clear();
            for (int i = 0; i < cd.size(); i++) {
                if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id())) {
                    itemlist.add(cd.get(i).getIn_dish_id());
                }
            }
        }
        RestaurantDetailsAdapterMenu.ViewHolder vh = new RestaurantDetailsAdapterMenu.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RestaurantDetailsAdapterMenu.ViewHolder holder, final int position) {
        Dishdetail dishdetail=parentDishArrayList.get(position);
                String flagaddchoice = dishdetail.getFlgAddChoices();
        Commons.restaurant_id = rid;
        if (flagaddchoice.equalsIgnoreCase("0")) {
            holder.childdishname.setText(dishdetail.getStDishName());
        } else {
            holder.childdishname.setText(dishdetail.getStDishName() + " " + '*');
        }

        if (itemlist.contains(dishdetail.getInDishId())) {
            int index = itemlist.indexOf(dishdetail.getInDishId());
            int sqty = Integer.parseInt(cd.get(index).getQuantity());
            if (sqty > 0) {
                holder.quantity_linear.setVisibility(View.VISIBLE);
                holder.add.setVisibility(View.GONE);
                holder.quantity_text.setText(String.valueOf(sqty));
            }
        } else {
            holder.quantity_linear.setVisibility(View.GONE);
            holder.add.setVisibility(View.VISIBLE);
        }
            /*for (int i=0;i<cd.size();i++){
                if (dishdetail.getInDishId().equalsIgnoreCase(cd.get(i).getIn_dish_id())) {
                    int sqty = Integer.parseInt(cd.get(i).getQuantity());
                    if (sqty > 0) {
                        holder.quantity_linear.setVisibility(View.VISIBLE);
                        holder.add.setVisibility(View.GONE);
                        holder.quantity_text.setText(String.valueOf(sqty));
                    }
                }
            }*/

        if (dishdetail.getStDishName().isEmpty()) {
            holder.linearLayout.setVisibility(View.GONE);
            holder.child_text.setText(dishdetail.getDishDesription());
        } else {
            holder.linearLayout.setVisibility(View.VISIBLE);
            holder.child_text.setText(dishdetail.getDishDesription());
            holder.price.setText("₹" + dishdetail.getStPrice().get(0).getMenuPrice());
        }

            holder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mMenuId.equals(SpeedzyConstants.FRUITS_MENU_ID)||
                            Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)) {
                       final boolean status = lOcaldbNew.checkIfCartContainsSomeOtherFruitData(rid);
                       /*  if (!status) {
                            addFruititem(dishdetail, holder);
                        } else {
                            delete_Fruit_cart_dialog(dishdetail, holder);
                        }*/
                        addFruititem(dishdetail, holder);
                    } else {
                        final boolean status = lOcaldbNew.checkIfCartContainsSomeOtherRestaurantData(rid);
                        if (!status) {
                            additem(dishdetail, holder);
                        } else {
                            delete_cart_dialog(dishdetail, holder);
                        }
                    }
                }
            });

            holder.add_quantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mMenuId.equals(SpeedzyConstants.FRUITS_MENU_ID)||
                            Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)) {
                        addFruititemupdatecart(dishdetail, holder);
                    } else {
                        additemupdatecart(dishdetail, holder);
                    }
                }
            });

            holder.remove_quantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mMenuId.equals(SpeedzyConstants.FRUITS_MENU_ID)||
                            Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)) {
                        removeFruitItem(dishdetail, holder);
                    } else {
                        removeitem(dishdetail, holder);
                    }
                }
            });

        if (dishdetail.getDishDesription() == null || dishdetail.getDishDesription().isEmpty()) {
            holder.child_text.setVisibility(View.GONE);
        } else {
            holder.child_text.setText(dishdetail.getDishDesription());
            holder.child_text.setVisibility(View.VISIBLE);
        }

        if (dishdetail.getNon_veg_status().equalsIgnoreCase("1")) {
            holder.childdishname.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_nonveg_item_indicator, 0, 0, 0);
        } else {
            holder.childdishname.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_veg_item_indicator,
                    0, 0, 0);
        }

        if (!additem) {
            if (flg_ac_status.equalsIgnoreCase("2")) {
                holder.add.setVisibility(View.GONE);
                holder.quantity_linear.setVisibility(View.GONE);
            }
        }
        if (gifImage!=null && position==(getItemCount()-1)){
            gifImage.setVisibility(View.GONE);
        }
    }
    private void removeFruitItem(Dishdetail dishdetail, RestaurantDetailsAdapterMenu.ViewHolder holder) {
        ArrayList<Cart_Model.Cart_Details> cd =
                lOcaldbNew.getFruitCart_Details(/*Commons.restaurant_id*/);
        for (int i = 0; i < cd.size(); i++) {
            if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id()) && cd.get(i)
                    .getIn_dish_id()
                    .equalsIgnoreCase(dishdetail.getInDishId())) {
                int qty = Integer.parseInt(holder.quantity_text.getText().toString());
                if (qty > 1) {
                    qty--;
                    cd.get(i).setQuantity(String.valueOf(qty));
                    lOcaldbNew.updateCart(String.valueOf(qty), cd.get(i).getIn_dish_id());
                    holder.quantity_linear.setVisibility(View.VISIBLE);
                    holder.quantity_text.setText(String.valueOf(qty));
                } else {
                    lOcaldbNew.deleteFruitcart(cd.get(i).getIn_dish_id());
                    holder.quantity_linear.setVisibility(View.GONE);
                    holder.add.setVisibility(View.VISIBLE);
                }
            }
        }
        RestarantDetails.setPriceDetails(lOcaldbNew.getFruitCart_Details(),
                lOcaldbNew.getFruitQuantity(Commons.restaurant_id));
    }

    private void addFruititemupdatecart(Dishdetail dishdetail, RestaurantDetailsAdapterMenu.ViewHolder holder) {
        ArrayList<Cart_Model.Cart_Details> cd =
                lOcaldbNew.getFruitCart_Details();
        for (int i = 0; i < cd.size(); i++) {
            if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id()) && cd.get(i)
                    .getIn_dish_id()
                    .equalsIgnoreCase(dishdetail.getInDishId())) {
                int qty = Integer.parseInt(holder.quantity_text.getText().toString());
                int Max_qty_peruser = 100;
                if (!GeneralUtil.isStringEmpty(dishdetail.getMax_qty_peruser())) {
                    Max_qty_peruser = Integer.parseInt(dishdetail.getMax_qty_peruser());
                }
                if (qty < Max_qty_peruser) {
                    qty++;
                    cd.get(i).setQuantity(String.valueOf(qty));
                    lOcaldbNew.updateFruitCart(String.valueOf(qty), cd.get(i).getIn_dish_id());
                    holder.quantity_linear.setVisibility(View.VISIBLE);
                    holder.quantity_text.setText(String.valueOf(qty));
                }else {
                    Toast.makeText(ctx.getApplicationContext(),"Maximum "+Max_qty_peruser+" Items Added",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
        RestarantDetails.setPriceDetails(lOcaldbNew.getFruitCart_Details(),
                lOcaldbNew.getFruitQuantity(Commons.restaurant_id));
    }   

    private void delete_cart_dialog(final Dishdetail dishdetail, final RestaurantDetailsAdapterMenu.ViewHolder holder) {
        final BottomSheetDialog dialog_msg = new BottomSheetDialog(ctx, R.style.SheetDialog);
        Objects.requireNonNull(dialog_msg.getWindow())
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_msg.setContentView(R.layout.layout_discard_cart);
        //TextView msg = (TextView) dialog_msg.findViewById(R.id.warning);
        TextView yes = (TextView) dialog_msg.findViewById(R.id.txt_yes);
        TextView no = (TextView) dialog_msg.findViewById(R.id.txt_no);
        ImageView imgClose = dialog_msg.findViewById(R.id.img_close);
        //msg.setText(
        //    "Your Cart has item(s) from other resturants.Do you want to discard it and add item(s) from this resturants?");
        if (no != null) {
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_msg.dismiss();
                }
            });
        }

        if (imgClose != null) {
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    dialog_msg.dismiss();
                }
            });
        }
        if (yes != null) {
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_msg.dismiss();
                    lOcaldbNew.deleteCart();
                    mySharedPrefrencesData.cart_clearAllSharedData(ctx);
                    additem(dishdetail, holder);
                }
            });
        }
        dialog_msg.show();
    }

    private void additemupdatecart(Dishdetail dishdetail, RestaurantDetailsAdapterMenu.ViewHolder holder) {
        ArrayList<Cart_Model.Cart_Details> cd = lOcaldbNew.getCart_Details(/*Commons.restaurant_id*/);
        for (int i = 0; i < cd.size(); i++) {
            if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id()) &&
                    cd.get(i).getIn_dish_id().equalsIgnoreCase(dishdetail.getInDishId())) {
                int qty = Integer.parseInt(holder.quantity_text.getText().toString());
                int Max_qty_peruser = 100;
                if (!GeneralUtil.isStringEmpty(dishdetail.getMax_qty_peruser())) {
                    Max_qty_peruser = Integer.parseInt(dishdetail.getMax_qty_peruser());
                }
                if (qty < Max_qty_peruser) {
                    qty++;
                    cd.get(i).setQuantity(String.valueOf(qty));
                    lOcaldbNew.updateCart(String.valueOf(qty), cd.get(i).getIn_dish_id());
                    holder.quantity_linear.setVisibility(View.VISIBLE);
                    holder.quantity_text.setText(String.valueOf(qty));
                }else {
                    Toast.makeText(ctx.getApplicationContext(),"Maximum "+Max_qty_peruser+" Items Added",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (!additem) {
            RestarantDetails.setPriceDetails(lOcaldbNew.getCart_Details(/*Commons.restaurant_id*/),
                    lOcaldbNew.getQuantity(Commons.restaurant_id));
        }
    }

    private void delete_Fruit_cart_dialog(final Dishdetail dishdetail, final RestaurantDetailsAdapterMenu.ViewHolder holder) {
        final Dialog dialog_msg = new Dialog(ctx, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog_msg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_msg.setContentView(R.layout.logout_popup);
        TextView msg = (TextView) dialog_msg.findViewById(R.id.warning);
        TextView yes = (TextView) dialog_msg.findViewById(R.id.yes);
        TextView no = (TextView) dialog_msg.findViewById(R.id.no);
        msg.setText(
                "Your Cart has item(s) from other resturants.Do you want to discard it and add item(s) from this resturants?");
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_msg.dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_msg.dismiss();
                lOcaldbNew.deleteCart();
                mySharedPrefrencesData.cart_clearAllSharedData(ctx);
                addFruititem(dishdetail, holder);
            }
        });
        dialog_msg.show();
    }

    private void removeitem(Dishdetail dishdetail, RestaurantDetailsAdapterMenu.ViewHolder holder) {
        ArrayList<Cart_Model.Cart_Details> cd = lOcaldbNew.getCart_Details(/*Commons.restaurant_id*/);
        for (int i = 0; i < cd.size(); i++) {
            if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id()) && cd.get(i)
                    .getIn_dish_id()
                    .equalsIgnoreCase(dishdetail.getInDishId())) {
                int qty = Integer.parseInt(holder.quantity_text.getText().toString());
                if (qty > 1) {
                    qty--;
                    cd.get(i).setQuantity(String.valueOf(qty));
                    lOcaldbNew.updateCart(String.valueOf(qty), cd.get(i).getIn_dish_id());
                    holder.quantity_linear.setVisibility(View.VISIBLE);
                    holder.quantity_text.setText(String.valueOf(qty));
                } else {
                    lOcaldbNew.deletecart(cd.get(i).getIn_dish_id());
                    holder.quantity_linear.setVisibility(View.GONE);
                    holder.add.setVisibility(View.VISIBLE);
                }
            }
        }
        if (!additem) {
            RestarantDetails.setPriceDetails(lOcaldbNew.getCart_Details(/*Commons.restaurant_id*/),
                    lOcaldbNew.getQuantity(Commons.restaurant_id));
        }
    }

    private void additem(Dishdetail dishdetail, RestaurantDetailsAdapterMenu.ViewHolder holder) {
        mySharedPrefrencesData.setpick_del(ctx, pick_del);
        mySharedPrefrencesData.setdeliveryfee(ctx, delivery_fee);
        mySharedPrefrencesData.setordernum(ctx, orders_count);
        mySharedPrefrencesData.setavg_order_value(ctx, avg_order_value);
        //mySharedPrefrencesData.set_cart_devliverySuburbArrayList(ctx,devliverySuburbArrayList);
        mySharedPrefrencesData.set_cart_offerArrayList(ctx, offerArrayList);

        String menupice;
        if (dishdetail.getStPrice().get(0).getMenuPrice().contains(",")) {
            menupice = dishdetail.getStPrice().get(0).getMenuPrice().replace(",", "");
        } else {
            menupice = dishdetail.getStPrice().get(0).getMenuPrice();
        }
        NumberFormat f = NumberFormat.getInstance();
        float dvp = 0;
        try {
            dvp = f.parse(menupice).floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArrayList<Cart_Model.Cart_Details> cd =
                lOcaldbNew.checkIfCartContainsSameDishWithPreference(dishdetail.getInDishId(), "");
        if (cd.size() > 0) {
            int sqty = Integer.parseInt(cd.get(0).getQuantity());
            sqty = sqty + 1;
            lOcaldbNew.deletecart(dishdetail.getInDishId());
            lOcaldbNew.insertTable_dish_info(rid, dishdetail.getInDishId(), dishdetail.getStDishName(),
                    dishdetail.getStPrice().get(0).getPriceItem(), dvp,
                    "1", dishdetail.getNon_veg_status(), dishdetail.getCake_flg(), sqty, Integer.parseInt(dishdetail.getMax_qty_peruser()),"", 0, res_name,
                    res_address, suburb, postcode,restcharge.getAmount(),restcharge.getDesc(),res_image);
        } else {
            lOcaldbNew.insertTable_dish_info(rid, dishdetail.getInDishId(), dishdetail.getStDishName(),
                    dishdetail.getStPrice().get(0).getPriceItem(), dvp,
                    "1", dishdetail.getNon_veg_status(), dishdetail.getCake_flg(), 1,  Integer.parseInt(dishdetail.getMax_qty_peruser()),"", 0, res_name,
                    res_address, suburb, postcode,restcharge.getAmount(),restcharge.getDesc(),res_image);
        }
        holder.add.setVisibility(View.GONE);
        holder.quantity_linear.setVisibility(View.VISIBLE);
        holder.quantity_text.setText("1");
        //holder.add.setVisibility(View.VISIBLE);
        //Toast.makeText(ctx, dishdetail.getStDishName() + " added", Toast.LENGTH_LONG).show();
   /* if (Commons.flag_for_hta.equals("7") && !additem) {
      ArrayList<Cart_Model.Cart_Details> cd1 =
          lOcaldbNew.getCart_Details();
      menuFragment.setPriceDetails(cd1, lOcaldbNew.getQuantity(Commons.restaurant_id));
    }*/
        ArrayList<Cart_Model.Cart_Details> cd1 =
                lOcaldbNew.getCart_Details();
        RestarantDetails.setPriceDetails(cd1, lOcaldbNew.getQuantity(Commons.restaurant_id));
    }

    private void addFruititem(Dishdetail dishdetail, RestaurantDetailsAdapterMenu.ViewHolder holder) {
        mySharedPrefrencesData.setpick_del(ctx, pick_del);
        mySharedPrefrencesData.setdeliveryfee(ctx, delivery_fee);
        mySharedPrefrencesData.setordernum(ctx, orders_count);
        mySharedPrefrencesData.setavg_order_value(ctx, avg_order_value);
        //mySharedPrefrencesData.set_cart_devliverySuburbArrayList(ctx,devliverySuburbArrayList);
        mySharedPrefrencesData.set_cart_offerArrayList(ctx, offerArrayList);

        String menupice;
        if (dishdetail.getStPrice().get(0).getMenuPrice().contains(",")) {
            menupice = dishdetail.getStPrice().get(0).getMenuPrice().replace(",", "");
        } else {
            menupice = dishdetail.getStPrice().get(0).getMenuPrice();
        }
        NumberFormat f = NumberFormat.getInstance();
        float dvp = 0;
        try {
            dvp = f.parse(menupice).floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        lOcaldbNew.insertTable_Fruit_info(rid, dishdetail.getInDishId(), dishdetail.getStDishName(),
                dishdetail.getStPrice().get(0).getPriceItem(), dvp,
                "1", dishdetail.getNon_veg_status(), dishdetail.getCake_flg(), 1, Integer.parseInt(dishdetail.getMax_qty_peruser()),"", 0, res_name,
                res_address, suburb, postcode,res_image);
        holder.add.setVisibility(View.GONE);
        holder.quantity_linear.setVisibility(View.VISIBLE);
        holder.quantity_text.setText("1");
        //holder.add.setVisibility(View.VISIBLE);
        //Toast.makeText(ctx, dishdetail.getStDishName() + " added", Toast.LENGTH_LONG).show();
        ArrayList<Cart_Model.Cart_Details> cd1 =
                lOcaldbNew.getFruitCart_Details();
        RestarantDetails.setPriceDetails(cd1, lOcaldbNew.getFruitQuantity(Commons.restaurant_id));
    }

    @Override
    public int getItemCount() {
        return parentDishArrayList.size();
    }
}

   

