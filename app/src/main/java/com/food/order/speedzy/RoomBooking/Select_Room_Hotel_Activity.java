package com.food.order.speedzy.RoomBooking;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;

import java.util.ArrayList;

public class Select_Room_Hotel_Activity extends AppCompatActivity {


    private ArrayList<SelectRoomModelClass> selectRoomModelClasses;
    private RecyclerView recyclerView;
    private SelectRoomRecycleAdapter mAdapter;
    ImageView back;

    Integer image[] = {R.drawable.hotel_list12,R.drawable.hotel_list12};
    String title[] = {"Classic Room","King Size Room"};
    String confirmation[]={"Free Cancelation, Sunrise Check-in Available","Non Refundable"};
    Integer pay_img[] = {R.drawable.ic_bill,R.drawable.ic_card};
    String payment[] = {"Pay at Hotel","Pay Now"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select__room__hotel);

        statusbar_bg(R.color.red);
        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView = findViewById(R.id.recyclerView);

        selectRoomModelClasses = new ArrayList<>();


        for (int i = 0; i < image.length; i++) {
            SelectRoomModelClass beanClassForRecyclerView_contacts = new SelectRoomModelClass(image[i],title[i],confirmation[i],pay_img[i],payment[i]);

            selectRoomModelClasses.add(beanClassForRecyclerView_contacts);
        }


        mAdapter = new SelectRoomRecycleAdapter(Select_Room_Hotel_Activity.this,selectRoomModelClasses);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Select_Room_Hotel_Activity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }
    private void statusbar_bg(int color) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(this);
    }
}
