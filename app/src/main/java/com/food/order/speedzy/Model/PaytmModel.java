package com.food.order.speedzy.Model;

import java.io.Serializable;

/**
 * Created by admin on 29-11-2018.
 */

public class PaytmModel implements Serializable {
    private String CHECKSUMHASH;
    private String ORDER_ID;

    public String getCHECKSUMHASH() {
        return CHECKSUMHASH;
    }

    public void setCHECKSUMHASH(String CHECKSUMHASH) {
        this.CHECKSUMHASH = CHECKSUMHASH;
    }

    public String getORDER_ID() {
        return ORDER_ID;
    }

    public void setORDER_ID(String ORDER_ID) {
        this.ORDER_ID = ORDER_ID;
    }

    public String getPayt_STATUS() {
        return payt_STATUS;
    }

    public void setPayt_STATUS(String payt_STATUS) {
        this.payt_STATUS = payt_STATUS;
    }

    private String payt_STATUS;
}
