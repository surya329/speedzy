package com.food.order.speedzy.RoomBooking;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.google.android.material.bottomsheet.BottomSheetDialog;

public class ReviewBookingActivity extends AppCompatActivity {

    TextView proceed;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_booking);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        statusbar_bg(R.color.red);

        proceed = (TextView)findViewById(R.id.proceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_dialog_show();
            }
        });
    }
    private void payment_dialog_show() {
        BottomSheetDialog dialog_payment = new BottomSheetDialog(this, R.style.SheetDialog);
        dialog_payment.setContentView(R.layout.payment_ride);
        dialog_payment.setCanceledOnTouchOutside(true);
        LinearLayout cash =dialog_payment.findViewById(R.id.cash);
        LinearLayout bhim =  dialog_payment.findViewById(R.id.bhim);
        LinearLayout phonepe =  dialog_payment.findViewById(R.id.phonepe);
        LinearLayout paytm =  dialog_payment.findViewById(R.id.paytm);
        LinearLayout gpay =  dialog_payment.findViewById(R.id.google_pay);
        cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Succssful_activity(dialog_payment);
            }
        });
        bhim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Succssful_activity(dialog_payment);
            }
        });
        phonepe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Succssful_activity(dialog_payment);
            }
        });
        paytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Succssful_activity(dialog_payment);
            }
        });
        gpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Succssful_activity(dialog_payment);
            }
        });
        dialog_payment.show();
    }

    private void Succssful_activity(BottomSheetDialog dialog_payment) { 
        Intent i = new Intent(ReviewBookingActivity.this, BookingSucessfullActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        dialog_payment.dismiss();
    }
    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(this);
    }
}
