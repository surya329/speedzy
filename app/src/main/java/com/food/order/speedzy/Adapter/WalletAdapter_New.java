package com.food.order.speedzy.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.food.order.speedzy.Model.WalletModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by admin on 30-11-2018.
 */

public class WalletAdapter_New extends RecyclerView.Adapter<WalletAdapter_New.ViewHolder> {
  Context context;
  List<WalletModel> walletModelArrayList;

  public WalletAdapter_New(Context context, List<WalletModel> walletModelArrayList) {
    this.context = context;
    this.walletModelArrayList = walletModelArrayList;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v =
        LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_inflate_wallet, parent, false);
    // set the view's size, margins, paddings and layout parameters
    WalletAdapter_New.ViewHolder vh = new WalletAdapter_New.ViewHolder(v);
    return vh;
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    WalletModel walletModel = walletModelArrayList.get(position);
    holder.transaction.setText(walletModel.getPurpose());
    //holder.transactions.setText("₹" + walletModel.getAmount());
    holder.amount.setText("Closing balance: " + "₹" + walletModel.getCurrentAmt());
    String string = walletModel.getDate();
    DateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
    try {
      Date date = dateFormatter1.parse(string);
      holder.time.setText("" + format.format(date));
    } catch (ParseException e) {
      e.printStackTrace();
    }
    //if (walletModel.getPaytype().equalsIgnoreCase("0")) {
    //  holder.transactions.setTextColor(context.getResources().getColor(R.color.red));
    //} else if (walletModel.getPaytype().equalsIgnoreCase("1")) {
    //  holder.transactions.setTextColor(context.getResources().getColor(R.color.red));
    //}

    switch (Integer.parseInt(walletModel.getType())) {
      case SpeedzyConstants
          .WalletType.ADMIN_ADDED:
        holder.transactions.setTextColor(context.getResources().getColor(R.color.red));
        holder.transactions.setText("₹" + walletModel.getAmount());
        holder.imgProfile.setImageResource(R.mipmap.ic_launcher);
        break;
      case SpeedzyConstants
          .WalletType.ORDER_CASHBACK:
        holder.transactions.setTextColor(context.getResources().getColor(R.color.price_green));
        holder.transactions.setText("+ ₹" + walletModel.getAmount());
        holder.imgProfile.setImageResource(R.mipmap.ic_launcher);
        break;
      case SpeedzyConstants
          .WalletType.ORDER_FOOD:
        holder.transactions.setTextColor(context.getResources().getColor(R.color.red2));
        holder.transactions.setText("- ₹" + walletModel.getAmount());
        holder.imgProfile.setImageResource(R.drawable.food_icon);
        break;
      default:
        holder.transactions.setTextColor(context.getResources().getColor(R.color.black));
        holder.transactions.setText("₹" + walletModel.getAmount());
        holder.imgProfile.setImageResource(R.mipmap.ic_launcher);
        break;
    }
  }

  @Override
  public int getItemCount() {
    return walletModelArrayList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView time, transaction, transactions, amount;
    ImageView imgProfile;

    public ViewHolder(View itemView) {
      super(itemView);
      time = (TextView) itemView.findViewById(R.id.tvTime);
      transaction = (TextView) itemView.findViewById(R.id.tvTransaction);
      transactions = (TextView) itemView.findViewById(R.id.tvAmount);
      amount = (TextView) itemView.findViewById(R.id.tvClosingBalance);
      imgProfile = itemView.findViewById(R.id.imgProfile);
    }
  }
}
