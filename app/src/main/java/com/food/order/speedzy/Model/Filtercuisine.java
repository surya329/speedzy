package com.food.order.speedzy.Model;

import java.io.Serializable;

/**
 * Created by Sujata Mohanty.
 */


public class Filtercuisine implements Serializable {

    private String st_venue_cuisine;
    private String st_pkup_del;
    private String cuisine;
    private boolean isSelected=false;


    public String getSt_venue_cuisine() {
        return st_venue_cuisine;
    }

    public void setSt_venue_cuisine(String st_venue_cuisine) {
        this.st_venue_cuisine = st_venue_cuisine;
    }

    public String getSt_pkup_del() {
        return st_pkup_del;
    }

    public void setSt_pkup_del(String st_pkup_del) {
        this.st_pkup_del = st_pkup_del;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}