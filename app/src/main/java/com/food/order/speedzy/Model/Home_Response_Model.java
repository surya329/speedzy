package com.food.order.speedzy.Model;

import com.food.order.speedzy.api.response.home.AppclosestatusItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class Home_Response_Model implements Serializable {

  public List<Home_Response_Model.sections> getSections() {
    return sections;
  }

  public void setSections(List<Home_Response_Model.sections> sections) {
    this.sections = sections;
  }

  private List<sections> sections = null;

  public Home_Response_Model.alltimings getAlltimings() {
    return alltimings;
  }

  public void setAlltimings(Home_Response_Model.alltimings alltimings) {
    this.alltimings = alltimings;
  }

  private alltimings alltimings;

  public Home_Response_Model.open_close_status getOpen_close_status() {
    return open_close_status;
  }

  public void setOpen_close_status(Home_Response_Model.open_close_status open_close_status) {
    this.open_close_status = open_close_status;
  }

  @SerializedName("appclosestatus")
  private List<AppclosestatusItem> appclosestatus;
  private open_close_status open_close_status;

  public List<AppclosestatusItem> getAppclosestatus() {
    return appclosestatus;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @SerializedName("app-version")
  @Expose
  public String app_version;

  public String getApp_version() {
    return app_version;
  }

  public void setApp_version(String app_version) {
    this.app_version = app_version;
  }

  public String getUpdate_required() {
    return update_required;
  }

  public void setUpdate_required(String update_required) {
    this.update_required = update_required;
  }

  @SerializedName("update-required")
  @Expose
  private String update_required;
  private String status;

  public List<String> getEndpoints() {
    return endpoints;
  }

  public void setEndpoints(List<String> endpoints) {
    this.endpoints = endpoints;
  }

  private List<String> endpoints = null;

  public String getImage_baseURL() {
    return image_baseURL;
  }

  public void setImage_baseURL(String image_baseURL) {
    this.image_baseURL = image_baseURL;
  }

  private String image_baseURL;

  public class sections implements Serializable {
    public String getSection_type() {
      return section_type;
    }

    public void setSection_type(String section_type) {
      this.section_type = section_type;
    }

    private String section_type;
    private String width;

    public String getWidth() {
      return width;
    }

    public void setWidth(String width) {
      this.width = width;
    }

    public String getHeight() {
      return height;
    }

    public void setHeight(String height) {
      this.height = height;
    }

    private String height;
    private String title;
    private List<items> items = null;

    public List<Home_Response_Model.sections.items> getItems() {
      return items;
    }

    public void setItems(List<Home_Response_Model.sections.items> items) {
      this.items = items;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public class items implements Serializable {

      @SerializedName("name")
      @Expose
      public String name;
      @SerializedName("rest_id")
      @Expose
      public String restId;
      @SerializedName("nav_type")
      @Expose
      public String navType;
      @SerializedName("menu_ID")
      @Expose
      public String menuID;
      @SerializedName("imagename")
      @Expose
      public String imagename;
      @SerializedName("imageURL")
      @Expose
      public String imageURL;
      @SerializedName("live_status")
      @Expose
      public String liveStatus;

      public String getName() {
        return name;
      }

      public void setName(String name) {
        this.name = name;
      }

      public String getRestId() {
        return restId;
      }

      public void setRestId(String restId) {
        this.restId = restId;
      }

      public String getNavType() {
        return navType;
      }

      public void setNavType(String navType) {
        this.navType = navType;
      }

      public String getMenuID() {
        return menuID;
      }

      public void setMenuID(String menuID) {
        this.menuID = menuID;
      }

      public String getImagename() {
        return imagename;
      }

      public void setImagename(String imagename) {
        this.imagename = imagename;
      }

      public String getImageURL() {
        return imageURL;
      }

      public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
      }

      public String getLiveStatus() {
        return liveStatus;
      }

      public void setLiveStatus(String liveStatus) {
        this.liveStatus = liveStatus;
      }

      public String getLiveMessage() {
        return liveMessage;
      }

      public void setLiveMessage(String liveMessage) {
        this.liveMessage = liveMessage;
      }

      public String getComboId() {
        return comboId;
      }

      public void setComboId(String comboId) {
        this.comboId = comboId;
      }

      public String getComboSection() {
        return comboSection;
      }

      public void setComboSection(String comboSection) {
        this.comboSection = comboSection;
      }

      public String getInDishId() {
        return inDishId;
      }

      public void setInDishId(String inDishId) {
        this.inDishId = inDishId;
      }

      public Object getDishDesription() {
        return dishDesription;
      }

      public void setDishDesription(Object dishDesription) {
        this.dishDesription = dishDesription;
      }

      public List<StPrice> getStPrice() {
        return stPrice;
      }

      public void setStPrice(List<StPrice> stPrice) {
        this.stPrice = stPrice;
      }

      public String getMaxQtyPeruser() {
        return maxQtyPeruser;
      }

      public void setMaxQtyPeruser(String maxQtyPeruser) {
        this.maxQtyPeruser = maxQtyPeruser;
      }

      public String getOutOfStock() {
        return outOfStock;
      }

      public void setOutOfStock(String outOfStock) {
        this.outOfStock = outOfStock;
      }

      public String getNonVegStatus() {
        return nonVegStatus;
      }

      public void setNonVegStatus(String nonVegStatus) {
        this.nonVegStatus = nonVegStatus;
      }

      @SerializedName("live_message")
      @Expose
      public String liveMessage;
      @SerializedName("combo_id")
      @Expose
      public String comboId;
      @SerializedName("combo_section")
      @Expose
      public String comboSection;
      @SerializedName("in_dish_id")
      @Expose
      public String inDishId;
      @SerializedName("dish_desription")
      @Expose
      public Object dishDesription;
      @SerializedName("st_price")
      @Expose
      public List<StPrice> stPrice = null;
      @SerializedName("max_qty_peruser")
      @Expose
      public String maxQtyPeruser;
      @SerializedName("out_of_stock")
      @Expose
      public String outOfStock;
      @SerializedName("non_veg_status")
      @Expose
      public String nonVegStatus;
    }
  }

  public class alltimings implements Serializable {
    private String dinner_start;
    private String dinner_end;
    private String breakfast_start;
    private String breakfast_end;
    private String tiffin_start;
    private String tiffin_end;
    private String fastfood_start;
    private String fastfood_end;
    private String sweets_start;
    private String sweets_end;
    private String fruits_start;
    private String fruits_end;
    private String lunch_start;

    public String getDinner_start() {
      return dinner_start;
    }

    public void setDinner_start(String dinner_start) {
      this.dinner_start = dinner_start;
    }

    public String getDinner_end() {
      return dinner_end;
    }

    public void setDinner_end(String dinner_end) {
      this.dinner_end = dinner_end;
    }

    public String getBreakfast_start() {
      return breakfast_start;
    }

    public void setBreakfast_start(String breakfast_start) {
      this.breakfast_start = breakfast_start;
    }

    public String getBreakfast_end() {
      return breakfast_end;
    }

    public void setBreakfast_end(String breakfast_end) {
      this.breakfast_end = breakfast_end;
    }

    public String getTiffin_start() {
      return tiffin_start;
    }

    public void setTiffin_start(String tiffin_start) {
      this.tiffin_start = tiffin_start;
    }

    public String getTiffin_end() {
      return tiffin_end;
    }

    public void setTiffin_end(String tiffin_end) {
      this.tiffin_end = tiffin_end;
    }

    public String getFastfood_start() {
      return fastfood_start;
    }

    public void setFastfood_start(String fastfood_start) {
      this.fastfood_start = fastfood_start;
    }

    public String getFastfood_end() {
      return fastfood_end;
    }

    public void setFastfood_end(String fastfood_end) {
      this.fastfood_end = fastfood_end;
    }

    public String getSweets_start() {
      return sweets_start;
    }

    public void setSweets_start(String sweets_start) {
      this.sweets_start = sweets_start;
    }

    public String getSweets_end() {
      return sweets_end;
    }

    public void setSweets_end(String sweets_end) {
      this.sweets_end = sweets_end;
    }

    public String getFruits_start() {
      return fruits_start;
    }

    public void setFruits_start(String fruits_start) {
      this.fruits_start = fruits_start;
    }

    public String getFruits_end() {
      return fruits_end;
    }

    public void setFruits_end(String fruits_end) {
      this.fruits_end = fruits_end;
    }

    public String getLunch_start() {
      return lunch_start;
    }

    public void setLunch_start(String lunch_start) {
      this.lunch_start = lunch_start;
    }

    public String getLunch_end() {
      return lunch_end;
    }

    public void setLunch_end(String lunch_end) {
      this.lunch_end = lunch_end;
    }

    private String lunch_end;
  }

  public class open_close_status implements Serializable {

    public String getOpenstatus() {
      return openstatus;
    }

    public void setOpenstatus(String openstatus) {
      this.openstatus = openstatus;
    }

    private String openstatus;
  }
}
