package com.food.order.speedzy.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Model.MeessageModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.SingleItemRowHolder> {

    private List<MeessageModel> itemsList;
    Context mContext;
    public static int sCorner = 30;
    public static int sMargin = 8;
    public MessageAdapter(Context context, List<MeessageModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public MessageAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.msg_row, viewGroup, false);
        return new MessageAdapter.SingleItemRowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MessageAdapter.SingleItemRowHolder holder, int i) {

            final MeessageModel items = itemsList.get(i);

        holder.msg.setText(items.getMsg());
            holder.msg1.setText(items.getMsg1());
            holder.msg2.setText(items.getMsg2());
            RequestBuilder<Drawable> thumbnailRequest = Glide
                    .with(mContext.getApplicationContext())
                    .load( items.getImage())
                    .placeholder(R.drawable.combo_placeholder);
            //.override(pixels_height, pixels_width);
            Glide
                    .with(mContext.getApplicationContext())
                    .load(items.getImage())
                    .thumbnail(thumbnailRequest)
                  .placeholder(R.drawable.combo_placeholder)
                    .into(holder.itemImage);
        //holder.linear.setBackgroundTintList(ColorStateList.valueOf(Commons.getRandomColor(mContext)));

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {
        LinearLayout linear;
        ImageView itemImage;
        TextView msg1,msg2,msg;
        public SingleItemRowHolder(View view) {
            super(view);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
            this.msg=view.findViewById(R.id.msg);
            this.msg1=view.findViewById(R.id.msg1);
            this.msg2=view.findViewById(R.id.msg2);
            this.linear=view.findViewById(R.id.linear);
        }
    }
}

