package com.food.order.speedzy.Utils;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import com.food.order.speedzy.maps.InfoWindowData;
import com.food.order.speedzy.screen.TrackOrder.TrackOrderDataModel;
import com.google.android.gms.maps.model.LatLng;
import io.reactivex.disposables.Disposable;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import timber.log.Timber;

public class GeneralUtil {
  private static final String CALL_DIALER_TELEPHONE = "tel:";

  public static void safelyDispose(Disposable... disposables) {
    for (Disposable subscription : disposables) {
      if (subscription != null && !subscription.isDisposed()) {
        subscription.dispose();
      }
    }
  }

  public static String getMimeType(Context context, Uri uri) {
    String mimeType = "";
    if (Objects.requireNonNull(uri.getScheme()).equals(ContentResolver.SCHEME_CONTENT)) {
      ContentResolver cr = context.getContentResolver();
      mimeType = Objects.requireNonNull(cr.getType(uri)).replace("image/", "").trim();
    } else {
      String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
          .toString());
      mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
          fileExtension.toLowerCase()).replace("image/", "").trim();
    }
    return mimeType;
  }

  public static boolean isStringEmpty(String str) {
    return str == null || str.trim().equals("");
  }

  public static boolean checkMimeFormatJpegOrPng(String format) {
    return format.contains("jpeg") || format.contains("png");
  }

  @SuppressLint("TimberArgCount")
  public static LatLng getLocationFromAddress(Context context, String strAddress) {
    Geocoder coder = new Geocoder(context);
    LatLng p1 = null;
    if (!GeneralUtil.isStringEmpty(strAddress)) {
      try {
        List<Address> address =
            coder.getFromLocationName(GeneralUtil.getFormattedCityAddress(strAddress, "Brahmapur"),
                5);
        if (address != null && address.size() > 0) {
          Address location = address.get(0);
          p1 = new LatLng(location.getLatitude(), location.getLongitude());
        } else {
          return null;
        }
      } catch (IOException ex) {
        Timber.e("exception", ex.getMessage());
      }
    }
    return p1;
  }

  @SuppressLint("TimberArgCount")
  public static LatLng getLatLngFromAddressWithoutCity(Context context, String strAddress) {
    Geocoder coder = new Geocoder(context);
    LatLng p1 = null;
    if (!GeneralUtil.isStringEmpty(strAddress)) {
      try {
        List<Address> address =
            coder.getFromLocationName(strAddress,
                5);
        if (address != null && address.size() > 0) {
          Address location = address.get(0);
          p1 = new LatLng(location.getLatitude(), location.getLongitude());
        } else {
          return null;
        }
      } catch (IOException ex) {
        Timber.e("exception", ex.getMessage());
      }
    }
    return p1;
  }

  public static String getFormattedCityAddress(String address, String cityName) {
    if (address.contains(cityName) || address.toLowerCase().contains(cityName.toLowerCase())) {
      return address;
    }
    return address + "," + cityName;
  }

  public static LatLng getLocationFromAddress(Context context, String strAddress, double latitude,
      double longitude) {
    if (getLocationFromAddress(context, strAddress) != null) {
      return getLocationFromAddress(context, strAddress);
    }
    return new LatLng(latitude, longitude);
  }

  public static InfoWindowData getInfoWindowData(TrackOrderDataModel data, boolean pickUpMarker) {
    InfoWindowData infoWindowData = new InfoWindowData();
    infoWindowData.setDestination(data.getDestination());
    infoWindowData.setPickUp(data.getPickup());
    infoWindowData.setDestinationAddress(data.getDestinationAddress());
    infoWindowData.setPickupAddress(data.getPickupAddress());
    infoWindowData.setOrderId(data.getOrderId());
    infoWindowData.setPickUpMarker(pickUpMarker);
    return infoWindowData;
  }
}
