package com.food.order.speedzy.SpeedzyRide;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.Adapter.Patanjali_Product_Adapter;
import com.food.order.speedzy.Model.Dishitem;
import com.food.order.speedzy.R;

import java.util.ArrayList;
import java.util.List;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.SingleItemRowHolder> {


    List<Car_model> car_modelList=new ArrayList<>();
    private Context mContext;
    CarAdapter.OnItemClickListener mOnItemClickListener;
    public CarAdapter(Context context,List<Car_model> car_modelList) {
        this.car_modelList = car_modelList;
        this.mContext = context;
    }
    public interface OnItemClickListener {
        void onItemClick(Car_model car_model);
    }

    public void setOnItemClickListener(
            CarAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public CarAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.car_item, null);
        return new CarAdapter.SingleItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CarAdapter.SingleItemRowHolder holder, int i) {
        Car_model car_model=car_modelList.get(i);
        holder.vehicle_name.setText(car_model.getName());
        holder.brand.setText(car_model.getModel());
        holder.facilities.setText(car_model.getFacility());
        holder.price.setText("₹ "+car_model.getPrice());
        holder.vehicle_img.setBackgroundResource(car_model.getImage());
        if (i==car_modelList.size()-1){
            holder.view.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(car_model);
            }
        });
    }

    @Override
    public int getItemCount() {

        return car_modelList.size();
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView vehicle_name, brand,facilities,price;
        ImageView vehicle_img;
        View view;

        public SingleItemRowHolder(View view) {
            super(view);
            this.vehicle_name = view.findViewById(R.id.vehicle_name);
            this.vehicle_img = view.findViewById(R.id.vehicle_img);
            this.price = view.findViewById(R.id.price);
            this.brand = view.findViewById(R.id.brand);
            this.facilities = view.findViewById(R.id.facilities);
            this.view = view.findViewById(R.id.view);
        }
    }
}

