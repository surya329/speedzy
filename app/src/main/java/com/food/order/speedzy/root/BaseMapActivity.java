package com.food.order.speedzy.root;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentActivity;
import com.afollestad.materialdialogs.MaterialDialog;
import com.food.order.speedzy.R;
import java.util.Objects;

public class BaseMapActivity extends FragmentActivity {
  private final static int REQUEST_CHECK_SETTINGS = 0;
  private final static String TAG = "BaseMapActivity";
  private MaterialDialog mNoInternetDialog;
  private boolean mViewDestroyed;
  private boolean mInBackground;
  private boolean mViewCreated;
  private Dialog mProgressDialog;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);
    mViewCreated = true;
    mViewDestroyed = false;
  }

  @Override
  protected void onStop() {
    super.onStop();
  }

  @Override public void onResume() {
    super.onResume();
    mInBackground = false;
  }

  @Override protected void onPause() {
    super.onPause();
    mInBackground = true;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    mViewDestroyed = true;
  }

  protected boolean isInBackground() {
    return mInBackground;
  }

  protected void dismissProgessLoading() {
    if (mProgressDialog != null && mProgressDialog.isShowing()) {
      mProgressDialog.dismiss();
    }
  }

  protected void showProgressDialog(String message) {
    if (mProgressDialog == null) {
      mProgressDialog = new Dialog(this);
      mProgressDialog.setContentView(R.layout.loader_layout);
      mProgressDialog.setCancelable(false);
      Objects.requireNonNull(mProgressDialog.getWindow())
          .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
    if (!isInBackground()) {
      mProgressDialog.show();
    }
  }

  protected void showProgressDialog() {
    showProgressDialog(R.string.general_label_pleasewait);
  }

  protected void showProgressDialog(@StringRes int messageResId) {
    showProgressDialog(getString(messageResId));
  }
}
