package com.food.order.speedzy.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class RestaurantModel implements Parcelable {

    private String mResPic;
    private String mResName;
    private String mRating;
    private Float mRatingBar;
    private String mMsg;
    private String mOpenClose;
    private String mMinOrder;
    private String mMenu;
    private String mInRestaurantId;

    public String getmInRestaurantId() {
        return mInRestaurantId;
    }

    public void setmInRestaurantId(String mInRestaurantId) {
        this.mInRestaurantId = mInRestaurantId;
    }

    public String getmStStreetAddress() {
        return mStStreetAddress;
    }

    public void setmStStreetAddress(String mStStreetAddress) {
        this.mStStreetAddress = mStStreetAddress;
    }

    public String getmStSuburb() {
        return mStSuburb;
    }

    public void setmStSuburb(String mStSuburb) {
        this.mStSuburb = mStSuburb;
    }

    private String mStStreetAddress;
    private String mStSuburb;

    public RestaurantModel() {

    }

    protected RestaurantModel(Parcel in) {
        mResPic = in.readString();
        mResName = in.readString();
        mRating = in.readString();
        if (in.readByte() == 0) {
            mRatingBar = null;
        } else {
            mRatingBar = in.readFloat();
        }
        mMsg = in.readString();
        mOpenClose = in.readString();
        mMinOrder = in.readString();
        mMenu = in.readString();
        mInRestaurantId = in.readString();
        mStStreetAddress = in.readString();
        mStSuburb = in.readString();
    }

    public static final Creator<RestaurantModel> CREATOR = new Creator<RestaurantModel>() {
        @Override
        public RestaurantModel createFromParcel(Parcel in) {
            return new RestaurantModel(in);
        }

        @Override
        public RestaurantModel[] newArray(int size) {
            return new RestaurantModel[size];
        }
    };

    public String getmResPic() {
        return mResPic;
    }

    public void setmResPic(String mResPic) {
        this.mResPic = mResPic;
    }

    public String getmResName() {
        return mResName;
    }

    public void setmResName(String mResName) {
        this.mResName = mResName;
    }

    public String getmRating() {
        return mRating;
    }

    public void setmRating(String mRating) {
        this.mRating = mRating;
    }

    public Float getmRatingBar() {
        return mRatingBar;
    }

    public void setmRatingBar(Float mRatingBar) {
        this.mRatingBar = mRatingBar;
    }

    public String getmMsg() {
        return mMsg;
    }

    public void setmMsg(String mMsg) {
        this.mMsg = mMsg;
    }

    public String getmOpenClose() {
        return mOpenClose;
    }

    public void setmOpenClose(String mOpenClose) {
        this.mOpenClose = mOpenClose;
    }

    public String getmMinOrder() {
        return mMinOrder;
    }

    public void setmMinOrder(String mMinOrder) {
        this.mMinOrder = mMinOrder;
    }

    public String getmMenu() {
        return mMenu;
    }

    public void setmMenu(String mMenu) {
        this.mMenu = mMenu;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mResPic);
        parcel.writeString(mResName);
        parcel.writeString(mRating);
        if (mRatingBar == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeFloat(mRatingBar);
        }
        parcel.writeString(mMsg);
        parcel.writeString(mOpenClose);
        parcel.writeString(mMinOrder);
        parcel.writeString(mMenu);
        parcel.writeString(mInRestaurantId);
        parcel.writeString(mStStreetAddress);
        parcel.writeString(mStSuburb);
    }
}
