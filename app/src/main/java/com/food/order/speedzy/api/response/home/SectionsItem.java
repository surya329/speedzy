package com.food.order.speedzy.api.response.home;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class SectionsItem implements Serializable {

	@SerializedName("section_type")
	private String sectionType;

	@SerializedName("section_id")
	private String sectionId;

	@SerializedName("name")
	private String name;

	@SerializedName("width")
	private String width;

	@SerializedName("itemcount")
	private String itemcount;

	@SerializedName("title")
	private String title;

	@SerializedName("items")
	private List<ItemsItem> items;

	@SerializedName("height")
	private String height;

	public void setSectionType(String sectionType){
		this.sectionType = sectionType;
	}

	public String getSectionType(){
		return sectionType;
	}

	public void setSectionId(String sectionId){
		this.sectionId = sectionId;
	}

	public String getSectionId(){
		return sectionId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setWidth(String width){
		this.width = width;
	}

	public String getWidth(){
		return width;
	}

	public void setItemcount(String itemcount){
		this.itemcount = itemcount;
	}

	public String getItemcount(){
		return itemcount;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

	public List<ItemsItem> getItems(){
		return items;
	}

	public void setHeight(String height){
		this.height = height;
	}

	public String getHeight(){
		return height;
	}

	@Override
 	public String toString(){
		return 
			"SectionsItem{" + 
			"section_type = '" + sectionType + '\'' + 
			",section_id = '" + sectionId + '\'' + 
			",name = '" + name + '\'' + 
			",width = '" + width + '\'' + 
			",itemcount = '" + itemcount + '\'' + 
			",title = '" + title + '\'' + 
			",items = '" + items + '\'' + 
			",height = '" + height + '\'' + 
			"}";
		}
}