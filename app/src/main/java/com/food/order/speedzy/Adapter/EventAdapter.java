package com.food.order.speedzy.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Activity.CateringActivity_New;
import com.food.order.speedzy.Model.EventListModelNew;
import com.food.order.speedzy.Model.ShopListModelNew;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.michael.easydialog.EasyDialog;

import java.util.ArrayList;
import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    Activity context;
    List<EventListModelNew.Event> events;
    public static int sCorner = 30;
    public static int sMargin = 0;

    public EventAdapter(Activity context,
                        List<EventListModelNew.Event> events) {
        this.context = context;
        this.events = events;
    }

    @NonNull
    @Override
    public EventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new EventAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(EventAdapter.ViewHolder holder, final int position) {
        EventListModelNew.Event event_model = events.get(position);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        float cal_height =(width * 386)/750;

        ViewGroup.LayoutParams params =
                holder.linear.getLayoutParams();
        params.height = (int) cal_height;
        holder.linear.requestLayout();
        ViewGroup.LayoutParams params1 = holder.res_main_image.getLayoutParams();
        params1.height = (int) cal_height;
       holder.res_main_image.requestLayout();

        String base_url = event_model.getEventPicturePath()+"/"+event_model.getEventPictureName();
        String base_url_logo =event_model.getLogoImagePath()+"/"+event_model.getLogoImageName();;
        RequestBuilder<Drawable> thumbnailRequest = Glide
                .with(context)
                .load(base_url_logo)
                .apply(RequestOptions.bitmapTransform(new CircleCrop()));
        Glide
                .with(context)
                .load(base_url_logo)
                .thumbnail(thumbnailRequest)
                .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                .into(holder.res_logo);
        RequestBuilder<Drawable> thumbnailRequest1 = Glide
                .with(context)
                .load(base_url)
                .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder));
        Glide
                .with(context)
                .load(base_url)
                .thumbnail(thumbnailRequest1)
                .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder).diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.res_main_image);
        holder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToolTipView(holder,holder.getAdapterPosition());
            }
        });
       /* holder.restname.setText(event_model.getEventTitle());
        if (event_model.getStStreetAddress()!=null) {
            holder.res_address.setText(event_model.getStStreetAddress());
        }else if (event_model.getStSuburb()!=null)  {
            holder.res_address.setText(event_model.getStSuburb());
        }else {
            holder.res_address.setText("- - - - - - - - - -");
        }
        holder.rating.setText(event_model.getCountRating() + " Ratings");
        holder.avg_rating.setText(event_model.getAvgRating());
       if (event_model.getFlag().length() > 0) {
            holder.offer_tag.setVisibility(View.VISIBLE);
            holder.offer_tag.setText(event_model.getFlag());
        } else {
            holder.offer_tag.setVisibility(View.INVISIBLE);
        }*/

        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CateringActivity_New.class);
                intent.putExtra("event_id",events.get(position).getEventId());
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
    }
    private void ToolTipView(EventAdapter.ViewHolder holder, int adapterPosition) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_tooltip, null);
        TextView amount = view.findViewById(R.id.amount);
        TextView desc = view.findViewById(R.id.desc);
        desc.setText(Html.fromHtml(events.get(adapterPosition).getEventDescription()+"\n\n"+
                events.get(adapterPosition).getEventTerms()));
        amount.setVisibility(View.GONE);
        new EasyDialog(context)
                .setLayout(view)
                .setBackgroundColor(context.getResources().getColor(R.color.red))
                .setLocationByAttachedView(holder.info)
                .setGravity(EasyDialog.GRAVITY_TOP)
                .setAnimationAlphaShow(100, 0.0f, 1.0f)
                .setAnimationAlphaDismiss(100, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(false)
                .setMarginLeftAndRight(24, 24)
                .setOutsideColor(Color.TRANSPARENT)
                .show();
    }
    @Override
    public int getItemCount() {
        return events.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       // TextView restname, rating,avg_rating,offer_tag, res_address;
        ImageView res_logo,res_main_image,info;
        RelativeLayout linear;
        public ViewHolder(View itemView) {
            super(itemView);
            linear =  itemView.findViewById(R.id.linear);
            res_logo = (ImageView) itemView.findViewById(R.id.res_logo);
            res_main_image=itemView.findViewById(R.id.res_main_image);
            info=itemView.findViewById(R.id.info);
           /* restname = (TextView) itemView.findViewById(R.id.restname);
            offer_tag = (TextView) itemView.findViewById(R.id.offer_tag);
            rating = (TextView) itemView.findViewById(R.id.rating);
            avg_rating=itemView.findViewById(R.id.avg_rating);
            res_address= itemView.findViewById(R.id.res_address);*/
        }
    }
}


