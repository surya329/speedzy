package com.food.order.speedzy.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.food.order.speedzy.Adapter.EventAdapter;
import com.food.order.speedzy.Model.EventListModelNew;
import com.food.order.speedzy.Model.ShopListModelNew;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedyLinearLayoutManager;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.root.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class EventActivity extends BaseActivity {

    RecyclerView mRecyclerView;
    LinearLayoutManager linearLayoutManager;
    EventAdapter eventAdapter;
    RelativeLayout comingsoon;
    EventListModelNew eventListModelNew;
    LoaderDiloag loaderDiloag;
    MySharedPrefrencesData mySharedPrefrencesData;
    String Veg_Flag;
    String res_id, nav_type, menu_id, name;
    Toolbar toolbar;
    ImageView ic_back;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        loaderDiloag = new LoaderDiloag(this);
        mySharedPrefrencesData = new MySharedPrefrencesData();
        Veg_Flag = mySharedPrefrencesData.getVeg_Flag(this);

        res_id = getIntent().getStringExtra("res_id");
        menu_id = getIntent().getStringExtra("menu_id");
        nav_type = getIntent().getStringExtra("nav_type");
        name = getIntent().getStringExtra("name");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ic_back = findViewById(R.id.ic_back);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        comingsoon = findViewById(R.id.comingsoon);

        setSupportActionBar(toolbar);
        statusbar_bg(R.color.red);


        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
       
    }

    private void statusbar_bg(int color) {
   /* getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
        .getColor(color)));*/
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }
    private void callapi() {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(EventActivity.this)
                .create(Apimethods.class);
        Call<EventListModelNew> call = methods.getEventList(mySharedPrefrencesData.getSelectCity(EventActivity.this));
        Log.d("url", "url=" + call.request().url().toString());
        Log.d("Latitude", Commons.latitude_str);
        Log.d("Longitude", Commons.longitude_str);
        loaderDiloag.displayDiloag();
        call.enqueue(new Callback<EventListModelNew>() {
            @Override
            public void onResponse(Call<EventListModelNew> call,
                                   retrofit2.Response<EventListModelNew> response) {
                eventListModelNew = response.body();
                linearLayoutManager = new LinearLayoutManager(EventActivity.this,
                        LinearLayoutManager.VERTICAL, false);
                mRecyclerView.setNestedScrollingEnabled(false);
                mRecyclerView.setLayoutManager(linearLayoutManager);
                mRecyclerView.setHasFixedSize(true);
                eventAdapter = new EventAdapter(EventActivity.this,eventListModelNew.getEvents());
                mRecyclerView.setAdapter(eventAdapter);
                eventAdapter.notifyDataSetChanged();

                if (eventListModelNew.getEvents().isEmpty()){
                    comingsoon.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }else {
                    comingsoon.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                }
                loaderDiloag.dismissDiloag();
            }

            @Override
            public void onFailure(@NonNull Call<EventListModelNew> call, @NonNull Throwable t) {
                t.printStackTrace();
                loaderDiloag.dismissDiloag();
            }
        });
    }
    @Override
    public void onBackPressed() {
        Commons.back_button_transition(EventActivity.this);
    }
    @Override
    public void onStart() {
        super.onStart();
        if (eventListModelNew == null) {
           callapi();
        }else {
            eventAdapter.notifyDataSetChanged();
        }
    }
}
