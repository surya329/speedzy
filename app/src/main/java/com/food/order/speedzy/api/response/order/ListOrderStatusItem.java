package com.food.order.speedzy.api.response.order;

import com.google.gson.annotations.SerializedName;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ListOrderStatusItem{

	@SerializedName("deliveryGuyId")
	private String deliveryGuyId;

	@SerializedName("orderNumber")
	private String orderNumber;

	@SerializedName("appId")
	private String appId;

	@SerializedName("statusMessage")
	private String statusMessage;

	@SerializedName("status")
	private int status;

	@SerializedName("deliveryGuyMobileNo")
	private String  deliveryGuyMobileNo;

	@SerializedName("deliveryGuyName")
	private String  deliveryGuyName;

	public String getDeliveryGuyMobileNo() {
		return deliveryGuyMobileNo;
	}

	public String getDeliveryGuyName() {
		return deliveryGuyName;
	}

	public String getDeliveryGuyId(){
		return deliveryGuyId;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public String getAppId(){
		return appId;
	}

	public String getStatusMessage(){
		return statusMessage;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ListOrderStatusItem{" + 
			"deliveryGuyId = '" + deliveryGuyId + '\'' + 
			",orderNumber = '" + orderNumber + '\'' + 
			",appId = '" + appId + '\'' + 
			",statusMessage = '" + statusMessage + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}