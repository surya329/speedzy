package com.food.order.speedzy.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.R;
import com.food.order.speedzy.api.response.grocery.CategoriesItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class Patanjali_SectionListDataAdapter
    extends RecyclerView.Adapter<Patanjali_SectionListDataAdapter.SingleItemRowHolder> {
  private List<CategoriesItem> mData;
  private Context mContext;
  private Patanjali_SectionListDataAdapter.OnItemClickListener mOnItemClickListener;
  int row_index = 0;

  public interface OnItemClickListener {
    void onItemClick(View view, int position);
  }

  public void setOnItemClickListener(
      final Patanjali_SectionListDataAdapter.OnItemClickListener mItemClickListener) {
    this.mOnItemClickListener = mItemClickListener;
  }

  public Patanjali_SectionListDataAdapter(Context context) {
    this.mData = new ArrayList<>();
    this.mContext = context;
  }

  @NonNull @Override
  public Patanjali_SectionListDataAdapter.SingleItemRowHolder onCreateViewHolder(
      ViewGroup viewGroup, int i) {
    View v =
        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_single_item, null);
    return new SingleItemRowHolder(v);
  }

  @Override
  public void onBindViewHolder(final Patanjali_SectionListDataAdapter.SingleItemRowHolder holder,
      int i) {
    holder.tvTitle.setText(mData.get(i).getName());
    circleImage(holder.itemImage, mData.get(i).getImage(), true, mContext);

    holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (mOnItemClickListener != null) {
          if (holder.getAdapterPosition() != RecyclerView.NO_POSITION) {
            mOnItemClickListener.onItemClick(view, holder.getAdapterPosition());
            row_index = holder.getAdapterPosition();
            notifyDataSetChanged();
          }
        }
      }
    });
    if (row_index == i) {
      holder.tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.red));
      //circleImage(holder.itemImage,mData.get(i).getImage(),true);
      holder.framelayout1.setVisibility(View.VISIBLE);
    } else {
      holder.tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.grey));
      //circleImage(holder.itemImage,mData.get(i).getImage(),false);
      holder.framelayout1.setVisibility(View.GONE);
    }
  }

  private static <T> void circleImage(final ImageView imageView, String uri, final boolean border,
      Context mContext) {
    //DrawableRequestBuilder<String> thumbnailRequest = Glide
    //        .with(mContext)
    //        .load( "https://storage.googleapis.com/speedzy_data/resources/patanjali/category_image/thumb/"+uri)
    //        .transform(new CircleTransform(mContext));
    //Glide.with(mContext)
    //        .load("https://storage.googleapis.com/speedzy_data/resources/patanjali/category_image/main_image/"+uri)
    //        .thumbnail( thumbnailRequest )
    //        .transform(new CircleTransform(mContext))
    //        .into(imageView);
    RequestBuilder<Drawable> thumbnailRequest = Glide
        .with(mContext)
        .load(uri)
            .apply(RequestOptions.bitmapTransform(new CircleCrop()));
    Glide.with(mContext)
        .load(uri)
        .thumbnail(thumbnailRequest)
            .apply(RequestOptions.bitmapTransform(new CircleCrop()))
        .into(imageView);
  }

  private static Bitmap addBorder(Bitmap resource, Context context) {
    int w = resource.getWidth();
    int h = resource.getHeight();
    int radius = Math.min(h / 2, w / 2);
    Bitmap output = Bitmap.createBitmap(w + 8, h + 8, Bitmap.Config.ARGB_8888);
    Paint p = new Paint();
    p.setAntiAlias(true);
    Canvas c = new Canvas(output);
    c.drawARGB(0, 0, 0, 0);
    p.setStyle(Paint.Style.FILL);
    c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);
    p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    c.drawBitmap(resource, 4, 4, p);
    p.setXfermode(null);
    p.setStyle(Paint.Style.STROKE);
    p.setColor(ContextCompat.getColor(context, R.color.red));
    p.setStrokeWidth(3);
    c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);
    return output;
  }

  @Override
  public int getItemCount() {
    return mData.size();
  }

  public void setData(
      List<CategoriesItem> data) {
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public void refreshData(
      List<CategoriesItem> data) {
    mData.clear();
    setData(data);
  }

  class SingleItemRowHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.online_userViewImage) ImageView itemImage;
    @BindView(R.id.lyt_parent) RelativeLayout lyt_parent;
    @BindView(R.id.framelayout1)
    FrameLayout framelayout1;

    public SingleItemRowHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }
}