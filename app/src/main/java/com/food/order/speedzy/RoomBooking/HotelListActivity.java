package com.food.order.speedzy.RoomBooking;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;

import java.util.ArrayList;

public class HotelListActivity extends AppCompatActivity {

   private ArrayList<HotelListModleClass> hotelListModleClasses;
    private RecyclerView recyclerView;
    private HotelListRecycleAdapter mAdapter;
    TextView place_name_txt,check_out_day_txt,check_in_day_txt,day_txt,guest_txt,room_txt;
Toolbar toolbar;

    Integer image[] = {R.drawable.hotel_list12,R.drawable.hotel_list12,R.drawable.hotel_list12,R.drawable.hotel_list12,
            R.drawable.hotel_list12,R.drawable.hotel_list12,R.drawable.hotel_list12,R.drawable.hotel_list12};
    String in_day,out_day,room,guest,place_name,day_str;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        statusbar_bg(R.color.red);

        in_day=getIntent().getStringExtra("in_day");
        out_day=getIntent().getStringExtra("out_day");
        room=getIntent().getStringExtra("room");
        guest=getIntent().getStringExtra("guest");
        place_name=getIntent().getStringExtra("place_name");
        day_str=getIntent().getStringExtra("day");

        place_name_txt= findViewById(R.id.place_name);
        check_out_day_txt= findViewById(R.id.check_out_day);
        check_in_day_txt= findViewById(R.id.check_in_day);
        day_txt= findViewById(R.id.day);
        guest_txt= findViewById(R.id.guest);
        room_txt= findViewById(R.id.room);

        place_name_txt.setText(place_name);
        check_in_day_txt.setText(in_day);
        check_out_day_txt.setText(out_day);
        guest_txt.setText(guest+" Guest");
        room_txt.setText(room+" Room");
        day_txt.setText(day_str);

        //        HotelName List Recyclerview code is here.

        recyclerView = findViewById(R.id.recyclerView);

        hotelListModleClasses = new ArrayList<>();


        for (int i = 0; i < image.length; i++) {
            HotelListModleClass beanClassForRecyclerView_contacts = new HotelListModleClass(image[i]);

            hotelListModleClasses.add(beanClassForRecyclerView_contacts);
        }
        mAdapter = new HotelListRecycleAdapter(HotelListActivity.this, hotelListModleClasses);
        LinearLayoutManager layoutManager = new LinearLayoutManager(HotelListActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }

    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(this);
    }
}
