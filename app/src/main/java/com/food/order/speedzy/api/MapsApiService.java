package com.food.order.speedzy.api;

import com.food.order.speedzy.api.response.maps.DirectionsResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MapsApiService {
  //Direction Api
  @GET("/maps/api/directions/json?")
  Observable<DirectionsResponse> mapApiDirection(@Query("mode") String mode,
      @Query("origin") String origin,
      @Query("destination") String destination,
      @Query("key") String apiKey, @Query("alternatives") String alternativeRoutes);
}
