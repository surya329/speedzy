package com.food.order.speedzy.screen.patanjali;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.food.order.speedzy.R;
import java.util.ArrayList;
import java.util.List;

public class PatanjaliSectionListAdapter extends RecyclerView.Adapter<PatanjaliSectionListVH> {
  private final Callback mCallback;
  private List<GroceryCategoryList> mData;

  public PatanjaliSectionListAdapter(
      Callback callback) {
    this.mData = new ArrayList<>();
    mCallback = callback;
  }

  @NonNull @Override
  public PatanjaliSectionListVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View view =
        LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.category_single_item, viewGroup, false);
    return new PatanjaliSectionListVH(view, new PatanjaliSectionListVH.Callback() {
      @Override public void onClickItem(int position) {
        setSelected(position);
        mCallback.onClickItem(mData.get(position));
      }
    });
  }

  @Override
  public void onBindViewHolder(@NonNull PatanjaliSectionListVH patanjaliSectionListVH, int i) {
    patanjaliSectionListVH.onBindView(mData.get(i));
  }

  @Override public int getItemCount() {

    return mData.size();
  }

  private void setSelected(int position) {
    for (int i = 0; i < mData.size(); i++) {
      mData.get(i).setSelected(i == position);
    }
    notifyDataSetChanged();
  }

  public void setData(
      List<GroceryCategoryList> data) {
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public void refreshData(
      List<GroceryCategoryList> data) {
    mData.clear();
    setData(data);
  }

  public void clearAdapter() {
    mData.clear();
    notifyDataSetChanged();
  }

  public interface Callback {
    void onClickItem(GroceryCategoryList data);
  }
}
