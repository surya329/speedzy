package com.food.order.speedzy.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.food.order.speedzy.Model.LoginUserResponse;
import com.food.order.speedzy.OtpSmsRetriverApi.AppSignatureHashHelper;
import com.food.order.speedzy.OtpSmsRetriverApi.SMSReceiver;
import com.food.order.speedzy.R;
import com.food.order.speedzy.SpeedzyRideApi.QueryBuilderRide;
import com.food.order.speedzy.SpeedzyRideApi.RideApiService;
import com.food.order.speedzy.SpeedzyRideApi.RideServiceApiGenerator;
import com.food.order.speedzy.SpeedzyRideApi.response.LoginRide.UserRegistration;
import com.food.order.speedzy.Utils.AllValidation;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MarshMallowPermission;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.PinEntryEditText;
import com.food.order.speedzy.api.response.upload.otp.UploadOTP;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.root.BaseActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.android.sdk.TrueSDK;
import com.truecaller.android.sdk.TrueSdkScope;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import net.hockeyapp.android.CrashManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */

public class SignupSignin_New extends BaseActivity
    implements
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    SMSReceiver.OTPReceiveListener {
  public static final int REQUEST_CODE = 99;
  TextView btnVerifyNumber, btnVerifyOtp, timerTextId, btnRetry;
  PinEntryEditText txtPinEntry;
  LinearLayout layoutLoginWithGoogle, layoutLoginWithTrueCaller, verifyNumberLayout,
      verifyOTPlayout;
  EditText etextMobileNumber;
  TextView countryCode;
  private GoogleApiClient mGoogleApiClient, googleApiClient;
  private static final int RC_SIGN_IN = 007;
  MarshMallowPermission p = new MarshMallowPermission(this);
  GoogleSignInAccount acct;
  String social_id, user_email, admin_first_name, admin_last_name, android_id;
  LoginUserResponse signUpModel;
  MySharedPrefrencesData mySharedPrefrencesData;
  LOcaldbNew lOcaldbNew;
  int verifyNumberLayout_flag = 0;
  LoaderDiloag loaderDiloag;
  FirebaseAuth auth;
  private String verificationCode;
  PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
  TextView otp_verify_num, privacy, terms;
  int login_flag = 0;
  ProgressBar progressBar1;
  private View viewToAttachDisplayerTo;
  boolean network_status = true;
  int onResumeCount = 0;
  private CountDownTimer countDownTimer;
  private Disposable mDisposable;

  private GoogleSignInClient mGoogleSignInClient;

  private boolean isRandomOTP = false;
  private String generatedOTP = "";

  private SMSReceiver smsReceiver;

  @SuppressLint("HardwareIds")
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login_new_);
    AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(this);
    Log.i(SignupSignin_New.class.getSimpleName(),
        "HashKey: " + appSignatureHashHelper.getAppSignatures().get(0));
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    viewToAttachDisplayerTo = findViewById(R.id.signup_signin_activity);

    loaderDiloag = new LoaderDiloag(this);
    mySharedPrefrencesData = new MySharedPrefrencesData();
    android_id = Settings.Secure.getString(SignupSignin_New.this.getContentResolver(),
        Settings.Secure.ANDROID_ID);
    mySharedPrefrencesData.setDeviceId(SignupSignin_New.this, android_id);
    lOcaldbNew = new LOcaldbNew(this);

    ITrueCallback sdkCallback = new ITrueCallback() {
      @Override
      public void onSuccessProfileShared(@NonNull final TrueProfile trueProfile) {

        // This method is invoked when the truecaller app is installed on the device and the user gives his
        // consent to share his truecaller profile

        callapi(trueProfile.phoneNumber, "1", trueProfile.firstName, trueProfile.lastName,
            trueProfile.email, "");
      }

      @Override
      public void onFailureProfileShared(@NonNull final TrueError trueError) {
        // This method is invoked when some error occurs or if an invalid request for verification is made

        // Toast.makeText(getApplicationContext(), "onFailureProfileShared: " + trueError.getErrorType(),Toast.LENGTH_SHORT).show();
        Log.d("onFailureProfileShared", "onFailureProfileShared: " + trueError.getErrorType());
      }

      @Override
      public void onVerificationRequired() {

      }
    };
    TrueSdkScope trueScope = new TrueSdkScope.Builder(this, sdkCallback)
        .consentMode(TrueSdkScope.CONSENT_MODE_POPUP)
        .consentTitleOption(TrueSdkScope.SDK_CONSENT_TITLE_VERIFY)
        .footerType(TrueSdkScope.FOOTER_TYPE_SKIP)
        .build();

    TrueSDK.init(trueScope);

      /*  if (TrueSDK.getInstance().isUsable()){
            TrueSDK.getInstance().getUserProfile(this);
        }*/
    main_method_call();
    //loginViaFacebook();
  }

  private void main_method_call() {

    googleApiClient =
        new GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build();
    googleApiClient.connect();

    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestEmail()
        .build();
    mGoogleApiClient = new GoogleApiClient.Builder(SignupSignin_New.this)
        .enableAutoManage(SignupSignin_New.this, SignupSignin_New.this)
        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
        .build();
    mGoogleSignInClient = GoogleSignIn.getClient(SignupSignin_New.this, gso);

    layoutLoginWithGoogle = findViewById(R.id.layoutLoginWithGoogle);
    layoutLoginWithTrueCaller = findViewById(R.id.layoutLoginWithTrueCaller);
    layoutLoginWithGoogle.setVisibility(View.VISIBLE);
    layoutLoginWithTrueCaller.setVisibility(View.VISIBLE);
    layoutLoginWithGoogle.getBackground()
        .setColorFilter(Color.parseColor("#E7473B"), PorterDuff.Mode.SRC_ATOP);
    layoutLoginWithTrueCaller.getBackground()
        .setColorFilter(Color.parseColor("#0885EE"), PorterDuff.Mode.SRC_ATOP);
    verifyNumberLayout = findViewById(R.id.verifyNumberLayout);
    btnVerifyNumber = findViewById(R.id.btnVerifyNumber);
    verifyNumberLayout.setVisibility(View.VISIBLE);
    verifyOTPlayout = findViewById(R.id.verifyOTPlayout);
    verifyOTPlayout.setVisibility(View.GONE);

    countryCode = findViewById(R.id.countryCode);
    countryCode.setEnabled(false);
    etextMobileNumber = findViewById(R.id.etextMobileNumber);
    timerTextId = findViewById(R.id.timerTextId);
    progressBar1 = findViewById(R.id.progressBar1);
    btnRetry = findViewById(R.id.btnRetry);
    btnVerifyOtp = findViewById(R.id.btnVerifyOtp);
    btnRetry.setVisibility(View.GONE);
    otp_verify_num = findViewById(R.id.otp_verify_num);
    txtPinEntry = findViewById(R.id.txt_pin_entry);
    privacy = findViewById(R.id.privacy);
    terms = findViewById(R.id.terms);
    privacy.setText(Html.fromHtml(getString(R.string.privacy)));
    privacy.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        privacy_dialogshow(1);
      }
    });
    terms.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        privacy_dialogshow(2);
      }
    });

    etextMobileNumber.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        String text_value = etextMobileNumber.getText().toString().trim();
        if (text_value.startsWith("+91") || text_value.equalsIgnoreCase("+91")) {
          etextMobileNumber.setText("");
        }
      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });

    btnVerifyNumber.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (AllValidation.emptyFieldValidation1(etextMobileNumber.getText().toString(), "Mobile no",
            1, SignupSignin_New.this)) {
            layoutLoginWithGoogle.setVisibility(View.GONE);
            layoutLoginWithTrueCaller.setVisibility(View.GONE);
            verifyNumberLayout.setVisibility(View.GONE);
            verifyOTPlayout.setVisibility(View.VISIBLE);
            otp_verify_num.setText(
                "Please enter the 6 digit OTP sent to +91 " + etextMobileNumber.getText()
                    .toString());
            txtPinEntry.setVisibility(View.VISIBLE);
            //generateOtpAndCallApi();
            callfircase_auth();
            //loginViaFacebook();
            timerFun();
            Commons.hideSoftKeyboard(txtPinEntry);
        }
      }
    });

    btnRetry.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (txtPinEntry.getText().toString().equalsIgnoreCase("")) {
          generateOtpAndCallApi();
        }
      }
    });
    btnVerifyOtp.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (!txtPinEntry.getText().toString().equalsIgnoreCase("") && !isRandomOTP) {
          otp_verification();
        } else if (isRandomOTP) {
          if (!txtPinEntry.getText().toString().equalsIgnoreCase("")) {
            if (txtPinEntry.getText().toString().equalsIgnoreCase(generatedOTP)) {
              callapi(etextMobileNumber.getText().toString(), "1", "", "", "", "");
            } else {
              Toast.makeText(SignupSignin_New.this, "Invalid Otp", Toast.LENGTH_SHORT).show();
            }
          } else {
            Toast.makeText(SignupSignin_New.this, "Enter otp", Toast.LENGTH_SHORT).show();
          }
        } else {
          Toast.makeText(SignupSignin_New.this, "Enter otp", Toast.LENGTH_SHORT).show();
        }
      }
    });

    layoutLoginWithGoogle.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
          login_flag = 1;
          signOut();
          //Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
          //startActivityForResult(signInIntent, RC_SIGN_IN);
          Intent signInIntent = mGoogleSignInClient.getSignInIntent();
          startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    });
    layoutLoginWithTrueCaller.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
          //login_flag = 1;
          if (TrueSDK.getInstance().isUsable()) {
            TrueSDK.getInstance().getUserProfile(SignupSignin_New.this);
          } else {
            Toast.makeText(getApplicationContext(), "Unable to open truecaller Application.",
                Toast.LENGTH_SHORT).show();
          }
      }
    });
  }

  private void otp_verification() {
    try {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode + "",
            txtPinEntry.getText().toString() + "");
        SigninWithPhone(credential);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void privacy_dialogshow(int i) {
    final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.setContentView(R.layout.privacy_popup);
    TextView dis = dialog.findViewById(R.id.dismiss);
    TextView warn = dialog.findViewById(R.id.warning);
    if (i == 1) {
      warn.setText("Privacy Policies");
    } else {
      warn.setText("Terms & Conditions");
    }
    dialog.show();
    dis.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
      }
    });
  }

  private void SigninWithPhone(PhoneAuthCredential credential) {
    auth.signInWithCredential(credential)
        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
          @Override
          public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful()) {
              if (verifyNumberLayout_flag == 1) {
                if (!GeneralUtil.isStringEmpty(etextMobileNumber.getText().toString())) {
                  callapi(etextMobileNumber.getText().toString(), "2", admin_first_name,
                      admin_last_name, user_email, social_id);
                }
              } else {
                if (!GeneralUtil.isStringEmpty(etextMobileNumber.getText().toString())) {
                  callapi(etextMobileNumber.getText().toString(), "1", "", "", "", "");
                }
              }
            } else {
             // Toast.makeText(SignupSignin_New.this, "Incorrect OTP", Toast.LENGTH_SHORT).show();
            }
          }
        });
  }

  private void callfircase_auth() {
    StartFirebaseLogin();
    PhoneAuthProvider.getInstance().verifyPhoneNumber(
        "+91" + etextMobileNumber.getText().toString(),
        // Phone number to verify
        60,                           // Timeout duration
        TimeUnit.SECONDS,                // Unit of timeout
        this,
        mCallback);
  }

  private void StartFirebaseLogin() {
    auth = FirebaseAuth.getInstance();
    mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

      @Override
      public void onVerificationCompleted(PhoneAuthCredential credential) {
        SigninWithPhone(credential);
      }

      @Override
      public void onVerificationFailed(FirebaseException e) {
        Toast.makeText(SignupSignin_New.this, "verification failed", Toast.LENGTH_SHORT).show();
        if (e instanceof FirebaseAuthInvalidCredentialsException) {
          Toast.makeText(SignupSignin_New.this, "Invalid request", Toast.LENGTH_SHORT).show();
        } else if (e instanceof FirebaseTooManyRequestsException) {
          Toast.makeText(SignupSignin_New.this, " SMS quota for the project has been exceeded",
              Toast.LENGTH_SHORT).show();
        }
      }

      @Override
      public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
        super.onCodeSent(s, forceResendingToken);
        verificationCode = s;
        txtPinEntry.setVisibility(View.VISIBLE);
      }
    };
  }

  private void signOut() {
    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
        new ResultCallback<Status>() {
          @Override
          public void onResult(@NonNull Status status) {
          }
        });
  }

  @Override
  public void onConnected(Bundle bundle) {

    System.out.println("Connected");
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
      if (!p.checkPermissionForLocation()) {
        p.requestPermissionForLocation();
      }
      if (ContextCompat.checkSelfPermission(this,
          android.Manifest.permission.ACCESS_COARSE_LOCATION)
          == PackageManager.PERMISSION_GRANTED) {
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        try {
          if (!(lastLocation == null)) {
            Commons.latitude = lastLocation.getLatitude();
            Commons.longitude = lastLocation.getLongitude();
            Commons.latitude_str = String.valueOf(lastLocation.getLatitude());
            Commons.longitude_str = String.valueOf(lastLocation.getLongitude());
            Log.d("Latitude", "" + Commons.latitude);
            Log.d("Longitude", "" + Commons.longitude);
          }
        } catch (Exception e) {

        }
      } else {

      }
    } else {
      Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
      if (!(lastLocation == null)) {
        Commons.latitude = lastLocation.getLatitude();
        Commons.longitude = lastLocation.getLongitude();
        Commons.latitude_str = String.valueOf(lastLocation.getLatitude());
        Commons.longitude_str = String.valueOf(lastLocation.getLongitude());
        Log.d("Latitude", "" + Commons.latitude);
        Log.d("Longitude", "" + Commons.longitude);
      }
    }
  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
    //if (requestCode == RC_SIGN_IN) {
    //  GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
    //  int statusCode = result.getStatus().getStatusCode();
    //  handleSignInResult(result);
    //} else {
    //  TrueSDK.getInstance().onActivityResultObtained(this, resultCode, data);
    //}
        /*else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }*/
    switch (requestCode) {
      case RC_SIGN_IN:
        try {
          // The Task returned from this call is always completed, no need to attach
          // a listener.
          Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
          GoogleSignInAccount account = task.getResult(ApiException.class);
          if (account != null) {
            handleSignInResult(account);
          }
        } catch (ApiException e) {
          // The ApiException status code indicates the detailed failure reason.
        }
        break;
      case REQUEST_CODE:
        AccountKitLoginResult loginResult =
            data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
        String toastMessage;
        if (loginResult.getError() != null) {
          toastMessage = loginResult.getError().getErrorType().getMessage();
        } else if (loginResult.wasCancelled()) {
          toastMessage = "Login Cancelled";
        } else {
          //toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
          handleOtpResult(loginResult);
        }
        break;
      default:
        TrueSDK.getInstance().onActivityResultObtained(this, resultCode, data);
        break;
    }
  }

  private void handleOtpResult(AccountKitLoginResult loginResult) {
    if (!GeneralUtil.isStringEmpty(etextMobileNumber.getText().toString())) {
      callapi(etextMobileNumber.getText().toString(), "1", "", "", "", "");
    }
    //if (loginResult != null) {
    //  social_id = Objects.requireNonNull(loginResult.getAccessToken()).getAccountId();
    //  if (verifyNumberLayout_flag == 1) {
    //    callapi(etextMobileNumber.getText().toString(), "2", admin_first_name,
    //        admin_last_name, user_email, social_id);
    //  } else {
    //    callapi(etextMobileNumber.getText().toString(), "1", "", "", "", "");
    //  }
    //}
  }

  private void handleSignInResult(GoogleSignInAccount result) {
    acct = result;
    if (acct != null) {
      social_id = acct.getId();
      user_email = acct.getEmail();
      admin_first_name = acct.getGivenName();
      admin_last_name = acct.getFamilyName();
      callapi("", "2", admin_first_name, admin_last_name, user_email, social_id);
    }
  }

  private void callapi(String mobile, String type, final String fname, final String lname,
      final String email, String social_id) {
    if (mobile.contains("+91")) {
      mobile = mobile.replace("+91", "");
    }
    Apimethods methods =
        API_Call_Retrofit.changeApiBaseUrl(SignupSignin_New.this).create(Apimethods.class);
    showProgressDialog();
    Call<LoginUserResponse> call = null;
    if (type.equalsIgnoreCase("1")) {
      call = methods.login_wid_no(mobile);
    } else if (type.equalsIgnoreCase("2")) {
      if (mobile.equalsIgnoreCase("")) {
        call = methods.check_login_wid_google(type, social_id);
      } else {
        call = methods.login_wid_google(mobile, type, fname, lname, email, social_id);
      }
    }
    Log.d("url", "url=" + call.request().url().toString());
    call.enqueue(new Callback<LoginUserResponse>() {
      @Override
      public void onResponse(@NonNull Call<LoginUserResponse> call,
          @NonNull Response<LoginUserResponse> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response.toString());
        dismissProgressDialog();
        signUpModel = response.body();

        if (signUpModel.getStatus().equalsIgnoreCase("Success")) {
          String mob = "";
          cancelTImer();
          mySharedPrefrencesData.setReferal(SignupSignin_New.this, signUpModel.getReferal());
          if (signUpModel.getSt_mobile().contains("+91")) {
            mob = signUpModel.getSt_mobile().replace("+91", "");
          } else {
            mob = signUpModel.getSt_mobile();
          }
          mySharedPrefrencesData.set_Party_mobile(SignupSignin_New.this, mob);
          mySharedPrefrencesData.setPartyEmail(SignupSignin_New.this,
              signUpModel.getSt_user_email());
          mySharedPrefrencesData.setUser_Id(SignupSignin_New.this, signUpModel.getIn_user_id());
          mySharedPrefrencesData.setFName(SignupSignin_New.this, signUpModel.getSt_first_name());
          mySharedPrefrencesData.setLName(SignupSignin_New.this, signUpModel.getSt_last_name());
          mySharedPrefrencesData.setLoginStatus(SignupSignin_New.this, true);
          mySharedPrefrencesData.setLoginType(SignupSignin_New.this,
              signUpModel.getFlg_user_type());
          mySharedPrefrencesData.setVeg_Flag(SignupSignin_New.this, "0");
          lOcaldbNew.clearDatabase();
          //login_RideApiCall();
          Intent intent = new Intent(SignupSignin_New.this, Saved_Addresses.class);
          intent.putExtra("change_address", false);
          intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
          intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
          startActivity(intent);
          overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
          dismissProgressDialog();
        } else {
          layoutLoginWithGoogle.setVisibility(View.GONE);
          layoutLoginWithTrueCaller.setVisibility(View.GONE);
          verifyNumberLayout.setVisibility(View.VISIBLE);
          verifyNumberLayout_flag = 1;
          showErrorDialog(signUpModel.getMsg());
        }
      }

      @Override
      public void onFailure(@NonNull Call<LoginUserResponse> call, @NonNull Throwable t) {
        dismissProgressDialog();
        showErrorDialog(t.getMessage());
      }
    });
  }

  private void login_RideApiCall() {
    String userid = mySharedPrefrencesData.getUser_Id(this);
    String name = mySharedPrefrencesData.getFName(this) + " " + mySharedPrefrencesData.getLName(this);
    String emailAddress = "";
    String mobileNumber = mySharedPrefrencesData.getSelectMobile(this);
    if (!mySharedPrefrencesData.getRideLoginStatus(SignupSignin_New.this)) {
      RideApiService rideApiService =
            RideServiceApiGenerator.provideRetrofit(this).create(RideApiService.class);
    Log.e("create query", QueryBuilderRide.loginRide(name, emailAddress, mobileNumber, userid));
    showProgressDialog();
    rideApiService.loginRide(
            QueryBuilderRide.loginRide(name, emailAddress, mobileNumber, userid))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<UserRegistration>() {
              @Override
              public void onSubscribe(Disposable d) {

              }

              @Override
              public void onNext(UserRegistration response) {
                if (response != null) {
                  mySharedPrefrencesData.setRideLoginStatus(SignupSignin_New.this, true);
                  Intent intent = new Intent(SignupSignin_New.this, Saved_Addresses.class);
                  intent.putExtra("change_address", false);
                  intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                  intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                  startActivity(intent);
                  overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                  dismissProgressDialog();
                }
              }

              @Override
              public void onError(Throwable e) {
                dismissProgressDialog();
                Log.e("error", e.getLocalizedMessage());
              }

              @Override
              public void onComplete() {

              }
            });
  }
  }

  public void onBackPressed() {
    verifyNumberLayout.setVisibility(View.VISIBLE);
    verifyOTPlayout.setVisibility(View.GONE);
    etextMobileNumber.setText("");
    etextMobileNumber.setHint("Phone Number");
    txtPinEntry.setText("");
    if (login_flag == 0) {
      layoutLoginWithGoogle.setVisibility(View.VISIBLE);
      layoutLoginWithTrueCaller.setVisibility(View.VISIBLE);
      finish();
    } else if (login_flag == 1) {
      layoutLoginWithGoogle.setVisibility(View.GONE);
      layoutLoginWithTrueCaller.setVisibility(View.GONE);
    } else if (onResumeCount > 0) {
      finish();
    } else {
      onResumeCount++;
    }
  }

  //@Override
  //protected Merlin createMerlin() {
  //  return new Merlin.Builder()
  //      .withConnectableCallbacks()
  //      .withDisconnectableCallbacks()
  //      .withBindableCallbacks()
  //      .build(this);
  //}

  @Override
  public void onStart() {
    super.onStart();
    onResumeCount = 0;
    checkForCrashes();

    startSMSListener();
     /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (!p.checkPermissionForReadSMS()) {
        p.requestPermissionReadSMS();
      }
      MySMSBroadCastReceiver.bindListener(new SmsListener() {
        @Override
        public void messageReceived(String messageText) {
          txtPinEntry.setText(messageText);
          if (!txtPinEntry.getText().toString().equalsIgnoreCase("")) {
            otp_verification();
          }
        }
      });
    } else {
      MySMSBroadCastReceiver.bindListener(new SmsListener() {
        @Override
        public void messageReceived(String messageText) {
          txtPinEntry.setText(messageText);
          if (!txtPinEntry.getText().toString().equalsIgnoreCase("")) {
            otp_verification();
          }
        }
      });
    }*/
  }
  @Override
  protected void onDestroy() {
    super.onDestroy();
    GeneralUtil.safelyDispose(mDisposable);

    if (smsReceiver != null) {
      unregisterReceiver(smsReceiver);
    }
  }

  // function for timer
  int count = 30;

  private void timerFun() {
    try {
      count = 30;
      progressBar1.setVisibility(View.VISIBLE);
      timerTextId.setVisibility(View.VISIBLE);
      progressBar1.setProgress(0);
      progressBar1.setMax(30);
      timerTextId.setText("" + 30);
      btnRetry.setVisibility(View.GONE);
      countDownTimer = new CountDownTimer(30 * 1000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
          count--;
          timerTextId.setText("" + count);
          progressBar1.setProgress(count);
        }

        @Override
        public void onFinish() {
          btnRetry.setVisibility(View.VISIBLE);
          progressBar1.setVisibility(View.GONE);
          timerTextId.setVisibility(View.GONE);
        }
      };
      countDownTimer.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void cancelTImer() {
    try {
      progressBar1.setVisibility(View.GONE);
      timerTextId.setVisibility(View.GONE);
      if (countDownTimer != null) {
        countDownTimer.cancel();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void checkForCrashes() {
    CrashManager.register(this);
  }

  private void loginViaFacebook() {
    final Intent intent = new Intent(SignupSignin_New.this, AccountKitActivity.class);
    AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
        new AccountKitConfiguration.AccountKitConfigurationBuilder(
            LoginType.PHONE,
            AccountKitActivity.ResponseType.CODE);
    intent.putExtra(
        AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
        configurationBuilder.build());
    startActivityForResult(intent, REQUEST_CODE);
  }

  public void generateOtpAndCallApi() {
    if (!GeneralUtil.isStringEmpty(etextMobileNumber.getText().toString())) {
      showProgressDialog("Resending Otp");
      Random rnd = new Random();
      int number = rnd.nextInt(999999);
      generatedOTP = String.format("%06d", number);
      Apimethods methods =
          API_Call_Retrofit.changeApiBaseUrl(SignupSignin_New.this).create(Apimethods.class);
      Call<UploadOTP> call = null;
      methods.set_resent_gotp(etextMobileNumber.getText().toString(), generatedOTP)
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(new Observer<UploadOTP>() {
            @Override public void onSubscribe(Disposable d) {
              mDisposable = d;
            }

            @Override public void onNext(UploadOTP uploadOTP) {
              dismissProgressDialog();
              isRandomOTP = uploadOTP != null && uploadOTP.getStatus().matches("Success");
            }

            @Override public void onError(Throwable e) {
              dismissProgressDialog();
              isRandomOTP = false;
            }

            @Override public void onComplete() {

            }
          });
    }
  }

  public void startSMSListener() {
    try {
      smsReceiver = new SMSReceiver();
      smsReceiver.setOTPListener(this);

      IntentFilter intentFilter = new IntentFilter();
      intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
      this.registerReceiver(smsReceiver, intentFilter);

      SmsRetrieverClient client = SmsRetriever.getClient(this);

      Task<Void> task = client.startSmsRetriever();
      task.addOnSuccessListener(new OnSuccessListener<Void>() {
        @Override
        public void onSuccess(Void aVoid) {
          // API successfully started
        }
      });

      task.addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
          // Fail to start API
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onOTPReceived(String otp) {
    String[] getOtp = otp.split("!");
    if (getOtp[0] != null) {
      String finalOtp = getOtp[0].replaceAll("[^0-9]", "").trim();
      if (!GeneralUtil.isStringEmpty(finalOtp)) {
        txtPinEntry.setText(finalOtp);
        otp_verification();
      }
    }
  }

  @Override
  public void onOTPTimeOut() {

  }

  @Override
  public void onOTPReceivedError(String error) {
  }
}
