package com.food.order.speedzy.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.food.order.speedzy.Activity.CheckoutActivity_New;
import com.food.order.speedzy.Activity.EventActivity;
import com.food.order.speedzy.Activity.HomeActivityNew;
import com.food.order.speedzy.Activity.MealComboActivity;
import com.food.order.speedzy.Activity.MedicineActivity;
import com.food.order.speedzy.Activity.PatanjaliProductActivity;
import com.food.order.speedzy.Activity.RestaurantActivity_NewVersion;
import com.food.order.speedzy.Adapter.MessageAdapter;
import com.food.order.speedzy.Adapter.MultiViewTypeAdapter;
import com.food.order.speedzy.BuildConfig;
import com.food.order.speedzy.CitySpecial.CitySpecialActivity;
import com.food.order.speedzy.Model.MeessageModel;
import com.food.order.speedzy.Model.Userwallet_model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.RoomBooking.RoomBookingActivity;
import com.food.order.speedzy.SpeedzyRide.SearchActivity_New;
import com.food.order.speedzy.SpeedzyStore.SpeedzyStoreActivity;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedyLinearLayoutManager;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.api.response.home.HomePageAppConfigResponse;
import com.food.order.speedzy.api.response.home.SectionsItem;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.screen.fruits.FruitShopActivity;
import com.food.order.speedzy.screen.home.SpeedzyOperationsClosedModel;
import com.food.order.speedzy.screen.operationsclosed.OperationsClosedActivity;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */
public class Home_New_Fragment extends Fragment {
  private RecyclerView mRecyclerView;
  private MultiViewTypeAdapter adapter;
  private List<SectionsItem> list = new ArrayList<>();
  private MySharedPrefrencesData mySharedPrefrencesData;
  private String city_id = "";
  private String Veg_Flag;
  private LinearLayout no_available;
  private String currentVersion;
  private String latestVersion;
  private String forceupdate;
  private LinearLayout food, brkfast, fruit, grocery, medicine, ride, room_booking, send_packages,
      city_special, speedzy_store, cake, sweet, event;
  private Disposable mDisposable;
  private SpeedzyOperationsClosedModel mSpeedzyOperationsClosedModel;
  private boolean isInitialApiCalled;
  @BindView(R.id.scroll_home)
  NestedScrollView scrollHome;
  private Apimethods methods;
  private Dialog dialog_update;
  private HomePageAppConfigResponse home_response_model;
  @BindView(R.id.recyclerView_msg)
  RecyclerView recyclerView_msg;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.home_new, container, false);
    mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
    no_available = (LinearLayout) view.findViewById(R.id.no_available);
    //linear = (LinearLayout) view.findViewById(R.id.linear);
    food = (LinearLayout) view.findViewById(R.id.food);
    brkfast = view.findViewById(R.id.brkfast);
    fruit = (LinearLayout) view.findViewById(R.id.fruit);
    grocery = (LinearLayout) view.findViewById(R.id.grocery);
    cake = (LinearLayout) view.findViewById(R.id.cake);
    sweet = (LinearLayout) view.findViewById(R.id.sweet);
    event = (LinearLayout) view.findViewById(R.id.event);
    medicine = (LinearLayout) view.findViewById(R.id.medicine);
    ride = (LinearLayout) view.findViewById(R.id.ride);
    room_booking = (LinearLayout) view.findViewById(R.id.room_booking);
    send_packages = (LinearLayout) view.findViewById(R.id.send_packages);
    city_special = (LinearLayout) view.findViewById(R.id.city_special);
    speedzy_store = (LinearLayout) view.findViewById(R.id.speedzy_store);
    ButterKnife.bind(this, view);
    mySharedPrefrencesData = new MySharedPrefrencesData();

    if (GeneralUtil.isStringEmpty(
        mySharedPrefrencesData.getSelectCity(Objects.requireNonNull(getContext()))) ||
        mySharedPrefrencesData.getSelectCity(getContext())
            .toUpperCase()
            .equalsIgnoreCase("NA")) {
      city_id = "FOODCITYBAM";
      mySharedPrefrencesData.setSelectCity(getContext(), "FOODCITYBAM");
    } else {
      city_id = mySharedPrefrencesData.getSelectCity(getContext());
    }
    Veg_Flag = mySharedPrefrencesData.getVeg_Flag(getContext());
    methods =
        API_Call_Retrofit.changeApiBaseUrl(Objects.requireNonNull(getActivity()))
            .create(Apimethods.class);
    initComponent();
    getCurrentVersion();
    MSGRecyclerviewInit();
    return view;
  }

  private void MSGRecyclerviewInit() {
    List<MeessageModel> msg_list = new ArrayList<>();
    MeessageModel model = new MeessageModel(R.drawable.aarogya, "Use of Aarogya Setu App",
        "All delivery partners have been advised to download the Aarogya Setu App.",
        "The app will warn partners if they happen to come into contact with a Covid-positive person,in which case,they're asked to take a paid leave of absence and self-isolate.");
    msg_list.add(model);

    model = new MeessageModel(R.drawable.facemask, "Mandatory usage of Face Masks",
        "Partners take a selfie everyday before duty.Speedzy delivery app uses facial recognition technology to verify and allow them to deliver.",
        "Free masks can be collected from a Speedzy Office.");
    msg_list.add(model);

    model = new MeessageModel(R.drawable.temp, "Daily Temperature Checks",
        "Several of our resturant partners and cloud kitchens check the temperatures of delivery partners when they go to pick up an order.",
        "If a delivery partner has a temperature of 99°F or higher,they are asked to stay at home and rest,and check for symptoms.");
    msg_list.add(model);

    recyclerView_msg.setLayoutManager(
        new SpeedyLinearLayoutManager(Objects.requireNonNull(getActivity()),
            SpeedyLinearLayoutManager.HORIZONTAL, false));
    MessageAdapter messageAdapter =
        new MessageAdapter(Objects.requireNonNull(getActivity()), msg_list);
    recyclerView_msg.setAdapter(messageAdapter);
  }

  private void initComponent() {
    mRecyclerView.setLayoutManager(
        new SpeedyLinearLayoutManager(Objects.requireNonNull(getActivity()),
            SpeedyLinearLayoutManager.VERTICAL, false));
    adapter = new MultiViewTypeAdapter(Objects.requireNonNull(getActivity()));
    mRecyclerView.setAdapter(adapter);
    food.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (mSpeedzyOperationsClosedModel != null
            && mSpeedzyOperationsClosedModel.getAppOpenStatus()
            == SpeedzyConstants.AppOpenStatus.CLOSED
            && mSpeedzyOperationsClosedModel.getAppClosedType()
            == SpeedzyConstants.AppCloseStatus.RESTURANT) {
          startActivity(
              OperationsClosedActivity.newIntent(getContext(), mSpeedzyOperationsClosedModel));
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Commons.menu_id = SpeedzyConstants.RESTURANT_MENU_ID;
          Commons.flag_for_hta = "7";
          Intent i = new Intent(Objects.requireNonNull(getActivity()),
              RestaurantActivity_NewVersion.class);
          i.putExtra("res_id", "");
          i.putExtra("menu_id", SpeedzyConstants.RESTURANT_MENU_ID);
          i.putExtra("nav_type", "3");
          i.putExtra("name", "Restaurants");
          startActivity(i);
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
    brkfast.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (mSpeedzyOperationsClosedModel != null
            && mSpeedzyOperationsClosedModel.getAppOpenStatus()
            == SpeedzyConstants.AppOpenStatus.CLOSED
            && mSpeedzyOperationsClosedModel.getAppClosedType()
            == SpeedzyConstants.AppCloseStatus.RESTURANT) {
          startActivity(
              OperationsClosedActivity.newIntent(getContext(), mSpeedzyOperationsClosedModel));
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Commons.menu_id = SpeedzyConstants.RESTURANT_MENU_ID;
          Commons.flag_for_hta = "7";
          Intent i = new Intent(Objects.requireNonNull(getActivity()),
              RestaurantActivity_NewVersion.class);
          i.putExtra("res_id", "");
          i.putExtra("menu_id", SpeedzyConstants.RESTURANT_MENU_ID);
          i.putExtra("nav_type", "3");
          i.putExtra("name", "Breakfast");
          startActivity(i);
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
    grocery.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //Commons.menu_id="M1016";
        //Commons.flag_for_hta = "Grocery";
        //Intent intent = new Intent(Objects.requireNonNull(getActivity()), PatanjaliProduct_DetailsActivity.class);
        //intent.putExtra("app_tittle", "Grocery");
        //startActivity(intent);
        //Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        if (mSpeedzyOperationsClosedModel != null
            && mSpeedzyOperationsClosedModel.getAppOpenStatus()
            == SpeedzyConstants.AppOpenStatus.CLOSED
            && mSpeedzyOperationsClosedModel.getAppClosedType()
            != SpeedzyConstants.AppCloseStatus.RESTURANT) {
          startActivity(
              OperationsClosedActivity.newIntent(getContext(), mSpeedzyOperationsClosedModel));
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Commons.menu_id = "M1004";
          Commons.restaurant_id = "";
          Commons.flag_for_hta = "Patanjali";
          Intent i =
              new Intent(Objects.requireNonNull(getActivity()), PatanjaliProductActivity.class);
          startActivity(i);
          Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.slide_from_right,
              R.anim.slide_to_left);
        }
      }
    });
    cake.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (mSpeedzyOperationsClosedModel != null
            && mSpeedzyOperationsClosedModel.getAppOpenStatus()
            == SpeedzyConstants.AppOpenStatus.CLOSED
            && mSpeedzyOperationsClosedModel.getAppClosedType()
            != SpeedzyConstants.AppCloseStatus.RESTURANT) {
          startActivity(
              OperationsClosedActivity.newIntent(getContext(), mSpeedzyOperationsClosedModel));
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          callApi(2);
        }
      }
    });
    sweet.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (mSpeedzyOperationsClosedModel != null
            && mSpeedzyOperationsClosedModel.getAppOpenStatus()
            == SpeedzyConstants.AppOpenStatus.CLOSED
            && mSpeedzyOperationsClosedModel.getAppClosedType()
            != SpeedzyConstants.AppCloseStatus.RESTURANT) {
          startActivity(
              OperationsClosedActivity.newIntent(getContext(), mSpeedzyOperationsClosedModel));
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Commons.menu_id = SpeedzyConstants.SWEET_MENU_ID;
          Commons.flag_for_hta = "7";
          Intent i = new Intent(Objects.requireNonNull(getActivity()),
              RestaurantActivity_NewVersion.class);
          i.putExtra("res_id", "");
          i.putExtra("menu_id", SpeedzyConstants.SWEET_MENU_ID);
          i.putExtra("nav_type", "3");
          i.putExtra("name", "Sweet");
          startActivity(i);
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
    event.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (mSpeedzyOperationsClosedModel != null
            && mSpeedzyOperationsClosedModel.getAppOpenStatus()
            == SpeedzyConstants.AppOpenStatus.CLOSED
            && mSpeedzyOperationsClosedModel.getAppClosedType()
            != SpeedzyConstants.AppCloseStatus.RESTURANT) {
          startActivity(
              OperationsClosedActivity.newIntent(getContext(), mSpeedzyOperationsClosedModel));
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Commons.menu_id = SpeedzyConstants.RESTURANT_MENU_ID;
          Commons.flag_for_hta = "7";
          Intent i = new Intent(Objects.requireNonNull(getActivity()), EventActivity.class);
          i.putExtra("res_id", "");
          i.putExtra("menu_id", SpeedzyConstants.RESTURANT_MENU_ID);
          i.putExtra("nav_type", "7");
          i.putExtra("name", "Sweet");
          startActivity(i);
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
    fruit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //Commons.menu_id = "M1026";
        if (mSpeedzyOperationsClosedModel != null
            && mSpeedzyOperationsClosedModel.getAppOpenStatus()
            == SpeedzyConstants.AppOpenStatus.CLOSED
            && mSpeedzyOperationsClosedModel.getAppClosedType()
            != SpeedzyConstants.AppCloseStatus.RESTURANT) {
          startActivity(
              OperationsClosedActivity.newIntent(getContext(), mSpeedzyOperationsClosedModel));
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Commons.menu_id = SpeedzyConstants.FRUITS_MENU_ID;
          //Commons.flag_for_hta = "Drinking Water";
          //Intent intent = new Intent(Objects.requireNonNull(getActivity()), PatanjaliProduct_DetailsActivity.class);
          //intent.putExtra("app_tittle", "Drinking Water");
          //startActivity(intent);
          //Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
          Commons.flag_for_hta = SpeedzyConstants.Fruits;
          Intent i = new Intent(Objects.requireNonNull(getActivity()), FruitShopActivity.class);
          i.putExtra("res_id", "");
          i.putExtra("menu_id", SpeedzyConstants.FRUITS_MENU_ID);
          i.putExtra("nav_type", "3");
          i.putExtra("name", "Fruits");
          startActivity(i);
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });

    medicine.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (mSpeedzyOperationsClosedModel != null
            && mSpeedzyOperationsClosedModel.getAppOpenStatus()
            == SpeedzyConstants.AppOpenStatus.CLOSED
            && mSpeedzyOperationsClosedModel.getAppClosedType()
            != SpeedzyConstants.AppCloseStatus.RESTURANT) {
          startActivity(
              OperationsClosedActivity.newIntent(getContext(), mSpeedzyOperationsClosedModel));
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Intent intent = new Intent(Objects.requireNonNull(getActivity()), MedicineActivity.class);
          startActivity(intent);
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
    ride.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent ride = new Intent(Objects.requireNonNull(getActivity()), SearchActivity_New.class);
        ride.putExtra("search_type", 1);
        startActivity(ride);
        Objects.requireNonNull(getActivity())
            .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      }
    });
    room_booking.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent room = new Intent(Objects.requireNonNull(getActivity()), RoomBookingActivity.class);
        startActivity(room);
        Objects.requireNonNull(getActivity())
            .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      }
    });
    send_packages.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent packages =
            new Intent(Objects.requireNonNull(getActivity()), SearchActivity_New.class);
        packages.putExtra("search_type", 0);
        startActivity(packages);
        Objects.requireNonNull(getActivity())
            .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      }
    });
    city_special.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Commons.menu_id = "M1016";
        Commons.flag_for_hta = "City Special";
        if (home_response_model != null) {
          Intent intent =
              new Intent(Objects.requireNonNull(getActivity()), CitySpecialActivity.class);
          intent.putExtra("start_time_lunch",
              home_response_model.getAlltimings().getSuper_daily_start());
          intent.putExtra("end_time_lunch",
              home_response_model.getAlltimings().getSuper_daily_end());
          intent.putExtra("start_time_dinner", "");
          intent.putExtra("end_time_dinner", "");
          intent.putExtra("app_openstatus",
              home_response_model.getOpenCloseStatus().getOpenstatus());
          startActivity(intent);
          Objects.requireNonNull(getActivity())
              .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
          // callApi(1);
        }
      }
    });
    speedzy_store.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent room = new Intent(Objects.requireNonNull(getActivity()), SpeedzyStoreActivity.class);
        startActivity(room);
        Objects.requireNonNull(getActivity())
            .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      }
    });
  }

  public void callApi(int i) {
    if (i == 1) {
      Commons.menu_id = "M1016";
      Commons.flag_for_hta = "City Special";
    } else if (i == 2) {
      Commons.menu_id = SpeedzyConstants.CAKE_MENU_ID;
      Commons.flag_for_hta = "Cakes";
    } else if (i == 3) {
      Commons.menu_id = "M1026";
      Commons.flag_for_hta = "Drinking Water";
    }

       /* Intent intent = new Intent(Objects.requireNonNull(getActivity()), PatanjaliProduct_DetailsActivity.class);
        if (i == 1) {
            intent.putExtra("app_tittle", "City Special");
        } else if (i == 2) {
            intent.putExtra("app_tittle", "Cakes");
        } else if (i == 3) {
            intent.putExtra("app_tittle", "Drinking Water");
        }*/
    Intent intent = new Intent(Objects.requireNonNull(getActivity()), MealComboActivity.class);
    intent.putExtra("app_tittle", "Cakes");
    startActivity(intent);
    Objects.requireNonNull(getActivity())
        .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
  }

  @Override public void onDestroy() {
    super.onDestroy();
    GeneralUtil.safelyDispose(mDisposable);
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  private void getCurrentVersion() {
    if (Objects.requireNonNull(getActivity()) != null) {
      PackageManager pm = Objects.requireNonNull(getActivity()).getPackageManager();
      PackageInfo pInfo = null;

      try {
        pInfo = pm.getPackageInfo(Objects.requireNonNull(getActivity()).getPackageName(), 0);
      } catch (PackageManager.NameNotFoundException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }
      if (pInfo != null) {
        currentVersion = pInfo.versionName;
      } else {
        currentVersion = BuildConfig.VERSION_NAME;
      }
      callapi();
    }
  }

  public void callapi() {
    methods.get_home(city_id, Veg_Flag)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<HomePageAppConfigResponse>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(HomePageAppConfigResponse home_response_model1) {
            home_response_model = home_response_model1;
            if (home_response_model != null) {
              mSpeedzyOperationsClosedModel = SpeedzyOperationsClosedModel.transform(
                  home_response_model.getAppclosestatus(),
                  Integer.parseInt(home_response_model.getAppOpenstatus()),
                  Integer.parseInt(home_response_model.getAppCloseType()));
              if (mSpeedzyOperationsClosedModel != null
                  && mSpeedzyOperationsClosedModel.getAppOpenStatus()
                  == SpeedzyConstants.AppOpenStatus.CLOSED
                  && mSpeedzyOperationsClosedModel.getAppClosedType()
                  == SpeedzyConstants.AppCloseStatus.RESTURANT) {
               /* startActivity(
                        OperationsClosedActivity.newIntent(getContext(), mSpeedzyOperationsClosedModel));
                Objects.requireNonNull(getActivity())
                        .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);*/
              } else {
                list.clear();
                //if (home_response_model.getStatus().equalsIgnoreCase("Success")) {
                if (home_response_model.getSections() != null) {
                  for (int i = 0; i < home_response_model.getSections().size(); i++) {
                    list.add(home_response_model.getSections().get(i));
                  }
                  ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
                  Log.w("response", home_response_model.getImageBaseURL());
                  Commons.image_baseURL_very_small = home_response_model.getImageBaseURL()
                      + home_response_model.getEndpoints().get(0)
                      + "/";
                  Commons.image_baseURL_small = home_response_model.getImageBaseURL()
                      + home_response_model.getEndpoints().get(1)
                      + "/";
                  Commons.image_baseURL = home_response_model.getImageBaseURL()
                      + home_response_model.getEndpoints().get(2)
                      + "/";

                  //adapter = new MultiViewTypeAdapter(home_response_model.getOpenCloseStatus(),
                  //    home_response_model.getAlltimings(), list, slideroot_nevigation_activity);
                  adapter.refreshData(list);
                  adapter.setAlltimings(home_response_model.getAlltimings());
                  adapter.setOpen_close_status(home_response_model.getOpenCloseStatus());
                  mRecyclerView.setVisibility(View.VISIBLE);
                  scrollHome.setVisibility(View.VISIBLE);
                  no_available.setVisibility(View.GONE);
                  calluserwalletApi();
                  HomeActivityNew.cart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                      Commons.flag_for_hta = "";
                      Intent intent = new Intent(Objects.requireNonNull(getActivity()),
                          CheckoutActivity_New.class);
                      intent.putExtra("start_time_lunch",
                          home_response_model.getAlltimings().getLunchStart());
                      intent.putExtra("end_time_lunch",
                          home_response_model.getAlltimings().getLunchEnd());
                      intent.putExtra("start_time_dinner",
                          home_response_model.getAlltimings().getDinnerStart());
                      intent.putExtra("end_time_dinner",
                          home_response_model.getAlltimings().getDinnerEnd());
                      intent.putExtra("app_openstatus",
                          home_response_model.getOpenCloseStatus().getOpenstatus());
                      startActivity(intent);
                      Objects.requireNonNull(getActivity())
                          .overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    }
                  });
                }
              }
              latestVersion = home_response_model.getAppVersion();
              forceupdate = String.valueOf(home_response_model.getUpdateRequired());
              if (!GeneralUtil.isStringEmpty(home_response_model.getMapApiKey())) {
                mySharedPrefrencesData.saveMapsKey(Objects.requireNonNull(getContext()),
                    home_response_model.getMapApiKey());
              }
              if (!GeneralUtil.isStringEmpty(latestVersion) && !GeneralUtil.isStringEmpty(
                  currentVersion)) {
                if (forceupdate.equalsIgnoreCase(
                    "1")
                    && Integer.parseInt(latestVersion.replaceAll("[.]", "")) > Integer.parseInt(
                    currentVersion.replaceAll("[.]", ""))) {
                  showUpdateDialog(forceupdate);
                }
              }
              //} else {
              //  for (int i = 0; i < home_response_model.getSections().size(); i++) {
              //    list.add(home_response_model.getSections().get(i));
              //  }
              //  ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
              //  Log.w("response", home_response_model.getImageBaseURL());
              //  Commons.image_baseURL_very_small = home_response_model.getImageBaseURL()
              //      + home_response_model.getEndpoints().get(0)
              //      + "/";
              //  Commons.image_baseURL_small = home_response_model.getImageBaseURL()
              //      + home_response_model.getEndpoints().get(1)
              //      + "/";
              //  Commons.image_baseURL = home_response_model.getImageBaseURL()
              //      + home_response_model.getEndpoints().get(2)
              //      + "/";
              //  mRecyclerView.setLayoutManager(
              //      new SpeedyLinearLayoutManager(slideroot_nevigation_activity,
              //          SpeedyLinearLayoutManager.VERTICAL, false));
              //  adapter = new MultiViewTypeAdapter(home_response_model.getOpenCloseStatus(),
              //      home_response_model.getAlltimings(), list, slideroot_nevigation_activity);
              //  mRecyclerView.setAdapter(adapter);
              //  mRecyclerView.setVisibility(View.VISIBLE);
              //  linear.setVisibility(View.VISIBLE);
              //  no_available.setVisibility(View.GONE);
              //  mSpeedzyOperationsClosedModel = SpeedzyOperationsClosedModel.transform(
              //      home_response_model.getAppclosestatus(),
              //      Integer.parseInt(home_response_model.getAppOpenstatus()),
              //      Integer.parseInt(home_response_model.getAppCloseType()));
              //}
            }
            if (!isInitialApiCalled) {
              isInitialApiCalled = true;
            }
          }

          @Override public void onError(Throwable e) {
            if (!isInitialApiCalled) {
              isInitialApiCalled = true;
            }
          }

          @Override public void onComplete() {

          }
        });
  }

  //Call<Home_Response_Model> call = methods.get_home(city_id, Veg_Flag);
  //Log.d("url", "url=" + call.request().url().toString());
  //// loaderDiloag.displayDiloag();
  //call.enqueue(new Callback<Home_Response_Model>() {
  //  @Override
  //  public void onResponse(Call<Home_Response_Model> call,
  //      Response<Home_Response_Model> response) {
  //    int statusCode = response.code();
  //    Log.d("Response", "" + response);
  //    home_response_model = response.body();
  //    list.clear();
  //    if (home_response_model.getStatus().equalsIgnoreCase("Success")) {
  //      if (home_response_model.getSections() != null) {
  //        for (int i = 0; i < home_response_model.getSections().size(); i++) {
  //          list.add(home_response_model.getSections().get(i));
  //        }
  //        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
  //        Log.w("response", home_response_model.getImage_baseURL());
  //        Commons.image_baseURL_very_small = home_response_model.getImage_baseURL()
  //            + home_response_model.getEndpoints().get(0)
  //            + "/";
  //        Commons.image_baseURL_small = home_response_model.getImage_baseURL()
  //            + home_response_model.getEndpoints().get(1)
  //            + "/";
  //        Commons.image_baseURL = home_response_model.getImage_baseURL()
  //            + home_response_model.getEndpoints().get(2)
  //            + "/";
  //        mRecyclerView.setLayoutManager(
  //            new SpeedyLinearLayoutManager(slideroot_nevigation_activity,
  //                SpeedyLinearLayoutManager.VERTICAL, false));
  //        adapter = new MultiViewTypeAdapter(home_response_model.getOpen_close_status(),
  //            home_response_model.getAlltimings(), list, slideroot_nevigation_activity);
  //        mRecyclerView.setAdapter(adapter);
  //        mRecyclerView.setVisibility(View.VISIBLE);
  //        linear.setVisibility(View.VISIBLE);
  //        no_available.setVisibility(View.GONE);
  //        calluserwalletApi();
  //      }
  //      latestVersion = home_response_model.getApp_version();
  //      forceupdate = home_response_model.getUpdate_required();
  //      if (!GeneralUtil.isStringEmpty(latestVersion) && !GeneralUtil.isStringEmpty(
  //          currentVersion)) {
  //        if (forceupdate.equalsIgnoreCase(
  //            "1") && Integer.parseInt(latestVersion.replaceAll("[.]", "")) > Integer.parseInt(
  //            currentVersion.replaceAll("[.]", ""))) {
  //          showUpdateDialog(forceupdate);
  //        }
  //      }
  //    } else {
  //      mRecyclerView.setVisibility(View.VISIBLE);
  //      linear.setVisibility(View.VISIBLE);
  //      no_available.setVisibility(View.GONE);
  //    }

  //  @Override
  //  public void onFailure(Call<Home_Response_Model> call, Throwable t) {
  //    loaderDiloag.dismissDiloag();
  //    //Toast.makeText(Objects.requireNonNull(getActivity()).getApplicationContext(), "internet not available..connect internet", Toast.LENGTH_LONG).show();
  //  }
  //});

  public void showUpdateDialog(String forceupdate) {
    dialog_update = new Dialog(Objects.requireNonNull(getActivity()),
        android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    Objects.requireNonNull(dialog_update.getWindow())
        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog_update.setContentView(R.layout.wallet_popup);
    TextView dis = (TextView) dialog_update.findViewById(R.id.dismiss);
    TextView update = (TextView) dialog_update.findViewById(R.id.update);
    TextView warn = (TextView) dialog_update.findViewById(R.id.warning);
    TextView msg = (TextView) dialog_update.findViewById(R.id.message);
    dis.setText("Cancel");
    dis.setVisibility(View.GONE);
    update.setVisibility(View.VISIBLE);
    warn.setText("Info!!" + "\n" + "A New Update is Available");
    msg.setText("What's New \n"
        +
        "Thanks for using Speedzy!\n"
        +
        "•To make our app better for you, we bring updates to the App Store regularly.\n"
        +
        "•Every update of our app includes improvements for speed and reliability, additional Bug fixes and tweaks.\n"
        +
        "•We hope you enjoy this update! if you do, please consider leaving a review on the play Store :)");
    dis.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog_update.dismiss();
      }
    });
    update.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final String appName = "com.food.order.speedzy";
        Objects.requireNonNull(getActivity()).startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
            ("market://details?id=" + appName)));
        //dialog_update.dismiss();
      }
    });
    if (forceupdate.equalsIgnoreCase("1")) {
      dis.setVisibility(View.GONE);
      if (!((Activity) Objects.requireNonNull(getContext())).isFinishing()) {
        //show dialog
        dialog_update.setCancelable(false);
        dialog_update.setCanceledOnTouchOutside(false);
        dialog_update.show();
      }
    } else {
      dis.setVisibility(View.GONE);
      if (!((Activity) Objects.requireNonNull(getContext())).isFinishing()) {
        //show dialog
        dialog_update.setCancelable(false);
        dialog_update.setCanceledOnTouchOutside(false);
        dialog_update.show();
      }
    }

    dialog_update.setOnKeyListener(new DialogInterface.OnKeyListener() {
      @Override
      public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
          dialog_update.setCancelable(false);
          return true;
        }
        return false;
      }
    });
  }

  private void calluserwalletApi() {
    //Apimethods methods =
    //    API_Call_Retrofit.changeApiBaseUrl(slideroot_nevigation_activity).create(Apimethods.class);
    Call<Userwallet_model> call =
        methods.getUserwallet(mySharedPrefrencesData.getUser_Id(getActivity()));
    Log.d("url", "url=" + call.request().url().toString());

    call.enqueue(new Callback<Userwallet_model>() {
      @Override
      public void onResponse(Call<Userwallet_model> call, Response<Userwallet_model> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        Userwallet_model userwallet = response.body();
        assert userwallet != null;
        if (userwallet.getStatus().equalsIgnoreCase("Success")) {
          Commons.wallet_static_value = userwallet.getWallet();
        }
      }

      @Override
      public void onFailure(@NonNull Call<Userwallet_model> call, @NonNull Throwable t) {
      }
    });
  }

  @Override
  public void onResume() {
    super.onResume();
    if (isInitialApiCalled) {
      callApiToGetOperationsStatus();
    }
  }

  public void callApiToGetOperationsStatus() {
    if (mySharedPrefrencesData == null) {
      mySharedPrefrencesData = new MySharedPrefrencesData();
    }
    if (methods == null) {
      methods =
          API_Call_Retrofit.changeApiBaseUrl(getContext()).create(Apimethods.class);
    }
    if (GeneralUtil.isStringEmpty(city_id) && !GeneralUtil.isStringEmpty(
        mySharedPrefrencesData.getSelectCity(Objects.requireNonNull(getContext())))) {
      city_id = mySharedPrefrencesData.getSelectCity(Objects.requireNonNull(getContext()));
    }
    methods.get_home(city_id, mySharedPrefrencesData.getVeg_Flag(
        Objects.requireNonNull(getContext())))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<HomePageAppConfigResponse>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(HomePageAppConfigResponse home_response_model) {
            if (home_response_model != null) {
              mSpeedzyOperationsClosedModel = SpeedzyOperationsClosedModel.transform(
                  home_response_model.getAppclosestatus(),
                  Integer.parseInt(home_response_model.getAppOpenstatus()),
                  Integer.parseInt(home_response_model.getAppCloseType()));
            }
          }

          @Override public void onError(Throwable e) {

          }

          @Override public void onComplete() {

          }
        });
  }
}
