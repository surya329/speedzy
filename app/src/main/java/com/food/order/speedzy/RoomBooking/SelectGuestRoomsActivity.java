package com.food.order.speedzy.RoomBooking;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SelectGuestRoomsActivity extends AppCompatActivity {
        TextView done;
        TextView add_another_room;
        RecyclerView recyclerView;
        List<GuestRoomModel> guest_room_list=new ArrayList<>();
    GuestRoomAdapter guestRoomAdapter;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_guest_rooms);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        statusbar_bg(R.color.red);

        guest_room_list= (List<GuestRoomModel>) getIntent().getSerializableExtra("guest_room_list");
        recyclerView = findViewById(R.id.recyclerView);
        done=findViewById(R.id.done);
        add_another_room=findViewById(R.id.add_another_room);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SelectGuestRoomsActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        guestRoomAdapter = new GuestRoomAdapter(SelectGuestRoomsActivity.this,guest_room_list);
        recyclerView.setAdapter(guestRoomAdapter);

        add_another_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestRoomModel guestRoomModel=new GuestRoomModel();
                guestRoomModel.setAdult("1");
                guestRoomModel.setChild("0");
                guest_room_list.add(guest_room_list.size()-1,guestRoomModel);
                guestRoomAdapter.notifyDataSetChanged();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent();
                intent1.putExtra("guest_room_list", (Serializable) guest_room_list);
                setResult(3,intent1);
                Commons.back_button_transition(SelectGuestRoomsActivity.this);
            }
        });

    }
    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(this);
    }

}
