package com.food.order.speedzy.apimodule;

import com.food.order.speedzy.CitySpecial.CitySpecialCategoryModel;
import com.food.order.speedzy.Model.AddUserAddressModel;
import com.food.order.speedzy.Model.BookEventModelNew;
import com.food.order.speedzy.Model.Bookmodel;
import com.food.order.speedzy.Model.Cakes_Model;
import com.food.order.speedzy.Model.CategoryModel;
import com.food.order.speedzy.Model.CityListModel;
import com.food.order.speedzy.Model.Combo_Details_Response_Model;
import com.food.order.speedzy.Model.Combo_Response_Model;
import com.food.order.speedzy.Model.Delivery_Charge_Response_Model;
import com.food.order.speedzy.Model.EventListModelNew;
import com.food.order.speedzy.Model.Feedbackmodel;
import com.food.order.speedzy.Model.LoginUserResponse;
import com.food.order.speedzy.Model.OrderdetailsModel;
import com.food.order.speedzy.Model.Patanjali_Category_model;
import com.food.order.speedzy.Model.Patanjali_Productdetails_Model;
import com.food.order.speedzy.Model.Patanjali_order_deatail_model;
import com.food.order.speedzy.Model.PaytmModel;
import com.food.order.speedzy.Model.Restaurant_Dish_Model_New;
import com.food.order.speedzy.Model.RestaurantsListModelNew;
import com.food.order.speedzy.Model.ShopListModelNew;
import com.food.order.speedzy.Model.StorepackagerModel;
import com.food.order.speedzy.Model.UserAddressModel;
import com.food.order.speedzy.Model.Userwallet_model;
import com.food.order.speedzy.Model.ViewOrderModel;
import com.food.order.speedzy.Model.View_Rating_model;
import com.food.order.speedzy.Model.WalletNewModel;
import com.food.order.speedzy.Model.payment.PaymentModel;
import com.food.order.speedzy.Model.voucher_check_model;
import com.food.order.speedzy.api.response.DeviceUpdateModel;
import com.food.order.speedzy.api.response.banner.BannerResponse;
import com.food.order.speedzy.api.response.home.HomePageAppConfigResponse;
import com.food.order.speedzy.api.response.coupon.CouponsResponse;
import com.food.order.speedzy.api.response.upload.otp.UploadOTP;
import io.reactivex.Observable;
import java.util.ArrayList;
import okhttp3.ResponseBody;
import org.json.JSONArray;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Sujata Mohanty.
 */

public interface Apimethods {
  @GET("service/login")
  Call<LoginUserResponse> login_wid_no(@Query("mobile") String mobile);

  @GET("service/login")
  Call<LoginUserResponse> check_login_wid_google(@Query("type") String type,
      @Query("socialID") String socialID);

  @GET("service/login")
  Call<LoginUserResponse> login_wid_google(@Query("mobile") String mobile,
      @Query("type") String type,
      @Query("fname") String fname,
      @Query("lname") String lname,
      @Query("email") String email,
      @Query("socialID") String socialID);

  @GET("service/addressupdate")
  Call<AddUserAddressModel> add_address(@Query("user_id") String user_id,
      @Query("completeAddress") String completeAddress,
      @Query("state") String state,
      @Query("district") String district,
      @Query("pincode") String pincode,
      @Query("landmark") String landmark,
      @Query("addComments") String addComments,
      @Query("addressType") String addressType,
      @Query("long") String longi,
      @Query("lat") String lat,
      @Query("other_name") String other_name,
      @Query("name") String name,
      @Query("mobile") String mobile,
      @Query("area") String area,
      @Query("cityid") String cityid);
  @GET("service/addressupdate")
  Call<AddUserAddressModel> edit_address(@Query("user_id") String user_id,
                                        @Query("completeAddress") String completeAddress,
                                        @Query("state") String state,
                                        @Query("district") String district,
                                        @Query("pincode") String pincode,
                                        @Query("landmark") String landmark,
                                        @Query("addComments") String addComments,
                                        @Query("addressType") String addressType,
                                        @Query("long") String longi,
                                        @Query("lat") String lat,
                                        @Query("other_name") String other_name,
                                        @Query("name") String name,
                                        @Query("mobile") String mobile,
                                        @Query("area") String area,
                                        @Query("cityid") String cityid,
                                         @Query("address_id") String address_id);
  @GET("service/deleteaddress/")
  Call<AddUserAddressModel> delete_address(@Query("address_id") String address_id);

  @GET("service/getaddress")
  Call<UserAddressModel> get_address(@Query("user_id") String user_id);

  @GET("mobile/packagercat")
  Call<CategoryModel> packagercat();

  @GET("mobile/storepackager/")
  Call<StorepackagerModel> storepackager(@Query("userid") String user_id,
      @Query("pick_address") String pick_address,
      @Query("drop_address") String drop_address,
      @Query("category") String category,
      @Query("distance") String distance,
      @Query("comment") String comment);

  @GET("Adminappapi/changepayment")
  Call<StorepackagerModel> changepayment(@Query("orderid") String orderid,
      @Query("menuid") String menuid);

  @GET("mobile/restaurants")
  Call<RestaurantsListModelNew> setRestaurant(@Query("restname") String restname, @Query("lat")
      String latitude, @Query("lang") String longitude, @Query("schedule") int schedule,
      @Query("page") int page, @Query("deliveryfee")
      String delivery_fee, @Query("rating")
      String goodrating, @Query("minimumorder")
      String min_order, @Query("cuisinelist")
      ArrayList<String> search_cuisines, @Query
      ("booktable") String booktable, @Query
      ("bookroom") String bookroom);

  @GET("service/shoplist")
  Call<ShopListModelNew> setShop_list(@Query("city_id") String city_id,
      @Query("menu_id") String menu_id,
      @Query("addressID") String addressID,
      @Query("veg") String veg,
      @Query("lat") String lat,
      @Query("lang") String lang,
      @Query("restname") String restname,
      @Query("test") String test);
  @GET("service/eventlist")
  Call<EventListModelNew> getEventList(@Query("city_id") String city_id);
  @GET("service/bookevent")
  Call<BookEventModelNew> bookevent(@Query("name") String name,@Query("phone") String phone,
                                    @Query("budget") String budget,@Query("comment") String comment,
                                    @Query("event_date") String event_date,@Query("event_time") String event_time,
                                    @Query("booking_options") String booking_options,@Query("occasion") String occasion,
                                    @Query("no_of_person") String no_of_person,@Query("is_nonveg") String is_nonveg,
                                    @Query("userid") String userid,@Query("event_id") String event_id);

    /*@GET("mobile/dishlist")
    Call<Restaurant_Dish_Model> setRes_dish(@Query("restid") String restid, @Query("user id") String userid);*/

  @GET("service/itemlist")
  Call<Restaurant_Dish_Model_New> setRes_dish(@Query("restid") String restid,
      @Query("userid") String userid, @Query("veg") String veg);

  @FormUrlEncoded
  @POST("mobile/booktable")
  Call<Bookmodel> setBook(@Field("userid") String userid, @Field("restaurant_id") String
      res_id, @Field("name") String name, @Field("mobile") String mobile, @Field
      ("no_of_guests") String guests, @Field("email") String email,
      @Field("spe_request") String spe_request, @Field("booking_date") String bookdate,
      @Field("dt_ordered_date") String dt_ordered_date);

  @GET("mobile/bookall")
  Call<Bookmodel> setSubmitcatering(@Query("userid") String userid, @Query("name") String name,
      @Query("phone") String phone, @Query("budget") String budget,
      @Query("comment") String comment, @Query("datetime") String datetime,
      @Query("type") String type, @Query("occasion") String occasion,
      @Query("no_of_person") String no_of_person, @Query("vag_non") String vag_non);

  @FormUrlEncoded
  @POST("mobile/userwallet")
  Call<Userwallet_model> getUserwallet(@Field("userid") String userid);

  @GET("mobile/vieworder")
  Call<ViewOrderModel> getvieworders(@Query("userid") String userid, @Query("page") String page);

  @FormUrlEncoded
  @POST("mobile/transactions")
  Call<WalletNewModel> getwalletdetails(@Field("userid") String userid);

  @FormUrlEncoded
  @POST("mobile/updateprofile")
  Call<Feedbackmodel> setUpdte(@Field("userid") String userid, @Field("fname") String fname,
      @Field("lname") String lname, @Field("mobile") String mobile);

  @FormUrlEncoded
  @POST("mobile/addneworder")
  Call<Feedbackmodel> setPaymentNew(@Field("userid") String userid,
      @Field("mobile") String mobile,
      @Field("restaurant_id") String restaurant_id,
      @Field("firstname") String firstname,
      @Field("lastname") String lastname,
      @Field("email") String email,
      @Field("order_detail") JSONArray jsonArray,
      @Field("suburb_id") String suburb_id,
      @Field("order_amount") String order_amount,
      @Field("delivery_charge") String delivery_charge,
      @Field("total_amount") String total_amount,
      @Field("company_name") String company_name,
      @Field("street_number") String street_number,
      @Field("nearest_cross_city") String nearest_cross_city,
      @Field("delivery_area") String delivery_area,
      @Field("wallet") String wallet,
      @Field("comment") String comment,
      @Field("voucher_id") String voucher_id,
      @Field("voucher_amount") String voucher_amount,
      @Field("res_special_offer") String res_special_offer,
      @Field("dicount_val") String discountvalue,
      @Field("order_type") String order_type,
      @Field("dt_order_date") String dt_order_date,
      @Field("pre_order_date") String pre_order_date,
      @Field("deviceid") String deviceid,
      @Field("devicetype") String devicetype,
      @Field("gst") double gst, @Field("lat") String lat,
      @Field("lang") String lang,
      @Field("addressID") String addressID,
      @Field("cityid") String cityid,
      @Field("payment_method") int payment_method,
      @Field("restaurant_charge") String restaurant_charge);

  @GET("mobile/getcategories")
  Observable<Patanjali_Category_model> getpatanjaliproduct();

  @GET("mobile/citycategories")
  Call<CitySpecialCategoryModel> getcityspecial();

  @GET("mobile/getcityproducts")
  Call<Patanjali_Productdetails_Model> getCitySpecialproduct_detail(
     /* @Query("city_special") String city_special,*/
      @Query("page") String page,
      @Query("city_id") String city_id,
      @Query("subcategory") String subcategory);

  @GET("mobile/getproducts")
  Call<Patanjali_Productdetails_Model> getWaterproduct_detail(@Query("water") String water,
      @Query("page") String page,
      @Query("city_id") String city_id);

  @GET("service/cakes")
  Call<Cakes_Model> getCakes(@Query("city_id") String city_id,
      @Query("egg_flag") String egg_flag,
      @Query("page") String page);

  @GET("mobile/getproducts")
  Call<Patanjali_Productdetails_Model> getpatanjaliproduct_detail(@Query("page") String page,
      @Query("type") String type,
      @Query("category") String category,
      @Query("subcategory") String subcategory,
      @Query("search_string") String name,
      @Query("childsubcat") String childsubcat);

  @GET("mobile/ordergrocery")
  Call<Feedbackmodel> getordergrocery(@Query("userid") String userid,
      @Query("devicetype") String devicetype,
      @Query("d_type") String d_type,
      @Query("firstname") String firstname,
      @Query("lastname") String lastname,
      @Query("email") String email,
      @Query("mobile") String mobile,
      @Query("address") String address,
      @Query("landmark") String landmark,
      @Query("comment") String comment,
      @Query("lat") String lat,
      @Query("long") String longi,
      @Query("delivery_charge") String delivery_charge,
      @Query("coupon") String coupon,
      @Query("coupon_amount") String coupon_amount,
      @Query("wallet_amount") String wallet_amount,
      @Query("total_amount") String total_amount,
      @Query("product_detail") JSONArray product_detail,
      @Query("addressID") String addressID,
      @Query("menuid") String menu_ID,
      @Query("order_image") String orderImage,
      @Query("payment_method") int payment_method);

  @GET("mobile/grocerydetails/")
  Call<Patanjali_order_deatail_model> getpatanjaliorder_details(@Query("userid") String userid,
      @Query("addressID") String addressID);

  @GET("service/appconfignew")
  Observable<HomePageAppConfigResponse> get_home(@Query("city_id") String userid,
      @Query("veg") String veg);

  @GET("service/citylist")
  Call<CityListModel> get_city_list();

  @GET("service/getDeliveryCharge")
  Call<Delivery_Charge_Response_Model> get_delivery_charge(@Query("long") String longi,
      @Query("lat") String lat,
      @Query("addressID") String addressID,
      @Query("cityID") String cityID,
      @Query("amount") String amount,
      @Query("userid") String userid,
      @Query("restID") String restID,
      @Query("cake_flag") String cake_flag,
      @Query("menuid") String menu_ID,@Query("distance") String distance);

  @GET("service/getDeliveryCharge")
  Call<Delivery_Charge_Response_Model> get_delivery_charge_patanjali(@Query("long") String longi,
      @Query("lat") String lat,
      @Query("addressID") String addressID,
      @Query("cityID") String cityID,
      @Query("amount") String amount,
      @Query("userid") String userid,
      @Query("cake_flag") String cake_flag,
      @Query("menuid") String menu_ID);

  @GET("payment/paytmmobile.php")
  Call<PaytmModel> get_checkhash(@Query("amount") String amount,
      @Query("orderid") String orderid);

  @GET("mobile/ordersuccess")
  Call<ResponseBody> get_cash_payment(@Query("type") String type,
      @Query("orderid") String orderid);

  @GET("mobile/ordergrocerysuccess/")
  Call<ResponseBody> get_cash_payment_patanjali(@Query("type") String type,
      @Query("orderid") String orderid);

  @GET("Adminappapi/orderdetails")
  Call<OrderdetailsModel> orderdetails(@Query("orderid") String orderid);

  @POST("service/combolist")
  Call<Combo_Response_Model> get_combo_list(@Query("city_id") String city_id,
      @Query("veg") String veg);

  @GET("service/combodetails")
  Call<Combo_Details_Response_Model> get_combo_details(@Query("combo_id") String combo_id,
      @Query("rest_id") String rest_id);

  @GET("service/combodetails")
  Call<Combo_Details_Response_Model> get_cake_details(@Query("rest_id") String rest_id);

  @GET("mobile/check_coupon")
  Call<voucher_check_model> getvoucher(@Query("code") String code, @Query("rest_id") String rest_id,
      @Query("userid") String userid, @Query("amount") String amount);

  @GET("mobile/paymentstatus/")
  Observable<PaymentModel> getPaymentStatus(
      @Query("orderid") String query);

  @GET("mobile/bannersnew?")
  Observable<BannerResponse> getBannerList(
      @Query("menu_id") String query);

  @GET("mobile/sendotp?")
  Observable<UploadOTP> set_resent_gotp(@Query("number") String mobile, @Query("otp") String otp);

  @GET("mobile/updatedeviceid")
  Call<DeviceUpdateModel> set_updatedeviceid(@Query("deviceid") String deviceid,
      @Query("userid") String userid,
      @Query("device_token") String device_token, @Query("status") String status,
      @Query("name") String name, @Query("device_type") String device_type,
      @Query("device_os") String device_os, @Query("device_os_version") String device_os_version,
      @Query("app_version") String app_version);

  @FormUrlEncoded
  @POST("mobile/addreview")
  Call<Feedbackmodel> setreview(@Field("userid") String userid, @Field("restaurant_id") String
      restaurant_id, @Field("food") String food, @Field("value") String value,
      @Field("speed") String speed, @Field("description") String description);

  @FormUrlEncoded
  @POST("mobile/viewreview")
  Call<View_Rating_model> getreview(@Field("restaurant_id") String restaurant_id);

  @GET("mobile/coupondetail/?")
  Call<CouponsResponse> getCouponList(@Query("restid") String resturantId,@Query("userid") String userid,
                                      @Query("menuid") String menu_ID);
}

