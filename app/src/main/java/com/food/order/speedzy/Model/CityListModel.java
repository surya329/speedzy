package com.food.order.speedzy.Model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */


public class CityListModel implements Serializable {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CityListModel.cityList> getCityList() {
        return cityList;
    }

    public void setCityList(List<CityListModel.cityList> cityList) {
        this.cityList = cityList;
    }

    private List<cityList> cityList=null;
    public static class cityList implements Serializable {
        private String id;
        private String city_id;
        private String cityName;
        private String cityImage;
        private String cityDescription;
        private String addedAt;
        private String Statename;

        public cityList(String city_id, String cityName,List<area_list> area_list) {
            this.city_id=city_id;
            this.cityName=cityName;
            this.area_list=area_list;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getCityImage() {
            return cityImage;
        }

        public void setCityImage(String cityImage) {
            this.cityImage = cityImage;
        }

        public String getCityDescription() {
            return cityDescription;
        }

        public void setCityDescription(String cityDescription) {
            this.cityDescription = cityDescription;
        }

        public String getAddedAt() {
            return addedAt;
        }

        public void setAddedAt(String addedAt) {
            this.addedAt = addedAt;
        }

        public String getStatename() {
            return Statename;
        }

        public void setStatename(String statename) {
            Statename = statename;
        }

        public String getStateID() {
            return StateID;
        }

        public void setStateID(String stateID) {
            StateID = stateID;
        }

        public String getDistrictName() {
            return DistrictName;
        }

        public void setDistrictName(String districtName) {
            DistrictName = districtName;
        }

        public String getDistrictID() {
            return DistrictID;
        }

        public void setDistrictID(String districtID) {
            DistrictID = districtID;
        }

        private String StateID;
        private String DistrictName;
        private String DistrictID;

        public List<area_list> getArea_list() {
            return area_list;
        }

        public void setArea_list(List<area_list> area_list) {
            this.area_list = area_list;
        }

        private List<area_list> area_list=null;
        public static class area_list implements Serializable {
            private String in_suburb_id;
            private String postcode_id;
            private String st_suburb;

            public area_list(String in_suburb_id, String st_suburb) {
                this.st_suburb=st_suburb;
                this.in_suburb_id=in_suburb_id;
            }

            public String getIn_suburb_id() {
                return in_suburb_id;
            }

            public void setIn_suburb_id(String in_suburb_id) {
                this.in_suburb_id = in_suburb_id;
            }

            public String getPostcode_id() {
                return postcode_id;
            }

            public void setPostcode_id(String postcode_id) {
                this.postcode_id = postcode_id;
            }

            public String getSt_suburb() {
                return st_suburb;
            }

            public void setSt_suburb(String st_suburb) {
                this.st_suburb = st_suburb;
            }

            public String getCity_id() {
                return city_id;
            }

            public void setCity_id(String city_id) {
                this.city_id = city_id;
            }

            private String city_id;
        }
    }
}
