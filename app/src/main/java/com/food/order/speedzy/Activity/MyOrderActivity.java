package com.food.order.speedzy.Activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import com.food.order.speedzy.root.BaseActivity;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import com.food.order.speedzy.Fragment.OrderFragment;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Sujata Mohanty.
 */

public class MyOrderActivity extends BaseActivity {
  String userid,  Veg_Flag;
  MySharedPrefrencesData mysharedpref;
  private Toolbar toolbar;
  LoaderDiloag loaderDiloag;
  int flag = 0;
  int type = 1;
  private View viewToAttachDisplayerTo;
  boolean network_status = true;
  ViewPager viewPager;
  private TabLayout tabLayout;

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_my_order);

    viewToAttachDisplayerTo = findViewById(R.id.displayerAttachableView);


    mysharedpref = new MySharedPrefrencesData();
    Veg_Flag = mysharedpref.getVeg_Flag(this);
    flag = getIntent().getIntExtra("flag", 0);
    type = getIntent().getIntExtra("type", 0);

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    if (Veg_Flag.equalsIgnoreCase("1")) {
      statusbar_bg(R.color.red);
    } else {
      statusbar_bg(R.color.red);
    }
    viewPager = (ViewPager) findViewById(R.id.viewpager);
    viewPager.setOffscreenPageLimit(4);
    setupViewPager(viewPager);
    tabLayout = (TabLayout)findViewById(R.id.tabs);
    tabLayout.setupWithViewPager(viewPager);

    for (int i = 0; i < tabLayout.getTabCount(); i++) {

      TabLayout.Tab tab = tabLayout.getTabAt(i);
      if (tab != null) {

        TextView tabTextView = new TextView(this);
        tab.setCustomView(tabTextView);

        tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
        tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;

        tabTextView.setText(tab.getText());

        // First tab is the selected tab, so if i==0 then set BOLD typeface
        if (i == 0) {
          tabTextView.setTypeface(null, Typeface.BOLD);
          tabTextView.setTextSize(16);
        }
        tabTextView.setTextColor(getResources().getColor(R.color.white));

      }

    }
    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        TextView text = (TextView) tab.getCustomView();
        text.setTypeface(null, Typeface.BOLD);
        text.setTextSize(16);
        text.setTextColor(getResources().getColor(R.color.white));
      }

      @Override
      public void onTabUnselected(TabLayout.Tab tab) {
        TextView text = (TextView) tab.getCustomView();
        text.setTypeface(null, Typeface.NORMAL);
        text.setTextSize(13);
        text.setTextColor(getResources().getColor(R.color.white));
      }

      @Override
      public void onTabReselected(TabLayout.Tab tab) {

      }
    });
    if (type!=1){
      viewPager.setCurrentItem(1);
    }
    if (type==5){
      viewPager.setCurrentItem(3);
    }
  }
  private void statusbar_bg(int color) {
    Objects.requireNonNull(getSupportActionBar())
            .setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(color)));
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
    }
  }
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case android.R.id.home:
        back_function();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void back_function() {
    if (flag == 1) {
      Intent intent = new Intent(MyOrderActivity.this, HomeActivityNew.class);
      intent.putExtra("flag", flag);
      startActivity(intent);
    }
    Commons.back_button_transition(MyOrderActivity.this);
  }

  @Override
  public void onBackPressed() {
    if (getFragmentManager().getBackStackEntryCount() == 0) {
      back_function();
    } else {
      super.onBackPressed();
    }
  }
  private void setupViewPager(ViewPager viewPager) {
    ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
    Fragment fragment1=OrderFragment.newInstance(0);
    Fragment fragment2= OrderFragment.newInstance(1);
    Fragment fragment3= OrderFragment.newInstance(2);
    Fragment fragment4= OrderFragment.newInstance(3);
    Fragment fragment5= OrderFragment.newInstance(4);
    Fragment fragment6= OrderFragment.newInstance(5);
    Fragment fragment7= OrderFragment.newInstance(6);
    adapter.addFragment(fragment1, "Food Order");
    adapter.addFragment(fragment2, "Grocery Order");
    adapter.addFragment(fragment3, "Medicine Order");
   // adapter.addFragment(fragment6, "Room Booking");
   // adapter.addFragment(fragment7, "My Rides");
//    adapter.addFragment(fragment4, "CitySpecial Order");
//    adapter.addFragment(fragment5, "Cake Order");
    viewPager.setAdapter(adapter);
  }

  class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmenttitlelist = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
      super(manager);
    }

    @Override
    public Fragment getItem(int position) {
      return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
      return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
      mFragmentList.add(fragment);
      mFragmenttitlelist.add(title);
    }
    @Override
    public CharSequence getPageTitle(int position) {
      switch (position) {
        case 0:
        case 1:
        case 2:
       /* case 3:
        case 4:*/
          return mFragmenttitlelist.get(position);
      }
      return null;
    }
    @Override
    public int getItemPosition(Object object) {
      return POSITION_NONE;
    }

  }
}
