package com.food.order.speedzy.Activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Adapter.Review_Adapter;
import com.food.order.speedzy.Model.Feedbackmodel;
import com.food.order.speedzy.Model.View_Rating_model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mathivanan on 11/03/17.
 */

public class Reviews extends AppCompatActivity {


    Toolbar toolbar;
    ActionBar actionbar;
    ImageView img;
    TextView name,address,rating_review,rateall;
    RatingBar ratingBar;
    String Address="";
    String Reviews="";
    TextView suburb_txtview;
    String suburb,postcode,rest_id;
    float Rating;
    String NAME="";
    String Image ;
    RecyclerView reviewrecyclerview;
    Review_Adapter ra;
    LinearLayoutManager lm;
    TextView reviewedit;
    String ratedfood,ratedspeed,ratedvalue;
    EditText comment;
    TextView submit;
    MySharedPrefrencesData mypreferences;
    View_Rating_model viewratingmodel;
    ArrayList<View_Rating_model.Review> reviewlist;
    ArrayList<View_Rating_model> viewarraylist;
    public static final int yellow= 0xFFFFC107;
    Dialog dialog,dialog_edit;
    TextView dis;
    TextView warn,msg;
    public static int sCorner = 15;
    public static int sMargin = 8;
    Context context;
    @Override
    protected void onCreate(Bundle savedinstance){
        super.onCreate(savedinstance);

        setContentView(R.layout.review_layout);
        context= Reviews.this;
        dialog_edit = new Dialog(Reviews.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog_edit.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_edit.setContentView(R.layout.reviewfeedback_layout);
        comment = (EditText) dialog_edit.findViewById(R.id.comment);

        dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
        dis=(TextView) dialog.findViewById(R.id.dismiss);
        warn=(TextView)dialog.findViewById(R.id.warning);
        msg=(TextView)dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialog_edit.dismiss();
                getreviewapi();
            }
        });
        mypreferences=new MySharedPrefrencesData();
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.red));
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionbar = getSupportActionBar();
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        actionbar.setDisplayHomeAsUpEnabled(true);
        img = (ImageView) findViewById(R.id.img);
        reviewlist=new ArrayList<>();
        lm = new LinearLayoutManager(this);
        name = (TextView) findViewById(R.id.name);
        address = (TextView) findViewById(R.id.address);
        suburb_txtview=findViewById(R.id.suburb);
        rating_review = (TextView) findViewById(R.id.rating_review);
        rateall = (TextView) findViewById(R.id.rate_line_with_all);
        ratingBar = (RatingBar) findViewById(R.id.rating);
        reviewrecyclerview = (RecyclerView) findViewById(R.id.reviewrecycler);
        reviewrecyclerview.setLayoutManager(lm);
        reviewrecyclerview.setItemAnimator(new DefaultItemAnimator());
       // reviewrecyclerview.addItemDecoration(new DividerItemDecoration(context));
        reviewrecyclerview.setHasFixedSize(true);

        Image = getIntent().getStringExtra("Image");
        NAME = getIntent().getStringExtra("Name");
        Address = getIntent().getStringExtra("Address");
        Reviews = getIntent().getStringExtra("Reviews");
        suburb = getIntent().getStringExtra("Suburb");
        postcode = getIntent().getStringExtra("postcode");
        rest_id = getIntent().getStringExtra("rest_id");
        Rating = getIntent().getFloatExtra("Rating",0);
        if(Image==null){
            img.setImageResource(R.drawable.app_logo);
        }else {

            Glide.with(this)
                    .load(Image)
                    .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder))
                    .into(img);
        }
        name.setText(NAME);
        suburb_txtview.setText(suburb);
        address.setText(Address);
        ratingBar.setRating(Rating);
        rating_review.setText("Rating "+Rating+" & "+Reviews+" review ");
        reviewedit=(TextView)findViewById(R.id.reviewedit);
        getreviewapi();
        reviewedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog_edit = new Dialog(Reviews.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                dialog_edit.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_edit.setContentView(R.layout.reviewfeedback_layout);
                comment = (EditText) dialog_edit.findViewById(R.id.comment);
                submit = (TextView) dialog_edit.findViewById(R.id.feedback);
                RatingBar ratingBar = (RatingBar) dialog_edit.findViewById(R.id.rating_food);
                LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);

                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating,
                                                boolean fromUser) {
                        ratedfood = String.valueOf(ratingBar.getRating());

                    }
                });

                RatingBar ratingBar1 = (RatingBar) dialog_edit.findViewById(R.id.rating_value);
                LayerDrawable stars1= (LayerDrawable) ratingBar1.getProgressDrawable();
                stars1.getDrawable(2).setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
                stars1.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                stars1.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);

                ratingBar1.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating,
                                                boolean fromUser) {
                        ratedvalue = String.valueOf(ratingBar.getRating());

                    }
                });


                RatingBar ratingBar2 = (RatingBar) dialog_edit.findViewById(R.id.rating_speed);
                LayerDrawable stars2 = (LayerDrawable) ratingBar2.getProgressDrawable();
                stars2.getDrawable(2).setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
                stars2.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                stars2.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);

                ratingBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating,
                                                boolean fromUser) {
                        ratedspeed = String.valueOf(ratingBar.getRating());

                    }
                });
                dialog_edit.show();
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callapisubmit();

                    }
                });

            }
        });

//        ra = new Review_Adapter(this);
//        reviewrecyclerview.setAdapter(ra);



    }

    private void getreviewapi() {

        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);

        Call<View_Rating_model> call =methods.getreview(rest_id);
        Log.d("url","url="+call.request().url().toString());

        call.enqueue(new Callback<View_Rating_model>() {
            @Override
            public void onResponse(Call<View_Rating_model> call, Response<View_Rating_model> response) {
                int statusCode = response.code();
                Log.d("Response",""+statusCode);
                Log.d("respones",""+response);
                viewratingmodel=response.body();
                if(viewratingmodel.getStatus().equalsIgnoreCase("Success"))
                {
                    double d=0.0,d1=0.0,d2=0.0,d3=0.0;
                    if (viewratingmodel.getOveral().getOveral()!=null) {
                         d = Double.parseDouble(viewratingmodel.getOveral().getOveral());
                    }
                    if (viewratingmodel.getOveral().getFoodRating()!=null) {
                         d1 = Double.parseDouble(viewratingmodel.getOveral().getFoodRating());
                    }
                    if (viewratingmodel.getOveral().getValueRating()!=null) {
                         d2 = Double.parseDouble(viewratingmodel.getOveral().getValueRating());
                    }
                    if (viewratingmodel.getOveral().getSpeedRating()!=null) {
                         d3 = Double.parseDouble(viewratingmodel.getOveral().getSpeedRating());
                    }
                    rateall.setText("Overall "+String.format("%.2f", d)+" | "+"Food "+String.format("%.2f", d1)+" | "+"Value "+String.format("%.2f", d2)+" | "+"Speed "+String.format("%.2f", d3));
                    reviewlist.clear();
                    for(int i=0;i<viewratingmodel.getReview().size();i++){

                        reviewlist.add(viewratingmodel.getReview().get(i));
                    }
                    ra = new Review_Adapter(Reviews.this,reviewlist);
                   reviewrecyclerview.setAdapter(ra);

                }else
                {
                    Toast.makeText(Reviews.this,"There is no review.",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<View_Rating_model> call, Throwable t) {

                //             Toast.makeText(Reviews.this,"internet not available..connect internet",Toast.LENGTH_LONG).show();
            }
        });


    }


    private void callapisubmit() {

        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);

        Call<Feedbackmodel> call =methods.setreview(mypreferences.getUser_Id(this),rest_id,ratedfood,ratedvalue,ratedspeed,comment.getText().toString());
        Log.d("url","url="+call.request().url().toString());

        call.enqueue(new Callback<Feedbackmodel>() {
            @Override
            public void onResponse(Call<Feedbackmodel> call, Response<Feedbackmodel> response) {
                int statusCode = response.code();
                Log.d("Response",""+statusCode);
                Log.d("respones",""+response);
                Feedbackmodel feedback=response.body();
                if(feedback.getStatus().equalsIgnoreCase("Success"))
                {
                    msg.setText(feedback.getMsg().toString());
                    dialog.show();
                }else {
                    msg.setText(feedback.getMsg().toString());
                    dialog.show();
                }
            }

            @Override
            public void onFailure(Call<Feedbackmodel> call, Throwable t) {

   //             Toast.makeText(Reviews.this,"internet not available..connect internet",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Commons.back_button_transition(Reviews.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(Reviews.this);
    }

}
