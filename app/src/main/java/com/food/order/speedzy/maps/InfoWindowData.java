package com.food.order.speedzy.maps;

import androidx.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;

public class InfoWindowData {
  private String pickupAddress;
  private String destinationAddress;
  private String mOrderId;
  private LatLng mPickUp;
  private LatLng mDestination;
  private boolean isPickUpMarker;

  public InfoWindowData(String pickupAddress, String destinationAddress, String orderId,
      LatLng pickUp, LatLng destination) {
    this.pickupAddress = pickupAddress;
    this.destinationAddress = destinationAddress;
    mOrderId = orderId;
    mPickUp = pickUp;
    mDestination = destination;
  }

  public InfoWindowData() {

  }

  public String getPickupAddress() {
    return pickupAddress;
  }

  public void setPickupAddress(String pickupAddress) {
    this.pickupAddress = pickupAddress;
  }

  public String getDestinationAddress() {
    return destinationAddress;
  }

  public void setDestinationAddress(String destinationAddress) {
    this.destinationAddress = destinationAddress;
  }

  public String getOrderId() {
    return mOrderId;
  }

  public void setOrderId(String orderId) {
    mOrderId = orderId;
  }

  public LatLng getPickUp() {
    return mPickUp;
  }

  public void setPickUp(LatLng pickUp) {
    mPickUp = pickUp;
  }

  public LatLng getDestination() {
    return mDestination;
  }

  public void setDestination(LatLng destination) {
    mDestination = destination;
  }

  public boolean isPickUpMarker() {
    return isPickUpMarker;
  }

  public void setPickUpMarker(boolean pickUpMarker) {
    isPickUpMarker = pickUpMarker;
  }

  @NonNull @Override public String toString() {
    return "InfoWindowData{" +
        "pickupAddress='" + pickupAddress + '\'' +
        ", destinationAddress='" + destinationAddress + '\'' +
        ", mOrderId='" + mOrderId + '\'' +
        ", mPickUp=" + mPickUp +
        ", mDestination=" + mDestination +
        '}';
  }
}
