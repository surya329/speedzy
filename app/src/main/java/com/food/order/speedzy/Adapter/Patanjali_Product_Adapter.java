package com.food.order.speedzy.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;

import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;

import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.food.order.speedzy.Activity.PatanjaliProduct_DetailsActivity;
import com.food.order.speedzy.Model.Cart_Model;
import com.food.order.speedzy.Model.Patanjali_Productdetails_Model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.database.LOcaldbNew;
import com.michael.easydialog.EasyDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class Patanjali_Product_Adapter
        extends RecyclerView.Adapter<Patanjali_Product_Adapter.ViewHolder> {

    Context ctx;
    Activity mcontext;
    List<Patanjali_Productdetails_Model.productlist> mData;
    Patanjali_Product_Adapter.OnItemClickListener mOnItemClickListener;
    LOcaldbNew lOcaldbNew;
    ArrayList<Cart_Model.Cart_Details> cd;
    String app_tittle;
    String base_url_main_image = "", base_url_thumb_image;
    String extension = "";
    List<String> itemlist = new ArrayList<>();

    public interface OnItemClickListener {
        void onItemClick(View view, String url);
    }

    public void setOnItemClickListener(
            Patanjali_Product_Adapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public Patanjali_Product_Adapter(Context context, List<Patanjali_Productdetails_Model.productlist> list, String app_tittle) {
        ctx = context;
       // mData = new ArrayList<>();
        this.mData=list;
        this.app_tittle=app_tittle;
    }

   /* public void setData(
            List<Patanjali_Productdetails_Model.productlist> data) {
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void setApp_tittle(String app_tittle) {
        this.app_tittle = app_tittle;
    }

    public void refreshData(
            List<Patanjali_Productdetails_Model.productlist> data) {
        mData.clear();
        setData(data);
    }*/

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgViewimg, ic_addtocart, remove_quantity, add_quantity;
        TextView name, sale_price, mrp, discount, add, quantity_text;
        RelativeLayout linearLayout;
        LinearLayout linear1, quantity_linear;
        RatingBar rating;
        FrameLayout mrp_price;
        ImageView tooltip;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.name);
            imgViewimg = (ImageView) v.findViewById(R.id.imageView1);
            sale_price = (TextView) v.findViewById(R.id.sale_price);
            mrp = (TextView) v.findViewById(R.id.mrp);
            // discount=(TextView) v.findViewById(R.id.discount);
            //ic_addtocart = (ImageView) v.findViewById(R.id.ic_addtocart);
            //addtocart=(LinearLayout) v.findViewById(R.id.addtocart);
            //rating=(RatingBar) v.findViewById(R.id.rating);
            linearLayout = (RelativeLayout) v.findViewById(R.id.linearley1);
            add = (TextView) itemView.findViewById(R.id.add);
            remove_quantity = (ImageView) itemView.findViewById(R.id.remove_quantity);
            add_quantity = (ImageView) itemView.findViewById(R.id.add_quantity);
            quantity_text = (TextView) itemView.findViewById(R.id.quantity_text);
            quantity_linear = (LinearLayout) v.findViewById(R.id.quantity_linear);
            linear1 = (LinearLayout) v.findViewById(R.id.linear1);
            mrp_price = (FrameLayout) v.findViewById(R.id.mrp_price);
            tooltip = v.findViewById(R.id.tooltip);
        }
    }

    @Override
    public Patanjali_Product_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row, parent, false);
        lOcaldbNew = new LOcaldbNew(ctx);
        if (Commons.flag_for_hta == "City Special") {
            cd = lOcaldbNew.getCart_Details_CitySpecial();
            itemlist.clear();
            for (int i = 0; i < cd.size(); i++) {
                if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id())) {
                    itemlist.add(cd.get(i).getIn_dish_id());
                }
            }
        } else if (Commons.flag_for_hta == "Drinking Water") {
            cd = lOcaldbNew.getCart_Details_Water();
            itemlist.clear();
            for (int i = 0; i < cd.size(); i++) {
                if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id())) {
                    itemlist.add(cd.get(i).getIn_dish_id());
                }
            }
        } else {
            cd = lOcaldbNew.getCart_Details_Ptanjali();
            itemlist.clear();
            for (int i = 0; i < cd.size(); i++) {
                if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id())) {
                    itemlist.add(cd.get(i).getIn_dish_id());
                }
            }
        }
        Patanjali_Product_Adapter.ViewHolder vh = new Patanjali_Product_Adapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(Patanjali_Product_Adapter.ViewHolder holder, int position) {
        String main_img = "", thumb_img = "";
        Patanjali_Productdetails_Model.productlist product = mData.get(position);
        holder.name.setText(product.getProductName());
        holder.tooltip.setVisibility(View.VISIBLE);
     /* if (app_tittle.equalsIgnoreCase("City Special")) {
        base_url_thumb_image =
            "https://storage.cloud.google.com/speedzy_data/resources/cityspecial/thumb_image/";
        base_url_main_image =
                //"https://storage.googleapis.com/speedzy_data/resources/cityspecial/main_image/";
            "https://storage.googleapis.com/speedzy_data/resources/cityspecial/main_image/";
        extension = "_1.png";
        main_img = base_url_main_image + product.getProductID() + extension;
        thumb_img = base_url_thumb_image + product.getProductID() + extension;
      } else*/
        if (app_tittle.equalsIgnoreCase("Drinking Water")) {
            base_url_thumb_image =
                    "https://storage.googleapis.com/speedzy_data/resources/water/thumb_image/";
            base_url_main_image =
                    "https://storage.googleapis.com/speedzy_data/resources/water/main_image/";
            extension = "_1.jpg";
            main_img = base_url_main_image + product.getProductID() + extension;
            thumb_img = base_url_thumb_image + product.getProductID() + extension;
        } else {
            base_url_thumb_image =
                    "https://storage.googleapis.com/speedzy_data/resources/patanjali/main_image/thumb_img/";
            base_url_main_image =
                    "https://storage.googleapis.com/speedzy_data/resources/patanjali/main_image/main_image/";
            extension = "_1.jpg";
            main_img = mData.get(position).getImage();
            thumb_img = mData.get(position).getImage();
        }
        RequestBuilder<Drawable> thumbnailRequest = Glide
                .with(ctx)
                .load(thumb_img);
        Glide.with(ctx)
                .load(main_img)
                .thumbnail(thumbnailRequest)
                .apply(new RequestOptions().placeholder(R.drawable.ic_product_coming_soon))
                .into(holder.imgViewimg);

        if (!mData.get(position).getDescription().equalsIgnoreCase("")) {
            holder.tooltip.setVisibility(View.VISIBLE);
        } else {
            holder.tooltip.setVisibility(View.GONE);
        }
        //holder.linear1.setBackgroundTintList(ColorStateList.valueOf(Commons.getRandomColor(mcontext)));
        holder.sale_price.setText("₹ " + product.getSale_price());
        double comparableSalePrice = Commons.getConvertPriceToDouble(product.getSale_price());
        double comparableMRP = Commons.getConvertPriceToDouble(product.getProductMRP());
        if (comparableSalePrice < comparableMRP) {
            holder.mrp_price.setVisibility(View.VISIBLE);
            holder.mrp.setText("₹ " + product.getProductMRP());
        } else {
            holder.mrp_price.setVisibility(View.GONE);
        }
        if (itemlist.contains(mData.get(holder.getAdapterPosition()).getProductID())) {
            int index = itemlist.indexOf(product.getProductID());
            int sqty = Integer.parseInt(cd.get(index).getQuantity());
            if (sqty > 0) {
                holder.quantity_linear.setVisibility(View.VISIBLE);
                holder.add.setVisibility(View.GONE);
                holder.quantity_text.setText(String.valueOf(sqty));
            }
        } else {
            holder.quantity_linear.setVisibility(View.GONE);
            holder.add.setVisibility(View.VISIBLE);
        }
     /* ArrayList<Cart_Model.Cart_Details> cd = null;
      if (Commons.flag_for_hta == "Patanjali") {
          cd = lOcaldbNew.getCart_Details_Ptanjali();
      } else if (Commons.flag_for_hta == "City Special") {
          cd = lOcaldbNew.getCart_Details_CitySpecial();
      } else if (Commons.flag_for_hta == "Drinking Water") {
          cd = lOcaldbNew.getCart_Details_Water();
      }
      if (cd != null) {
          for (int i = 0; i < cd.size(); i++) {
              if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id()) && cd.get(i)
                      .getIn_dish_id()
                      .equalsIgnoreCase(mData.get(holder.getAdapterPosition()).getProductID())) {
                  int sqty = Integer.parseInt(cd.get(i).getQuantity());
                  if (sqty > 0) {
                      holder.quantity_linear.setVisibility(View.VISIBLE);
                      holder.add.setVisibility(View.GONE);
                      holder.quantity_text.setText(String.valueOf(sqty));
                  } else {
                      holder.quantity_linear.setVisibility(View.GONE);
                      holder.add.setVisibility(View.VISIBLE);
                  }
              }
          }
      }*/
        holder.imgViewimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, mData.get(holder.getAdapterPosition()).getImage());
            }
        });

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Commons.flag_for_hta == "City Special") {
                    lOcaldbNew.insertTable_dish_info_CitySpecial(mData.get(holder.getAdapterPosition()).getProductID(),
                            mData.get(holder.getAdapterPosition()).getProductName(),
                            Float.parseFloat(mData.get(holder.getAdapterPosition()).getSale_price()),
                            "1", 1, 0);
                } else if (Commons.flag_for_hta == "Drinking Water") {
                    lOcaldbNew.insertTable_dish_info_Water(mData.get(holder.getAdapterPosition()).getProductID(),
                            mData.get(holder.getAdapterPosition()).getProductName(),
                            Float.parseFloat(mData.get(holder.getAdapterPosition()).getSale_price()),
                            "1", 1, 0);
                }else {
                        lOcaldbNew.insertTable_dish_info_Ptanjali(mData.get(holder.getAdapterPosition()).getProductID(),
                                mData.get(holder.getAdapterPosition()).getProductName(),
                                Float.parseFloat(mData.get(holder.getAdapterPosition()).getSale_price()),
                                "1", 1, 0);
                }
                holder.add.setVisibility(View.GONE);
                holder.quantity_linear.setVisibility(View.VISIBLE);
                holder.quantity_text.setText("1");

                ArrayList<Cart_Model.Cart_Details> cd = null;
                 if (Commons.flag_for_hta == "City Special") {
                    cd = lOcaldbNew.getCart_Details_CitySpecial();
                    PatanjaliProduct_DetailsActivity.setPriceDetails(cd,
                            lOcaldbNew.getQuantity_CitySpecial());
                } else if (Commons.flag_for_hta == "Drinking Water") {
                    cd = lOcaldbNew.getCart_Details_Water();
                    PatanjaliProduct_DetailsActivity.setPriceDetails(cd, lOcaldbNew.getQuantity_Water());
                }else {
                     cd = lOcaldbNew.getCart_Details_Ptanjali();
                     PatanjaliProduct_DetailsActivity.setPriceDetails(cd, lOcaldbNew.getQuantity_Ptanjali());
                 }
            }
        });

        holder.add_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Cart_Model.Cart_Details> cd = null;
                if (Commons.flag_for_hta == "City Special") {
                    cd = lOcaldbNew.getCart_Details_CitySpecial();
                } else if (Commons.flag_for_hta == "Drinking Water") {
                    cd = lOcaldbNew.getCart_Details_Water();
                }else {
                        cd = lOcaldbNew.getCart_Details_Ptanjali();
                }
                if (cd != null) {
                    for (int i = 0; i < cd.size(); i++) {
                        if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id()) && cd.get(i)
                                .getIn_dish_id()
                                .equalsIgnoreCase(mData.get(holder.getAdapterPosition()).getProductID())) {
                            int qty = Integer.parseInt(holder.quantity_text.getText().toString());
                            if (qty < 11) {
                                qty++;
                                cd.get(i).setQuantity(String.valueOf(qty));
                                if (Commons.flag_for_hta == "Patanjali") {
                                    lOcaldbNew.updateCart_Ptanjali(String.valueOf(qty), cd.get(i).getIn_dish_id());
                                } else if (Commons.flag_for_hta == "City Special") {
                                    lOcaldbNew.updateCart_CitySpecial(String.valueOf(qty),
                                            cd.get(i).getIn_dish_id());
                                } else if (Commons.flag_for_hta == "Drinking Water") {
                                    lOcaldbNew.updateCart_Water(String.valueOf(qty), cd.get(i).getIn_dish_id());
                                }
                                holder.quantity_linear.setVisibility(View.VISIBLE);
                                holder.quantity_text.setText(String.valueOf(qty));
                            }
                        }
                    }
                }
                update_price();
            }
        });
        holder.remove_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Cart_Model.Cart_Details> cd = null;
                if (Commons.flag_for_hta == "City Special") {
                    cd = lOcaldbNew.getCart_Details_CitySpecial();
                } else if (Commons.flag_for_hta == "Drinking Water") {
                    cd = lOcaldbNew.getCart_Details_Water();
                }else {
                        cd = lOcaldbNew.getCart_Details_Ptanjali();
                }
                if (cd != null) {
                    for (int i = 0; i < cd.size(); i++) {
                        if (!GeneralUtil.isStringEmpty(cd.get(i).getIn_dish_id()) && cd.get(i)
                                .getIn_dish_id()
                                .equalsIgnoreCase(mData.get(holder.getAdapterPosition()).getProductID())) {
                            int qty = Integer.parseInt(holder.quantity_text.getText().toString());
                            if (qty > 1) {
                                qty--;
                                cd.get(i).setQuantity(String.valueOf(qty));
                                 if (Commons.flag_for_hta == "City Special") {
                                    lOcaldbNew.updateCart_CitySpecial(String.valueOf(qty),
                                            cd.get(i).getIn_dish_id());
                                } else if (Commons.flag_for_hta == "Drinking Water") {
                                    lOcaldbNew.updateCart_Water(String.valueOf(qty), cd.get(i).getIn_dish_id());
                                }else {
                                     lOcaldbNew.updateCart_Ptanjali(String.valueOf(qty), cd.get(i).getIn_dish_id());
                                 }
                                holder.quantity_linear.setVisibility(View.VISIBLE);
                                holder.quantity_text.setText(String.valueOf(qty));
                            } else {
                                if (Commons.flag_for_hta == "City Special") {
                                    lOcaldbNew.deletecart_CitySpecial(cd.get(i).getIn_dish_id());
                                } else if (Commons.flag_for_hta == "Drinking Water") {
                                    lOcaldbNew.deletecart_Water(cd.get(i).getIn_dish_id());
                                }else {
                                        lOcaldbNew.deletecart_Ptanjali(cd.get(i).getIn_dish_id());
                                }
                                holder.quantity_linear.setVisibility(View.GONE);
                                holder.add.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
                update_price();
            }
        });
        holder.tooltip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToolTipView(holder, holder.getAdapterPosition());
            }
        });
    }


    private void ToolTipView(ViewHolder holder, int adapterPosition) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.custom_tooltip, null);
        TextView amount = view.findViewById(R.id.amount);
        TextView desc = view.findViewById(R.id.desc);
        desc.setText(Html.fromHtml(mData.get(adapterPosition).getDescription()));
        amount.setVisibility(View.GONE);
        new EasyDialog(ctx)
                .setLayout(view)
                .setBackgroundColor(ctx.getResources().getColor(R.color.red))
                .setLocationByAttachedView(holder.tooltip)
                .setGravity(EasyDialog.GRAVITY_TOP)
                .setAnimationAlphaShow(100, 0.0f, 1.0f)
                .setAnimationAlphaDismiss(100, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(false)
                .setMarginLeftAndRight(24, 24)
                .setOutsideColor(Color.TRANSPARENT)
                .show();
    }

    private void update_price() {
        ArrayList<Cart_Model.Cart_Details> cd = null;
        if (Commons.flag_for_hta == "City Special") {
            cd = lOcaldbNew.getCart_Details_CitySpecial();
            PatanjaliProduct_DetailsActivity.setPriceDetails(cd,
                    lOcaldbNew.getQuantity_CitySpecial());
        } else if (Commons.flag_for_hta == "Drinking Water") {
            cd = lOcaldbNew.getCart_Details_Water();
            PatanjaliProduct_DetailsActivity.setPriceDetails(cd, lOcaldbNew.getQuantity_Water());
        }else {
                cd = lOcaldbNew.getCart_Details_Ptanjali();
                PatanjaliProduct_DetailsActivity.setPriceDetails(cd, lOcaldbNew.getQuantity_Ptanjali());
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
