package com.food.order.speedzy.RoomBooking;

/**
 * Created by wolfsoft5 on 22/6/18.
 */

public class HotelListModleClass {

    Integer image;

    public HotelListModleClass(Integer image) {
        this.image = image;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }
}
