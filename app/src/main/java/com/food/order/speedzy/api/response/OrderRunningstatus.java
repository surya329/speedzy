package com.food.order.speedzy.api.response;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class OrderRunningstatus implements Serializable {

	@SerializedName("status_title")
	private String statusTitle;

	@SerializedName("status_description")
	private String statusDescription;

	@SerializedName("delivery_person_number")
	private String deliveryPersonNumber;

	@SerializedName("delivery_person_id")
	private String deliveryPersonId;

	@SerializedName("delivery_person_name")
	private String deliveryPersonName;

	public String getDeliveryPersonId() {
		return deliveryPersonId;
	}

	public void setStatusTitle(String statusTitle){
		this.statusTitle = statusTitle;
	}

	public String getStatusTitle(){
		return statusTitle;
	}

	public void setStatusDescription(String statusDescription){
		this.statusDescription = statusDescription;
	}

	public String getStatusDescription(){
		return statusDescription;
	}

	public void setDeliveryPersonNumber(String deliveryPersonNumber){
		this.deliveryPersonNumber = deliveryPersonNumber;
	}

	public String getDeliveryPersonNumber(){
		return deliveryPersonNumber;
	}

	public void setDeliveryPersonName(String deliveryPersonName){
		this.deliveryPersonName = deliveryPersonName;
	}

	public String getDeliveryPersonName(){
		return deliveryPersonName;
	}

	@Override
 	public String toString(){
		return 
			"OrderRunningstatus{" + 
			"status_title = '" + statusTitle + '\'' + 
			",status_description = '" + statusDescription + '\'' + 
			",delivery_person_number = '" + deliveryPersonNumber + '\'' + 
			",delivery_person_name = '" + deliveryPersonName + '\'' + 
			"}";
		}
}