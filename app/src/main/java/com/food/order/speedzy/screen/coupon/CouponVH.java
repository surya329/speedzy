package com.food.order.speedzy.screen.coupon;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.api.response.coupon.CouponItem;

public class CouponVH extends RecyclerView.ViewHolder {
  private final Callback mCallback;
  private final Context mContext;
  @BindView(R.id.txt_short_description) TextView txtShortDescription;
  @BindView(R.id.txt_full_description) TextView txtFullDescription;
  @BindView(R.id.txt_coupon_code) TextView txtCouponCode;

  public CouponVH(@NonNull View itemView,
      Callback callback) {
    super(itemView);
    mCallback = callback;
    mContext = itemView.getContext();
    ButterKnife.bind(this, itemView);
  }

  public void onBindView(CouponItem couponItem) {
    txtCouponCode.setText(couponItem.getStCouponCode());
    txtShortDescription.setText(couponItem.getTitle());
    if (!GeneralUtil.isStringEmpty(couponItem.getDescription())) {
      txtFullDescription.setText(couponItem.getDescription());
    }
  }

  @OnClick(R.id.btn_apply) void onClickApplyButton() {
    if (getAdapterPosition() != RecyclerView.NO_POSITION) {
      mCallback.onClickApplyButton(getAdapterPosition());
    }
  }

  public interface Callback {
    void onClickApplyButton(int position);
  }
}
