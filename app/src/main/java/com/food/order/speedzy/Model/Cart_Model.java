package com.food.order.speedzy.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */



public class Cart_Model implements Serializable {
    private List<Cart_Details> Cart_Details;

    public List<Cart_Model.Cart_Details> getCart_Details() {
        return Cart_Details;
    }

    public void setCart_Details(List<Cart_Model.Cart_Details> cart_Details) {
        Cart_Details = cart_Details;
    }


    public class Cart_Details {

        ArrayList<ArrayList<Preferencemodel>> preferencelfromdb;
        private String in_restaurant_id;
        private String in_dish_id;
        private String st_dish_name;
        private String price_item;
        private String quantity;

        public String getMax_qty_peruser() {
            return max_qty_peruser;
        }

        public void setMax_qty_peruser(String max_qty_peruser) {
            this.max_qty_peruser = max_qty_peruser;
        }

        private String max_qty_peruser="100";
        private String menu_price;
        private String delivery_type;
        private String is_preference;
        private String id;
        private String type;
        private String attribute_name;
        private String attribute_price;
        private String checkbox_attr_name;
        private String checkbox_attr_price;
        private String total_price;
        private String total_price_adapter;
        private String non_veg_status;

        public String getSt_restaurant_image() {
            return st_restaurant_image;
        }

        public void setSt_restaurant_image(String st_restaurant_image) {
            this.st_restaurant_image = st_restaurant_image;
        }

        private String st_restaurant_image="";
        private String st_restaurant_name;
        private String st_street_address;
        private String st_suburb;

        public String getSt_rest_charge_amount() {
            return st_rest_charge_amount;
        }

        public void setSt_rest_charge_amount(String st_rest_charge_amount) {
            this.st_rest_charge_amount = st_rest_charge_amount;
        }

        public String getSt_rest_charge_desc() {
            return st_rest_charge_desc;
        }

        public void setSt_rest_charge_desc(String st_rest_charge_desc) {
            this.st_rest_charge_desc = st_rest_charge_desc;
        }

        private String st_rest_charge_amount;
        private String st_rest_charge_desc;;

        public String getSt_suburb() {
            return st_suburb;
        }

        public void setSt_suburb(String st_suburb) {
            this.st_suburb = st_suburb;
        }

        public String getSt_postcode() {
            return st_postcode;
        }

        public void setSt_postcode(String st_postcode) {
            this.st_postcode = st_postcode;
        }

        private String st_postcode;

        public String getCake_flg() {
            return cake_flg;
        }

        public void setCake_flg(String cake_flg) {
            this.cake_flg = cake_flg;
        }

        private String cake_flg;

        public String getSt_restaurant_name() {
            return st_restaurant_name;
        }

        public void setSt_restaurant_name(String st_restaurant_name) {
            this.st_restaurant_name = st_restaurant_name;
        }

        public String getSt_street_address() {
            return st_street_address;
        }

        public void setSt_street_address(String st_street_address) {
            this.st_street_address = st_street_address;
        }

        public String getIn_restaurant_id() {
            return in_restaurant_id;
        }

        public void setIn_restaurant_id(String in_restaurant_id) {
            this.in_restaurant_id = in_restaurant_id;
        }

        public String getIn_dish_id() {
            return in_dish_id;
        }

        public void setIn_dish_id(String in_dish_id) {
            this.in_dish_id = in_dish_id;
        }

        public String getNon_veg_status() {
            return non_veg_status;
        }

        public void setNon_veg_status(String non_veg_status) {
            this.non_veg_status = non_veg_status;
        }

        public String getSt_dish_name() {
            return st_dish_name;
        }

        public void setSt_dish_name(String st_dish_name) {
            this.st_dish_name = st_dish_name;
        }

        public String getPrice_item() {
            return price_item;
        }

        public void setPrice_item(String price_item) {
            this.price_item = price_item;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getMenu_price() {
            return menu_price;
        }

        public void setMenu_price(String menu_price) {
            this.menu_price = menu_price;
        }

        public String getIs_preference() {
            return is_preference;
        }

        public void setIs_preference(String is_preference) {
            this.is_preference = is_preference;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAttribute_name() {
            return attribute_name;
        }

        public void setAttribute_name(String attribute_name) {
            this.attribute_name = attribute_name;
        }

        public String getAttribute_price() {
            return attribute_price;
        }

        public void setAttribute_price(String attribute_price) {
            this.attribute_price = attribute_price;
        }

        public String getCheckbox_attr_name() {
            return checkbox_attr_name;
        }

        public void setCheckbox_attr_name(String checkbox_attr_name) {
            this.checkbox_attr_name = checkbox_attr_name;
        }

        public String getCheckbox_attr_price() {
            return checkbox_attr_price;
        }

        public void setCheckbox_attr_price(String checkbox_attr_price) {
            this.checkbox_attr_price = checkbox_attr_price;
        }

        public String getTotal_price() {
            return total_price;
        }

        public void setTotal_price(String total_price) {
            this.total_price = total_price;
        }

        public String getTotal_price_adapter() {
            return total_price_adapter;
        }

        public void setTotal_price_adapter(String total_price_adapter) {
            this.total_price_adapter = total_price_adapter;
        }

        public String getDelivery_type() {
            return delivery_type;
        }

        public void setDelivery_type(String delivery_type) {
            this.delivery_type = delivery_type;
        }

        public ArrayList<ArrayList<Preferencemodel>> getPreferencelfromdb() {
            return preferencelfromdb;
        }

        public void setPreferencelfromdb(ArrayList<ArrayList<Preferencemodel>> preferencelfromdb) {
            this.preferencelfromdb = preferencelfromdb;
        }
    }
}
