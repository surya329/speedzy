package com.food.order.speedzy.screen.patanjali;

import com.food.order.speedzy.api.response.grocery.CategoriesItem;
import java.util.ArrayList;
import java.util.List;

public class GroceryCategoryList {
  private String image;

  private List<GrocerySubcategoryList> subCategories;

  private String name;

  private String id;

  private boolean mSelected;

  public boolean isSelected() {
    return mSelected;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public List<GrocerySubcategoryList> getSubCategories() {
    return subCategories;
  }

  public void setSubCategories(
      List<GrocerySubcategoryList> subCategories) {
    this.subCategories = subCategories;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setSelected(boolean selected) {
    mSelected = selected;
  }

  public static List<GroceryCategoryList> transform(List<CategoriesItem> data) {
    List<GroceryCategoryList> mData = new ArrayList<>();
    if (data != null && data.size() > 0) {
      for (CategoriesItem vm : data) {
        GroceryCategoryList groceryCategoryList = new GroceryCategoryList();
        groceryCategoryList.setId(vm.getId());
        groceryCategoryList.setImage(vm.getImage());
        groceryCategoryList.setName(vm.getName());
        groceryCategoryList.setSubCategories(
            GrocerySubcategoryList.transform(vm.getSubCategories()));
        mData.add(groceryCategoryList);
      }
    }
    return mData;
  }
}
