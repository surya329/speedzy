package com.food.order.speedzy.api.response.home;

import java.io.Serializable;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OpenCloseStatus implements Serializable {

	@SerializedName("openstatus")
	private String openstatus;

	@SerializedName("message")
	private String message;

	public void setOpenstatus(String openstatus){
		this.openstatus = openstatus;
	}

	public String getOpenstatus(){
		return openstatus;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"OpenCloseStatus{" + 
			"openstatus = '" + openstatus + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}