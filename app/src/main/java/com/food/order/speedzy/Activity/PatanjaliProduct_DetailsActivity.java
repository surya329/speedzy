package com.food.order.speedzy.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Adapter.MealComboAdapter;
import com.food.order.speedzy.Adapter.Patanjali_Product_Adapter;
import com.food.order.speedzy.Adapter.Patanjali_SubcatListDataAdapter;
import com.food.order.speedzy.Model.Cakes_Model;
import com.food.order.speedzy.Model.Cart_Model;
import com.food.order.speedzy.Model.Patanjali_Productdetails_Model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.EndlessRecyclerViewScrollListener;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedyLinearLayoutManager;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.root.BaseActivity;
import com.food.order.speedzy.screen.patanjali.GroceryChildList;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */

public class PatanjaliProduct_DetailsActivity extends BaseActivity {
    int page = 1;
    private EndlessRecyclerViewScrollListener scrollListener;
    boolean apiCallBlocler = false;
    private Toolbar toolbar;
    LoaderDiloag loaderDiloag;
    RecyclerView recyclerView, recycler_view_list;
    View view;
    LinearLayoutManager lm;
    Patanjali_Product_Adapter ra;
    Patanjali_SubcatListDataAdapter patanjali_subcatListDataAdapter;
    //LinearLayout cart_add_layout;
    //Button viewcart;
    //List<Product_Model> plist = new ArrayList<>();
    String subCategory_id = "", category_id = "", app_tittle;
    Patanjali_Productdetails_Model.delivery_time delivery_time_city_special;
    Patanjali_Productdetails_Model.open_close_status open_close_status;
    List<Patanjali_Productdetails_Model.productlist> list = new ArrayList<>();
    List<Patanjali_Productdetails_Model.banners> water_bannerlist = new ArrayList<>();
    List<Patanjali_Productdetails_Model.deliveryarea> deliveryarea_list = new ArrayList<>();
    List<GroceryChildList> child_subcat =
            new ArrayList<>();
    MySharedPrefrencesData mySharedPrefrencesData;
    String userid = "", city_id = "";
    com.food.order.speedzy.fonts.Product_Sans title;
    static LinearLayout cartaddedlay;
    static TextView item;
    static TextView price;
    ArrayList<Cart_Model.Cart_Details> cd = new ArrayList<>();
    LOcaldbNew lOcaldbNew;
    int quantity;
    private static double totalsum = 0;
    LinearLayout no_product, no_available;
    RelativeLayout linear1;
    int position = 0;
    int egg_flag = 0;
    TextView msg;
    Animation anim;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        loaderDiloag = new LoaderDiloag(this);
        mySharedPrefrencesData = new MySharedPrefrencesData();
        userid = mySharedPrefrencesData.getUser_Id(this);
        city_id = mySharedPrefrencesData.getSelectCity(this);
        app_tittle = getIntent().getStringExtra("app_tittle");
        if (getIntent().getExtras() != null && getIntent().getParcelableArrayListExtra(
                "child_subcat") != null) {
            if (app_tittle.equalsIgnoreCase("Grocery World")) {
                subCategory_id = getIntent().getStringExtra("subcategory_id");
                category_id = getIntent().getStringExtra("category_id");
                child_subcat = getIntent().getParcelableArrayListExtra(
                        "child_subcat");
            }
        }
        if (app_tittle.equalsIgnoreCase("City Special")) {
            subCategory_id = getIntent().getStringExtra("subcategory_id");
            category_id = getIntent().getStringExtra("category_id");
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(
                    ContextCompat.getColor(PatanjaliProduct_DetailsActivity.this, R.color.red));
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (com.food.order.speedzy.fonts.Product_Sans) findViewById(R.id.title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (app_tittle.equalsIgnoreCase("City Special")) {
            title.setText("Super Daily");
        } else {
            title.setText(app_tittle);
        }

        cartaddedlay = (LinearLayout) findViewById(R.id.cart_add_layout);
        item = (TextView) findViewById(R.id.item);
        price = (TextView) findViewById(R.id.price);
        no_product = (LinearLayout) findViewById(R.id.no_product);
        linear1 = (RelativeLayout) findViewById(R.id.linear1);
        no_available = (LinearLayout) findViewById(R.id.no_available);
        msg = findViewById(R.id.msg);

        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(900); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        msg.startAnimation(anim);

        recycler_view_list = (RecyclerView) findViewById(R.id.recycler_view_list);
        view = (View) findViewById(R.id.view);
        recycler_view_list.setHasFixedSize(true);
        recycler_view_list.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycler_view_list.setNestedScrollingEnabled(false);
        recycler_view_list.setVisibility(View.VISIBLE);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        lm = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(lm);
        /*ra = new Patanjali_Product_Adapter(this);
        recyclerView.setAdapter(ra);*/
        patanjali_subcatListDataAdapter =
                new Patanjali_SubcatListDataAdapter(PatanjaliProduct_DetailsActivity.this, child_subcat);
        recycler_view_list.setAdapter(patanjali_subcatListDataAdapter);

        //call_getcart_api();
        cartaddedlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (app_tittle.equalsIgnoreCase("City Special") ||
                        app_tittle.equalsIgnoreCase("Drinking Water") || app_tittle.equalsIgnoreCase("Grocery World")) {
                    Intent intent =
                            new Intent(PatanjaliProduct_DetailsActivity.this, CheckoutActivity_New.class);
                    if (app_tittle.equalsIgnoreCase("City Special") ||
                            app_tittle.equalsIgnoreCase("Drinking Water")) {
                        intent.putExtra("start_time_lunch", delivery_time_city_special.getMorning_start());
                        intent.putExtra("end_time_lunch", delivery_time_city_special.getMorning_end());
                        intent.putExtra("start_time_dinner", delivery_time_city_special.getEvening_start());
                        intent.putExtra("end_time_dinner", delivery_time_city_special.getEvening_end());
                        intent.putExtra("app_openstatus", open_close_status.getOpenstatus());
                    }
                    startActivityForResult(intent, 22);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
            }
        });
        patanjali_subcatListDataAdapter.setOnItemClickListener(
                new Patanjali_SubcatListDataAdapter.OnItemClickListener() {
                    public void onItemClick(View v, int position1) {
                        System.out.println("Clicked View");
                        //callProductApi(list.get(position).getCategory_ID());
                        page = 1;
                        position = position1;
                        if (position1 == 0) {
                            callApi(subCategory_id, "", "");
                        } else {
                            if (!child_subcat.isEmpty()) {
                                callApi(subCategory_id, child_subcat.get(position).getId(), child_subcat.get(position).getName());
                            } else {
                                callApi(subCategory_id, "", "");
                            }
                        }
                    }
                });
        CreateMethodCall();
    }

    private void main_method_call() {
        if (app_tittle.equalsIgnoreCase("City Special")) {
            recycler_view_list.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            callApi_city_special();
        } else if (app_tittle.equalsIgnoreCase("Drinking Water")) {
            callApi_water();
        } else {
     /* if (!child_subcat.isEmpty()) {
        callApi(subCategory_id,child_subcat.get(0).getId(), child_subcat.get(0).getName());
        view.setVisibility(View.VISIBLE);
        recycler_view_list.setVisibility(View.VISIBLE);
      } else {
        callApi(subCategory_id,"", "");
        view.setVisibility(View.GONE);
        recycler_view_list.setVisibility(View.GONE);
      }*/
            callApi(subCategory_id, "", "");
        }
    }

    private void callApi_water() {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<Patanjali_Productdetails_Model> call =
                methods.getWaterproduct_detail("1", String.valueOf(page), city_id);
        Log.d("url", "url=" + call.request().url().toString());
        loaderDiloag.displayDiloag();
        call.enqueue(new Callback<Patanjali_Productdetails_Model>() {
            @Override
            public void onResponse(Call<Patanjali_Productdetails_Model> call,
                                   Response<Patanjali_Productdetails_Model> response) {
                int statusCode = response.code();
                apiCallBlocler = true;
                list.clear();
                Patanjali_Productdetails_Model epm = response.body();
                if (epm != null) {
                    page++;
                    list.addAll(epm.getProductlist());
                    water_bannerlist = epm.getBanners();
                    delivery_time_city_special = epm.getDelivery_time();
                    open_close_status = epm.getOpen_close_status();
                    if (epm.getApp_openstatus().equalsIgnoreCase("0")) {
                        linear1.setVisibility(View.GONE);
                        no_available.setVisibility(View.VISIBLE);
                    } else {
                        linear1.setVisibility(View.VISIBLE);
                        no_available.setVisibility(View.GONE);

                        if (water_bannerlist.size() > 0) {
                            view.setVisibility(View.GONE);
                            recycler_view_list.setVisibility(View.VISIBLE);
                            recycler_view_list.requestLayout();
                            recycler_view_list.setHasFixedSize(true);
                            recycler_view_list.setLayoutManager(
                                    new SpeedyLinearLayoutManager(PatanjaliProduct_DetailsActivity.this,
                                            SpeedyLinearLayoutManager.HORIZONTAL, false));
                            recycler_view_list.setNestedScrollingEnabled(false);
                            SnapHelper snapHelper = new LinearSnapHelper();
                            recycler_view_list.setOnFlingListener(null);
                            snapHelper.attachToRecyclerView(recycler_view_list);
                            MealComboAdapter bannerAdapter =
                                    new MealComboAdapter(2, PatanjaliProduct_DetailsActivity.this, water_bannerlist,
                                            Commons.banner_height, "banner");
                            recycler_view_list.setAdapter(bannerAdapter);
                            bannerAdapter.notifyDataSetChanged();
                        }

                        if (list.size() > 0) {
                            ra = new Patanjali_Product_Adapter(PatanjaliProduct_DetailsActivity.this, list,
                                app_tittle);
                            recyclerView.setAdapter(ra);
                            ra.notifyDataSetChanged();
                           /* ra.setApp_tittle(app_tittle);
                            ra.refreshData(list);*/
                            recyclerView.setVisibility(View.VISIBLE);
                            msg.setVisibility(View.VISIBLE);
                            msg.startAnimation(anim);
                            no_product.setVisibility(View.GONE);

                            ra.setOnItemClickListener(new Patanjali_Product_Adapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, String url) {
                                    dialog_show(url);
                                }
                            });
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            msg.setVisibility(View.GONE);
                            no_product.setVisibility(View.VISIBLE);
                            cartaddedlay.setVisibility(View.GONE);
                        }
                        scrollListener_call(4);
                    }
                }
                loaderDiloag.dismissDiloag();
            }

            @Override
            public void onFailure(Call<Patanjali_Productdetails_Model> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet",
                        Toast.LENGTH_LONG).show();
                loaderDiloag.dismissDiloag();
            }
        });
    }

    private void callApi_city_special() {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<Patanjali_Productdetails_Model> call =
                methods.getCitySpecialproduct_detail(/*"1",*/ String.valueOf(page), city_id, subCategory_id);
        Log.d("url", "url=" + call.request().url().toString());
        loaderDiloag.displayDiloag();
        call.enqueue(new Callback<Patanjali_Productdetails_Model>() {
            @Override
            public void onResponse(@NonNull Call<Patanjali_Productdetails_Model> call,
                                   Response<Patanjali_Productdetails_Model> response) {
                loaderDiloag.dismissDiloag();
                int statusCode = response.code();
                apiCallBlocler = true;
                list.clear();
                Patanjali_Productdetails_Model epm = response.body();
                if (epm != null) {
                    page++;
                    list.addAll(epm.getProductlist());
                    Commons.min_order_grocery = Double.parseDouble(epm.getMinorder());
                    delivery_time_city_special = epm.getDelivery_time();
                    open_close_status = epm.getOpen_close_status();
                    if (epm.getApp_openstatus().equalsIgnoreCase("0")) {
                        linear1.setVisibility(View.GONE);
                        no_available.setVisibility(View.VISIBLE);
                    } else {
                        linear1.setVisibility(View.VISIBLE);
                        no_available.setVisibility(View.GONE);
                        if (list.size() > 0) {
                             ra = new Patanjali_Product_Adapter(PatanjaliProduct_DetailsActivity.this, list,
                               app_tittle);
                            recyclerView.setAdapter(ra);
                            ra.notifyDataSetChanged();
                           /* ra.setApp_tittle(app_tittle);
                            ra.refreshData(list);*/
                            recyclerView.setVisibility(View.VISIBLE);
                            msg.setVisibility(View.VISIBLE);
                            msg.startAnimation(anim);
                            no_product.setVisibility(View.GONE);

                            ra.setOnItemClickListener(new Patanjali_Product_Adapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, String url) {
                                    dialog_show(url);
                                }
                            });
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            msg.setVisibility(View.GONE);
                            no_product.setVisibility(View.VISIBLE);
                            cartaddedlay.setVisibility(View.GONE);
                        }
                        scrollListener_call(1);
                    }
                }
            }

            @Override
            public void onFailure(Call<Patanjali_Productdetails_Model> call, Throwable t) {
                loaderDiloag.dismissDiloag();
            }
        });
    }

    private void scrollListener_call(final int i) {
        scrollListener = new EndlessRecyclerViewScrollListener(lm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (apiCallBlocler) {
                    apiCallBlocler = false;
                    if (page > 1) {
                        if (i == 1) {
                            callApi_city_special();
                        } else if (i == 2) {
                            if (!child_subcat.isEmpty()) {
                                callApi(subCategory_id, child_subcat.get(position).getId(), child_subcat.get(position).getName());
                            } else {
                                callApi(subCategory_id, "", "");
                            }
                        } else if (i == 4) {
                            callApi_water();
                        }
                    }
                }
            }
        };
        recyclerView.addOnScrollListener(scrollListener);
    }

    void callApi(String subCategory_id, String childcat_id, String name) {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<Patanjali_Productdetails_Model> call =
                methods.getpatanjaliproduct_detail(String.valueOf(page), "patanjali", category_id,
                        subCategory_id,/*name*/"", childcat_id);
        Log.d("url", "url=" + call.request().url().toString());
        loaderDiloag.displayDiloag();
        call.enqueue(new Callback<Patanjali_Productdetails_Model>() {
            @Override
            public void onResponse(Call<Patanjali_Productdetails_Model> call,
                                   Response<Patanjali_Productdetails_Model> response) {
                int statusCode = response.code();
                apiCallBlocler = true;
                Patanjali_Productdetails_Model epm = response.body();
                linear1.setVisibility(View.VISIBLE);
                no_available.setVisibility(View.GONE);
                list.clear();
                if (epm != null) {
                    page++;
                    deliveryarea_list.clear();
                    list.addAll(epm.getProductlist());
                    Commons.min_order_grocery = Double.parseDouble(epm.getMinorder());
                    for (int i = 0; i < epm.getDeliveryarea().size(); i++) {
                        deliveryarea_list.add(epm.getDeliveryarea().get(i));
                    }
                    if (list.size() > 0) {
                        ra = new Patanjali_Product_Adapter(PatanjaliProduct_DetailsActivity.this, list,
                            app_tittle);
                        recyclerView.setAdapter(ra);
                        ra.notifyDataSetChanged();
                        /*ra.setApp_tittle(app_tittle);
                        ra.refreshData(list);*/
                        recyclerView.setVisibility(View.VISIBLE);
                        msg.setVisibility(View.VISIBLE);
                        msg.startAnimation(anim);
                        no_product.setVisibility(View.GONE);

                        ra.setOnItemClickListener(new Patanjali_Product_Adapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, String url) {
                                dialog_show(url);
                            }
                        });
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        msg.setVisibility(View.GONE);
                        no_product.setVisibility(View.VISIBLE);
                        cartaddedlay.setVisibility(View.GONE);
                    }
                    scrollListener_call(2);
                }
                loaderDiloag.dismissDiloag();
            }

            @Override
            public void onFailure(Call<Patanjali_Productdetails_Model> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet",
                        Toast.LENGTH_LONG).show();
                loaderDiloag.dismissDiloag();
            }
        });
    }

    private void dialog_show(String url) {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.image_popup);
        PhotoView imgViewimg = (PhotoView) dialog.findViewById(R.id.imageView1);
        ImageView back = (ImageView) dialog.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if (url != null) {
            Glide.with(this)
                    .load(url)
                    .apply(new RequestOptions()
                            .error(R.drawable.ic_product_coming_soon))
                    .into(imgViewimg);
        } else {
            Glide
                    .with(this)
                    .load(R.drawable.ic_product_coming_soon)
                    .into(imgViewimg);
        }

        dialog.show();
    }

    public static void setPriceDetails(ArrayList<Cart_Model.Cart_Details> listdata, int quantity) {
        if (listdata.size() > 0) {
            totalsum = 0.00;
            int qty = 0;
            for (int k = 0; k < listdata.size(); k++) {
                final Cart_Model.Cart_Details cart_details = listdata.get(k);
                qty = Integer.parseInt(cart_details.getQuantity());
                double dishPrice = Double.parseDouble(cart_details.getMenu_price()) * qty;
                totalsum = totalsum + dishPrice;
            }
        }
        if (quantity == 0) {
            try {
                cartaddedlay.setVisibility(View.GONE);
            } catch (Exception e) {
            }
        } else {
            try {
                cartaddedlay.setVisibility(View.VISIBLE);
                item.setText(String.valueOf(quantity));
                price.setText("₹" + String.format("%.2f", totalsum));
            } catch (Exception e) {

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Commons.back_button_transition(PatanjaliProduct_DetailsActivity.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(PatanjaliProduct_DetailsActivity.this);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == RESULT_OK && requestCode == 22) {
           CreateMethodCall();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void CreateMethodCall() {
        lOcaldbNew = new LOcaldbNew(this);
        if (Commons.flag_for_hta== "City Special") {
            cd = lOcaldbNew.getCart_Details_CitySpecial();
            quantity = lOcaldbNew.getQuantity_CitySpecial();
            setPriceDetails(cd, quantity);
        } else if (Commons.flag_for_hta=="Drinking Water"){
            cd = lOcaldbNew.getCart_Details_Water();
            quantity = lOcaldbNew.getQuantity_Water();
            setPriceDetails(cd, quantity);
        }else {
            cd = lOcaldbNew.getCart_Details_Ptanjali();
            quantity = lOcaldbNew.getQuantity_Ptanjali();
            setPriceDetails(cd, quantity);
        }
        page = 1;
        main_method_call();
    }
}