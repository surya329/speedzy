package com.food.order.speedzy.api.response.upload.otp;

public class UploadOTP{
	private String number;
	private String oTP;
	private String status;

	public void setNumber(String number){
		this.number = number;
	}

	public String getNumber(){
		return number;
	}

	public void setOTP(String oTP){
		this.oTP = oTP;
	}

	public String getOTP(){
		return oTP;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"UploadOTP{" + 
			"number = '" + number + '\'' + 
			",oTP = '" + oTP + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
