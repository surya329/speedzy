package com.food.order.speedzy.maps;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import com.food.order.speedzy.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoAdapter implements GoogleMap.InfoWindowAdapter {
  private final FragmentActivity mContext;

  public CustomInfoAdapter(FragmentActivity context) {
    mContext = context;
  }

  @Override public View getInfoWindow(Marker marker) {
    @SuppressLint("InflateParams") View view =
            mContext.getLayoutInflater().inflate(R.layout.view_launch_marker, null);
    TextView txtPickup = view.findViewById(R.id.txt_pickup);
    TextView txtDrop = view.findViewById(R.id.txt_drop);
    InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();
    if (infoWindowData != null
            && infoWindowData.isPickUpMarker()
            && txtPickup != null
            && txtDrop != null) {
      txtPickup.setText(infoWindowData.getPickupAddress());
      txtPickup.setVisibility(View.VISIBLE);
      txtDrop.setVisibility(View.GONE);
    } else if (infoWindowData != null && txtDrop != null && txtPickup != null) {
      txtDrop.setText(infoWindowData.getDestinationAddress());
      txtPickup.setVisibility(View.GONE);
      txtDrop.setVisibility(View.VISIBLE);
    }
    return view;
  }

  @Override public View getInfoContents(Marker marker) {
//    @SuppressLint("InflateParams") View view =
//        mContext.getLayoutInflater().inflate(R.layout.view_launch_marker, null);
//    TextView txtPickup = view.findViewById(R.id.txt_pickup);
//    TextView txtDrop = view.findViewById(R.id.txt_drop);
//    InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();
//    if (infoWindowData != null
//        && infoWindowData.isPickUpMarker()
//        && txtPickup != null
//        && txtDrop != null) {
//      txtPickup.setText(infoWindowData.getPickupAddress());
//      txtPickup.setVisibility(View.VISIBLE);
//      txtDrop.setVisibility(View.GONE);
//    } else if (infoWindowData != null && txtDrop != null && txtPickup != null) {
//      txtDrop.setText(infoWindowData.getDestinationAddress());
//      txtPickup.setVisibility(View.GONE);
//      txtDrop.setVisibility(View.VISIBLE);
//    }
    return null;
  }
}
