package com.food.order.speedzy.CitySpecial;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.R;

import java.util.ArrayList;
import java.util.List;

public class CitySpecialSubCategoriesAdapter extends RecyclerView.Adapter<SubCategoriesVH> {
    private final CitySpecialSubCategoriesAdapter.Callback mCallback;
    private List<CitySpecialCategoryModel.Category.SubCategory> mData;
    String category_id = "";

    public CitySpecialSubCategoriesAdapter(
            CitySpecialSubCategoriesAdapter.Callback callback) {
        this.mData = new ArrayList<>();
        mCallback = callback;
    }

    @NonNull
    @Override
    public SubCategoriesVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view =
                LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.image_view, viewGroup, false);
        return new SubCategoriesVH(view, new SubCategoriesVH.Callback() {
            @Override public void onClickItem(int position) {
                mCallback.onClickItem(mData.get(position));
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoriesVH HomeSubCategoriesVH, int i) {
        HomeSubCategoriesVH.onBindView(mData.get(i));
    }

    @Override public int getItemCount() {
        return mData.size();
    }

    public void setData(
            CitySpecialCategoryModel.Category data) {
        mData.addAll(data.getSubCategories());
        notifyDataSetChanged();
    }

    public void refreshData(
            CitySpecialCategoryModel.Category data) {
        mData.clear();
        setData(data);
    }

    public void clearAdapter() {
        mData.clear();
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public interface Callback {

        void onClickItem(CitySpecialCategoryModel.Category.SubCategory integer);
    }
}

