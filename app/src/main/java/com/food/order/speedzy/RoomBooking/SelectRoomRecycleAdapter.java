package com.food.order.speedzy.RoomBooking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.R;

import java.util.List;


public class SelectRoomRecycleAdapter extends RecyclerView.Adapter<SelectRoomRecycleAdapter.MyViewHolder>{

    Activity context;

    public static int sCorner = 30;
    public static int sMargin = 0;

    private List<SelectRoomModelClass> OfferList;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView imageView,pay_img;

        TextView title,payment,confirmation,book;


        public MyViewHolder(View view) {
            super(view);

            title=(TextView)view.findViewById(R.id.title);
            confirmation=(TextView)view.findViewById(R.id.confirmation);
            payment=(TextView)view.findViewById(R.id.payment);
            imageView = (ImageView)view.findViewById(R.id.image);
            pay_img = (ImageView)view.findViewById(R.id.pay_img);
            book=view.findViewById(R.id.book);


        }

    }


    public SelectRoomRecycleAdapter(Activity context, List<SelectRoomModelClass> offerList) {
        this.OfferList = offerList;
        this.context = context;
    }

    @Override
    public SelectRoomRecycleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_select_room, parent, false);


        return new SelectRoomRecycleAdapter.MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final  int position) {
        SelectRoomModelClass lists = OfferList.get(position);
        holder.title.setText(lists.getTitle());
        holder.confirmation.setText(lists.getConfirmation());
        holder.payment.setText(lists.getPayment());
        holder.pay_img.setImageResource(lists.getPay_img());
        Glide
                .with(context)
                .load(lists.getImage())
                .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder).diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.imageView);


        if(position ==1){

            holder.confirmation.setTextColor(Color.parseColor("#ed4a47"));

        }else {

            holder.confirmation.setTextColor(Color.parseColor("#26b63a"));
        }
        holder.book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ReviewBookingActivity.class);
                context.startActivity(i);
                context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });

    }



    @Override
    public int getItemCount() {
        return OfferList.size();

    }

}


