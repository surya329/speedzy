package com.food.order.speedzy.Model;

import com.food.order.speedzy.api.response.OrderDetailItem;
import java.util.ArrayList;
import java.util.List;

public class OrderDataModel {
  private String mDishName;
  private String mPrice;
  private String mQuantity;

  public String getDishName() {
    return mDishName;
  }

  public void setDishName(String dishName) {
    mDishName = dishName;
  }

  public String getPrice() {
    return mPrice;
  }

  public void setPrice(String price) {
    mPrice = price;
  }

  public String getQuantity() {
    return mQuantity;
  }

  public void setQuantity(String quantity) {
    mQuantity = quantity;
  }

  public static List<OrderDataModel> transform(List<OrderDetailItem> data) {
    List<OrderDataModel> mData = new ArrayList<>();
    for (OrderDetailItem vm : data) {
      OrderDataModel orderDataModel = new OrderDataModel();
      orderDataModel.setDishName(vm.getDishname());
      orderDataModel.setPrice(vm.getPrice());
      orderDataModel.setQuantity(vm.getQty());
      mData.add(orderDataModel);
    }
    return mData;
  }

  public static List<OrderDataModel> transformOrderDetails(
      List<Patanjali_order_deatail_model.Grocery.ProductDetail> data) {
    List<OrderDataModel> mData = new ArrayList<>();
    if (data != null && data.size() > 0) {
      for (Patanjali_order_deatail_model.Grocery.ProductDetail vm : data) {
        OrderDataModel orderDataModel = new OrderDataModel();
        orderDataModel.setDishName(vm.getData().getTitle());
        orderDataModel.setPrice(vm.getData().getSalePrice());
        orderDataModel.setQuantity(vm.getQty());
        mData.add(orderDataModel);
      }
    }
    return mData;
  }

  @Override public String toString() {
    return "OrderDataModel{" +
        "mDishName='" + mDishName + '\'' +
        ", mPrice='" + mPrice + '\'' +
        ", mQuantity='" + mQuantity + '\'' +
        '}';
  }
}
