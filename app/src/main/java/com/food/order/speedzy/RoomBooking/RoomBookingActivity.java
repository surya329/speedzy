package com.food.order.speedzy.RoomBooking;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.Activity.Saved_Addresses;
import com.food.order.speedzy.R;
import com.food.order.speedzy.SpeedzyRide.PickupDropActivity;
import com.food.order.speedzy.SpeedzyRide.PlacesAutoCompleteAdapter;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.root.BaseActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RoomBookingActivity extends BaseActivity {

    String Veg_Flag;
    ImageView mClear;
    TextView mSearchEdittext;
    MySharedPrefrencesData mySharedPrefrencesData;
    TextView title;
    Toolbar toolbar;

    TextView search_btn,in_date,in_month_year,in_day,out_date,out_month_year,out_day,room,guest,day;
    LinearLayout check_in,check_out,room_lin,guest_lin;
    List<GuestRoomModel> guest_room_list=new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_booking);

        mySharedPrefrencesData=new MySharedPrefrencesData();
        Places.initialize(this, mySharedPrefrencesData.getMapsApiKey(RoomBookingActivity.this));
        Veg_Flag = mySharedPrefrencesData.getVeg_Flag(RoomBookingActivity.this);
        mClear = (ImageView)findViewById(R.id.clear);
        mSearchEdittext =findViewById(R.id.search_et);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title= findViewById(R.id.title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Veg_Flag.equalsIgnoreCase("1")) {
            statusbar_bg(R.color.red);
        } else {
            statusbar_bg(R.color.red);
        }
        mSearchEdittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RoomBookingActivity.this, Saved_Addresses.class);
                intent.putExtra("change_address", true);
                startActivityForResult(intent, 11);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
        mClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchEdittext.setText("Select your location");
            }
        });

        search_btn=findViewById(R.id.search_btn);
        check_in=findViewById(R.id.check_in);
        check_out=findViewById(R.id.check_out);
        room_lin=findViewById(R.id.room_lin);
        room=findViewById(R.id.room);
        guest=findViewById(R.id.guest);
        day=findViewById(R.id.day);
        guest_lin=findViewById(R.id.guest_lin);
        in_date=findViewById(R.id.in_date);
        in_month_year=findViewById(R.id.in_month_year);
        in_day=findViewById(R.id.in_day);
        out_date=findViewById(R.id.out_date);
        out_month_year=findViewById(R.id.out_month_year);
        out_day=findViewById(R.id.out_day);

        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        String in_year_str = String.valueOf( calendar.get(Calendar.YEAR));
        String in_date_str = String.valueOf( today.getDate());
        String in_month_str = Commons.month( today.getMonth());
        String in_day_str=Commons.day( today.getDay());

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        String out_date_str = String.valueOf( tomorrow.getDate());
        String out_month_str = Commons.month( tomorrow.getMonth());
        String out_year_str =  String.valueOf( calendar.get(Calendar.YEAR));
        String out_day_str=Commons.day( tomorrow.getDay());

        in_date.setText(in_date_str);
        in_month_year.setText(in_month_str+" "+in_year_str);
        in_day.setText(in_day_str);

        out_date.setText(out_date_str);
        out_month_year.setText(out_month_str+" "+out_year_str);
        out_day.setText(out_day_str);


        check_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_date_intent(1);
            }
        });
        check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_date_intent(2);
            }
        });

        GuestRoomModel guestRoomModel=new GuestRoomModel();
        guestRoomModel.setAdult("1");
        guestRoomModel.setChild("0");
        guest_room_list.add(guestRoomModel);

        guest_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RoomBookingActivity.this, SelectGuestRoomsActivity.class);
                i.putExtra("guest_room_list", (Serializable) guest_room_list);
                startActivityForResult(i,3);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
        room_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RoomBookingActivity.this, SelectGuestRoomsActivity.class);
                i.putExtra("guest_room_list", (Serializable) guest_room_list);
                startActivityForResult(i,3);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSearchEdittext.getText().toString().equalsIgnoreCase("")) {
                    Intent intent1 = new Intent(RoomBookingActivity.this, HotelListActivity.class);
                    intent1.putExtra("in_day", in_day.getText().toString().substring(0, 3) + " " + in_date.getText().toString() + " " +
                            in_month_year.getText().toString().substring(0, in_month_year.getText().toString().length() - 4));
                    intent1.putExtra("out_day", out_day.getText().toString().substring(0, 3) + " " + out_date.getText().toString() + " " +
                            out_month_year.getText().toString().substring(0, out_month_year.getText().toString().length() - 4));
                    intent1.putExtra("room", room.getText().toString());
                    intent1.putExtra("guest", guest.getText().toString());
                    intent1.putExtra("place_name", mSearchEdittext.getText().toString());
                    intent1.putExtra("day", day.getText().toString());
                    startActivity(intent1);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }else {
                    Toast.makeText(getApplicationContext(),"Please first Search to Place",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void select_date_intent(int i) {
        Intent intent1 = new Intent(RoomBookingActivity.this, SelectDateActivity.class);
        intent1.putExtra("flag",i);
        intent1.putExtra("in_date",in_date.getText().toString());
        intent1.putExtra("in_month_year", in_month_year.getText().toString());
        intent1.putExtra("in_day", in_day.getText().toString());
        intent1.putExtra("out_date",out_date.getText().toString());
        intent1.putExtra("out_month_year", out_month_year.getText().toString());
        intent1.putExtra("out_day", out_day.getText().toString());
        startActivityForResult(intent1,1);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==11 && data != null) {
            String message = data.getStringExtra("SELECTED_LOCATION");
            mSearchEdittext.setText(message);
        }else  if(requestCode==3 || requestCode==4){
            if (data!=null) {
                List<GuestRoomModel> guest_room_list_final = (List<GuestRoomModel>) data.getSerializableExtra("guest_room_list");
                guest_room_list.clear();
                guest_room_list.addAll(guest_room_list_final);
                int room_size = guest_room_list.size();
                int guest_size = 0;
                for (int i = 0; i < guest_room_list.size(); i++) {
                    guest_size = guest_size + Integer.parseInt(guest_room_list.get(i).getAdult()) +
                            Integer.parseInt(guest_room_list.get(i).getChild());
                }
                room.setText(String.format("%02d", room_size));
                guest.setText(String.format("%02d", guest_size));
            }
        }else {
            if (data!=null) {
                String in_date_str = data.getStringExtra("in_date");
                String in_month_year_str = data.getStringExtra("in_month_year");
                String in_day_str = data.getStringExtra("in_day");

                String out_date_str = data.getStringExtra("out_date");
                String out_month_year_str = data.getStringExtra("out_month_year");
                String out_day_str = data.getStringExtra("out_day");

                String calculate_day = data.getStringExtra("calculate_day");

                in_date.setText(in_date_str);
                in_month_year.setText(in_month_year_str);
                in_day.setText(in_day_str);

                out_date.setText(out_date_str);
                out_month_year.setText(out_month_year_str);
                out_day.setText(out_day_str);
                day.setText(calculate_day);
            }
        }
    }

    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(this);
    }
}
