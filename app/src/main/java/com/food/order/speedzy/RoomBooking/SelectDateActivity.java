package com.food.order.speedzy.RoomBooking;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.fonts.FontCache;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static android.provider.Settings.System.DATE_FORMAT;
import static com.food.order.speedzy.Utils.Commons.day;
import static com.food.order.speedzy.Utils.Commons.month;

public class SelectDateActivity extends AppCompatActivity {
    DateRangeCalendarView calendar;
TextView title;
    TextView done,in_date,in_month_year,in_day,out_date,out_month_year,out_day,day;
    int flag=1;
    String in_date_str,in_month_year_str,in_day_str,out_date_str,out_month_year_str,out_day_str;
    Toolbar toolbar;
    long calculate_day=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_date);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        statusbar_bg(R.color.red);

        flag=getIntent().getIntExtra("flag",1);
        in_date_str=getIntent().getStringExtra("in_date");
        in_month_year_str=getIntent().getStringExtra("in_month_year");
        in_day_str=getIntent().getStringExtra("in_day");
        out_date_str=getIntent().getStringExtra("out_date");
        out_month_year_str=getIntent().getStringExtra("out_month_year");
        out_day_str=getIntent().getStringExtra("out_day");

        calendar=(DateRangeCalendarView)findViewById(R.id.calendar);
        title=(TextView)findViewById(R.id.title);
        in_date=findViewById(R.id.in_date);
        in_month_year=findViewById(R.id.in_month_year);
        in_day=findViewById(R.id.in_day);
        out_date=findViewById(R.id.out_date);
        out_month_year=findViewById(R.id.out_month_year);
        out_day=findViewById(R.id.out_day);
        day=findViewById(R.id.day);
        done=findViewById(R.id.done);
        title.setText("Choose Date");

        in_date.setText(in_date_str);
        in_month_year.setText(in_month_year_str);
        in_day.setText(in_day_str);

        out_date.setText(out_date_str);
        out_month_year.setText(out_month_year_str);
        out_day.setText(out_day_str);


        Typeface typeface = FontCache.getTypeface("ProductSansRegular.ttf", this);
//        Typeface typeface = Typeface.createFromAsset(getAssets(), "LobsterTwo-Regular.ttf");
        calendar.setFonts(typeface);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent();
                intent1.putExtra("in_date",in_date.getText().toString());
                intent1.putExtra("in_month_year", in_month_year.getText().toString());
                intent1.putExtra("in_day", in_day.getText().toString());
                intent1.putExtra("out_date",out_date.getText().toString());
                intent1.putExtra("out_month_year", out_month_year.getText().toString());
                intent1.putExtra("out_day", out_day.getText().toString());
                intent1.putExtra("calculate_day", day.getText().toString());
                setResult(1,intent1);
                Commons.back_button_transition(SelectDateActivity.this);
            }
        });


        calendar.setCalendarListener(new DateRangeCalendarView.CalendarListener() {
            @Override
            public void onFirstDateSelected(Calendar startDate) {
               //todo do something
                String in_date_str = String.valueOf(startDate.getTime().getDate());
                String in_month_str = month(startDate.getTime().getMonth());
                String in_year_str =  String.valueOf( startDate.get(Calendar.YEAR));
                String in_day_str=day(startDate.getTime().getDay());
                if (flag==1) {
                    in_date.setText(in_date_str);
                    in_month_year.setText(in_month_str+" "+in_year_str);
                    in_day.setText(in_day_str);
                }else {
                    out_date.setText(in_date_str);
                    out_month_year.setText(in_month_str+" "+in_year_str);
                    out_day.setText(in_day_str);
                }
                String in=in_date.getText().toString()+"/"+in_month_year.getText().toString().substring(0, in_month_year.getText().toString().length() - 5)+"/"+in_month_year.getText().toString().substring(in_month_year.getText().toString().length()-4);
                String out=out_date.getText().toString()+"/"+in_month_year.getText().toString().substring(0, in_month_year.getText().toString().length() - 5)+"/"+out_month_year.getText().toString().substring(out_month_year.getText().toString().length() - 4);
                day.setText(get_count_of_days(in,out)+" Night");
            }

            @Override
            public void onDateRangeSelected(Calendar startDate, Calendar endDate) {
                if (startDate != null && endDate != null) {
                    String in_date_str = String.valueOf(startDate.getTime().getDate());
                    String in_month_str = month(startDate.getTime().getMonth());
                    String in_year_str = String.valueOf( startDate.get(Calendar.YEAR));
                    String in_day_str= day(startDate.getTime().getDay());

                    String out_date_str = String.valueOf(endDate.getTime().getDate());
                    String out_month_str = month(endDate.getTime().getMonth());
                    String out_year_str =  String.valueOf( endDate.get(Calendar.YEAR));
                    String out_day_str=day(endDate.getTime().getDay());

                    in_date.setText(in_date_str);
                    in_month_year.setText(in_month_str+" "+in_year_str);
                    in_day.setText(in_day_str);

                    out_date.setText(out_date_str);
                    out_month_year.setText(out_month_str+" "+out_year_str);
                    out_day.setText(out_day_str);

                    day.setText(get_count_of_days(in_date_str+"/"+in_month_str+"/"+in_year_str,out_date_str+"/"+out_month_str+"/"+out_year_str)+" Night");
                }
            }

        });
    }

    public String get_count_of_days(String in, String out) {
            SimpleDateFormat myFormat = new SimpleDateFormat("dd/MMM/yyyy");
            long diff=0;
                try{
                Date date1 = myFormat.parse(in);
                Date date2 = myFormat.parse(out);
                diff = date2.getTime() - date1.getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            return String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
    }

    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(this);
    }


}
