package com.food.order.speedzy.api;

import android.content.Context;
import com.food.order.speedzy.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class MapsApiGenerator {
  public static String apiBaseUrl = "";
  private static Gson gson = new GsonBuilder()
      .setLenient()
      .create();

  static String token = "";
  private static OkHttpClient client;

  public static Retrofit provideRetrofit(final Context ctx) {
    apiBaseUrl = "https://maps.googleapis.com/";

    Interceptor interceptor = new Interceptor() {
      @Override
      public Response intercept(Chain chain) throws IOException {
        Request newRequest = chain.request().newBuilder().build();
        Response response = chain.proceed(chain.request());
        return chain.proceed(newRequest);
      }
    };
    OkHttpClient.Builder builder = new OkHttpClient.Builder();

    if (BuildConfig.DEBUG) {
      HttpLoggingInterceptor httpLogging = new HttpLoggingInterceptor();
      httpLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
      builder.addInterceptor(httpLogging);
      HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor((messages) ->
          Timber.i(messages));
      httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
    }

    client = builder.readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .build();

    builder.interceptors().add(interceptor);

    return new Retrofit.Builder()
        .baseUrl(apiBaseUrl)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(client)
        .build();
  }
}
