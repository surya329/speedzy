package com.food.order.speedzy.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.food.order.speedzy.Adapter.GalleryAdapter;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.FileUtil;
import com.food.order.speedzy.Utils.GifSizeFilter;
import com.food.order.speedzy.Utils.Glide4Engine;
import com.food.order.speedzy.Utils.IntentUtils;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedyLinearLayoutManager;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.root.BaseActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;
import com.zhihu.matisse.listener.OnCheckedListener;
import com.zhihu.matisse.listener.OnSelectedListener;
import id.zelory.compressor.Compressor;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class UploadActivity extends BaseActivity implements MultiplePermissionsListener {

  Toolbar toolbar;
  RecyclerView gvGallery;
  LinearLayout gallery;
  LinearLayout camera;
  @BindView(R.id.title)
  TextView txtTitle;
  int TAKE_PICTURE = 11;
  String imageEncoded;
  File mImageFile;
  private Uri imageToUploadUri;
  private MaterialDialog mPermissionDeniedDialog;
  private static final int REQUEST_CODE_CHOOSE = 23;
  private String mSelectedPictureMode = SpeedzyConstants.SELECTED_PICTURE_MODE_GALLERY;
  GalleryAdapter galleryAdapter;
  private StorageReference storageRef, imageRef;
  private List<String> mReviewImageList;
  String suburbid = "1234";
  String ordertype = "1";
  String userid = "";
  LOcaldbNew lOcaldbNew;
  String delivery_charge = "";
  MySharedPrefrencesData mySharedPrefrencesData;
  File compressedFile;
  File acutalImage;

  private List<Uri> mImageUriList;
  //private List<Uri> mCameraImageList;
  //private List<String> mImageList;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_upload);

    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(UploadActivity.this, R.color.red));
    }

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    ButterKnife.bind(this);
    txtTitle.setText("Upload Medicine");
    gallery = (LinearLayout) findViewById(R.id.gallery);
    camera = (LinearLayout) findViewById(R.id.camera);
    initPermissionDialog();
    initComponent();
    gallery.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mSelectedPictureMode = SpeedzyConstants.SELECTED_PICTURE_MODE_GALLERY;
        checkStoragePermisssion();
      }
    });

    camera.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mSelectedPictureMode = SpeedzyConstants.SELECTED_PICTURE_MODE_CAMERA;
        checkStoragePermisssion();
      }
    });
  }

  private void initComponent() {
    mySharedPrefrencesData = new MySharedPrefrencesData();
    userid = mySharedPrefrencesData.getUser_Id(this);
    gvGallery = (RecyclerView) findViewById(R.id.recyclerView);
    //gvGallery.setHasFixedSize(true);
    gvGallery.setLayoutManager(
        new SpeedyLinearLayoutManager(UploadActivity.this, LinearLayout.HORIZONTAL, false));
    mReviewImageList = new ArrayList<>();
    mImageUriList = new ArrayList<>();
    //mCameraImageList = new ArrayList<>();
    //mImageList = new ArrayList<>();
    FirebaseStorage storage = FirebaseStorage.getInstance(SpeedzyConstants.FIREBASE_SPEEDZY_BUCKET);
    storageRef = storage.getReference().child(SpeedzyConstants.FIREBASE_MEDICINE)
        .child(SpeedzyConstants.FIREBASE_ORIGINAL);
    galleryAdapter = new GalleryAdapter(UploadActivity.this, new GalleryAdapter.Callback() {
      @Override
      public void onClickDeleteItem(Uri uri) {
        updateAdapter(uri);
      }
    });
    gvGallery.setAdapter(galleryAdapter);
  }

  private void updateAdapter(Uri uri) {
    ListIterator<Uri> iterator = mImageUriList.listIterator();
    while (iterator.hasNext()) {
      if (iterator.next().equals(uri)) {
        iterator.remove();
      }
    }
    //ListIterator<Uri> iterator2 = mCameraImageList.listIterator();
    //while (iterator2.hasNext()) {
    //  if (iterator2.next().equals(uri)) {
    //    iterator2.remove();
    //  }
    //}
  }

  private void checkStoragePermisssion() {
    Dexter.withActivity(UploadActivity.this)
        .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
        .withListener(UploadActivity.this).check();
  }

  //@Override
  //protected void onActivityResult(int requestCode, int resultCode, Intent data) {
  //  try {
  //    // When an Image is picked
  //    if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK
  //        && null != data) {
  //      // Get the Image from data
  //
  //      String[] filePathColumn = {MediaStore.Images.Media.DATA};
  //      imagesEncodedList = new ArrayList<String>();
  //      if (data.getData() != null) {
  //
  //        Uri mImageUri = data.getData();
  //
  //        // Get the cursor
  //        Cursor cursor = getContentResolver().query(mImageUri,
  //            filePathColumn, null, null, null);
  //        // Move to first row
  //        cursor.moveToFirst();
  //
  //        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
  //        imageEncoded = cursor.getString(columnIndex);
  //        cursor.close();
  //
  //        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
  //        mArrayUri.add(mImageUri);
  //        galleryAdapter = new GalleryAdapter(getApplicationContext(), mArrayUri);
  //        gvGallery.setAdapter(galleryAdapter);
  //      } else {
  //        if (data.getClipData() != null) {
  //          ClipData mClipData = data.getClipData();
  //          ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
  //          for (int i = 0; i < mClipData.getItemCount(); i++) {
  //
  //            ClipData.Item item = mClipData.getItemAt(i);
  //            Uri uri = item.getUri();
  //            mArrayUri.add(uri);
  //            // Get the cursor
  //            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
  //            // Move to first row
  //            cursor.moveToFirst();
  //
  //            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
  //            imageEncoded = cursor.getString(columnIndex);
  //            imagesEncodedList.add(imageEncoded);
  //            cursor.close();
  //
  //            galleryAdapter = new GalleryAdapter(getApplicationContext(), mArrayUri);
  //            gvGallery.setAdapter(galleryAdapter);
  //          }
  //          Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
  //        }
  //      }
  //    } else if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK
  //        && null != data) {
  //      //File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
  //      //Uri uri = FileProvider.getUriForFile(this,
  //      //    this.getApplicationContext().getPackageName() + ".provider", file);
  //      Uri selectedImage = imageToUploadUri;
  //      getContentResolver().notifyChange(selectedImage, null);
  //      //imgUpload.setVisibility(View.GONE);
  //      ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
  //      mArrayUri.add(selectedImage);
  //      galleryAdapter = new GalleryAdapter(getApplicationContext(), mArrayUri);
  //      gvGallery.setAdapter(galleryAdapter);
  //    } else {
  //      Toast.makeText(this, "You haven't picked Image",
  //          Toast.LENGTH_LONG).show();
  //    }
  //  } catch (Exception e) {
  //    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
  //        .show();
  //  }
  //  super.onActivityResult(requestCode, resultCode, data);
  //}

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case android.R.id.home:
        Commons.back_button_transition(UploadActivity.this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    Commons.back_button_transition(UploadActivity.this);
  }

  private void openGallery() {
    if (mSelectedPictureMode.equals(SpeedzyConstants.SELECTED_PICTURE_MODE_GALLERY)) {
      openMatisseGallery();
    } else if (mReviewImageList.size() < 5) {
      Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
      if (cameraIntent.resolveActivity(getPackageManager()) != null) {
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, getImageURI());
        startActivityForResult(cameraIntent, TAKE_PICTURE);
      }
    } else {
      Toast.makeText(this, "Can select maximum 5 images", Toast.LENGTH_SHORT).show();
    }
  }

  private Uri getImageURI() {
    String root = Environment.getExternalStorageDirectory().getAbsolutePath();
    File myDir = new File(root + "/speedzy_images");
    myDir.mkdirs();
    @SuppressLint("SimpleDateFormat") String timeStamp =
        new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    String fname = "Image-" + timeStamp + ".jpg";
    mImageFile = new File(myDir, fname);
    imageToUploadUri = IntentUtils.getImageUri(this, mImageFile);
    return imageToUploadUri;
  }

  private void initPermissionDialog() {
    mPermissionDeniedDialog =
        new MaterialDialog.Builder(this).title(R.string.general_error_permissiondenied)
            .positiveText(R.string.general_label_gotosetting)
            .negativeText(R.string.general_label_cancel)
            .onPositive(new MaterialDialog.SingleButtonCallback() {
              @Override
              public void onClick(
                  @NonNull MaterialDialog dialog,
                  @NonNull DialogAction which) {
                //should open settings page to enable permission
                startActivity(IntentUtils.newSettingsIntent(UploadActivity.this));
              }
            })
            .build();
  }

  @Override
  public void onPermissionsChecked(MultiplePermissionsReport report) {
    if (report.areAllPermissionsGranted()) {
      openGallery();
    } else if (report.isAnyPermissionPermanentlyDenied()) {
      mPermissionDeniedDialog.show();
    }
  }

  @Override
  public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
      PermissionToken token) {
    new MaterialDialog.Builder(this).title(R.string.general_error_permissiondenied)
        .content("Require permissions")
        .positiveText(R.string.general_label_ok)
        .negativeText(R.string.general_label_cancel)
        .onPositive(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(
              @NonNull MaterialDialog dialog,

              @NonNull DialogAction which) {
            //should open settings page to enable permission
            dialog.dismiss();
            token.continuePermissionRequest();
          }
        })
        .onNegative(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog,
              @NonNull DialogAction which) {
            dialog.dismiss();
            token.cancelPermissionRequest();
          }
        })
        .build().show();
  }

  private void openMatisseGallery() {
    if (mImageUriList.size() < 5) {
      Matisse.from(this)
          .choose(MimeType.ofAll(), false)
          .countable(true)
          .capture(false)
          .theme(R.style.Matisse_Zhihu)
          .captureStrategy(
              new CaptureStrategy(true, "com.food.order.speedzy.fileprovider"))
          .maxSelectable(5 - mImageUriList.size())
          .addFilter(new GifSizeFilter(400, 320, 5 * Filter.K * Filter.K))
          .gridExpectedSize(
              getResources().getDimensionPixelSize(R.dimen.size_120))
          .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
          .thumbnailScale(0.85f)
          .imageEngine(new Glide4Engine())
          // for glide-V4
          .setOnSelectedListener(new OnSelectedListener() {
            @Override
            public void onSelected(@NonNull List<Uri> uriList,
                @NonNull List<String> pathList) {
              // DO SOMETHING IMMEDIATELY HERE
            }
          })
          .originalEnable(true)
          .setOnCheckedListener(new OnCheckedListener() {
            @Override
            public void onCheck(boolean isChecked) {
              // DO SOMETHING IMMEDIATELY HERE
            }
          })
          .forResult(REQUEST_CODE_CHOOSE);
    } else {
      Toast.makeText(this, "Can select maximum 5 images", Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    try {
      if (requestCode == REQUEST_CODE_CHOOSE && data != null && resultCode == RESULT_OK) {
        // Get the Image from data
        setData(Matisse.obtainPathResult(data), Matisse.obtainResult(data));
      } else if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
        if (imageToUploadUri != null) {
          Uri selectedImage = imageToUploadUri;
          getContentResolver().notifyChange(selectedImage, null);
          //List<Uri> mArrayUri = new ArrayList<Uri>();
          //mCameraImageList.add(selectedImage);
          mImageUriList.add(selectedImage);
          //mImageList.add(String.valueOf(selectedImage));
          //galleryAdapter = new GalleryAdapter(getApplicationContext(), mArrayUri);
          //gvGallery.setAdapter(galleryAdapter);
          galleryAdapter.refreshData(mImageUriList);
        }
      } else {
        Toast.makeText(this, "You haven't picked Image",
            Toast.LENGTH_LONG).show();
      }
    } catch (Exception e) {
      Log.e("exception", e.getLocalizedMessage());
      //Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
      //    .show();
      showErrorDialog(e.getMessage());
    }
    super.onActivityResult(requestCode, resultCode, data);
  }

  private void setData(List<String> obtainPathResult, List<Uri> obtainResult) {
    //galleryAdapter = new GalleryAdapter(UploadActivity.this, obtainResult);
    //mImageUriList.clear();
    //mImageList.clear();
    //mImageList.addAll(obtainPathResult);
    mImageUriList.addAll(obtainResult);
    galleryAdapter.refreshData(mImageUriList);
  }

  @OnClick(R.id.btn_upload)
  void onClickSubmitButton() {
    if (mImageUriList.size() == 0) {
      Toast.makeText(this, "Select images before placing order", Toast.LENGTH_SHORT).show();
    } else if ((mImageUriList.size() <= 5)) {
      uploadFromFile();
      showPaymentDialog();
    } else {
      Toast.makeText(this, "Maximum selection limit is 5 pictures", Toast.LENGTH_SHORT).show();
    }
  }

  //private void uploadCameraImages() {
  //  if (mCameraImageList.size() > 0) {
  //    showProgressDialog("Uploading images");
  //    for (Uri path : mCameraImageList) {
  //      imageRef = storageRef
  //          .child(Objects.requireNonNull(
  //              path.getLastPathSegment()));
  //      // Create Upload Task
  //      UploadTask uploadTask = imageRef.putFile(path);
  //      mReviewImageList.add(path.getLastPathSegment());
  //      uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
  //        @Override
  //        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
  //        }
  //      })
  //          .addOnFailureListener(new OnFailureListener() {
  //            @Override public void onFailure(@NonNull Exception e) {
  //              Log.e("exception", e.getLocalizedMessage());
  //            }
  //          }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
  //        @Override
  //        public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
  //        }
  //      });
  //      dismissProgressDialog();
  //    }
  //  }
  //}

  private void showPaymentDialog() {
    Commons.dialogMedicineshow_payment(this, "1",
        0.0, suburbid, ordertype, userid, mySharedPrefrencesData, lOcaldbNew,
        "0", null, mReviewImageList);
  }

  //private void uploadFromFile() {
  //  showProgressDialog("Uploading images");
  //  for (String path : mImageList) {
  //    Uri file = Uri.fromFile(new File(path));
  //    imageRef = storageRef
  //        .child(Objects.requireNonNull(
  //            file.getLastPathSegment()));
  //    // Create Upload Task
  //    UploadTask uploadTask = imageRef.putFile(file);
  //    mReviewImageList.add(file.getLastPathSegment());
  //    uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
  //      @Override
  //      public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
  //      }
  //    })
  //        .addOnFailureListener(new OnFailureListener() {
  //          @Override public void onFailure(@NonNull Exception e) {
  //            Log.e("exception", e.getLocalizedMessage());
  //          }
  //        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
  //      @Override
  //      public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
  //      }
  //    });
  //    dismissProgressDialog();
  //  }
  //}

  private void uploadFromFile() {
    showProgressDialog("Uploading images");
    if (mImageUriList.size() > 0) {
      for (Uri path : mImageUriList) {
        try {
          acutalImage = FileUtil.from(this, path);
          compressedFile = new Compressor(this)
              .setCompressFormat(Bitmap.CompressFormat.JPEG)
              .setQuality(75).compressToFile(acutalImage);
        } catch (IOException e) {
          e.printStackTrace();
        }
        String imageName = "";
        //if (!Objects.requireNonNull(path.getLastPathSegment()).contains("jpg")
        //    && !GeneralUtil.isStringEmpty(GeneralUtil.getMimeType(this, path))) {
        //  Uri uri = Uri.fromFile(compressedFile);
        //  imageName = uri.getLastPathSegment() + "." + GeneralUtil.getMimeType(this, uri);
        //} else {
        imageName = Uri.fromFile(compressedFile).getLastPathSegment();
        //}
        imageRef = storageRef
            .child(imageName);
        // Create Upload Task
        UploadTask uploadTask = imageRef.putFile(Uri.fromFile(compressedFile));
        mReviewImageList.add(imageName);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
          @Override
          public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
          }
        })
            .addOnFailureListener(new OnFailureListener() {
              @Override public void onFailure(@NonNull Exception e) {
                Log.e("exception", e.getLocalizedMessage());
              }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
          @Override
          public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
          }
        });
      }
    }
    dismissProgressDialog();
  }
}
