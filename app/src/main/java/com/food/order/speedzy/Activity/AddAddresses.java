package com.food.order.speedzy.Activity;

import android.Manifest;
import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;

import com.food.order.speedzy.Model.UserAddressModel;
import com.food.order.speedzy.SpeedzyRide.PlacesAutoCompleteAdapter;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.root.BaseActivity;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.Model.AddUserAddressModel;
import com.food.order.speedzy.Model.CityListModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.fonts.MySpinnerAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */

public class AddAddresses extends BaseActivity implements
        AdapterView.OnItemSelectedListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, OnMapReadyCallback, LocationListener, PlacesAutoCompleteAdapter.ClickListener {
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private RecyclerView recyclerView;
    ImageView mClear;
    EditText mSearchEdittext;
    RelativeLayout search_layout;
    LinearLayout bottom_sheet;
    TextView location;
    EditText name, mobile, landmark, pincode, other_name;
    LinearLayout home, work, other, other_detail;
    TextView home_txt, work_txt, other_txt, add_address, change, title;
    Spinner city, area;
    MySpinnerAdapter CityAdapter;
    MySpinnerAdapter AreaAdapter;
    ImageView home_img, work_img, other_img, dismiss;
    String type = "1";
    MySharedPrefrencesData mySharedPrefrencesData;
    LOcaldbNew lOcaldbNew;
    String userid = "", Veg_Flag;
    Toolbar toolbar;
    String locality, flag;
    List<CityListModel.cityList> cityList = new ArrayList<>();
    ArrayList<String> cityList_name = new ArrayList<>();
    List<CityListModel.cityList.area_list> area_list_specific = new ArrayList<>();
    List<List<CityListModel.cityList.area_list>> areaList = new ArrayList<>();
    ArrayList<String> areaList_name = new ArrayList<>();
    int city_selected_pos = -1;
    String city_selected = "";
    int area_selected_pos = -1;
    String state, district, lati, longi;
    LinearLayout other_name_linear;
    LocationRequest mLocationRequest;
    private static final int REQUEST_LOCATION = 1;
    private View viewToAttachDisplayerTo;
    boolean network_status = true;
    GoogleApiClient mGoogleApiClient;
    SupportMapFragment mapFragment;
    private GoogleMap mGoogleMap;
    View mapView;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private LatLng mCenterLatLong;
    boolean change_address, edit_address = false;
    Activity context;
    UserAddressModel.info address_data;
    boolean map_zoom = true;
    ImageView my_location;
    View search;
    LoaderDiloag loaderDiloag;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_addresses);

        buildGoogleApiClient();

        viewToAttachDisplayerTo = findViewById(R.id.add_address_activity);

        loaderDiloag = new LoaderDiloag(this);

        context = AddAddresses.this;
        change_address = getIntent().getBooleanExtra("change_address", false);
        edit_address = getIntent().getBooleanExtra("edit_address", false);
        lati = getIntent().getStringExtra("latitude");
        longi = getIntent().getStringExtra("longitude");

        if (!change_address || edit_address) {
            locality = getIntent().getStringExtra("locality");
            flag = getIntent().getStringExtra("flag");
        } else {
            locality = "";
            flag = "0";
        }
        mySharedPrefrencesData = new MySharedPrefrencesData();
        if (!mySharedPrefrencesData.getMapsApiKey(AddAddresses.this).equalsIgnoreCase("")) {
            Places.initialize(this, mySharedPrefrencesData.getMapsApiKey(AddAddresses.this));
        } else {
            Places.initialize(this, "AIzaSyAAJbFwweX1VFQI-1JYa0_bzmZLp8HzFOc");
        }
        lOcaldbNew = new LOcaldbNew(this);
        userid = mySharedPrefrencesData.getUser_Id(context);
        Veg_Flag = mySharedPrefrencesData.getVeg_Flag(context);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = findViewById(R.id.title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (!edit_address) {
            title.setText("Add Address");
        } else {
            title.setText("Save Address");
        }

        if (Veg_Flag.equalsIgnoreCase("1")) {
            statusbar_bg(R.color.red);
        } else {
            statusbar_bg(R.color.red);
        }
        other_detail = findViewById(R.id.other_detail);
        dismiss = findViewById(R.id.dismiss);
        change = (TextView) findViewById(R.id.change);
        add_address = (TextView) findViewById(R.id.add_address);
        home_txt = (TextView) findViewById(R.id.home_txt);
        work_txt = (TextView) findViewById(R.id.work_txt);
        other_txt = (TextView) findViewById(R.id.other_txt);
        home_img = (ImageView) findViewById(R.id.home_img);
        work_img = (ImageView) findViewById(R.id.work_img);
        other_img = (ImageView) findViewById(R.id.other_img);
        home = (LinearLayout) findViewById(R.id.home);
        work = (LinearLayout) findViewById(R.id.work);
        other = (LinearLayout) findViewById(R.id.other);
        name = (EditText) findViewById(R.id.name);
        mobile = (EditText) findViewById(R.id.mobile);
        location = findViewById(R.id.location);
        landmark = (EditText) findViewById(R.id.landmark);
        pincode = (EditText) findViewById(R.id.pincode);
        other_name = (EditText) findViewById(R.id.other_name);
        my_location = findViewById(R.id.my_location);
        other_name_linear = (LinearLayout) findViewById(R.id.other_name_linear);
        city = (Spinner) findViewById(R.id.city);
        area = (Spinner) findViewById(R.id.area);
        search = findViewById(R.id.search);

        search_layout = findViewById(R.id.search_layout);
        mClear = (ImageView) findViewById(R.id.clear);
        recyclerView = (RecyclerView) findViewById(R.id.list_search);
        mSearchEdittext = findViewById(R.id.search_et);
        bottom_sheet = findViewById(R.id.bottom_sheet);

        if (edit_address) {
            address_data = (UserAddressModel.info) getIntent().getSerializableExtra("address_data");
        }
        call_api_citylist();
        my_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = "0";
                current_location_show();
            }
        });
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                other_detail.setVisibility(View.GONE);
                dismiss.setVisibility(View.GONE);
                add_address.setText("Confirm location & Proceed");
            }
        });
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Commons.back_button_transition(context);
                flag = "1";
                location.setText("");
                mSearchEdittext.requestFocus();

                bottom_sheet_visible();
                other_detail.setVisibility(View.GONE);
                dismiss.setVisibility(View.GONE);
                add_address.setText("Confirm location & Proceed");
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "1";
                home_click();
            }
        });
        work.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "2";
                work_click();
            }
        });
        other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "0";
                other_click();
            }
        });

        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (add_address.getText().toString().equalsIgnoreCase("Confirm location & Proceed")) {
                    if (edit_address) {
                        add_address.setText("Save Address");
                    } else {
                        add_address.setText("Add Address");
                    }
                    other_detail.setVisibility(View.VISIBLE);
                    dismiss.setVisibility(View.VISIBLE);
                } else {
                    if (!location.getText().toString().equalsIgnoreCase("")) {
                        if (!name.getText().toString().equalsIgnoreCase("")) {
                            if (!city.getSelectedItem().toString().equalsIgnoreCase("Select City")) {
                                if (!area.getSelectedItem().toString().equalsIgnoreCase("Select Area")) {
                                    if (type.equalsIgnoreCase("0")) {
                                        if (!other_name.getText().toString().equalsIgnoreCase("")) {
                                            apicall();
                                        } else {
                                            Toast.makeText(context, "Enter Other name", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        apicall();
                                    }
                                } else {
                                    Toast.makeText(context, "Select your Area", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(context, "Select your City", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "Enter your Name", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Enter your Address", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        mSearchEdittext.addTextChangedListener(filterTextWatcher);
        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAutoCompleteAdapter.setClickListener(this);
        recyclerView.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();

        //bottom_sheet_in_visible();

        mClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchEdittext.setText("");
            }
        });
        if (edit_address) {
            bottom_sheet_visible();
            add_address.setText("Save Address");
            other_detail.setVisibility(View.VISIBLE);
            dismiss.setVisibility(View.VISIBLE);
        }
    }

    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            other_detail.setVisibility(View.GONE);
            mClear.setVisibility(View.VISIBLE);
            if (!s.toString().equals("")) {
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                if (recyclerView.getVisibility() == View.GONE) {
                    recyclerView.setVisibility(View.VISIBLE);
                }
            } else {
                if (recyclerView.getVisibility() == View.VISIBLE) {
                    recyclerView.setVisibility(View.GONE);
                }
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    @Override
    public void click(Place place) {
        flag = "11";
        mSearchEdittext.setText(place.getAddress());
        mClear.setVisibility(View.VISIBLE);
        location.setText(mSearchEdittext.getText().toString());
        LatLng latLng = place.getLatLng();
        map_zoom = true;
        find_address(1, String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));
        bottom_sheet_visible();
        add_address.setText("Confirm location & Proceed");
    }

    private void bottom_sheet_visible() {
        recyclerView.setVisibility(View.GONE);
        //search_layout.setVisibility(View.GONE);
        search_layout.setVisibility(View.VISIBLE);
        bottom_sheet.setVisibility(View.VISIBLE);
        add_address.setVisibility(View.VISIBLE);
        Commons.hideSoftKeyboard(mSearchEdittext);
    }

    private void bottom_sheet_in_visible() {
        mSearchEdittext.setText("");
        search_layout.setVisibility(View.VISIBLE);
        bottom_sheet.setVisibility(View.GONE);
        add_address.setVisibility(View.GONE);
    }

    private void other_click() {
        background_change(other, other_txt, work, work_txt, home, home_txt);
        other_name_linear_fun(1);
    }

    private void home_click() {
        background_change(home, home_txt, other, other_txt, work, work_txt);
        other_name_linear_fun(0);
    }

    private void work_click() {
        background_change(work, work_txt, home, home_txt, other, other_txt);
        other_name_linear_fun(0);
    }

    private void background_change(LinearLayout select, TextView select_text, LinearLayout unselect1, TextView unselect_text1,
                                   LinearLayout unselect2, TextView unselect_text2) {

        select.setBackground(context.getResources().getDrawable(R.drawable.select_bg));
        select_text.setTextColor(context.getResources().getColor(R.color.white));

        unselect1.setBackground(context.getResources().getDrawable(R.drawable.unselect_bg));
        unselect_text1.setTextColor(context.getResources().getColor(R.color.red));

        unselect2.setBackground(context.getResources().getDrawable(R.drawable.unselect_bg));
        unselect_text2.setTextColor(context.getResources().getColor(R.color.red));
    }

    private void data_show() {
        location.setText(address_data.getComplete_address());
        name.setText(address_data.getName());
        mobile.setText(mySharedPrefrencesData.get_Party_mobile(AddAddresses.this));
        String select_city = address_data.getCityid();
        String select_area = address_data.getArea();
        for (int i = 0; i < cityList.size(); i++) {
            if (select_city.equalsIgnoreCase(cityList.get(i).getCity_id().toString())) {
                city.setSelection(i);
                city_selected_pos = i;
            }
        }
        if (city_selected_pos > 0) {
            for (int i = 0; i < areaList.get(city_selected_pos).size(); i++) {
                areaList_name.add(areaList.get(city_selected_pos).get(i).getSt_suburb());
                AreaAdapter.notifyDataSetChanged();
            }
        }
        for (int i = 0; i < areaList_name.size(); i++) {
            if (select_area.equalsIgnoreCase(areaList_name.get(i).toString())) {
                area.setSelection(i);
            }
        }
        landmark.setText(address_data.getLandmark());
        pincode.setText(address_data.getPincode());
        if (address_data.getAddress_type()
                .equalsIgnoreCase("1")) {
            home_click();
        } else if (address_data.getAddress_type()
                .equalsIgnoreCase("2")) {
            work_click();
        } else if (address_data.getAddress_type()
                .equalsIgnoreCase("3")) {
            other_click();
        }
    }

    private void current_location_show() {
        map_zoom = true;
        if (mGoogleApiClient.isConnected()) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10 * 1000);
            mLocationRequest.setFastestInterval(10 * 1000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (this.mGoogleMap != null) {
            this.mGoogleApiClient.connect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (flag.equalsIgnoreCase("0")) {
            find_address(2, String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (flag.equalsIgnoreCase("0")) {
            current_location_show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void find_address(int flag1, String latitude, String longitude) {
        Log.e("find_address flag= ", "flag" + flag1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                search.clearAnimation();
                search.setVisibility(View.GONE);

            }
        }, 3000);

        Geocoder geocoder = new Geocoder(AddAddresses.this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(latitude),
                    Double.parseDouble(longitude), 1);
            if (addresses != null && addresses.size() > 0) {
                Address location1 = addresses.get(0);
                lati = String.valueOf(location1.getLatitude());
                longi = String.valueOf(location1.getLongitude());
                state = location1.getAdminArea();
                district = location1.getSubAdminArea();
                pincode.setText(location1.getPostalCode());
                String city_name = location1.getLocality();
                if (!change_address) {
                    if (cityList_name.contains(city_name)) {
                        for (int i = 0; i < cityList.size(); i++) {
                            if (location1.getLocality().equalsIgnoreCase(cityList.get(i).getCityName())) {
                                city_selected = cityList.get(i).getCity_id();
                                city.setSelection(i);
                            }
                        }
                    }
                }
                String str = location1.getAddressLine(0);
                if (str != null && flag1 != 1) {
                    location.setText("");
                    String[] new_str = str.split(",");
                    String s = "";
                    for (int i = 0; i < new_str.length; i++) {
                        s = s + new_str[i];
                        if (i == 2) {
                            s = s + "\n";
                        }
                        location.setText(s);
                    }
                }
                mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                if (map_zoom) {
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))      // Sets the center of the map to location user
                            .zoom(17f)                   // Sets the zoom
                            //.bearing(90)                // Sets the orientation of the camera to east
                            //.tilt(40)                   // Sets the tilt of the camera to 30 degrees
                            .build();                   // Creates a CameraPosition from the builder
                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    //  mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(lati), Double.parseDouble(longi)), 13.0f));
                }
                map_zoom = false;

                if (flag1 == 1 || flag1 == 4) {
                    if (!mSearchEdittext.getText().toString().equalsIgnoreCase("")) {
                        location.setText(mSearchEdittext.getText().toString());
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void search_anim() {
        circleAnimation();
        search.setVisibility(View.VISIBLE);
        Display display = getWindowManager().getDefaultDisplay();
        float width = display.getWidth();
        TranslateAnimation animation = new TranslateAnimation(0, width - 50, 0, 0); // new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation.setDuration(1000);
        animation.setRepeatCount(10); // animation repeat count
        animation.setRepeatMode(2); // repeat animation (left to right, right to
        // left )
        // animation.setFillAfter(true);
        search.startAnimation(animation);
        if (!flag.equalsIgnoreCase("11")) {
            location.setText("");
        }
    }

    private void circleAnimation() {
        GradientDrawable d = new GradientDrawable();
        d.setShape(GradientDrawable.OVAL);
        d.setSize(1000, 1000);
        d.setColor(0x55dddddd);
        d.setStroke(5, getResources().getColor(R.color.red));

        Bitmap bitmap = Bitmap.createBitmap(d.getIntrinsicWidth()
                , d.getIntrinsicHeight()
                , Bitmap.Config.ARGB_8888);

        // Convert the drawable to bitmap
        Canvas canvas = new Canvas(bitmap);
        d.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        d.draw(canvas);

        // Radius of the circle
        final int radius = 20;

        // Add the circle to the map
        final GroundOverlay circle = mGoogleMap.addGroundOverlay(new GroundOverlayOptions()
                .position(mGoogleMap.getCameraPosition().target, 10 * radius).image(BitmapDescriptorFactory.fromBitmap(bitmap)));

        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator.setRepeatMode(ValueAnimator.RESTART);
        valueAnimator.setIntValues(0, radius);
        valueAnimator.setDuration(2000);
        valueAnimator.setEvaluator(new IntEvaluator());
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedFraction = valueAnimator.getAnimatedFraction();
                circle.setDimensions(animatedFraction * radius * 10);
            }
        });

        valueAnimator.start();
    }

    private void other_name_linear_fun(int i) {
        if (i == 1) {
            other_name_linear.setVisibility(View.VISIBLE);
        } else {
            other_name_linear.setVisibility(View.GONE);
        }
    }

    private void apicall() {
        callapi_add_address();
    }

   /* private void calldialog_city() {
        final Dialog d = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setContentView(R.layout.deliveryarea_dialog);
        TextView disbtn = (TextView) d.findViewById(R.id.dismissdialog_deliveryarea);
        listView_city = (ListView) d.findViewById(R.id.listview);
        disbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        call_api_citylist();
        ArrayAdapter aa = new ArrayAdapter(context, android.R.layout.test_list_item, cityList_name);
        listView_city.setAdapter(aa);

        listView_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                city_selected_pos = i;
                city.setText(listView_city.getItemAtPosition(i).toString());
                city_selected=cityList.get(i).getCity_id();
                d.dismiss();
            }
        });
        d.show();
    }*/

    private void call_api_citylist() {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<CityListModel> call = methods.get_city_list();
        Log.d("url", "url=" + call.request().url().toString());
        call.enqueue(new Callback<CityListModel>() {
            @Override
            public void onResponse(Call<CityListModel> call, Response<CityListModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                CityListModel cityListModel = response.body();
                cityList.clear();
                cityList_name.clear();
                areaList.clear();
                area_list_specific.clear();
                if (!change_address) {
                    area_list_specific.add(new CityListModel.cityList.area_list("", "Select Area"));
                    areaList.add(area_list_specific);
                    cityList.add(new CityListModel.cityList("", "Select City", area_list_specific));
                    cityList.addAll(cityListModel.getCityList());
                    for (int i = 0; i < cityList.size(); i++) {
                        cityList_name.add(cityList.get(i).getCityName());
                    }
                    for (int i = 0; i < cityListModel.getCityList().size(); i++) {
                        areaList.add(cityListModel.getCityList().get(i).getArea_list());
                    }
                    CityAdapter = new MySpinnerAdapter(context, android.R.layout.simple_spinner_item, cityList_name);
                    CityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    city.setAdapter(CityAdapter);
                    city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            city_selected_pos = position;
                            city_selected = cityList.get(position).getCity_id();
                            areaList_name.clear();
                            areaList_name.add("Select Area");
                            for (int i = 0; i < areaList.get(city_selected_pos).size(); i++) {
                                areaList_name.add(areaList.get(city_selected_pos).get(i).getSt_suburb());
                                AreaAdapter.notifyDataSetChanged();
                            }
                            area.setSelection(0);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    AreaAdapter = new MySpinnerAdapter(context, android.R.layout.simple_spinner_item, areaList_name);
                    AreaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    area.setAdapter(AreaAdapter);
                    area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            if (!city.getSelectedItem().toString().equalsIgnoreCase("Select City")) {
                                area_selected_pos = position;
                            } else {
                                areaList_name.clear();
                                areaList_name.add("Select Area");
                                AreaAdapter.notifyDataSetChanged();
                            }
                            //area_selected = areaList.get(city_selected_pos).get(position).getIn_suburb_id();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                } else {
                    cityList.addAll(cityListModel.getCityList());
                    String city1 = "";
                    if (!mySharedPrefrencesData.getSelectCity(context).equalsIgnoreCase("")) {
                        city1 = mySharedPrefrencesData.getSelectCity(context);
                    } else {
                        city1 = address_data.getCityid();
                    }
                    for (int i = 0; i < cityList.size(); i++) {
                        if (city1.equalsIgnoreCase(cityList.get(i).getCity_id())) {
                            city_selected_pos = cityList.indexOf(city1);
                            cityList_name.add(cityList.get(i).getCityName());
                            city_selected = city1;
                        }
                    }
                    for (int i = 0; i < cityListModel.getCityList().size(); i++) {
                        areaList.add(cityListModel.getCityList().get(i).getArea_list());
                    }
                    CityAdapter = new MySpinnerAdapter(context, android.R.layout.simple_spinner_item, cityList_name);
                    CityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    city.setAdapter(CityAdapter);

                    areaList_name.clear();
                    areaList_name.add("Select Area");
                    for (int i = 0; i < areaList.get(city_selected_pos + 1).size(); i++) {
                        areaList_name.add(areaList.get(city_selected_pos + 1).get(i).getSt_suburb());
                    }
                    area.setSelection(0);

                    AreaAdapter = new MySpinnerAdapter(context, android.R.layout.simple_spinner_item, areaList_name);
                    AreaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    area.setAdapter(AreaAdapter);
                    area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            area_selected_pos = position;
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }

                if (!locality.equalsIgnoreCase("")) {
                    find_address(3, lati, longi);
                } else {
                    if (flag.equalsIgnoreCase("0")) {
                        current_location_show();
                    }
                }
                mySharedPrefrencesData_show();
            }

            @Override
            public void onFailure(Call<CityListModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void mySharedPrefrencesData_show() {
        name.setText(mySharedPrefrencesData.getSelectName(AddAddresses.this));
        mobile.setText(mySharedPrefrencesData.get_Party_mobile(AddAddresses.this));
        if (edit_address) {
            data_show();
        }
    }

    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Commons.back_button_transition(context);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(context);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.setMinZoomPreference(13.0f);
        mGoogleMap.setMaxZoomPreference(25.0f);
        /*try {
            boolean success = mGoogleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.map_style));

            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }*/
        //mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        /*mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                Log.d("Camera postion change" + "", cameraPosition + "");
                mCenterLatLong = cameraPosition.target;
                mGoogleMap.clear();

                try {
                    search_anim();
                    Location mLocation = new Location("");
                    mLocation.setLatitude(mCenterLatLong.latitude);
                    mLocation.setLongitude(mCenterLatLong.longitude);
                    find_address(4,String.valueOf(mCenterLatLong.latitude), String.valueOf(mCenterLatLong.longitude));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });*/
        mGoogleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                flag = "11";
                mGoogleMap.clear();
            }
        });

        mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (!flag.equalsIgnoreCase("0")) {
                    search_anim();
                    mCenterLatLong = mGoogleMap.getCameraPosition().target;
                    Location mLocation = new Location("");
                    mLocation.setLatitude(mCenterLatLong.latitude);
                    mLocation.setLongitude(mCenterLatLong.longitude);
                    find_address(4, String.valueOf(mCenterLatLong.latitude), String.valueOf(mCenterLatLong.longitude));
                    Log.e("LatLong", String.valueOf(mCenterLatLong.latitude) + "\n" +
                            String.valueOf(mCenterLatLong.longitude));
                }
            }
        });

      /*  //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
        }*/
    }

  /*  private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(context,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }*/

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getBaseContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void callapi_add_address() {
        // if (complete_location_str.equalsIgnoreCase("")) {
       /* getLocationFromAddress(1, location.getText().toString()
                + ","
                + area.getSelectedItem().toString().toLowerCase()
                + ","
                + city.getSelectedItem().toString().toLowerCase()
                + pincode.getText().toString());*/
        // }
        String alternative_no = "";
        if (!mobile.getText().toString().equalsIgnoreCase("")) {
            alternative_no = mobile.getText().toString();
        } else {
            alternative_no = "NA";
        }
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<AddUserAddressModel> call = null;
        if (!edit_address) {
            call = methods.add_address(userid, location.getText().toString(), state, district,
                    pincode.getText().toString(), landmark.getText().toString(),
                    "", type, longi, lati,
                    other_name.getText().toString(), name.getText().toString(), alternative_no,
                    area.getSelectedItem().toString(),
                    city_selected);
        } else if (address_data != null && !GeneralUtil.isStringEmpty(address_data.getAddress_id())) {
            call = methods.edit_address(userid, location.getText().toString(), state, district,
                    pincode.getText().toString(), landmark.getText().toString(),
                    "", type, longi, lati,
                    other_name.getText().toString(), name.getText().toString(), alternative_no,
                    area.getSelectedItem().toString(),
                    city_selected, address_data.getAddress_id());
        }
        Log.d("url", "url=" + call.request().url().toString());
        loaderDiloag.displayDiloag();
        call.enqueue(new Callback<AddUserAddressModel>() {
            @Override
            public void onResponse(Call<AddUserAddressModel> call,
                                   Response<AddUserAddressModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                AddUserAddressModel response1 = response.body();
                if (response1.getStatus().equalsIgnoreCase("Success")) {

                    String complete_address = response1.getInfo().getComplete_address().toString();
                    String city_selected1 = city_selected;
                    String longitude_selected = response1.getInfo().getLongitude().toString();
                    String latitude_selected = response1.getInfo().getLatitude().toString();
                    String addressid_selected = response1.getInfo().getAddress_id().toString();
                    String name_str = response1.getInfo().getName().toString();
                    String mobile = response1.getInfo().getMobile().toString();
                    String area = response1.getInfo().getArea().toString();
                    String pincode = response1.getInfo().getPincode().toString();
                    String address_type = response1.getInfo().getAddress_type().toString();
                    String landmark = response1.getInfo().getLandmark().toString();

                    if (city_selected1!=null &&
                            !city_selected1.equalsIgnoreCase("")&&
                            !city_selected1.equalsIgnoreCase("NA")) {
                        mySharedPrefrencesData.setSelectCity(context, city_selected1);
                    }else {
                        mySharedPrefrencesData.setSelectCity(context, "FOODCITYBAM");
                    }
                    mySharedPrefrencesData.setSelectCity_latitude(context, latitude_selected);
                    mySharedPrefrencesData.setSelectCity_longitude(context, longitude_selected);
                    mySharedPrefrencesData.setSelectAddress_ID(context, addressid_selected);
                    mySharedPrefrencesData.setSelectName(context, name.getText().toString());
                    mySharedPrefrencesData.setSelectMobile(context, mobile);
                    mySharedPrefrencesData.setFName(context, name.getText().toString());
                    mySharedPrefrencesData.set_Party_mobile(context, mobile);
                    mySharedPrefrencesData.setSelectPincode(context, pincode);
                    mySharedPrefrencesData.setSelectAddresstype(context, address_type);
                    mySharedPrefrencesData.setSelectArea(context, area);
                    mySharedPrefrencesData.setSelectLandMark(context, landmark);
                    if (edit_address) {
                        mySharedPrefrencesData.setLocation(context, complete_address);
                        Commons.back_button_transition(AddAddresses.this);
                    } else {
                        String type_str = "";
                        if (type.equalsIgnoreCase("0")) {
                            type_str = "Other";
                        }
                        if (type.equalsIgnoreCase("1")) {
                            type_str = "Home";
                        }
                        if (type.equalsIgnoreCase("2")) {
                            type_str = "Work";
                        }
                        if (mySharedPrefrencesData.getLocation(context) == null
                                || mySharedPrefrencesData.getLocation(context).equalsIgnoreCase("")) {
                            mySharedPrefrencesData.setLocation(context, complete_address);
                            Intent intent = new Intent(context, HomeActivityNew.class);
                            intent.putExtra("flag", 1);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                            finish();
                        } else {
                            mySharedPrefrencesData.setLocation(context, complete_address);
                             if (change_address) {
                                Intent intent = new Intent();
                                intent.putExtra("SELECTED_LOCATION", complete_address);
                                intent.putExtra("type", type_str);
                                intent.putExtra("lati", latitude_selected);
                                intent.putExtra("longi", longitude_selected);
                                setResult(1, intent);
                                Commons.back_button_transition(context);
                            }else  {
                                Intent intent = new Intent();
                                intent.putExtra("type", type_str);
                                intent.putExtra("city_selected", city_selected1);
                                intent.putExtra("longitude_selected", longitude_selected);
                                intent.putExtra("latitude_selected", latitude_selected);
                                intent.putExtra("addressid_selected", addressid_selected);
                                intent.putExtra("name", name.getText().toString());
                                intent.putExtra("area", area);
                                intent.putExtra("pincode", pincode);
                                intent.putExtra("address_type", address_type);
                                intent.putExtra("landmark", landmark);
                                intent.putExtra("ADD_ADDRESS", complete_address);
                                setResult(2, intent);
                                Commons.back_button_transition(context);
                            }
                        }
                    }
                }
                    loaderDiloag.dismissDiloag();
        }

        @Override
        public void onFailure (Call < AddUserAddressModel > call, Throwable t){
            loaderDiloag.dismissDiloag();
        }
    });
}
}
