package com.food.order.speedzy.api.response.order;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OrderStatusResponse{

	@SerializedName("data")
	private Data data;

	public Data getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"OrderStatusResponse{" + 
			"data = '" + data + '\'' + 
			"}";
		}
}