package com.food.order.speedzy.screen.patanjali;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.GeneralUtil;

public class PatanjaliSectionListVH extends RecyclerView.ViewHolder {
  private final Callback mCallback;
  private final Context mContext;
  @BindView(R.id.tvTitle) TextView tvTitle;
  @BindView(R.id.online_userViewImage) ImageView itemImage;
  @BindView(R.id.lyt_parent) RelativeLayout lyt_parent;
  @BindView(R.id.framelayout1)
  View framelayout1;

  public PatanjaliSectionListVH(@NonNull View itemView,
      Callback callback) {
    super(itemView);
    mCallback = callback;
    mContext = itemView.getContext();
    ButterKnife.bind(this, itemView);
  }

  public void onBindView(GroceryCategoryList categories) {
    tvTitle.setText(categories.getName());
    if (!GeneralUtil.isStringEmpty(categories.getImage())) {
      circleImage(itemImage, categories.getImage(), true, mContext);
    }
    if (categories.isSelected()) {
      tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.red));
      //circleImage(holder.itemImage,mData.get(i).getImage(),true);
      framelayout1.setVisibility(View.VISIBLE);
    } else {
      tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.grey));
      //circleImage(holder.itemImage,mData.get(i).getImage(),false);
      framelayout1.setVisibility(View.GONE);
    }


  }

  @OnClick(R.id.lyt_parent) void onClickContainerMain() {
    if (getAdapterPosition() != RecyclerView.NO_POSITION) {
      mCallback.onClickItem(getAdapterPosition());
    }
  }

  private static <T> void circleImage(final ImageView imageView, String uri, final boolean border,
      Context mContext) {
    RequestBuilder<Drawable> thumbnailRequest = Glide
        .with(mContext)
        .load(uri)
            .apply(RequestOptions.bitmapTransform(new CircleCrop()));
    Glide.with(mContext)
        .load(uri)
        .thumbnail(thumbnailRequest)
        .into(imageView);
  }

  public interface Callback {
    void onClickItem(int position);
  }
}
