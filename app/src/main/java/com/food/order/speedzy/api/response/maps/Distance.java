package com.food.order.speedzy.api.response.maps;

import com.google.gson.annotations.SerializedName;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Distance{

	@SerializedName("text")
	private String text;

	@SerializedName("value")
	private int value;

	public String getText(){
		return text;
	}

	public int getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"Distance{" + 
			"text = '" + text + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}