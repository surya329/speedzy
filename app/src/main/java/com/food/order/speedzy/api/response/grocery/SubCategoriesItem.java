package com.food.order.speedzy.api.response.grocery;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class SubCategoriesItem{

	//@SerializedName("parent")
	//private String parent;

	@SerializedName("image")
	private String image;

	@SerializedName("child_subcat")
	private ArrayList<ChildSubcatItem> childSubcat;

	//@SerializedName("product_count")
	//private String productCount;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	//public String getParent(){
	//	return parent;
	//}

	public String getImage(){
		return image;
	}

	public ArrayList<ChildSubcatItem> getChildSubcat(){
		return childSubcat;
	}

	//public String getProductCount(){
	//	return productCount;
	//}

	public String getName(){
		return name;
	}

	public String getId(){
		return id;
	}

}