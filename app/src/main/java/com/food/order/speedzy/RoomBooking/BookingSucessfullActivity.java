package com.food.order.speedzy.RoomBooking;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.food.order.speedzy.Activity.MyOrderActivity;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;

public class BookingSucessfullActivity extends AppCompatActivity {
        ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_sucessfull);
        statusbar_bg(R.color.red);
        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookingDetailActivity();
            }
        });
    }

    private void BookingDetailActivity() {
        Intent intent = new Intent(BookingSucessfullActivity.this, MyOrderActivity.class);
        intent.putExtra("flag", 1);
        intent.putExtra("type", 5);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    private void statusbar_bg(int color) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        BookingDetailActivity();
    }

}
