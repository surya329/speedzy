package com.food.order.speedzy.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.food.order.speedzy.Adapter.GrocerySubCategoriesAdapterNew;
import com.food.order.speedzy.Model.Home_Response_Model;
import com.food.order.speedzy.Model.Patanjali_Category_model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.Utils.reyclerviewutils.RecyclerViewUtils;
import com.food.order.speedzy.api.response.banner.BannerResponse;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.features.uploadgrocery.UploadGroceryActivity;
import com.food.order.speedzy.root.BaseActivity;
import com.food.order.speedzy.screen.banner.BannerAdapter;
import com.food.order.speedzy.screen.banner.BannersVM;
import com.food.order.speedzy.screen.patanjali.GroceryCategoryList;
import com.food.order.speedzy.screen.patanjali.GrocerySubcategoryList;
import com.food.order.speedzy.screen.patanjali.PatanjaliSectionListAdapter;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Sujata Mohanty.
 */

public class PatanjaliProductActivity extends BaseActivity {
  @BindView(R.id.toolbar) Toolbar toolbar;
  //LoaderDiloag loaderDiloag;
  @BindView(R.id.recycler_view_list) RecyclerView recycler_view_list;
  @BindView(R.id.my_recycler_view) RecyclerView my_recycler_view;
  PatanjaliSectionListAdapter itemListDataAdapter;
  GrocerySubCategoriesAdapterNew adapter2;
  List<GroceryCategoryList> list;
  @BindView(R.id.category) TextView category;
  @BindView(R.id.banner) RecyclerView banner;
  BannerAdapter bannerAdapter;
  Home_Response_Model pm;
  @BindView(R.id.cart) ImageView cart;
  MySharedPrefrencesData mySharedPrefrencesData;
  String city_id = "";
  private Apimethods mApimethods;
  private Disposable mDisposable;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_grocery);
    ButterKnife.bind(this);
    list = new ArrayList<>();
    mySharedPrefrencesData = new MySharedPrefrencesData();
    city_id = mySharedPrefrencesData.getSelectCity(PatanjaliProductActivity.this);
    mApimethods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    setSupportActionBar(toolbar);
    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(
          ContextCompat.getColor(PatanjaliProductActivity.this, R.color.red));
    }
    initComponent();
    cart.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(PatanjaliProductActivity.this, CheckoutActivity_New.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      }
    });
  }

  private void initComponent() {
    banner.requestLayout();
    //banner.setHasFixedSize(true);
    banner.setLayoutManager(RecyclerViewUtils.newLinearHorizontalLayoutManager(this));
    banner.setNestedScrollingEnabled(false);
    SnapHelper snapHelper = new LinearSnapHelper();
    banner.setOnFlingListener(null);
    snapHelper.attachToRecyclerView(banner);
    bannerAdapter = new BannerAdapter(
        new BannerAdapter.Callback() {
          @Override public void onClickItem(BannersVM BannersVM) {

          }
        });
    banner.setAdapter(bannerAdapter);
    //bannerAdapter = new MealComboAdapter(1, PatanjaliProductActivity.this, ,
    //    Commons.banner_height, "banner", null, null);
    //bannerAdapter = new BannerAdapter(new BannerAdapter.Callback() {
    //  @Override public void onClickItem(BannersVM BannersVM) {
    //
    //  }
    //});
    //banner.setAdapter(bannerAdapter);

    recycler_view_list.setLayoutManager(RecyclerViewUtils.newLinearHorizontalLayoutManager(this));
    recycler_view_list.setNestedScrollingEnabled(false);
    itemListDataAdapter =
        new PatanjaliSectionListAdapter(new PatanjaliSectionListAdapter.Callback() {
          @Override public void onClickItem(GroceryCategoryList data) {
            //category.setText(data.getName());
            //adapter2.setCategory_id(data);
            //adapter2.refreshData(data);
          }
        });
    recycler_view_list.setAdapter(itemListDataAdapter);

    my_recycler_view.setLayoutManager(RecyclerViewUtils.newLinearVerticalLayoutManager(this));
    my_recycler_view.setNestedScrollingEnabled(false);
    my_recycler_view.setVisibility(View.VISIBLE);
    adapter2 = new GrocerySubCategoriesAdapterNew(PatanjaliProductActivity.this,
            list/*new GrocerySubCategoriesAdapterNew.Callback() {
      @Override public void onClickItem(GroceryCategoryList data, String categoryId) {
        //goToDetailsPage(data, categoryId);
      }
    }*/);
    my_recycler_view.setAdapter(adapter2);
  }

  private void goToDetailsPage(GrocerySubcategoryList data,
      String categoryId) {
    Intent intent =
        new Intent(PatanjaliProductActivity.this, PatanjaliProduct_DetailsActivity.class);
    intent.putExtra("app_tittle", "Grocery World");
    intent.putExtra("category_id", categoryId);
    intent.putExtra("subcategory_id", data.getId());
    intent.putParcelableArrayListExtra("child_subcat",
        (ArrayList<? extends Parcelable>) data.getChildSubcat());
    startActivity(intent);
    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    GeneralUtil.safelyDispose(mDisposable);
  }

  void callApi() {
    /*Call<Patanjali_Category_model> call = mApimethods.getpatanjaliproduct();
    Log.d("url", "url=" + call.request().url().toString());
    loaderDiloag.displayDiloag();
    call.enqueue(new Callback<Patanjali_Category_model>() {
      @Override
      public void onResponse(Call<Patanjali_Category_model> call,
          Response<Patanjali_Category_model> response) {
        int statusCode = response.code();
        Patanjali_Category_model epm = response.body();

        //banner.requestLayout();
        //banner.setHasFixedSize(true);
        //banner.setLayoutManager(new SpeedyLinearLayoutManager(PatanjaliProductActivity.this,
        //    SpeedyLinearLayoutManager.HORIZONTAL, false));
        //banner.setNestedScrollingEnabled(false);
        //SnapHelper snapHelper = new LinearSnapHelper();
        //banner.setOnFlingListener(null);
        //snapHelper.attachToRecyclerView(banner);
        //bannerAdapter = new MealComboAdapter(1, PatanjaliProductActivity.this, epm.getBanners(),
        //    Commons.banner_height, "banner", null, null);
        //banner.setAdapter(bannerAdapter);

        list = epm.getCategories();
        itemListDataAdapter =
            new Patanjali_SectionListDataAdapter(PatanjaliProductActivity.this, list);
        recycler_view_list.setAdapter(itemListDataAdapter);
        //------by Default set sub_category adapter-----
        category.setText(list.get(0).getName());
        adapter2 = new Patanjali_ComplexRecyclerViewAdapter(PatanjaliProductActivity.this,
            list.get(0).getId(), list.get(0).getSub_categories());
        my_recycler_view.setAdapter(adapter2);

        itemListDataAdapter.setOnItemClickListener(
            new Patanjali_SectionListDataAdapter.OnItemClickListener() {
              public void onItemClick(View v, int position) {
                System.out.println("Clicked View");
                //callProductApi(list.get(position).getCategory_ID());
                category.setText(list.get(position).getName());
                adapter2 = new Patanjali_ComplexRecyclerViewAdapter(PatanjaliProductActivity.this,
                    list.get(position).getId(), list.get(position).getSub_categories());
                my_recycler_view.setAdapter(adapter2);
              }
            });
        loaderDiloag.dismissDiloag();
      }

      @Override
      public void onFailure(Call<Patanjali_Category_model> call, Throwable t) {
        Toast.makeText(getApplicationContext(), "internet not available..connect internet",
            Toast.LENGTH_LONG).show();
        loaderDiloag.dismissDiloag();
      }
    });*/
    showProgressDialog();
    mApimethods.getpatanjaliproduct()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Patanjali_Category_model>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(Patanjali_Category_model patanjali_Category_model) {
            dismissProgressDialog();
            setData(patanjali_Category_model);
          }

          @Override public void onError(Throwable e) {
            dismissProgressDialog();
            showErrorDialog(e.getLocalizedMessage());
          }

          @Override public void onComplete() {

          }
        });
  }

  private void setData(Patanjali_Category_model patanjaliCategoryModel) {
    list.clear();
    list.addAll(GroceryCategoryList.transform(patanjaliCategoryModel.getCategories()));
    if (list != null && list.size() > 0) {
      list.get(0).setSelected(true);
    }
    itemListDataAdapter.refreshData(list);
    adapter2.notifyDataSetChanged();

   // adapter2.refreshData(list.get(0).getSubCategories());
   // category.setText(patanjaliCategoryModel.getCategories().get(0).getName());
    //adapter2.setCategory_id(patanjaliCategoryModel.getCategories().get(0).getId());
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case android.R.id.home:
        Commons.back_button_transition(PatanjaliProductActivity.this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    Commons.back_button_transition(PatanjaliProductActivity.this);
  }

  @OnClick(R.id.upload) void onClickUploadGrocery() {
    startActivity(UploadGroceryActivity.newIntent(this));
  }

  private void getBannerDataFromApi() {
    showProgressDialog();
    mApimethods.getBannerList(SpeedzyConstants.GROCERY_ORDER_ID)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<BannerResponse>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(BannerResponse bannerResponse) {
            dismissProgressDialog();
            if (bannerResponse != null
                && bannerResponse.getBanners() != null
                && bannerResponse.getBanners().size() > 0) {
              setBanner(bannerResponse);
            }
          }

          @Override public void onError(Throwable e) {
            dismissProgressDialog();
            showErrorDialog(e.getLocalizedMessage());
          }

          @Override public void onComplete() {

          }
        });
  }

  private void setBanner(BannerResponse bannerResponse) {
    bannerAdapter.refreshData(BannersVM.transform(bannerResponse.getBanners()));
    banner.smoothScrollToPosition(1);
  }
  @Override public void onStart() {
    super.onStart();
    getBannerDataFromApi();
    callApi();
  }

}

