package com.food.order.speedzy.Utils.reyclerviewutils;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

public class HorizontalSpacingItemDecoration extends RecyclerView.ItemDecoration {

  private final int mHorizontalSpacing;
  private final boolean mShowFirstSpacing;
  private final boolean mShowLastSpacing;
  private final int mFirstSpacing;
  private final int mLastSpacing;

  public HorizontalSpacingItemDecoration(int horizontalSpacing) {
    this(horizontalSpacing, false, false);
  }

  public HorizontalSpacingItemDecoration(int horizontalSpacing, boolean showFirstSpacing,
      boolean showLastSpacing) {
    mHorizontalSpacing = horizontalSpacing;
    mShowFirstSpacing = showFirstSpacing;
    mShowLastSpacing = showLastSpacing;
    mFirstSpacing = 0;
    mLastSpacing = 0;
  }

  public HorizontalSpacingItemDecoration(int horizontalSpacing, int firstSpacing, int lastSpacing) {
    mHorizontalSpacing = horizontalSpacing;
    mShowFirstSpacing = true;
    mFirstSpacing = firstSpacing;
    mShowLastSpacing = true;
    mLastSpacing = lastSpacing;
  }

  @Override public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
      RecyclerView.State state) {
    final int position = parent.getChildAdapterPosition(view);
    if (mShowFirstSpacing && position == 0) {
      outRect.left = mFirstSpacing > 0 ? mFirstSpacing : mHorizontalSpacing;
    }
    if (position != parent.getAdapter().getItemCount() - 1) {
      outRect.right = mHorizontalSpacing;
    }
    if (mShowLastSpacing && position == parent.getAdapter().getItemCount() - 1) {
      outRect.right = mLastSpacing > 0 ? mLastSpacing : mHorizontalSpacing;
    }
  }
}
