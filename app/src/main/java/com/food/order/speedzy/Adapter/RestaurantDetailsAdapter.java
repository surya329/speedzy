package com.food.order.speedzy.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.Fragment.MenuFragment;
import com.food.order.speedzy.Model.Dishitem;
import com.food.order.speedzy.Model.Restaurant_Dish_Model;
import com.food.order.speedzy.Model.Restaurant_Dish_Model_New;
import com.food.order.speedzy.Model.View_Rating_model;
import com.food.order.speedzy.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RestaurantDetailsAdapter  extends RecyclerView.Adapter<RestaurantDetailsAdapter.ViewHolder>{

    private Context ctx;
    ArrayList<Dishitem> parentDishArrayList;
    String res_id,res_name,res_address,suburb,postcode,pick_del,delivery_fee,avg_order_value,orders_count,mMenuId,flg_ac_status;
    ArrayList<Restaurant_Dish_Model.NewOffers> offerArrayList;
    int i;
    boolean additem;
    Restaurant_Dish_Model_New.restaurant_details.restcharge restcharge;
    String res_image;
    ImageView gifImage;
    public RestaurantDetailsAdapter(ImageView gifImage,ArrayList<Dishitem> parentDishArrayList,
                                    Context ctx, String res_id, String res_name, String res_address, String suburb, String postcode, String pick_del, String delivery_fee, ArrayList<Restaurant_Dish_Model.NewOffers> offerArrayList, String avg_order_value, String orders_count, int i, String mMenuId, boolean additem, String flg_ac_status, Restaurant_Dish_Model_New.restaurant_details.restcharge restcharge, String res_image) {
        this.ctx = ctx;
        this.gifImage=gifImage;
        this.parentDishArrayList = parentDishArrayList;
        this.res_id=res_id;
         this. res_name=res_name;
         this.res_address=res_address;
         this. suburb=suburb;
         this. postcode=postcode;
         this. pick_del=pick_del;
         this. delivery_fee=delivery_fee;
         this. offerArrayList=offerArrayList;
         this. avg_order_value=avg_order_value;
         this. orders_count=orders_count;
         this. i=i;
         this. mMenuId=mMenuId;
         this. additem=additem;
         this. flg_ac_status=flg_ac_status;
         this. restcharge=restcharge;
        this. res_image=res_image;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        RecyclerView recyclerView;
        TextView parent_dish_name;
        LinearLayout relativelayparent;
        public ViewHolder(View v) {
            super(v);
            relativelayparent=v.findViewById(R.id.relativelayparent);
            recyclerView=v.findViewById(R.id.menu_list);
            parent_dish_name=(TextView)v.findViewById(R.id.parent_dish_name);
        }
    }

    @Override
    public RestaurantDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_list_recyclerview, parent, false);
        System.out.println("View Holder");
        // set the view's size, margins, paddings and layout parameters
        RestaurantDetailsAdapter.ViewHolder vh = new RestaurantDetailsAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RestaurantDetailsAdapter.ViewHolder holder, final int position) {
        Dishitem dishitem=parentDishArrayList.get(position);
        holder.parent_dish_name.setText(dishitem.getCuisineName());
        LinearLayoutManager layoutManager = new LinearLayoutManager(ctx);
        holder.recyclerView.setLayoutManager(layoutManager);
        holder.recyclerView.setHasFixedSize(false);
        holder.recyclerView.setNestedScrollingEnabled(false);
        RestaurantDetailsAdapterMenu adapter =new RestaurantDetailsAdapterMenu(gifImage,parentDishArrayList.get(position).getDishdetails(), ctx,
                        res_id, res_name, res_address, suburb, postcode, pick_del,
                        delivery_fee,offerArrayList,
                        avg_order_value,
                        orders_count,i,mMenuId,additem,flg_ac_status, restcharge,res_image);
        holder.recyclerView.setAdapter(adapter);

    }
    @Override
    public int getItemCount() {
        return  parentDishArrayList.size();
    }
    public void updateList(ArrayList<Dishitem> list){
        parentDishArrayList = list;
        notifyDataSetChanged();
    }

}
