package com.food.order.speedzy.Model;


/**
 * Created by Sujata Mohanty.
 */


public class DeliveryFee {

    private String st_delivery_charge;

    public String getStDeliveryCharge() {
        return st_delivery_charge;
    }

    public void setStDeliveryCharge(String stDeliveryCharge) {
        this.st_delivery_charge = stDeliveryCharge;
    }

}