package com.food.order.speedzy.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.IntentUtils;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedyLinearLayoutManager;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.api.response.banner.BannerResponse;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.root.BaseActivity;
import com.food.order.speedzy.screen.banner.BannerAdapter;
import com.food.order.speedzy.screen.banner.BannersVM;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.Objects;

public class MedicineActivity extends BaseActivity {
  Toolbar toolbar;
  //LoaderDiloag loaderDiloag;
  RecyclerView banner;
  BannerAdapter bannerAdapter;
  MySharedPrefrencesData mySharedPrefrencesData;
  String city_id = "";
  TextView upload,subscribe_now,without_prescription;
  ImageView img_call;
  LinearLayout linear1;
  private Apimethods mApimethods;
  private Disposable mDisposable;

  private static final int PERMISSIONS_REQUEST_PHONE_CALL = 100;
  String phone_digit;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_medicine);

    //loaderDiloag = new LoaderDiloag(this);
    mySharedPrefrencesData = new MySharedPrefrencesData();
    city_id = mySharedPrefrencesData.getSelectCity(MedicineActivity.this);
    mApimethods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(MedicineActivity.this, R.color.red));
    }

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    upload = (TextView) findViewById(R.id.upload);
    subscribe_now=findViewById(R.id.subscribe_now);
    without_prescription=findViewById(R.id.without_prescription);
    img_call=findViewById(R.id.img_call);
    linear1 = (LinearLayout) findViewById(R.id.linear1);

    banner = (RecyclerView) findViewById(R.id.banner);
    linear1.setVisibility(View.VISIBLE);
    initComponent();
    //callApi();
    getBannerDataFromApi();

    upload.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        call_intent();
      }
    });
    subscribe_now.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        call_intent();
      }
    });
    img_call.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Dialog_show_call("+91 986 183 0013");
      }
    });
    without_prescription.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Dialog_show_call("+91 986 183 0013");
      }
    });
  }
  private void Dialog_show_call(String mobile) {
    BottomSheetDialog dialog = new BottomSheetDialog(MedicineActivity.this, R.style.SheetDialog);
    dialog.setContentView(R.layout.call_dialog);
    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
    TextView mobile_no = (TextView) dialog.findViewById(R.id.mobile_no);
    LinearLayout mobile_no_linear = dialog.findViewById(R.id.mobile_no_linear);
    mobile_no.setText("Call " + mobile);
    cancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        dialog.dismiss();
      }
    });
    phone_digit = mobile.trim().replaceAll("[^\\.0123456789]", "").trim();
    mobile_no_linear.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        call();
        dialog.dismiss();
      }
    });
    if (!isFinishing()) {
      dialog.show();
    }
  }

  private void call() {
    if (!GeneralUtil.isStringEmpty(phone_digit)) {
      startActivity(IntentUtils.callDialerIntent(phone_digit));
    }
  }
  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                         int[] grantResults) {
    if (requestCode == PERMISSIONS_REQUEST_PHONE_CALL) {
      if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        // Permission is granted
        call();
      } else {
        Toast.makeText(getApplicationContext(), "Sorry!!! Permission Denied", Toast.LENGTH_SHORT)
                .show();
      }
    }
  }

  private void call_intent() {
    Intent intent = new Intent(MedicineActivity.this, UploadActivity.class);
    startActivity(intent);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    GeneralUtil.safelyDispose(mDisposable);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case android.R.id.home:
        Commons.back_button_transition(MedicineActivity.this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    Commons.back_button_transition(MedicineActivity.this);
  }

  private void getBannerDataFromApi() {
    //loaderDiloag.displayDiloag();
    showProgressDialog();
    mApimethods.getBannerList(SpeedzyConstants.MEDICINE_ORDER_ID)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<BannerResponse>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(BannerResponse bannerResponse) {
            dismissProgressDialog();
            if (bannerResponse != null
                && bannerResponse.getBanners() != null
                && bannerResponse.getBanners().size() > 0) {
              setBanner(bannerResponse);
            }
          }

          @Override public void onError(Throwable e) {
            //loaderDiloag.dismissDiloag();
            dismissProgressDialog();
          }

          @Override public void onComplete() {

          }
        });
  }

  private void setBanner(BannerResponse bannerResponse) {
    bannerAdapter.refreshData(BannersVM.transform(bannerResponse.getBanners()));
    banner.smoothScrollToPosition(1);
  }

  private void initComponent() {
    banner.requestLayout();
    //banner.setHasFixedSize(true);

    banner.setLayoutManager(new SpeedyLinearLayoutManager(MedicineActivity.this,
        SpeedyLinearLayoutManager.HORIZONTAL, false));
    banner.setNestedScrollingEnabled(false);
    SnapHelper snapHelper = new LinearSnapHelper();
    banner.setOnFlingListener(null);
    snapHelper.attachToRecyclerView(banner);
    bannerAdapter = new BannerAdapter(
        new BannerAdapter.Callback() {
          @Override public void onClickItem(BannersVM BannersVM) {

          }
        });
    banner.setAdapter(bannerAdapter);
    //bannerAdapter = new MealComboAdapter(1, PatanjaliProductActivity.this, ,
    //    Commons.banner_height, "banner", null, null);
  }
}
