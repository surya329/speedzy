package com.food.order.speedzy.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Fragment.MenuFragment;
import com.food.order.speedzy.Model.Cart_Model;
import com.food.order.speedzy.Model.RestaurantModel;
import com.food.order.speedzy.Model.Restaurant_Dish_Model_New;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.root.BaseActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */

public class RestarantDetails_New extends BaseActivity {
  LinearLayout menu, book_table;
  String name, res_image, res_logo_image, res_minorder, res_id, res_rating, rest_street,
      res_reviews, postcode, suburb, res_type_flag;
  String booktable_count, closeopen, venuecuisine;
  String start_time_lunch, end_time_lunch, start_time_dinner, end_time_dinner, app_openstatus,Promotion_msg,Flg_ac_status="",
      menu_id="", nav_type;
  ImageView res_imgview, res_img_logo;
  TextView res_name;
  androidx.appcompat.widget.Toolbar toolbar;
  //CollapsingToolbarLayout collapsingToolbarLayout;
  public RelativeLayout cartaddedlay;
  public LinearLayout add_item_layout;
  public TextView item;
  public TextView cart_res_name;
  MySharedPrefrencesData mySharedPrefrencesData;
  String Veg_Flag;
  String base_url = "https://storage.googleapis.com/speedzy_data/resources/restaurant/";
  String base_url_logo =
          "https://storage.googleapis.com/speedzy_data/resources/restaurantlogo/";
  Restaurant_Dish_Model_New restmodel;
  LoaderDiloag loaderDiloag;

  private View viewToAttachDisplayerTo;
  boolean network_status = true;
  boolean additem=false;
  RatingBar ratingbar;
  TextView rating,avg_rating;
  LinearLayout rating_linear;

  public static int sCorner = 30;
  public static int sMargin = 0;
  @BindView(R.id.res_logo) ImageView imgLogo;
  @BindView(R.id.open_close_img) ImageView open_close_img;
  @BindView(R.id.restname)
  TextView txtResturantName;
  @BindView(R.id.msg) TextView txtMsg;
  @BindView(R.id.min_order) TextView txtMinOrder;
  @BindView(R.id.res_address) TextView res_address;
  RestaurantModel restaurantModel;
  LOcaldbNew lOcaldbNew= new LOcaldbNew(this);;
  ArrayList<Cart_Model.Cart_Details> cd = new ArrayList<>();
  LinearLayout includedLayout;
  NestedScrollView nestedScrollView;
  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_resturant_details_new_layout);
    ButterKnife.bind(this);
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    viewToAttachDisplayerTo = findViewById(R.id.rex_details);


    loaderDiloag = new LoaderDiloag(this);
    mySharedPrefrencesData = new MySharedPrefrencesData();
    Veg_Flag = mySharedPrefrencesData.getVeg_Flag(this);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    //collapsingToolbarLayout=(CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar);
    setSupportActionBar(toolbar);
    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_back);
    Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    statusbar_bg(R.color.red);

    nestedScrollView=findViewById(R.id.nestedScrollView);
    book_table = (LinearLayout) findViewById(R.id.book_table);
    menu = (LinearLayout) findViewById(R.id.menu);
    cartaddedlay = findViewById(R.id.cart_add_layout);
    includedLayout= findViewById(R.id.includedLayout);
    add_item_layout=(LinearLayout) findViewById(R.id.add_item_layout);
    item = (TextView) findViewById(R.id.item);
    cart_res_name = (TextView) findViewById(R.id.cart_res_name);
    rating_linear=(LinearLayout) findViewById(R.id.linear1);
    rating = (TextView) findViewById(R.id.rating);
    avg_rating= (TextView) findViewById(R.id.avg_rating);
    ratingbar = (RatingBar) findViewById(R.id.ratingbar);


    res_id = getIntent().getStringExtra("res_id");
    if (getIntent().getStringExtra("res_name") != null) {
      additem=getIntent().getBooleanExtra("additem",false);
      name = getIntent().getStringExtra("res_name");
      res_image = getIntent().getStringExtra("res_image");
      res_logo_image = getIntent().getStringExtra("res_logo_image");
      rest_street = getIntent().getStringExtra("res_add");
      res_minorder = getIntent().getStringExtra("res_minorder");
      res_rating = getIntent().getStringExtra("res_rating");
      res_reviews = getIntent().getStringExtra("res_reviews");
      Promotion_msg=getIntent().getStringExtra("Promotion_msg");
      Flg_ac_status=getIntent().getStringExtra("Flg_ac_status");
      app_openstatus = getIntent().getStringExtra("app_openstatus");
      menu_id = getIntent().getStringExtra("menu_id");
      nav_type = getIntent().getStringExtra("nav_type");
      start_time_lunch = getIntent().getStringExtra("start_time_lunch");
      end_time_lunch = getIntent().getStringExtra("end_time_lunch");
      start_time_dinner = getIntent().getStringExtra("start_time_dinner");
      end_time_dinner = getIntent().getStringExtra("end_time_dinner");
      suburb = getIntent().getStringExtra("Suburb");
      postcode = getIntent().getStringExtra("postcode");
      restaurantModel = (RestaurantModel) getIntent().getParcelableExtra("extra_model");
      //setUIData();
      if (nav_type.equalsIgnoreCase("3") && menu_id.equalsIgnoreCase("M1014")) {
      } else {
        //restapicall(res_id);
      }
    }
    restapicall(res_id);

    //            restaurantModel = getIntent().getParcelableExtra("RestaurantModel");

    //res_type_flag=getIntent().getStringExtra("res_type_flag");
        /*    booktable_count = getIntent().getStringExtra("booktableparameter");
            closeopen = getIntent().getStringExtra("stclose_open");
            venuecuisine = getIntent().getStringExtra("venuecuisine");
            deliverySchedules = (Restaurant_model.DeliverySchedule) getIntent().getSerializableExtra("deliverytime");
            pickupSchedules = (Restaurant_model.PickupSchedule) getIntent().getSerializableExtra("pickuptime");
            tom_deliverySchedules = (Restaurant_model.DeliverySchedule) getIntent().getSerializableExtra("tom_deliverytime");
            tom_pickupSchedules = (Restaurant_model.PickupSchedule) getIntent().getSerializableExtra("tom_pickuptime");
            innerImageArrayList = (ArrayList<String>) getIntent().getSerializableExtra("inner_image_list");*/


           /* AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                boolean isShow = true;
                int scrollRange = -1;

                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.getTotalScrollRange();
                    }
                    if (scrollRange + verticalOffset == 0) {
                        collapsingToolbarLayout.setTitle(name);
                        isShow = true;
                    } else if(isShow) {
                        collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                        isShow = false;
                    }
                }
            });*/

    if (Veg_Flag.equalsIgnoreCase("1")) {
      statusbar_bg(R.color.red);
    } else {
      statusbar_bg(R.color.red);
    }

    res_imgview = (ImageView) findViewById(R.id.res_img);
    res_img_logo = (ImageView) findViewById(R.id.res_img_logo);
    //res_name = (TextView) findViewById(R.id.res_name);

    Commons.restaurant_id = res_id;
    /*res_name.setText(name);
    if (nav_type.equalsIgnoreCase("3") && menu_id.equalsIgnoreCase("M1014")) {
      book_table_function();
    } else {
      menu.setVisibility(View.GONE);
      book_table.setVisibility(View.GONE);
    }
    book_table.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        book_table_function();
      }
    });

    menu.setVisibility(View.GONE);
    menu.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        book_table.setVisibility(View.VISIBLE);
        menu.setVisibility(View.GONE);
        selectFrag(
            new MenuFragment(RestarantDetails_New.this, restmodel, app_openstatus, start_time_lunch,
                end_time_lunch, start_time_dinner, end_time_dinner, res_id, name, rest_street,
                suburb, postcode, menu_id,additem,Flg_ac_status));
      }
    });*/
    rating_linear.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent in = new Intent(RestarantDetails_New.this, Reviews.class);
        if (nav_type.equalsIgnoreCase("1") || restaurantModel == null) {
          in.putExtra("rest_id", restmodel.getRestaurant_details().get(0).getIn_restaurant_id());
          in.putExtra("Image",base_url_logo + "big/" + restmodel.getRestaurant_details()
                  .get(0)
                  .getRestaurant_logo_image());
          in.putExtra("Name", restmodel.getRestaurant_details().get(0).getSt_restaurant_name());
          in.putExtra("Address", restmodel.getRestaurant_details().get(0).getSt_street_address());
          in.putExtra("Reviews", restmodel.getRestaurant_details().get(0).getCount_rating());
          in.putExtra("Rating", restmodel.getRestaurant_details().get(0).getAvg_rating());
          in.putExtra("Suburb", restmodel.getRestaurant_details().get(0).getSt_suburb());
          in.putExtra("postcode", "");
        }else {
          in.putExtra("rest_id", restaurantModel.getmInRestaurantId());
          in.putExtra("Image", restaurantModel.getmResPic());
          in.putExtra("Name", restaurantModel.getmResName());
          in.putExtra("Address", restaurantModel.getmStStreetAddress());
          in.putExtra("Reviews", restaurantModel.getmRating());
          in.putExtra("Rating", restaurantModel.getmRatingBar());
          in.putExtra("Suburb", restaurantModel.getmStSuburb());
          in.putExtra("postcode", "");
        }
        startActivity(in);
      }
    });
    add_item_layout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",1);
        setResult(RESULT_OK,returnIntent);
        Commons.back_button_transition(RestarantDetails_New.this);
      }
    });
    cartaddedlay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (menu_id.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID)) {
          Commons.flag_for_hta = SpeedzyConstants.Fruits;
          Intent intent = new Intent(RestarantDetails_New.this, CheckoutActivity_New.class);
          intent.putExtra("ordernumber", restmodel.getOrders_count());
          intent.putExtra("avg_order_value", restmodel.getAvg_order_value());
          intent.putExtra("start_time_lunch", start_time_lunch);
          intent.putExtra("end_time_lunch", end_time_lunch);
          intent.putExtra("start_time_dinner", start_time_dinner);
          intent.putExtra("end_time_dinner", end_time_dinner);
          intent.putExtra("app_openstatus", app_openstatus);
          intent.putExtra("offerlist", (Serializable) restmodel.getOffers());
          //intent.putExtra("deliverysuburb", (Serializable) restmodel.getDeliveryAreaList());
               /* intent.putExtra("deliverySchedule", (Serializable) deliverySchedules);
                intent.putExtra("pickupSchedule", (Serializable) pickupSchedules);
                intent.putExtra("tom_deliverySchedule", (Serializable) tom_deliverySchedules);
                intent.putExtra("tom_pickupSchedule", (Serializable) tom_pickupSchedules);*/
          startActivityForResult(intent,2);
          overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Intent intent = new Intent(RestarantDetails_New.this, CheckoutActivity_New.class);
          intent.putExtra("ordernumber", restmodel.getOrders_count());
          intent.putExtra("avg_order_value", restmodel.getAvg_order_value());
          intent.putExtra("start_time_lunch", start_time_lunch);
          intent.putExtra("end_time_lunch", end_time_lunch);
          intent.putExtra("start_time_dinner", start_time_dinner);
          intent.putExtra("end_time_dinner", end_time_dinner);
          intent.putExtra("app_openstatus", app_openstatus);
          intent.putExtra("offerlist", (Serializable) restmodel.getOffers());
          //intent.putExtra("deliverysuburb", (Serializable) restmodel.getDeliveryAreaList());
               /* intent.putExtra("deliverySchedule", (Serializable) deliverySchedules);
                intent.putExtra("pickupSchedule", (Serializable) pickupSchedules);
                intent.putExtra("tom_deliverySchedule", (Serializable) tom_deliverySchedules);
                intent.putExtra("tom_pickupSchedule", (Serializable) tom_pickupSchedules);*/
          startActivityForResult(intent,2);
         overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
  }

  private void setUIData() {
    toolbar.setTitle(name);
    if (restaurantModel != null) {
      String base_url_logo =
            "https://storage.googleapis.com/speedzy_data/resources/restaurantlogo/";
      Glide.with(this).load(restaurantModel.getmResPic()).into(imgLogo);
      txtResturantName.setText(restaurantModel.getmResName());
      res_address.setText(restaurantModel.getmStStreetAddress());
      rating.setText("("+restaurantModel.getmRating() + " Ratings)");
      avg_rating.setText(""+restaurantModel.getmRatingBar());
      ratingbar.setRating(restaurantModel.getmRatingBar());
      txtMsg.setText(restaurantModel.getmMsg());
      txtMinOrder.setText(restaurantModel.getmMinOrder());
      if (restaurantModel.getmOpenClose().equalsIgnoreCase("Open")) {
        open_close_img.setImageResource(R.drawable.ic_open);
      }else if (restaurantModel.getmOpenClose().equalsIgnoreCase("Closed")) {
        open_close_img.setImageResource(R.drawable.ic_closed);
      }
     /* if (app_openstatus.equalsIgnoreCase("0")) {
        txtOpenStatus.setText("Close");
      } else if (app_openstatus.equalsIgnoreCase("1")) {
        txtOpenStatus.setText("Open");
      }*/
    }
  }

  private void restapicall(String res_id) {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    Call<Restaurant_Dish_Model_New> call =
        methods.setRes_dish(res_id, mySharedPrefrencesData.getUser_Id(this), Veg_Flag);
    Log.d("url", "url=" + call.request().url().toString());
    loaderDiloag.displayDiloag();
    call.enqueue(new Callback<Restaurant_Dish_Model_New>() {
      @Override
      public void onResponse(Call<Restaurant_Dish_Model_New> call,
          Response<Restaurant_Dish_Model_New> response) {

        restmodel = response.body();
        if (restmodel.getStatus().equalsIgnoreCase("Success")) {
          int statusCode = response.code();
          Log.d("Response", "" + statusCode);
          Log.d("response", "" + response);
          if (!RestarantDetails_New.this.isFinishing()) {
            //menu.setVisibility(View.GONE);
            setHeaderData(restmodel);
            selectFrag(new MenuFragment(RestarantDetails_New.this, restmodel, app_openstatus,
                start_time_lunch, end_time_lunch, start_time_dinner, end_time_dinner, RestarantDetails_New.this.res_id, name,
                rest_street, suburb, postcode, menu_id,additem,Flg_ac_status,nestedScrollView));
            includedLayout.setVisibility(View.VISIBLE);
            loaderDiloag.dismissDiloag();
          }
        }
      }

      @Override
      public void onFailure(@NonNull Call<Restaurant_Dish_Model_New> call, @NonNull Throwable t) {
        loaderDiloag.dismissDiloag();
      }
    });
  }

  private void setHeaderData(Restaurant_Dish_Model_New model) {
    if (model.getRestaurant_details() != null && model.getRestaurant_details().size() > 0) {
      RequestBuilder<Drawable> thumbnailRequest = Glide
              .with(RestarantDetails_New.this)
              .load(base_url + "very_small/" + restmodel.getRestaurant_details()
                      .get(0).getRestaurant_main_image())
              .apply(RequestOptions.bitmapTransform(new RoundedCorners(sCorner)).placeholder(R.drawable.combo_placeholder));

      Glide
              .with(RestarantDetails_New.this)
              .load(base_url_logo + "big/" + restmodel.getRestaurant_details()
                      .get(0).getRestaurant_logo_image())
              .thumbnail(thumbnailRequest)
              .apply(RequestOptions.bitmapTransform(new RoundedCorners(sCorner)).placeholder(R.drawable.combo_placeholder).diskCacheStrategy(DiskCacheStrategy.ALL))
              .into(imgLogo);
      toolbar.setTitle(model.getRestaurant_details().get(0).getSt_restaurant_name());
      txtResturantName.setText(model.getRestaurant_details().get(0).getSt_restaurant_name());
      res_address.setText(model.getRestaurant_details().get(0).getSt_street_address());
      rating.setText("("+restmodel.getRestaurant_details().get(0).getCount_rating() + " Ratings)");
      avg_rating.setText(""+restmodel.getRestaurant_details().get(0).getAvg_rating());
      ratingbar.setRating(Float.parseFloat(restmodel.getRestaurant_details().get(0).getAvg_rating()));
      txtMsg.setVisibility(View.GONE);
      txtMsg.setText("");
      txtMinOrder.setText("Min ₹" + model.getRestaurant_details().get(0).getSt_min_order());
      open_close_img.setImageResource(R.drawable.ic_open);
      cart_res_name_txt();
    }
  }

  private void book_table_function() {
    menu.setVisibility(View.GONE);
    book_table.setVisibility(View.VISIBLE);
    //selectFrag(new DetailsFragment());
    Intent intent = new Intent(RestarantDetails_New.this, BooktableActivity_New.class);
    intent.putExtra("res_id", res_id);
    intent.putExtra("res_img", res_image);
    intent.putExtra("res_name", name);
    intent.putExtra("menu_id", menu_id);
    intent.putExtra("nav_type", nav_type);
    intent.putExtra("rest_street_details", rest_street + "," + suburb + "," + postcode);
    intent.putExtra("rest_close_open_rest_venue_cuisine", closeopen + "," + venuecuisine);
    intent.putExtra("start_time_lunch", start_time_lunch);
    intent.putExtra("end_time_lunch", end_time_lunch);
    intent.putExtra("start_time_dinner", start_time_dinner);
    intent.putExtra("end_time_dinner", end_time_dinner);
    if (nav_type.equalsIgnoreCase("3") && menu_id.equalsIgnoreCase("M1014")) {
      startActivityForResult(intent, 1);
    } else {
      startActivity(intent);
    }
    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
  }

  private void selectFrag(Fragment fragment) {
    try {
      FragmentManager supportFragmentManager = getSupportFragmentManager();
      FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
      fragmentTransaction.replace(R.id.fragment_container, fragment)
          .addToBackStack(null)
          .commit();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void statusbar_bg(int color) {
    //collapsingToolbarLayout.setContentScrimColor(getResources().getColor(color));
    //collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(color));
   /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,color ));
        }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.rex_detail_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.search) {
      Intent intent = new Intent(RestarantDetails_New.this, Resturant_SearchActivity.class);
      intent.putExtra("start_time_lunch", start_time_lunch);
      intent.putExtra("end_time_lunch", end_time_lunch);
      intent.putExtra("start_time_dinner", start_time_dinner);
      intent.putExtra("end_time_dinner", end_time_dinner);
      intent.putExtra("app_openstatus", app_openstatus);
      startActivity(intent);
      overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      return true;
    }
    if (id == R.id.cart) {
      if (menu_id.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID)) {
        Commons.flag_for_hta = SpeedzyConstants.Fruits;
      }
      Intent intent = new Intent(RestarantDetails_New.this, CheckoutActivity_New.class);
      intent.putExtra("start_time_lunch", start_time_lunch);
      intent.putExtra("end_time_lunch", end_time_lunch);
      intent.putExtra("start_time_dinner", start_time_dinner);
      intent.putExtra("end_time_dinner", end_time_dinner);
      intent.putExtra("app_openstatus", app_openstatus);
      startActivity(intent);
      overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      return true;
    }
    if (id == android.R.id.home) {
      onBackPressed();
      return true;
    }

    return super.onOptionsItemSelected(item);
  }



  @Override
  public void onBackPressed() {
    super.onBackPressed();
    //supportFinishAfterTransition();
    Commons.back_button_transition(RestarantDetails_New.this);
  }

 @Override public void onResume() {
    super.onResume();
  }

  private void cart_res_name_txt() {
    if (menu_id.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID) ||
            Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)) {
    cd = lOcaldbNew.getFruitCart_Details();
  } else {
    cd = lOcaldbNew.getCart_Details();
  }
    if (cd.isEmpty()) {
      cart_res_name.setText("From : " +txtResturantName.getText().toString());
    }else {
      cart_res_name.setText("From : " + cd.get(0).getSt_restaurant_name());
    }
  }

  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    // check if the request code is same as what is passed  here it is 2
    if (resultCode == 1) {
      finish();
    }else if (resultCode == 2){
      restapicall(cd.get(0).getIn_restaurant_id());
    }else {
      cart_res_name_txt();
    }
  }
}
