package com.food.order.speedzy.api.response.home;

import java.io.Serializable;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Alltimings implements Serializable {

	@SerializedName("tiffin_end")
	private String tiffinEnd;

	@SerializedName("fastfood_end")
	private String fastfoodEnd;

	@SerializedName("fastfood_start")
	private String fastfoodStart;

	@SerializedName("lunch_end")
	private String lunchEnd;

	@SerializedName("breakfast_start")
	private String breakfastStart;

	@SerializedName("dinner_end")
	private String dinnerEnd;

	@SerializedName("sweets_end")
	private String sweetsEnd;

	@SerializedName("sweets_start")
	private String sweetsStart;

	@SerializedName("dinner_start")
	private String dinnerStart;

	@SerializedName("lunch_start")
	private String lunchStart;

	@SerializedName("breakfast_end")
	private String breakfastEnd;

	@SerializedName("fruits_end")
	private String fruitsEnd;

	@SerializedName("tiffin_start")
	private String tiffinStart;

	@SerializedName("fruits_start")
	private String fruitsStart;

	public String getSuper_daily_start() {
		return super_daily_start;
	}

	public void setSuper_daily_start(String super_daily_start) {
		this.super_daily_start = super_daily_start;
	}

	public String getSuper_daily_end() {
		return super_daily_end;
	}

	public void setSuper_daily_end(String super_daily_end) {
		this.super_daily_end = super_daily_end;
	}

	@SerializedName("super_daily_start")
	private String super_daily_start;

	@SerializedName("super_daily_end")
	private String super_daily_end;

	public void setTiffinEnd(String tiffinEnd){
		this.tiffinEnd = tiffinEnd;
	}

	public String getTiffinEnd(){
		return tiffinEnd;
	}

	public void setFastfoodEnd(String fastfoodEnd){
		this.fastfoodEnd = fastfoodEnd;
	}

	public String getFastfoodEnd(){
		return fastfoodEnd;
	}

	public void setFastfoodStart(String fastfoodStart){
		this.fastfoodStart = fastfoodStart;
	}

	public String getFastfoodStart(){
		return fastfoodStart;
	}

	public void setLunchEnd(String lunchEnd){
		this.lunchEnd = lunchEnd;
	}

	public String getLunchEnd(){
		return lunchEnd;
	}

	public void setBreakfastStart(String breakfastStart){
		this.breakfastStart = breakfastStart;
	}

	public String getBreakfastStart(){
		return breakfastStart;
	}

	public void setDinnerEnd(String dinnerEnd){
		this.dinnerEnd = dinnerEnd;
	}

	public String getDinnerEnd(){
		return dinnerEnd;
	}

	public void setSweetsEnd(String sweetsEnd){
		this.sweetsEnd = sweetsEnd;
	}

	public String getSweetsEnd(){
		return sweetsEnd;
	}

	public void setSweetsStart(String sweetsStart){
		this.sweetsStart = sweetsStart;
	}

	public String getSweetsStart(){
		return sweetsStart;
	}

	public void setDinnerStart(String dinnerStart){
		this.dinnerStart = dinnerStart;
	}

	public String getDinnerStart(){
		return dinnerStart;
	}

	public void setLunchStart(String lunchStart){
		this.lunchStart = lunchStart;
	}

	public String getLunchStart(){
		return lunchStart;
	}

	public void setBreakfastEnd(String breakfastEnd){
		this.breakfastEnd = breakfastEnd;
	}

	public String getBreakfastEnd(){
		return breakfastEnd;
	}

	public void setFruitsEnd(String fruitsEnd){
		this.fruitsEnd = fruitsEnd;
	}

	public String getFruitsEnd(){
		return fruitsEnd;
	}

	public void setTiffinStart(String tiffinStart){
		this.tiffinStart = tiffinStart;
	}

	public String getTiffinStart(){
		return tiffinStart;
	}

	public void setFruitsStart(String fruitsStart){
		this.fruitsStart = fruitsStart;
	}

	public String getFruitsStart(){
		return fruitsStart;
	}

	@Override
 	public String toString(){
		return 
			"Alltimings{" + 
			"tiffin_end = '" + tiffinEnd + '\'' + 
			",fastfood_end = '" + fastfoodEnd + '\'' + 
			",fastfood_start = '" + fastfoodStart + '\'' + 
			",lunch_end = '" + lunchEnd + '\'' + 
			",breakfast_start = '" + breakfastStart + '\'' + 
			",dinner_end = '" + dinnerEnd + '\'' + 
			",sweets_end = '" + sweetsEnd + '\'' + 
			",sweets_start = '" + sweetsStart + '\'' + 
			",dinner_start = '" + dinnerStart + '\'' + 
			",lunch_start = '" + lunchStart + '\'' + 
			",breakfast_end = '" + breakfastEnd + '\'' + 
			",fruits_end = '" + fruitsEnd + '\'' + 
			",tiffin_start = '" + tiffinStart + '\'' + 
			",fruits_start = '" + fruitsStart + '\'' + 
			"}";
		}
}