package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class voucher_check_model implements Serializable {
    @SerializedName("status")
    @Expose
    private String status;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
    @SerializedName("Message")
    @Expose
    private String Message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public code_info getCode_info() {
        return code_info;
    }

    public void setCode_info(code_info code_info) {
        this.code_info = code_info;
    }

    @SerializedName("code_info")
    @Expose
    private code_info code_info;
    public class code_info implements Serializable{
        @SerializedName("in_coupon_id")
        @Expose
        private String in_coupon_id;
        @SerializedName("discount_value")
        @Expose
        private String discount_value;
        @SerializedName("discount_type")
        @Expose
        private String discount_type;
        @SerializedName("max_amount")
        @Expose
        private String max_amount;
        @SerializedName("coupon_valid_date")
        @Expose
        private String coupon_valid_date;

        public String getMin_order_value() {
            return min_order_value;
        }

        public void setMin_order_value(String min_order_value) {
            this.min_order_value = min_order_value;
        }
        @SerializedName("min_order_value")
        @Expose
        private String min_order_value;

        public String getIn_coupon_id() {
            return in_coupon_id;
        }

        public void setIn_coupon_id(String in_coupon_id) {
            this.in_coupon_id = in_coupon_id;
        }

        public String getDiscount_value() {
            return discount_value;
        }

        public void setDiscount_value(String discount_value) {
            this.discount_value = discount_value;
        }

        public String getDiscount_type() {
            return discount_type;
        }

        public void setDiscount_type(String discount_type) {
            this.discount_type = discount_type;
        }

        public String getMax_amount() {
            return max_amount;
        }

        public void setMax_amount(String max_amount) {
            this.max_amount = max_amount;
        }

        public String getCoupon_valid_date() {
            return coupon_valid_date;
        }

        public void setCoupon_valid_date(String coupon_valid_date) {
            this.coupon_valid_date = coupon_valid_date;
        }
    }
}
