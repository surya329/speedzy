
package com.food.order.speedzy.Model;


import java.io.Serializable;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */


public class Extra implements Serializable {

    private List<StPrice> stPriceList;
    private String id;
    private String attribute_name;
    private String attribute_price;
    private String isChecked;
    private Boolean isSelected=false;

   public Extra(List<StPrice> stPriceList) {
        this.stPriceList = stPriceList;
    }

    public Boolean getSelected() {
        return isSelected;
    }
    public void setSelected(Boolean selected) {
        isSelected = selected;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAttributeName() {
        return attribute_name;
    }

    public void setAttributeName(String attributeName) {
        this.attribute_name = attributeName;
    }

    public String getAttributePrice() {
        return attribute_price;
    }

    public void setAttributePrice(String attributePrice) {
        this.attribute_price = attributePrice;
    }

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

}