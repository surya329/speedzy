package com.food.order.speedzy.api.response.coupon;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class CouponItem{

	@SerializedName("flg_approved_status")
	private String flgApprovedStatus;

	@SerializedName("in_restaurant_id")
	private String inRestaurantId;

	@SerializedName("discount_value")
	private String discountValue;

	@SerializedName("description")
	private String description;

	public List<CouponItem.discount_slabs> getDiscount_slabs() {
		return discount_slabs;
	}

	public void setDiscount_slabs(List<CouponItem.discount_slabs> discount_slabs) {
		this.discount_slabs = discount_slabs;
	}

	@SerializedName("discount_slabs")
	private List<discount_slabs> discount_slabs=null;

	@SerializedName("discount_type")
	private String discountType;

	@SerializedName("title")
	private String title;

	@SerializedName("coupon_valid_date")
	private String couponValidDate;

	@SerializedName("created_by")
	private String createdBy;

	@SerializedName("min_order_value")
	private String minOrderValue;

	@SerializedName("no_of_use")
	private String noOfUse;

	@SerializedName("flg_is_delete")
	private String flgIsDelete;

	@SerializedName("in_coupon_id")
	private String inCouponId;

	@SerializedName("max_amount")
	private String maxAmount;

	@SerializedName("st_coupon_code")
	private String stCouponCode;

	@SerializedName("created_date")
	private String createdDate;

	public String getFlgApprovedStatus(){
		return flgApprovedStatus;
	}

	public String getInRestaurantId(){
		return inRestaurantId;
	}

	public String getDiscountValue(){
		return discountValue;
	}

	public String getDescription(){
		return description;
	}

	public String getDiscountType(){
		return discountType;
	}

	public String getTitle(){
		return title;
	}

	public String getCouponValidDate(){
		return couponValidDate;
	}

	public String getCreatedBy(){
		return createdBy;
	}

	public String getMinOrderValue(){
		return minOrderValue;
	}

	public String getNoOfUse(){
		return noOfUse;
	}

	public String getFlgIsDelete(){
		return flgIsDelete;
	}

	public String getInCouponId(){
		return inCouponId;
	}

	public String getMaxAmount(){
		return maxAmount;
	}

	public String getStCouponCode(){
		return stCouponCode;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public class discount_slabs implements Serializable {
		@SerializedName("discount")
		private String discount;

		public String getDiscount() {
			return discount;
		}

		public void setDiscount(String discount) {
			this.discount = discount;
		}

		public String getOrdervalue() {
			return ordervalue;
		}

		public void setOrdervalue(String ordervalue) {
			this.ordervalue = ordervalue;
		}

		@SerializedName("ordervalue")
		private String ordervalue;

	}

	@Override
 	public String toString(){
		return 
			"CouponItem{" + 
			"flg_approved_status = '" + flgApprovedStatus + '\'' + 
			",in_restaurant_id = '" + inRestaurantId + '\'' + 
			",discount_value = '" + discountValue + '\'' + 
			",description = '" + description + '\'' + 
			",discount_type = '" + discountType + '\'' + 
			",title = '" + title + '\'' + 
			",coupon_valid_date = '" + couponValidDate + '\'' + 
			",created_by = '" + createdBy + '\'' + 
			",min_order_value = '" + minOrderValue + '\'' + 
			",no_of_use = '" + noOfUse + '\'' + 
			",flg_is_delete = '" + flgIsDelete + '\'' + 
			",in_coupon_id = '" + inCouponId + '\'' + 
			",max_amount = '" + maxAmount + '\'' + 
			",st_coupon_code = '" + stCouponCode + '\'' + 
			",created_date = '" + createdDate + '\'' + 
			"}";
		}
}