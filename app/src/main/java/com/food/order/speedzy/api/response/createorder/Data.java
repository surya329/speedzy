package com.food.order.speedzy.api.response.createorder;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Data{

	@SerializedName("createOrders")
	private List<CreateOrdersItem> createOrders;

	public List<CreateOrdersItem> getCreateOrders(){
		return createOrders;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"createOrders = '" + createOrders + '\'' + 
			"}";
		}
}