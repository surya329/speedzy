package com.food.order.speedzy.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.food.order.speedzy.Activity.MealComboActivity;
import com.food.order.speedzy.Activity.RestaurantActivity_NewVersion;
import com.food.order.speedzy.Model.RestaurantModel;
import com.food.order.speedzy.Model.ShopListModelNew;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.RecyclerViewMargin;
import com.food.order.speedzy.Utils.SpeedyLinearLayoutManager;
import com.food.order.speedzy.api.response.home.Alltimings;
import com.food.order.speedzy.api.response.home.OpenCloseStatus;
import com.food.order.speedzy.api.response.home.SectionsItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class MultiViewTypeAdapter extends RecyclerView.Adapter {

  private List<SectionsItem> mData;
  Activity mContext;
  int total_types;
  Alltimings alltimings;
  OpenCloseStatus open_close_status;
  public static int sCorner = 15;
  public static int sMargin = 8;

  public static class MealComboTypeViewHolder extends RecyclerView.ViewHolder {
    TextView tittle;
    RecyclerView mealrecyclerView;
    RelativeLayout see_all_ley;
    TextView see_all_location;

    public MealComboTypeViewHolder(View itemView) {
      super(itemView);
      this.tittle = (TextView) itemView.findViewById(R.id.tittle);
      this.mealrecyclerView = (RecyclerView) itemView.findViewById(R.id.mealrecyclerView);
      this.see_all_ley = (RelativeLayout) itemView.findViewById(R.id.see_all_ley);
      this.see_all_location = (TextView) itemView.findViewById(R.id.see_all_location);
    }
  }

  public static class PatanjaliTypeViewHolder extends RecyclerView.ViewHolder {
    ImageView itemImage;
    TextView button;
    RelativeLayout relative_layout;
    View viewDivider;

    public PatanjaliTypeViewHolder(View itemView) {
      super(itemView);
      this.itemImage = (ImageView) itemView.findViewById(R.id.itemImage);
      this.button = (TextView) itemView.findViewById(R.id.button);
      this.relative_layout = (RelativeLayout) itemView.findViewById(R.id.relative_layout);
      this.viewDivider = (View) itemView.findViewById(R.id.viewDivider);
    }
  }

  //public MultiViewTypeAdapter(OpenCloseStatus open_close_status,
  //    Alltimings alltimings, List<SectionsItem> mData,
  //    Activity context) {
  //  this.mData = mData;
  //  this.mContext = context;
  //  this.alltimings = alltimings;
  //  this.open_close_status = open_close_status;
  //  total_types = mData.size();
  //}

  public MultiViewTypeAdapter(Activity context) {
    this.mData = new ArrayList<>();
    this.mContext = context;
  }

  public void setAlltimings(Alltimings alltimings) {
    this.alltimings = alltimings;
  }

  public void setOpen_close_status(
      OpenCloseStatus open_close_status) {
    this.open_close_status = open_close_status;
  }

  public void setData(List<SectionsItem> data) {
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public void refreshData(List<SectionsItem> data) {
    mData.clear();
    setData(data);
  }

  @Override

  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    View view;
    switch (viewType) {
      case 1:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.meal_combo_type, parent, false);
        return new MealComboTypeViewHolder(view);
      case 2:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.meal_combo_type, parent, false);
        return new MealComboTypeViewHolder(view);
      case 3:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.meal_combo_type, parent, false);
        return new MealComboTypeViewHolder(view);
      case 4:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.patanjali_type_row, parent, false);
        return new PatanjaliTypeViewHolder(view);
      case 5:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.meal_combo_type, parent, false);
        return new MealComboTypeViewHolder(view);
      case 6:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.meal_combo_type, parent, false);
        return new MealComboTypeViewHolder(view);
      case 7:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.patanjali_type_row, parent, false);
        return new PatanjaliTypeViewHolder(view);
      case 8:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.meal_combo_type, parent, false);
        return new MealComboTypeViewHolder(view);
    }
    return null;
  }

  @Override
  public int getItemViewType(int position) {

    switch (Integer.parseInt(mData.get(position).getSectionType())) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
        return Integer.parseInt(mData.get(position).getSectionType());
      default:
        return 0;
    }
  }

  @Override
  public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {

    SectionsItem object = mData.get(listPosition);
    if (object != null) {
      switch (Integer.parseInt(object.getSectionType())) {
        case 1:
          MealCombo(holder, object, 1);
          break;
        case 2:
          MealCombo(holder, object, 2);
          break;
               /* case 2:
                    Commons.offer_list.clear();
                    Commons.offer_list.addAll(object.getItems());
                    MealCombo(holder, object, 2);
                    break;
                case 3:
                    Feature(holder, object, 3);
                    break;
                case 4:
                    Patanjali(holder, object, 4);
                    break;
                case 5:
                    MealCombo(holder, object, 5);
                    break;
                case 6:
                    Feature(holder, object, 6);
                    break;
                case 7:
                    Patanjali(holder, object, 7);
                    break;
                case 8:
                    Commons.insight_list.clear();
                    Commons.insight_list.addAll(object.getItems());
                    Feature(holder, object, 8);
                    break;*/

      }
    }
  }

  private void MealCombo(RecyclerView.ViewHolder holder, SectionsItem object,
      final int flag) {
  /*  int pixels = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
        Float.parseFloat(object.getHeight()), mContext.getResources().getDisplayMetrics());
    ViewGroup.LayoutParams params =
        ((MealComboTypeViewHolder) holder).mealrecyclerView.getLayoutParams();
    params.height = pixels;
    ((MealComboTypeViewHolder) holder).mealrecyclerView.requestLayout();*/

    ((MealComboTypeViewHolder) holder).tittle.setText(object.getTitle());
    //((MealComboTypeViewHolder) holder).mealrecyclerView.setHasFixedSize(true);
    ((MealComboTypeViewHolder) holder).mealrecyclerView.setLayoutManager(
        new SpeedyLinearLayoutManager(mContext, SpeedyLinearLayoutManager.HORIZONTAL, false));
    ((MealComboTypeViewHolder) holder).mealrecyclerView.setNestedScrollingEnabled(false);
    MealComboAdapter mealComboAdapter = null;

    float cal_height = Commons.image_height_width(mContext, object.getHeight(), object.getWidth());
    int pixels_height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, cal_height,
        mContext.getResources().getDisplayMetrics());

    if (flag == 1) {
      Commons.banner_height = pixels_height;
      SnapHelper snapHelper = new LinearSnapHelper();
      ((MealComboTypeViewHolder) holder).mealrecyclerView.setOnFlingListener(null);
      snapHelper.attachToRecyclerView(((MealComboTypeViewHolder) holder).mealrecyclerView);
      ((MealComboTypeViewHolder) holder).see_all_ley.setVisibility(View.GONE);
      mealComboAdapter =
          new MealComboAdapter(mContext, object.getItems(), pixels_height, "banner", alltimings,
              open_close_status);
      if (object.getItems().isEmpty()){
        ((MealComboTypeViewHolder) holder).mealrecyclerView.setVisibility(View.GONE);
      }
    }
    if (flag == 2) {
      ((MealComboTypeViewHolder) holder).see_all_ley.setVisibility(View.VISIBLE);
      mealComboAdapter =
          new MealComboAdapter(mContext, object.getItems(), pixels_height, "meal", alltimings,
              open_close_status);
      if (object.getItems().isEmpty()){
        ((MealComboTypeViewHolder) holder).see_all_ley.setVisibility(View.GONE);
        ((MealComboTypeViewHolder) holder).tittle.setVisibility(View.GONE);
        ((MealComboTypeViewHolder) holder).mealrecyclerView.setVisibility(View.GONE);
      }
    }
    if (flag == 5) {
      ((MealComboTypeViewHolder) holder).see_all_ley.setVisibility(View.GONE);
      //((MealComboTypeViewHolder) holder).see_all_ley.setVisibility(View.VISIBLE);
      mealComboAdapter =
          new MealComboAdapter(mContext, object.getItems(), pixels_height, "offer", alltimings,
              open_close_status);
    }
    ((MealComboTypeViewHolder) holder).mealrecyclerView.setAdapter(mealComboAdapter);
    if (flag == 1) {
      ((MealComboTypeViewHolder) holder).mealrecyclerView.smoothScrollToPosition(1);
    }
    ((MealComboTypeViewHolder) holder).see_all_ley.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (flag == 2) {
          Commons.menu_id = "M1021";
          Intent intent = new Intent(mContext, MealComboActivity.class);
          intent.putExtra("app_tittle", "Meal Combo");
          mContext.startActivity(intent);
          mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Intent offer = new Intent(mContext, MealComboActivity.class);
          offer.putExtra("app_tittle", "Deal & Offers");
          mContext.startActivity(offer);
          mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
  }

 /* private void Patanjali(RecyclerView.ViewHolder holder, final SectionsItem content,
      int flag) {

    float cal_height =
        Commons.image_height_width(mContext, content.getHeight(), content.getWidth());
    int pixels_height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, cal_height,
        mContext.getResources().getDisplayMetrics());

    ViewGroup.LayoutParams params =
        ((PatanjaliTypeViewHolder) holder).relative_layout.getLayoutParams();
    params.height = (int) pixels_height;
    ((PatanjaliTypeViewHolder) holder).relative_layout.requestLayout();
    ViewGroup.LayoutParams params1 = ((PatanjaliTypeViewHolder) holder).itemImage.getLayoutParams();
    params1.height = (int) pixels_height;
    ((PatanjaliTypeViewHolder) holder).itemImage.requestLayout();

    DrawableRequestBuilder<String> thumbnailRequest = Glide
        .with(mContext)
        .load(Commons.image_baseURL_very_small + content.getItems().get(0).getImagename());
    Glide.with(mContext)
        .load(Commons.image_baseURL + content.getItems().get(0).getImagename())
        .thumbnail(thumbnailRequest)
        .placeholder(R.drawable.patanjali_placeholder)
        .into(((PatanjaliTypeViewHolder) holder).itemImage);
    if (flag == 4) {
      ((PatanjaliTypeViewHolder) holder).button.setText("Browse");
      ((PatanjaliTypeViewHolder) holder).viewDivider.setVisibility(View.GONE);
    }
    if (flag == 7) {
      ((PatanjaliTypeViewHolder) holder).button.setText("Contact Us");
      ((PatanjaliTypeViewHolder) holder).viewDivider.setVisibility(View.VISIBLE);
    }

    ((PatanjaliTypeViewHolder) holder).itemImage.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        FoodinnsMediaPlayer.getInstance().playSound("pin_drop_sound.mp3", mContext);
        Commons.clicking_event(mContext, content.getItems().get(0), alltimings, open_close_status);
      }
    });
  }
*/
  private void Feature(RecyclerView.ViewHolder holder, final SectionsItem object,
      final int flag) {
    ((MealComboTypeViewHolder) holder).tittle.setText(object.getTitle());
    ((MealComboTypeViewHolder) holder).see_all_ley.setVisibility(View.VISIBLE);
    ((MealComboTypeViewHolder) holder).see_all_location.setVisibility(View.GONE);
    ((MealComboTypeViewHolder) holder).mealrecyclerView.setHasFixedSize(true);
    if (flag == 3) {
      final LinearLayoutManager mLayoutManager;
      mLayoutManager = new GridLayoutManager(mContext, 3);
      ((MealComboTypeViewHolder) holder).mealrecyclerView.setLayoutManager(mLayoutManager);
      ((MealComboTypeViewHolder) holder).mealrecyclerView.setNestedScrollingEnabled(false);
      RecyclerViewMargin decoration = new RecyclerViewMargin(10, 3);
      ((MealComboTypeViewHolder) holder).mealrecyclerView.addItemDecoration(decoration);
    }
    if (flag == 6 || flag == 8) {
      ((MealComboTypeViewHolder) holder).mealrecyclerView.setLayoutManager(
          new SpeedyLinearLayoutManager(mContext, SpeedyLinearLayoutManager.HORIZONTAL, false));
      ((MealComboTypeViewHolder) holder).mealrecyclerView.setNestedScrollingEnabled(false);
      RecyclerViewMargin decoration = new RecyclerViewMargin(10, object.getItems().size());
      ((MealComboTypeViewHolder) holder).mealrecyclerView.addItemDecoration(decoration);
    }
    FeatureAdapter featureAdapter =
        new FeatureAdapter(mContext, object.getItems(), object.getWidth(), object.getHeight(), flag,
            alltimings, open_close_status);
    ((MealComboTypeViewHolder) holder).mealrecyclerView.setAdapter(featureAdapter);
    ((MealComboTypeViewHolder) holder).see_all_ley.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (flag == 8) {
          Commons.flag_for_hta = "7";
          Intent i = new Intent(mContext, RestaurantActivity_NewVersion.class);
          i.putExtra("res_id", object.getItems().get(0).getRestId());
          i.putExtra("menu_id", object.getItems().get(0).getMenuID());
          i.putExtra("nav_type", object.getItems().get(0).getNavType());
          i.putExtra("name", object.getItems().get(0).getName());
          mContext.startActivity(i);
          mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
  }

  private RestaurantModel transform(ShopListModelNew.Restaurants restaurant_model) {
    String base_url_logo =
        "https://storage.googleapis.com/speedzy_data/resources/restaurantlogo/";
    RestaurantModel obj = new RestaurantModel();
    obj.setmResPic(base_url_logo + "big/" + restaurant_model.getRestaurantLogoImage());
    obj.setmResName(restaurant_model.getStRestaurantName());
    obj.setmRating(restaurant_model.getAvgRating() + " Rating");
    obj.setmRatingBar(Float.parseFloat(restaurant_model.getAvgRating()));

    if (open_close_status.getOpenstatus().toString().equalsIgnoreCase("0")) {
      obj.setmOpenClose("Close");
    } else if (open_close_status.getOpenstatus().toString().equalsIgnoreCase("1")) {
      obj.setmOpenClose("Open");
    }
    obj.setmMinOrder("Min ₹" + restaurant_model.getStMinOrder());
    //if (nav_type.equalsIgnoreCase("7") || menu_id.equalsIgnoreCase("M1014")) {
    //    obj.setmMenu("Book");
    //} else {
    //    obj.setmMenu("Menu");
    //}
    return obj;
  }

  @Override
  public int getItemCount() {
    return mData.size();
  }
}