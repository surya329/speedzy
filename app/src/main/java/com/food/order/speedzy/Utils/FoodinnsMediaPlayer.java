package com.food.order.speedzy.Utils;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

public class FoodinnsMediaPlayer {


    private FoodinnsMediaPlayer() {
    }

    private static class FoodinnsMediaPlayerHelper {
        private static final FoodinnsMediaPlayer INSTANCE = new FoodinnsMediaPlayer();
    }

    public static FoodinnsMediaPlayer getInstance() {
        return FoodinnsMediaPlayerHelper.INSTANCE;
    }


    public void playSound(String filename, Context mContext) {
        try {

            AssetFileDescriptor afd = mContext.getAssets().openFd(filename);
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
