package com.food.order.speedzy.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Adapter.MenuAdapter;
import com.food.order.speedzy.Adapter.RestaurantDetailsAdapter;
import com.food.order.speedzy.Fragment.MyDialogFragment;
import com.food.order.speedzy.Model.Cart_Model;
import com.food.order.speedzy.Model.Dishdetail;
import com.food.order.speedzy.Model.Dishitem;
import com.food.order.speedzy.Model.Preferencemodel;
import com.food.order.speedzy.Model.RestaurantModel;
import com.food.order.speedzy.Model.Restaurant_Dish_Model;
import com.food.order.speedzy.Model.Restaurant_Dish_Model_New;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.root.BaseActivity;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RestarantDetails extends BaseActivity{
    static String name;
    String res_image;
    String res_logo_image;
    String res_minorder;
    String res_id;
    String res_rating;
    String rest_street;
    String res_reviews;
    String postcode;
    String suburb;
    String start_time_lunch;
    String end_time_lunch;
    String start_time_dinner;
    String end_time_dinner;
    String app_openstatus;
    String Promotion_msg,lati,longi;
    static String Flg_ac_status="";
    static String menu_id="";
    String nav_type;
    ImageView res_imgview, res_img_logo;
    androidx.appcompat.widget.Toolbar toolbar;
    TextView title;
    CollapsingToolbarLayout collapsingToolbarLayout;
    public static  RelativeLayout cartaddedlay;
    public static LinearLayout add_item_layout;
    public static TextView item;
    public static TextView cart_res_name;
    MySharedPrefrencesData mySharedPrefrencesData;
    String Veg_Flag;
    String base_url = "https://storage.googleapis.com/speedzy_data/resources/restaurant/";
    String base_url_logo =
            "https://storage.googleapis.com/speedzy_data/resources/restaurantlogo/";
    Restaurant_Dish_Model_New restmodel;
    private View viewToAttachDisplayerTo;
    boolean network_status = true;
    static boolean additem=false;
    RatingBar ratingbar;
    TextView rating,avg_rating;
    LinearLayout rating_linear;

    public static int sCorner = 30;
    public static int sMargin = 0;
    @BindView(R.id.res_logo) ImageView imgLogo;
    @BindView(R.id.open_close_img) ImageView open_close_img;
    @BindView(R.id.restname)
    TextView txtResturantName;
    @BindView(R.id.msg) TextView txtMsg;
    @BindView(R.id.min_order) TextView txtMinOrder;
    @BindView(R.id.res_address) TextView res_address;
    RestaurantModel restaurantModel;
    LOcaldbNew lOcaldbNew= new LOcaldbNew(RestarantDetails.this);;
    static ArrayList<Cart_Model.Cart_Details> cd = new ArrayList<>();
    CoordinatorLayout coordinator;
    LinearLayout includedLayout;

    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    NestedScrollView nestedScrollingView;
    public RestaurantDetailsAdapter adapter;
    Switch veg_switch;
    TextView search_menu,menu,parent_dish_name;
    String userid;
    String pick_del;
    String delivery_fee;
    ArrayList<Restaurant_Dish_Model.NewOffers> offerArrayList;
    ArrayList<Dishitem> parentDishArrayList;
    ArrayList<Dishdetail> childDishArrayList;
    ArrayList<Dishitem> parentDishArrayListVeg;
    ArrayList<Dishdetail> childDishArrayListVeg;
    int quantity;
    private static double totalsum = 0;
    private String mMenuId = "";
    ImageView gifImage;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restarant_details);
        ButterKnife.bind(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        viewToAttachDisplayerTo = findViewById(R.id.rex_details);


        mySharedPrefrencesData = new MySharedPrefrencesData();
        Veg_Flag = mySharedPrefrencesData.getVeg_Flag(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title=findViewById(R.id.title);
        collapsingToolbarLayout= findViewById(R.id.collapsingToolbarLayout);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_back);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        statusbar_bg(R.color.red);

        nestedScrollingView= findViewById(R.id.nestedScrollView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_restaurant_details);
        gifImage=findViewById(R.id.gifImage);
        cartaddedlay = findViewById(R.id.cart_add_layout);
        coordinator= findViewById(R.id.coordinator);
        includedLayout=findViewById(R.id.includedLayout);
        add_item_layout=(LinearLayout) findViewById(R.id.add_item_layout);
        item = (TextView) findViewById(R.id.item);
        cart_res_name = (TextView) findViewById(R.id.cart_res_name);
        rating_linear=(LinearLayout) findViewById(R.id.linear1);
        rating = (TextView) findViewById(R.id.rating);
        avg_rating= (TextView) findViewById(R.id.avg_rating);
        ratingbar = (RatingBar) findViewById(R.id.ratingbar);

        veg_switch=findViewById(R.id.veg_switch);
        search_menu=findViewById(R.id.search_menu);
        menu=findViewById(R.id.menu);
        parent_dish_name=findViewById(R.id.parent_dish_name);

        res_id = getIntent().getStringExtra("res_id");
        if (getIntent().getStringExtra("res_name") != null) {
            additem=getIntent().getBooleanExtra("additem",false);
            name = getIntent().getStringExtra("res_name");
            res_image = getIntent().getStringExtra("res_image");
            res_logo_image = getIntent().getStringExtra("res_logo_image");
            rest_street = getIntent().getStringExtra("res_add");
            res_minorder = getIntent().getStringExtra("res_minorder");
            res_rating = getIntent().getStringExtra("res_rating");
            res_reviews = getIntent().getStringExtra("res_reviews");
            Promotion_msg=getIntent().getStringExtra("Promotion_msg");
            lati=getIntent().getStringExtra("lati");
            longi=getIntent().getStringExtra("longi");
            Flg_ac_status=getIntent().getStringExtra("Flg_ac_status");
            app_openstatus = getIntent().getStringExtra("app_openstatus");
            menu_id = getIntent().getStringExtra("menu_id");
            nav_type = getIntent().getStringExtra("nav_type");
            start_time_lunch = getIntent().getStringExtra("start_time_lunch");
            end_time_lunch = getIntent().getStringExtra("end_time_lunch");
            start_time_dinner = getIntent().getStringExtra("start_time_dinner");
            end_time_dinner = getIntent().getStringExtra("end_time_dinner");
            suburb = getIntent().getStringExtra("Suburb");
            postcode = getIntent().getStringExtra("postcode");
            restaurantModel = (RestaurantModel) getIntent().getParcelableExtra("extra_model");
            //setUIData();
            if (nav_type.equalsIgnoreCase("3") && menu_id.equalsIgnoreCase("M1014")) {
            } else {
                //restapicall(res_id);
            }
        }
        title.setText(name);
        restapicall(res_id,1);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                   // collapsingToolbarLayout.setTitle(name);
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        if (Veg_Flag.equalsIgnoreCase("1")) {
            statusbar_bg(R.color.red);
        } else {
            statusbar_bg(R.color.red);
        }

        res_imgview = (ImageView) findViewById(R.id.res_img);
        res_img_logo = (ImageView) findViewById(R.id.res_img_logo);
        Commons.restaurant_id = res_id;
        rating_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(RestarantDetails.this, Reviews.class);
                if (nav_type.equalsIgnoreCase("1") || restaurantModel == null) {
                    in.putExtra("rest_id", restmodel.getRestaurant_details().get(0).getIn_restaurant_id());
                    in.putExtra("Image",restmodel.getRestaurant_details()
                            .get(0)
                            .getRestaurant_main_image());
                    in.putExtra("Name", restmodel.getRestaurant_details().get(0).getSt_restaurant_name());
                    in.putExtra("Address", restmodel.getRestaurant_details().get(0).getSt_street_address());
                    in.putExtra("Reviews", restmodel.getRestaurant_details().get(0).getCount_rating());
                    in.putExtra("Rating", restmodel.getRestaurant_details().get(0).getAvg_rating());
                    in.putExtra("Suburb", restmodel.getRestaurant_details().get(0).getSt_suburb());
                    in.putExtra("postcode", "");
                }else {
                    in.putExtra("rest_id", restaurantModel.getmInRestaurantId());
                    in.putExtra("Image", restaurantModel.getmResPic());
                    in.putExtra("Name", restaurantModel.getmResName());
                    in.putExtra("Address", restaurantModel.getmStStreetAddress());
                    in.putExtra("Reviews", restaurantModel.getmRating());
                    in.putExtra("Rating", restaurantModel.getmRatingBar());
                    in.putExtra("Suburb", restaurantModel.getmStSuburb());
                    in.putExtra("postcode", "");
                }
                startActivity(in);
            }
        });
        add_item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",1);
                setResult(RESULT_OK,returnIntent);
                Commons.back_button_transition(RestarantDetails.this);
            }
        });
        cartaddedlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menu_id.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID)) {
                    Commons.flag_for_hta = SpeedzyConstants.Fruits;
                    Intent intent = new Intent(RestarantDetails.this, CheckoutActivity_New.class);
                    intent.putExtra("ordernumber", restmodel.getOrders_count());
                    intent.putExtra("avg_order_value", restmodel.getAvg_order_value());
                    intent.putExtra("start_time_lunch", start_time_lunch);
                    intent.putExtra("end_time_lunch", end_time_lunch);
                    intent.putExtra("start_time_dinner", start_time_dinner);
                    intent.putExtra("end_time_dinner", end_time_dinner);
                    intent.putExtra("app_openstatus", app_openstatus);
                    intent.putExtra("offerlist", (Serializable) restmodel.getOffers());
                    intent.putExtra("lati",lati);
                    intent.putExtra("longi",longi);
                    startActivityForResult(intent,22);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                } else {
                    Intent intent = new Intent(RestarantDetails.this, CheckoutActivity_New.class);
                    intent.putExtra("ordernumber", restmodel.getOrders_count());
                    intent.putExtra("avg_order_value", restmodel.getAvg_order_value());
                    intent.putExtra("start_time_lunch", start_time_lunch);
                    intent.putExtra("end_time_lunch", end_time_lunch);
                    intent.putExtra("start_time_dinner", start_time_dinner);
                    intent.putExtra("end_time_dinner", end_time_dinner);
                    intent.putExtra("app_openstatus", app_openstatus);
                    intent.putExtra("offerlist", (Serializable) restmodel.getOffers());
                    intent.putExtra("lati",lati);
                    intent.putExtra("longi",longi);
                    startActivityForResult(intent,22);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
            }
        });
        Glide.with(this)
                .asGif()
                .load(R.drawable.loader)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .into(gifImage);
    }

    public void restapicall(String res_id,int flag) {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<Restaurant_Dish_Model_New> call =
                methods.setRes_dish(res_id, mySharedPrefrencesData.getUser_Id(this), Veg_Flag);
        Log.d("url", "url=" +flag+ call.request().url().toString());
        call.enqueue(new Callback<Restaurant_Dish_Model_New>() {
            @Override
            public void onResponse(Call<Restaurant_Dish_Model_New> call,
                                   Response<Restaurant_Dish_Model_New> response) {

                restmodel = response.body();
                parentDishArrayList = new ArrayList<>();
                parentDishArrayListVeg= new ArrayList<>();
                childDishArrayList = new ArrayList<>();
                childDishArrayListVeg = new ArrayList<>();
                if (restmodel.getStatus().equalsIgnoreCase("Success")) {
                    int statusCode = response.code();
                    mMenuId=menu_id;
                    setHeaderData(restmodel);
                    selectFrag();
                    coordinator.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Restaurant_Dish_Model_New> call, @NonNull Throwable t) {
                gifImage.setVisibility(View.GONE);
            }
        });
    }

    private void selectFrag() {
        if (mMenuId.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID) || Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)) {
            cd = lOcaldbNew.getFruitCart_Details();
            quantity = lOcaldbNew.getFruitQuantity(Commons.restaurant_id);
            setPriceDetails(cd, quantity);
        } else {
            cd = lOcaldbNew.getCart_Details();
            quantity = lOcaldbNew.getQuantity(Commons.restaurant_id);
            setPriceDetails(cd, quantity);
        }
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        setData();
        offerArrayList = (ArrayList<Restaurant_Dish_Model.NewOffers>) restmodel.getOffers();
        adapter =
                new RestaurantDetailsAdapter(gifImage,parentDishArrayList,  RestarantDetails.this,
                        res_id, restmodel.getRestaurant_details().get(0).getSt_restaurant_name(),
                        restmodel.getRestaurant_details().get(0).getSt_street_address(), suburb, postcode, pick_del,
                        delivery_fee, /*devliverySuburbs,*/ offerArrayList,
                        restmodel.getAvg_order_value(),
                        restmodel.getOrders_count(),/*restmodel.getDeliveryAreaList(),*/7, mMenuId,additem,Flg_ac_status, restmodel.getRestaurant_details().get(0).getRestcharge(),restmodel.getRestaurant_details().get(0).getRestaurant_main_image());
        recyclerView.setAdapter(adapter);
        veg_switch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Switch btn = (Switch) v;
                if (!btn.isChecked()) {
                    adapter.updateList(parentDishArrayList);
                }else {
                    adapter.updateList(parentDishArrayListVeg);
                }
            }
        });
        search_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogMenuList();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupmenu();
            }
        });
        /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                parent_dish_name_show();
            }
        });*/
    }

    private void parent_dish_name_show() {
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
        if (firstVisibleItemPosition!=0) {
            parent_dish_name.setVisibility(View.VISIBLE);
        }else {
            parent_dish_name.setVisibility(View.GONE);
        }
        if (veg_switch.isChecked()){
            parent_dish_name.setText(parentDishArrayListVeg.get(firstVisibleItemPosition).getCuisineName());
        }else {
            parent_dish_name.setText(parentDishArrayList.get(firstVisibleItemPosition).getCuisineName());
        }
    }

    private void popupmenu() {
        PopupWindow popup = new PopupWindow(this);
        View layout = getLayoutInflater().inflate(R.layout.popup_list, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(menu);

        RecyclerView recyclerViewmenu=layout.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewmenu.setLayoutManager(layoutManager);
        recyclerViewmenu.setHasFixedSize(true);
        recyclerViewmenu.setNestedScrollingEnabled(false);
        MenuAdapter menuAdapter;
        if (veg_switch.isChecked()) {
            menuAdapter = new MenuAdapter(parentDishArrayListVeg, RestarantDetails.this,popup,recyclerView,nestedScrollingView);
        }else {
            menuAdapter = new MenuAdapter(parentDishArrayList, RestarantDetails.this,popup,recyclerView,nestedScrollingView);
        }
        recyclerViewmenu.setAdapter(menuAdapter);
        menuAdapter.notifyDataSetChanged();
    }

    private void setData() {
        int sizenew = restmodel.getOffers().size();

        if (sizenew > 0) {

            for (int k = 0; k < restmodel.getOffers().size(); k++) {
                childDishArrayList.add(new Dishdetail("",
                        "",
                        restmodel.getOffers().get(k).getOffer_description(),
                        null,
                        "",
                        "",
                        /*null,*/
                        "",
                        "",
                        "",
                        ""
                ));
                childDishArrayListVeg.add(new Dishdetail("",
                        "",
                        restmodel.getOffers().get(k).getOffer_description(),
                        null,
                        "",
                        "",
                        /*null,*/
                        "",
                        "",
                        "",
                        ""
                ));
            }
            parentDishArrayList.add(0, new Dishitem("Special offers", childDishArrayList));
            parentDishArrayListVeg.add(0, new Dishitem("Special offers", childDishArrayListVeg));
        }
        for (int i = 0; i < restmodel.getCuisines().size(); i++) {
            childDishArrayList = new ArrayList<>();
            childDishArrayListVeg = new ArrayList<>();
            if (!restmodel.getCuisines().get(i).getDishdetails().isEmpty()) {
                for (int j = 0; j < restmodel.getCuisines().get(i).getDishdetails().size(); j++) {

                    childDishArrayList.add(
                            new Dishdetail(restmodel.getCuisines().get(i).getDishdetails().get(j).getInDishId(),
                                    restmodel.getCuisines().get(i).getDishdetails().get(j).getStDishName(),
                                    restmodel.getCuisines().get(i).getDishdetails().get(j).getDishDesription(),
                                    restmodel.getCuisines().get(i).getDishdetails().get(j).getStPrice(),
                                    restmodel.getCuisines().get(i).getDishdetails().get(j).getFlgAddChoices(),
                                    restmodel.getCuisines().get(i).getDishdetails().get(j).getDishCuisineId(),
                                    /*null,*/
                                    restmodel.getCuisines().get(i).getDishdetails().get(j).getEgg_status(),
                                    restmodel.getCuisines().get(i).getDishdetails().get(j).getNon_veg_status(),
                                    restmodel.getCuisines().get(i).getDishdetails().get(j).getCake_flg(),
                                    restmodel.getCuisines().get(i).getDishdetails().get(j).getMax_qty_peruser()
                            ));
                    if (restmodel.getCuisines().get(i).getDishdetails().get(j).getNon_veg_status().equalsIgnoreCase("0")) {
                        childDishArrayListVeg.add(
                                new Dishdetail(restmodel.getCuisines().get(i).getDishdetails().get(j).getInDishId(),
                                        restmodel.getCuisines().get(i).getDishdetails().get(j).getStDishName(),
                                        restmodel.getCuisines().get(i).getDishdetails().get(j).getDishDesription(),
                                        restmodel.getCuisines().get(i).getDishdetails().get(j).getStPrice(),
                                        restmodel.getCuisines().get(i).getDishdetails().get(j).getFlgAddChoices(),
                                        restmodel.getCuisines().get(i).getDishdetails().get(j).getDishCuisineId(),
                                        /*null,*/
                                        restmodel.getCuisines().get(i).getDishdetails().get(j).getEgg_status(),
                                        restmodel.getCuisines().get(i).getDishdetails().get(j).getNon_veg_status(),
                                        restmodel.getCuisines().get(i).getDishdetails().get(j).getCake_flg(),
                                        restmodel.getCuisines().get(i).getDishdetails().get(j).getMax_qty_peruser()
                                ));
                    }
                }
            }else {
                childDishArrayList.add(new Dishdetail("",
                        "",
                        "No Items",
                        null,
                        "",
                        "",
                        /*null,*/
                        "",
                        "",
                        "",
                        ""
                ));
                childDishArrayListVeg.add(new Dishdetail("",
                        "",
                        "No Items",
                        null,
                        "",
                        "",
                        /*null,*/
                        "",
                        "",
                        "",
                        ""
                ));
            }

            parentDishArrayList.add(
                    new Dishitem(restmodel.getCuisines().get(i).getCuisineName(), childDishArrayList));
            if (!childDishArrayListVeg.isEmpty()) {
                parentDishArrayListVeg.add(
                        new Dishitem(restmodel.getCuisines().get(i).getCuisineName(), childDishArrayListVeg));
            }
        }
    }

    public static void setPriceDetails(ArrayList<Cart_Model.Cart_Details> listdata, int quantity) {

        int qty = 0;
        if (listdata != null) {
            totalsum = 0.00;
            qty = 0;
            for (int k = 0; k < listdata.size(); k++) {
                final Cart_Model.Cart_Details cart_details = listdata.get(k);
                qty = Integer.parseInt(cart_details.getQuantity());
                if (!GeneralUtil.isStringEmpty(cart_details.getMenu_price())) {
                    double dishPrice = Double.parseDouble(cart_details.getMenu_price()) * qty;
                    ArrayList<ArrayList<Preferencemodel>> myarray = cart_details.getPreferencelfromdb();
                    if (myarray.size() > 1) {

                        for (int i = 1; i < myarray.size(); i++) {
                            ArrayList<Preferencemodel> mychildarray = cart_details.getPreferencelfromdb().get(i);
                            for (int j = 0; j < mychildarray.size(); j++) {
                                Preferencemodel prefData = mychildarray.get(j);
                                if (prefData.getMenuprice().length() > 2) {
                                    double itemPrice = Double.parseDouble(prefData.getMenuprice()) * qty;
                                    dishPrice = dishPrice + itemPrice;
                                } else {
                                    double itemPrice = 0.00;
                                    dishPrice = dishPrice + itemPrice;
                                }
                            }
                        }
                    }
                    totalsum = totalsum + dishPrice;
                }
            }
        }

        if (quantity == 0) {
            try {
                cartaddedlay.setVisibility(View.VISIBLE);
                item.setText("0 Items  |  "+"₹" + String.format("%.2f", 0.00));
                cart_res_name.setText("From : " +name);
            } catch (Exception e) {
            }
        } else {
            try {
                cartaddedlay.setVisibility(View.VISIBLE);
                item.setText(String.valueOf(quantity)+" Items  |  "+"₹" + String.format("%.2f", totalsum));
                cart_res_name.setText("From : " + listdata.get(0).getSt_restaurant_name());
            } catch (Exception e) {

            }
        }
        if (additem){
            cartaddedlay.setVisibility(View.GONE);
            add_item_layout.setVisibility(View.VISIBLE);
        }else {
            cartaddedlay.setVisibility(View.VISIBLE);
            add_item_layout.setVisibility(View.GONE);
            if (Flg_ac_status.equalsIgnoreCase("2")){
                cartaddedlay.setVisibility(View.GONE);
                add_item_layout.setVisibility(View.GONE);
            }
        }
    }

    private void showDialogMenuList() {
        ArrayList<Dishitem> parentDishArrayList_filter=new ArrayList<>();
        if (veg_switch.isChecked()){
            parentDishArrayList_filter=parentDishArrayListVeg;
        }else {
            parentDishArrayList_filter=parentDishArrayList;
        }
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        DialogFragment newFragment = MyDialogFragment.newInstance(parentDishArrayList_filter, RestarantDetails.this,
                res_id, txtResturantName.getText().toString(), res_address.getText().toString(), suburb, postcode, pick_del,
                delivery_fee, offerArrayList,
                restmodel.getAvg_order_value(),
                restmodel.getOrders_count(),7, mMenuId,additem,Flg_ac_status, restmodel.getRestaurant_details().get(0).getRestcharge(),restmodel.getRestaurant_details().get(0).getRestaurant_main_image());
        newFragment.show(ft, "dialog");
    }

    private void setHeaderData(Restaurant_Dish_Model_New model) {
        if (model.getRestaurant_details() != null && model.getRestaurant_details().size() > 0) {
            if(!isFinishing()) {
                RequestBuilder<Drawable> thumbnailRequest = Glide
                    .with(this)
                    .load(/*base_url + "very_small/" +*/ restmodel.getRestaurant_details()
                        .get(0).getRestaurant_main_image())
                    .apply(RequestOptions.bitmapTransform(new RoundedCorners(sCorner))
                        .placeholder(R.drawable.combo_placeholder));

                Glide
                    .with(this)
                    .load(/*base_url_logo + "big/" +*/ restmodel.getRestaurant_details()
                        .get(0).getRestaurant_main_image())
                    .thumbnail(thumbnailRequest)
                    .apply(RequestOptions.bitmapTransform(new RoundedCorners(sCorner))
                        .placeholder(R.drawable.combo_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(imgLogo);
            }
            toolbar.setTitle(model.getRestaurant_details().get(0).getSt_restaurant_name());
            txtResturantName.setText(model.getRestaurant_details().get(0).getSt_restaurant_name());
            res_address.setText(model.getRestaurant_details().get(0).getSt_street_address());
            rating.setText("("+restmodel.getRestaurant_details().get(0).getCount_rating() + " Ratings)");
            avg_rating.setText(""+restmodel.getRestaurant_details().get(0).getAvg_rating());
            ratingbar.setRating(Float.parseFloat(restmodel.getRestaurant_details().get(0).getAvg_rating()));
            txtMsg.setVisibility(View.GONE);
            txtMsg.setText("");
            txtMinOrder.setText("Min ₹" + model.getRestaurant_details().get(0).getSt_min_order());
            open_close_img.setImageResource(R.drawable.ic_open);
        }
    }

    private void statusbar_bg(int color) {
        //collapsingToolbarLayout.setContentScrimColor(getResources().getColor(color));
        //collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(color));
   /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,color ));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.rex_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search) {
            Intent intent = new Intent(RestarantDetails.this, Resturant_SearchActivity.class);
            intent.putExtra("start_time_lunch", start_time_lunch);
            intent.putExtra("end_time_lunch", end_time_lunch);
            intent.putExtra("start_time_dinner", start_time_dinner);
            intent.putExtra("end_time_dinner", end_time_dinner);
            intent.putExtra("app_openstatus", app_openstatus);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            return true;
        }
        if (id == R.id.cart) {
            if (menu_id.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID)) {
                Commons.flag_for_hta = SpeedzyConstants.Fruits;
            }
            Intent intent = new Intent(RestarantDetails.this, CheckoutActivity_New.class);
            intent.putExtra("start_time_lunch", start_time_lunch);
            intent.putExtra("end_time_lunch", end_time_lunch);
            intent.putExtra("start_time_dinner", start_time_dinner);
            intent.putExtra("end_time_dinner", end_time_dinner);
            intent.putExtra("app_openstatus", app_openstatus);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //supportFinishAfterTransition();
        Commons.back_button_transition(RestarantDetails.this);
    }

    @Override public void onStart() {
        super.onStart();

        //restapicall(res_id,1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == 1) {
            finish();
        }else if (resultCode == RESULT_OK && requestCode==22){
            String res_id1=data.getStringExtra("res_id");
            if (!res_id1.equalsIgnoreCase("")) {
                //restapicall(cd.get(0).getIn_restaurant_id(), 2);
                res_id=res_id1;
                restapicall(res_id1, 2);
            }else {
                restapicall(res_id, 2);
            }
        }else {
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}