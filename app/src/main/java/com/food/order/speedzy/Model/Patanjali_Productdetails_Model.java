package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */


public class Patanjali_Productdetails_Model implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;

    public String getMinorder() {
        return minorder;
    }

    public void setMinorder(String minorder) {
        this.minorder = minorder;
    }

    @SerializedName("minorder")
    @Expose
    private String minorder;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Patanjali_Productdetails_Model.productlist> getProductlist() {
        return productlist;
    }

    public void setProductlist(List<Patanjali_Productdetails_Model.productlist> productlist) {
        this.productlist = productlist;
    }

    @SerializedName("productlist")
    @Expose
    private List<productlist> productlist=null;

    public List<Patanjali_Productdetails_Model.deliveryarea> getDeliveryarea() {
        return deliveryarea;
    }

    public void setDeliveryarea(List<Patanjali_Productdetails_Model.deliveryarea> deliveryarea) {
        this.deliveryarea = deliveryarea;
    }

    @SerializedName("deliveryarea")
    @Expose
    private List<deliveryarea> deliveryarea=null;

    public Patanjali_Productdetails_Model.delivery_time getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(Patanjali_Productdetails_Model.delivery_time delivery_time) {
        this.delivery_time = delivery_time;
    }

    @SerializedName("delivery_time")
    @Expose
    private delivery_time delivery_time=null;

    public Patanjali_Productdetails_Model.open_close_status getOpen_close_status() {
        return open_close_status;
    }

    public void setOpen_close_status(Patanjali_Productdetails_Model.open_close_status open_close_status) {
        this.open_close_status = open_close_status;
    }

    @SerializedName("open_close_status")
    @Expose
    private open_close_status open_close_status=null;

    public String getApp_openstatus() {
        return app_openstatus;
    }

    public void setApp_openstatus(String app_openstatus) {
        this.app_openstatus = app_openstatus;
    }

    @SerializedName("app_openstatus")
    @Expose
    private String app_openstatus=null;

    public List<Patanjali_Productdetails_Model.banners> getBanners() {
        return banners;
    }

    public void setBanners(List<Patanjali_Productdetails_Model.banners> banners) {
        this.banners = banners;
    }

    @SerializedName("banners")
    @Expose
    private List<banners> banners=null;
    public class banners implements Serializable {
        @SerializedName("name")
        @Expose
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @SerializedName("id")
        @Expose
        private String id;
    }
    public class open_close_status implements Serializable {
        @SerializedName("message")
        @Expose
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getOpenstatus() {
            return openstatus;
        }

        public void setOpenstatus(String openstatus) {
            this.openstatus = openstatus;
        }

        @SerializedName("openstatus")
        @Expose
        private String openstatus;
    }
    public class delivery_time implements Serializable{
        @SerializedName("morning_start")
        @Expose
        private String morning_start;
        @SerializedName("morning_end")
        @Expose
        private String morning_end;
        @SerializedName("evening_start")
        @Expose
        private String evening_start;

        public String getMorning_start() {
            return morning_start;
        }

        public void setMorning_start(String morning_start) {
            this.morning_start = morning_start;
        }

        public String getMorning_end() {
            return morning_end;
        }

        public void setMorning_end(String morning_end) {
            this.morning_end = morning_end;
        }

        public String getEvening_start() {
            return evening_start;
        }

        public void setEvening_start(String evening_start) {
            this.evening_start = evening_start;
        }

        public String getEvening_end() {
            return evening_end;
        }

        public void setEvening_end(String evening_end) {
            this.evening_end = evening_end;
        }

        @SerializedName("evening_end")
        @Expose
        private String evening_end;
    }
    public class deliveryarea implements Serializable {

        public String getPostcode_id() {
            return postcode_id;
        }

        public void setPostcode_id(String postcode_id) {
            this.postcode_id = postcode_id;
        }

        public String getIn_suburb_id() {
            return in_suburb_id;
        }

        public void setIn_suburb_id(String in_suburb_id) {
            this.in_suburb_id = in_suburb_id;
        }

        public String getSt_suburb() {
            return st_suburb;
        }

        public void setSt_suburb(String st_suburb) {
            this.st_suburb = st_suburb;
        }

        public String getSt_postcode() {
            return st_postcode;
        }

        public void setSt_postcode(String st_postcode) {
            this.st_postcode = st_postcode;
        }

        public String getSt_city() {
            return st_city;
        }

        public void setSt_city(String st_city) {
            this.st_city = st_city;
        }

        public String getSt_delivery_charge() {
            return st_delivery_charge;
        }

        public void setSt_delivery_charge(String st_delivery_charge) {
            this.st_delivery_charge = st_delivery_charge;
        }

        public String getFlg_delivery_status() {
            return flg_delivery_status;
        }

        public void setFlg_delivery_status(String flg_delivery_status) {
            this.flg_delivery_status = flg_delivery_status;
        }

        public String getFlg_menu_choice() {
            return flg_menu_choice;
        }

        public void setFlg_menu_choice(String flg_menu_choice) {
            this.flg_menu_choice = flg_menu_choice;
        }

        public String getNormal_del_charge() {
            return normal_del_charge;
        }

        public void setNormal_del_charge(String normal_del_charge) {
            this.normal_del_charge = normal_del_charge;
        }

        public String getMin_Order() {
            return min_Order;
        }

        public void setMin_Order(String min_Order) {
            this.min_Order = min_Order;
        }

        private String in_suburb_id;
        private String postcode_id;
        private String st_suburb;
        private String st_postcode;
        private String st_city;
        private String st_delivery_charge;
        private String flg_delivery_status;
        private String flg_menu_choice;
        private String normal_del_charge;
        private String min_Order;
    }

    public class productlist implements Serializable {
        @SerializedName("productID")
        @Expose
        private String productID;
        @SerializedName("img_baseURL")
        @Expose
        private String img_baseURL;
        @SerializedName("productName")
        @Expose
        private String productName;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("image_count")
        @Expose
        private String image_count;

        public String getProductID() {
            return productID;
        }

        public void setProductID(String productID) {
            this.productID = productID;
        }

        public String getImg_baseURL() {
            return img_baseURL;
        }

        public void setImg_baseURL(String img_baseURL) {
            this.img_baseURL = img_baseURL;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage_count() {
            return image_count;
        }

        public void setImage_count(String image_count) {
            this.image_count = image_count;
        }

        public String getSale_price() {
            return sale_price;
        }

        public void setSale_price(String sale_price) {
            this.sale_price = sale_price;
        }

        public String getProductMRP() {
            return productMRP;
        }

        public void setProductMRP(String productMRP) {
            this.productMRP = productMRP;
        }

        public String getIsFeatured() {
            return isFeatured;
        }

        public void setIsFeatured(String isFeatured) {
            this.isFeatured = isFeatured;
        }

        public String getMain_image() {
            return main_image;
        }

        public void setMain_image(String main_image) {
            this.main_image = main_image;
        }

        public String getImage() {
            return image;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getMax_stock() {
            return max_stock;
        }

        public void setMax_stock(String max_stock) {
            this.max_stock = max_stock;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getDiscount_type() {
            return discount_type;
        }

        public void setDiscount_type(String discount_type) {
            this.discount_type = discount_type;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public String getTax_type() {
            return tax_type;
        }

        public void setTax_type(String tax_type) {
            this.tax_type = tax_type;
        }

        @SerializedName("sale_price")
        @Expose
        private String sale_price;
        @SerializedName("productMRP")
        @Expose
        private String productMRP;
        @SerializedName("isFeatured")
        @Expose
        private String isFeatured;
        @SerializedName("main_image")
        @Expose
        private String main_image;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("max_stock")
        @Expose
        private String max_stock;
        @SerializedName("unit")
        @Expose
        private String unit;
        @SerializedName("discount")
        @Expose
        private String discount;
        @SerializedName("discount_type")
        @Expose
        private String discount_type;
        @SerializedName("tax")
        @Expose
        private String tax;
        @SerializedName("tax_type")
        @Expose
        private String tax_type;
        @SerializedName("image")
        @Expose
        private String image;

    }
}