package com.food.order.speedzy.RoomBooking;

/**
 * Created by wolfsoft5 on 26/5/18.
 */

public class HotelInnListModelClass {

    String title;

    public HotelInnListModelClass(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
