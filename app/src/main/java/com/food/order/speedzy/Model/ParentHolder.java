package com.food.order.speedzy.Model;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.order.speedzy.R;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

/**
 * Created by Sujata Mohanty.
 */

public class ParentHolder extends GroupViewHolder {

    public TextView parent_dish_name;
    RelativeLayout parentlay;
    public ParentHolder(View itemView) {
        super(itemView);
        parent_dish_name=(TextView)itemView.findViewById(R.id.parent_dish_name);
        parentlay=(RelativeLayout) itemView.findViewById(R.id.relativelayparent);
    }

    @Override
    public void expand() {

        parent_dish_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up, 0);
        parent_dish_name.setTextColor(0xff7a07cc);
        Log.i("Adapter", "expand");
    }

    @Override
    public void collapse() {
        Log.i("Adapter", "collapse");
        parent_dish_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
        parent_dish_name.setTextColor(Color.BLACK);


    }


}


