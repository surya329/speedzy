package com.food.order.speedzy.Adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Model.Patanjali_Productdetails_Model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.FoodinnsMediaPlayer;
import com.food.order.speedzy.api.response.home.Alltimings;
import com.food.order.speedzy.api.response.home.ItemsItem;
import com.food.order.speedzy.api.response.home.OpenCloseStatus;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class MealComboAdapter extends RecyclerView.Adapter<MealComboAdapter.SingleItemRowHolder> {

  private List<ItemsItem> itemsList;
  //List<Patanjali_Category_model.Banners> banners;
  private static Activity mContext;
  public static int sCorner = 30;
  public static int sMargin = 8;
  String base_url_thumb = "", base_url_main = "";
  String flag;
  int pixels_height;
  Alltimings alltimings;
  OpenCloseStatus open_close_status;
  int patanjali_banner = 0;
  List<Patanjali_Productdetails_Model.banners> water_bannerlist;

  public MealComboAdapter(Activity context, List<ItemsItem> itemsList,
      int pixels_height, String flag, Alltimings alltimings,
      OpenCloseStatus open_close_status) {
    this.itemsList = itemsList;
    this.mContext = context;
    this.flag = flag;
    this.pixels_height = pixels_height;
    this.alltimings = alltimings;
    this.open_close_status = open_close_status;
  }

  public MealComboAdapter(int patanjali_banner, Activity context, int pixels_height, String flag,
      Alltimings alltimings,
      OpenCloseStatus open_close_status) {
    this.mContext = context;
    this.patanjali_banner = patanjali_banner;
    this.flag = flag;
    this.pixels_height = pixels_height;
    this.alltimings = alltimings;
    this.open_close_status = open_close_status;
  }

  public MealComboAdapter(int patanjali_banner, Activity context,
      List<Patanjali_Productdetails_Model.banners> water_bannerlist, int pixels_height,
      String flag) {
    this.water_bannerlist = water_bannerlist;
    this.mContext = context;
    this.patanjali_banner = patanjali_banner;
    this.flag = flag;
    this.pixels_height = pixels_height;
  }

  @NonNull @Override
  public MealComboAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
    /*View v =
        LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.meal_combo_type_row, null);
    return new SingleItemRowHolder(v);*/

    View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.meal_combo_type_row, null, false);
    if (flag.equalsIgnoreCase("banner")){
      RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      view.setLayoutParams(lp);
    }
    return new SingleItemRowHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull final MealComboAdapter.SingleItemRowHolder holder, int i) {
    if (patanjali_banner == 0) {
      final ItemsItem items = itemsList.get(i);
      if (flag.equalsIgnoreCase("meal")) {
        base_url_thumb = "https://storage.googleapis.com/speedzy_data/resources/mealcombo/thumb/";
        base_url_main =
            "https://storage.googleapis.com/speedzy_data/resources/mealcombo/main_image/";
            /*float pixels_height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (SplashScreen.systemWidth/2-148), mContext.getResources().getDisplayMetrics());
            ViewGroup.LayoutParams params1= holder.itemImage.getLayoutParams();
            params1.height = (int) (pixels_height/2)+70;
            params1.width = (int) pixels_height;
            holder.itemImage.requestLayout();*/
        ViewGroup.LayoutParams params1 = holder.itemImage.getLayoutParams();
        params1.width = mContext.getResources().getDimensionPixelSize(R.dimen._190sdp);
        params1.height = mContext.getResources().getDimensionPixelSize(R.dimen._100sdp);
        holder.itemImage.requestLayout();
      } else if (flag.equalsIgnoreCase("banner")) {
            /*ViewGroup.LayoutParams params1 = holder.itemImage.getLayoutParams();
            params1.height = (int) pixels_height;
            params1.width = (int) pixels_height;
            holder.itemImage.requestLayout();*/
        base_url_thumb = "https://storage.googleapis.com/speedzy_data/resources/banner/small/";
        base_url_main = "https://storage.googleapis.com/speedzy_data/resources/banner/big/";
      } else {
        /*ViewGroup.LayoutParams params1 = holder.itemImage.getLayoutParams();
        params1.height = (int) pixels_height;
        params1.width = (int) pixels_height;
        holder.itemImage.requestLayout();*/
        base_url_thumb = Commons.image_baseURL_very_small;
        base_url_main = Commons.image_baseURL;
      }

      RequestBuilder<Drawable> thumbnailRequest = Glide
          .with(mContext.getApplicationContext())
          .load(base_url_thumb + items.getImagename())
              .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder));
      //.override(pixels_height, pixels_width);


      Glide
          .with(mContext.getApplicationContext())
          .load(base_url_main + items.getImagename())
          // .override(pixels_height, pixels_width)
         .thumbnail(thumbnailRequest)
             .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder))
             .into(holder.itemImage);
      if (flag.equalsIgnoreCase("banner")) {
        holder.buy.setVisibility(View.GONE);
        holder.about.setVisibility(View.GONE);
      } else {
        if (flag.equalsIgnoreCase("meal")) {
          holder.buy.setVisibility(View.VISIBLE);
          holder.about.setVisibility(View.GONE);
        }
        if (flag.equalsIgnoreCase("offer")) {
          holder.buy.setVisibility(View.GONE);
          holder.about.setVisibility(View.VISIBLE);
        }
      }
      holder.about.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Commons.dialogshow(mContext, Commons.image_baseURL + items.getImagename());
        }
      });

      holder.itemImage.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          FoodinnsMediaPlayer.getInstance().playSound("pin_drop_sound.mp3", mContext);
         Commons.clicking_event(mContext, items, alltimings, open_close_status);
        }
      });
      //} else if (patanjali_banner == 2) {
      //  final Patanjali_Productdetails_Model.banners banners_list = water_bannerlist.get(i);
      //  base_url_thumb = "https://storage.googleapis.com/speedzy_data/resources/banner/small/";
      //  base_url_main = "https://storage.googleapis.com/speedzy_data/resources/banner/big/";
      //  DrawableRequestBuilder<String> thumbnailRequest = Glide
      //      .with(mContext.getApplicationContext())
      //      .load(base_url_thumb + banners_list.getName());
      //  //.override(pixels_height, pixels_width);
      //  Glide
      //      .with(mContext.getApplicationContext())
      //      .load(base_url_main + banners_list.getName())
      //      // .override(pixels_height, pixels_width)
      //      .thumbnail(thumbnailRequest)
      //      .placeholder(R.drawable.combo_placeholder)
      //      .into(holder.itemImage);
      //  holder.buy.setVisibility(View.GONE);
      //  holder.about.setVisibility(View.GONE); }
      //else {
      //  final Patanjali_Category_model.Banners banners_list = banners.get(i);
      //  base_url_thumb = "https://storage.googleapis.com/speedzy_data/resources/banner/small/";
      //  base_url_main = "https://storage.googleapis.com/speedzy_data/resources/banner/big/";
      //  DrawableRequestBuilder<String> thumbnailRequest = Glide
      //      .with(mContext.getApplicationContext())
      //      .load(base_url_thumb + banners_list.getImageName());
      //  //.override(pixels_height, pixels_width);
      //  Glide
      //      .with(mContext.getApplicationContext())
      //      .load(base_url_main + banners_list.getImageName())
      //      // .override(pixels_height, pixels_width)
      //      .thumbnail(thumbnailRequest)
      //      .placeholder(R.drawable.combo_placeholder)
      //      .into(holder.itemImage);
      //  holder.buy.setVisibility(View.GONE);
      //  holder.about.setVisibility(View.GONE);
      //  holder.itemImage.setOnClickListener(new View.OnClickListener() {
      //    @Override
      //    public void onClick(View v) {
      //      List<Patanjali_Category_model.categories.sub_categories.child_subcat> child_subcat =
      //          new ArrayList<>();
      //      Intent intent = new Intent(mContext, PatanjaliProduct_DetailsActivity.class);
      //      intent.putExtra("app_tittle", "Grocery World");
      //      intent.putExtra("category_id", banners_list.getCategoryId());
      //      intent.putExtra("subcategory_id", banners_list.getSubCategoryId());
      //      intent.putExtra("child_subcat", (Serializable) child_subcat);
      //      mContext.startActivity(intent);
      //      mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      //    }
      //  });
      //}
    }
  }

  @Override
  public int getItemCount() {
    if (patanjali_banner == 0) {
      return itemsList.size();
    } else if (patanjali_banner == 2) {
      return water_bannerlist.size();
    }
    return itemsList.size();
  }

  public class SingleItemRowHolder extends RecyclerView.ViewHolder {

    ImageView itemImage, about;
    LinearLayout buy;
    RelativeLayout relative_layout;

    public SingleItemRowHolder(View view) {
      super(view);
      this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
      this.about = (ImageView) view.findViewById(R.id.about);
      this.buy = (LinearLayout) view.findViewById(R.id.buy);
      this.relative_layout = itemView.findViewById(R.id.relative_layout);

    /*  if (flag.equalsIgnoreCase("meal")) {
        ViewGroup.LayoutParams params1 = itemImage.getLayoutParams();
        params1.width = mContext.getResources().getDimensionPixelSize(R.dimen._150sdp);
        params1.height = mContext.getResources().getDimensionPixelSize(R.dimen._100sdp);
        itemImage.requestLayout();
      } else if (flag.equalsIgnoreCase("banner")) {
        ViewGroup.LayoutParams params1 = itemImage.getLayoutParams();
        params1.width = mContext.getResources().getDimensionPixelSize(R.dimen._215sdp);
        params1.height = mContext.getResources().getDimensionPixelSize(R.dimen._150sdp);
        params1.height = mContext.getResources().getDimensionPixelSize(R.dimen._150sdp);
        itemImage.requestLayout();
      }*/
    }
  }
}

