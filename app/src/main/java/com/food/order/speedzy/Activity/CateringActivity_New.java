package com.food.order.speedzy.Activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.food.order.speedzy.Adapter.BookTableAdapter;
import com.food.order.speedzy.Adapter.BooktableTimeListAdapter;
import com.food.order.speedzy.Adapter.EventAdapter;
import com.food.order.speedzy.Model.BookEventModelNew;
import com.food.order.speedzy.Model.DateModel;
import com.food.order.speedzy.Model.EventListModelNew;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.AllValidation;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Sujata Mohanty.
 */


public class CateringActivity_New extends AppCompatActivity {
    private Toolbar toolbar;
    RecyclerView date_recycler_view_list,time_recycler_view_list;
    BookTableAdapter bookTableAdapter;
    BooktableTimeListAdapter booktableTimeListAdapter;
    List<DateModel> datelist=new ArrayList<>();
    EditText name,email,phone,person_no,comment,occasion,budget;
    String date_sentto_web="";
    String time_sentto_web="";
    TextView get_quote;
    String name1,email1,phone1,person1,comment1,Veg_Flag,occasion1,budget1;
    MySharedPrefrencesData mySharedPrefrencesData;
    Dialog dialog;
    TextView warn,ok;
    String[] arrayspinner=new String[0];
    TextView text1;
    String event_id="";
    TextView veg,non_veg;
    int flg_for_veg=0;
    LoaderDiloag loaderDiloag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catering__new);

        event_id=getIntent().getStringExtra("event_id");
        loaderDiloag=new LoaderDiloag(CateringActivity_New.this);
        mySharedPrefrencesData=new MySharedPrefrencesData();
        Veg_Flag=mySharedPrefrencesData.getVeg_Flag(CateringActivity_New.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (Veg_Flag.equalsIgnoreCase("1")){
            statusbar_bg(R.color.red);
        }else {
            statusbar_bg(R.color.red);
        }
        dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.order_successful_popup);
        warn=(TextView)dialog.findViewById(R.id.warning);
        ok=(TextView)dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });


        name=(EditText) findViewById(R.id.name);
        email=(EditText)findViewById(R.id.email);
        phone=(EditText)findViewById(R.id.mobile);
        person_no=(EditText)findViewById(R.id.no_of_person);
        comment=(EditText) findViewById(R.id.comment);
        occasion=(EditText) findViewById(R.id.occasion);
        budget=(EditText) findViewById(R.id.budget);
        get_quote=(TextView) findViewById(R.id.get_quote);
        text1=(TextView) findViewById(R.id.text1);
        veg = (TextView) findViewById(R.id.veg);
        non_veg = (TextView) findViewById(R.id.non_veg);

        function(0);
        veg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flg_for_veg=0;
               function(0);
            }
        });
        non_veg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flg_for_veg=1;
                function(1);
            }
        });


        SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE");
        SimpleDateFormat sdf2 = new SimpleDateFormat("MMM dd");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < 4; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            String day = sdf1.format(calendar.getTime());
            String date = sdf2.format(calendar.getTime());
            String sent_to_api_date = inputFormat.format(calendar.getTime());
            DateModel dateModel=new DateModel(day,date,sent_to_api_date);
            datelist.add(dateModel);
        }

        ArrayList<String> picktime = new ArrayList<>();
        picktime.add("11:00");
        picktime.add("11:30");
        picktime.add("12:00");
        picktime.add("12:30");
        picktime.add("13:00");
        picktime.add("13:30");
        picktime.add("14:00");
        picktime.add("14:30");
        picktime.add("18:00");
        picktime.add("18:30");
        picktime.add("19:00");
        picktime.add("19:30");
        picktime.add("20:00");
        picktime.add("20:30");
        picktime.add("21:00");
        picktime.add("21:30");
        DateFormat dateFormatcuurenttime = new SimpleDateFormat("H:mm");
        String str = dateFormatcuurenttime.format(new Date());
        Date cuurenttime = null;
        try {
            cuurenttime = dateFormatcuurenttime.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArrayList<String> time_new_with_cuurenttime_pick = new ArrayList<>();
        for (int i = 0; i < picktime.size(); i++) {
            Date timecheck = null;
            try {
                timecheck = dateFormatcuurenttime.parse(picktime.get(i));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (timecheck.compareTo(cuurenttime) > 0) {
                time_new_with_cuurenttime_pick.add(dateFormatcuurenttime.format(timecheck));
            }
        }
        if (time_new_with_cuurenttime_pick.size() > 0) {
            time_recycler_view_list = (RecyclerView) findViewById(R.id.time_recycler_view_list);
            time_recycler_view_list.setHasFixedSize(true);
            time_recycler_view_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            time_recycler_view_list.setNestedScrollingEnabled(false);
            arrayspinner = time_new_with_cuurenttime_pick.toArray(new String[0]);
            if (arrayspinner.length>0) {
                text1.setVisibility(View.VISIBLE);
                time_recycler_view_list.setVisibility(View.VISIBLE);
                booktableTimeListAdapter = new BooktableTimeListAdapter(CateringActivity_New.this, arrayspinner);
                time_recycler_view_list.setAdapter(booktableTimeListAdapter);
                booktableTimeListAdapter.setOnItemClickListener(new BooktableTimeListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        time_sentto_web=arrayspinner[position];
                    }
                });
            }else {
                text1.setVisibility(View.GONE);
                time_recycler_view_list.setVisibility(View.GONE);
            }
        }
        date_recycler_view_list = (RecyclerView) findViewById(R.id.date_recycler_view_list);
        date_recycler_view_list.setHasFixedSize(true);
        date_recycler_view_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        date_recycler_view_list.setNestedScrollingEnabled(false);
        bookTableAdapter = new BookTableAdapter(CateringActivity_New.this, datelist);
        date_recycler_view_list.setAdapter(bookTableAdapter);

        bookTableAdapter.setOnItemClickListener(new BookTableAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, String return_date, int position) {
                date_sentto_web=return_date;
            }
        });

        get_quote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name1=name.getText().toString();
                email1=email.getText().toString();
                phone1=phone.getText().toString();
                person1=person_no.getText().toString();
                comment1=comment.getText().toString();
                occasion1=occasion.getText().toString();
                budget1=budget.getText().toString();
                if (date_sentto_web.equalsIgnoreCase("")) {
                    date_sentto_web = datelist.get(0).getSent_to_api_date();
                }
                if (time_sentto_web.equalsIgnoreCase("")) {
                    time_sentto_web = arrayspinner[0];
                }
                if(AllValidation.book_table_validate(name1,email1,phone1,person1,date_sentto_web,time_sentto_web,occasion1,budget1,CateringActivity_New.this)){
                    callApi();
                }
            }


        });

    }

    private void callApi() {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(CateringActivity_New.this)
                .create(Apimethods.class);
        Call<BookEventModelNew> call = methods.bookevent(name1,phone1,budget1,comment1,date_sentto_web,time_sentto_web,"1,2,3",occasion1,
                person1, String.valueOf(flg_for_veg),mySharedPrefrencesData.getUser_Id(CateringActivity_New.this),event_id);
        Log.d("url", "url=" + call.request().url().toString());
        Log.d("Latitude", Commons.latitude_str);
        Log.d("Longitude", Commons.longitude_str);
        loaderDiloag.displayDiloag();
        call.enqueue(new Callback<BookEventModelNew>() {
            @Override
            public void onResponse(Call<BookEventModelNew> call,
                                   retrofit2.Response<BookEventModelNew> response) {
                BookEventModelNew model = response.body();

                warn.setText("Your Booking has been successful.");
                dialog.show();
                loaderDiloag.dismissDiloag();
            }

            @Override
            public void onFailure(@NonNull Call<BookEventModelNew> call, @NonNull Throwable t) {
                t.printStackTrace();
                loaderDiloag.dismissDiloag();
            }
        });
    }

    private void function(int i) {
        if (flg_for_veg==0) {
            veg.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_checked, 0, 0, 0);
            non_veg.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_unchecked, 0, 0, 0);
        } else {
            non_veg.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_checked, 0, 0, 0);
            veg.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_unchecked, 0, 0, 0);
        }
    }

    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,color ));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Commons.back_button_transition(CateringActivity_New.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Commons.back_button_transition(CateringActivity_New.this);
    }
}
