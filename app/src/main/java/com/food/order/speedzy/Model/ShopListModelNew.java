package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by admin on 01-12-2018.
 */

public class ShopListModelNew implements Serializable {
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApp_openstatus() {
        return app_openstatus;
    }

    public void setApp_openstatus(String app_openstatus) {
        this.app_openstatus = app_openstatus;
    }

    public String getApp_message() {
        return app_message;
    }

    public void setApp_message(String app_message) {
        this.app_message = app_message;
    }

    public ArrayList<ShopListModelNew.Restaurants> getRestaurants() {
        return Restaurants;
    }

    public void setRestaurants(ArrayList<ShopListModelNew.Restaurants> restaurants) {
        Restaurants = restaurants;
    }

    private String status;
    private String app_openstatus;
    private String app_message;
    private ArrayList<Restaurants> Restaurants = null;

    public ShopListModelNew.Restaurant_timings getRestaurant_timings() {
        return Restaurant_timings;
    }

    public void setRestaurant_timings(ShopListModelNew.Restaurant_timings restaurant_timings) {
        Restaurant_timings = restaurant_timings;
    }

    private Restaurant_timings Restaurant_timings;

    public ShopListModelNew.open_close_status getOpen_close_status() {
        return open_close_status;
    }

    public void setOpen_close_status(ShopListModelNew.open_close_status open_close_status) {
        this.open_close_status = open_close_status;
    }

    private open_close_status open_close_status;
    public class Restaurants implements Serializable {
        public String getRestaurantLogoImage() {
            return restaurantLogoImage;
        }

        public void setRestaurantLogoImage(String restaurantLogoImage) {
            this.restaurantLogoImage = restaurantLogoImage;
        }

        public String getRestaurantCoverImage() {
            return restaurantCoverImage;
        }

        public void setRestaurantCoverImage(String restaurantCoverImage) {
            this.restaurantCoverImage = restaurantCoverImage;
        }

        public String getRestaurantMainImage() {
            return restaurantMainImage;
        }

        public void setRestaurantMainImage(String restaurantMainImage) {
            this.restaurantMainImage = restaurantMainImage;
        }

        public String getInRestaurantId() {
            return inRestaurantId;
        }

        public void setInRestaurantId(String inRestaurantId) {
            this.inRestaurantId = inRestaurantId;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getStRestaurantName() {
            return stRestaurantName;
        }

        public void setStRestaurantName(String stRestaurantName) {
            this.stRestaurantName = stRestaurantName;
        }

        public String getStSuburb() {
            return stSuburb;
        }

        public void setStSuburb(String stSuburb) {
            this.stSuburb = stSuburb;
        }

        public String getStStreetAddress() {
            return stStreetAddress;
        }

        public void setStStreetAddress(String stStreetAddress) {
            this.stStreetAddress = stStreetAddress;
        }

        public String getStMinOrder() {
            return stMinOrder;
        }

        public void setStMinOrder(String stMinOrder) {
            this.stMinOrder = stMinOrder;
        }

        public Object getStMinFreeOrder() {
            return stMinFreeOrder;
        }

        public void setStMinFreeOrder(Object stMinFreeOrder) {
            this.stMinFreeOrder = stMinFreeOrder;
        }

        public String getAvgRating() {
            return avgRating;
        }

        public void setAvgRating(String avgRating) {
            this.avgRating = avgRating;
        }

        public String getCountRating() {
            return countRating;
        }

        public void setCountRating(String countRating) {
            this.countRating = countRating;
        }

        public String getStreetAddress() {
            return streetAddress;
        }

        public void setStreetAddress(String streetAddress) {
            this.streetAddress = streetAddress;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getStGstCommission() {
            return stGstCommission;
        }

        public void setStGstCommission(String stGstCommission) {
            this.stGstCommission = stGstCommission;
        }

        public String getPromotion_msg() {
            return promotion_msg;
        }

        public void setPromotion_msg(String promotion_msg) {
            this.promotion_msg = promotion_msg;
        }

        @SerializedName("promotion_msg")
        @Expose
        public String promotion_msg;

        public String getFlg_ac_status() {
            return flg_ac_status;
        }

        public void setFlg_ac_status(String flg_ac_status) {
            this.flg_ac_status = flg_ac_status;
        }

        @SerializedName("flg_ac_status")
        @Expose
        public String flg_ac_status;

        @SerializedName("restaurant_logo_image")
            @Expose
            public String restaurantLogoImage;
            @SerializedName("restaurant_cover_image")
            @Expose
            public String restaurantCoverImage;
            @SerializedName("restaurant_main_image")
            @Expose
            public String restaurantMainImage;
            @SerializedName("in_restaurant_id")
            @Expose
            public String inRestaurantId;
            @SerializedName("distance")
            @Expose
            public String distance;
            @SerializedName("st_restaurant_name")
            @Expose
            public String stRestaurantName;
            @SerializedName("st_suburb")
            @Expose
            public String stSuburb;
            @SerializedName("st_street_address")
            @Expose
            public String stStreetAddress;
            @SerializedName("st_min_order")
            @Expose
            public String stMinOrder;
            @SerializedName("st_min_free_order")
            @Expose
            public Object stMinFreeOrder;
            @SerializedName("avg_rating")
            @Expose
            public String avgRating;
            @SerializedName("count_rating")
            @Expose
            public String countRating;
            @SerializedName("street_address")
            @Expose
            public String streetAddress;
            @SerializedName("flag")
            @Expose
            public String flag;
            @SerializedName("st_gst_commission")
            @Expose
            public String stGstCommission;

        public String getSt_latitude() {
            return st_latitude;
        }

        public void setSt_latitude(String st_latitude) {
            this.st_latitude = st_latitude;
        }

        public String getSt_longitude() {
            return st_longitude;
        }

        public void setSt_longitude(String st_longitude) {
            this.st_longitude = st_longitude;
        }

        @SerializedName("st_latitude")
        @Expose
        public String st_latitude;
        @SerializedName("st_longitude")
        @Expose
        public String st_longitude;
    }
    public class Restaurant_timings implements Serializable{
        private String lunch_start;
        private String lunch_end;

        public String getLunch_start() {
            return lunch_start;
        }

        public void setLunch_start(String lunch_start) {
            this.lunch_start = lunch_start;
        }

        public String getLunch_end() {
            return lunch_end;
        }

        public void setLunch_end(String lunch_end) {
            this.lunch_end = lunch_end;
        }

        public String getDinner_start() {
            return dinner_start;
        }

        public void setDinner_start(String dinner_start) {
            this.dinner_start = dinner_start;
        }

        public String getDinner_end() {
            return dinner_end;
        }

        public void setDinner_end(String dinner_end) {
            this.dinner_end = dinner_end;
        }

        public String getBreakfast_start() {
            return breakfast_start;
        }

        public void setBreakfast_start(String breakfast_start) {
            this.breakfast_start = breakfast_start;
        }

        public String getBreakfast_end() {
            return breakfast_end;
        }

        public void setBreakfast_end(String breakfast_end) {
            this.breakfast_end = breakfast_end;
        }

        public String getTiffin_start() {
            return tiffin_start;
        }

        public void setTiffin_start(String tiffin_start) {
            this.tiffin_start = tiffin_start;
        }

        public String getTiffin_end() {
            return tiffin_end;
        }

        public void setTiffin_end(String tiffin_end) {
            this.tiffin_end = tiffin_end;
        }

        public String getFastfood_start() {
            return fastfood_start;
        }

        public void setFastfood_start(String fastfood_start) {
            this.fastfood_start = fastfood_start;
        }

        public String getFastfood_end() {
            return fastfood_end;
        }

        public void setFastfood_end(String fastfood_end) {
            this.fastfood_end = fastfood_end;
        }

        public String getSweets_start() {
            return sweets_start;
        }

        public void setSweets_start(String sweets_start) {
            this.sweets_start = sweets_start;
        }

        public String getSweets_end() {
            return sweets_end;
        }

        public void setSweets_end(String sweets_end) {
            this.sweets_end = sweets_end;
        }

        public String getFruits_start() {
            return fruits_start;
        }

        public void setFruits_start(String fruits_start) {
            this.fruits_start = fruits_start;
        }

        public String getFruits_end() {
            return fruits_end;
        }

        public void setFruits_end(String fruits_end) {
            this.fruits_end = fruits_end;
        }

        private String dinner_start;
        private String dinner_end;
        private String breakfast_start;
        private String breakfast_end;
        private String tiffin_start;
        private String tiffin_end;
        private String fastfood_start;
        private String fastfood_end;
        private String sweets_start;
        private String sweets_end;
        private String fruits_start;
        private String fruits_end;
        private String bakery_start;

        public String getBakery_start() {
            return bakery_start;
        }

        public void setBakery_start(String bakery_start) {
            this.bakery_start = bakery_start;
        }

        public String getBakery_end() {
            return bakery_end;
        }

        public void setBakery_end(String bakery_end) {
            this.bakery_end = bakery_end;
        }

        private String bakery_end;
    }
    public class open_close_status implements Serializable{
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getOpenstatus() {
            return openstatus;
        }

        public void setOpenstatus(String openstatus) {
            this.openstatus = openstatus;
        }

        private String openstatus;
    }
}
