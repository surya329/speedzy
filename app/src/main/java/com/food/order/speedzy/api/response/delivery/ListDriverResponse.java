package com.food.order.speedzy.api.response.delivery;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ListDriverResponse{

	@SerializedName("data")
	private Data data;

	public Data getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ListDriverResponse{" + 
			"data = '" + data + '\'' + 
			"}";
		}
}