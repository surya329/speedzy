package com.food.order.speedzy.screen.banner;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.food.order.speedzy.R;
import java.util.ArrayList;
import java.util.List;

public class BannerAdapter extends RecyclerView.Adapter<BannerVH> {
  private final Callback mCallback;
  private List<BannersVM> mData;

  public BannerAdapter(Callback callback) {
    this.mData = new ArrayList<>();
    mCallback = callback;
  }

  @NonNull @Override public BannerVH onCreateViewHolder(@NonNull ViewGroup parent, int i) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.meal_combo_type_row, null, false);
    RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    view.setLayoutParams(lp);
    return new BannerVH(view, new BannerVH.Callback() {
      @Override public void onClickItem(int position) {
        mCallback.onClickItem(mData.get(position));
      }
    });
  }

  @Override public void onBindViewHolder(@NonNull BannerVH holder, int position) {
    Log.e("data", String.valueOf(mData.get(position)));
    holder.onBindView(mData.get(position));
  }

  @Override public int getItemCount() {
    return mData.size();
  }

  public void setData(List<BannersVM> data) {
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public void refreshData(List<BannersVM> data) {
    mData.clear();
    setData(data);
  }

  public interface Callback {
    void onClickItem(BannersVM BannersVM);
  }
}
