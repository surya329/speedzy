package com.food.order.speedzy.CitySpecial;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.Activity.PatanjaliProduct_DetailsActivity;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.reyclerviewutils.RecyclerViewUtils;

import java.util.List;
import java.util.Objects;

public class CitySpecialCategoryAdapter extends RecyclerView.Adapter<CitySpecialCategoryAdapter.GrocerySubCategoriesVHNew> {
    private List<CitySpecialCategoryModel.Category> mData;
    Activity contex;
    CitySpecialSubCategoriesAdapter adapter2;
    String start_time_lunch_list, end_time_lunch_list, start_time_dinner_list, end_time_dinner_list,
            app_openstatus;

    public CitySpecialCategoryAdapter(CitySpecialActivity mcontex, List<CitySpecialCategoryModel.Category> mData, String app_openstatus, String start_time_lunch_list, String end_time_lunch_list, String start_time_dinner_list, String end_time_dinner_list) {
        this.mData = mData;
        contex=mcontex;
        this.start_time_lunch_list=start_time_lunch_list;
        this.end_time_lunch_list=end_time_lunch_list;
        this.start_time_dinner_list=start_time_dinner_list;
        this.end_time_dinner_list=end_time_dinner_list;
                this.app_openstatus=app_openstatus;
    }

    @NonNull
    @Override
    public CitySpecialCategoryAdapter.GrocerySubCategoriesVHNew onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view =
                LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.city_item, viewGroup, false);
        return new CitySpecialCategoryAdapter.GrocerySubCategoriesVHNew(view,contex);
    }

    @Override
    public void onBindViewHolder(@NonNull CitySpecialCategoryAdapter.GrocerySubCategoriesVHNew holder, int i) {
        holder.my_recycler_view.setLayoutManager(RecyclerViewUtils.newGridLayoutManager(contex,3));
        holder.my_recycler_view.setNestedScrollingEnabled(false);
        holder.my_recycler_view.setVisibility(View.VISIBLE);
        holder.category.setText(mData.get(i).getName());
        adapter2 = new CitySpecialSubCategoriesAdapter(new CitySpecialSubCategoriesAdapter.Callback() {
            @Override public void onClickItem(CitySpecialCategoryModel.Category.SubCategory data) {
                goToDetailsPage(data,mData.get(i).getId());
            }
        });
        adapter2.refreshData(mData.get(i));
        holder.my_recycler_view.setAdapter(adapter2);
    }

    @Override public int getItemCount() {
        return mData.size();
    }

    public class GrocerySubCategoriesVHNew extends RecyclerView.ViewHolder {
        RecyclerView my_recycler_view;
        TextView category;

        public GrocerySubCategoriesVHNew(@NonNull View itemView, Activity contex) {
            super(itemView);
            my_recycler_view = itemView.findViewById(R.id.sub_cat_list);
            category = itemView.findViewById(R.id.category_name);
        }
    }
    private void goToDetailsPage(CitySpecialCategoryModel.Category.SubCategory data,String category_id) {
        Intent intent =
                new Intent(contex, PatanjaliProduct_DetailsActivity.class);
        intent.putExtra("app_tittle", "City Special");
        intent.putExtra("category_id", category_id);
        intent.putExtra("subcategory_id", data.getId());
        intent.putExtra("start_time_lunch",start_time_lunch_list);
        intent.putExtra("end_time_lunch", end_time_lunch_list);
        intent.putExtra("start_time_dinner",start_time_dinner_list);
        intent.putExtra("end_time_dinner",end_time_dinner_list);
        intent.putExtra("app_openstatus", app_openstatus);
        contex.startActivity(intent);
        contex.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
