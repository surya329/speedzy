package com.food.order.speedzy.Model;

import com.food.order.speedzy.api.response.DeliveryareaItem;
import com.food.order.speedzy.api.response.OrderItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class ViewOrderModel implements Serializable {
  //private String status;
  //private ArrayList<Order> order = null;
  //
  //public ArrayList<Order> getOrder() {
  //  return order;
  //}
  //
  //public void setOrder(ArrayList<Order> order) {
  //  this.order = order;
  //}
  //
  //public String getStatus() {
  //  return status;
  //}
  //
  //public void setStatus(String status) {
  //  this.status = status;
  //}
  //
  //public class Order implements Serializable {
  //  private String restaurant_name;
  //  private String restaurant_id;
  //  private String image;
  //  private String orderid;
  //  private String suburb_id;
  //  private String order_amount;
  //  private String delivery_charge;
  //  private String total_amount;
  //  private String fname;
  //  private String lname;
  //  private String companyname;
  //  private String mobile;
  //  private String email;
  //  private String address;
  //  private String streetnumber;
  //  private String nearest_cross_city;
  //  private String wallet;
  //  private String delivery_time;
  //  private String transaction_id;
  //  private String voucher_id;
  //  private String st_voucher_amount;
  //  private String method_of_ordering;
  //  private String flg_order_type;
  //  private String st_gst_commission;
  //  private String flg_order_status;
  //
  //  public String getPayment_method() {
  //    return payment_method;
  //  }
  //
  //  public void setPayment_method(String payment_method) {
  //    this.payment_method = payment_method;
  //  }
  //
  //  public String getPre_order_date() {
  //    return pre_order_date;
  //  }
  //
  //  public void setPre_order_date(String pre_order_date) {
  //    this.pre_order_date = pre_order_date;
  //  }
  //
  //  private String payment_method;
  //  private String pre_order_date;
  //
  //  public order_runningstatus getOrder_runningstatus() {
  //    return order_runningstatus;
  //  }
  //
  //  public void setOrder_runningstatus(order_runningstatus order_runningstatus) {
  //    this.order_runningstatus = order_runningstatus;
  //  }
  //
  //  private order_runningstatus order_runningstatus;
  //  private ArrayList<Order_detail> order_detail = null;
  //
  //  public String getRestaurant_name() {
  //    return restaurant_name;
  //  }
  //
  //  public void setRestaurant_name(String restaurant_name) {
  //    this.restaurant_name = restaurant_name;
  //  }
  //
  //  public String getRestaurant_id() {
  //    return restaurant_id;
  //  }
  //
  //  public void setRestaurant_id(String restaurant_id) {
  //    this.restaurant_id = restaurant_id;
  //  }
  //
  //  public String getImage() {
  //    return image;
  //  }
  //
  //  public void setImage(String image) {
  //    this.image = image;
  //  }
  //
  //  public String getOrderid() {
  //    return orderid;
  //  }
  //
  //  public void setOrderid(String orderid) {
  //    this.orderid = orderid;
  //  }
  //
  //  public String getSuburb_id() {
  //    return suburb_id;
  //  }
  //
  //  public void setSuburb_id(String suburb_id) {
  //    this.suburb_id = suburb_id;
  //  }
  //
  //  public String getOrder_amount() {
  //    return order_amount;
  //  }
  //
  //  public void setOrder_amount(String order_amount) {
  //    this.order_amount = order_amount;
  //  }
  //
  //  public String getDelivery_charge() {
  //    return delivery_charge;
  //  }
  //
  //  public void setDelivery_charge(String delivery_charge) {
  //    this.delivery_charge = delivery_charge;
  //  }
  //
  //  public String getTotal_amount() {
  //    return total_amount;
  //  }
  //
  //  public void setTotal_amount(String total_amount) {
  //    this.total_amount = total_amount;
  //  }
  //
  //  public String getFname() {
  //    return fname;
  //  }
  //
  //  public void setFname(String fname) {
  //    this.fname = fname;
  //  }
  //
  //  public String getLname() {
  //    return lname;
  //  }
  //
  //  public void setLname(String lname) {
  //    this.lname = lname;
  //  }
  //
  //  public String getCompanyname() {
  //    return companyname;
  //  }
  //
  //  public void setCompanyname(String companyname) {
  //    this.companyname = companyname;
  //  }
  //
  //  public String getMobile() {
  //    return mobile;
  //  }
  //
  //  public void setMobile(String mobile) {
  //    this.mobile = mobile;
  //  }
  //
  //  public String getEmail() {
  //    return email;
  //  }
  //
  //  public void setEmail(String email) {
  //    this.email = email;
  //  }
  //
  //  public String getAddress() {
  //    return address;
  //  }
  //
  //  public void setAddress(String address) {
  //    this.address = address;
  //  }
  //
  //  public String getStreetnumber() {
  //    return streetnumber;
  //  }
  //
  //  public void setStreetnumber(String streetnumber) {
  //    this.streetnumber = streetnumber;
  //  }
  //
  //  public String getNearest_cross_city() {
  //    return nearest_cross_city;
  //  }
  //
  //  public void setNearest_cross_city(String nearest_cross_city) {
  //    this.nearest_cross_city = nearest_cross_city;
  //  }
  //
  //  public String getWallet() {
  //    return wallet;
  //  }
  //
  //  public void setWallet(String wallet) {
  //    this.wallet = wallet;
  //  }
  //
  //  public String getDelivery_time() {
  //    return delivery_time;
  //  }
  //
  //  public void setDelivery_time(String delivery_time) {
  //    this.delivery_time = delivery_time;
  //  }
  //
  //  public String getTransaction_id() {
  //    return transaction_id;
  //  }
  //
  //  public void setTransaction_id(String transaction_id) {
  //    this.transaction_id = transaction_id;
  //  }
  //
  //  public String getVoucher_id() {
  //    return voucher_id;
  //  }
  //
  //  public void setVoucher_id(String voucher_id) {
  //    this.voucher_id = voucher_id;
  //  }
  //
  //  public String getSt_voucher_amount() {
  //    return st_voucher_amount;
  //  }
  //
  //  public void setSt_voucher_amount(String st_voucher_amount) {
  //    this.st_voucher_amount = st_voucher_amount;
  //  }
  //
  //  public String getMethod_of_ordering() {
  //    return method_of_ordering;
  //  }
  //
  //  public void setMethod_of_ordering(String method_of_ordering) {
  //    this.method_of_ordering = method_of_ordering;
  //  }
  //
  //  public String getFlg_order_type() {
  //    return flg_order_type;
  //  }
  //
  //  public void setFlg_order_type(String flg_order_type) {
  //    this.flg_order_type = flg_order_type;
  //  }
  //
  //  public ArrayList<Order_detail> getOrder_detail() {
  //    return order_detail;
  //  }
  //
  //  public void setOrder_detail(ArrayList<Order_detail> order_detail) {
  //    this.order_detail = order_detail;
  //  }
  //
  //  public String getSt_gst_commission() {
  //    return st_gst_commission;
  //  }
  //
  //  public void setSt_gst_commission(String st_gst_commission) {
  //    this.st_gst_commission = st_gst_commission;
  //  }
  //
  //  public String getFlg_order_status() {
  //    return flg_order_status;
  //  }
  //
  //  public void setFlg_order_status(String flg_order_status) {
  //    this.flg_order_status = flg_order_status;
  //  }
  //
  //  public class order_runningstatus implements Serializable {
  //    private String status_title;
  //    private String status_description;
  //
  //    public String getStatus_title() {
  //      return status_title;
  //    }
  //
  //    public void setStatus_title(String status_title) {
  //      this.status_title = status_title;
  //    }
  //
  //    public String getStatus_description() {
  //      return status_description;
  //    }
  //
  //    public void setStatus_description(String status_description) {
  //      this.status_description = status_description;
  //    }
  //
  //    public String getDelivery_person_name() {
  //      return delivery_person_name;
  //    }
  //
  //    public void setDelivery_person_name(String delivery_person_name) {
  //      this.delivery_person_name = delivery_person_name;
  //    }
  //
  //    public String getDelivery_person_number() {
  //      return delivery_person_number;
  //    }
  //
  //    public void setDelivery_person_number(String delivery_person_number) {
  //      this.delivery_person_number = delivery_person_number;
  //    }
  //
  //    private String delivery_person_name;
  //    private String delivery_person_number;
  //  }
  //
  //  public class Order_detail implements Serializable {
  //
  //    private String dishid;
  //    private String qty;
  //    private String price;
  //    private String dishname;
  //    private String subtotal;
  //    private Options options;
  //
  //    public String getQty() {
  //      return qty;
  //    }
  //
  //    public void setQty(String qty) {
  //      this.qty = qty;
  //    }
  //
  //    public String getDishid() {
  //      return dishid;
  //    }
  //
  //    public void setDishid(String dishid) {
  //      this.dishid = dishid;
  //    }
  //
  //    public String getPrice() {
  //      return price;
  //    }
  //
  //    public void setPrice(String price) {
  //      this.price = price;
  //    }
  //
  //    public String getDishname() {
  //      return dishname;
  //    }
  //
  //    public void setDishname(String dishname) {
  //      this.dishname = dishname;
  //    }
  //
  //    public String getSubtotal() {
  //      return subtotal;
  //    }
  //
  //    public void setSubtotal(String subtotal) {
  //      this.subtotal = subtotal;
  //    }
  //
  //    public Options getOptions() {
  //      return options;
  //    }
  //
  //    public void setOptions(Options options) {
  //      this.options = options;
  //    }
  //
  //
  //
  //    public class Options implements Serializable {
  //
  //      @SerializedName("Unit")
  //      @Expose
  //      private String unit;
  //      @SerializedName("Actual Price")
  //      @Expose
  //      private String actualPrice;
  //      @SerializedName("Extra Item")
  //      @Expose
  //      private String extraItem;
  //      @SerializedName("Extra Price")
  //      @Expose
  //      private String extraPrice;
  //      @SerializedName("Type")
  //      @Expose
  //      private String type;
  //
  //      public String getUnit() {
  //        return unit;
  //      }
  //
  //      public void setUnit(String unit) {
  //        this.unit = unit;
  //      }
  //
  //      public String getActualPrice() {
  //        return actualPrice;
  //      }
  //
  //      public void setActualPrice(String actualPrice) {
  //        this.actualPrice = actualPrice;
  //      }
  //
  //      public String getExtraItem() {
  //        return extraItem;
  //      }
  //
  //      public void setExtraItem(String extraItem) {
  //        this.extraItem = extraItem;
  //      }
  //
  //      public String getExtraPrice() {
  //        return extraPrice;
  //      }
  //
  //      public void setExtraPrice(String extraPrice) {
  //        this.extraPrice = extraPrice;
  //      }
  //
  //      public String getType() {
  //        return type;
  //      }
  //
  //      public void setType(String type) {
  //        this.type = type;
  //      }
  //
  //      @Override public String toString() {
  //        return "Options{" +
  //            "unit='" + unit + '\'' +
  //            ", actualPrice='" + actualPrice + '\'' +
  //            ", extraItem='" + extraItem + '\'' +
  //            ", extraPrice='" + extraPrice + '\'' +
  //            ", type='" + type + '\'' +
  //            '}';
  //      }
  //    }
  //
  //    @Override public String toString() {
  //      return "Order_detail{" +
  //          "dishid='" + dishid + '\'' +
  //          ", qty='" + qty + '\'' +
  //          ", price='" + price + '\'' +
  //          ", dishname='" + dishname + '\'' +
  //          ", subtotal='" + subtotal + '\'' +
  //          ", options=" + options +
  //          '}';
  //    }
  //  }
  //
  //}
  //
  //@Override public String toString() {
  //  return "ViewOrderModel{" +
  //      "status='" + status + '\'' +
  //      ", order=" + order +
  //      '}';
  //}

  @SerializedName("msg")
  private String msg;

  @SerializedName("deliveryarea")
  private List<DeliveryareaItem> deliveryarea;

  @SerializedName("status")
  private String status;

  @SerializedName("order")
  private List<OrderItem> order;

  public void setMsg(String msg){
    this.msg = msg;
  }

  public String getMsg(){
    return msg;
  }

  public void setDeliveryarea(List<DeliveryareaItem> deliveryarea){
    this.deliveryarea = deliveryarea;
  }

  public List<DeliveryareaItem> getDeliveryarea(){
    return deliveryarea;
  }

  public void setStatus(String status){
    this.status = status;
  }

  public String getStatus(){
    return status;
  }

  public void setOrder(List<OrderItem> order){
    this.order = order;
  }

  public List<OrderItem> getOrder(){
    return order;
  }

  @Override
  public String toString(){
    return
        "OrderDetailsResponse{" +
            "msg = '" + msg + '\'' +
            ",deliveryarea = '" + deliveryarea + '\'' +
            ",status = '" + status + '\'' +
            ",order = '" + order + '\'' +
            "}";
  }
}
