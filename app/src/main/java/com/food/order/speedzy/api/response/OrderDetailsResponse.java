package com.food.order.speedzy.api.response;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OrderDetailsResponse implements Serializable {

	@SerializedName("msg")
	private String msg;

	@SerializedName("deliveryarea")
	private List<DeliveryareaItem> deliveryarea;

	@SerializedName("status")
	private String status;

	@SerializedName("order")
	private List<OrderItem> order;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setDeliveryarea(List<DeliveryareaItem> deliveryarea){
		this.deliveryarea = deliveryarea;
	}

	public List<DeliveryareaItem> getDeliveryarea(){
		return deliveryarea;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setOrder(List<OrderItem> order){
		this.order = order;
	}

	public List<OrderItem> getOrder(){
		return order;
	}

	@Override
 	public String toString(){
		return 
			"OrderDetailsResponse{" + 
			"msg = '" + msg + '\'' + 
			",deliveryarea = '" + deliveryarea + '\'' + 
			",status = '" + status + '\'' + 
			",order = '" + order + '\'' + 
			"}";
		}
}