package com.food.order.speedzy.Activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.IntentCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Adapter.MyCartSectionAdapter_New;
import com.food.order.speedzy.Adapter.Review_Adapter;
import com.food.order.speedzy.Adapter.SpecialOfferAdapter;
import com.food.order.speedzy.Adapter.TimeListAdapter;
import com.food.order.speedzy.Model.Cart_Model;
import com.food.order.speedzy.Model.Delivery_Charge_Response_Model;
import com.food.order.speedzy.Model.Preferencemodel;
import com.food.order.speedzy.Model.Restaurant_Dish_Model;
import com.food.order.speedzy.Model.Restaurant_model;
import com.food.order.speedzy.Model.StorepackagerModel;
import com.food.order.speedzy.Model.View_Rating_model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.Utils.reyclerviewutils.RecyclerViewUtils;
import com.food.order.speedzy.api.response.coupon.CouponItem;
import com.food.order.speedzy.api.response.coupon.CouponsResponse;
import com.food.order.speedzy.api.response.home.HomePageAppConfigResponse;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.root.BaseActivity;
import com.food.order.speedzy.screen.coupon.CouponAdapter;
import com.food.order.speedzy.screen.home.SpeedzyOperationsClosedModel;
import com.food.order.speedzy.screen.operationsclosed.OperationsClosedActivity;
import com.food.order.speedzy.screen.ordersuccess.ActivityOrderDataModel;
import com.food.order.speedzy.screen.ordersuccess.ActivityOrderSuccess;
import com.food.order.speedzy.screen.orderupi.OrderUPIDataModel;
import com.michael.easydialog.EasyDialog;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */

public class CheckoutActivity_New extends BaseActivity {
  LinearLayout delivery, pickup;
  View delivery_view, pickup_view;
  private Toolbar toolbar;
  MySharedPrefrencesData mySharedPrefrencesData;
  Apimethods methods;
  String userid = "", Veg_Flag;
  LinearLayout nocart, cartlay;
  TextView checkout;
  TextView additem;
  LOcaldbNew lOcaldbNew;
  Context mc;
  String cake_flag = "0";
  ArrayList<Cart_Model.Cart_Details> cd = new ArrayList<>();
  RecyclerView recyclerView;
  LinearLayoutManager lm;
  MyCartSectionAdapter_New myCartAdapter;
  double totalsum = 0.00, Final_totalsum = 0.00;
  TextView subtotaltxt, deliverychargetxt, grandtotal, gstamount, wallet_text, discount_text;
  LinearLayout gstlayout, discount_lay;
  RelativeLayout delivery_frag, pickup_frag;
  TextView asap, later;
  Spinner later_time;
  int mYear, mMonth, mDay;
  String avg_order_value;
  String avadadate;
  String ordernum;
  ArrayList<Restaurant_Dish_Model.NewOffers> offerArrayList = new ArrayList<>();
  ArrayList<Delivery_Charge_Response_Model.Offer> offerArrayList_patanjali = new ArrayList<>();
  RecyclerView time_recyclerview;
  TimeListAdapter timeListAdapter;
  String ordertype = "";
  String[] arrayspinner = new String[0];
  Date starttime_lunch, endtime_lunch, starttime_dinner, endtime_dinner, newtime;
  boolean istimecycle = true;
  LinearLayout linear_pick_del;
  ArrayList<Restaurant_Dish_Model.NewOffers> selectedofferlist = new ArrayList<>();
  ArrayList<Delivery_Charge_Response_Model.Offer> selectedofferlist_patanjali = new ArrayList<>();
  TextView selected_location, change,add_address, res_address, res_name;
  ImageView res_logo;
  String delivery_charge = "";
  TextView specialoffertextheader;
  RecyclerView specialofferslistview;
  SpecialOfferAdapter specialOfferAdapter;
  Dialog dialog_msg;
  TextView msg, no, ok;
  double MRPsum = 0.00;
  String suburbid = "1234";
  String start_time_lunch_list, end_time_lunch_list, start_time_dinner_list, end_time_dinner_list,
      app_openstatus;
  String time_selected = "1";
  ArrayList<String> time_new_with_cuurenttime = new ArrayList<>();
  Calendar c = Calendar.getInstance();
  DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
  DateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd");
  String tommorrow_del = "0";
  ArrayList<String> deltime = new ArrayList<>();
  TextView add_coupon_txt, apply;
  String coupon_flag="",coupon_msg="";
  String coupon_id = "voucherid", coupon_amount = "vaoucheramount", discount_type = "",
      discount_value = "0", max_amount = "0", min_order_value = "0";
  double coupon_discount_amount = 0.00, max_coupon_discount = 0.00;
  List<CouponItem.discount_slabs> discount_slabs = new ArrayList<>();
  boolean add_coupon_flag = false;
  String coupon_code = "voucherid";
  LinearLayout time_layout, coupon_dis_lay;
  TextView coupon_dis_text;
  static String city_id = "";
  private Disposable mDisposable;
  //static HomeActivityNew slideroot_nevigation_activity;
  private SpeedzyOperationsClosedModel mSpeedzyOperationsClosedModel;
  OrderUPIDataModel mOrderUPIDataModel;
  double discountvalue = 0.00;
  private List<CouponItem> mCouponItems;
  ImageView res_charge_tooltip;
  TextView restaurant__charges;
  LinearLayout restaurant__charges_lay;
  double rest_charge_amount = 0.0;
  String lati,longi;
  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_checkout__new);
    mySharedPrefrencesData = new MySharedPrefrencesData();
    Log.e("sname",mySharedPrefrencesData.getSelectName(this)+" "
            +mySharedPrefrencesData.getLName(this)+" "
            +mySharedPrefrencesData.get_Party_mobile(this));
    methods =
        API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    mCouponItems = new ArrayList<>();
    Veg_Flag = mySharedPrefrencesData.getVeg_Flag(CheckoutActivity_New.this);
    userid = mySharedPrefrencesData.getUser_Id(this);
    mc = CheckoutActivity_New.this;
    lOcaldbNew = new LOcaldbNew(mc);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    if (Veg_Flag.equalsIgnoreCase("1")) {
      statusbar_bg(R.color.red);
    } else {
      statusbar_bg(R.color.red);
    }

    dialog_msg =
        new Dialog(CheckoutActivity_New.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    dialog_msg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog_msg.setContentView(R.layout.logout_popup);
    msg = (TextView) dialog_msg.findViewById(R.id.warning);
    ok = (TextView) dialog_msg.findViewById(R.id.yes);
    no = (TextView) dialog_msg.findViewById(R.id.no);
    ok.setText("Ok");
    no.setVisibility(View.GONE);
    ok.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (tommorrow_del.equalsIgnoreCase("1")) {
          dialogshow_time(deltime);
        }
        dialog_msg.dismiss();
      }
    });
    if (Commons.flag_for_hta != null && Commons.flag_for_hta.equals("Patanjali")) {
      cd = lOcaldbNew.getCart_Details_Ptanjali();
    } else if (Commons.flag_for_hta != null && Commons.flag_for_hta.equals("City Special")) {
      cd = lOcaldbNew.getCart_Details_CitySpecial();
    } else if (Commons.flag_for_hta != null && Commons.flag_for_hta.equals("Drinking Water")) {
      cd = lOcaldbNew.getCart_Details_Water();
    } else if (Commons.flag_for_hta != null && Commons.flag_for_hta.equals(
        SpeedzyConstants.Fruits)) {
      cd = lOcaldbNew.getFruitCart_Details();
    } else {
      cd = lOcaldbNew.getCart_Details(/*Commons.restaurant_id*/);
      if (cd != null) {
        for (int f = 0; f < cd.size(); f++) {
          if (!GeneralUtil.isStringEmpty(cd.get(f).getCake_flg()) &&
              cd.get(f).getCake_flg().equalsIgnoreCase("1")) {
            cake_flag = "1";
          }
        }
      }
    }
    ordernum = mySharedPrefrencesData.getordernum(this);
    avg_order_value = mySharedPrefrencesData.getavg_order_value(this);
    offerArrayList = mySharedPrefrencesData.get_cart_offerArrayList(this);
    if (Commons.flag_for_hta != null && Commons.flag_for_hta.equals("Patanjali")) {
      app_openstatus = "0";
    } else {
      app_openstatus = getIntent().getStringExtra("app_openstatus");
    }
    start_time_lunch_list = getIntent().getStringExtra("start_time_lunch");
    end_time_lunch_list = getIntent().getStringExtra("end_time_lunch");
    start_time_dinner_list = getIntent().getStringExtra("start_time_dinner");
    end_time_dinner_list = getIntent().getStringExtra("end_time_dinner");
    lati=getIntent().getStringExtra("lati");
    longi=getIntent().getStringExtra("longi");

    cartlay = (LinearLayout) findViewById(R.id.cart_lay);
    nocart = (LinearLayout) findViewById(R.id.nocart);
    checkout = findViewById(R.id.checkout);
    additem = (TextView) findViewById(R.id.additem);
    linear_pick_del = (LinearLayout) findViewById(R.id.linear_pick_del);

    selected_location = (TextView) findViewById(R.id.selected_location);
    change = (TextView) findViewById(R.id.change);
    add_address=findViewById(R.id.add_address);
    res_address = (TextView) findViewById(R.id.res_address);
    res_name = (TextView) findViewById(R.id.res_name);
    res_logo=findViewById(R.id.res_logo);

    res_charge_tooltip = findViewById(R.id.res_charge_tooltip);
    restaurant__charges = findViewById(R.id.restaurant__charges);
    restaurant__charges_lay = findViewById(R.id.restaurant__charges_lay);
    subtotaltxt = (TextView) findViewById(R.id.subtotal);
    grandtotal = (TextView) findViewById(R.id.grandtotal);
    discount_text = (TextView) findViewById(R.id.discount_text);
    discount_lay = findViewById(R.id.discount_lay);
    wallet_text = (TextView) findViewById(R.id.wallet_text);
    deliverychargetxt = (TextView) findViewById(R.id.deliverycharge_cart);
    gstlayout = (LinearLayout) findViewById(R.id.gst_lay);
    gstamount = (TextView) findViewById(R.id.gst_text);
    coupon_dis_text = findViewById(R.id.coupon_dis_text);
    coupon_dis_lay = findViewById(R.id.coupon_dis_lay);

    delivery_frag = (RelativeLayout) findViewById(R.id.delivery_frag);
    pickup_frag = (RelativeLayout) findViewById(R.id.pickup_frag);
    asap = (TextView) findViewById(R.id.asap);
    later = (TextView) findViewById(R.id.later);
    later_time = (Spinner) findViewById(R.id.later_time);
    time_layout = (LinearLayout) findViewById(R.id.time_layout);
    delivery = (LinearLayout) findViewById(R.id.delivery);
    pickup = (LinearLayout) findViewById(R.id.pickup);
    delivery_view = (View) findViewById(R.id.delivery_view);
    pickup_view = (View) findViewById(R.id.pickup_view);
    add_coupon_txt = (TextView) findViewById(R.id.add_coupon_txt);
    apply = (TextView) findViewById(R.id.apply);

    specialoffertextheader = (TextView) findViewById(R.id.specialofferid);
    specialofferslistview = (RecyclerView) findViewById(R.id.special_offers_recyclerview);
    specialofferslistview.setLayoutManager(new LinearLayoutManager(this));
    specialOfferAdapter = new SpecialOfferAdapter(this, selectedofferlist_patanjali);
    specialofferslistview.setAdapter(specialOfferAdapter);
    if (Commons.flag_for_hta == "Grocerry") {
      specialoffertextheader.setVisibility(View.GONE);
      specialofferslistview.setVisibility(View.GONE);
    }
    if (Commons.flag_for_hta == "Patanjali") {
      time_layout.setVisibility(View.GONE);
      specialoffertextheader.setVisibility(View.GONE);
      specialofferslistview.setVisibility(View.GONE);
    }

    recyclerView = (RecyclerView) findViewById(R.id.cartlistItems);
    recyclerView.setHasFixedSize(true);
    lm = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(lm);

    pickup_view.setVisibility(View.GONE);
    delivery_view.setVisibility(View.VISIBLE);
    delivery_frag.setVisibility(View.VISIBLE);
    pickup_frag.setVisibility(View.GONE);
    ordertype = "1";

    if (mySharedPrefrencesData.getLocation(this) == null
            || mySharedPrefrencesData.getLocation(this).equalsIgnoreCase("")) {
      delivery_frag.setVisibility(View.GONE);
      add_address.setVisibility(View.VISIBLE);
    }else {
      delivery_frag.setVisibility(View.VISIBLE);
      add_address.setVisibility(View.GONE);
      selected_location.setText(mySharedPrefrencesData.getLocation(this));
    }
    change.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        address_change_intent();
      }
    });
    add_address.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        address_change_intent();
      }
    });

    delivery.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ordertype = "1";
        pickup_view.setVisibility(View.GONE);
        delivery_view.setVisibility(View.VISIBLE);
        delivery_frag.setVisibility(View.VISIBLE);
        pickup_frag.setVisibility(View.GONE);
        if (Commons.flag_for_hta == "Patanjali" || Commons.flag_for_hta == "City Special"
            || Commons.flag_for_hta == "Drinking Water") {
          setPriceDetailsPatanjali(cd);
        } else {
          setPriceDetails1(cd);
        }
      }
    });
    Commons.order_type = ordertype;
    pickup.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ordertype = "2";
        delivery_view.setVisibility(View.GONE);
        pickup_view.setVisibility(View.VISIBLE);
        delivery_frag.setVisibility(View.GONE);
        pickup_frag.setVisibility(View.VISIBLE);
        if (Commons.flag_for_hta == "Patanjali" || Commons.flag_for_hta == "City Special"
            || Commons.flag_for_hta == "Drinking Water") {
          setPriceDetailsPatanjali(cd);
        } else {
          setPriceDetails1(cd);
        }
      }
    });

    Commons.order_date_from_cart = dateFormatter.format(c.getTime());
    Commons.Preorder_date_from_cart = "";
    Commons.Preorder_date_from_cart = dateFormatter1.format(c.getTime());
    try {
      setimagefortdto(0);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    asap.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (app_openstatus.equalsIgnoreCase("1")) {
          time_selected = "1";
          time_slect_fun();
        } else if (mSpeedzyOperationsClosedModel != null
            && mSpeedzyOperationsClosedModel.getAppOpenStatus()
            == SpeedzyConstants.AppOpenStatus.CLOSED) {
          startActivity(OperationsClosedActivity.newIntent(CheckoutActivity_New.this,
              mSpeedzyOperationsClosedModel));
        }
        //callapi();
      }
    });
    later.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        time_selected = "2";
        time_slect_fun();
        if (Commons.flag_for_hta != "Patanjali" || Commons.flag_for_hta != "City Special"
            || Commons.flag_for_hta != "Drinking Water") {
          if (tommorrow_del.equalsIgnoreCase("0")) {
            dialogshow_time(time_new_with_cuurenttime);
          } else {
            dialogshow_time(deltime);
          }
        }
        if (Commons.flag_for_hta.equals("Drinking Water")) {
          datelist();
        }
      }
    });
    additem.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
       onBackPressed();
      }
    });
    apply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (!apply.getText().toString().equalsIgnoreCase("Remove")) {
          if (Commons.flag_for_hta ==SpeedzyConstants.Fruits ||
              Commons.flag_for_hta == "Patanjali" ||
                  Commons.flag_for_hta == "City Special"
                  || Commons.flag_for_hta == "Drinking Water") {
            if (mCouponItems != null && mCouponItems.size() > 0) {
              add_coupon_dialog(mCouponItems);
            } else if (!GeneralUtil.isStringEmpty(Commons.menu_id)) {
              getCouponList(Commons.menu_id);
            }
          }  else {
            if (mCouponItems != null && mCouponItems.size() > 0) {
              add_coupon_dialog(mCouponItems);
            } else if (!GeneralUtil.isStringEmpty(lOcaldbNew.getRestaurantIdFromCart())) {
              getCouponList(lOcaldbNew.getRestaurantIdFromCart());
            } else if (!GeneralUtil.isStringEmpty(Commons.restaurant_id)) {
              getCouponList(Commons.restaurant_id);
            }
          }
        } else {
          add_coupon_txt.setText("Apply Coupon ");
          apply.setText("Apply");
          if (Commons.flag_for_hta == "Patanjali" || Commons.flag_for_hta == "City Special"
              || Commons.flag_for_hta == "Drinking Water") {
            add_coupon_flag = false;
            setPriceDetailsPatanjali(cd);
          } else if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID)) {
            add_coupon_flag = false;
            setPriceDetails1(cd);
          } else {
            add_coupon_flag = false;
            setPriceDetails1(cd);
          }
        }
      }
    });

    if (Commons.flag_for_hta.equals("Patanjali") || Commons.flag_for_hta.equals("City Special")
        || Commons.flag_for_hta.equals("Drinking Water")) {
      //call_getcart_patanjali();
      linear_pick_del.setVisibility(View.GONE);
      if (cd.size() > 0) {
        checkout.setVisibility(View.VISIBLE);
        cartlay.setVisibility(View.VISIBLE);
        nocart.setVisibility(View.GONE);
      } else {
        cartlay.setVisibility(View.GONE);
        nocart.setVisibility(View.VISIBLE);
        checkout.setVisibility(View.GONE);
      }
      myCartAdapter = new MyCartSectionAdapter_New(CheckoutActivity_New.this, cd);
      recyclerView.setAdapter(myCartAdapter);
      myCartAdapter.shouldShowHeadersForEmptySections(true);
      myCartAdapter.notifyItemInserted(0);
      myCartAdapter.notifyDataSetChanged();
      setPriceDetailsPatanjali(cd);
      checkout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (mSpeedzyOperationsClosedModel != null
              && mSpeedzyOperationsClosedModel.getAppOpenStatus()
              == SpeedzyConstants.AppOpenStatus.CLOSED
              && mSpeedzyOperationsClosedModel.getAppClosedType()
              != SpeedzyConstants.AppCloseStatus.RESTURANT) {
            startActivity(OperationsClosedActivity.newIntent(CheckoutActivity_New.this,
                mSpeedzyOperationsClosedModel));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
          } else if (Commons.subtotal_amount != null) {
            amountcheck();
          }
        }
      });
    } else if (Commons.flag_for_hta.equals("Grocerry")) {
      //call_getcart_api1();
    } else {
      if (cd.size() > 0) {
        checkout.setVisibility(View.VISIBLE);
        cartlay.setVisibility(View.VISIBLE);
        nocart.setVisibility(View.GONE);
        Glide
                .with(this)
                .load( cd.get(0).getSt_restaurant_image())
                .apply(RequestOptions.bitmapTransform(new RoundedCorners( 30)).placeholder(R.drawable.combo_placeholder).diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(res_logo);
        res_name.setText(cd.get(0).getSt_restaurant_name());
        res_address.setText(cd.get(0).getSt_street_address());
      } else {
        cartlay.setVisibility(View.GONE);
        nocart.setVisibility(View.VISIBLE);
        checkout.setVisibility(View.GONE);
      }
      myCartAdapter = new MyCartSectionAdapter_New(CheckoutActivity_New.this, cd);
      recyclerView.setAdapter(myCartAdapter);
      myCartAdapter.shouldShowHeadersForEmptySections(true);
      myCartAdapter.notifyItemInserted(0);
      myCartAdapter.notifyDataSetChanged();
      setPriceDetails1(cd);
      checkout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (Commons.subtotal_amount != null) {
            amountcheck();
          }
        }
      });
    }
    if (Commons.flag_for_hta.equalsIgnoreCase("Patanjali") || Commons.flag_for_hta.equalsIgnoreCase(
        "City Special")
        || Commons.flag_for_hta == "Drinking Water" || Commons.flag_for_hta.equals(
        SpeedzyConstants.Fruits)) {
      restaurant__charges_lay.setVisibility(View.GONE);
      rest_charge_amount = 0.0;
    } else if (cd != null && cd.size() > 0) {
      restaurant__charges_lay.setVisibility(View.VISIBLE);
      restaurant__charges.setText("₹ " + cd.get(0).getSt_rest_charge_amount());
      rest_charge_amount = Double.parseDouble(cd.get(0).getSt_rest_charge_amount());
      Commons.resturant_charge_amount=cd.get(0).getSt_rest_charge_amount();
    }
    res_charge_tooltip.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        ToolTipView();
      }
    });
  }

  private void address_change_intent() {
    Intent intent = new Intent(CheckoutActivity_New.this, Saved_Addresses.class);
    intent.putExtra("change_address", true);
    startActivityForResult(intent, 1);
    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                /*Intent intent = new Intent(CheckoutActivity_New.this, LocationActivity.class);
                startActivityForResult(intent, 1);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);*/
  }

  private void ToolTipView() {
    View view = this.getLayoutInflater().inflate(R.layout.custom_tooltip, null);
    TextView amount = view.findViewById(R.id.amount);
    TextView desc = view.findViewById(R.id.desc);
    desc.setText(cd.get(0).getSt_rest_charge_desc());
    amount.setText("₹ " + cd.get(0).getSt_rest_charge_amount());
    new EasyDialog(CheckoutActivity_New.this)
        .setLayout(view)
        .setBackgroundColor(getResources().getColor(R.color.red))
        .setLocationByAttachedView(res_charge_tooltip)
        .setGravity(EasyDialog.GRAVITY_TOP)
        .setAnimationAlphaShow(100, 0.0f, 1.0f)
        .setAnimationAlphaDismiss(100, 1.0f, 0.0f)
        .setTouchOutsideDismiss(true)
        .setMatchParent(false)
        .setMarginLeftAndRight(24, 24)
        .setOutsideColor(Color.TRANSPARENT)
        .show();
  }

  private void getCouponList(String resturantId) {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);

    Call<CouponsResponse> call =methods.getCouponList(resturantId,mySharedPrefrencesData.getUser_Id(this),Commons.menu_id);
    Log.d("url","url="+call.request().url().toString());

    call.enqueue(new Callback<CouponsResponse>() {
      @Override
      public void onResponse(Call<CouponsResponse> call, Response<CouponsResponse> response) {
        int statusCode = response.code();
        Log.d("Response",""+statusCode);
        Log.d("respones",""+response);
        CouponsResponse couponsResponse=response.body();
        coupon_flag=couponsResponse.getFlag().toString();
        coupon_msg=couponsResponse.getMsg().toString();
        if (couponsResponse != null
                && couponsResponse.getStatus()
                .equalsIgnoreCase("Success")
                && couponsResponse.getCoupon() != null
                && couponsResponse.getCoupon().size() > 0) {
          mCouponItems.clear();
          mCouponItems.addAll(couponsResponse.getCoupon());
          add_coupon_dialog(mCouponItems);
        } else {
          add_coupon_dialog(mCouponItems);
        }
      }

      @Override
      public void onFailure(Call<CouponsResponse> call, Throwable t) {

        //             Toast.makeText(Reviews.this,"internet not available..connect internet",Toast.LENGTH_LONG).show();
      }
    });
  }

  private void add_coupon_dialog(List<CouponItem> data) {
    final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    Objects.requireNonNull(dialog.getWindow())
        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.setContentView(R.layout.popup_coupon_view);
    final EditText etCoupon = (EditText) dialog.findViewById(R.id.et_coupon);
    //forgotemail.getBackground()
    //    .setColorFilter(getResources().getColor(R.color.light_gray), PorterDuff.Mode.SRC_ATOP);
    //TextView reset = (TextView) d.findViewById(R.id.reset);
    //reset.setText("Apply");
    //reset.setOnClickListener(new View.OnClickListener() {
    //  @Override
    //  public void onClick(View v) {
    //    if (!forgotemail.getText().toString().equalsIgnoreCase("")) {
    //      call_voucher_api(forgotemail.getText().toString(), d);
    //    } else {
    //      msg.setText("Please enter voucher code");
    //      ok.setOnClickListener(new View.OnClickListener() {
    //        @Override
    //        public void onClick(View v) {
    //          dialog_msg.dismiss();
    //        }
    //      });
    //      dialog_msg.show();
    //    }
    //  }
    //});
    //TextView dismiss = (TextView) d.findViewById(R.id.dismiss);
    //dismiss.setOnClickListener(new View.OnClickListener() {
    //  @Override
    //  public void onClick(View v) {
    //    add_coupon_flag = false;
    //    apply_coupon.setText("Apply Coupon ");
    //    add_coupon.setText("Add");
    //    add_coupon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_right_black, 0);
    //    if (Commons.flag_for_hta == "Patanjali" || Commons.flag_for_hta == "City Special") {
    //      setPriceDetailsPatanjali(cd);
    //    } else {
    //      setPriceDetails1(cd);
    //    }
    //    d.dismiss();
    //  }
    //});
    //d.show();
    final RecyclerView rvCoupon = dialog.findViewById(R.id.rv_coupon);
    final TextView btnApply = dialog.findViewById(R.id.btn_apply);
    final TextView txtAvailableCoupons = dialog.findViewById(R.id.txt_available_coupons);
    final TextView txtNoCouponsAvailable = dialog.findViewById(R.id.txt_no_coupons_available);
    CouponAdapter mAdapter = new CouponAdapter(new CouponAdapter.CallBack() {
      @Override public void onClickApplyButton(CouponItem couponItem) {
        call_voucher_data(couponItem.getStCouponCode(), dialog);
      }
    });
    rvCoupon.setLayoutManager(RecyclerViewUtils.newLinearVerticalLayoutManager(this));
    rvCoupon.addItemDecoration(
        RecyclerViewUtils.newVerticalSpacingItemDecoration(this, R.dimen.spacing_12, true, true));
    rvCoupon.setAdapter(mAdapter);
    if (data.size() > 0) {
      mAdapter.setData(data);
      txtNoCouponsAvailable.setVisibility(View.GONE);
      txtAvailableCoupons.setVisibility(View.VISIBLE);
    } else {
      txtNoCouponsAvailable.setVisibility(View.VISIBLE);
      txtAvailableCoupons.setVisibility(View.GONE);
    }
    if (coupon_flag.equalsIgnoreCase("1")){
      txtNoCouponsAvailable.setVisibility(View.VISIBLE);
      txtAvailableCoupons.setVisibility(View.GONE);
      txtNoCouponsAvailable.setText(coupon_msg);
    }
    btnApply.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        if (!etCoupon.getText().toString().trim().isEmpty()) {
          call_voucher_data(etCoupon.getText().toString().trim(), dialog);
        } else {
          Toast.makeText(CheckoutActivity_New.this, "Enter Coupon Code", Toast.LENGTH_SHORT)
              .show();
        }
      }
    });
    dialog.show();
  }

  private void call_voucher_data(final String code, final Dialog d) {
    CouponItem couponItem = null;
    for (int i = 0; i < mCouponItems.size(); i++) {
      if (code.equalsIgnoreCase(mCouponItems.get(i).getStCouponCode())) {
        couponItem = mCouponItems.get(i);
      }
    }
    if (code != null && couponItem != null) {
      coupon_id = couponItem.getInCouponId().toString();
      discount_type = couponItem.getDiscountType().toString();
      discount_value = couponItem.getDiscountValue().toString();
      max_amount = couponItem.getMaxAmount().toString();
      min_order_value = couponItem.getMinOrderValue().toString();
      discount_slabs = couponItem.getDiscount_slabs();
      coupon_code = code;
      if (d != null) {
        if (Commons.flag_for_hta == "Patanjali" || Commons.flag_for_hta == "City Special"
            || Commons.flag_for_hta == "Drinking Water") {
          add_coupon_flag = true;
          setPriceDetailsPatanjali(cd);
        } else if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID)) {
          add_coupon_flag = true;
          setPriceDetails1(cd);
        } else {
          add_coupon_flag = true;
          setPriceDetails1(cd);
        }
        d.dismiss();
      }
    } else {
      msg.setText("Wrong code Applied");
      ok.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          add_coupon_flag = false;
          apply.setVisibility(View.VISIBLE);
          add_coupon_txt.setText("Apply Coupon ");
          add_coupon_txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
              0);
          dialog_msg.dismiss();
        }
      });
      dialog_msg.show();
    }
  }
  /*private void call_voucher_api(final String code, final Dialog d) {
    String resturantId = "";
    if (!GeneralUtil.isStringEmpty(lOcaldbNew.getRestaurantIdFromCart())) {
      resturantId = lOcaldbNew.getRestaurantIdFromCart();
    } else if (!GeneralUtil.isStringEmpty(Commons.restaurant_id)) {
      resturantId = Commons.restaurant_id;
    }
    Call<voucher_check_model> call =
        methods.getvoucher(code, resturantId, mySharedPrefrencesData.getUser_Id(this),
            String.valueOf(totalsum));
    Log.d("url", "url=" + call.request().url().toString());

    call.enqueue(new Callback<voucher_check_model>() {
      @Override
      public void onResponse(@NonNull Call<voucher_check_model> call,
          Response<voucher_check_model> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        voucher_check_model getvoucher_code = response.body();
        if (getvoucher_code != null && getvoucher_code.getStatus().equalsIgnoreCase("Success")) {
          coupon_id = getvoucher_code.getCode_info().getIn_coupon_id().toString();
          discount_type = getvoucher_code.getCode_info().getDiscount_type().toString();
          discount_value = getvoucher_code.getCode_info().getDiscount_value().toString();
          max_amount = getvoucher_code.getCode_info().getMax_amount().toString();
          min_order_value = getvoucher_code.getCode_info().getMin_order_value().toString();
          coupon_code = code;
          if (d != null) {
            if (Commons.flag_for_hta == "Patanjali" || Commons.flag_for_hta == "City Special"
                || Commons.flag_for_hta == "Drinking Water") {
              setPriceDetailsPatanjali(cd);
            } else if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID)) {
              add_coupon_flag = true;
              setPriceDetails1(cd);
            } else {
              add_coupon_flag = true;
              setPriceDetails1(cd);
            }
            d.dismiss();
          }
        } else {
          msg.setText(getvoucher_code.getMessage().toString());
          ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              add_coupon_flag = false;
              apply_coupon.setText("Apply Discount Coupon ");
              add_coupon.setText("Apply");
             *//* add_coupon.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_right_black,
                  0);*//*
              dialog_msg.dismiss();
            }
          });
          dialog_msg.show();
        }
      }

      @Override
      public void onFailure(@NonNull Call<voucher_check_model> call, @NonNull Throwable t) {
        //Toast.makeText(mc, "internet not available..connect internet", Toast.LENGTH_LONG).show();
        showErrorDialog(t.getMessage());
      }
    });
  }*/

  private void time_slect_fun() {
    if (time_selected.equalsIgnoreCase("1")) {
      asap.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_checked, 0, 0, 0);
      later.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_unchecked, 0, 0, 0);
      later_time.setVisibility(View.GONE);
    } else {
      later.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_checked, 0, 0, 0);
      asap.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_unchecked, 0, 0, 0);
    }
  }

  private void amountcheck() {
    double totalamnt = Double.parseDouble(Commons.subtotal_amount);
    if (Commons.flag_for_hta == "Grocerry"
            || Commons.flag_for_hta == "City Special"
        || Commons.flag_for_hta == "Patanjali"
        || Commons.flag_for_hta == "Drinking Water") {
      if (totalamnt >= Commons.min_order_grocery) {
        time_check();
      } else {
        msg.setText("Please add more dishes to get total amount of ₹ "+Commons.min_order_grocery);
        dialog_msg.show();
      }
    } else {
      if (totalamnt >= Commons.min_order_rest) {
        time_check();
      } else {
        msg.setText("Please add more dishes to get total amount of ₹" + Commons.min_order_rest);
        dialog_msg.show();
      }
    }
  }

  private void time_check() {
    if (Commons.flag_for_hta == "Grocerry" || Commons.flag_for_hta == "Patanjali"
        || Commons.flag_for_hta == "Drinking Water") {
      address_check();
    } else {
      if (time_selected.equalsIgnoreCase("2") && Commons.flag_for_hta != "Patanjali") {
        String s = "";
        if (tommorrow_del.equalsIgnoreCase("1")) {
          c.add(Calendar.DAY_OF_YEAR, 1);
        }
        if (later_time.getSelectedItem() != null) {
          s = dateFormatter1.format(c.getTime())
              + " "
              + later_time.getSelectedItem().toString()
              + ":00";
          Commons.Preorder_date_from_cart = s;

          address_check();
        } else {
          if (time_new_with_cuurenttime.isEmpty()) {
            dialog_show();
          }
        }
      } else {
        String s;
        if (time_new_with_cuurenttime.size() > 0) {
          //s = dateFormatter1.format(c.getTime()) + " " + time_new_with_cuurenttime.get(0) + ":00";
          s = dateFormatter.format(c.getTime());
        } else {
          s = dateFormatter1.format(c.getTime()) + " ";
        }
        if (Integer.parseInt(app_openstatus) == SpeedzyConstants.AppOpenStatus.OPEN) {
          Commons.Preorder_date_from_cart = s;
          Commons.dialogshow_payment(CheckoutActivity_New.this, cd.get(0).getDelivery_type(),
              MRPsum,
              suburbid, ordertype, userid, mySharedPrefrencesData, lOcaldbNew, delivery_charge,
              null);
        } else if (mSpeedzyOperationsClosedModel != null) {
          startActivity(OperationsClosedActivity.newIntent(this, mSpeedzyOperationsClosedModel));
        }
      }
    }
  }

  private void address_check() {
    if (mySharedPrefrencesData.getLocation(this) == null
            || mySharedPrefrencesData.getLocation(this).equalsIgnoreCase("")) {
      msg.setText("Please add or select default Address");
      dialog_msg.show();
    }else {
      Commons.dialogshow_payment(CheckoutActivity_New.this, cd.get(0).getDelivery_type(),
              MRPsum, suburbid, ordertype, userid, mySharedPrefrencesData, lOcaldbNew,
              delivery_charge, null);
    }
  }

  private void dialog_show() {
    final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.setContentView(R.layout.wallet_popup);
    LinearLayout linear1 = (LinearLayout) dialog.findViewById(R.id.linear1);
    TextView proceed = (TextView) dialog.findViewById(R.id.proceed);
    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
    TextView dis = (TextView) dialog.findViewById(R.id.dismiss);
    TextView warn = (TextView) dialog.findViewById(R.id.warning);
    TextView msg = (TextView) dialog.findViewById(R.id.message);
    linear1.setVisibility(View.VISIBLE);
    dis.setVisibility(View.GONE);
    warn.setText("Info!!");
    msg.setText("Sorry we are closed at this moment.please order for tomorrow");
    cancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //tommorrow_del = "1";
        dialog.dismiss();
      }
    });
    proceed.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        tommorrow_del = "1";
        dialogshow_time(deltime);
        dialog.dismiss();
      }
    });
    dialog.show();
  }

  private void statusbar_bg(int color) {
    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
        .getColor(color)));
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
    }
  }

  private void dialogshow_time(ArrayList<String> delivery_time) {
        /*final BottomSheetDialog dialog = new BottomSheetDialog(mc, R.style.SheetDialog);
        dialog.setContentView(R.layout.time_dialog);
        time_recyclerview = (RecyclerView) dialog.findViewById(R.id.time_recyclerview);
        final TextView today = (TextView) dialog.findViewById(R.id.today);
        final TextView tomorrow = (TextView) dialog.findViewById(R.id.tomorrow);
        TextView order_type = (TextView) dialog.findViewById(R.id.order_type);
        time_recyclerview.setHasFixedSize(true);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        time_recyclerview.setLayoutManager(lm);
        if (ordertype.equalsIgnoreCase("1")) {
            order_type.setText("Delivery By");
        } else {
            order_type.setText("Pickup By");
        }
        order_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
       *//* if ((!(Commons.flag_for_hta == null) && (!(Commons.closedtoday == null)) && Commons.flag_for_hta.equalsIgnoreCase("7") && Commons.closedtoday.equalsIgnoreCase("Close"))) {
            //analogClock.setVisibility(View.GONE);
            today.setVisibility(View.GONE);
            tomorrow.setVisibility(View.VISIBLE);
            istimecycle = true;
            setimagefortdto(1);
        } else {*//*
        today.setVisibility(View.VISIBLE);
        tomorrow.setVisibility(View.GONE);
        //tomorrow.setVisibility(View.VISIBLE);
       *//* istimecycle = true;
        try {
            setimagefortdto(0);
        } catch (ParseException e) {
            e.printStackTrace();
        }*//*
        // }
       *//* today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    istimecycle = true;
                    setimagefortdto(0);
                    today.setTextColor(getResources().getColor(R.color.black_transparent));
                    tomorrow.setTextColor(getResources().getColor(R.color.light_gray));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });*//*
     *//* tomorrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    istimecycle = true;
                    setimagefortdto(1);
                    tomorrow.setTextColor(getResources().getColor(R.color.black_transparent));
                    today.setTextColor(getResources().getColor(R.color.light_gray));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });*//*
        arrayspinner = time_new_with_cuurenttime.toArray(new String[0]);
        timeListAdapter = new TimeListAdapter(CheckoutActivity_New.this, arrayspinner);
        time_recyclerview.setAdapter(timeListAdapter);
        timeListAdapter.setOnItemClickListener(new TimeListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                later_time.setVisibility(View.VISIBLE);
                later_time.setText(arrayspinner[position]);
                dialog.dismiss();
            }
        });
        dialog.show();*/
    arrayspinner = delivery_time.toArray(new String[0]);
    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
        android.R.layout.simple_spinner_item, arrayspinner);
    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    later_time.setAdapter(dataAdapter);
    dataAdapter.notifyDataSetChanged();
    later_time.setVisibility(View.VISIBLE);
    later_time.setSelection(1);
    final Handler h = new Handler();
    new Thread(new Runnable() {
      public void run() {
        h.postDelayed(new Runnable() {
          public void run() {
            later_time.performClick();
          }
        }, 1000);
      }
    }).start();
  }

  public void setimagefortdto(int ca) throws ParseException, NullPointerException {
    try {
      if (ca == 0) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());

        String date_order = dateFormatter.format(c.getTime());
        Commons.order_date_from_cart = date_order;
        Commons.Preorder_date_from_cart = "";
        Commons.Preorder_date_from_cart = dateFormatter1.format(c.getTime());
        // if (ordertype == "1") {
        deltime = callfortime_del(/*deliverySchedule*/start_time_lunch_list,
            end_time_lunch_list, start_time_dinner_list, end_time_dinner_list);

        DateFormat dateFormatcuurenttime = new SimpleDateFormat("H:mm");
        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();
        String str = dateFormatcuurenttime.format(new Date(t + (30 * 60000)));
        Date cuurenttime = dateFormatcuurenttime.parse(str);

        time_new_with_cuurenttime.clear();
        for (int i = 0; i < deltime.size(); i++) {
          Date timecheck = dateFormatcuurenttime.parse(deltime.get(i));
          if (timecheck.compareTo(cuurenttime) > 0) {
            time_new_with_cuurenttime.add(dateFormatcuurenttime.format(timecheck));
          }
        }

        if (Commons.flag_for_hta.equals("Patanjali")) {
          time_selected = "2";
          time_slect_fun();
          asap.setVisibility(View.GONE);
          later.setVisibility(View.VISIBLE);
        } else if (Commons.flag_for_hta.equals("City Special")) {
          app_openstatus_fun();
        } else if (Commons.flag_for_hta.equals("Drinking Water")) {
          datelist();
          app_openstatus_fun();
        } else {
          if (cd.size() > 0) {
            app_openstatus_fun();
          }
        }
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }
  }

  private void datelist() {
    later_time.setVisibility(View.GONE);
    final Calendar c = Calendar.getInstance();
    mYear = c.get(Calendar.YEAR);
    mMonth = c.get(Calendar.MONTH);
    mDay = c.get(Calendar.DAY_OF_MONTH);
    DatePickerDialog dpd = new DatePickerDialog(CheckoutActivity_New.this,
        new DatePickerDialog.OnDateSetListener() {
          @Override
          public void onDateSet(DatePicker view, int year, int month, int day) {
            c.set(year, month, day);
            String date = dateFormatter1.format(c.getTime());
            later.setText(date);
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
          }
        }, mYear, mMonth, mDay);
    long now = System.currentTimeMillis() - 1000;
    dpd.getDatePicker().setMinDate(now);
    dpd.getDatePicker().setMaxDate(now + (1000 * 60 * 60 * 24 * 15));
    Calendar d = Calendar.getInstance();
    dpd.updateDate(d.get(Calendar.YEAR), d.get(Calendar.MONTH), d.get(Calendar.DAY_OF_MONTH));
    dpd.show();
  }

  private void app_openstatus_fun() {
    if (app_openstatus != null && app_openstatus.equalsIgnoreCase("0")) {
      time_selected = "2";
      time_slect_fun();
      asap.setTextColor(getResources().getColor(R.color.grey));
      if (!Commons.flag_for_hta.equals("Drinking Water")) {
        if (time_new_with_cuurenttime.size() > 0) {
          dialogshow_time(time_new_with_cuurenttime);
        } else {
          dialog_show();
        }
      }
    } else {
      time_selected = "1";
      time_slect_fun();
      asap.setTextColor(getResources().getColor(R.color.black));
    }
  }

  private ArrayList<String> callfortime_del(/*Restaurant_model.DeliverySchedule deliverySchedule*/
      String start_time_lunch_list,
      String end_time_lunch_list, String start_time_dinner_list, String end_time_dinner_list) {

    DateFormat dateFormat = new SimpleDateFormat("H:mm");
    DateFormat dateFormat2 = new SimpleDateFormat("H:mm");

    ArrayList<String> time_todaylist = new ArrayList<>();
    ArrayList<String> time_todaylist_lunch = new ArrayList<>();
    ArrayList<String> time_todaylist_dinner = new ArrayList<>();
    try {

      if (!(start_time_lunch_list == null)) {
        starttime_lunch = dateFormat.parse(start_time_lunch_list);
        endtime_lunch = dateFormat.parse(end_time_lunch_list);
        newtime = starttime_lunch;
        String str = dateFormat.format(new Date());
        Date cuurenttime = dateFormat.parse(str);
        time_todaylist_lunch.add(String.valueOf(dateFormat2.format(starttime_lunch)));
        while (newtime.compareTo(endtime_lunch) < 0 && istimecycle == true) {
          Date timget = covert_time_for_add(newtime, endtime_lunch);
          time_todaylist_lunch.add(String.valueOf(dateFormat2.format(timget)));
        }
        time_todaylist.addAll(time_todaylist_lunch);
      }

      if (!Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits) && !(
          start_time_dinner_list
              == null)) {
        starttime_dinner = dateFormat.parse(start_time_dinner_list);
        endtime_dinner = dateFormat.parse(end_time_dinner_list);
        System.out.println("Time: " + dateFormat.format(starttime_dinner));
        newtime = starttime_dinner;
        time_todaylist_dinner.add(String.valueOf(dateFormat2.format(starttime_dinner)));
        if (istimecycle == true) {
          while (newtime.compareTo(endtime_dinner) < 0 && istimecycle == true) {
            Date timget = covert_time_for_add(newtime, endtime_dinner);
            time_todaylist_dinner.add(String.valueOf(dateFormat2.format(timget)));
          }
        }
        time_todaylist.addAll(time_todaylist_dinner);
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }

    Log.d("lunch_del_arr", time_todaylist + "");
    return time_todaylist;
  }

  private ArrayList<String> callfortime_pick(Restaurant_model.PickupSchedule pickupSchedule) {

    DateFormat dateFormat = new SimpleDateFormat("H:mm");
    DateFormat dateFormat2 = new SimpleDateFormat("H:mm");

    ArrayList<String> time_todaylist = new ArrayList<>();
    try {

      if (!(pickupSchedule.getPick_start_time_lunch() == null)) {
        starttime_lunch = dateFormat.parse(pickupSchedule.getPick_start_time_lunch());
        endtime_lunch = dateFormat.parse(pickupSchedule.getPick_close_time_lunch());
        newtime = starttime_lunch;
        time_todaylist.add(String.valueOf(dateFormat2.format(starttime_lunch)));

        while (newtime.compareTo(endtime_lunch) < 0 && istimecycle == true) {
          Date timget = covert_time_for_add(newtime, endtime_lunch);
          time_todaylist.add(String.valueOf(dateFormat2.format(timget)));
        }
      }

      if (!(pickupSchedule.getPick_start_time_dinner() == null)) {
        starttime_dinner = dateFormat.parse(pickupSchedule.getPick_start_time_dinner());
        endtime_dinner = dateFormat.parse(pickupSchedule.getPick_close_time_dinner());
        System.out.println("Time: " + dateFormat.format(starttime_dinner));
        newtime = starttime_dinner;
        time_todaylist.add(String.valueOf(dateFormat2.format(starttime_dinner)));

        while (newtime.compareTo(endtime_dinner) < 0 && istimecycle == true) {
          Date timget = covert_time_for_add(newtime, endtime_dinner);
          time_todaylist.add(String.valueOf(dateFormat2.format(timget)));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    Log.d("lunch_del_arr", time_todaylist + "");
    return time_todaylist;
  }

  public Date covert_time_for_add(Date date, Date dateend) {

    SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.MINUTE, 30);
    String newTime1 = df.format(cal.getTime());
    Date addedtime = null;
    try {
      if (newTime1.equalsIgnoreCase("00:00")) {
        istimecycle = false;
        newtime = dateend;
      } else {
        addedtime = df.parse(newTime1);
        newtime = addedtime;
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return newtime;
  }

  public void setPriceDetails1(ArrayList<Cart_Model.Cart_Details> listdata) {
    if (listdata.size() > 0) {
      totalsum = 0.00;
      for (int k = 0; k < listdata.size(); k++) {
        final Cart_Model.Cart_Details cart_details = listdata.get(k);
        int qty = Integer.parseInt(cart_details.getQuantity());
        double dishPrice = Double.parseDouble(cart_details.getMenu_price()) * qty;
        ArrayList<ArrayList<Preferencemodel>> myarray = cart_details.getPreferencelfromdb();
        if (myarray.size() > 1) {
          for (int i = 1; i < myarray.size(); i++) {
            ArrayList<Preferencemodel> mychildarray = cart_details.getPreferencelfromdb().get(i);
            for (int j = 0; j < mychildarray.size(); j++) {
              Preferencemodel prefData = mychildarray.get(j);
              if (prefData.getMenuprice().length() > 2) {
                double itemPrice = Double.parseDouble(prefData.getMenuprice()) * qty;
                dishPrice = dishPrice + itemPrice;
              } else {
                double itemPrice = 0.00;
                dishPrice = dishPrice + itemPrice;
              }
            }
          }
        }
        totalsum = totalsum + dishPrice;
      }
            /*double discountvalue = 0.00;
            double discountvalue_total = 0.00;

            if (mySharedPrefrencesData.getUser_Id(this).equalsIgnoreCase(null) || mySharedPrefrencesData.getUser_Id(this).isEmpty()) {
                specialoffertextheader.setVisibility(View.GONE);
                specialofferslistview.setVisibility(View.GONE);
            } else {
                int orderno = Integer.parseInt(ordernum);
                CheckOfferlist(totalsum, orderno);
                Log.d("selectedofferlist", selectedofferlist + "");
                if (selectedofferlist.size() > 0) {
                    specialoffertextheader.setVisibility(View.VISIBLE);
                    specialofferslistview.setVisibility(View.VISIBLE);
                    for (int i = 0; i < selectedofferlist.size(); i++) {
                        int offertype = Integer.parseInt(selectedofferlist.get(i).getFlg_offer_type());
                        if (offertype == 1) {
                            discountvalue = totalsum * (Double.valueOf(selectedofferlist.get(i).getFirst_offer_value()) / 100);
                            selectedofferlist.get(i).setOfferprice((float) discountvalue);
                        } else if (offertype == 2) {
                            discountvalue = Double.parseDouble(avg_order_value);
                            selectedofferlist.get(i).setOfferprice((float) discountvalue);
                        } else if (offertype == 3) {
                            discountvalue = totalsum * (Double.valueOf(selectedofferlist.get(i).getSecond_offer_value()) / 100);
                            selectedofferlist.get(i).setOfferprice((float) discountvalue);
                        } else if (offertype == 4) {
                            discountvalue = Double.parseDouble(selectedofferlist.get(i).getThird_offer_value());
                            selectedofferlist.get(i).setOfferprice((float) discountvalue);
                        } else if (offertype == 5) {
                            discountvalue = Double.parseDouble(selectedofferlist.get(i).getThird_offer_value());
                            selectedofferlist.get(i).setOfferprice((float) discountvalue);
                        } else if (offertype == 6) {
                            discountvalue = totalsum * (Double.valueOf(selectedofferlist.get(i).getFirst_offer_value()) / 100);
                            selectedofferlist.get(i).setOfferprice((float) discountvalue);
                        } else if (offertype == 7) {
                            // String deliverychrg_selected = deliverycostfor_del.replace("₹", "");
                            String deliverychrg_selected = delivery_charge;
                            discountvalue = Double.parseDouble(deliverychrg_selected);
                            selectedofferlist.get(i).setOfferprice((float) discountvalue);
                            double delchrg = discountvalue - discountvalue;
                            deliverychargetxt.setText("₹" + String.valueOf(delchrg));
                        } else if (offertype == 8) {
                            discountvalue = totalsum * (Double.valueOf(selectedofferlist.get(i).getFirst_offer_value()) / 100);
                            selectedofferlist.get(i).setOfferprice((float) discountvalue);
                        } else if (offertype == 9) {
                            discountvalue = -(totalsum * (Double.valueOf(selectedofferlist.get(i).getFirst_offer_value()) / 100));
                            selectedofferlist.get(i).setOfferprice((float) discountvalue);

                        }
                        discountvalue_total = discountvalue_total + discountvalue;
                    }
                    specialOfferAdapter.notifyDataSetChanged();
                } else {
                    specialoffertextheader.setVisibility(View.GONE);
                    specialofferslistview.setVisibility(View.GONE);
                }

            }*/

      if (add_coupon_flag) {
              /*  specialoffertextheader.setVisibility(View.GONE);
                specialofferslistview.setVisibility(View.GONE);*/
        discountvalue = 0.00;
        call_voucher_data(coupon_code, null);
        AddCoupon();
      } else {
        max_coupon_discount = 0.0;
      }
      Final_totalsum = totalsum - discountvalue - max_coupon_discount;
      discount_text.setText("-₹" + String.format("%.2f", discountvalue));
      subtotaltxt.setText("₹" + String.format("%.2f", totalsum));
      Log.e("totalsum:", "" + totalsum);
      call_deliverychargeApi(0, Final_totalsum);
    } else {
      nocart.setVisibility(View.VISIBLE);
      cartlay.setVisibility(View.GONE);
      checkout.setVisibility(View.GONE);
    }

    if (Commons.flag_for_hta.equals("7")) {
      RestarantDetails.setPriceDetails(cd, lOcaldbNew.getQuantity(Commons.restaurant_id));
    }
  }

  private void AddCoupon() {
    if (Double.parseDouble(min_order_value) <= totalsum) {
      if (discount_type.equalsIgnoreCase("0")) {
        coupon_discount_amount = Double.parseDouble(discount_value);
        if (coupon_discount_amount >= Double.parseDouble(max_amount)) {
          max_coupon_discount = Double.parseDouble(max_amount);
        } else {
          max_coupon_discount = coupon_discount_amount;
        }
      } else {
        coupon_discount_amount = totalsum * (Double.parseDouble(discount_value) / 100);
        if (!discount_slabs.isEmpty()) {
          for (int i = 0; i < discount_slabs.size(); i++) {
            if (!discount_slabs.get(i).getOrdervalue().equalsIgnoreCase("")) {
              if (totalsum >= Double.parseDouble(discount_slabs.get(i).getOrdervalue())) {
                if (coupon_discount_amount >= Double.parseDouble(
                    discount_slabs.get(i).getDiscount())) {
                  max_coupon_discount = Double.parseDouble(discount_slabs.get(i).getDiscount());
                } else {
                  max_coupon_discount = coupon_discount_amount;
                }
              }
            } else {
              if (coupon_discount_amount >= Double.parseDouble(max_amount)) {
                max_coupon_discount = Double.parseDouble(max_amount);
              } else {
                max_coupon_discount = coupon_discount_amount;
              }
            }
          }
        }
      }
    } else {
      coupon_discount_amount = 0.00;
      max_coupon_discount = 0.00;
      msg.setText("Sorry! this offer is applicable only on the orders above ₹ " + min_order_value);
      ok.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          apply.setVisibility(View.VISIBLE);
          add_coupon_txt.setText("Apply Coupon ");
          add_coupon_txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
              0);
          dialog_msg.dismiss();
        }
      });
      dialog_msg.show();
    }
  }

  private void call_deliverychargeApi(final int i, double final_totalsum) {
    double distance=Commons.getDistance(lati,longi,mySharedPrefrencesData.getSelectCity_latitude(this),
            mySharedPrefrencesData.getSelectCity_longitude(this));
    Call<Delivery_Charge_Response_Model> call = null;
    showProgressDialog();
    if (i == 0) {
      call = methods.get_delivery_charge(mySharedPrefrencesData.getSelectCity_longitude(this),
          mySharedPrefrencesData.getSelectCity_latitude(this),
          mySharedPrefrencesData.getSelectAddress_ID(this),
          mySharedPrefrencesData.getSelectCity(this), String.valueOf(final_totalsum), userid,
          Commons.restaurant_id, cake_flag, Commons.menu_id, String.valueOf(distance));
      Log.d("url", "url=" + call.request().url().toString());
    } else if (i == 1) {
      call = methods.get_delivery_charge_patanjali(
          mySharedPrefrencesData.getSelectCity_longitude(this),
          mySharedPrefrencesData.getSelectCity_latitude(this),
          mySharedPrefrencesData.getSelectAddress_ID(this),
          mySharedPrefrencesData.getSelectCity(this), String.valueOf(final_totalsum), userid,
          cake_flag, Commons.menu_id);
      Log.d("url", "url=" + call.request().url().toString());
    }
    if (call != null) {
      call.enqueue(new Callback<Delivery_Charge_Response_Model>() {
        @Override
        public void onResponse(@NonNull Call<Delivery_Charge_Response_Model> call,
            @NonNull Response<Delivery_Charge_Response_Model> response) {
          dismissProgressDialog();
          int statusCode = response.code();
          Log.d("Response", "" + response);
          Delivery_Charge_Response_Model delivery_charge_response_model = response.body();
          if (delivery_charge_response_model.getStatus().equalsIgnoreCase("Success")) {

            double discountvalue_total = 0.00;
            discountvalue_total = patanjali_offer_check(delivery_charge_response_model);

            if (ordertype.equalsIgnoreCase("1")) {
              delivery_charge = delivery_charge_response_model.getDeliverycharge().toString();
              deliverychargetxt.setText("₹" + delivery_charge);
            } else {
              delivery_charge = String.valueOf(0.00);
              deliverychargetxt.setText("₹" + delivery_charge);
            }

            double grandtotalamount = 0.00, walletvalue = 0;
            if (mySharedPrefrencesData.getUser_Id(CheckoutActivity_New.this).length() > 0) {
              if (Commons.flag_for_hta == "7") {
                gstlayout.setVisibility(View.VISIBLE);
                double gstvalue = 0;
                if (Commons.gst_commision != null) {
                  gstvalue = totalsum * (Double.parseDouble(Commons.gst_commision) / 100);
                }
                gstamount.setText("₹" + String.format("%.2f", gstvalue));
                grandtotalamount =
                    totalsum - discountvalue_total + rest_charge_amount + (Double.parseDouble(
                        delivery_charge) + gstvalue
                        - max_coupon_discount);
                grandtotal.setText("₹" + String.format("%.2f", grandtotalamount));
                Commons.gstamount = Double.parseDouble(String.format("%.2f", gstvalue));
              } else {
                gstlayout.setVisibility(View.GONE);
                grandtotalamount =
                    totalsum - discountvalue_total + rest_charge_amount + (Double.parseDouble(
                        delivery_charge)
                        - max_coupon_discount);
                grandtotal.setText("₹" + String.format("%.2f", grandtotalamount));
                Commons.gstamount = 0;
              }
            } else {
              if (Commons.flag_for_hta == "7") {
                gstlayout.setVisibility(View.VISIBLE);
                double gstvalue = 0;
                if (Commons.gst_commision != null) {
                  gstvalue = totalsum * (Double.parseDouble(Commons.gst_commision) / 100);
                }
                gstamount.setText("₹" + String.format("%.2f", gstvalue));
                grandtotalamount =
                    totalsum - discountvalue_total
                        + rest_charge_amount
                        + (Double.parseDouble(delivery_charge))
                        + gstvalue;
                grandtotal.setText("₹" + String.format("%.2f", grandtotalamount));
                Commons.gstamount = Double.parseDouble(String.format("%.2f", gstvalue));
              } else {
                gstlayout.setVisibility(View.GONE);
                grandtotalamount =
                    totalsum - discountvalue_total
                        + rest_charge_amount
                        + (Double.parseDouble(delivery_charge));
                grandtotal.setText("₹" + String.format("%.2f", grandtotalamount));
                Commons.gstamount = 0;
              }
            }
            if (Commons.wallet_static_value != null) {
              if (grandtotalamount > Double.parseDouble(Commons.wallet_static_value)) {
                walletvalue = Double.parseDouble(Commons.wallet_static_value);
              } else {
                walletvalue = grandtotalamount;
              }
            }
            wallet_text.setText("-₹" + walletvalue);
            grandtotalamount = grandtotalamount - walletvalue;
            grandtotal.setText("₹" + String.format("%.2f", grandtotalamount));
            String walletrupees = wallet_text.getText().toString();
            walletrupees = walletrupees.replace("-₹", "");
            Commons.wallet = String.valueOf(walletrupees);
            Commons.order_amount = String.format("%.2f", totalsum);
            Commons.subtotal_amount = String.format("%.2f", totalsum);
            Commons.total_amount = String.format("%.2f", grandtotalamount);
            Commons.dicount_val = String.valueOf(discountvalue);
            Commons.delivery_charge = String.valueOf(delivery_charge);
            Commons.coupon_id = coupon_id;
            discount_text.setText("-₹" + String.format("%.2f", discountvalue_total));
            subtotaltxt.setText("₹" + String.format("%.2f", totalsum));
            if (add_coupon_flag) {
              discount_lay.setVisibility(View.GONE);
              apply.setVisibility(View.VISIBLE);
              coupon_amount = String.format("%.2f", max_coupon_discount);
              add_coupon_txt.setText("Coupon Applied (" + coupon_code + " )");
              apply.setText("Remove");
              coupon_dis_lay.setVisibility(View.VISIBLE);
              coupon_dis_text.setText("-₹" + coupon_amount);
            } else {
              coupon_dis_lay.setVisibility(View.GONE);
              discount_lay.setVisibility(View.GONE);
              coupon_amount = String.format("%.2f", max_coupon_discount);

              apply.setVisibility(View.VISIBLE);
              add_coupon_txt.setText("Apply Coupon ");
              add_coupon_txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
                  0);
            }
            Commons.dicount_val = coupon_amount;
            Commons.coupon_amount = coupon_amount;
            Commons.voucherid = coupon_code;
            Commons.vaoucheramount = coupon_amount;
            if (Commons.flag_for_hta.equals("7")) {
              RestarantDetails.setPriceDetails(cd, lOcaldbNew.getQuantity(Commons.restaurant_id));
            }
            if (Commons.flag_for_hta.equals("8")) {
              //  Train_Abhada.setPriceDetails(cd, lOcaldbNew.getQuantity(Commons.restaurant_id));
            }
            if (Commons.flag_for_hta.equals("9") || Commons.flag_for_hta.equals("10")) {
              //Train_Abhada.setPriceDetails(cd, lOcaldbNew.getQuantity(Commons.restaurant_id));
            }
          }
        }

        @Override
        public void onFailure(@NonNull Call<Delivery_Charge_Response_Model> call,
            @NonNull Throwable t) {
          //Toast.makeText(getApplicationContext(), "internet not available..connect internet", Toast.LENGTH_LONG).show();
          dismissProgressDialog();
          showErrorDialog(t.getMessage());
        }
      });
    }
  }

  private double patanjali_offer_check(Delivery_Charge_Response_Model offers) {
    double discountvalue = 0.00, discountvalue_total = 0.00;
    offerArrayList_patanjali.clear();
    offerArrayList_patanjali.addAll(offers.getOffers());
    if (!GeneralUtil.isStringEmpty(mySharedPrefrencesData.getUser_Id(this))
        || mySharedPrefrencesData.getUser_Id(this).isEmpty()) {
      specialoffertextheader.setVisibility(View.GONE);
      specialofferslistview.setVisibility(View.GONE);
    } else {
      int orderno = Integer.parseInt(offers.getOrderno());
      CheckOfferlist_patanjali(totalsum, orderno);
      if (selectedofferlist_patanjali.size() > 0) {
        specialoffertextheader.setVisibility(View.VISIBLE);
        specialofferslistview.setVisibility(View.VISIBLE);
        for (int i = 0; i < selectedofferlist_patanjali.size(); i++) {
          int offertype = Integer.parseInt(selectedofferlist_patanjali.get(i).getFlgOfferType());
          if (offertype == 1) {
            discountvalue =
                totalsum * (Double.valueOf(selectedofferlist_patanjali.get(i).getFirstOfferValue())
                    / 100);
            selectedofferlist_patanjali.get(i).setOfferprice((float) discountvalue);
          } else if (offertype == 2) {
            discountvalue = Double.parseDouble(avg_order_value);
            selectedofferlist_patanjali.get(i).setOfferprice((float) discountvalue);
          } else if (offertype == 3) {
            discountvalue =
                totalsum * (Double.valueOf(selectedofferlist_patanjali.get(i).getSecondOfferValue())
                    / 100);
            selectedofferlist_patanjali.get(i).setOfferprice((float) discountvalue);
          } else if (offertype == 4) {
            discountvalue =
                Double.parseDouble(selectedofferlist_patanjali.get(i).getThirdOfferValue());
            selectedofferlist_patanjali.get(i).setOfferprice((float) discountvalue);
          } else if (offertype == 5) {
            discountvalue =
                Double.parseDouble(selectedofferlist_patanjali.get(i).getThirdOfferValue());
            selectedofferlist_patanjali.get(i).setOfferprice((float) discountvalue);
          } else if (offertype == 6) {
            discountvalue =
                totalsum * (Double.valueOf(selectedofferlist_patanjali.get(i).getFirstOfferValue())
                    / 100);
            selectedofferlist_patanjali.get(i).setOfferprice((float) discountvalue);
          } else if (offertype == 7) {
            // String deliverychrg_selected = deliverycostfor_del.replace("₹", "");
            String deliverychrg_selected = delivery_charge;
            discountvalue = Double.parseDouble(deliverychrg_selected);
            selectedofferlist_patanjali.get(i).setOfferprice((float) discountvalue);
            double delchrg = discountvalue - discountvalue;
            deliverychargetxt.setText("₹" + String.valueOf(delchrg));
          } else if (offertype == 8) {
            discountvalue =
                totalsum * (Double.valueOf(selectedofferlist_patanjali.get(i).getFirstOfferValue())
                    / 100);
            selectedofferlist_patanjali.get(i).setOfferprice((float) discountvalue);
          } else if (offertype == 9) {
            discountvalue = -(totalsum * (Double.valueOf(
                selectedofferlist_patanjali.get(i).getFirstOfferValue()) / 100));
            selectedofferlist_patanjali.get(i).setOfferprice((float) discountvalue);
          }
          discountvalue_total = discountvalue_total + discountvalue;
        }
        specialOfferAdapter.notifyDataSetChanged();
      } else {
        specialoffertextheader.setVisibility(View.GONE);
        specialofferslistview.setVisibility(View.GONE);
      }
    }
    if (add_coupon_flag) {
      specialoffertextheader.setVisibility(View.GONE);
      specialofferslistview.setVisibility(View.GONE);
      discountvalue_total = 0.00;
      AddCoupon();
    } else {
      if (selectedofferlist_patanjali.size() > 0) {
        specialoffertextheader.setVisibility(View.VISIBLE);
        specialofferslistview.setVisibility(View.VISIBLE);
      }
    }
    return discountvalue_total;
  }

  public void setPriceDetailsPatanjali(ArrayList<Cart_Model.Cart_Details> listdata) {
    if (listdata.size() > 0) {
      totalsum = 0.00;
      MRPsum = 0.0;
      for (int k = 0; k < listdata.size(); k++) {
        Cart_Model.Cart_Details cart1 = listdata.get(k);
        double itemPrice =
            Double.parseDouble(cart1.getMenu_price()) * Double.parseDouble(cart1.getQuantity());
        double MRP_itemPrice =
            Double.parseDouble(cart1.getMenu_price()) * Double.parseDouble(cart1.getQuantity());
        totalsum = totalsum + itemPrice;
        MRPsum = MRPsum + MRP_itemPrice;
      }
      double discountvalue_total = 0.00;

      if (add_coupon_flag) {
        discountvalue_total = 0.00;
        AddCoupon();
      } else {
        max_coupon_discount = 0.0;
      }

      Final_totalsum = totalsum - discountvalue_total - max_coupon_discount;
      discount_text.setText("-₹" + String.format("%.2f", discountvalue_total));
      String deliverychagestr = deliverychargetxt.getText().toString();
      deliverychagestr = deliverychagestr.replace("₹", "");
      subtotaltxt.setText("₹" + String.format("%.2f", totalsum));
      Log.e("totalsum:", "" + totalsum);
      call_deliverychargeApi(1, Final_totalsum);
    } else {
      nocart.setVisibility(View.VISIBLE);
      cartlay.setVisibility(View.GONE);
      checkout.setVisibility(View.GONE);
    }
  }

  public void CheckOfferlist_patanjali(double orderPrice, int ordernumber) {
    selectedofferlist_patanjali.clear();
    for (int i = 0; i < offerArrayList_patanjali.size(); i++) {

      Delivery_Charge_Response_Model.Offer offers = offerArrayList_patanjali.get(i);
      int offerType = Integer.parseInt(offers.getFlgOfferType());
      if (offerType == 9) {
        selectedofferlist_patanjali.add(offers);
      } else if (offerType == 2 && ordernumber == Integer.parseInt(offers.getSecondOfferValue())) {
        selectedofferlist_patanjali.add(offers);
        break;
      } else if (offerType == 1 && ordernumber < 2) {
        selectedofferlist_patanjali.add(offers);
      } else if (offerType == 7 && orderPrice >= Double.parseDouble(offers.getFirstOfferValue())) {
        selectedofferlist_patanjali.add(offers);
      } else if (offerType == 8 && ordernumber == Integer.parseInt(offers.getFirstOfferValue())) {
        selectedofferlist_patanjali.add(offers);
      } else if (offerType == 3 && orderPrice >= Double.parseDouble(offers.getFirstOfferValue())) {
        selectedofferlist_patanjali.add(offers);
      } else if (orderPrice >= Double.parseDouble(offers.getFirstOfferValue()) && (offerType == 5
          || offerType == 4
          || offerType == 6)) {
        checkPreviousOfferWithCurrentOffer_patanjali(offers);
      }
    }
  }

  void checkPreviousOfferWithCurrentOffer_patanjali(
      Delivery_Charge_Response_Model.Offer currentOffer) {
    boolean needsToInsert = true;
    for (int i = 0; i < selectedofferlist_patanjali.size(); i++) {
      Delivery_Charge_Response_Model.Offer offer = selectedofferlist_patanjali.get(i);
      int offerType = Integer.parseInt(offer.getFlgOfferType());
      if (offerType < 4 || offerType > 6 || offerType == Integer.parseInt(
          currentOffer.getFlgOfferType())) {
        continue;
      }
      if (offerType < Double.parseDouble(currentOffer.getFirstOfferValue())) {
        selectedofferlist_patanjali.set(i, currentOffer);
        needsToInsert = false;
      }
    }
    if (needsToInsert) {
      selectedofferlist_patanjali.add(currentOffer);
    }
  }

  public void CheckOfferlist(double orderPrice, int ordernumber) {
    selectedofferlist.clear();
    for (int i = 0; i < offerArrayList.size(); i++) {

      Restaurant_Dish_Model.NewOffers offers = offerArrayList.get(i);
      int offerType = Integer.parseInt(offers.getFlg_offer_type());
      if (offerType == 9) {
        selectedofferlist.add(offers);
      } else if (offerType == 2 && ordernumber == Integer.parseInt(
          offers.getSecond_offer_value())) {
        selectedofferlist.add(offers);
        break;
      } else if (offerType == 1 && ordernumber < 2) {
        selectedofferlist.add(offers);
      } else if (offerType == 7 && orderPrice >= Double.parseDouble(
          offers.getFirst_offer_value())) {
        selectedofferlist.add(offers);
      } else if (offerType == 8 && ordernumber == Integer.parseInt(offers.getFirst_offer_value())) {
        selectedofferlist.add(offers);
      } else if (offerType == 3 && orderPrice >= Double.parseDouble(
          offers.getFirst_offer_value())) {
        selectedofferlist.add(offers);
      } else if (orderPrice >= Double.parseDouble(offers.getFirst_offer_value()) && (offerType == 5
          || offerType == 4
          || offerType == 6)) {
        checkPreviousOfferWithCurrentOffer(offers);
      }
    }
  }

  void checkPreviousOfferWithCurrentOffer(Restaurant_Dish_Model.NewOffers currentOffer) {
    boolean needsToInsert = true;
    for (int i = 0; i < selectedofferlist.size(); i++) {
      Restaurant_Dish_Model.NewOffers offer = selectedofferlist.get(i);
      int offerType = Integer.parseInt(offer.getFlg_offer_type());
      if (offerType < 4 || offerType > 6 || offerType == Integer.parseInt(
          currentOffer.getFlg_offer_type())) {
        continue;
      }
      if (offerType < Double.parseDouble(currentOffer.getFirst_offer_value())) {
        selectedofferlist.set(i, currentOffer);
        needsToInsert = false;
      }
    }
    if (needsToInsert) {
      selectedofferlist.add(currentOffer);
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home) {
      onBackPressed();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    Intent intent=new Intent();
    if (Commons.flag_for_hta=="7" || Commons.flag_for_hta == SpeedzyConstants.Fruits) {
      if (cd != null && !cd.isEmpty()) {
        intent.putExtra("res_id", cd.get(0).getIn_restaurant_id());
      } else {
        intent.putExtra("res_id", "");
      }
    }
    setResult(RESULT_OK, intent);
    Commons.back_button_transition(CheckoutActivity_New.this);

    super.onBackPressed();
  }

  public void callapi() {
    if (mySharedPrefrencesData == null) {
      mySharedPrefrencesData = new MySharedPrefrencesData();
    }
    if (methods == null) {
      methods =
          API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    }
    city_id = mySharedPrefrencesData.getSelectCity(this);
    methods.get_home(city_id, mySharedPrefrencesData.getVeg_Flag(this))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<HomePageAppConfigResponse>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(HomePageAppConfigResponse home_response_model) {
            if (home_response_model != null) {
              app_openstatus = home_response_model.getAppOpenstatus();
              mSpeedzyOperationsClosedModel = SpeedzyOperationsClosedModel.transform(
                  home_response_model.getAppclosestatus(),
                  Integer.parseInt(home_response_model.getAppOpenstatus()),
                  Integer.parseInt(home_response_model.getAppCloseType()));
            }
          }

          @Override public void onError(Throwable e) {

          }

          @Override public void onComplete() {

          }
        });
  }

  private void goToClosedPage(SpeedzyOperationsClosedModel speedzyOperationsClosedModel) {
    if (speedzyOperationsClosedModel != null) {
      startActivity(OperationsClosedActivity.newIntent(this, speedzyOperationsClosedModel));
    }
  }

  @Override public void onStart() {
    super.onStart();
    callapi();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    GeneralUtil.safelyDispose(mDisposable);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    try {
      if (requestCode == 11 && resultCode == RESULT_OK) {
        payment_status(data);
      } else if (requestCode == 22 && resultCode == RESULT_OK) {
        payment_status(data);
      } else if (requestCode == 33 && resultCode == RESULT_OK) {
        payment_status(data);
      } else if (requestCode == 1) {
        if (mySharedPrefrencesData.getLocation(this).equalsIgnoreCase("")||
                mySharedPrefrencesData.getLocation(this) == null) {
          delivery_frag.setVisibility(View.GONE);
          add_address.setVisibility(View.VISIBLE);
        }else {
          delivery_frag.setVisibility(View.VISIBLE);
          add_address.setVisibility(View.GONE);
          String message = data.getStringExtra("SELECTED_LOCATION");
          selected_location.setText(message);
        }
        if (Commons.flag_for_hta == "Patanjali" || Commons.flag_for_hta == "City Special"
            || Commons.flag_for_hta == "Drinking Water") {
          setPriceDetailsPatanjali(cd);
        } else {
          setPriceDetails1(cd);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    super.onActivityResult(requestCode, resultCode, data);
  }

  private void payment_status(Intent data) {
    Bundle bundle = data.getExtras();
    for (String key : bundle.keySet()) {
      Log.e("UPI_txn=>", key + ":" + bundle.get(key));
    }
    String orderid = data.getStringExtra("txnRef");
    mOrderUPIDataModel = new OrderUPIDataModel();
    mOrderUPIDataModel.setOrderNumber(orderid);
    Log.e("UPI_txn", orderid);
    Log.e("UPI_txn", data.getStringExtra("Status"));
    if (data.getStringExtra("Status").equalsIgnoreCase("SUCCESS")) {
      apicall();
    } else {
      Toast.makeText(this, "Payment Failed", Toast.LENGTH_SHORT).show();
    }
  }

  private void apicall() {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    Call<StorepackagerModel> call =
        methods.changepayment(mOrderUPIDataModel.getOrderNumber(), Commons.menu_id);
    Log.d("url", "url=" + call.request().url().toString());
    call.enqueue(new Callback<StorepackagerModel>() {
      @Override
      public void onResponse(Call<StorepackagerModel> call, Response<StorepackagerModel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        StorepackagerModel deviceUpdateModel = response.body();
        if (deviceUpdateModel.getStatus().equalsIgnoreCase("Success")) {
          goToOrderSuccessPage();
        }
      }

      @Override
      public void onFailure(Call<StorepackagerModel> call, Throwable t) {
        Toast.makeText(getApplicationContext(), "internet not available..connect internet",
            Toast.LENGTH_LONG).show();
      }
    });
  }

  private void goToOrderSuccessPage() {
    Toast.makeText(this, "Payment Successful", Toast.LENGTH_SHORT).show();
    ActivityOrderDataModel activityOrderDataModel = new ActivityOrderDataModel();
    activityOrderDataModel.setOrderId(mOrderUPIDataModel.getOrderNumber());
    startActivity(ActivityOrderSuccess.newIntent(this, activityOrderDataModel));
    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    this.finish();
  }
}
