package com.food.order.speedzy.api.response.maps;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class RoutesItem{

	@SerializedName("summary")
	private String summary;

	@SerializedName("copyrights")
	private String copyrights;

	@SerializedName("legs")
	private List<LegsItem> legs;

	@SerializedName("warnings")
	private List<Object> warnings;

	@SerializedName("bounds")
	private Bounds bounds;

	@SerializedName("overview_polyline")
	private OverviewPolyline overviewPolyline;

	@SerializedName("waypoint_order")
	private List<Object> waypointOrder;

	public String getSummary(){
		return summary;
	}

	public String getCopyrights(){
		return copyrights;
	}

	public List<LegsItem> getLegs(){
		return legs;
	}

	public List<Object> getWarnings(){
		return warnings;
	}

	public Bounds getBounds(){
		return bounds;
	}

	public OverviewPolyline getOverviewPolyline(){
		return overviewPolyline;
	}

	public List<Object> getWaypointOrder(){
		return waypointOrder;
	}

	@Override
 	public String toString(){
		return 
			"RoutesItem{" + 
			"summary = '" + summary + '\'' + 
			",copyrights = '" + copyrights + '\'' + 
			",legs = '" + legs + '\'' + 
			",warnings = '" + warnings + '\'' + 
			",bounds = '" + bounds + '\'' + 
			",overview_polyline = '" + overviewPolyline + '\'' + 
			",waypoint_order = '" + waypointOrder + '\'' + 
			"}";
		}
}