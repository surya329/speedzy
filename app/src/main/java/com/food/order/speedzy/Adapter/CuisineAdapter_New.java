package com.food.order.speedzy.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.food.order.speedzy.Activity.RestaurantActivity_NewVersion;
import com.food.order.speedzy.Model.Filtercuisine;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;

import java.util.ArrayList;

/**
 * Created by Sujata Mohanty.
 */


public class CuisineAdapter_New extends RecyclerView.Adapter<CuisineAdapter_New.ViewHolder> {
    Context context;
    private ArrayList<Filtercuisine> filtercuisineArrayList;
    int a = 0;
    MySharedPrefrencesData mySharedPrefrencesData;
    RestaurantActivity_NewVersion filteractivity;


    public CuisineAdapter_New(Context context, ArrayList<Filtercuisine> filtercuisineArrayList) {
        this.context = context;
        this.filtercuisineArrayList = filtercuisineArrayList;
        this.filteractivity = (RestaurantActivity_NewVersion) context;
    }

    @Override
    public CuisineAdapter_New.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_view_cuisine_row, null);
        CuisineAdapter_New.ViewHolder rcv = new CuisineAdapter_New.ViewHolder(layoutView);
        Commons.cuisinearray = new ArrayList<String>();
        Commons.cuisinearray.clear();
        return rcv;
    }

    @Override
    public void onBindViewHolder(final CuisineAdapter_New.ViewHolder holder, final int position) {
        holder.textView.setText(filtercuisineArrayList.get(position).getCuisine());
        if (filtercuisineArrayList.get(position).isSelected()) {
            holder.textView.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_checkbox_checked, 0, 0, 0);

            ArrayList<String> arrayList = new ArrayList<String>();
            String cuisine_text = holder.textView.getText().toString();
            arrayList.add(cuisine_text);
            Commons.cuisinearray.addAll(arrayList);

        } else {
            holder.textView.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_checkbox_unchecked, 0, 0, 0);
        }

        Log.d("cuisinearray", Commons.cuisinearray + "");

        holder.textView.setOnClickListener(new View.OnClickListener() {
            private boolean stateChanged;

            @Override
            public void onClick(View v) {
                boolean status = filtercuisineArrayList.get(position).isSelected();
                filtercuisineArrayList.get(position).setSelected(!status);
                notifyDataSetChanged();
                filteractivity.setData(filtercuisineArrayList);

            }
        });


    }

    @Override
    public int getItemCount() {
        return filtercuisineArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv);
        }
    }
}
