package com.food.order.speedzy.SpeedzyStore;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.food.order.speedzy.Activity.Saved_Addresses;
import com.food.order.speedzy.Adapter.GalleryAdapter;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;

import com.food.order.speedzy.Utils.GifSizeFilter;
import com.food.order.speedzy.Utils.Glide4Engine;
import com.food.order.speedzy.Utils.IntentUtils;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedyLinearLayoutManager;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.root.BaseActivity;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;
import com.zhihu.matisse.listener.OnCheckedListener;
import com.zhihu.matisse.listener.OnSelectedListener;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

public class SpeedzyStoreActivity extends BaseActivity implements MultiplePermissionsListener {

    private Toolbar toolbar;
    MySharedPrefrencesData mySharedPrefrencesData;
    String userid = "", Veg_Flag;
    TextView checkout;
    Context mc;
    RecyclerView recyclerView;
    LinearLayoutManager lm;
    MyListAdapter myCartAdapter;
    TextView asap;
    TextView later;
    TextView change;
    TextView selected_location;
    Dialog dialog_msg;
    TextView msg, no, ok;
    String time_selected="1";

    RecyclerView gvGallery;
    LinearLayout gallery;
    TextView camera;
    int TAKE_PICTURE = 11;
    String imageEncoded;
    File mImageFile;
    private Uri imageToUploadUri;
    private MaterialDialog mPermissionDeniedDialog;
    private static final int REQUEST_CODE_CHOOSE = 23;
    private String mSelectedPictureMode = SpeedzyConstants.SELECTED_PICTURE_MODE_GALLERY;
    GalleryAdapter galleryAdapter;
    private StorageReference storageRef, imageRef;
    private List<String> mReviewImageList;
    File compressedFile;
    File acutalImage;
    private List<Uri> mImageUriList;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speedzy_store);
        mySharedPrefrencesData = new MySharedPrefrencesData();

        Veg_Flag = mySharedPrefrencesData.getVeg_Flag(SpeedzyStoreActivity.this);
        userid = mySharedPrefrencesData.getUser_Id(this);
        mc = SpeedzyStoreActivity.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Veg_Flag.equalsIgnoreCase("1")) {
            statusbar_bg(R.color.red);
        } else {
            statusbar_bg(R.color.red);
        }

        dialog_msg =
                new Dialog(mc, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog_msg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_msg.setContentView(R.layout.logout_popup);
        msg = (TextView) dialog_msg.findViewById(R.id.warning);
        ok = (TextView) dialog_msg.findViewById(R.id.yes);
        no = (TextView) dialog_msg.findViewById(R.id.no);
        ok.setText("Ok");
        no.setVisibility(View.GONE);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialog_msg.dismiss();
            }
        });
        selected_location=findViewById(R.id.selected_location);
        change= findViewById(R.id.change);
        checkout = findViewById(R.id.checkout);
        asap = (TextView) findViewById(R.id.asap);
        later = (TextView) findViewById(R.id.later);

        recyclerView = (RecyclerView) findViewById(R.id.cartlistItems);
        recyclerView.setHasFixedSize(false);
        lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);

        selected_location.setText(mySharedPrefrencesData.getLocation(SpeedzyStoreActivity.this));

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mc, Saved_Addresses.class);
                intent.putExtra("change_address", true);
                startActivityForResult(intent, 1);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
        time_slect_fun();
        asap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    time_selected = "1";
                    time_slect_fun();
            }
        });
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time_selected = "2";
                time_slect_fun();
            }
        });
        myCartAdapter = new MyListAdapter(SpeedzyStoreActivity.this);
        recyclerView.setAdapter(myCartAdapter);
        myCartAdapter.addItem(0);
            checkout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        gallery = (LinearLayout) findViewById(R.id.gallery);
        camera = (TextView) findViewById(R.id.camera);
        initPermissionDialog();
        initComponent();
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedPictureMode = SpeedzyConstants.SELECTED_PICTURE_MODE_GALLERY;
                checkStoragePermisssion();
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedPictureMode = SpeedzyConstants.SELECTED_PICTURE_MODE_CAMERA;
                checkStoragePermisssion();
            }
        });

    }

    private void time_slect_fun() {
        if (time_selected.equalsIgnoreCase("1")) {
            asap.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_checked, 0, 0, 0);
            later.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_unchecked, 0, 0, 0);

        } else {
            later.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_checked, 0, 0, 0);
            asap.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_unchecked, 0, 0, 0);
        }
    }

    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Commons.back_button_transition(SpeedzyStoreActivity.this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(SpeedzyStoreActivity.this);
    }


    @Override public void onStart() {
        super.onStart();
    }

    @Override protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
           if (requestCode == 1 && data != null) {
                String message = data.getStringExtra("SELECTED_LOCATION");
                selected_location.setText(message);

            }
            if (requestCode == REQUEST_CODE_CHOOSE && data != null && resultCode == RESULT_OK) {
                // Get the Image from data
                setData(Matisse.obtainPathResult(data), Matisse.obtainResult(data));
            } else if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
                if (imageToUploadUri != null) {
                    Uri selectedImage = imageToUploadUri;
                    getContentResolver().notifyChange(selectedImage, null);
                    mImageUriList.add(selectedImage);
                    galleryAdapter.refreshData(mImageUriList);
                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            showErrorDialog(e.getMessage());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initComponent() {
        gvGallery = (RecyclerView) findViewById(R.id.recyclerView);
        //gvGallery.setHasFixedSize(true);
        gvGallery.setLayoutManager(
                new SpeedyLinearLayoutManager(SpeedzyStoreActivity.this, LinearLayout.HORIZONTAL, false));
        mReviewImageList = new ArrayList<>();
        mImageUriList = new ArrayList<>();
        //mCameraImageList = new ArrayList<>();
        //mImageList = new ArrayList<>();
        FirebaseStorage storage = FirebaseStorage.getInstance(SpeedzyConstants.FIREBASE_SPEEDZY_BUCKET);
        storageRef = storage.getReference().child(SpeedzyConstants.FIREBASE_MEDICINE)
                .child(SpeedzyConstants.FIREBASE_ORIGINAL);
        galleryAdapter = new GalleryAdapter(SpeedzyStoreActivity.this, new GalleryAdapter.Callback() {
            @Override
            public void onClickDeleteItem(Uri uri) {
                updateAdapter(uri);
            }
        });
        gvGallery.setAdapter(galleryAdapter);
    }

    private void updateAdapter(Uri uri) {
        ListIterator<Uri> iterator = mImageUriList.listIterator();
        while (iterator.hasNext()) {
            if (iterator.next().equals(uri)) {
                iterator.remove();
            }
        }
    }

    private void checkStoragePermisssion() {
        Dexter.withActivity(SpeedzyStoreActivity.this)
                .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(SpeedzyStoreActivity.this).check();
    }
    private void openGallery() {
        if (mSelectedPictureMode.equals(SpeedzyConstants.SELECTED_PICTURE_MODE_GALLERY)) {
            openMatisseGallery();
        } else if (mReviewImageList.size() < 5) {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, getImageURI());
                startActivityForResult(cameraIntent, TAKE_PICTURE);
            }
        } else {
            Toast.makeText(this, "Can select maximum 5 images", Toast.LENGTH_SHORT).show();
        }
    }

    private Uri getImageURI() {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/speedzy_images");
        myDir.mkdirs();
        @SuppressLint("SimpleDateFormat") String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = "Image-" + timeStamp + ".jpg";
        mImageFile = new File(myDir, fname);
        imageToUploadUri = IntentUtils.getImageUri(this, mImageFile);
        return imageToUploadUri;
    }

    private void initPermissionDialog() {
        mPermissionDeniedDialog =
                new MaterialDialog.Builder(this).title(R.string.general_error_permissiondenied)
                        .positiveText(R.string.general_label_gotosetting)
                        .negativeText(R.string.general_label_cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(
                                    @NonNull MaterialDialog dialog,
                                    @NonNull DialogAction which) {
                                //should open settings page to enable permission
                                startActivity(IntentUtils.newSettingsIntent(SpeedzyStoreActivity.this));
                            }
                        })
                        .build();
    }

    @Override
    public void onPermissionsChecked(MultiplePermissionsReport report) {
        if (report.areAllPermissionsGranted()) {
            openGallery();
        } else if (report.isAnyPermissionPermanentlyDenied()) {
            mPermissionDeniedDialog.show();
        }
    }

    @Override
    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
                                                   PermissionToken token) {
        new MaterialDialog.Builder(this).title(R.string.general_error_permissiondenied)
                .content("Require permissions")
                .positiveText(R.string.general_label_ok)
                .negativeText(R.string.general_label_cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(
                            @NonNull MaterialDialog dialog,

                            @NonNull DialogAction which) {
                        //should open settings page to enable permission
                        dialog.dismiss();
                        token.continuePermissionRequest();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog,
                                        @NonNull DialogAction which) {
                        dialog.dismiss();
                        token.cancelPermissionRequest();
                    }
                })
                .build().show();
    }

    private void openMatisseGallery() {
        if (mImageUriList.size() < 5) {
            Matisse.from(this)
                    .choose(MimeType.ofAll(), false)
                    .countable(true)
                    .capture(false)
                    .theme(R.style.Matisse_Zhihu)
                    .captureStrategy(
                            new CaptureStrategy(true, "com.food.order.speedzy.fileprovider"))
                    .maxSelectable(5 - mImageUriList.size())
                    .addFilter(new GifSizeFilter(400, 320, 5 * Filter.K * Filter.K))
                    .gridExpectedSize(
                            getResources().getDimensionPixelSize(R.dimen.size_120))
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                    .thumbnailScale(0.85f)
                    .imageEngine(new Glide4Engine())
                    // for glide-V4
                    .setOnSelectedListener(new OnSelectedListener() {
                        @Override
                        public void onSelected(@NonNull List<Uri> uriList,
                                               @NonNull List<String> pathList) {
                            // DO SOMETHING IMMEDIATELY HERE
                        }
                    })
                    .originalEnable(true)
                    .setOnCheckedListener(new OnCheckedListener() {
                        @Override
                        public void onCheck(boolean isChecked) {
                            // DO SOMETHING IMMEDIATELY HERE
                        }
                    })
                    .forResult(REQUEST_CODE_CHOOSE);
        } else {
            Toast.makeText(this, "Can select maximum 5 images", Toast.LENGTH_SHORT).show();
        }
    }
    private void setData(List<String> obtainPathResult, List<Uri> obtainResult) {
        mImageUriList.addAll(obtainResult);
        galleryAdapter.refreshData(mImageUriList);
    }
}
