package com.food.order.speedzy.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.database.LOcaldbNew;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Sujata Mohanty.
 */


public class ProfileActivityNew extends AppCompatActivity {
    LinearLayout linear_saved,linear_wallet,ll_order,ll_likes_rest,logout,ll_referal;
    MySharedPrefrencesData mySharedPrefrencesData;
    String referralcode;
    LOcaldbNew lOcaldbNew;
    String name_str,mobile_str;
    TextView name,mobile;
    ImageView back,edit_profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_new);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.red ));
        }
        mySharedPrefrencesData = new MySharedPrefrencesData();
        name_str=mySharedPrefrencesData.getFName(this)+" "+mySharedPrefrencesData.getLName(this);
        mobile_str=mySharedPrefrencesData.get_Party_mobile(this);
        referralcode=mySharedPrefrencesData.getReferal(this);
        lOcaldbNew=new LOcaldbNew(this);
        linear_saved=(LinearLayout)findViewById(R.id.ll_saved);
        linear_wallet=(LinearLayout)findViewById(R.id.wallet_linear);
        ll_order=(LinearLayout)findViewById(R.id.ll_order);
        ll_likes_rest=(LinearLayout)findViewById(R.id.ll_likes_rest);
        ll_referal=(LinearLayout)findViewById(R.id.ll_referal);
        logout=(LinearLayout)findViewById(R.id.logout);
        name = (TextView) findViewById(R.id.name);
        mobile = (TextView) findViewById(R.id.mobile);
        back = (ImageView) findViewById(R.id.back);
        edit_profile= (ImageView) findViewById(R.id.edit_profile);

        if (mySharedPrefrencesData.getFName(this).equalsIgnoreCase("")){
            name.setText("Guest");
        }else {
            name.setText(name_str);
        }
        mobile.setText("+91-"+mobile_str);

        ll_referal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT,"Foodinns");
                    String sAux = "Register on Foodins with "+referralcode+" and avail attractive offers.Download on ";
                    sAux = sAux +"https://play.google.com/store/apps/details?id=com.food.order.foodinns&referrer=utm_source="+referralcode;
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "Choose One"));
                } catch(Exception e) {
                    //e.toString();
                }
            }
        });

        linear_saved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProfileActivityNew.this,Saved_Addresses.class);
                intent.putExtra("change_address", false);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
        linear_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProfileActivityNew.this,WalletActivityNew.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
        ll_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProfileActivityNew.this,MyOrderActivity.class);
                intent.putExtra("flag",0);
                intent.putExtra("type",1);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
        ll_likes_rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProfileActivityNew.this,LikedResturantsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialogshow();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commons.back_button_transition(ProfileActivityNew.this);
            }
        });
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProfileActivityNew.this,Edit_Profile.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
    }

    private void dialogshow() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.logout_popup);
        TextView yes = (TextView) dialog.findViewById(R.id.yes);
        TextView no = (TextView) dialog.findViewById(R.id.no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mySharedPrefrencesData.setLoginStatus(ProfileActivityNew.this,false);
                mySharedPrefrencesData.clearAllSharedData(ProfileActivityNew.this);
                mySharedPrefrencesData.cart_clearAllSharedData(ProfileActivityNew.this);
                lOcaldbNew.clearDatabase();
                FirebaseAuth.getInstance().signOut();
                Intent intent=new Intent(ProfileActivityNew.this,SignupSignin_New.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });
        dialog.show();
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Commons.back_button_transition(ProfileActivityNew.this);
    }
    @Override
    public void onResume(){
        super.onResume();
        name_str=mySharedPrefrencesData.getFName(this)+" "+mySharedPrefrencesData.getLName(this);
        mobile_str=mySharedPrefrencesData.get_Party_mobile(this);
        if (mySharedPrefrencesData.getFName(this).equalsIgnoreCase("")){
            name.setText("Guest");
        }else {
            name.setText(name_str);
        }
        mobile.setText("+91-"+mobile_str);
    }
}
