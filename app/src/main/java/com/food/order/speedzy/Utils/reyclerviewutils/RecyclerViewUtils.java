package com.food.order.speedzy.Utils.reyclerviewutils;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.R;

public class RecyclerViewUtils {

  private RecyclerViewUtils() {
  }

  public static RecyclerView.LayoutManager newLinearVerticalLayoutManager(
      @NonNull final Activity context) {
    return new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
  }

  public static RecyclerView.LayoutManager newLinearHorizontalLayoutManager(
      @NonNull final Context context) {
    return new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
  }

  public static RecyclerView.LayoutManager newGridLayoutManager(@NonNull final Context context,
      int spanCount) {
    return new GridLayoutManager(context, spanCount);
  }

  public static RecyclerView.ItemDecoration newDividerItemDecoration(@NonNull final Context context,
      @ColorRes final int color) {
    return new LineDividerItemDecoration(ContextCompat.getColor(context, color),
        context.getResources().getDimensionPixelSize(R.dimen.line_1), false, true);
  }

  public static RecyclerView.ItemDecoration newDividerItemDecoration(@NonNull final Context context,
      @ColorRes final int color, @DimenRes final int dividerSize, boolean topMostDrawn,
      boolean bottomMostDrawn) {
    return new LineDividerItemDecoration(ContextCompat.getColor(context, color),
        context.getResources().getDimensionPixelSize(dividerSize), topMostDrawn, bottomMostDrawn);
  }

  public static RecyclerView.ItemDecoration newHorizontalSpacingItemDecoration(
      @NonNull Context context, @DimenRes final int spacingSizeResId, boolean firstSpacing,
      boolean lastSpacing) {
    return new HorizontalSpacingItemDecoration(
        context.getResources().getDimensionPixelOffset(spacingSizeResId), firstSpacing,
        lastSpacing);
  }

  public static RecyclerView.ItemDecoration newHorizontalSpacingItemDecoration(
      @NonNull Context context, @DimenRes final int spacingSizeResId,
      @DimenRes final int firstSpacingResId, @DimenRes final int lastSpacingResId) {
    return new HorizontalSpacingItemDecoration(
        context.getResources().getDimensionPixelOffset(spacingSizeResId),
        context.getResources().getDimensionPixelOffset(firstSpacingResId),
        context.getResources().getDimensionPixelOffset(lastSpacingResId));
  }

  public static RecyclerView.ItemDecoration newVerticalSpacingItemDecoration(
      @NonNull Context context, @DimenRes final int spacingSizeResId, boolean firstSpacing,
      boolean lastSpacing) {
    return new VerticalSpacingItemDecoration(
        context.getResources().getDimensionPixelOffset(spacingSizeResId), firstSpacing,
        lastSpacing);
  }

  public static RecyclerView.ItemDecoration newVerticalSpacingItemDecoration(
      @NonNull Context context, @DimenRes final int spacingSizeResId,
      @DimenRes final int firstSpacingResId, @DimenRes final int lastSpacingResId) {
    return new VerticalSpacingItemDecoration(
        context.getResources().getDimensionPixelOffset(spacingSizeResId),
        context.getResources().getDimensionPixelOffset(firstSpacingResId),
        context.getResources().getDimensionPixelOffset(lastSpacingResId));
  }

  public static RecyclerView.ItemDecoration newGridSpacingItemDecoration(
      @NonNull final Context context, final int spanCount, @DimenRes final int spacing,
      final boolean includeEdge) {
    return new GridSpacingItemDecoration(spanCount,
        context.getResources().getDimensionPixelOffset(spacing), includeEdge);
  }
}
