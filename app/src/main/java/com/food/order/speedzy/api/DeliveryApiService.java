package com.food.order.speedzy.api;

import com.food.order.speedzy.api.response.createorder.CreateOrderResponse;
import com.food.order.speedzy.api.response.delivery.ListDriverResponse;
import com.food.order.speedzy.api.response.order.OrderStatusResponse;
import io.reactivex.Observable;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface DeliveryApiService {
  @POST("api") Observable<OrderStatusResponse> getOrderStatus(@Query("query") String query);

  @POST("api") Observable<CreateOrderResponse> createOrder(@Query("query") String query);

  @POST("api") Observable<ListDriverResponse> getDeliveryGuyLastKnownLocation(
      @Query("query") String query);
}
