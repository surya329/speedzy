package com.food.order.speedzy.Fragment;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.Activity.RestarantDetails;
import com.food.order.speedzy.Adapter.RestaurantDetailsAdapterMenu;
import com.food.order.speedzy.Model.Dishdetail;
import com.food.order.speedzy.Model.Dishitem;
import com.food.order.speedzy.Model.Restaurant_Dish_Model;
import com.food.order.speedzy.Model.Restaurant_Dish_Model_New;
import com.food.order.speedzy.R;

import java.util.ArrayList;
import java.util.List;

public class MyDialogFragment extends DialogFragment {
    RestaurantDetailsAdapterMenu adapter;
    List<Dishdetail> parentDishArrayList;
    RecyclerView recyclerView;
    ImageView clear,back;
    EditText search_et;
    static RestarantDetails ctx1;
    static ArrayList<Dishitem> parentDishArrayList1;
    static String res_id1,res_name1,res_address1,suburb1,postcode1,pick_del1,delivery_fee1,avg_order_value1,orders_count1,mMenuId1,flg_ac_status1;
    static ArrayList<Restaurant_Dish_Model.NewOffers> offerArrayList1;
    static int i1;
    static boolean additem1;
    static Restaurant_Dish_Model_New.restaurant_details.restcharge restcharge1;
    static String res_image1;
    RelativeLayout nodishlay;
    public static MyDialogFragment newInstance(ArrayList<Dishitem> parentDishArrayList, RestarantDetails ctx, String res_id, String res_name, String res_address, String suburb, String postcode, String pick_del, String delivery_fee, ArrayList<Restaurant_Dish_Model.NewOffers> offerArrayList, String avg_order_value, String orders_count, int i, String mMenuId, boolean additem, String flg_ac_status, Restaurant_Dish_Model_New.restaurant_details.restcharge restcharge, String res_image) {
        MyDialogFragment f = new MyDialogFragment();
        ctx1 = ctx;
       parentDishArrayList1 = parentDishArrayList;
       res_id1=res_id;
        res_name1=res_name;
       res_address1=res_address;
        suburb1=suburb;
        postcode1=postcode;
        pick_del1=pick_del;
        delivery_fee1=delivery_fee;
        offerArrayList1=offerArrayList;
        avg_order_value1=avg_order_value;
        orders_count1=orders_count;
        i1=i;
        mMenuId1=mMenuId;
        additem1=additem;
        flg_ac_status1=flg_ac_status;
        restcharge1=restcharge;
        res_image1=res_image;
        return f;
    }
    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                dismiss();
                ctx1.adapter.notifyDataSetChanged();
            }
        };
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog, container, false);

        recyclerView=v.findViewById(R.id.menu_list);
        back=v.findViewById(R.id.back);
        clear=v.findViewById(R.id.clear);
        search_et=v.findViewById(R.id.search_et);
        nodishlay=v.findViewById(R.id.nodishlay);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        parentDishArrayList=new ArrayList<>();
        for (int i=0;i<parentDishArrayList1.size();i++){
            parentDishArrayList.addAll(parentDishArrayList1.get(i).getDishdetails());
        }
        adapter =new RestaurantDetailsAdapterMenu(null,parentDishArrayList, ctx1,
                res_id1, res_name1, res_address1, suburb1, postcode1, pick_del1,
                delivery_fee1,offerArrayList1,
                avg_order_value1,
                orders_count1,i1,mMenuId1,additem1,flg_ac_status1, restcharge1,res_image1);
        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(View.INVISIBLE);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_et.setText("");
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                ctx1.adapter.notifyDataSetChanged();
            }
        });
        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // filter your list from your input
                if (s.length()>0) {
                    nodishlay.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    filter(s.toString());
                }else {
                    recyclerView.setVisibility(View.GONE);
                    nodishlay.setVisibility(View.VISIBLE);
                }
                //you can use runnable postDelayed like 500 ms to delay search text
            }
        });
        return v;
    }
    void filter(String text){
        ArrayList<Dishdetail> temp = new ArrayList();
        for(Dishdetail d: parentDishArrayList){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getStDishName().toLowerCase().contains(text.toLowerCase())){
                temp.add(d);
            }
        }
        //update recyclerview
        adapter.updateList(temp);
    }

}
