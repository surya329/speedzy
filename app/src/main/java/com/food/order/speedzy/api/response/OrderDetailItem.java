package com.food.order.speedzy.api.response;

import java.io.Serializable;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OrderDetailItem implements Serializable {

	@SerializedName("price")
	private String price;

	@SerializedName("subtotal")
	private String subtotal;

	@SerializedName("dishid")
	private String dishid;

	@SerializedName("qty")
	private String qty;

	@SerializedName("options")
	private Options options;

	@SerializedName("dishname")
	private String dishname;

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setSubtotal(String subtotal){
		this.subtotal = subtotal;
	}

	public String getSubtotal(){
		return subtotal;
	}

	public void setDishid(String dishid){
		this.dishid = dishid;
	}

	public String getDishid(){
		return dishid;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setOptions(Options options){
		this.options = options;
	}

	public Options getOptions(){
		return options;
	}

	public void setDishname(String dishname){
		this.dishname = dishname;
	}

	public String getDishname(){
		return dishname;
	}

	@Override
 	public String toString(){
		return 
			"OrderDetailItem{" + 
			"price = '" + price + '\'' + 
			",subtotal = '" + subtotal + '\'' + 
			",dishid = '" + dishid + '\'' + 
			",qty = '" + qty + '\'' + 
			",options = '" + options + '\'' + 
			",dishname = '" + dishname + '\'' + 
			"}";
		}
}