package com.food.order.speedzy.Model;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.food.order.speedzy.R;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

/**
 * Created by Sujata Mohanty.
 */

public class ChildHolder extends ChildViewHolder {

    public TextView childdishname,child_text,add,price;

    public RecyclerView pricerecycle;
    public LinearLayout linearLayout,quantity_linear;
    public AppCompatImageView  remove_quantity,add_quantity;
    public TextView quantity_text;

    public ChildHolder(View itemView) {
        super(itemView);
        childdishname=(TextView)itemView.findViewById(R.id.txt1);
        child_text=(TextView)itemView.findViewById(R.id.child_text);
        price=(TextView)itemView.findViewById(R.id.txt3);
 //       childspecial=(TextView)itemView.findViewById(R.id.child_text1);
        linearLayout=(LinearLayout)itemView.findViewById(R.id.card_view);
//        pricerecycle=(RecyclerView)itemView.findViewById(R.id.pricerecyclerview);
    }
    public ChildHolder(View itemView, String flag) {
        super(itemView);
        childdishname=(TextView)itemView.findViewById(R.id.txt1);
        child_text=(TextView)itemView.findViewById(R.id.child_text);
        price=(TextView)itemView.findViewById(R.id.txt3);
        //       childspecial=(TextView)itemView.findViewById(R.id.child_text1);
        linearLayout=(LinearLayout)itemView.findViewById(R.id.card_view);
        add=(TextView) itemView.findViewById(R.id.add);
        remove_quantity=(AppCompatImageView) itemView.findViewById(R.id.remove_quantity);
        add_quantity=(AppCompatImageView ) itemView.findViewById(R.id.add_quantity);
        quantity_text=(TextView) itemView.findViewById(R.id.quantity_text);
        quantity_linear=(LinearLayout)itemView.findViewById(R.id.quantity_linear);
//        pricerecycle=(RecyclerView)itemView.findViewById(R.id.pricerecyclerview);
    }


}
