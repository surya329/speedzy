package com.food.order.speedzy.api.response;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.food.order.speedzy.Model.OrderDataModel;
import com.food.order.speedzy.R;

public class OrderVH extends RecyclerView.ViewHolder {
  @BindView(R.id.odrdishname) TextView txtDishName;
  @BindView(R.id.qtyprice) TextView txtPrice;
  @BindView(R.id.order_item_sub_total_tv) TextView order_item_sub_total_tv;

  public OrderVH(@NonNull View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
  }

  public void onBindView(OrderDataModel order_detail) {
    txtDishName.setText(order_detail.getDishName());
    txtPrice.setText(order_detail.getQuantity() + " X " + "₹ " + order_detail.getPrice());
    int sale_price=(int) Float.parseFloat(order_detail.getPrice());
    int subTotal = Integer.parseInt(order_detail.getQuantity()) * sale_price;
    order_item_sub_total_tv.setText(""+subTotal);
  }
}
