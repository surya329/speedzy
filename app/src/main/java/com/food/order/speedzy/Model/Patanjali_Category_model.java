package com.food.order.speedzy.Model;

import com.food.order.speedzy.api.response.grocery.CategoriesItem;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class Patanjali_Category_model  {

  @SerializedName("categories")
  private List<CategoriesItem> categories;

  @SerializedName("status")
  private String status;

  @SerializedName("Banners")
  private List<Object> banners;

  public List<Object> getBanners() {
    return banners;
  }

  public List<CategoriesItem> getCategories() {
    return categories;
  }

  public String getStatus() {
    return status;
  }

  //@SerializedName("status")
  //private String status;
  //
  //public String getStatus() {
  //  return status;
  //}
  //
  //@SerializedName("categories")
  //private List<categories> mCategories;
  //
  //public List<Patanjali_Category_model.categories> getCategories() {
  //  return mCategories;
  //}
  //
  //@Override public String toString() {
  //  return "Patanjali_Category_model{" +
  //      "status='" + status + '\'' +
  //      ", mCategories=" + mCategories +
  //      '}';
  //}
  //
  //public class categories implements Serializable {
  //  @SerializedName("id")
  //  private String id;
  //  @SerializedName("name")
  //  private String name;
  //  @SerializedName("parent")
  //  private String parent;
  //  @SerializedName("image")
  //  private String image;
  //  @SerializedName("sub_categories")
  //  List<sub_categories> mSub_categories;
  //
  //  public boolean mSelected;
  //
  //  public boolean isSelected() {
  //    return mSelected;
  //  }
  //
  //  public void setSelected(boolean selected) {
  //    mSelected = selected;
  //  }
  //
  //  public String getId() {
  //    return id;
  //  }
  //
  //  public void setId(String id) {
  //    this.id = id;
  //  }
  //
  //  public String getName() {
  //    return name;
  //  }
  //
  //  public void setName(String name) {
  //    this.name = name;
  //  }
  //
  //  public String getParent() {
  //    return parent;
  //  }
  //
  //  public void setParent(String parent) {
  //    this.parent = parent;
  //  }
  //
  //  public String getImage() {
  //    return image;
  //  }
  //
  //  public void setImage(String image) {
  //    this.image = image;
  //  }
  //
  //  public List<Patanjali_Category_model.categories.sub_categories> getSub_categories() {
  //    return mSub_categories;
  //  }
  //
  //  @Override public String toString() {
  //    return "categories{" +
  //        "id='" + id + '\'' +
  //        ", name='" + name + '\'' +
  //        ", parent='" + parent + '\'' +
  //        ", image='" + image + '\'' +
  //        ", mSub_categories=" + mSub_categories +
  //        ", mSelected=" + mSelected +
  //        '}';
  //  }
  //
  //  public class sub_categories implements Serializable {
  //    @SerializedName("id")
  //    private String id;
  //    @SerializedName("name")
  //    private String name;
  //    @SerializedName("parent")
  //    private String parent;
  //    @SerializedName("image")
  //    private String image;
  //    @SerializedName("child_subcat")
  //    private List<child_subcat> mChild_subcat;
  //
  //    public String getId() {
  //      return id;
  //    }
  //
  //    public void setId(String id) {
  //      this.id = id;
  //    }
  //
  //    public String getName() {
  //      return name;
  //    }
  //
  //    public void setName(String name) {
  //      this.name = name;
  //    }
  //
  //    public String getParent() {
  //      return parent;
  //    }
  //
  //    public void setParent(String parent) {
  //      this.parent = parent;
  //    }
  //
  //    public String getImage() {
  //      return image;
  //    }
  //
  //    public void setImage(String image) {
  //      this.image = image;
  //    }
  //
  //    public List<Patanjali_Category_model.categories.sub_categories.child_subcat> getChild_subcat() {
  //      return mChild_subcat;
  //    }
  //
  //    public class child_subcat implements Serializable {
  //      @SerializedName("id")
  //      private String id;
  //      @SerializedName("name")
  //      private String name;
  //      @SerializedName("parent")
  //      private String parent;
  //      @SerializedName("image")
  //      private String image;
  //
  //      public String getId() {
  //        return id;
  //      }
  //
  //      public void setId(String id) {
  //        this.id = id;
  //      }
  //
  //      public String getName() {
  //        return name;
  //      }
  //
  //      public void setName(String name) {
  //        this.name = name;
  //      }
  //
  //      public String getParent() {
  //        return parent;
  //      }
  //
  //      public void setParent(String parent) {
  //        this.parent = parent;
  //      }
  //
  //      public String getImage() {
  //        return image;
  //      }
  //
  //      public void setImage(String image) {
  //        this.image = image;
  //      }
  //    }
  //
  //    @Override public String toString() {
  //      return "sub_categories{" +
  //          "id='" + id + '\'' +
  //          ", name='" + name + '\'' +
  //          ", parent='" + parent + '\'' +
  //          ", image='" + image + '\'' +
  //          ", mChild_subcat=" + mChild_subcat +
  //          '}';
  //    }
  //  }
  //}
}

