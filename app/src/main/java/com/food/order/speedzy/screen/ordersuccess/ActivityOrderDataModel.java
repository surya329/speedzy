package com.food.order.speedzy.screen.ordersuccess;

import android.os.Parcel;
import android.os.Parcelable;

public class ActivityOrderDataModel implements Parcelable {
  private String mOrderId;
  private String mFlag;

  public ActivityOrderDataModel() {
  }

  protected ActivityOrderDataModel(Parcel in) {
    mOrderId = in.readString();
    mFlag = in.readString();
  }

  public static final Creator<ActivityOrderDataModel> CREATOR =
      new Creator<ActivityOrderDataModel>() {
        @Override
        public ActivityOrderDataModel createFromParcel(Parcel in) {
          return new ActivityOrderDataModel(in);
        }

        @Override
        public ActivityOrderDataModel[] newArray(int size) {
          return new ActivityOrderDataModel[size];
        }
      };

  public String getFlag() {
    return mFlag;
  }

  public void setFlag(String flag) {
    mFlag = flag;
  }

  public String getOrderId() {
    return mOrderId;
  }

  public void setOrderId(String orderId) {
    mOrderId = orderId;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(mOrderId);
    parcel.writeString(mFlag);
  }
}
