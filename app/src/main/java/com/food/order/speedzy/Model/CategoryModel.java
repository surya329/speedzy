package com.food.order.speedzy.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CategoryModel implements Serializable {
    String status;
    List<cat> cat=null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CategoryModel.cat> getCat() {
        return cat;
    }

    public void setCat(List<CategoryModel.cat> cat) {
        this.cat = cat;
    }

    public class cat implements Serializable {
        public boolean isIsclicked() {
            return isclicked;
        }

        public void setIsclicked(boolean isclicked) {
            this.isclicked = isclicked;
        }

        public boolean isclicked=false;
        String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        String name;
    }
}
