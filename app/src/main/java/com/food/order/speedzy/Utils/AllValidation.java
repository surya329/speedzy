package com.food.order.speedzy.Utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sujata Mohanty.
 */

public class AllValidation {

    public static Dialog dialog;
    public static TextView dis;
    public static TextView warn;
    public static TextView msg;

    public static boolean getRegisterValidate(String fname, String lname, String email, String mobile, String password, final Context mContext){
        dialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
        dis=(TextView) dialog.findViewById(R.id.dismiss);
        warn=(TextView)dialog.findViewById(R.id.warning);
        msg=(TextView)dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        boolean flag=false;
        if(!fname.equalsIgnoreCase("")){
            if(!lname.equalsIgnoreCase("")){
                if(!email.equalsIgnoreCase("")){
                    if(validate(email)){
                        if(!password.equalsIgnoreCase("")){
                            if(!mobile.equalsIgnoreCase("")){
                                if(validatephonenum(mobile)&& mobile.length()==10){
                                    flag=true;
                                    }else{
                                            msg.setText("enter 10 digit valid mobile no.!");
                                            dialog.show();

                                        }
                                    }else{
                                        msg.setText("plesae enter mobile no.");
                                        dialog.show();

                                    }

                                }else{
                                    msg.setText("please enter password");
                                    dialog.show();

                                }

                            }else{
                                msg.setText("invalid email id");
                                dialog.show();

                            }
                        }else{
                            msg.setText("please enter email id!");
                            dialog.show();

                    }
            }else{
                msg.setText("please enter last name");
                dialog.show();
            }
        }else{
            msg.setText("please enter first name");
            dialog.show();

        }

        return flag;
    }


    public static boolean validate(final String hex) {
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher= pattern.matcher(hex);
        return matcher.matches();

    }

    public static boolean validatephonenum(final String hex) {
       // final String EMAIL_PATTERN ="((\\+*)((0[ -]+)*|(91 )*)(\\d{12}+|\\d{10}+))|\\d{5}([- ]*)" +"\\d{6}";
        /*final String ph="^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$";
        Pattern pattern = Pattern.compile(ph);
        Matcher matcher= pattern.matcher(hex);
        return matcher.matches();*/
        boolean flag=false;
        if(hex.length()==10){
            flag=true;
        }
        return flag;

    }

    public static boolean emptyField(final String str) {
        boolean flag=false;
        if(str.length()>3){
            flag=true;
        }
        return flag;

    }


    public static void myToast(Context mContext, String msg){
        try {
            Toast.makeText(mContext,msg, Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static boolean loginValidate(String email, String pass, Context mContext) {

        dialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
        dis=(TextView) dialog.findViewById(R.id.dismiss);
        warn=(TextView)dialog.findViewById(R.id.warning);
        msg=(TextView)dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if(!email.equalsIgnoreCase("")){
            if(!pass.equalsIgnoreCase("")){
                return true;
            }else{

                msg.setText("Please enter password");
                dialog.show();

            }
        }else{
            msg.setText("Please enter email id");
            dialog.show();

        }
        return false;
    }


    public static boolean addaddressvalidate(String email, String phoneno, Context mContext) {
        if(!email.equalsIgnoreCase("")){
            if(!phoneno.equalsIgnoreCase("")&& phoneno.length()<11){
                return true;
            }else{
                myToast(mContext,"Please enter 10 digit phone number");
            }
        }else{
            myToast(mContext,"Please enter email id");
        }
        return false;
    }
    public static boolean addaddressvalidate_train(String fname, String lname, String stname, String email, String phoneno, Context mContext) {
        dialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
        dis=(TextView) dialog.findViewById(R.id.dismiss);
        warn=(TextView)dialog.findViewById(R.id.warning);
        msg=(TextView)dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if (!fname.equalsIgnoreCase("")) {
            if (!lname.equalsIgnoreCase("")) {
               // if (!strtnum.equalsIgnoreCase("")) {
                    if (!stname.equalsIgnoreCase("")) {
                        if (!email.equalsIgnoreCase("")) {
                            if (validatephonenum(phoneno)&& !phoneno.equalsIgnoreCase("") && phoneno.length() != 11) {

                                    return true;

                            } else {
                                msg.setText( "Please enter 10 digit phone number");
                                dialog.show();

                            }
                        } else {
                            msg.setText("Please enter email id");
                            dialog.show();

                        }
                    } else {
                        msg.setText("Please enter seat number ");
                        dialog.show();

                    }
               /* } else {
                    msg.setText("Please enter company name");
                    dialog.show();

                }*/
            } else {
                msg.setText("Please enter train number");
                dialog.show();

            }
        } else {
            msg.setText("Please enter passenger name ");
            dialog.show();

        }

        return false;
    }
    public static boolean addaddressvalidate1(String fname, String lname, String stname, String email, String phoneno, Context mContext) {
        dialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
       dis=(TextView) dialog.findViewById(R.id.dismiss);
        warn=(TextView)dialog.findViewById(R.id.warning);
        msg=(TextView)dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if (!fname.equalsIgnoreCase("")) {
            if (!lname.equalsIgnoreCase("")) {
               // if (!strtnum.equalsIgnoreCase("")) {
                    if (!stname.equalsIgnoreCase("")) {
                        if (!email.equalsIgnoreCase("")) {
                            if (validatephonenum(phoneno)&&!phoneno.equalsIgnoreCase("") && phoneno.length()== 10) {

                                    return true;

                            } else {
                                msg.setText("Please enter 10 digit valid phone number");
                                dialog.show();

                            }
                        } else {
                            msg.setText("Please enter email id");
                            dialog.show();

                        }
                    } else {
                        msg.setText("Please enter Address");
                        dialog.show();

                    }
                /*} else {
                    msg.setText("Please enter street number ");
                    dialog.show();

                }*/
            } else {
                msg.setText("Please enter last name");
                dialog.show();

            }
        } else {
            msg.setText("Please enter first name ");
            dialog.show();

        }

        return false;
    }

    public static boolean emptyFieldValidation(String field, String mag, Context context){
        dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
       dis=(TextView) dialog.findViewById(R.id.dismiss);
        warn=(TextView)dialog.findViewById(R.id.warning);
        msg=(TextView)dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        boolean flag=false;
        if(!field.equalsIgnoreCase("")){
            flag=true;
        }else{
            if(field.equalsIgnoreCase("")) {
                msg.setText("please enter " + mag + "");
                dialog.show();
            }else if(validatephonenum(field)&& !field.equalsIgnoreCase("") && field.length()== 10){

            }

        }
        return flag;
    }
    public static boolean emptyFieldValidation1(String field, String mag, int flag1, Context context){
        dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
       dis=(TextView) dialog.findViewById(R.id.dismiss);
        warn=(TextView)dialog.findViewById(R.id.warning);
        msg=(TextView)dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        boolean flag=false;
        if(flag1==2){
            if(!field.equalsIgnoreCase("")){
                if(validate(field)){
                    flag=true;
                }else{
                    msg.setText("invalid email id");
                    dialog.show();

                }
            }else{
                msg.setText("please enter email id!");
                dialog.show();

            }
        }else{
            if(!field.equalsIgnoreCase("")){
                if(validatephonenum(field)&& field.length()==10){
                    flag=true;
                }else{
                    msg.setText("invalid Phone no");
                    dialog.show();

                }
            }else{
                msg.setText("please enter Phone no");
                dialog.show();

            }
        }
        return flag;
    }
    public static boolean book_table_validate(String name, String email, String phone, String person, String date, String time, String occasion1, String budget1, Context mContext) {
        dialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
        dis=(TextView) dialog.findViewById(R.id.dismiss);
        warn=(TextView)dialog.findViewById(R.id.warning);
        msg=(TextView)dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if(!name.equalsIgnoreCase("")){
            if(!email.equalsIgnoreCase("") && validate(email)){
                if(validatephonenum(phone)&& !phone.equalsIgnoreCase("") && phone.length()== 10){
                    if(!person.equalsIgnoreCase("")){
                        if (!date.equalsIgnoreCase("")){
                            if(!time.equalsIgnoreCase("")&& time.contains(":")){
                                if(!occasion1.equalsIgnoreCase("")){
                                    if(!budget1.equalsIgnoreCase("")){
                                        return true;
                                    }else {
                                        msg.setText("Please enter budget");
                                        dialog.show();
                                    }
                                }else {
                                    msg.setText("Please enter occasion");
                                    dialog.show();
                                }
                            }else {
                                msg.setText("Please select time");
                                dialog.show();
                            }
                        }else {
                            msg.setText("Please select date");
                            dialog.show();

                        }
                    }else {
                        msg.setText("Please enter no.of guests");
                        dialog.show();

                    }
                }else {
                    msg.setText("Please enter valid phone number ");
                    dialog.show();

                }
            }else {
                msg.setText("Please enter an valid email");
                dialog.show();

            }
        }else {
            msg.setText("Please enter name ");
            dialog.show();

        }
        return false;
    }

    public static boolean emptyFieldValidation2(String mobile, String email, Context context) {
        dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
        dis = (Button) dialog.findViewById(R.id.dismiss);
        warn = (TextView) dialog.findViewById(R.id.warning);
        msg = (TextView) dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        boolean flag=false;
        if(!mobile.equalsIgnoreCase("")){
            if(validatephonenum(mobile)&& mobile.length()==10){
                    if(!email.equalsIgnoreCase("")){
                        if(validate(email)){
                            flag=true;
                        }else{
                            msg.setText("invalid email id");
                            dialog.show();

                        }
                    }else{
                        msg.setText("please enter email id!");
                        dialog.show();

                    }

            }else{
                msg.setText("enter 10 digit valid mobile no.!");
                dialog.show();

            }
        }else{
            msg.setText("plesae enter mobile no.");
            dialog.show();
        }
        return flag;
    }
    public static boolean book_all_submit(String name, String phone, String occasion, String date, String time, Context mContext) {
        dialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
       dis=(TextView) dialog.findViewById(R.id.dismiss);
        warn=(TextView)dialog.findViewById(R.id.warning);
        msg=(TextView)dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if(!name.equalsIgnoreCase("")){
                if(validatephonenum(phone)&& !phone.equalsIgnoreCase("") && phone.length()== 10) {
                    if (!occasion.equalsIgnoreCase("") && !occasion.equalsIgnoreCase("Select Occasion")) {
                        if (!date.equalsIgnoreCase("")) {
                            if (!time.equalsIgnoreCase("") && time.contains(":")) {
                                return true;
                                /*if (!budget.equalsIgnoreCase("")) {
                                    if(Integer.valueOf(budget)>10000){*/
                               /* if (!comment.equalsIgnoreCase("")) {
                                    return true;
                                } else {
                                    msg.setText("Please enter Comment");
                                    dialog.show();
                                }*/
                                    /*} else {
                                        msg.setText("Please enter budget Should be more than Rs.10,000");
                                        dialog.show();
                                    }

                                } else {
                                    msg.setText("Please enter budget");
                                    dialog.show();
                                }*/
                            } else {
                                msg.setText("Please select time");
                                dialog.show();

                            }
                        } else {
                            msg.setText("Please select date");
                            dialog.show();

                        }
                    } else {
                        msg.setText("Select Occasion");
                        dialog.show();
                    }
                }else {
                    msg.setText("Please enter valid phone number ");
                    dialog.show();

                }
            }else {
            msg.setText("Please enter name ");
            dialog.show();

        }
        return false;
    }
    public static boolean book_all_submit1(String name, String phone, String occasion, String person, String menu, String date, String time, Context mContext) {
        dialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
       dis=(TextView) dialog.findViewById(R.id.dismiss);
        warn=(TextView)dialog.findViewById(R.id.warning);
        msg=(TextView)dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if(!name.equalsIgnoreCase("")){
            if(validatephonenum(phone)&& !phone.equalsIgnoreCase("") && phone.length()== 10) {
                if (!occasion.equalsIgnoreCase("") && !occasion.equalsIgnoreCase("Select Occasion")) {
                    if (!person.equalsIgnoreCase("")) {
                        if (!menu.equalsIgnoreCase("") && !menu.equalsIgnoreCase("Select menu")) {
                    if (!date.equalsIgnoreCase("")) {
                        if (!time.equalsIgnoreCase("") && time.contains(":")) {
                            return true;
                                /*if (!budget.equalsIgnoreCase("")) {
                                    if(Integer.valueOf(budget)>10000){*/
                               /* if (!comment.equalsIgnoreCase("")) {
                                    return true;
                                } else {
                                    msg.setText("Please enter Comment");
                                    dialog.show();
                                }*/
                                    /*} else {
                                        msg.setText("Please enter budget Should be more than Rs.10,000");
                                        dialog.show();
                                    }

                                } else {
                                    msg.setText("Please enter budget");
                                    dialog.show();
                                }*/
                        } else {
                            msg.setText("Please select time");
                            dialog.show();

                        }
                    } else {
                        msg.setText("Please select date");
                        dialog.show();

                    }
                        } else {
                            msg.setText("Select Menu");
                            dialog.show();
                        }
                    } else {
                        msg.setText("Enter no.of person");
                        dialog.show();
                    }
                } else {
                    msg.setText("Select Occasion");
                    dialog.show();
                }
            }else {
                msg.setText("Please enter valid phone number ");
                dialog.show();

            }
        }else {
            msg.setText("Please enter name ");
            dialog.show();

        }
        return false;
    }

}
