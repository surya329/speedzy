package com.food.order.speedzy.Activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.food.order.speedzy.Adapter.Saved_Addresses_Adapter;
import com.food.order.speedzy.Model.UserAddressModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.SpeedzyRide.PlacesAutoCompleteAdapter;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.IntentUtils;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.features.uploadgrocery.UploadGroceryActivity;
import com.food.order.speedzy.root.BaseActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.food.order.speedzy.Activity.AddAddresses.MY_PERMISSIONS_REQUEST_LOCATION;

/**
 * Created by Sujata Mohanty.
 */


public class Saved_Addresses extends BaseActivity implements  PlacesAutoCompleteAdapter.ClickListener, MultiplePermissionsListener {
    private Toolbar toolbar;
    TextView current_address,add_address_manual;
    MySharedPrefrencesData mySharedPrefrencesData;
    String Veg_Flag,userid="",location="";
    RecyclerView saved_adds_recyclerview;
    LinearLayoutManager llm2;
    Saved_Addresses_Adapter savedAddressAdapter;
    ArrayList<UserAddressModel.info> savedAddressList = new ArrayList<>();
    private View viewToAttachDisplayerTo;
    boolean network_status=true;
    ProgressBar progressBar1;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private RecyclerView recyclerView;
    ImageView mClear;
    EditText mSearchEdittext;
    Context context;
    RelativeLayout search_layout;
    TextView text;
    boolean change_address=false;
    private MaterialDialog mPermissionDeniedDialog;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved__addresses);

        context=Saved_Addresses.this;
        viewToAttachDisplayerTo = findViewById(R.id.displayerAttachableView);

        change_address = getIntent().getBooleanExtra("change_address", false);

        mySharedPrefrencesData=new MySharedPrefrencesData();
        location=mySharedPrefrencesData.getLocation(this);
        Veg_Flag=mySharedPrefrencesData.getVeg_Flag(this);
        userid = mySharedPrefrencesData.getUser_Id(this);

        search_layout=findViewById(R.id.search_layout);
        mClear = (ImageView)findViewById(R.id.clear);
        recyclerView = (RecyclerView) findViewById(R.id.list_search);
        mSearchEdittext =findViewById(R.id.search_et);
        text =findViewById(R.id.text);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Veg_Flag.equalsIgnoreCase("1")){
            statusbar_bg(R.color.red);
        }else {
            statusbar_bg(R.color.red);
        }
        if (location.equalsIgnoreCase("") || location.equalsIgnoreCase(null)) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        progressBar1=(ProgressBar) findViewById(R.id.progressBar1);
        current_address=findViewById(R.id.current_address);
        add_address_manual=findViewById(R.id.add_address_manual);

        initPermissionDialog();

        add_address_manual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_address_manual_click();
            }
        });
        current_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLocationPermission();
            }
        });
        if (!mySharedPrefrencesData.getMapsApiKey(Saved_Addresses.this).equalsIgnoreCase("")) {
            Places.initialize(this, mySharedPrefrencesData.getMapsApiKey(Saved_Addresses.this));
            search_layout.setVisibility(View.VISIBLE);
            mSearchEdittext.addTextChangedListener(filterTextWatcher);
            mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            mAutoCompleteAdapter.setClickListener(this);
            recyclerView.setAdapter(mAutoCompleteAdapter);
            mAutoCompleteAdapter.notifyDataSetChanged();
        }else {
            search_layout.setVisibility(View.GONE);
        }
        mClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchEdittext.setText("");
            }
        });
    }

    private void add_address_manual_click() {
        Intent intent=new Intent(Saved_Addresses.this,AddAddresses_Manual.class);
        startActivityForResult(intent, 3);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
    public void checkLocationPermission() {
        Dexter.withActivity(Saved_Addresses.this)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(Saved_Addresses.this).check();
    }

    private void add_address_activity_call(String address,String lat,String longi, int i) {
        Intent intent=new Intent(Saved_Addresses.this,AddAddresses.class);
        intent.putExtra("change_address",false);
        intent.putExtra("locality",address);
        intent.putExtra("latitude",lat);
        intent.putExtra("longitude",longi);
        intent.putExtra("flag",String.valueOf(i));
        startActivityForResult(intent, 2);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            mClear.setVisibility(View.VISIBLE);
            if (!s.toString().equals("")) {
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                if (recyclerView.getVisibility() == View.GONE) {recyclerView.setVisibility(View.VISIBLE);}
            } else {
                if (recyclerView.getVisibility() == View.VISIBLE) {recyclerView.setVisibility(View.GONE);}
            }
        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        public void onTextChanged(CharSequence s, int start, int before, int count) { }
    };

    @Override
    public void click(Place place) {
        mSearchEdittext.setText(place.getAddress());
        mClear.setVisibility(View.VISIBLE);
        LatLng latLng=place.getLatLng();
        add_address_activity_call(place.getAddress(),String.valueOf(latLng.latitude),String.valueOf(latLng.longitude),1);
        recyclerView.setVisibility(View.GONE);
    }
    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,color ));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Commons.back_button_transition(Saved_Addresses.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Commons.back_button_transition(Saved_Addresses.this);
    }
   @Override
    public void onStart() {
        super.onStart();
       callapi_get_address();
    }

    private void callapi_get_address() {
        saved_adds_recyclerview = (RecyclerView)findViewById(R.id.saved_adds_recyclerview);
        progressBar1.setVisibility(View.VISIBLE);
        saved_adds_recyclerview.setVisibility(View.GONE);
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<UserAddressModel> call = methods.get_address(userid);
        Log.d("url","url="+call.request().url().toString());
        call.enqueue(new Callback<UserAddressModel>() {
            @Override
            public void onResponse(Call<UserAddressModel> call, Response<UserAddressModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                UserAddressModel response1 = response.body();
                savedAddressList.clear();
                if (response1.getStatus().equalsIgnoreCase("Success")) {
                    savedAddressList.addAll(response1.getInfo_list());
                    Collections.reverse(savedAddressList);

                    saved_adds_recyclerview.setHasFixedSize(true);
                    llm2 = new LinearLayoutManager(Saved_Addresses.this);
                    saved_adds_recyclerview.setLayoutManager(llm2);
                    savedAddressAdapter = new Saved_Addresses_Adapter(Saved_Addresses.this,savedAddressList);
                    saved_adds_recyclerview.setAdapter(savedAddressAdapter);
                    savedAddressAdapter.setOnItemClickListener(
                            new Saved_Addresses_Adapter.OnItemClickListener() {
                                public void onItemClick(String type, String cityid, String selected_location,
                                                        String longi,
                                                        String lati, String address_id, String name, String area, String pincode,
                                                        String address_type, String landmark) {
                                    home_Activity_Intent(type, cityid, longi, lati, address_id, selected_location,
                                            name,
                                            area, pincode, address_type, landmark);
                                }
                            });
                    progressBar1.setVisibility(View.GONE);
                    if (savedAddressList.isEmpty()){
                        text.setVisibility(View.GONE);
                    }else {
                        text.setVisibility(View.VISIBLE);
                        saved_adds_recyclerview.setVisibility(View.VISIBLE);
                    }
                }
            }
            @Override
            public void onFailure(Call<UserAddressModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void home_Activity_Intent(String type, String city_selected, String longitude_selected,
                                      String latitude_selected, String addressid_selected, String selected_location, String name,
                                      String area, String pincode, String address_type, String landmark) {
        if (change_address){
            mySharedPrefrencesData.setLocation(context, selected_location);
            if (city_selected!=null &&
                    !city_selected.equalsIgnoreCase("")&&
                    !city_selected.equalsIgnoreCase("NA")) {
                mySharedPrefrencesData.setSelectCity(context, city_selected);
            }else {
                mySharedPrefrencesData.setSelectCity(context, "FOODCITYBAM");
            }
            mySharedPrefrencesData.setSelectCity_latitude(context, latitude_selected);
            mySharedPrefrencesData.setSelectCity_longitude(context, longitude_selected);
            mySharedPrefrencesData.setSelectAddress_ID(context, addressid_selected);
            mySharedPrefrencesData.setSelectName(context, name);
            mySharedPrefrencesData.setSelectArea(context, area);
            mySharedPrefrencesData.setSelectMobile(context,
                    mySharedPrefrencesData.get_Party_mobile(this));
            mySharedPrefrencesData.setSelectPincode(context, pincode);
            mySharedPrefrencesData.setSelectAddresstype(context, address_type);
            mySharedPrefrencesData.setSelectLandMark(context, landmark);
            Intent intent = new Intent();
            intent.putExtra("SELECTED_LOCATION", selected_location);
            intent.putExtra("type", type);
            intent.putExtra("lati", latitude_selected);
            intent.putExtra("longi", longitude_selected);
            setResult(1, intent);
            Commons.back_button_transition(Saved_Addresses.this);
        }else if (mySharedPrefrencesData.getLocation(context) == null || mySharedPrefrencesData.getLocation(
                this).equalsIgnoreCase("")) {
            mySharedPrefrencesData.setLocation(context, selected_location);
            if (city_selected!=null &&
                    !city_selected.equalsIgnoreCase("")&&
                    !city_selected.equalsIgnoreCase("NA")) {
                mySharedPrefrencesData.setSelectCity(context, city_selected);
            }else {
                mySharedPrefrencesData.setSelectCity(context, "FOODCITYBAM");
            }
            mySharedPrefrencesData.setSelectCity_latitude(context, latitude_selected);
            mySharedPrefrencesData.setSelectCity_longitude(context, longitude_selected);
            mySharedPrefrencesData.setSelectAddress_ID(context, addressid_selected);
            mySharedPrefrencesData.setSelectArea(context, area);
            mySharedPrefrencesData.setSelectName(context, name);
            mySharedPrefrencesData.setSelectMobile(context,mySharedPrefrencesData.get_Party_mobile(this));
            mySharedPrefrencesData.setSelectPincode(context, pincode);
            mySharedPrefrencesData.setSelectAddresstype(context, address_type);
            mySharedPrefrencesData.setSelectLandMark(context, landmark);
            Intent intent = new Intent(context, HomeActivityNew.class);
            intent.putExtra("flag", 1);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }else {
            mySharedPrefrencesData.setLocation(context, selected_location);
            if (city_selected!=null &&
                    !city_selected.equalsIgnoreCase("")&&
                    !city_selected.equalsIgnoreCase("NA")) {
                mySharedPrefrencesData.setSelectCity(context, city_selected);
            }else {
                mySharedPrefrencesData.setSelectCity(context, "FOODCITYBAM");
            }
            mySharedPrefrencesData.setSelectCity_latitude(context, latitude_selected);
            mySharedPrefrencesData.setSelectCity_longitude(context, longitude_selected);
            mySharedPrefrencesData.setSelectAddress_ID(context, addressid_selected);
            mySharedPrefrencesData.setSelectName(context, name);
            mySharedPrefrencesData.setSelectArea(context, area);
            mySharedPrefrencesData.setSelectMobile(context,
                    mySharedPrefrencesData.get_Party_mobile(this));
            mySharedPrefrencesData.setSelectPincode(context, pincode);
            mySharedPrefrencesData.setSelectAddresstype(context, address_type);
            mySharedPrefrencesData.setSelectLandMark(context, landmark);
            Commons.back_button_transition(Saved_Addresses.this);
        }
        finish();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            if (requestCode == 1 && data != null) {
               // String message = data.getStringExtra("SELECTED_LOCATION");
                callapi_get_address();
            }else {
                if (data!=null) {
                    String type = data.getStringExtra("type");
                    String message = data.getStringExtra("ADD_ADDRESS");
                    String city_selected=data.getStringExtra("city_selected");
                    String longitude_selected=data.getStringExtra("longitude_selected");
                    String latitude_selected=data.getStringExtra("latitude_selected");
                    String addressid_selected=data.getStringExtra("addressid_selected");
                    String name=data.getStringExtra("name");
                    String area=data.getStringExtra("area");
                    String pincode=data.getStringExtra("pincode");
                    String address_type=data.getStringExtra("address_type");
                    String landmark=data.getStringExtra("landmark");
                    home_Activity_Intent(type,city_selected,longitude_selected,latitude_selected,addressid_selected,message, name,area,
                            pincode, address_type, landmark);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initPermissionDialog() {
        mPermissionDeniedDialog =
                new MaterialDialog.Builder(this).title(R.string.general_error_permissiondenied)
                        .positiveText(R.string.general_label_gotosetting)
                        .negativeText(R.string.general_label_cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(
                                    @NonNull MaterialDialog dialog,
                                    @NonNull DialogAction which) {
                                //should open settings page to enable permission
                                startActivity(IntentUtils.newSettingsIntent(Saved_Addresses.this));
                            }
                        })
                        .build();
    }

    @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
        if (report.areAllPermissionsGranted()) {
            add_address_activity_call("","","",0);
        } else if (report.isAnyPermissionPermanentlyDenied()) {
            mPermissionDeniedDialog.show();
        }
    }

    @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
                                                             PermissionToken token) {
        new MaterialDialog.Builder(this).title(R.string.general_error_permissiondenied)
                .content("Require permissions")
                .positiveText(R.string.general_label_ok)
                .negativeText(R.string.general_label_cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(
                            @NonNull MaterialDialog dialog,

                            @NonNull DialogAction which) {
                        //should open settings page to enable permission
                        dialog.dismiss();
                        token.continuePermissionRequest();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override public void onClick(@NonNull MaterialDialog dialog,
                                                  @NonNull DialogAction which) {
                        dialog.dismiss();
                        token.cancelPermissionRequest();
                    }
                })
                .build().show();
    }
}
