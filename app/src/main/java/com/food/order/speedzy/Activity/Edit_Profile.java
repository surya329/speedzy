package com.food.order.speedzy.Activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.Model.Feedbackmodel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.AllValidation;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_Profile extends AppCompatActivity {
    String userid = "", Veg_Flag;
    Toolbar toolbar;
    EditText fstname, lastname, email, mobilenum;
    MySharedPrefrencesData mySharedPrefrencesData;
    TextView update;
    Dialog dialog;
    TextView dis;
    TextView warn, msg;
    LoaderDiloag loaderDiloag;
    AllValidation allValidation;
    ProgressBar progressbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__profile);

        allValidation = new AllValidation();
        loaderDiloag = new LoaderDiloag(this);
        mySharedPrefrencesData = new MySharedPrefrencesData();
        Veg_Flag = mySharedPrefrencesData.getVeg_Flag(Edit_Profile.this);
        userid = mySharedPrefrencesData.getUser_Id(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Veg_Flag.equalsIgnoreCase("1")) {
            statusbar_bg(R.color.red);
        } else {
            statusbar_bg(R.color.red);
        }
        dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
        dis = (TextView) dialog.findViewById(R.id.dismiss);
        warn = (TextView) dialog.findViewById(R.id.warning);
        msg = (TextView) dialog.findViewById(R.id.message);
        warn.setText("Info!!");
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        fstname = (EditText) findViewById(R.id.firstname);
        lastname = (EditText) findViewById(R.id.lastname);
        email = (EditText) findViewById(R.id.email);
        mobilenum = (EditText) findViewById(R.id.mobileno);
        update = (TextView) findViewById(R.id.update);

        progressbarLayout = (ProgressBar) findViewById(R.id.progressbarLayout);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allValidation.validatephonenum(mobilenum.getText().toString())) {
                    if (allValidation.emptyField(fstname.getText().toString())) {
                        progressbarLayout.setVisibility(View.VISIBLE);
                        callapi();
                    } else {
                        allValidation.myToast(Edit_Profile.this, "please provide first name!");
                    }

                } else {
                    allValidation.myToast(Edit_Profile.this, "mobile number is not valid!");
                }

            }
        });
        fstname.setText(mySharedPrefrencesData.getFName(Edit_Profile.this));
        lastname.setText(mySharedPrefrencesData.getLName(Edit_Profile.this));
        email.setText(mySharedPrefrencesData.getPartyemail(Edit_Profile.this));
        mobilenum.setText(mySharedPrefrencesData.get_Party_mobile(Edit_Profile.this));
        email.setEnabled(false);
    }

    private void callapi() {

        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(Edit_Profile.this).create(Apimethods.class);

        Call<Feedbackmodel> call = methods.setUpdte(userid, fstname.getText().toString(), lastname.getText().toString(), mobilenum.getText().toString());
        Log.d("url", "url=" + call.request().url().toString());
        loaderDiloag.dismissDiloag();
        call.enqueue(new Callback<Feedbackmodel>() {
            @Override
            public void onResponse(Call<Feedbackmodel> call, Response<Feedbackmodel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + statusCode);
                Log.d("respones", "" + response);
                Feedbackmodel feedback = response.body();
                if (feedback.getStatus().equalsIgnoreCase("Success")) {
                    loaderDiloag.dismissDiloag();
                    mySharedPrefrencesData.setFName(Edit_Profile.this, fstname.getText().toString());
                    mySharedPrefrencesData.setLName(Edit_Profile.this, lastname.getText().toString());
                    mySharedPrefrencesData.set_Party_mobile(Edit_Profile.this, mobilenum.getText().toString());
                    msg.setText(feedback.getMsg().toString());
                    dialog.show();
                } else {
                    loaderDiloag.dismissDiloag();
                    msg.setText("Updated unsuccessfully");
                    dialog.show();
                }
                progressbarLayout.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Feedbackmodel> call, Throwable t) {
                loaderDiloag.dismissDiloag();
                progressbarLayout.setVisibility(View.GONE);
                Toast.makeText(Edit_Profile.this, "internet not available..connect internet", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Commons.back_button_transition(Edit_Profile.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(Edit_Profile.this);
    }
}
