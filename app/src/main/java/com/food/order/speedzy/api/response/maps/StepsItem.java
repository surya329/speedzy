package com.food.order.speedzy.api.response.maps;

import com.google.gson.annotations.SerializedName;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class StepsItem{

	@SerializedName("duration")
	private Duration duration;

	@SerializedName("start_location")
	private StartLocation startLocation;

	@SerializedName("distance")
	private Distance distance;

	@SerializedName("travel_mode")
	private String travelMode;

	@SerializedName("html_instructions")
	private String htmlInstructions;

	@SerializedName("end_location")
	private EndLocation endLocation;

	@SerializedName("maneuver")
	private String maneuver;

	@SerializedName("polyline")
	private Polyline polyline;

	public Duration getDuration(){
		return duration;
	}

	public StartLocation getStartLocation(){
		return startLocation;
	}

	public Distance getDistance(){
		return distance;
	}

	public String getTravelMode(){
		return travelMode;
	}

	public String getHtmlInstructions(){
		return htmlInstructions;
	}

	public EndLocation getEndLocation(){
		return endLocation;
	}

	public String getManeuver(){
		return maneuver;
	}

	public Polyline getPolyline(){
		return polyline;
	}

	@Override
 	public String toString(){
		return 
			"StepsItem{" + 
			"duration = '" + duration + '\'' + 
			",start_location = '" + startLocation + '\'' + 
			",distance = '" + distance + '\'' + 
			",travel_mode = '" + travelMode + '\'' + 
			",html_instructions = '" + htmlInstructions + '\'' + 
			",end_location = '" + endLocation + '\'' + 
			",maneuver = '" + maneuver + '\'' + 
			",polyline = '" + polyline + '\'' + 
			"}";
		}
}