package com.food.order.speedzy.SpeedzyRide;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.R;
import com.food.order.speedzy.RoomBooking.HotelInnListModelClass;
import com.food.order.speedzy.RoomBooking.UpcomingRecycleAdapter;

import java.util.List;

public class RideRecycleAdapter extends RecyclerView.Adapter<RideRecycleAdapter.MyViewHolder>{

    Context context;
    public static int sCorner = 30;

    private List<HotelInnListModelClass> OfferList;


    public class MyViewHolder extends RecyclerView.ViewHolder {



        TextView title,vehicle_name;
        ImageView image;

        public MyViewHolder(View view) {
            super(view);

            title=(TextView)view.findViewById(R.id.title);
            vehicle_name=view.findViewById(R.id.vehicle_name);
            image=view.findViewById(R.id.image);
        }

    }


    public RideRecycleAdapter(Context context, List<HotelInnListModelClass> offerList) {
        this.OfferList = offerList;
        this.context = context;
    }

    @Override
    public RideRecycleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ride_list, parent, false);


        return new RideRecycleAdapter.MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull RideRecycleAdapter.MyViewHolder holder, final  int position) {
        HotelInnListModelClass lists = OfferList.get(position);
        int imglist[]={R.drawable.mini,R.drawable.auto,R.drawable.micro};
        String vlist[]={"Mini  . CRN 4567892342","Auto . CRN 4567892342","Micro . CRN 4567892342"};
        holder.title.setText(lists.getTitle());
        holder.vehicle_name.setText(vlist[position]);
        Glide
                .with(context)
                .load(imglist[position])
                .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder).diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.image);
    }



    @Override
    public int getItemCount() {
        return OfferList.size();

    }

}

