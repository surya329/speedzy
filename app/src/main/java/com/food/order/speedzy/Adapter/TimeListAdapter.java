package com.food.order.speedzy.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.food.order.speedzy.R;


/**
 * Created by Sujata Mohanty.
 */


public class TimeListAdapter extends RecyclerView.Adapter<TimeListAdapter.ViewHolder> {

    Context context;
    String[] timelist;
    int row_index=0;
    private TimeListAdapter.OnItemClickListener mOnItemClickListener;
    public TimeListAdapter(Context context, String[] timelist) {
        this.context=context;
        this.timelist=timelist;
    }
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(TimeListAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @Override
    public TimeListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_row, parent, false);
        System.out.println("View Holder");
        // set the view's size, margins, paddings and layout parameters
        TimeListAdapter.ViewHolder vh = new TimeListAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(TimeListAdapter.ViewHolder holder, final int position) {
holder.time.setText(timelist[position].toString());
        holder.time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, position);
                    row_index=position;
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return timelist.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView time;
        public ViewHolder(View itemView) {
            super(itemView);
            time=(TextView)itemView.findViewById(R.id.time);
        }
    }

}

