package com.food.order.speedzy.Activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Sujata Mohanty.
 */

public class SplashScreen extends AppCompatActivity {
  public static int systemWidth = 0;
  MySharedPrefrencesData mySharedPrefrencesData;
  String location = "";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash_screen_new);
    printKeyHash();
    DisplayMetrics displayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    systemWidth = displayMetrics.widthPixels;

    mySharedPrefrencesData = new MySharedPrefrencesData();
    Commons.deviceid = Settings.Secure.getString(SplashScreen.this.getContentResolver(),
        Settings.Secure.ANDROID_ID);

    boolean hasLoggedIn = mySharedPrefrencesData.getLoginStatus(this);
    location = mySharedPrefrencesData.getLocation(this);

    if (hasLoggedIn) {//Go directly to main activity.
      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          if (location.equalsIgnoreCase("") || location.equalsIgnoreCase(null)) {
                   /* Intent in = new Intent(SplashScreen.this, LocationActivity.class);
                    startActivity(in);*/
            Intent intent = new Intent(SplashScreen.this, Saved_Addresses.class);
            intent.putExtra("change_address", false);
            startActivity(intent);
          } else {
            Intent in = new Intent(SplashScreen.this, HomeActivityNew.class);
            in.putExtra("flag", 0);
            startActivity(in);
          }
            finish();
          overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }, 1000 * 2);
    } else {
      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          Intent in = new Intent(SplashScreen.this, SignupSignin_New.class);
          startActivity(in);
            finish();
          overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }, 1000 * 2);
    }

    //  //Dummy testing credentials
    //{
    //  if (location.equalsIgnoreCase("") || location.equalsIgnoreCase(null)) {
    //                   /* Intent in = new Intent(SplashScreen.this, LocationActivity.class);
    //                    startActivity(in);*/
    //    mySharedPrefrencesData.set_Party_mobile(this,"9003014142");
    //    mySharedPrefrencesData.setPartyEmail(this, "suryam.mvm@gmail.com");
    //    mySharedPrefrencesData.setUser_Id(this, "5");
    //    mySharedPrefrencesData.setFName(this,"Surya");
    //    mySharedPrefrencesData.setLName(this,"M");
    //    mySharedPrefrencesData.setLoginStatus(this, true);
    //    mySharedPrefrencesData.setLoginType(this, "1");
    //    mySharedPrefrencesData.setVeg_Flag(this, "0");
    //    Intent intent = new Intent(SplashScreen.this, AddAddresses_Manual.class);
    //    intent.putExtra("change_address", false);
    //    startActivity(intent);
    //    finish();
    //    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    //  }
    //}
    //else {
    //  Intent in = new Intent(SplashScreen.this, HomeActivityNew.class);
    //  in.putExtra("flag",0);
    //  startActivity(in);
    //  finish();
    //  overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    //}

  }

  private void printKeyHash() {

    try {
      PackageInfo info = getPackageManager().getPackageInfo("com.food.order.speedzy",
          PackageManager.GET_SIGNATURES);

      for (Signature signature : info.signatures) {
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(signature.toByteArray());
        Log.e("KEYHASH", Base64.encodeToString(md.digest(), Base64.DEFAULT));
      }
    } catch (PackageManager.NameNotFoundException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }
}
