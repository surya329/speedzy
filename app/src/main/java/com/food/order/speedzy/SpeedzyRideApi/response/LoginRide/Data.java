package com.food.order.speedzy.SpeedzyRideApi.response.LoginRide;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Data{

	@SerializedName("userRegistration")
	private List<UserRegistrationModel> userRegistrationModels;

	public List<UserRegistrationModel> getCreateOrders(){
		return userRegistrationModels;
	}

	@Override
 	public String toString(){
		return 
			"data{" +
			"userRegistration = '" + userRegistrationModels + '\'' +
			"}";
		}
}