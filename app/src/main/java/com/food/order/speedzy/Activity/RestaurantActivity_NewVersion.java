package com.food.order.speedzy.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.food.order.speedzy.Utils.SpeedyLinearLayoutManager;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.api.response.banner.BannerResponse;
import com.food.order.speedzy.root.BaseActivity;
import com.food.order.speedzy.screen.banner.BannerAdapter;
import com.food.order.speedzy.screen.banner.BannersVM;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.SnapHelper;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.food.order.speedzy.Adapter.CuisineAdapter_New;
import com.food.order.speedzy.Adapter.HotelsAdapter_New;
import com.food.order.speedzy.Model.Filtercuisine;
import com.food.order.speedzy.Model.Restaurant_model;
import com.food.order.speedzy.Model.ShopListModelNew;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.api.response.home.HomePageAppConfigResponse;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.screen.home.SpeedzyOperationsClosedModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;

import net.vrgsoft.layoutmanager.RollingLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Sujata Mohanty.
 */

public class RestaurantActivity_NewVersion extends BaseActivity{
  Toolbar toolbar;
  CollapsingToolbarLayout collapsingToolbarLayout;
  RecyclerView mRecyclerView;
  //LinearLayoutManager lm;
  RollingLayoutManager rollingLayoutManager;
  LinearLayoutManager linearLayoutManager;
  HotelsAdapter_New hotelsAdapter;
  List<Restaurant_model> restaurantModelList = new ArrayList<>();
  //RestaurantsListModelNew restaurantsListModelNew=new RestaurantsListModelNew();
  ShopListModelNew restaurantsListModelNew;
  ArrayList<String> filtercuisineArrayList = new ArrayList<>();
  ArrayList<Filtercuisine> update_filtercuisineArrayList = new ArrayList<>();
  ArrayList<Filtercuisine> cuisinielist = new ArrayList<>();
  ImageView filter, search_resturant, cart,ic_back;
  boolean isdatafilter = true;
  boolean isrestaurantlist = true;
  String relevanceflag = "";
  String ratingflag = "";
  String deliveryfeeflag = "";
  String minflag = "";
  TextView clear;
  RecyclerView recyclerview;
  LinearLayout sort_lay;
  TextView sortby;
  TextView cuisines;
  TextView relevance;
  TextView rating;
  TextView delivery_fee;
  TextView min_order, type;
  MySharedPrefrencesData mySharedPrefrencesData;
  String Veg_Flag;
  String res_id, nav_type, menu_id, name;
  private View viewToAttachDisplayerTo;
  boolean network_status = true;
  LoaderDiloag loaderDiloag;
  TextView res_food, bakery, fast_food,brk_fst,sweet;
  @BindView(R.id.ll_resturant_type) LinearLayout llResturantType;
  private Disposable mDisposable;
  static String city_id = "";
  private SpeedzyOperationsClosedModel mSpeedzyOperationsClosedModel;
  RelativeLayout comingsoon;
  RecyclerView banner;
  BannerAdapter bannerAdapter;
  private Apimethods mApimethods;
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.resturant_list);
    ButterKnife.bind(this);
    viewToAttachDisplayerTo = findViewById(R.id.activity_main);


    loaderDiloag = new LoaderDiloag(this);
    mySharedPrefrencesData = new MySharedPrefrencesData();
    Veg_Flag = mySharedPrefrencesData.getVeg_Flag(this);

    res_id = getIntent().getStringExtra("res_id");
    menu_id = getIntent().getStringExtra("menu_id");
    nav_type = getIntent().getStringExtra("nav_type");
    name = getIntent().getStringExtra("name");
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    ic_back = findViewById(R.id.ic_back);
    type = (TextView) findViewById(R.id.type);

    res_food = (TextView) findViewById(R.id.res_food);
    fast_food = (TextView) findViewById(R.id.fast_food);
    brk_fst= (TextView) findViewById(R.id.brk_fst);
    bakery = (TextView) findViewById(R.id.bakery);
    sweet=(TextView) findViewById(R.id.sweet);
    comingsoon=findViewById(R.id.comingsoon);

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    collapsingToolbarLayout= findViewById(R.id.collapsingToolbarLayout);
    setSupportActionBar(toolbar);
    statusbar_bg(R.color.red);
    type.setText(name);
    AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
    appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
      boolean isShow = true;
      int scrollRange = -1;

      @Override
      public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (scrollRange == -1) {
          scrollRange = appBarLayout.getTotalScrollRange();
        }
        if (scrollRange + verticalOffset == 0) {
          // collapsingToolbarLayout.setTitle(name);
          collapsingToolbarLayout.setTitle(" ");
          isShow = true;
        } else if(isShow) {
          collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
          isShow = false;
        }
      }
    });

    llResturantType.setVisibility(menu_id.equalsIgnoreCase("M1005") ? View.GONE : View.VISIBLE);

    search_resturant = (ImageView) findViewById(R.id.search_resturant);
    cart = (ImageView) findViewById(R.id.cart);
    filter = (ImageView) findViewById(R.id.filter);

    banner = (RecyclerView) findViewById(R.id.banner);
    mApimethods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    initComponent();

    ic_back.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });

    mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
    //lm=new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
      /*  rollingLayoutManager=new RollingLayoutManager(this);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(rollingLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(RestaurantActivity_NewVersion.this,0));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        hotelsAdapter = new HotelsAdapter_New(RestaurantActivity_NewVersion.this,restaurantModelList,nav_type);*/
    // mRecyclerView.setAdapter(hotelsAdapter);

    res_food.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        res_food_click();
      }
    });
    brk_fst.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        brk_fst_click();
      }
    });
    sweet.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        sweet_click();
      }
    });
    fast_food.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        fast_food_click();
      }
    });
    bakery.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        bakery_click();
      }
    });
    filter.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        calldialog();
      }
    });
  }
  private void initComponent() {
    banner.requestLayout();
    banner.setLayoutManager(new SpeedyLinearLayoutManager(RestaurantActivity_NewVersion.this,
            SpeedyLinearLayoutManager.HORIZONTAL, false));
    banner.setNestedScrollingEnabled(false);
    SnapHelper snapHelper = new LinearSnapHelper();
    banner.setOnFlingListener(null);
    snapHelper.attachToRecyclerView(banner);
    bannerAdapter = new BannerAdapter(
            new BannerAdapter.Callback() {
              @Override public void onClickItem(BannersVM BannersVM) {

              }
            });
    banner.setAdapter(bannerAdapter);
  }
  private void getBannerDataFromApi() {
    mApimethods.getBannerList(menu_id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<BannerResponse>() {
              @Override public void onSubscribe(Disposable d) {
                mDisposable = d;
              }

              @Override public void onNext(BannerResponse bannerResponse) {
                if (bannerResponse != null
                        && bannerResponse.getBanners() != null
                        && bannerResponse.getBanners().size() > 0) {
                  banner.setVisibility(View.VISIBLE);
                  banner.setBackgroundColor(getResources().getColor(R.color.red));
                  setBanner(bannerResponse);
                }else {
                  banner.setVisibility(View.GONE);
                }
              }

              @Override public void onError(Throwable e) {
              }

              @Override public void onComplete() {

              }
            });
  }

  private void setBanner(BannerResponse bannerResponse) {
    bannerAdapter.refreshData(BannersVM.transform(bannerResponse.getBanners()));
    banner.smoothScrollToPosition(1);
  }

  private void res_food_click() {
    res_id = "";
    menu_id = "M1001";
    nav_type = "3";
    name = "Restaurants";
    res_food.setTextColor(getResources().getColor(R.color.red));
    fast_food.setTextColor(getResources().getColor(R.color.white));
    brk_fst.setTextColor(getResources().getColor(R.color.white));
    brk_fst.setBackgroundColor(Color.TRANSPARENT);
    bakery.setTextColor(getResources().getColor(R.color.white));
    res_food.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
    fast_food.setBackgroundColor(Color.TRANSPARENT);
    bakery.setBackgroundColor(Color.TRANSPARENT);
    sweet.setTextColor(getResources().getColor(R.color.white));
    sweet.setBackgroundColor(Color.TRANSPARENT);
    callapi();
    getBannerDataFromApi();
  }

  private void brk_fst_click() {
    res_id = "";
    menu_id = "M1003";
    nav_type = "3";
    name = "Breakfast";
    brk_fst.setTextColor(getResources().getColor(R.color.red));
    res_food.setTextColor(getResources().getColor(R.color.white));
    fast_food.setTextColor(getResources().getColor(R.color.white));
    bakery.setTextColor(getResources().getColor(R.color.white));
    sweet.setTextColor(getResources().getColor(R.color.white));
    sweet.setBackgroundColor(Color.TRANSPARENT);
    brk_fst.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
    fast_food.setBackgroundColor(Color.TRANSPARENT);
    bakery.setBackgroundColor(Color.TRANSPARENT);
    res_food.setBackgroundColor(Color.TRANSPARENT);
    callapi();
    getBannerDataFromApi();
  }
  private void sweet_click() {
    res_id = "";
    menu_id = SpeedzyConstants.SWEET_MENU_ID;
    nav_type = "3";
    name = "Sweet";
    sweet.setTextColor(getResources().getColor(R.color.red));
    res_food.setTextColor(getResources().getColor(R.color.white));
    fast_food.setTextColor(getResources().getColor(R.color.white));
    bakery.setTextColor(getResources().getColor(R.color.white));
    brk_fst.setTextColor(getResources().getColor(R.color.white));
    sweet.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
    fast_food.setBackgroundColor(Color.TRANSPARENT);
    bakery.setBackgroundColor(Color.TRANSPARENT);
    res_food.setBackgroundColor(Color.TRANSPARENT);
    brk_fst.setBackgroundColor(Color.TRANSPARENT);
    callapi();
    getBannerDataFromApi();
  }
  private void fast_food_click() {
    res_id = "";
    menu_id = "M1012";
    nav_type = "3";
    name = "Fastfood";
    fast_food.setTextColor(getResources().getColor(R.color.red));
    res_food.setTextColor(getResources().getColor(R.color.white));
    bakery.setTextColor(getResources().getColor(R.color.white));
    fast_food.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
    res_food.setBackgroundColor(Color.TRANSPARENT);
    bakery.setBackgroundColor(Color.TRANSPARENT);
    brk_fst.setTextColor(getResources().getColor(R.color.white));
    brk_fst.setBackgroundColor(Color.TRANSPARENT);
    sweet.setTextColor(getResources().getColor(R.color.white));
    sweet.setBackgroundColor(Color.TRANSPARENT);
    callapi();
    getBannerDataFromApi();
  }
  private void bakery_click() {
    res_id = "";
    menu_id = "M1007";
    nav_type = "3";
    name = "Bakery";
    bakery.setTextColor(getResources().getColor(R.color.red));
    fast_food.setTextColor(getResources().getColor(R.color.white));
    res_food.setTextColor(getResources().getColor(R.color.white));
    bakery.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
    fast_food.setBackgroundColor(Color.TRANSPARENT);
    res_food.setBackgroundColor(Color.TRANSPARENT);
    brk_fst.setTextColor(getResources().getColor(R.color.white));
    brk_fst.setBackgroundColor(Color.TRANSPARENT);
    sweet.setTextColor(getResources().getColor(R.color.white));
    sweet.setBackgroundColor(Color.TRANSPARENT);
    callapi();
    getBannerDataFromApi();
  }


  private void statusbar_bg(int color) {
   /* getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
        .getColor(color)));*/
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
    }
  }

  private void calldialog() {
    final BottomSheetDialog dialog = new BottomSheetDialog(this, R.style.SheetDialog);
    dialog.setContentView(R.layout.rex_filter_dialog);
    recyclerview = (RecyclerView) dialog.findViewById(R.id.recyclerview);
    sort_lay = (LinearLayout) dialog.findViewById(R.id.sort_lay);
    sortby = (TextView) dialog.findViewById(R.id.sortby);
    cuisines = (TextView) dialog.findViewById(R.id.cuisines);
    relevance = (TextView) dialog.findViewById(R.id.relevance);
    rating = (TextView) dialog.findViewById(R.id.ratings);
    delivery_fee = (TextView) dialog.findViewById(R.id.delivery_fee);
    min_order = (TextView) dialog.findViewById(R.id.minimum_order);
    clear = (TextView) dialog.findViewById(R.id.clear);
    recyclerview.setHasFixedSize(true);
    LinearLayoutManager lm = new LinearLayoutManager(this);
    recyclerview.setLayoutManager(lm);
    sort_lay.setVisibility(View.VISIBLE);
    recyclerview.setVisibility(View.GONE);
    for (int i = 0; i < filtercuisineArrayList.size(); i++) {
      Filtercuisine filtercuisine = new Filtercuisine();
      filtercuisine.setCuisine(filtercuisineArrayList.get(i));
      cuisinielist.add(filtercuisine);
    }
    CuisineAdapter_New rcAdapter =
        new CuisineAdapter_New(RestaurantActivity_NewVersion.this, cuisinielist);
    recyclerview.setAdapter(rcAdapter);
    sortby.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        sortby.setTextColor(getResources().getColor(R.color.black_transparent));
        cuisines.setTextColor(getResources().getColor(R.color.light_gray));
        sort_lay.setVisibility(View.VISIBLE);
        recyclerview.setVisibility(View.GONE);
      }
    });
    cuisines.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        cuisines.setTextColor(getResources().getColor(R.color.black_transparent));
        sortby.setTextColor(getResources().getColor(R.color.light_gray));
        sort_lay.setVisibility(View.GONE);
        recyclerview.setVisibility(View.VISIBLE);
      }
    });
    if (relevanceflag.equalsIgnoreCase("1")) {
      sort_item_selected(relevance, rating, delivery_fee, min_order);
    } else if (ratingflag.equalsIgnoreCase("1")) {
      sort_item_selected(rating, relevance, delivery_fee, min_order);
    } else if (deliveryfeeflag.equalsIgnoreCase("1")) {
      sort_item_selected(delivery_fee, relevance, rating, min_order);
    } else if (minflag.equalsIgnoreCase("1")) {
      sort_item_selected(min_order, relevance, rating, delivery_fee);
    } else {

    }
    relevance.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        clear.setText("Ok");
        relevanceflag = "1";
        ratingflag = " ";
        deliveryfeeflag = " ";
        minflag = " ";
        sort_item_selected(relevance, rating, delivery_fee, min_order);
      }
    });
    rating.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        clear.setText("Ok");
        ratingflag = "1";
        deliveryfeeflag = " ";
        minflag = " ";
        relevanceflag = "";
        sort_item_selected(rating, relevance, delivery_fee, min_order);
      }
    });
    delivery_fee.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        clear.setText("Ok");
        ratingflag = " ";
        deliveryfeeflag = "1";
        minflag = " ";
        relevanceflag = "";
        sort_item_selected(delivery_fee, relevance, rating, min_order);
      }
    });
    min_order.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        clear.setText("Ok");
        ratingflag = " ";
        deliveryfeeflag = " ";
        minflag = "1";
        relevanceflag = "";
        sort_item_selected(min_order, relevance, rating, delivery_fee);
      }
    });
    clear.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (clear.getText().toString().equalsIgnoreCase("Clear All")) {
          ratingflag = " ";
          deliveryfeeflag = "";
          minflag = " ";
          Commons.cuisinelist = new ArrayList<>();
          callapi();
          dialog.dismiss();
        } else {
          ArrayList<String> selected_pref_arrey = new ArrayList<>();
          for (int i = 0; i < update_filtercuisineArrayList.size(); i++) {
            Filtercuisine myPrefData = update_filtercuisineArrayList.get(i);
            if (myPrefData.isSelected()) {
              selected_pref_arrey.add(myPrefData.getCuisine());
            }
          }
          Commons.ratingflag = ratingflag;
          Commons.minimumorder = minflag;
          Commons.deliveryfee = deliveryfeeflag;
          Commons.cuisinelist = selected_pref_arrey;
          isdatafilter = false;
          isrestaurantlist = false;
          callapi();
          dialog.dismiss();
        }
      }
    });
    dialog.show();
  }

  private void sort_item_selected(TextView selected_textview, TextView non_selected1,
      TextView non_selected2, TextView non_selected3) {
    selected_textview.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_radio_on, 0, 0, 0);
    non_selected1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_radio_off, 0, 0, 0);
    non_selected2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_radio_off, 0, 0, 0);
    non_selected3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_radio_off, 0, 0, 0);
  }

  /*private void callapi() {
      Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(RestaurantActivity_NewVersion.this).create(Apimethods.class);
      Call<RestaurantsListModelNew> call = methods.setRestaurant(res_id, Commons.latitude_str, Commons.longitude_str,0,1, ratingflag,deliveryfeeflag,minflag, Commons.cuisinelist,"0","0");

      Log.d("url","url="+call.request().url().toString());
      Log.d("Latitude", Commons.latitude_str);
      Log.d("Longitude", Commons.longitude_str);
      call.enqueue(new Callback<RestaurantsListModelNew>() {
          @Override
          public void onResponse(Call<RestaurantsListModelNew> call,
                                 retrofit2.Response<RestaurantsListModelNew> response)
          {
              restaurantModelList.clear();
              restaurantsListModelNew=response.body();
              restaurantModelList.addAll(restaurantsListModelNew.getRestaurants());
              if(isrestaurantlist==true){

                  Commons.restlist=new ArrayList<Restaurant_model>();
                  Commons.restlist=restaurantsListModelNew.getRestaurants();
              }
              hotelsAdapter.notifyDataSetChanged();
              if(isdatafilter==true){
                  filtercuisineArrayList=restaurantsListModelNew.getFilter_cuisine();
              }

          }
          @Override
          public void onFailure(Call<RestaurantsListModelNew> call, Throwable t) {
              t.printStackTrace();
              Toast.makeText(RestaurantActivity_NewVersion.this,"internet not available..connect internet", Toast.LENGTH_SHORT).show();
          }
      });
  }*/
  private void callapi() {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(RestaurantActivity_NewVersion.this)
        .create(Apimethods.class);
    Call<ShopListModelNew> call = methods.setShop_list(
        mySharedPrefrencesData.getSelectCity(RestaurantActivity_NewVersion.this),
        menu_id, mySharedPrefrencesData.getSelectAddress_ID(RestaurantActivity_NewVersion.this),
        Veg_Flag, mySharedPrefrencesData.getSelectCity_latitude(RestaurantActivity_NewVersion.this),
            mySharedPrefrencesData.getSelectCity_longitude(RestaurantActivity_NewVersion.this), "", "");
    Log.d("url", "url=" + call.request().url().toString());
    Log.d("Latitude", Commons.latitude_str);
    Log.d("Longitude", Commons.longitude_str);
    loaderDiloag.displayDiloag();
    call.enqueue(new Callback<ShopListModelNew>() {
      @Override
      public void onResponse(Call<ShopListModelNew> call,
          retrofit2.Response<ShopListModelNew> response) {
        restaurantsListModelNew = response.body();
        rollingLayoutManager = new RollingLayoutManager(RestaurantActivity_NewVersion.this);
        linearLayoutManager = new LinearLayoutManager(RestaurantActivity_NewVersion.this,
            LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setNestedScrollingEnabled(false);
       /* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
          mRecyclerView.setLayoutManager(linearLayoutManager);
        } else {
          mRecyclerView.setLayoutManager(rollingLayoutManager);
        }*/
        mRecyclerView.setLayoutManager(linearLayoutManager);
        //mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(RestaurantActivity_NewVersion.this, 0));
        mRecyclerView.setHasFixedSize(true);
       /* mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);*/
        hotelsAdapter = new HotelsAdapter_New(RestaurantActivity_NewVersion.this,
            restaurantsListModelNew.getRestaurants(), restaurantsListModelNew.getApp_message(),
            restaurantsListModelNew.getApp_openstatus(),
            restaurantsListModelNew.getRestaurant_timings(),
            restaurantsListModelNew.getOpen_close_status(), nav_type, menu_id);
        mRecyclerView.setAdapter(hotelsAdapter);
        hotelsAdapter.notifyDataSetChanged();

        if (restaurantsListModelNew.getRestaurants().isEmpty()){
          comingsoon.setVisibility(View.VISIBLE);
          mRecyclerView.setVisibility(View.GONE);
        }else {
          comingsoon.setVisibility(View.GONE);
          mRecyclerView.setVisibility(View.VISIBLE);
        }

        cart.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            Commons.flag_for_hta="";
            Intent intent =
                new Intent(RestaurantActivity_NewVersion.this, CheckoutActivity_New.class);
            intent.putExtra("start_time_lunch",
                restaurantsListModelNew.getRestaurant_timings().getLunch_start());
            intent.putExtra("end_time_lunch",
                restaurantsListModelNew.getRestaurant_timings().getLunch_end());
            intent.putExtra("start_time_dinner",
                restaurantsListModelNew.getRestaurant_timings().getDinner_start());
            intent.putExtra("end_time_dinner",
                restaurantsListModelNew.getRestaurant_timings().getDinner_end());
            intent.putExtra("app_openstatus",
                restaurantsListModelNew.getOpen_close_status().getOpenstatus());
            startActivity(intent);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
          }
        });
        search_resturant.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            Intent intent =
                new Intent(RestaurantActivity_NewVersion.this, Resturant_SearchActivity.class);
            intent.putExtra("start_time_lunch",
                restaurantsListModelNew.getRestaurant_timings().getLunch_start());
            intent.putExtra("end_time_lunch",
                restaurantsListModelNew.getRestaurant_timings().getLunch_end());
            intent.putExtra("start_time_dinner",
                restaurantsListModelNew.getRestaurant_timings().getDinner_start());
            intent.putExtra("end_time_dinner",
                restaurantsListModelNew.getRestaurant_timings().getDinner_end());
            intent.putExtra("app_openstatus",
                restaurantsListModelNew.getOpen_close_status().getOpenstatus());
            startActivity(intent);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
          }
        });
        loaderDiloag.dismissDiloag();
      }

      @Override
      public void onFailure(@NonNull Call<ShopListModelNew> call, @NonNull Throwable t) {
        t.printStackTrace();
        loaderDiloag.dismissDiloag();
      }
    });
  }
  @Override
  public void onBackPressed() {
   /* if (getFragmentManager().getBackStackEntryCount() == 0) {
      Commons.back_button_transition(RestaurantActivity_NewVersion.this);
    } else {
      super.onBackPressed();
      Commons.back_button_transition(RestaurantActivity_NewVersion.this);
    }*/
    Commons.back_button_transition(RestaurantActivity_NewVersion.this);
  }

  public void setData(ArrayList<Filtercuisine> mylist) {
    clear.setText("Ok");
    update_filtercuisineArrayList = mylist;
  }

 @Override
  public void onStart() {
    super.onStart();
   if (restaurantsListModelNew == null) {
     if (name.equalsIgnoreCase("Breakfast")){
       brk_fst_click();
     }else  if (name.equalsIgnoreCase("Sweet")){
       sweet_click();
     }else {
       res_food_click();
     }
   }else {
     hotelsAdapter.notifyDataSetChanged();
   }
  }
  @Override
  protected void onDestroy() {
    super.onDestroy();
    GeneralUtil.safelyDispose(mDisposable);
  }

  public void callapiHome() {
    if (mySharedPrefrencesData == null) {
      mySharedPrefrencesData = new MySharedPrefrencesData();
    }

    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    city_id = mySharedPrefrencesData.getSelectCity(this);
    methods.get_home(city_id, mySharedPrefrencesData.getVeg_Flag(this))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<HomePageAppConfigResponse>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(HomePageAppConfigResponse home_response_model) {
            if (home_response_model != null) {
              mSpeedzyOperationsClosedModel = SpeedzyOperationsClosedModel.transform(
                  home_response_model.getAppclosestatus(),
                  Integer.parseInt(home_response_model.getAppOpenstatus()),
                  Integer.parseInt(home_response_model.getAppCloseType()));
            }
          }

          @Override public void onError(Throwable e) {

          }

          @Override public void onComplete() {

          }
        });
  }
}
