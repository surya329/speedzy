package com.food.order.speedzy.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class DateTimePicker extends DialogFragment {
    TheListener listener;
    Button btnDateTimeOK, btnDateTimeCancel;
    Spinner spinSelectedDay;
    TimePicker tpSelectedTime;
    TextView appbar;
    int i = 1;
    String leave_on, return_by;
    List<String> datelist = new ArrayList<>();
    String strDay = null;
    String strTime = null;
    SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd MMM yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");

    public DateTimePicker(int i, String leave_on, String return_by) {
        this.i = i;
        this.leave_on = leave_on;
        this.return_by = return_by;
    }

    public interface TheListener {
        public void returnDate(int falg,String date, String time);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View thisDialog = inflater.inflate(R.layout.fragment_date_time_picker, container, false);

        listener = (TheListener) getActivity();
        btnDateTimeOK = (Button) thisDialog.findViewById(R.id.btnDateTimeOK);
        btnDateTimeCancel = (Button) thisDialog.findViewById(R.id.btnDateTimeCancel);
        spinSelectedDay = (Spinner) thisDialog.findViewById(R.id.spinSelectedDay);
        tpSelectedTime = (TimePicker) thisDialog.findViewById(R.id.tpSelectedTime);
        appbar=thisDialog.findViewById(R.id.appbar);

        if (i==1 || i==2){
            appbar.setText("Leave On");
        }else  if (i==3){
            appbar.setText("Return By");
        }

        for (int j = 0; j < 8; j++) {
            if (i == 1 || i==2) {
                Calendar calendar = new GregorianCalendar();
                calendar.add(Calendar.DATE, j);
                String day = dateFormat.format(calendar.getTime());
                datelist.add(day);
            } else {
                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime(dateFormat.parse(leave_on.substring(0, leave_on.indexOf(","))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cal.add(Calendar.DATE, j);
                String day = dateFormat.format(cal.getTime());
                datelist.add(day);
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_day_picker, datelist);
        spinSelectedDay.setAdapter(adapter);

        if (i != 1) {
            String date_leave = "", time_leave = "";
            if (i == 2) {
                date_leave = leave_on.substring(0, leave_on.indexOf(","));
                time_leave = leave_on.substring(leave_on.indexOf(",") + 1, leave_on.length());
            } else {
                if (!return_by.equalsIgnoreCase("SELECT")) {
                    date_leave = return_by.substring(0, return_by.indexOf(","));
                    time_leave = return_by.substring(return_by.indexOf(",") + 1, return_by.length());
                }
            }

            for (int i = 0; i < datelist.size(); i++) {
                if (datelist.get(i).equalsIgnoreCase(date_leave)) {
                    spinSelectedDay.setSelection(i);
                }
            }
            try {
                if (i == 2 || !return_by.equalsIgnoreCase("SELECT")) {
                    Date time = timeFormat.parse(time_leave);
                    tpSelectedTime.setHour(time.getHours());
                    tpSelectedTime.setMinute(time.getMinutes());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        btnDateTimeCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnDateTimeOK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                day_time();
                dismiss();
            }
        });

        return thisDialog;
    }


    private void day_time() {
        strDay = ((String) spinSelectedDay.getSelectedItem());
        strTime = Commons.Time(tpSelectedTime);

        if (listener != null) {
            listener.returnDate(i,strDay, strTime);
        }
    }
}
