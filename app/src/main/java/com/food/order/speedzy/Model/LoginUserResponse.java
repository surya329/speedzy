package com.food.order.speedzy.Model;

import android.annotation.TargetApi;
import android.os.Build;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Type;

/**
 * Created by Sujata Mohanty.
 */

public class LoginUserResponse implements Serializable {

  @SerializedName("status")
  private String status;
  @SerializedName("info")
  private Object info;
  @SerializedName("msg")
  private String msg;

  private String in_user_id;
  private String st_user_email;
  private String st_first_name;
  private String st_last_name;
  private String st_mobile;
  private String referal;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Object getInfo() {
    return info;
  }

  public void setInfo(Object info) {
    this.info = info;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String getIn_user_id() {
    return in_user_id;
  }

  public void setIn_user_id(String in_user_id) {
    this.in_user_id = in_user_id;
  }

  public String getSt_user_email() {
    return st_user_email;
  }

  public void setSt_user_email(String st_user_email) {
    this.st_user_email = st_user_email;
  }

  public String getSt_first_name() {
    return st_first_name;
  }

  public void setSt_first_name(String st_first_name) {
    this.st_first_name = st_first_name;
  }

  public String getSt_last_name() {
    return st_last_name;
  }

  public void setSt_last_name(String st_last_name) {
    this.st_last_name = st_last_name;
  }

  public String getSt_mobile() {
    return st_mobile;
  }

  public void setSt_mobile(String st_mobile) {
    this.st_mobile = st_mobile;
  }

  public String getReferal() {
    return referal;
  }

  public void setReferal(String referal) {
    this.referal = referal;
  }

  public String getFlg_user_type() {
    return flg_user_type;
  }

  public void setFlg_user_type(String flg_user_type) {
    this.flg_user_type = flg_user_type;
  }

  private String flg_user_type;

  public static class DataStateDeserializer implements JsonDeserializer<LoginUserResponse> {

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public LoginUserResponse deserialize(JsonElement json, Type typeOfT,
        JsonDeserializationContext context) throws JsonParseException {
      LoginUserResponse userResponse = new Gson().fromJson(json, LoginUserResponse.class);
      JsonObject jsonObject = json.getAsJsonObject();

      if (jsonObject.has("info")) {
        JsonElement elem = jsonObject.get("info");
        if (elem instanceof JsonArray) {

        } else {
          if (elem != null && !elem.isJsonNull()) {
            if (elem.isJsonPrimitive()) {
            } else {
              infodata(elem, userResponse);
            }
          }
        }
      }
      return userResponse;
    }

    private void infodata(JsonElement elem, LoginUserResponse userResponse) {
      String in_user_id = "";
      String st_user_email = "";
      String st_first_name = "";
      String st_last_name = "";
      String st_mobile = "";
      String referal = "";
      String flg_user_type = "";

      if (elem.getAsJsonObject().has("in_user_id")) {
        in_user_id = elem.getAsJsonObject().get("in_user_id").getAsString();
      }
      if (elem.getAsJsonObject().has("st_user_email")) {
        st_user_email = elem.getAsJsonObject().get("st_user_email").getAsString();
      }
      if (elem.getAsJsonObject().has("st_first_name")) {
        st_first_name = elem.getAsJsonObject().get("st_first_name").getAsString();
      }
      if (elem.getAsJsonObject().has("st_last_name")) {
        st_last_name = elem.getAsJsonObject().get("st_last_name").getAsString();
      }
      if (elem.getAsJsonObject().has("st_mobile")) {
        st_mobile = elem.getAsJsonObject().get("st_mobile").getAsString();
      }
      if (elem.getAsJsonObject().has("referal")) {
        referal = elem.getAsJsonObject().get("referal").getAsString();
      }
      if (elem.getAsJsonObject().has("flg_user_type")) {
        flg_user_type = elem.getAsJsonObject().get("flg_user_type").getAsString();
      }

      userResponse.setIn_user_id(in_user_id);
      userResponse.setSt_user_email(st_user_email);
      userResponse.setSt_first_name(st_first_name);
      userResponse.setSt_last_name(st_last_name);
      userResponse.setSt_mobile(st_mobile);
      userResponse.setReferal(referal);
      userResponse.setFlg_user_type(flg_user_type);
    }
  }

  @Override public String toString() {
    return "LoginUserResponse{" +
        "status='" + status + '\'' +
        ", info=" + info +
        ", msg='" + msg + '\'' +
        ", in_user_id='" + in_user_id + '\'' +
        ", st_user_email='" + st_user_email + '\'' +
        ", st_first_name='" + st_first_name + '\'' +
        ", st_last_name='" + st_last_name + '\'' +
        ", st_mobile='" + st_mobile + '\'' +
        ", referal='" + referal + '\'' +
        ", flg_user_type='" + flg_user_type + '\'' +
        '}';
  }
}
