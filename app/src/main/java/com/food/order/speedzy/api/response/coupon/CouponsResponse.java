package com.food.order.speedzy.api.response.coupon;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class CouponsResponse {

	@SerializedName("coupon")
	private List<CouponItem> coupon;

	@SerializedName("status")
	private String status;

	@SerializedName("msg")
	private String msg;

	public void setCoupon(List<CouponItem> coupon) {
		this.coupon = coupon;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@SerializedName("flag")
	private String flag;

	public List<CouponItem> getCoupon(){
		return coupon;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OffersResponse{" + 
			"coupon = '" + coupon + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}