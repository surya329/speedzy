package com.food.order.speedzy.Adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.api.response.home.Alltimings;
import com.food.order.speedzy.api.response.home.ItemsItem;
import com.food.order.speedzy.api.response.home.OpenCloseStatus;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class FeatureAdapter extends RecyclerView.Adapter<FeatureAdapter.SingleItemRowHolder> {

  private List<ItemsItem> itemsList;
  private Activity mContext;
  public static int sCorner = 15;
  public static int sMargin = 8;
  String width1;
  String height1;
  int flag;
  int pixels_height, pixels_width;
  Alltimings alltimings;
  OpenCloseStatus open_close_status;

  public FeatureAdapter(Activity context, List<ItemsItem> itemsList, String width1, String height1,
      int flag, Alltimings alltimings,
      OpenCloseStatus open_close_status) {
    this.itemsList = itemsList;
    this.mContext = context;
    this.width1 = width1;
    this.height1 = height1;
    this.flag = flag;
    this.alltimings = alltimings;
    this.open_close_status = open_close_status;
  }

  @Override
  public FeatureAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
    View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feature_type_row, null);
    FeatureAdapter.SingleItemRowHolder mh = new FeatureAdapter.SingleItemRowHolder(v);
    return mh;
  }

  @Override
  public void onBindViewHolder(FeatureAdapter.SingleItemRowHolder holder, int i) {
    final ItemsItem items = itemsList.get(i);
        /*if (flag == 3) {
           pixels_height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.parseFloat(height1), mContext.getResources().getDisplayMetrics());
            ViewGroup.LayoutParams params = holder.itemImage.getLayoutParams();
            params.height = pixels_height;
            params.width = pixels_height;
           holder.itemImage.requestLayout();
        }
        if (flag == 6 || flag == 8) {
            pixels_height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.parseFloat(height1), mContext.getResources().getDisplayMetrics());
            pixels_width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.parseFloat(width1), mContext.getResources().getDisplayMetrics());
            ViewGroup.LayoutParams params = holder.itemImage.getLayoutParams();
            params.width = pixels_height;
            params.height = pixels_height;
            holder.itemImage.requestLayout();
        }*/
    RequestBuilder<Drawable> thumbnailRequest = Glide
        .with(mContext)
        .load(Commons.image_baseURL_very_small + items.getImagename());
    Glide.with(mContext)
        .load(Commons.image_baseURL + items.getImagename())
        .thumbnail(thumbnailRequest)
            .apply(new RequestOptions().placeholder(R.drawable.grid_placeholder))
        .into(holder.itemImage);
   /* holder.itemImage.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (flag == 6) {
          Intent i = new Intent(mContext, TechnicalIssueActivity.class);
          i.putExtra("msg", items.getLiveMessage());
          mContext.startActivity(i);
          mContext.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
          Commons.clicking_event(mContext, items, alltimings, open_close_status);
        }
        FoodinnsMediaPlayer.getInstance().playSound("pin_drop_sound.mp3", mContext);
      }
    });*/
  }

  @Override
  public int getItemCount() {
    return itemsList.size();
  }

  public class SingleItemRowHolder extends RecyclerView.ViewHolder {

    protected ImageView itemImage;
    RelativeLayout relative_lay;

    public SingleItemRowHolder(View view) {
      super(view);
      this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
      this.relative_lay = (RelativeLayout) view.findViewById(R.id.relative_lay);
      if (flag == 3) {
        ViewGroup.LayoutParams params = itemImage.getLayoutParams();
        params.height = mContext.getResources().getDimensionPixelSize(R.dimen._95sdp);
        params.width = mContext.getResources().getDimensionPixelSize(R.dimen._95sdp);
        itemImage.requestLayout();
      } else if (flag == 6 || flag == 8) {
        ViewGroup.LayoutParams params = itemImage.getLayoutParams();
        params.width = mContext.getResources().getDimensionPixelSize(R.dimen._70sdp);
        params.height = mContext.getResources().getDimensionPixelSize(R.dimen._70sdp);
        itemImage.requestLayout();
      }
    }
  }
}

