package com.food.order.speedzy.screen.home;

import android.os.Parcel;
import android.os.Parcelable;
import com.food.order.speedzy.api.response.home.AppclosestatusItem;
import java.util.List;

public class SpeedzyOperationsClosedModel implements Parcelable {
  private int appOpenStatus;
  private String closingBanner;
  private String closeTitle;
  private String id;
  private int closeType;
  private String closeMessage;
  private String closingIcon;
  private String cityId;
  private int mAppClosedType;

  public SpeedzyOperationsClosedModel() {

  }

  protected SpeedzyOperationsClosedModel(Parcel in) {
    appOpenStatus = in.readInt();
    closingBanner = in.readString();
    closeTitle = in.readString();
    id = in.readString();
    closeType = in.readInt();
    closeMessage = in.readString();
    closingIcon = in.readString();
    cityId = in.readString();
    mAppClosedType = in.readInt();
  }

  public static final Creator<SpeedzyOperationsClosedModel> CREATOR =
      new Creator<SpeedzyOperationsClosedModel>() {
        @Override
        public SpeedzyOperationsClosedModel createFromParcel(Parcel in) {
          return new SpeedzyOperationsClosedModel(in);
        }

        @Override
        public SpeedzyOperationsClosedModel[] newArray(int size) {
          return new SpeedzyOperationsClosedModel[size];
        }
      };

  public int getAppOpenStatus() {
    return appOpenStatus;
  }

  public void setAppOpenStatus(int appOpenStatus) {
    this.appOpenStatus = appOpenStatus;
  }

  public String getClosingBanner() {
    return closingBanner;
  }

  public void setClosingBanner(String closingBanner) {
    this.closingBanner = closingBanner;
  }

  public String getCloseTitle() {
    return closeTitle;
  }

  public void setCloseTitle(String closeTitle) {
    this.closeTitle = closeTitle;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getCloseType() {
    return closeType;
  }

  public void setCloseType(int closeType) {
    this.closeType = closeType;
  }

  public String getCloseMessage() {
    return closeMessage;
  }

  public void setCloseMessage(String closeMessage) {
    this.closeMessage = closeMessage;
  }

  public String getClosingIcon() {
    return closingIcon;
  }

  public void setClosingIcon(String closingIcon) {
    this.closingIcon = closingIcon;
  }

  public String getCityId() {
    return cityId;
  }

  public void setCityId(String cityId) {
    this.cityId = cityId;
  }

  public int getAppClosedType() {
    return mAppClosedType;
  }

  public void setAppClosedType(int appClosedType) {
    mAppClosedType = appClosedType;
  }

  public static SpeedzyOperationsClosedModel transform(List<AppclosestatusItem> data,
      int appOpenStatus, int appClosedType) {
    if (appClosedType != 0) {
      SpeedzyOperationsClosedModel vm = new SpeedzyOperationsClosedModel();
      vm.setAppOpenStatus(appOpenStatus);
      vm.setAppClosedType(appClosedType);
      if (data != null && data.size() >= appClosedType) {
        vm.setCloseMessage(data.get(appClosedType - 1).getCloseMessage());
        vm.setCityId(data.get(appClosedType - 1).getCityId());
        vm.setCloseTitle(data.get(appClosedType - 1).getCloseTitle());
        vm.setClosingBanner(data.get(appClosedType - 1).getClosingBanner());
        vm.setClosingIcon(data.get(appClosedType - 1).getClosingIcon());
        vm.setCloseType(appClosedType);
        return vm;
      }
    }
    return null;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {
    parcel.writeInt(appOpenStatus);
    parcel.writeString(closingBanner);
    parcel.writeString(closeTitle);
    parcel.writeString(id);
    parcel.writeInt(closeType);
    parcel.writeString(closeMessage);
    parcel.writeString(closingIcon);
    parcel.writeString(cityId);
    parcel.writeInt(mAppClosedType);
  }

  @Override public String toString() {
    return "SpeedzyOperationsClosedModel{" +
        "appOpenStatus=" + appOpenStatus +
        ", closingBanner='" + closingBanner + '\'' +
        ", closeTitle='" + closeTitle + '\'' +
        ", id='" + id + '\'' +
        ", closeType=" + closeType +
        ", closeMessage='" + closeMessage + '\'' +
        ", closingIcon='" + closingIcon + '\'' +
        ", cityId='" + cityId + '\'' +
        ", mAppClosedType=" + mAppClosedType +
        '}';
  }
}
