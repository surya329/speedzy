package com.food.order.speedzy.screen.TrackOrder;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fasterxml.jackson.databind.JsonNode;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.CommonConvertion;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.IconUtils;
import com.food.order.speedzy.Utils.IntentUtils;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.api.DeliveryApiService;
import com.food.order.speedzy.api.DeliveryServiceApiGenerator;
import com.food.order.speedzy.api.MapsApiGenerator;
import com.food.order.speedzy.api.MapsApiService;
import com.food.order.speedzy.api.QueryBuilder;
import com.food.order.speedzy.api.response.delivery.ListDriverLocationItem;
import com.food.order.speedzy.api.response.delivery.ListDriverResponse;
import com.food.order.speedzy.api.response.maps.DirectionsResponse;
import com.food.order.speedzy.api.response.order.ListOrderStatusItem;
import com.food.order.speedzy.api.response.order.OrderStatusResponse;
import com.food.order.speedzy.maps.CustomInfoAdapter;
import com.food.order.speedzy.root.BaseMapActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import org.phoenixframework.channels.Channel;
import org.phoenixframework.channels.Socket;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import timber.log.Timber;

import static com.food.order.speedzy.Utils.SpeedzyConstants.OrderStatusType.DELIVERY_PARTNER_WAITING_AT_RESTURANT;

public class TrackOrderActivity extends BaseMapActivity
    implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
    ResultCallback<LocationSettingsResult>, GoogleApiClient.OnConnectionFailedListener {
  @BindView(R.id.txt_title) TextView txtTitle;
  @BindView(R.id.txt_order_accepted_status) TextView txtOrderAccepetedStatus;
  @BindView(R.id.txt_food_status) TextView txtStatusTitle;
  @BindView(R.id.txt_order_status) TextView txtOrderStatus;
  @BindView(R.id.img_call) AppCompatImageView imgCall;
  @BindView(R.id.fab_locate) FloatingActionButton fabLocate;

  private GoogleMap mMap;
  private ReactiveLocationProvider locationProvider;
  private static final String EXTRA_ORDER_INFO = "EXTRA_ORDER_INFO";
  private Location myLocation;
  private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
  private final static int REQUEST_CHECK_SETTINGS = 0;

  private Socket socket;
  private Socket socketReceiver;
  private Channel channel;
  private Channel channelReceiver;
  private Disposable mDisposable;
  private Marker destinationMarker;
  Observable<DirectionsResponse> updateRoute;
  private Marker sourceMarker;
  PolylineOptions polylineOptions;
  float rotationValue;
  private Polyline polyline;
  List<LatLng> updatedLatLng;
  LatLng oldLatLng = null;
  LatLng newLatLng = null;
  private TrackOrderDataModel mTrackOrderDataModel;
  private JsonNode locationNode;

  String routePolyline = null;
  String eta = "";
  @BindView(R.id.txt_eta) TextView txtEta;
  private MapsApiService mMapsApiService;
  private DeliveryApiService mDeliveryApiService;
  private MySharedPrefrencesData mySharedPrefrencesData;

  public static final int PATTERN_DASH_LENGTH_PX = 16;
  public static final int PATTERN_GAP_LENGTH_PX = 16;
  public static final PatternItem DOT = new Dot();
  public static final PatternItem DASH = new Dash(PATTERN_DASH_LENGTH_PX);
  public static final PatternItem GAP = new Gap(PATTERN_GAP_LENGTH_PX);
  public static final List<PatternItem> PATTERN_POLYGON_ALPHA = Arrays.asList(GAP, DASH);
  private ListOrderStatusItem mListOrderStatusItem;
  private PermissionListener mCheckLocationPermissionListener;
  private LocationRequest mLocationRequest;
  protected GoogleApiClient mGoogleApiClient;
  private MaterialDialog mPermissionDeniedDialog;
  private LocationSettingsRequest.Builder builder;

  public static Intent newIntent(Context context, TrackOrderDataModel trackOrderDataModel) {
    Intent intent = new Intent(context, TrackOrderActivity.class);
    intent.putExtra(EXTRA_ORDER_INFO, trackOrderDataModel);
    return intent;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_track);
    ButterKnife.bind(this);
    initComponent();
    if (mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
        mTrackOrderDataModel.getDeliveryBoyId())) {
      getDeliveryGuyLastKnownLocation(mTrackOrderDataModel.getOrderId(),
          mTrackOrderDataModel.getDeliveryBoyId());
    } else if (mTrackOrderDataModel != null && mTrackOrderDataModel.getPickup() != null) {
      updateRoute(mTrackOrderDataModel.getPickup());
    }
  }

  @SuppressLint("SetTextI18n") private void initComponent() {
    locationProvider = new ReactiveLocationProvider(TrackOrderActivity.this);
    if (getIntent().getExtras() != null) {
      mTrackOrderDataModel = getIntent().getExtras().getParcelable(EXTRA_ORDER_INFO);
    }
    if (mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
        mTrackOrderDataModel.getOrderId())) {
      txtTitle.setText("Order ID : " + mTrackOrderDataModel.getOrderId());
    }

    mMapsApiService = MapsApiGenerator.provideRetrofit(this).create(MapsApiService.class);
    mDeliveryApiService =
        DeliveryServiceApiGenerator.provideRetrofit(this).create(DeliveryApiService.class);
    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
    if (mapFragment != null) {
      mapFragment.getMapAsync(this);
    }
    mySharedPrefrencesData = new MySharedPrefrencesData();
    mCheckLocationPermissionListener = new PermissionListener() {
      @Override public void onPermissionGranted(PermissionGrantedResponse response) {
        Timber.d("permission granted");
        initLocationProvider();
      }

      @Override public void onPermissionDenied(PermissionDeniedResponse response) {
        Timber.d("permission denied");
        if (response.isPermanentlyDenied()) {
          if (mPermissionDeniedDialog == null) {
            initLocationDialog();
          }
          mPermissionDeniedDialog.show();
        } else {
          Toast.makeText(TrackOrderActivity.this, "Location permission not enabled",
              Toast.LENGTH_SHORT)
              .show();
        }
      }

      @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
          PermissionToken token) {
        Timber.d("permission rationale should be shown");
        token.continuePermissionRequest();
      }
    };
  }

  private void initLocationRequest() {
    mLocationRequest = new LocationRequest();
    mLocationRequest.setInterval(10000);
    mLocationRequest.setFastestInterval(5000);
    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
  }

  @Override protected void onStop() {
    super.onStop();
    disconnectAllSockets();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    GeneralUtil.safelyDispose(mDisposable);
    disconnectAllSockets();
  }

  @Override public void onResume() {
    super.onResume();
    if (mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
        mTrackOrderDataModel.getOrderId())) {
      getOrderStatus(mTrackOrderDataModel.getOrderId());
    }
    if (socket == null
        && mTrackOrderDataModel != null
        && !GeneralUtil.isStringEmpty(mTrackOrderDataModel.getDeliveryBoyId())) {
      initSocketConnection(mTrackOrderDataModel.getDeliveryBoyId());
    }
    if (socketReceiver == null
        && mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
        mTrackOrderDataModel.getOrderId())) {
      initReceiverSocketConnection(mTrackOrderDataModel.getOrderId());
    }
  }

  private void getOrderStatus(String orderId) {
    showProgressDialog();
    Log.e("query", QueryBuilder.getOrderStatus(orderId.replaceAll("#", "").trim()));
    mDeliveryApiService.getOrderStatus(
        QueryBuilder.getOrderStatus(orderId.replaceAll("#", "").trim()))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<OrderStatusResponse>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(OrderStatusResponse response) {
            dismissProgessLoading();
            if (response.getData() != null
                && response.getData().getListOrderStatus() != null
                && response.getData().getListOrderStatus().size() > 0) {
              mListOrderStatusItem = response.getData().getListOrderStatus().get(0);
              showFoodStatusData(response.getData().getListOrderStatus().get(0));
            } else {
              setFoodDeliveryStatus();
            }
          }

          @Override public void onError(Throwable e) {
            dismissProgessLoading();
          }

          @Override public void onComplete() {

          }
        });
  }

  private void setFoodDeliveryStatus() {
    if (mTrackOrderDataModel != null
        && !GeneralUtil.isStringEmpty(
        mTrackOrderDataModel.getOrderStatusMessage())
        && mTrackOrderDataModel.getOrderStatusMessage()
        .toLowerCase()
        .contains("delivery partner")
        && !GeneralUtil.isStringEmpty(
        mTrackOrderDataModel.getDeliveryBoyName())) {
      txtOrderAccepetedStatus.setText(SpeedzyConstants.OrderStatusMesssage.ACCEPTED);
      txtStatusTitle.setText(mTrackOrderDataModel.getOrderTitle());
      txtOrderStatus.setText(mTrackOrderDataModel.getOrderStatusMessage()
          .replace("partner", "partner " + mTrackOrderDataModel.getDeliveryBoyName()));
      imgCall.setVisibility(View.VISIBLE);
    } else if (mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
        mTrackOrderDataModel.getOrderTitle()) && !GeneralUtil.isStringEmpty(
        mTrackOrderDataModel.getOrderStatusMessage())) {
      txtOrderAccepetedStatus.setText(SpeedzyConstants.OrderStatusMesssage.ACCEPTED);
      txtOrderStatus.setText(mTrackOrderDataModel.getOrderTitle());
      txtStatusTitle.setText(mTrackOrderDataModel.getOrderStatusMessage());
      imgCall.setVisibility(View.GONE);
    } else {
      txtOrderAccepetedStatus.setText(SpeedzyConstants.OrderStatusMesssage.ACCEPTED);
      txtOrderStatus.setText("Order Confirmed");
      txtStatusTitle.setText("Food is being prepared");
      imgCall.setVisibility(View.GONE);
    }
  }

  private void showFoodStatusData(ListOrderStatusItem data) {
    if (!GeneralUtil.isStringEmpty(data.getDeliveryGuyId()) && mTrackOrderDataModel !=
        null && GeneralUtil.isStringEmpty(mTrackOrderDataModel.getDeliveryBoyId())) {
      mTrackOrderDataModel.setDeliveryBoyId(data.getDeliveryGuyId());
    }
    if (data.getStatus() != 0) {
      txtOrderAccepetedStatus.setText(SpeedzyConstants.OrderStatusMesssage.ACCEPTED);
    } else {
      txtOrderAccepetedStatus.setText("Order Recieved");
    }

    switch (data.getStatus()) {
      case SpeedzyConstants.OrderStatusType.ORDER_RECIEVED:
        txtStatusTitle.setText(SpeedzyConstants.OrderStatusTitle.ORDER_RECIEVED);
        txtOrderStatus.setText(
            SpeedzyConstants.OrderStatusMesssage.AWAITING_CONFIRMATION_RESTURANT);
        imgCall.setVisibility(View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.ACCEPTED:
        txtStatusTitle.setText(SpeedzyConstants.OrderStatusTitle.ACCEPTED);
        txtOrderStatus.setText(SpeedzyConstants.OrderStatusMesssage.ACCEPTED);
        imgCall.setVisibility(View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.CANCELLED:
        txtStatusTitle.setText(SpeedzyConstants.OrderStatusTitle.CANCELLED);
        txtOrderStatus.setText(SpeedzyConstants.OrderStatusMesssage.CANCELLED);
        imgCall.setVisibility(View.GONE);
        Toast.makeText(this, SpeedzyConstants.OrderStatusMesssage.CANCELLED, Toast.LENGTH_SHORT)
            .show();
        onBackPressed();
        break;
      case SpeedzyConstants.OrderStatusType.ORDER_CANCELLED:
        txtStatusTitle.setText(SpeedzyConstants.OrderStatusTitle.CANCELLED);
        txtOrderStatus.setText(SpeedzyConstants.OrderStatusMesssage.CANCELLED);
        imgCall.setVisibility(View.GONE);
        Toast.makeText(this, SpeedzyConstants.OrderStatusMesssage.CANCELLED, Toast.LENGTH_SHORT)
            .show();
        onBackPressed();
        break;
      case SpeedzyConstants.OrderStatusType.DELIVERY_PARTNER_ASSIGNED:
        imgCall.setVisibility(View.VISIBLE);
        if (mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
            mTrackOrderDataModel.getDeliveryBoyName())) {
          txtStatusTitle.setText(SpeedzyConstants.OrderStatusTitle.DELIVERY_PARTNER_ASSIGNED);
          txtOrderStatus.setText(SpeedzyConstants.OrderStatusMesssage.DELIVERY_PARTNER_ASSIGNED
              .replace("Delivery Partner",
                  mTrackOrderDataModel.getDeliveryBoyName()));
        } else {
          txtStatusTitle.setText(SpeedzyConstants.OrderStatusTitle.DELIVERY_PARTNER_ASSIGNED);
          txtOrderStatus.setText(SpeedzyConstants.OrderStatusMesssage.DELIVERY_PARTNER_ASSIGNED);
        }
        break;
      case SpeedzyConstants.OrderStatusType.ORDER_PICKED_UP:
        txtStatusTitle.setText(SpeedzyConstants.OrderStatusTitle.ORDER_PICKED_UP);
        txtOrderStatus.setText(SpeedzyConstants.OrderStatusMesssage.ORDER_PICKED_UP);
        imgCall.setVisibility(mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
            mTrackOrderDataModel.getDeliveryBoyNumber()) ? View.VISIBLE : View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.ORDER_DELIVERED:
        txtStatusTitle.setText(SpeedzyConstants.OrderStatusTitle.ORDER_DELIVERED);
        txtOrderStatus.setText(SpeedzyConstants.OrderStatusMesssage.ORDER_DELIVERED);
        imgCall.setVisibility(mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
            mTrackOrderDataModel.getDeliveryBoyNumber()) ? View.VISIBLE : View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.ORDER_CANCEL:
        txtStatusTitle.setText(SpeedzyConstants.OrderStatusTitle.ORDER_CANCEL);
        txtOrderStatus.setText(SpeedzyConstants.OrderStatusMesssage.ORDER_CANCEL);
        imgCall.setVisibility(View.GONE);
        Toast.makeText(this, SpeedzyConstants.OrderStatusMesssage.ORDER_CANCEL, Toast.LENGTH_SHORT)
            .show();
        onBackPressed();
        break;
      case SpeedzyConstants.OrderStatusType.ORDER_READY_FOR_PICKUP:
        txtStatusTitle.setText(SpeedzyConstants.OrderStatusTitle.ORDER_READY_FOR_PICKUP);
        txtOrderStatus.setText(SpeedzyConstants.OrderStatusMesssage.ORDER_READY_FOR_PICKUP);
        imgCall.setVisibility(mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
            mTrackOrderDataModel.getDeliveryBoyNumber()) ? View.VISIBLE : View.GONE);
        break;
      case DELIVERY_PARTNER_WAITING_AT_RESTURANT:
        txtStatusTitle.setText(
            SpeedzyConstants.OrderStatusTitle.DELIVERY_PARTNER_WAITING_AT_RESTURANT);
        txtOrderStatus.setText(
            SpeedzyConstants.OrderStatusMesssage.DELIVERY_PARTNER_WAITING_AT_RESTURANT);
        imgCall.setVisibility(mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
            mTrackOrderDataModel.getDeliveryBoyNumber()) ? View.VISIBLE : View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.WE_CANT_REACH_YOU:
        txtStatusTitle.setText(
            SpeedzyConstants.OrderStatusTitle.WE_CANT_REACH_YOU);
        txtOrderStatus.setText(
            SpeedzyConstants.OrderStatusMesssage.WE_CANT_REACH_YOU);
        imgCall.setVisibility(View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.AWAITING_CONFIRMATION_RESTURANT:
        txtStatusTitle.setText(
            SpeedzyConstants.OrderStatusTitle.AWAITING_CONFIRMATION_RESTURANT);
        txtStatusTitle.setText(
            SpeedzyConstants.OrderStatusMesssage.AWAITING_CONFIRMATION_RESTURANT);
        imgCall.setVisibility(View.GONE);
        break;
      default:
        txtStatusTitle.setText(
            SpeedzyConstants.OrderStatusTitle.AWAITING_CONFIRMATION_RESTURANT);
        txtStatusTitle.setText(
            SpeedzyConstants.OrderStatusMesssage.AWAITING_CONFIRMATION_RESTURANT);
        imgCall.setVisibility(View.GONE);
        break;
    }
    if (socket == null || !socket.isConnected()
        && mTrackOrderDataModel != null
        && mTrackOrderDataModel.getDeliveryBoyId() != null) {
      initSocketConnection(mTrackOrderDataModel.getDeliveryBoyId());
    }
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    try {
      boolean success = mMap.setMapStyle(
          MapStyleOptions.loadRawResourceStyle(
              this, R.raw.map_style));

      if (!success) {
        Log.e("MapsActivityRaw", "Style parsing failed.");
      }
    } catch (Resources.NotFoundException e) {
      Log.e("MapsActivityRaw", "Can't find style.", e);
    }
    mMap.setInfoWindowAdapter(new CustomInfoAdapter(this));
  }

  private void initSocketConnection(String deliveryGuyId) {
    try {
      socket = new Socket(SpeedzyConstants.SOCKET_URL);
      socket.connect();
      channel = socket.chan("track:" + deliveryGuyId, null);
      channel.join().receive("ok", envelope -> {
        Log.e("okay socket", envelope.toString());
      }).receive("ignore", envelope -> {
        Log.e("ignore socket", envelope.toString());
      });
      channel.on("receive_location", envelope -> {
        locationNode = envelope.getPayload().get("location");
        Log.e("recieved  location", locationNode.textValue().toString());
        if (!locationNode.isNull()) {
          if (mListOrderStatusItem != null) {
            switch (mListOrderStatusItem.getStatus()) {
              case SpeedzyConstants.OrderStatusType.DELIVERY_PARTNER_ASSIGNED:
              case SpeedzyConstants.OrderStatusType.ORDER_READY_FOR_PICKUP:
              case DELIVERY_PARTNER_WAITING_AT_RESTURANT:
                drawRestaurantRoute(locationNode);
                break;
              case SpeedzyConstants.OrderStatusType.ORDER_PICKED_UP:
                drawRoute(locationNode);
                break;
              default:
                drawRoute(locationNode);
                break;
            }
          } else {
            drawRoute(locationNode);
          }
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void initReceiverSocketConnection(String orderID) {
    try {
      socketReceiver = new Socket(SpeedzyConstants.SOCKET_URL);
      socketReceiver.connect();
      channelReceiver = socketReceiver.chan("tracking:" + orderID.replaceAll("#", "".trim()), null);
      channelReceiver.join().receive("ok", envelope -> {
        Log.e("okay socket", envelope.toString());
      }).receive("ignore", envelope -> {
        Log.e("ignore socket", envelope.toString());
      });
      channelReceiver.on("order_status", envelope -> {
        Log.e("status", envelope.toString());
        getStatus(orderID);
      });
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void getStatus(String orderID) {
    runOnUiThread(new Runnable() {
      @Override public void run() {
        getOrderStatus(orderID);
      }
    });
  }

  public void drawRoute(JsonNode jsonNodeLocation) {
    if (mTrackOrderDataModel != null && mTrackOrderDataModel.getDestination() != null) {
      if (updatedLatLng != null) {
        updatedLatLng.clear();
      }
      int[] ployLineColor = getResources().getIntArray(R.array.ploycolor);
      polylineOptions = new PolylineOptions().
          geodesic(true).
          color(ployLineColor[0]).
          width(10);
      String source = CommonConvertion.sRID_replace(String.valueOf(jsonNodeLocation));
      if (mySharedPrefrencesData == null) {
        mySharedPrefrencesData = new MySharedPrefrencesData();
      }
      if (!GeneralUtil.isStringEmpty(source)) {
        double driverCurrentLat = Double.parseDouble(source.split(" ")[0]);
        double driverCurrentLng = Double.parseDouble(source.split(" ")[1]);
        updateRoute = mMapsApiService.mapApiDirection("driving",
            driverCurrentLat + "," + driverCurrentLng,
            mTrackOrderDataModel.getDestination().latitude
                + ","
                + mTrackOrderDataModel.getDestination().longitude,
            mySharedPrefrencesData.getMapsApiKey(TrackOrderActivity.this),
            "false");

        updateRoute.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<DirectionsResponse>() {
              @Override public void onSubscribe(Disposable d) {
                mDisposable = d;
              }

              @Override public void onNext(DirectionsResponse data) {
                if (data != null && data.getRoutes() != null && data.getRoutes().size() > 0) {

                  if (updatedLatLng != null) {
                    updatedLatLng.clear();
                  }
                  updatedLatLng = CommonConvertion.decodePolyLine(
                      data.getRoutes().get(0).getOverviewPolyline().getPoints());
                  routePolyline = CommonConvertion.convert_to_base64(
                      data.getRoutes().get(0).getOverviewPolyline().getPoints());
                  eta = data.getRoutes().get(0).getLegs().get(0).getDuration().getText();
                  int currentDistance = CommonConvertion.detour(
                      data.getRoutes().get(0).getLegs().get(0).getDistance().getText());
                  if (destinationMarker != null) {
                    destinationMarker.remove();
                  }
                  LayoutInflater inflater =
                      (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                  assert inflater != null;
                  @SuppressLint("InflateParams") View layout =
                      inflater.inflate(R.layout.custom_map_marker, null);
                  TextView endMarker = layout.findViewById(R.id.txtView_map_time);
                  String[] traveltime = eta.split(" ");
                  Calendar currentTime = Calendar.getInstance();
                  Calendar tmp = (Calendar) currentTime.clone();
                  tmp.add(Calendar.HOUR_OF_DAY, 1);
                  if (traveltime.length == 2) {
                    tmp.add(Calendar.MINUTE, Integer.parseInt(traveltime[0]));
                  } else {
                    if (traveltime.length == 4) {
                      tmp.add(Calendar.HOUR_OF_DAY, Integer.parseInt(traveltime[0]));
                      tmp.add(Calendar.MINUTE, Integer.parseInt(traveltime[2]));
                    }
                  }
                  @SuppressLint("SimpleDateFormat")
                  SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                  String currentTimeAMPM = dateFormat.format(tmp.getTime());
                  if (currentDistance <= 50) {
                    endMarker.setText("Reached !");
                    txtEta.setText("Reached");
                    txtEta.setVisibility(View.VISIBLE);
                  } else {
                    endMarker.setText(currentTimeAMPM);
                    txtEta.setText(getString(R.string.trackorder_format_eta, eta));
                    txtEta.setVisibility(View.VISIBLE);
                  }
                  if (sourceMarker != null) {
                    sourceMarker.remove();
                  }

                  sourceMarker = mMap.addMarker(
                      new MarkerOptions().icon(
                          IconUtils.getBikeImageSvgIcon(TrackOrderActivity.this))
                          .flat(true)
                          .position(new LatLng(
                              data.getRoutes().get(0).getLegs().get(0).getStartLocation().getLat(),
                              data.getRoutes()
                                  .get(0)
                                  .getLegs()
                                  .get(0)
                                  .getStartLocation()
                                  .getLng())));

                  destinationMarker = mMap.addMarker(
                      new MarkerOptions().icon(IconUtils.getDropSVGIcon(TrackOrderActivity.this))
                          .position(new LatLng(
                              data.getRoutes().get(0).getLegs().get(0).getEndLocation().getLat(),
                              data.getRoutes().get(0).getLegs().get(0).getEndLocation().getLng())));
                  destinationMarker.setAnchor(0.9f, 0.7f);

                  if (polyline != null) {
                    polyline.getPoints().clear();
                    polyline.remove();
                    polylineOptions.getPoints().clear();
                    polylineOptions.addAll(updatedLatLng);
                    polylineOptions.pattern(null);
                    polyline = mMap.addPolyline(polylineOptions);
                    oldLatLng = polyline.getPoints().get(0);
                    if (polyline.getPoints().size() > 1) {
                      newLatLng = polyline.getPoints().get(1);
                      if (newLatLng != null) {
                        rotationValue = getBearing(oldLatLng, newLatLng);
                      }
                    } else {
                      polylineOptions.addAll(updatedLatLng);
                      polylineOptions.pattern(null);
                      polyline = mMap.addPolyline(polylineOptions);
                      oldLatLng = polyline.getPoints().get(0);
                      if (polyline.getPoints().size() > 1) {
                        newLatLng = polyline.getPoints().get(1);
                        if (newLatLng != null) {
                          rotationValue = getBearing(oldLatLng, newLatLng);
                        }
                      }
                    }
                    sourceMarker
                        .setPosition(
                            new LatLng(
                                data.getRoutes()
                                    .get(0)
                                    .getLegs()
                                    .get(0)
                                    .getStartLocation()
                                    .getLat(),
                                data.getRoutes()
                                    .get(0)
                                    .getLegs()
                                    .get(0)
                                    .getStartLocation()
                                    .getLng()));
                    sourceMarker.setAnchor(0.5f, 0.5f);
                    sourceMarker.setRotation(rotationValue);
                    destinationMarker.setTag(
                        GeneralUtil.getInfoWindowData(mTrackOrderDataModel, false));
                    destinationMarker.showInfoWindow();
                  }
                }
              }

              @Override public void onError(Throwable e) {

              }

              @Override public void onComplete() {

              }
            });
      }
    }
  }

  public void drawRestaurantRoute(JsonNode jsonNodeLocation) {
    if (mTrackOrderDataModel != null && mTrackOrderDataModel.getPickup() != null) {
      if (updatedLatLng != null) {
        updatedLatLng.clear();
      }
      int[] ployLineColor = getResources().getIntArray(R.array.ploycolor);
      polylineOptions = new PolylineOptions().
          geodesic(true).
          color(ployLineColor[0]).
          width(10);
      String source = CommonConvertion.sRID_replace(String.valueOf(jsonNodeLocation));
      if (mySharedPrefrencesData == null) {
        mySharedPrefrencesData = new MySharedPrefrencesData();
      }
      if (!GeneralUtil.isStringEmpty(source)) {
        double driverCurrentLat = Double.parseDouble(source.split(" ")[0]);
        double driverCurrentLng = Double.parseDouble(source.split(" ")[1]);
        updateRoute = mMapsApiService.mapApiDirection("driving",
            driverCurrentLat + "," + driverCurrentLng,
            mTrackOrderDataModel.getPickup().latitude
                + ","
                + mTrackOrderDataModel.getPickup().longitude,
            mySharedPrefrencesData.getMapsApiKey(TrackOrderActivity.this),
            "false");

        updateRoute.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<DirectionsResponse>() {
              @Override public void onSubscribe(Disposable d) {
                mDisposable = d;
              }

              @Override public void onNext(DirectionsResponse data) {
                if (data != null && data.getRoutes() != null && data.getRoutes().size() > 0) {

                  if (updatedLatLng != null) {
                    updatedLatLng.clear();
                  }
                  updatedLatLng = CommonConvertion.decodePolyLine(
                      data.getRoutes().get(0).getOverviewPolyline().getPoints());
                  routePolyline = CommonConvertion.convert_to_base64(
                      data.getRoutes().get(0).getOverviewPolyline().getPoints());
                  eta = data.getRoutes().get(0).getLegs().get(0).getDuration().getText();
                  int currentDistance = CommonConvertion.detour(
                      data.getRoutes().get(0).getLegs().get(0).getDistance().getText());
                  if (destinationMarker != null) {
                    destinationMarker.remove();
                  }
                  LayoutInflater inflater =
                      (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                  assert inflater != null;
                  @SuppressLint("InflateParams") View layout =
                      inflater.inflate(R.layout.custom_map_marker, null);
                  TextView endMarker = layout.findViewById(R.id.txtView_map_time);
                  String[] traveltime = eta.split(" ");
                  Calendar currentTime = Calendar.getInstance();
                  Calendar tmp = (Calendar) currentTime.clone();
                  tmp.add(Calendar.HOUR_OF_DAY, 1);
                  if (traveltime.length == 2) {
                    tmp.add(Calendar.MINUTE, Integer.parseInt(traveltime[0]));
                  } else {
                    if (traveltime.length == 4) {
                      tmp.add(Calendar.HOUR_OF_DAY, Integer.parseInt(traveltime[0]));
                      tmp.add(Calendar.MINUTE, Integer.parseInt(traveltime[2]));
                    }
                  }
                  @SuppressLint("SimpleDateFormat")
                  SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                  String currentTimeAMPM = dateFormat.format(tmp.getTime());
                  if (currentDistance <= 50) {
                    endMarker.setText("Reached !");
                    txtEta.setText("Reached");
                    txtEta.setVisibility(View.VISIBLE);
                  } else {
                    endMarker.setText(currentTimeAMPM);
                    txtEta.setText(getString(R.string.trackorder_format_eta, eta));
                    txtEta.setVisibility(View.VISIBLE);
                  }

                  if (sourceMarker != null) {
                    sourceMarker.remove();
                  }

                  sourceMarker = mMap.addMarker(
                      new MarkerOptions().icon(
                          IconUtils.getBikeImageSvgIcon(TrackOrderActivity.this))
                          .flat(true)
                          .position(new LatLng(
                              data.getRoutes().get(0).getLegs().get(0).getStartLocation().getLat(),
                              data.getRoutes()
                                  .get(0)
                                  .getLegs()
                                  .get(0)
                                  .getStartLocation()
                                  .getLng())));

                  destinationMarker = mMap.addMarker(
                      new MarkerOptions().icon(IconUtils.getDropSVGIcon(TrackOrderActivity.this))
                          .position(new LatLng(
                              data.getRoutes().get(0).getLegs().get(0).getEndLocation().getLat(),
                              data.getRoutes().get(0).getLegs().get(0).getEndLocation().getLng())));
                  destinationMarker.setAnchor(0.9f, 0.7f);

                  if (polyline != null) {
                    polyline.getPoints().clear();
                    polyline.remove();
                    polylineOptions.getPoints().clear();
                    polylineOptions.addAll(updatedLatLng);
                    polylineOptions.pattern(null);
                    polyline = mMap.addPolyline(polylineOptions);
                    oldLatLng = polyline.getPoints().get(0);
                    if (polyline.getPoints().size() > 1) {
                      newLatLng = polyline.getPoints().get(1);
                      if (newLatLng != null) {
                        rotationValue = getBearing(oldLatLng, newLatLng);
                      }
                    } else {
                      polylineOptions.addAll(updatedLatLng);
                      polylineOptions.pattern(null);
                      polyline = mMap.addPolyline(polylineOptions);
                      oldLatLng = polyline.getPoints().get(0);
                      if (polyline.getPoints().size() > 1) {
                        newLatLng = polyline.getPoints().get(1);
                        if (newLatLng != null) {
                          rotationValue = getBearing(oldLatLng, newLatLng);
                        }
                      }
                    }
                    sourceMarker
                        .setPosition(
                            new LatLng(
                                data.getRoutes()
                                    .get(0)
                                    .getLegs()
                                    .get(0)
                                    .getStartLocation()
                                    .getLat(),
                                data.getRoutes()
                                    .get(0)
                                    .getLegs()
                                    .get(0)
                                    .getStartLocation()
                                    .getLng()));
                    sourceMarker.setAnchor(0.5f, 0.5f);
                    sourceMarker.setRotation(rotationValue);
                    destinationMarker.setTag(
                        GeneralUtil.getInfoWindowData(mTrackOrderDataModel, false));
                    destinationMarker.showInfoWindow();
                  }
                }
              }

              @Override public void onError(Throwable e) {

              }

              @Override public void onComplete() {

              }
            });
      }
    }
  }

  private void updateRoute(LatLng snapLoc) {
    if (snapLoc != null
        && mTrackOrderDataModel != null
        && mTrackOrderDataModel.getDestination() != null) {
      LatLng destinationLatlng;
      if (updatedLatLng != null) {
        updatedLatLng.clear();
      }
      destinationLatlng = mTrackOrderDataModel.getDestination();
      int[] ployLineColor = this.getResources().getIntArray(R.array.ploycolor);
      polylineOptions = new PolylineOptions().
          geodesic(true).
          color(ployLineColor[0]).
          width(10);
      if (mySharedPrefrencesData == null) {
        mySharedPrefrencesData = new MySharedPrefrencesData();
      }
      updateRoute =
          mMapsApiService.mapApiDirection("driving", snapLoc.latitude + "," + snapLoc.longitude,
              destinationLatlng.latitude + "," + destinationLatlng.longitude,
              mySharedPrefrencesData.getMapsApiKey(TrackOrderActivity.this), "false");
      updateRoute.subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(new Observer<DirectionsResponse>() {
            @Override public void onSubscribe(Disposable d) {
              mDisposable = d;
            }

            @Override public void onNext(DirectionsResponse data) {
              if (data != null && data.getRoutes() != null && data.getRoutes().size() > 0) {
                if (updatedLatLng != null) {
                  updatedLatLng.clear();
                }
                updatedLatLng = CommonConvertion.decodePolyLine(
                    data.getRoutes().get(0).getOverviewPolyline().getPoints());
                routePolyline = CommonConvertion.convert_to_base64(
                    data.getRoutes().get(0).getOverviewPolyline().getPoints());
                eta = data.getRoutes().get(0).getLegs().get(0).getDuration().getText();
                int currentDistance = CommonConvertion.detour(
                    data.getRoutes().get(0).getLegs().get(0).getDistance().getText());
                if (destinationMarker != null) {
                  destinationMarker.remove();
                }
                LayoutInflater inflater =
                    (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                assert inflater != null;
                @SuppressLint("InflateParams") View layout =
                    inflater.inflate(R.layout.custom_map_marker, null);
                TextView endMarker = layout.findViewById(R.id.txtView_map_time);
                String[] traveltime = eta.split(" ");
                Calendar currentTime = Calendar.getInstance();
                Calendar tmp = (Calendar) currentTime.clone();
                tmp.add(Calendar.HOUR_OF_DAY, 1);
                if (traveltime.length == 2) {
                  tmp.add(Calendar.MINUTE, Integer.parseInt(traveltime[0]));
                } else {
                  if (traveltime.length == 4) {
                    tmp.add(Calendar.HOUR_OF_DAY, Integer.parseInt(traveltime[0]));
                    tmp.add(Calendar.MINUTE, Integer.parseInt(traveltime[2]));
                  }
                }
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                String currentTimeAMPM = dateFormat.format(tmp.getTime());
                if (currentDistance <= 50) {
                  endMarker.setText("Reached !");
                } else {
                  endMarker.setText(currentTimeAMPM);
                  txtEta.setText(getString(R.string.trackorder_format_eta, "40 mins"));
                  txtEta.setVisibility(View.VISIBLE);
                }
                if (sourceMarker != null) {
                  sourceMarker.remove();
                }

                sourceMarker = mMap.addMarker(
                    new MarkerOptions().icon(
                        IconUtils.getBikeImageSvgIcon(TrackOrderActivity.this))
                        .flat(true)
                        .position(new LatLng(
                            data.getRoutes().get(0).getLegs().get(0).getStartLocation().getLat(),
                            data.getRoutes()
                                .get(0)
                                .getLegs()
                                .get(0)
                                .getStartLocation()
                                .getLng())));
                destinationMarker = mMap.addMarker(
                    new MarkerOptions().icon(IconUtils.getDropSVGIcon(TrackOrderActivity.this))
                        .position(new LatLng(
                            data.getRoutes().get(0).getLegs().get(0).getEndLocation().getLat(),
                            data.getRoutes().get(0).getLegs().get(0).getEndLocation().getLng())));
                destinationMarker.setAnchor(0.9f, 0.7f);

                if (polyline != null) {
                  polyline.getPoints().clear();
                  polyline.remove();
                  polylineOptions.getPoints().clear();
                  polylineOptions.addAll(updatedLatLng);
                  polylineOptions.pattern(PATTERN_POLYGON_ALPHA);
                  polyline = mMap.addPolyline(polylineOptions);
                  oldLatLng = polyline.getPoints().get(0);
                  if (polyline.getPoints().size() > 1) {
                    newLatLng = polyline.getPoints().get(1);
                    if (newLatLng != null) {
                      rotationValue = getBearing(oldLatLng, newLatLng);
                    }
                  }
                } else {
                  polylineOptions.addAll(updatedLatLng);
                  polylineOptions.pattern(PATTERN_POLYGON_ALPHA);
                  polyline = mMap.addPolyline(polylineOptions);
                  oldLatLng = polyline.getPoints().get(0);
                  if (polyline.getPoints().size() > 1) {
                    newLatLng = polyline.getPoints().get(1);
                    if (newLatLng != null) {
                      rotationValue = getBearing(oldLatLng, newLatLng);
                    }
                  }
                }
                sourceMarker
                    .setPosition(
                        new LatLng(
                            data.getRoutes()
                                .get(0)
                                .getLegs()
                                .get(0)
                                .getStartLocation()
                                .getLat(),
                            data.getRoutes()
                                .get(0)
                                .getLegs()
                                .get(0)
                                .getStartLocation()
                                .getLng()));
                sourceMarker.setAnchor(0.5f, 0.5f);
                sourceMarker.setRotation(rotationValue);
                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(mTrackOrderDataModel.getDestination(),
                        13.5f));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    snapLoc)   // Sets the center of the map to myLocation user
                    .zoom(13.5f)// Sets the zoom
                    .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                destinationMarker.setTag(
                    GeneralUtil.getInfoWindowData(mTrackOrderDataModel, false));
                destinationMarker.showInfoWindow();
              }
            }

            @Override public void onError(Throwable e) {

            }

            @Override public void onComplete() {

            }
          });
    }
  }

  private float getBearing(LatLng begin, LatLng end) {
    double lat = Math.abs(begin.latitude - end.latitude);
    double lng = Math.abs(begin.longitude - end.longitude);

    if (begin.latitude < end.latitude && begin.longitude < end.longitude) {
      return (float) (Math.toDegrees(Math.atan(lng / lat)));
    } else if (begin.latitude >= end.latitude && begin.longitude < end.longitude) {
      return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
    } else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude) {
      return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
    } else if (begin.latitude < end.latitude && begin.longitude >= end.longitude) {
      return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
    }
    return -1;
  }

  private Bitmap getMarkerBitmapFromView(View view) {
    view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
    view.layout(0, 0, view.getMeasuredWidth() / 3, view.getMeasuredHeight() / 3);
    view.buildDrawingCache();
    Bitmap returnedBitmap =
        Bitmap.createBitmap(view.getMeasuredWidth() / 3, view.getMeasuredHeight() / 3,
            Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(returnedBitmap);
    canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
    Drawable drawable = view.getBackground();
    if (drawable != null) {
      drawable.draw(canvas);
    }
    view.draw(canvas);
    return returnedBitmap;
  }

  private void disconnectAllSockets() {
    if (socket != null) {
      try {
        channel.leave();
        socket.removeAllChannels();
        socket.disconnect();
        socket = null;
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if (socketReceiver != null) {
      try {
        channelReceiver.leave();
        socketReceiver.removeAllChannels();
        socketReceiver.disconnect();
        socketReceiver = null;
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @OnClick(R.id.img_back) void onClickBackButton() {
    onBackPressed();
  }

  @OnClick(R.id.img_call) void onClickCallButton() {
    if (mTrackOrderDataModel != null && !GeneralUtil.isStringEmpty(
        mTrackOrderDataModel.getDeliveryBoyNumber())) {
      Dialog_show_call(mTrackOrderDataModel.getDeliveryBoyNumber());
    }
  }

  private void Dialog_show_call(String mobile) {
    BottomSheetDialog dialog = new BottomSheetDialog(TrackOrderActivity.this, R.style.SheetDialog);
    dialog.setContentView(R.layout.call_dialog);
    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
    TextView mobile_no = (TextView) dialog.findViewById(R.id.mobile_no);
    LinearLayout mobile_no_linear = dialog.findViewById(R.id.mobile_no_linear);
    if (mobile_no != null) {
      mobile_no.setText("Call " + mobile);
    }
    if (cancel != null) {
      cancel.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          dialog.dismiss();
        }
      });
    }
    String phone_digit = mobile.trim().replaceAll("[^\\.0123456789]", "").trim();
    if (mobile_no_linear != null) {
      mobile_no_linear.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          call(phone_digit);
          dialog.dismiss();
        }
      });
    }
    if (!isFinishing()) {
      dialog.show();
    }
  }

  private void call(String phone_digit) {
    if (!GeneralUtil.isStringEmpty(phone_digit)) {
      startActivity(IntentUtils.callDialerIntent(phone_digit));
    }
  }

  private void getDeliveryGuyLastKnownLocation(String orderNumber, String deliveryGuyId) {
    showProgressDialog();
    mDeliveryApiService.getDeliveryGuyLastKnownLocation(
        QueryBuilder.getDeliveryGuyLastKnownLocation(deliveryGuyId,
            orderNumber.replaceAll("#", "").trim()))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<ListDriverResponse>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(ListDriverResponse response) {
            dismissProgessLoading();
            if (response.getData() != null
                && response.getData().getListDriverLocation() != null
                && response.getData().getListDriverLocation().size() > 0) {
              drawLastKnownDriverRoute(response.getData().getListDriverLocation().get(0));
            } else if (mTrackOrderDataModel != null && mTrackOrderDataModel.getPickup() != null) {
              updateRoute(mTrackOrderDataModel.getPickup());
            }
          }

          @Override public void onError(Throwable e) {
            dismissProgessLoading();
            if (mTrackOrderDataModel != null && mTrackOrderDataModel.getPickup() != null) {
              updateRoute(mTrackOrderDataModel.getPickup());
            }
          }

          @Override public void onComplete() {

          }
        });
  }

  private void drawLastKnownDriverRoute(ListDriverLocationItem listDriverLocationItem) {
    String source = CommonConvertion.sRID_replace(listDriverLocationItem.getLocation());
    double driverCurrentLat = Double.parseDouble(source.split(" ")[0]);
    double driverCurrentLng = Double.parseDouble(source.split(" ")[1]);
    updateRoute(new LatLng(driverCurrentLat, driverCurrentLng));
  }

  @SuppressLint("CheckResult") private void initLocationProvider() {
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED
        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
      return;
    }
    locationProvider.getLastKnownLocation().subscribe(newLocation -> {
      myLocation = newLocation;
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
          new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 15));
    }, throwable ->
        Log.e("error", Objects.requireNonNull(throwable.getLocalizedMessage())));
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == REQUEST_CHECK_SETTINGS) {
      if (resultCode == RESULT_OK) {
        Toast.makeText(TrackOrderActivity.this, "GPS enabled", Toast.LENGTH_LONG).show();
      } else {
        Toast.makeText(TrackOrderActivity.this, "Gps not enabled", Toast.LENGTH_SHORT).show();
      }
    }
  }

  private void checkLocationPermission() {
    Dexter.withActivity(this)
        .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        .withListener(mCheckLocationPermissionListener)
        .check();
  }

  @Override public void onConnected(@Nullable Bundle bundle) {
    builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
    builder.setAlwaysShow(true);
    PendingResult<LocationSettingsResult> result =
        LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
    result.setResultCallback(this);
  }

  @Override public void onConnectionSuspended(int i) {

  }

  @Override public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
    final Status status = locationSettingsResult.getStatus();
    switch (status.getStatusCode()) {
      case LocationSettingsStatusCodes.SUCCESS:
        initLocationProvider();
        break;

      case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
        try {
          status.startResolutionForResult(TrackOrderActivity.this, REQUEST_CHECK_SETTINGS);
        } catch (IntentSender.SendIntentException e) {
        }
        break;

      case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
        break;
    }
  }

  @Override public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  private void initLocationDialog() {
    mPermissionDeniedDialog =
        new MaterialDialog.Builder(this).title(R.string.general_dialog_enable_location)
            .positiveText(R.string.general_label_gotosetting)
            .negativeText(R.string.general_label_cancel)
            .onPositive(new MaterialDialog.SingleButtonCallback() {
              @Override
              public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                //should open settings page to enable permission
                startActivity(IntentUtils.newSettingsIntent(TrackOrderActivity.this));
              }
            })
            .onNegative(new MaterialDialog.SingleButtonCallback() {
              @Override
              public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                checkLocationPermission();
              }
            })
            .build();
  }

  private void initGoogleApiClient() {
    mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
        .addConnectionCallbacks(TrackOrderActivity.this)
        .addOnConnectionFailedListener(this)
        .build();
    mGoogleApiClient.connect();
  }

  @OnClick(R.id.fab_locate) void onClickLocateButton() {
    if (oldLatLng != null) {
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(oldLatLng, 15));
    } else {
      if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
        initGoogleApiClient();
      }
      initLocationRequest();
      checkLocationPermission();
      initLocationProvider();
    }
  }
}
