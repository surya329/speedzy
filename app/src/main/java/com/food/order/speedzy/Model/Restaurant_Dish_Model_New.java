package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 01-12-2018.
 */

public class Restaurant_Dish_Model_New {

  @SerializedName("offers")
  @Expose
  private List<Restaurant_Dish_Model.NewOffers> offers = null;

  private String status;
  private String avg_order_value;
  private String order_no;

  public String getOrders_count() {
    return orders_count;
  }

  public void setOrders_count(String orders_count) {
    this.orders_count = orders_count;
  }

  private String orders_count;
  private List<Dishitem> Cuisines = null;
  private List<DeliveryFee> delivery_fee = null;

  public List<restaurant_details> getRestaurant_details() {
    return restaurant_details;
  }

  public void setRestaurant_details(List<restaurant_details> restaurant_details) {
    this.restaurant_details = restaurant_details;
  }

  private List<restaurant_details> restaurant_details = null;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public List<DeliveryFee> getDeliveryFee() {
    return delivery_fee;
  }

  public void setDeliveryFee(List<DeliveryFee> deliveryFee) {
    this.delivery_fee = deliveryFee;
  }

  public List<Restaurant_Dish_Model.NewOffers> getOffers() {
    return offers;
  }

  public void setOffers(List<Restaurant_Dish_Model.NewOffers> offers) {
    this.offers = offers;
  }

  public List<Dishitem> getCuisines() {
    return Cuisines;
  }

  public void setCuisines(List<Dishitem> cuisines) {
    Cuisines = cuisines;
  }

  public String getAvg_order_value() {
    return avg_order_value;
  }

  public void setAvg_order_value(String avg_order_value) {
    this.avg_order_value = avg_order_value;
  }

  public String getOrder_no() {
    return order_no;
  }

  public void setOrder_no(String order_no) {
    this.order_no = order_no;
  }

  public class restaurant_details implements Serializable {
    private String restaurant_logo_image;
    private String restaurant_cover_image;
    private String restaurant_main_image;
    private String st_restaurant_name;

    public String getSt_street_address() {
      return st_street_address;
    }

    public void setSt_street_address(String st_street_address) {
      this.st_street_address = st_street_address;
    }

    private String st_street_address;

    public String getSt_suburb() {
      return st_suburb;
    }

    public void setSt_suburb(String st_suburb) {
      this.st_suburb = st_suburb;
    }

    private  String st_suburb;
    private String in_restaurant_id;
    private String avg_rating;

    public String getCount_rating() {
      return count_rating;
    }

    public void setCount_rating(String count_rating) {
      this.count_rating = count_rating;
    }

    private String count_rating;
    private String st_min_order;

    public Restaurant_Dish_Model_New.restaurant_details.restcharge getRestcharge() {
      return restcharge;
    }

    public void setRestcharge(Restaurant_Dish_Model_New.restaurant_details.restcharge restcharge) {
      this.restcharge = restcharge;
    }

    private restcharge restcharge;

    public String getIn_restaurant_id() {
      return in_restaurant_id;
    }

    public void setIn_restaurant_id(String in_restaurant_id) {
      this.in_restaurant_id = in_restaurant_id;
    }

    public String getAvg_rating() {
      return avg_rating;
    }

    public void setAvg_rating(String avg_rating) {
      this.avg_rating = avg_rating;
    }

    public String getSt_min_order() {
      return st_min_order;
    }

    public void setSt_min_order(String st_min_order) {
      this.st_min_order = st_min_order;
    }

    public String getRestaurant_logo_image() {
      return restaurant_logo_image;
    }

    public void setRestaurant_logo_image(String restaurant_logo_image) {
      this.restaurant_logo_image = restaurant_logo_image;
    }

    public String getRestaurant_cover_image() {
      return restaurant_cover_image;
    }

    public void setRestaurant_cover_image(String restaurant_cover_image) {
      this.restaurant_cover_image = restaurant_cover_image;
    }

    public String getRestaurant_main_image() {
      return restaurant_main_image;
    }

    public void setRestaurant_main_image(String restaurant_main_image) {
      this.restaurant_main_image = restaurant_main_image;
    }

    public String getSt_restaurant_name() {
      return st_restaurant_name;
    }

    public void setSt_restaurant_name(String st_restaurant_name) {
      this.st_restaurant_name = st_restaurant_name;
    }
    public class restcharge implements Serializable {

      private String amount;

      public String getAmount() {
        return amount;
      }

      public void setAmount(String amount) {
        this.amount = amount;
      }

      public String getDesc() {
        return desc;
      }

      public void setDesc(String desc) {
        this.desc = desc;
      }

      private String desc;
    }

  }
}
