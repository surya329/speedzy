package com.food.order.speedzy.SpeedzyRideApi.response.LoginRide;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class UserRegistrationModel {

	@SerializedName("emailAddress")
	private String emailAddress;

	@SerializedName("id")
	private String id;

	@SerializedName("mobileNumber")
	private String mobileNumber;

	@SerializedName("name")
	private String name;

	public String getEmailAddress(){
		return emailAddress;
	}

	public String getId(){
		return id;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"userRegistration{" +
			"emailAddress = '" + emailAddress + '\'' +
			",id = '" + id + '\'' +
					",mobileNumber = '" + mobileNumber + '\'' +
			",name = '" + name + '\'' +
			"}";
		}
}