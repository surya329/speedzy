package com.food.order.speedzy.screen.banner;

import com.food.order.speedzy.api.response.banner.BannersItem;
import java.util.ArrayList;
import java.util.List;

public class BannersVM {
  private String mImageName;

  public String getImageName() {
    return mImageName;
  }

  public void setImageName(String imageName) {
    mImageName = imageName;
  }

  public static List<BannersVM> transform(List<BannersItem> data) {
    List<BannersVM> mData = new ArrayList<>();
    if (data != null && data.size() > 0) {
      for (BannersItem vm : data) {
        BannersVM bannersVM = new BannersVM();
        bannersVM.setImageName(vm.getImageName());
        mData.add(bannersVM);
      }
    }
    return mData;
  }
}
