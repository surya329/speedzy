package com.food.order.speedzy.Model;


/**
 * Created by Sujata Mohanty.
 */


public class Preferencemodel {

    private String id="";
    private String priceitem="";
    private String menuprice="";
    private String sectionname="";
    private Boolean ismultiselected;
    private Boolean isselected;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPriceitem() {
        return priceitem;
    }

    public void setPriceitem(String priceitem) {
        this.priceitem = priceitem;
    }

    public String getMenuprice() {
        return menuprice;
    }

    public void setMenuprice(String menuprice) {
        this.menuprice = menuprice;
    }

    public Boolean getIsselected() {
        return isselected;
    }

    public void setIsselected(Boolean isselected) {
        this.isselected = isselected;
    }

    public String getSectionname() {
        return sectionname;
    }

    public void setSectionname(String sectionname) {
        this.sectionname = sectionname;
    }

    public Boolean getIsmultiselected() {
        return ismultiselected;
}

    public void setIsmultiselected(Boolean ismultiselected) {
        this.ismultiselected = ismultiselected;
    }
}
