package com.food.order.speedzy.screen.ordersuccess;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.food.order.speedzy.Activity.HomeActivityNew;
import com.food.order.speedzy.Activity.MyOrderActivity;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.api.DeliveryApiService;
import com.food.order.speedzy.api.DeliveryServiceApiGenerator;
import com.food.order.speedzy.api.QueryBuilder;
import com.food.order.speedzy.api.response.createorder.CreateOrderResponse;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.root.BaseActivity;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ActivityOrderSuccess extends BaseActivity {
  private static final String EXTRA_ORDER_SUCCESS = "EXTRA_ORDER_SUCCESS";
  private ActivityOrderDataModel mActivityOrderDataModel;
  @BindView(R.id.toolbar) Toolbar toolbar;
  private LOcaldbNew lOcaldbNew;
  private DeliveryApiService mDeliveryApiService;
  private int type = 1;

  public static Intent newIntent(Context context, ActivityOrderDataModel dataModel) {
    Intent intent = new Intent(context, ActivityOrderSuccess.class);
    intent.putExtra(EXTRA_ORDER_SUCCESS, dataModel);
    return intent;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_track_order);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
    initComponent();
    updateOrderInServer();
  }

  private void initComponent() {
    mDeliveryApiService =
        DeliveryServiceApiGenerator.provideRetrofit(this).create(DeliveryApiService.class);
    if (getIntent().getExtras() != null) {
      mActivityOrderDataModel = getIntent().getExtras().getParcelable(EXTRA_ORDER_SUCCESS);
    }
  }

  private void updateOrderInServer() {
    if (mActivityOrderDataModel != null && !GeneralUtil.isStringEmpty(
        mActivityOrderDataModel.getOrderId())) {
      Log.e("create query", QueryBuilder.createOrder(mActivityOrderDataModel.getOrderId()));
      showProgressDialog();
      mDeliveryApiService.createOrder(
          QueryBuilder.createOrder(mActivityOrderDataModel.getOrderId()))
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(new Observer<CreateOrderResponse>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(CreateOrderResponse response) {
              dismissProgressDialog();
            }

            @Override public void onError(Throwable e) {
              dismissProgressDialog();
              Log.e("error", e.getLocalizedMessage());
            }

            @Override public void onComplete() {

            }
          });
    }
  }

  @OnClick(R.id.btn_trackorder) void onClickTrackOrder() {
    goToOrderPage();
  }

  private void goToOrderPage() {
    if (Commons.flag_for_hta != null) {
      if (Commons.flag_for_hta.equals("Grocerry")
          || Commons.flag_for_hta.equals("Patanjali")) {
        //lOcaldbNew.deleteCart_patanjali();
        type = 2;
      } else if (Commons.flag_for_hta.equals("City Special")) {
        //lOcaldbNew.deleteCart_CitySpecial();
        type = 2;
      } else if (Commons.flag_for_hta.equals("Drinking Water")) {
        //lOcaldbNew.deleteCart_Water();
        type = 2;
      }
      Intent intent = new Intent(this, MyOrderActivity.class);
      intent.putExtra("flag", 1);
      intent.putExtra("type", type);
      finish();
      startActivity(intent);
      overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    } else {
      Intent intent = new Intent(this, MyOrderActivity.class);
      intent.putExtra("flag", 1);
      intent.putExtra("type", type);
      finish();
      startActivity(intent);
      overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
  }

  @Override public void onBackPressed() {
    super.onBackPressed();
    Intent intent = new Intent(ActivityOrderSuccess.this, HomeActivityNew.class);
    intent.putExtra("flag", 1);
    startActivity(intent);
    Commons.back_button_transition(ActivityOrderSuccess.this);
  }
}
