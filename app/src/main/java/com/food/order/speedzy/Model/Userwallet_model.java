package com.food.order.speedzy.Model;

import java.io.Serializable;

/**
 * Created by Sujata Mohanty.
 */

public class Userwallet_model implements Serializable {
    private String status;
    private String wallet;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }
}
