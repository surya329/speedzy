package com.food.order.speedzy.api.response.grocery;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CategoriesItem {

  //@SerializedName("parent")
  //private String parent;

  @SerializedName("image")
  private String image;

  @SerializedName("sub_categories")
  private List<SubCategoriesItem> subCategories;

  //@SerializedName("product_count")
  //private String productCount;

  @SerializedName("name")
  private String name;

  @SerializedName("id")
  private String id;

  private boolean mSelected;

  public boolean isSelected() {
    return mSelected;
  }

  public void setSelected(boolean selected) {
    mSelected = selected;
  }

  //public String getParent() {
  //  return parent;
  //}

  public String getImage() {
    return image;
  }

  public List<SubCategoriesItem> getSubCategories() {
    return subCategories;
  }


  public String getName() {
    return name;
  }

  public String getId() {
    return id;
  }

}