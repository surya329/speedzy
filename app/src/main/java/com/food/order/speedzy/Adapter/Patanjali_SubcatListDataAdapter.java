package com.food.order.speedzy.Adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.food.order.speedzy.R;
import com.food.order.speedzy.screen.patanjali.GroceryChildList;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class Patanjali_SubcatListDataAdapter
    extends RecyclerView.Adapter<Patanjali_SubcatListDataAdapter.MyView> {

  private List<GroceryChildList> list;
  private Patanjali_SubcatListDataAdapter.OnItemClickListener mOnItemClickListener;
  Context context;
  int row_index = 0;

  public Patanjali_SubcatListDataAdapter(Context context, List<GroceryChildList> list) {
    this.context = context;
    this.list = list;
    GroceryChildList model = new GroceryChildList();
    model.setId("0");
    model.setName("All");
    list.add(0,model);
  }

  public interface OnItemClickListener {
    void onItemClick(View view, int position);
  }

  public void setOnItemClickListener(
      final Patanjali_SubcatListDataAdapter.OnItemClickListener mItemClickListener) {
    this.mOnItemClickListener = mItemClickListener;
  }

  public class MyView extends RecyclerView.ViewHolder {

    public TextView textView;

    public MyView(View view) {
      super(view);

      textView = (TextView) view.findViewById(R.id.textview1);
    }
  }

  @Override
  public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

    View itemView =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_item, parent, false);

    return new MyView(itemView);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  @Override
  public void onBindViewHolder(final MyView holder, final int position) {

    holder.textView.setText(list.get(position).getName());
    holder.textView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (mOnItemClickListener != null) {
          mOnItemClickListener.onItemClick(view, position);
          row_index = position;
          notifyDataSetChanged();
        }
      }
    });
    if (row_index == position) {
      holder.textView.setTextColor(context.getResources().getColor(R.color.red));
      holder.textView.setBackground(context.getDrawable(R.drawable.rectangle_border_red_new));
    } else {
      holder.textView.setTextColor(context.getResources().getColor(R.color.grey));
      holder.textView.setBackground(context.getDrawable(R.drawable.rectangle_border_grey));
    }
  }

  @Override
  public int getItemCount() {
    return list.size();
  }
}
