package com.food.order.speedzy.Activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import androidx.annotation.Nullable;

import com.food.order.speedzy.root.BaseActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.content.ContextCompat;
import android.os.Bundle;
import androidx.core.widget.CompoundButtonCompat;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.Model.CategoryModel;
import com.food.order.speedzy.Model.StorepackagerModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendPackages extends BaseActivity {
    private View viewToAttachDisplayerTo;
    boolean network_status=true;
    MySharedPrefrencesData mySharedPrefrencesData;
    String Veg_Flag,userid="";
    private Toolbar toolbar;
    TextView text1,text2,text3,text4,text5,confirm;
    ImageView image1,image2,image3;
    View view1,view2;
    int flag=0;
    double lati1,lati2,longi1,longi2;
    String cat="";
    EditText comment;
    List<CategoryModel.cat> categoryModels=new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_packages_new);

        viewToAttachDisplayerTo = findViewById(R.id.displayerAttachableView);

        mySharedPrefrencesData=new MySharedPrefrencesData();
        Veg_Flag=mySharedPrefrencesData.getVeg_Flag(this);
        userid = mySharedPrefrencesData.getUser_Id(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Veg_Flag.equalsIgnoreCase("1")){
            statusbar_bg(R.color.red);
        }else {
            statusbar_bg(R.color.red);
        }

        text1 = findViewById(R.id.text1);
        text2 =  findViewById(R.id.text2);
        text3 =  findViewById(R.id.text3);
        text4 =  findViewById(R.id.text4);
        text5 =  findViewById(R.id.text5);
        confirm=findViewById(R.id.confirm);
        image1 =  findViewById(R.id.image1);
        image2 =  findViewById(R.id.image2);
        image3 =  findViewById(R.id.image3);
        view1 =  findViewById(R.id.view1);
        view2 =  findViewById(R.id.view2);
        comment= findViewById(R.id.comment);

        text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SendPackages.this, Saved_Addresses.class);
                intent.putExtra("change_address", true);
                startActivityForResult(intent, 1);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
        text4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!text2.getText().toString().equalsIgnoreCase("Search pickup location")) {
                    flag=1;
                    Intent intent = new Intent(SendPackages.this, Saved_Addresses.class);
                    intent.putExtra("change_address", true);
                    startActivityForResult(intent, 1);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }else{
                    Toast.makeText(SendPackages.this,"Please Search pickup location",Toast.LENGTH_SHORT).show();
                }
            }
        });

        BottomSheetDialog dialog_payment = new BottomSheetDialog(this, R.style.SheetDialog);
        dialog_payment.setContentView(R.layout.category_dialog);
        TextView cancel=dialog_payment.findViewById(R.id.cancel);
        TextView done=dialog_payment.findViewById(R.id.done);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_payment.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str="";
                for(int i=0;i<categoryModels.size();i++)
                {
                    if (categoryModels.get(i).isclicked)
                    {
                        str=str+categoryModels.get(i).getName()+",";
                        cat=cat+categoryModels.get(i).getId()+",";
                    }
                }
                text5.setText(str.substring(0,str.length()-1));
                image3.setImageDrawable(getResources().getDrawable(R.drawable.check_round));
                dialog_payment.dismiss();
            }
        });
        apicall(dialog_payment);
        text5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!text2.getText().toString().equalsIgnoreCase("Search pickup location")) {
                    if (!text4.getText().toString().equalsIgnoreCase("Search delivery location")) {
                        dialog_payment.show();
                    }else{
                        Toast.makeText(SendPackages.this,"Please Search delivery location",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(SendPackages.this,"Please Search pickup location",Toast.LENGTH_SHORT).show();
                }
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!text2.getText().toString().equalsIgnoreCase("Search pickup location")) {
                    if (!text4.getText().toString().equalsIgnoreCase("Search delivery location")) {
                        if (!text5.getText().toString().equalsIgnoreCase("Select category")) {
                            apicall_confirm();
                        }else{
                            Toast.makeText(SendPackages.this,"Please Select category",Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(SendPackages.this,"Please Search delivery location",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(SendPackages.this,"Please Search pickup location",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void apicall_confirm() {
        double distance=getdistance();
        String category=cat.substring(0,cat.length()-1);
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<StorepackagerModel> call = methods.storepackager(userid,text2.getText().toString(),
                text4.getText().toString(),category, String.valueOf(distance),comment.getText().toString());
        Log.d("url", "url=" + call.request().url().toString());
        call.enqueue(new Callback<StorepackagerModel>() {
            @Override
            public void onResponse(Call<StorepackagerModel> call, Response<StorepackagerModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + statusCode);
                Log.d("respones", "" + response);
                StorepackagerModel deviceUpdateModel = response.body();
                if (deviceUpdateModel.getStatus().equalsIgnoreCase("Success")) {
                    Toast.makeText(getApplicationContext(), deviceUpdateModel.getMsg(),Toast.LENGTH_LONG).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<StorepackagerModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private double getdistance() {
        Location startPoint=new Location("locationA");
        startPoint.setLatitude(lati1);
        startPoint.setLongitude(longi1);

        Location endPoint=new Location("locationA");
        endPoint.setLatitude(lati2);
        endPoint.setLongitude(longi2);

        double distance=startPoint.distanceTo(endPoint);
        return distance;
    }

    private void apicall(BottomSheetDialog dialog_payment) {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<CategoryModel> call = methods.packagercat();
        Log.d("url", "url=" + call.request().url().toString());
        call.enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                CategoryModel response1 = response.body();
                if (response1.getStatus().equalsIgnoreCase("Success")) {
                    categoryModels.addAll(response1.getCat());
                    ListView listView = (ListView) dialog_payment.findViewById(R.id.listview);
                    listView.invalidate();
                    listView.setAdapter(new MyAdapter());

                }
            }

            @Override
            public void onFailure(Call<CategoryModel> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), "internet not available..connect internet",
                //    Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            if (requestCode == 1) {
                String message = data.getStringExtra("SELECTED_LOCATION");
                String type=data.getStringExtra("type");
                String lati=data.getStringExtra("lati");
                String longi=data.getStringExtra("longi");
                if (flag==0) {
                    lati1= Double.parseDouble(lati);
                    longi1= Double.parseDouble(longi);
                    text1.setText(type);
                    text1.setVisibility(View.VISIBLE);
                    text2.setText(message);
                    image1.setImageDrawable(getResources().getDrawable(R.drawable.check_round));
                    view1.setBackgroundColor(getResources().getColor(R.color.red));
                   /* ViewGroup.LayoutParams params = view1.getLayoutParams();
                    params.height = 400;
                    view1.setLayoutParams(params);*/
                }
                if (flag==1) {
                    lati2= Double.parseDouble(lati);
                    longi2= Double.parseDouble(longi);
                    text3.setText(type);
                    text3.setVisibility(View.VISIBLE);
                    text4.setText(message);
                    image2.setImageDrawable(getResources().getDrawable(R.drawable.check_round));
                    view2.setBackgroundColor(getResources().getColor(R.color.red));
                   /* ViewGroup.LayoutParams params = view2.getLayoutParams();
                    params.height = 400;
                    view2.setLayoutParams(params);*/
                }
            }
        super.onActivityResult(requestCode, resultCode, data);
    }
    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,color ));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Commons.back_button_transition(SendPackages.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Commons.back_button_transition(SendPackages.this);
    }
   @Override
    public void onStart() {
        super.onStart();
    }

    public class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return categoryModels.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row = null;
            row = View.inflate(getApplicationContext(), R.layout.list_cat_row, null);
            TextView tvContent=(TextView) row.findViewById(R.id.tvContent);
            tvContent.setText(categoryModels.get(position).getName());

            final CheckBox cb = (CheckBox) row.findViewById(R.id.chbContent);
            int states[][] = {{android.R.attr.state_checked}, {}};
            int colors[] = {R.color.red, R.color.grey};
            CompoundButtonCompat.setButtonTintList(cb, new ColorStateList(states, colors));
            cb.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (categoryModels.get(position).isclicked) {
                        categoryModels.get(position).isclicked = false;
                    } else {
                        categoryModels.get(position).isclicked = true;
                    }
                }
            });

            if (categoryModels.get(position).isclicked) {

                cb.setChecked(true);
            }
            else {
                cb.setChecked(false);
            }
            return row;
        }

    }

}
