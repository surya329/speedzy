package com.food.order.speedzy.screen.operationsclosed;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.Toolbar;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.root.BaseActivity;
import com.food.order.speedzy.screen.home.SpeedzyOperationsClosedModel;
import java.util.Objects;

public class OperationsClosedActivity extends BaseActivity {
  private static final String EXTRA_OPERATIONS_CLOSED_MODEL = "EXTRA_OPERATIONS_CLOSED_MODEL";
  @BindView(R.id.img_operation_closed) ImageView imgClosed;
  @BindView(R.id.img_operation_closed_banner) ImageView imgClosedBanner;
  @BindView(R.id.txt_reason) TextView txtReason;
  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.title) TextView txtTitle;
  String Veg_Flag;
  MySharedPrefrencesData mySharedPrefrencesData;
  private SpeedzyOperationsClosedModel mOperationsClosedDataModel;

  public static Intent newIntent(Context context,
      SpeedzyOperationsClosedModel operationsClosedDataModel) {
    Intent intent = new Intent(context, OperationsClosedActivity.class);
    intent.putExtra(EXTRA_OPERATIONS_CLOSED_MODEL, operationsClosedDataModel);
    return intent;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_operations_closed);
    ButterKnife.bind(this);
    mySharedPrefrencesData = new MySharedPrefrencesData();
    Veg_Flag = mySharedPrefrencesData.getVeg_Flag(this);
    setSupportActionBar(toolbar);
    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    initComponent();
    setUIData(mOperationsClosedDataModel);
    if (Veg_Flag.equalsIgnoreCase("1")) {
      statusbar_bg(R.color.red);
    } else {
      statusbar_bg(R.color.red);
    }
  }
  private void statusbar_bg(int color) {
    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
            .getColor(color)));
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
    }
  }

  private void setUIData(SpeedzyOperationsClosedModel operationsClosedDataModel) {
    if (mOperationsClosedDataModel != null) {
      txtReason.setText(!GeneralUtil.isStringEmpty(operationsClosedDataModel.getCloseMessage())
          ? operationsClosedDataModel.getCloseMessage() : "We are Closed Today");
      txtTitle.setText(!GeneralUtil.isStringEmpty(operationsClosedDataModel.getCloseTitle())
          ? operationsClosedDataModel.getCloseTitle() : "We are Closed Today");
      if (!GeneralUtil.isStringEmpty(operationsClosedDataModel.getClosingIcon())) {
        Glide.with(this)
            .load(
                SpeedzyConstants.STORAGE_BASE_BANNER_URL
                    + operationsClosedDataModel.getClosingIcon())
                .apply(new RequestOptions()
                        .placeholder(R.color.grey))
            .into(imgClosed);
      }
      if (!GeneralUtil.isStringEmpty(operationsClosedDataModel.getClosingBanner())) {
        Glide.with(this)
            .load(
                SpeedzyConstants.STORAGE_BASE_BANNER_URL
                    + operationsClosedDataModel.getClosingBanner())
                .apply(new RequestOptions()
            .placeholder(R.color.grey))
            .into(imgClosedBanner);
      }
    }
  }

  private void initComponent() {
    if (getIntent().getExtras() != null) {
      mOperationsClosedDataModel =
          getIntent().getExtras().getParcelable(EXTRA_OPERATIONS_CLOSED_MODEL);
    }
  }

  @OnClick(R.id.btn_home) void onClickGobackToHomePage() {
    /*Intent intent = new Intent(OperationsClosedActivity.this, HomeActivityNew.class);
    intent.putExtra("flag", 1);
    startActivity(intent);*/
    Commons.back_button_transition(OperationsClosedActivity.this);
  }

  @Override public void onBackPressed() {
    super.onBackPressed();
    Commons.back_button_transition(OperationsClosedActivity.this);
  }
}
