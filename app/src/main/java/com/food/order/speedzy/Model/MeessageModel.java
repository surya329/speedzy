package com.food.order.speedzy.Model;

import java.io.Serializable;

public class MeessageModel implements Serializable {
    private  String msg1;

    public MeessageModel(int image, String msg, String msg1, String msg2) {
        this.image=image;
        this.msg=msg;
        this.msg1=msg1;
        this.msg2=msg2;
    }

    public String getMsg1() {
        return msg1;
    }

    public void setMsg1(String msg1) {
        this.msg1 = msg1;
    }

    public String getMsg2() {
        return msg2;
    }

    public void setMsg2(String msg2) {
        this.msg2 = msg2;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    private String msg2;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private String msg;
    private Integer image;
}
