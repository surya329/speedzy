package com.food.order.speedzy.Adapter;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;

public class OrderImageAdapter extends RecyclerView.Adapter<OrderImageAdapter.ViewHolder> {
  private final FirebaseStorage mFirebaseStorage;
  private String mOrderType = SpeedzyConstants.GROCERY_ORDER;
  private List<String> mData;
  Context context;

  public OrderImageAdapter(
      Context context) {
    mFirebaseStorage = FirebaseStorage.getInstance(SpeedzyConstants.FIREBASE_SPEEDZY_BUCKET);
    this.context = context;
    this.mData = new ArrayList<>();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery, parent, false);
    System.out.println("View Holder");
    // set the view's size, margins, paddings and layout parameters
    ViewHolder vh = new ViewHolder(v);
    return vh;
  }

  @Override
  public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
    if (mOrderType.equals(SpeedzyConstants.GROCERY_ORDER) && !GeneralUtil.isStringEmpty(
        mData.get(position))) {
      mFirebaseStorage.getReference()
              .child(SpeedzyConstants.FIREBASE_GROCERY)
              .child(SpeedzyConstants.FIREBASE_ORIGINAL)
              .child(mData.get(position)).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
        @Override
        public void onSuccess(Uri uri) {
          Glide.with(getApplicationContext()).load(uri.toString())
                  .apply(new RequestOptions()
                          .placeholder(R.color.grey)
                          .error(R.drawable.combo_placeholder)
                          .fitCenter())
                  .into(holder.itemImage);
        }
      }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception exception) {
          // Handle any errors
        }
      });
    } else if (!GeneralUtil.isStringEmpty(
        mData.get(position))) {
      mFirebaseStorage.getReference()
              .child(SpeedzyConstants.FIREBASE_MEDICINE)
              .child(SpeedzyConstants.FIREBASE_ORIGINAL)
              .child(mData.get(position)).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
        @Override
        public void onSuccess(Uri uri) {
          Glide.with(getApplicationContext()).load(uri.toString())
                  .apply(new RequestOptions()
                          .placeholder(R.color.grey)
                          .error(R.drawable.combo_placeholder)
                          .fitCenter())
                  .into(holder.itemImage);
        }
      }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception exception) {
          // Handle any errors
        }
      });
    }
    holder.clear.setVisibility(View.GONE);
  }

  @Override
  public int getItemCount() {
    return mData.size();
  }

  public void setData(List<String> data) {
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public void setOrderType(String orderType) {
    mOrderType = orderType;
  }

  public void refreshData(List<String> data) {
    Log.e("recieved data", String.valueOf(data));
    mData.clear();
    setData(data);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    ImageView itemImage;
    LinearLayout clear;
    public ViewHolder(View itemView) {
      super(itemView);
      itemImage = (ImageView) itemView.findViewById(R.id.itemImage);
      clear = (LinearLayout) itemView.findViewById(R.id.clear);
    }
  }

  public interface Callback {
    void onClickDeleteItem(Uri uri);
  }
}

