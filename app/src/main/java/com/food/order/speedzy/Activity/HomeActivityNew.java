package com.food.order.speedzy.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.food.order.speedzy.FCM.Config;
import com.food.order.speedzy.FCM.NotificationUtils;
import com.food.order.speedzy.Fragment.Help_Support_Fragment;
import com.food.order.speedzy.Fragment.Home_New_Fragment;
import com.food.order.speedzy.NetworkConnectivity.NetworkStateReceiver;
import com.food.order.speedzy.R;
import com.food.order.speedzy.RoomBooking.RoomBookingActivity;
import com.food.order.speedzy.SpeedzyRide.SearchActivity_New;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SharedPreferenceSingleton;
import com.food.order.speedzy.api.response.DeviceUpdateModel;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.root.BaseActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Objects;

/**
 * Created by Sujata Mohanty.
 */

public class HomeActivityNew extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener
    /*,GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener*/{
  static Fragment fragment1 = null;
  static Fragment fragment2 = null;
  static Fragment fragment3 = null;
  static Fragment fragment5 = null;
  ImageView home, my_order, meal_combo, city_special,  cake, patanjali, help, term, about,
      privacy, upload_patanjali, offer, wallet, profile;
  Toolbar toolbar;
  LinearLayout profile_linear,water;
  public static TextView current_address;
 /* private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
  private GoogleApiClient mGoogleApiClient;
  private LocationRequest mLocationRequest;*/
  MySharedPrefrencesData mySharedPrefrencesData;
  TextView name, mobile, veg_txt;
  LOcaldbNew lOcaldbNew;
  String name_str, mobile_str, Veg_Flag;
  boolean doubleBackToExitPressedOnce = false;
  ImageView navigation, search_resturant;
  public static ImageView cart;
  DrawerLayout drawer;
  private BroadcastReceiver mRegistrationBroadcastReceiver;
  //List<Patanjali_Category_model.categories> list;
  int flag = 0;
  int first_time_load = 0;
  private static String message = "";
  String refreshedToken,app_version,device_type,device_name,device_version,userid;
  SharedPreferences prefs;
  private NetworkStateReceiver networkStateReceiver;
  Dialog dialog;
  private boolean mInBackground;
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_home_new);
      ButterKnife.bind(this);
      if (android.os.Build.VERSION.SDK_INT >= 21) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().setStatusBarColor(
            ContextCompat.getColor(HomeActivityNew.this, R.color.red));
      }

    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
    networkStateReceiver = new NetworkStateReceiver();
    networkStateReceiver.addListener(this);
    this.registerReceiver(networkStateReceiver, intentFilter);
      initView();
  }

  private void DeviceInformation() {
    userid=mySharedPrefrencesData.getUser_Id(this);
    Commons.deviceid = Settings.Secure.getString(HomeActivityNew.this.getContentResolver(),
            Settings.Secure.ANDROID_ID);
    Log.d("device_name", "5 " + Commons.deviceid);
    app_version=getCurrentVersion();
     device_type=getDeviceName();
     device_name = BluetoothAdapter.getDefaultAdapter().getName();
     device_version=Build.VERSION.RELEASE;
    Log.d("device_name", "1 " + app_version);
    Log.d("device_name", "2 " + device_type);
    Log.d("device_name", "3 " + device_name);
    Log.d("device_name", "4 " + device_version);

    //if (!prefs.getBoolean("FIRSTTime", false)) {

      // run your one time code
      ApiCallUpdateDevice();
   // }
  }

  private void ApiCallUpdateDevice() {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    Call<DeviceUpdateModel> call = methods.set_updatedeviceid(Commons.deviceid,userid,refreshedToken,"1",device_name,device_type,
                                          "android",device_version,app_version);
    Log.d("url", "url=" + call.request().url().toString());
    call.enqueue(new Callback<DeviceUpdateModel>() {
      @Override
      public void onResponse(Call<DeviceUpdateModel> call, Response<DeviceUpdateModel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        DeviceUpdateModel deviceUpdateModel = response.body();
        if (deviceUpdateModel.getStatus().equalsIgnoreCase("Success")) {
         // Toast.makeText(getApplicationContext(), deviceUpdateModel.getMsg(),Toast.LENGTH_LONG).show();
          SharedPreferences.Editor editor = prefs.edit();
          editor.putBoolean("FIRSTTime", true);
          editor.commit();
        }
      }

      @Override
      public void onFailure(Call<DeviceUpdateModel> call, Throwable t) {
        Toast.makeText(getApplicationContext(), "internet not available..connect internet",
                Toast.LENGTH_LONG).show();
      }
    });
  }

  public String getCurrentVersion() {
    String currentVersion = "";
    if (this != null) {
      PackageManager pm = getPackageManager();
      PackageInfo pInfo = null;

      try {
        pInfo = pm.getPackageInfo(getPackageName(), 0);
      } catch (PackageManager.NameNotFoundException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }
      if (pInfo != null) {
        currentVersion = pInfo.versionName;
      }

    }
    return currentVersion;
  }
  public String getDeviceName() {
    String manufacturer = Build.MANUFACTURER;
    String model = Build.MODEL;
    if (model.startsWith(manufacturer)) {
      return capitalize(model);
    } else {
      return capitalize(manufacturer) + " " + model;
    }
  }
  private String capitalize(String s) {
    if (s == null || s.length() == 0) {
      return "";
    }
    char first = s.charAt(0);
    if (Character.isUpperCase(first)) {
      return s;
    } else {
      return Character.toUpperCase(first) + s.substring(1);
    }
  }

  private void veg_nonveg_fun(int colorcode) {
    home.setColorFilter(color(colorcode));
    my_order.setColorFilter(color(colorcode));
    meal_combo.setColorFilter(color(colorcode));
    city_special.setColorFilter(color(colorcode));
    cake.setColorFilter(color(colorcode));
    //water.setColorFilter(color(colorcode));
    patanjali.setColorFilter(color(colorcode));
    upload_patanjali.setColorFilter(color(colorcode));
    help.setColorFilter(color(colorcode));
    term.setColorFilter(color(colorcode));
    about.setColorFilter(color(colorcode));
    privacy.setColorFilter(color(colorcode));
    offer.setColorFilter(color(colorcode));
    wallet.setColorFilter(color(colorcode));
    profile.setColorFilter(color(colorcode));
    statusbar_bg(colorcode);
    home_fragment_call();
  }

  private void home_fragment_call() {
    first_time_load=1;
    fragment1 = new Home_New_Fragment();
    Fragment fragment = fragment1;
    showFragment(fragment);
  }

  private void statusbar_bg(int color) {
    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
        .getColor(color)));
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    // check if the request code is same as what is passed  here it is 2
    if (requestCode == 1) {
      if (data != null) {
        message = data.getStringExtra("SELECTED_LOCATION");
        address_change(message);
      }
    }
  }

  private void address_change(String message1) {
    current_address.setText(message1);
    //Home_New_Fragment.callapi();
  }
  private void initView() {
    SharedPreferenceSingleton.getInstance().init(this);
    String fcm = SharedPreferenceSingleton.getInstance().getStringPreference("FCMTOKEN");

    prefs = PreferenceManager.getDefaultSharedPreferences(this);
    refreshedToken = FirebaseInstanceId.getInstance().getToken();
    Log.e("FCMTOKEN",refreshedToken);
    //Toast.makeText(getApplicationContext(),refreshedToken.toString(),Toast.LENGTH_SHORT).show();

    drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    lOcaldbNew = new LOcaldbNew(this);
    mySharedPrefrencesData = new MySharedPrefrencesData();
    DeviceInformation();
    name_str =
            mySharedPrefrencesData.getFName(this) + " " + mySharedPrefrencesData.getLName(this);
    mobile_str = mySharedPrefrencesData.get_Party_mobile(this);
    Veg_Flag = mySharedPrefrencesData.getVeg_Flag(this);
    flag = getIntent().getIntExtra("flag", 0);

    /*  mGoogleApiClient = new GoogleApiClient.Builder(this)
          .addConnectionCallbacks(this)
          .addOnConnectionFailedListener(this)
          .addApi(LocationServices.API)
          .build();
      mLocationRequest = LocationRequest.create()
          .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
          .setInterval(10 * 1000)        // 10 seconds, in milliseconds
          .setFastestInterval(1 * 1000); // 1 second, in milliseconds*/

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    navigation = (ImageView) findViewById(R.id.navigation);
    search_resturant = (ImageView) findViewById(R.id.search_resturant);
    cart = (ImageView) findViewById(R.id.cart);
    home = (ImageView) findViewById(R.id.home);
    my_order = (ImageView) findViewById(R.id.order_img);
    meal_combo = (ImageView) findViewById(R.id.meal_combo);
    city_special = (ImageView) findViewById(R.id.city_special_img);
    cake = (ImageView) findViewById(R.id.cake);
    water = (LinearLayout) findViewById(R.id.water);
    patanjali = (ImageView) findViewById(R.id.patanjali);
    help = (ImageView) findViewById(R.id.help);
    term = (ImageView) findViewById(R.id.term);
    about = (ImageView) findViewById(R.id.about);
    privacy = (ImageView) findViewById(R.id.privacy);
    upload_patanjali = (ImageView) findViewById(R.id.upload_patanjali);
    offer = (ImageView) findViewById(R.id.offer);
    wallet = (ImageView) findViewById(R.id.wallet);
    profile = (ImageView) findViewById(R.id.profile);
    profile_linear = (LinearLayout) findViewById(R.id.profile_linear);
    Switch aSwitch = (Switch) findViewById(R.id.switchButton);

    current_address = (TextView) findViewById(R.id.current_address);
    name = (TextView) findViewById(R.id.name);
    mobile = (TextView) findViewById(R.id.mobile);
    veg_txt = (TextView) findViewById(R.id.veg_txt);
    current_address.setVisibility(View.GONE);
    //current_address.setVisibility(View.VISIBLE);
    HomeActivityNew.current_address.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_location, 0, R.drawable.ic_drop, 0);
    current_address.setText(mySharedPrefrencesData.getLocation(this));
    if (mySharedPrefrencesData.getFName(this).equalsIgnoreCase("")) {
      name.setText("Guest");
    } else {
      name.setText(name_str);
    }
    mobile.setText("+91-" + mobile_str);
    current_address.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
                  /*  String location = current_address.getText().toString();
                    if (select_location.equalsIgnoreCase(location)) {
                        Intent intent = new Intent(HomeActivityNew.this, LocationActivity.class);
                        startActivityForResult(intent, 1);
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    }*/
        Intent intent = new Intent(HomeActivityNew.this, Saved_Addresses.class);
        intent.putExtra("change_address", false);
        startActivityForResult(intent, 1);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                   /* Intent intent = new Intent(HomeActivityNew.this, LocationActivity.class);
                    startActivityForResult(intent, 1);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);*/

      }
    });

    navigation.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        drawer.openDrawer(GravityCompat.START);
      }
    });
    search_resturant.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(HomeActivityNew.this, Resturant_SearchActivity.class));
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
      }
    });
    profile_linear.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(HomeActivityNew.this, Edit_Profile.class));
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        drawer.closeDrawers();
      }
    });

           /* if (Veg_Flag.equalsIgnoreCase("1")) {
                statusbar_bg(R.color.white);
                aSwitch.setChecked(true);
                veg_txt.setTextColor(color(R.color.red));
                veg_nonveg_fun(R.color.red);
            } else {
                statusbar_bg(R.color.white);
                aSwitch.setChecked(false);
                veg_txt.setTextColor(color(R.color.white));
                veg_nonveg_fun(R.color.red);
            }*/

    aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          veg_txt.setTextColor(color(R.color.red));
          mySharedPrefrencesData.setVeg_Flag(HomeActivityNew.this, "1");
          veg_nonveg_fun(R.color.red);
        } else {
          veg_txt.setTextColor(color(R.color.white));
          mySharedPrefrencesData.setVeg_Flag(HomeActivityNew.this, "0");
          veg_nonveg_fun(R.color.red);
        }
      }
    });
    mRegistrationBroadcastReceiver = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {

        // checking for type intent filter
        if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
          // gcm successfully registered
          // now subscribe to `global` topic to receive app wide notifications
          FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
        } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
          // new push notification is received

          String message = intent.getStringExtra("message");

          Toast.makeText(getApplicationContext(), "Push notification: " + message,
                  Toast.LENGTH_LONG).show();
        }
      }
    };

    drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
      @Override
      public void onDrawerSlide(View drawerView, float slideOffset) {

      }

      @Override
      public void onDrawerOpened(View drawerView) {
        mobile_str = mySharedPrefrencesData.get_Party_mobile(HomeActivityNew.this);
        name_str = mySharedPrefrencesData.getFName(HomeActivityNew.this)
                + " "
                + mySharedPrefrencesData.getLName(HomeActivityNew.this);
        mobile.setText("+91-" + mobile_str);
        if (mySharedPrefrencesData.getFName(HomeActivityNew.this).equalsIgnoreCase("")) {
          name.setText("Guest");
        } else {
          name.setText(name_str);
        }
      }

      @Override
      public void onDrawerClosed(View drawerView) {

      }

      @Override
      public void onDrawerStateChanged(int newState) {

      }
    });
  }

  @Override
  protected void onPause() {
    super.onPause();
  /*  if (mGoogleApiClient.isConnected()) {
      LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
      mGoogleApiClient.disconnect();
    }*/
    mInBackground = true;
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
  }

 /* @Override
  public void onConnected(Bundle bundle) {
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED
        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
      return;
    }
    Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    if (location == null) {
      LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
          this);
    }
  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onConnectionFailed(ConnectionResult connectionResult) {
    if (connectionResult.hasResolution()) {
      try {
        // Start an Activity that tries to resolve the error
        connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
        *//*
         * Thrown if Google Play services canceled the original
         * PendingIntent
         *//*
      } catch (IntentSender.SendIntentException e) {
        // Log the error
        e.printStackTrace();
      }
    } else {
    }
  }

  @Override
  public void onLocationChanged(Location location) {
  }
*/
  public void ClickNavigation(View view) {
    Fragment fragment = null;
    switch (view.getId()) {
      case R.id.home_linear:
        current_address.setText(mySharedPrefrencesData.getLocation(this));
        if (fragment1 == null) fragment1 = new Home_New_Fragment();
        fragment = fragment1;
        showFragment(fragment);
        break;
      case R.id.ll_send_packages:
        Intent packages = new Intent(HomeActivityNew.this, SearchActivity_New.class);
        packages.putExtra("search_type", 0);
        startActivity(packages);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        break;
      case R.id.ll_speedzy_ride:
        Intent ride = new Intent(HomeActivityNew.this, SearchActivity_New.class);
        ride.putExtra("search_type", 1);
        startActivity(ride);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        break;
      case R.id.ll_room_booking:
        Intent room = new Intent(HomeActivityNew.this, RoomBookingActivity.class);
        startActivity(room);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        break;
      case R.id.my_order_linear:
        Intent order = new Intent(HomeActivityNew.this, MyOrderActivity.class);
        order.putExtra("flag", 0);
        order.putExtra("type", 1);
        startActivity(order);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        break;
      case R.id.meal_combo_linear:
        Commons.menu_id = "M1021";
        Intent intent = new Intent(HomeActivityNew.this, MealComboActivity.class);
        intent.putExtra("app_tittle", "Meal Combo");
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        break;
      case R.id.city_special_linear:
        callApi(1);
         break;
      case R.id.cake_linear:
        callApi(2);
        break;
      //case R.id.water_linear:
      //  callApi(3);
      //  break;
      case R.id.offer_linear:
        Intent offer = new Intent(HomeActivityNew.this, MealComboActivity.class);
        offer.putExtra("app_tittle", "Offer Zone");
        startActivity(offer);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        break;
      case R.id.patanjali_linear:
        Commons.menu_id = "M1004";
        Commons.restaurant_id = "";
        Commons.flag_for_hta = "Patanjali";
        Intent i = new Intent(HomeActivityNew.this, PatanjaliProductActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        //Commons.menu_id = "M1016";
        //Commons.flag_for_hta = "City Special";
        //Intent i = new Intent(this, PatanjaliProduct_DetailsActivity.class);
        //i.putExtra("app_tittle", "City Special");
        //startActivity(i);
        //overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        break;
      case R.id.wallet_linear:
        startActivity(new Intent(HomeActivityNew.this, WalletActivityNew.class));
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        break;
      case R.id.profile_linear1:
        startActivity(new Intent(HomeActivityNew.this, Edit_Profile.class));
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        break;
      case R.id.help_linear:
        if (fragment2 == null) fragment2 = new Help_Support_Fragment("1");
        fragment = fragment2;
        showFragment(fragment);
        break;
      case R.id.term_linear:
        if (fragment3 == null) fragment3 = new Help_Support_Fragment("2");
        fragment = fragment3;
        showFragment(fragment);
        break;
      //case R.id.about_linear:
      //  if (fragment4 == null) fragment4 = new Help_Support_Fragment("3");
      //  fragment = fragment4;
      //  showFragment(fragment);
      //  break;
      case R.id.privacy_linear:
        if (fragment5 == null) fragment5 = new Help_Support_Fragment("4");
        fragment = fragment5;
        showFragment(fragment);
        break;
      case R.id.ll_address:
        Intent intent1=new Intent(HomeActivityNew.this, Saved_Addresses.class);
        intent1.putExtra("change_address", false);
        startActivity(intent1);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        break;
      case R.id.ll_referal:
        openReferralIntent(mySharedPrefrencesData.getReferal(this));
        break;
      case R.id.ll_logout:
        dialogshow();
        break;

      default:
        break;
    }
    drawer.closeDrawer(GravityCompat.START);
  }

  private void openReferralIntent(String referralcode) {
    try {
      Intent i = new Intent(Intent.ACTION_SEND);
      i.setType("text/plain");
      i.putExtra(Intent.EXTRA_SUBJECT, "Speedzy");
      String sAux =
          "Register on Speedzy with " + referralcode + " and avail attractive offers.Download on ";
      sAux = sAux
          + "https://play.google.com/store/apps/details?id=com.food.order.speedzy&referrer=utm_source="
          + referralcode;
      i.putExtra(Intent.EXTRA_TEXT, sAux);
      startActivity(Intent.createChooser(i, "Choose One"));
    } catch (Exception e) {
      //e.toString();
    }
  }

  private void dialogshow() {
    final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    Objects.requireNonNull(dialog.getWindow())
        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.setContentView(R.layout.logout_popup);
    TextView yes = (TextView) dialog.findViewById(R.id.yes);
    TextView no = (TextView) dialog.findViewById(R.id.no);
    no.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
      }
    });
    yes.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mySharedPrefrencesData.setLoginStatus(HomeActivityNew.this, false);
        mySharedPrefrencesData.clearAllSharedData(HomeActivityNew.this);
        mySharedPrefrencesData.cart_clearAllSharedData(HomeActivityNew.this);
        lOcaldbNew.clearDatabase();
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(HomeActivityNew.this, SignupSignin_New.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        if (dialog != null && dialog.isShowing()) {
          dialog.dismiss();
        }
        startActivity(intent);
      }
    });
    if (!((Activity) this).isFinishing()) {
      dialog.show();
    }
  }

  public void callApi(int i) {
    if (i == 1) {
      Commons.menu_id = "M1016";
      Commons.flag_for_hta = "City Special";
    } else if (i == 2) {
      Commons.menu_id = "M1007";
      Commons.flag_for_hta = "Cakes";
    } else if (i == 3) {
      Commons.menu_id = "M1026";
      Commons.flag_for_hta = "Drinking Water";
    }

    Intent intent = new Intent(HomeActivityNew.this, PatanjaliProduct_DetailsActivity.class);
    if (i == 1) {
      intent.putExtra("app_tittle", "City Special");
    } else if (i == 2) {
      lOcaldbNew.deleteCart();
      intent.putExtra("app_tittle", "Cakes");
    } else if (i == 3) {
      intent.putExtra("app_tittle", "Drinking Water");
    }
    startActivity(intent);
    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
  }

  private void showFragment(Fragment fragment) {
    FragmentManager supportFragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
    fragmentTransaction.replace(R.id.container, fragment).addToBackStack(null).commit();
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    }
    if (doubleBackToExitPressedOnce) {
      moveTaskToBack(true);
      android.os.Process.killProcess(android.os.Process.myPid());
      System.exit(0);
      return;
    }
    this.doubleBackToExitPressedOnce = true;
    Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        doubleBackToExitPressedOnce = false;
      }
    }, 2000);
  }

  @ColorInt
  private int color(@ColorRes int res) {
    return ContextCompat.getColor(this, res);
  }


    @Override
    public void onResume() {
        super.onResume();
      mInBackground = false;

      LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
              new IntentFilter(Config.REGISTRATION_COMPLETE));
      LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
              new IntentFilter(Config.PUSH_NOTIFICATION));
      NotificationUtils.clearNotifications(getApplicationContext());
     /* if (flag == 1) {
        home_fragment_call();
        flag++;
      }*/
//    address_change(mySharedPrefrencesData.getLocation(this));
    }
  @Override
  protected void onDestroy() {
    super.onDestroy();
    networkStateReceiver.removeListener(this);
    this.unregisterReceiver(networkStateReceiver);
  }

  @Override
  public void networkAvailable() {
    if (dialog != null && dialog.isShowing() && !isFinishing()) {
      dialog.dismiss();
    }
    if (first_time_load==0) {
      home_fragment_call();
    }
  }
  @Override
  public void networkUnavailable() {
    popupshow();
  }
  private void popupshow() {
    if (dialog == null) {
      dialog = new Dialog(this);
      dialog.setContentView(R.layout.connection_popup);
      dialog.setCancelable(false);
      Objects.requireNonNull(dialog.getWindow())
              .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
    if (!isInBackground()) {
      dialog.show();
    }
  }

  protected boolean isInBackground() {
    return mInBackground;
  }
}




