
package com.food.order.speedzy.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.food.order.speedzy.Model.Cart_Model;
import com.food.order.speedzy.Model.DeliveryAddressModel;
import com.food.order.speedzy.Model.Patanjali_product_details;
import com.food.order.speedzy.Model.Payment_OrderDetail;
import com.food.order.speedzy.Model.Preferencemodel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Sujata Mohanty.
 */

public class LOcaldbNew extends SQLiteOpenHelper {

  public static final String db_name = "OzFoodHunterdb2";

  private String Table_Dish_Info = "dish_info";
  private String Fruit_Info = "fruit_info";
  private String id = "id";
  private String restaurant_id = "in_restaurant_id";
  private String dish_id = "in_dish_id";
  private String dish_name = "st_dish_name";
  private String dish_variety_name = "price_item";
  private String dish_variety_price = "menu_price";
  private String delivery_type = "delivery_type";
  private String quantity = "quantity";
  private String max_qty_peruser = "max_qty_peruser";
  private String totalprice = "totalprice";
  private String prefdetails = "prefdetails";
  private String non_veg_status = "non_veg_status";
  private String cake_flg = "cake_flg";
  private String restaurant_image = "st_restaurant_image";
  private String restaurant_name = "st_restaurant_name";
  private String restaurant_address = "st_street_address";
  private String restaurant_suburb = "st_suburb";
  private String restaurant_postcode = "st_postcode";
  private String rest_charge_amount = "st_rest_charge_amount";
  private String rest_charge_desc = "st_rest_charge_desc";

  private String Table_Preference_Info = "dish_preference_info";
  private String preference_dish_id = "Preference_dish_id";
  private String preference_id = "preferenceid";
  private String preference_type = "type";
  private String preference_name = "attribute_name";
  private String preference_price = "attribute_price";

  private String Ptanjali_order_Info = "Ptanjali_item_info";
  private String Ptanjali_id = "Ptanjali_id";
  private String Ptanjali_item_id = "Ptanjali_in_dish_id";
  private String Ptanjali_item_name = "Ptanjali_st_dish_name";
  private String Ptanjali_item_variety_price = "Ptanjali_menu_price";
  private String Ptanjali_delivery_type = "Ptanjali_delivery_type";
  private String Ptanjali_quantity = "Ptanjali_quantity";
  private String Ptanjali_totalprice = "Ptanjali_totalprice";

  private String CitySpecial_order_Info = "CitySpecial_item_info";
  private String CitySpecial_id = "CitySpecial_id";
  private String CitySpecial_item_id = "CitySpecial_in_dish_id";
  private String CitySpecial_item_name = "CitySpecial_st_dish_name";
  private String CitySpecial_item_variety_price = "CitySpecial_menu_price";
  private String CitySpecial_delivery_type = "CitySpecial_delivery_type";
  private String CitySpecial_quantity = "CitySpecial_quantity";
  private String CitySpecial_totalprice = "CitySpecial_totalprice";

  private String Water_order_Info = "Water_item_info";
  private String Water_id = "Water_id";
  private String Water_item_id = "Water_in_dish_id";
  private String Water_item_name = "Water_st_dish_name";
  private String Water_item_variety_price = "Water_menu_price";
  private String Water_delivery_type = "Water_delivery_type";
  private String Water_quantity = "Water_quantity";
  private String Water_totalprice = "Water_totalprice";

  private static final String RecentSearch_Addresses = "RecentSearchAddresses";
  private static final String CONTACTS_COLUMN_ID = "id";
  private static final String CONTACTS_COLUMN_USERID = "userid";
  private static final String CONTACTS_COLUMN_ADDRESS = "address";
  private static final String CONTACTS_COLUMN_LATITUDE = "latitude";
  private static final String CONTACTS_COLUMN_LONGITUDE = "longitude";

  public static final int old_db_version =13;
  public static final int new_db_version = 14;

  public LOcaldbNew(Context context) {

    super(context, db_name, null, new_db_version);
    Log.d("database:", "database created");
  }

  @Override
  public void onCreate(SQLiteDatabase sqLiteDatabase) {
    String CREATE_DISH = "CREATE TABLE " + Table_Dish_Info + "("
        + id + " INTEGER PRIMARY KEY NOT NULL UNIQUE,"
        + restaurant_id + " VARCHAR,"
        + dish_id + " VARCHAR,"
        + dish_name + " VARCHAR,"
        + dish_variety_name + " VARCHAR,"
        + dish_variety_price + " FLOAT,"
        + delivery_type + " VARCHAR,"
        + non_veg_status + " VARCHAR,"
        + cake_flg + " VARCHAR,"
        + quantity + " INTEGER,"
        + prefdetails + " TEXT,"
        + totalprice + " FLOAT,"
        + restaurant_name + " VARCHAR,"
        + restaurant_address + " VARCHAR,"
        + restaurant_suburb + " VARCHAR,"
        + restaurant_postcode + " VARCHAR,"
        + rest_charge_amount + " VARCHAR,"
        + rest_charge_desc + " VARCHAR,"
            + max_qty_peruser + " INTEGER,"
            + restaurant_image + " VARCHAR)";
    String Ptanjali_CREATE_DISH = "CREATE TABLE " + Ptanjali_order_Info + "("
        + Ptanjali_id + " INTEGER PRIMARY KEY NOT NULL UNIQUE,"
        + Ptanjali_item_id + " VARCHAR,"
        + Ptanjali_item_name + " VARCHAR,"
        + Ptanjali_item_variety_price + " FLOAT,"
        + Ptanjali_delivery_type + " VARCHAR,"
        + Ptanjali_quantity + " INTEGER,"
        + Ptanjali_totalprice + " FLOAT )";
    String CitySpecial_CREATE_DISH = "CREATE TABLE " + CitySpecial_order_Info + "("
        + CitySpecial_id + " INTEGER PRIMARY KEY NOT NULL UNIQUE,"
        + CitySpecial_item_id + " VARCHAR,"
        + CitySpecial_item_name + " VARCHAR,"
        + CitySpecial_item_variety_price + " FLOAT,"
        + CitySpecial_delivery_type + " VARCHAR,"
        + CitySpecial_quantity + " INTEGER,"
        + CitySpecial_totalprice + " FLOAT )";
    String Water_CREATE_DISH = "CREATE TABLE " + Water_order_Info + "("
        + Water_id + " INTEGER PRIMARY KEY NOT NULL UNIQUE,"
        + Water_item_id + " VARCHAR,"
        + Water_item_name + " VARCHAR,"
        + Water_item_variety_price + " FLOAT,"
        + Water_delivery_type + " VARCHAR,"
        + Water_quantity + " INTEGER,"
        + Water_totalprice + " FLOAT )";
    String SeachRecentAddresses = "CREATE TABLE " + RecentSearch_Addresses +
        "(id integer primary key, userid text, address text,latitude text,longitude text)";

    String CREATE_FRUIT_TABLE = "CREATE TABLE " + Fruit_Info + "("
        + id + " INTEGER PRIMARY KEY NOT NULL UNIQUE,"
        + restaurant_id + " VARCHAR,"
        + dish_id + " VARCHAR,"
        + dish_name + " VARCHAR,"
        + dish_variety_name + " VARCHAR,"
        + dish_variety_price + " FLOAT,"
        + delivery_type + " VARCHAR,"
        + non_veg_status + " VARCHAR,"
        + cake_flg + " VARCHAR,"
        + quantity + " INTEGER,"
        + prefdetails + " TEXT,"
        + totalprice + " FLOAT,"
        + restaurant_name + " VARCHAR,"
        + restaurant_address + " VARCHAR,"
        + restaurant_suburb + " VARCHAR,"
            + restaurant_postcode + " VARCHAR,"
            + max_qty_peruser + " INTEGER,"
            + restaurant_image + " VARCHAR)";
    try {
      sqLiteDatabase.execSQL(SeachRecentAddresses);
      sqLiteDatabase.execSQL(Ptanjali_CREATE_DISH);
      sqLiteDatabase.execSQL(CitySpecial_CREATE_DISH);
      sqLiteDatabase.execSQL(Water_CREATE_DISH);
      sqLiteDatabase.execSQL(CREATE_DISH);
      sqLiteDatabase.execSQL(CREATE_FRUIT_TABLE);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
   /* String ALTER_TABLE_ADD_RES_CHARGE = "ALTER TABLE "
        + Table_Dish_Info + " ADD COLUMN " + rest_charge_amount + " VARCHAR;";
    String ALTER_TABLE_ADD_RES_DESCRIPTION = "ALTER TABLE "
        + Table_Dish_Info + " ADD COLUMN " + rest_charge_desc + " VARCHAR;";*/
    if (oldVersion != newVersion) {
      db.execSQL("DROP TABLE IF EXISTS " + Table_Dish_Info);
      db.execSQL("DROP TABLE IF EXISTS " + Ptanjali_order_Info);
      db.execSQL("DROP TABLE IF EXISTS " + CitySpecial_order_Info);
      db.execSQL("DROP TABLE IF EXISTS " + Water_order_Info);
      db.execSQL("DROP TABLE IF EXISTS " + RecentSearch_Addresses);
      db.execSQL("DROP TABLE IF EXISTS " + Fruit_Info);
      onCreate(db);
     /* if (oldVersion < 8) {
        db.execSQL(ALTER_TABLE_ADD_RES_CHARGE);
        db.execSQL(ALTER_TABLE_ADD_RES_DESCRIPTION);
      }*/
    }
  }

  public void clearDatabase() {
    SQLiteDatabase db = this.getWritableDatabase();
    db.execSQL("DELETE FROM " + Table_Dish_Info);
    db.execSQL("DELETE FROM " + Ptanjali_order_Info);
    db.execSQL("DELETE FROM " + CitySpecial_order_Info);
    db.execSQL("DELETE FROM " + Water_order_Info);
    db.execSQL("DELETE FROM " + RecentSearch_Addresses);
    db.execSQL("DELETE FROM " + Fruit_Info);
    db.close();
  }

  public boolean insertRecentSearchAdd(String userid, String address, String latitude,
      String longitude) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues contentValues = new ContentValues();
    contentValues.put("userid", userid);
    contentValues.put("address", address);
    contentValues.put("latitude", latitude);
    contentValues.put("longitude", longitude);
    db.insert("RecentSearchAddresses", null, contentValues);
    return true;
  }

  public ArrayList<DeliveryAddressModel> getAllRecentSearchAddress(String userid) {
    ArrayList<DeliveryAddressModel> array_list = new ArrayList<>();

    //hp = new HashMap();
    SQLiteDatabase db = this.getReadableDatabase();
    try {
      Cursor res =
          db.rawQuery("select * from RecentSearchAddresses where userid=" + userid + "", null);
      res.moveToFirst();

      while (!res.isAfterLast()) {

        DeliveryAddressModel deliveryData = new DeliveryAddressModel();
        deliveryData.setId(res.getString(res.getColumnIndex(CONTACTS_COLUMN_ID)));
        deliveryData.setUserId(res.getString(res.getColumnIndex(CONTACTS_COLUMN_USERID)));
        deliveryData.setLocation(res.getString(res.getColumnIndex(CONTACTS_COLUMN_ADDRESS)));
        deliveryData.setLatitude(res.getString(res.getColumnIndex(CONTACTS_COLUMN_LATITUDE)));
        deliveryData.setLongitude(res.getString(res.getColumnIndex(CONTACTS_COLUMN_LONGITUDE)));
        array_list.add(deliveryData);

        res.moveToNext();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return array_list;
  }

  public ArrayList<Cart_Model.Cart_Details> getCart_Details_Ptanjali() {
    ArrayList<Cart_Model.Cart_Details> cd = new ArrayList<>();
    try {
      SQLiteDatabase db = this.getReadableDatabase();
      String query = "SELECT  * FROM " + Ptanjali_order_Info + " ORDER BY " +
          Ptanjali_item_id + " DESC";
      Cursor cursor = db.rawQuery(query, null);
      while (cursor.moveToNext()) {
        Cart_Model.Cart_Details d = new Cart_Model().new Cart_Details();
        d.setId(cursor.getString(0));
        d.setIn_dish_id(cursor.getString(1));
        d.setSt_dish_name(cursor.getString(2));
        d.setMenu_price(cursor.getString(3));
        d.setDelivery_type(cursor.getString(4));
        d.setQuantity(cursor.getString(5));
        cd.add(d);
      }
      db.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return cd;
  }

  public int getQuantity_Ptanjali() {
    int status = 0;
    SQLiteDatabase db = this.getReadableDatabase();
    String query = "select count(1) from " + Ptanjali_order_Info;
    Cursor cursor = db.rawQuery(query, null);
    int cnt = cursor.getCount();
    while (cursor.moveToNext()) {
      status = cursor.getInt(0);
    }
    cursor.close();
    db.close();
    return status;
  }

  public void insertTable_dish_info_Ptanjali(String dishid, String dishname, float dvp,
      String del_type, int qty, float tp) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues cv = new ContentValues();
    cv.put(Ptanjali_item_id, dishid);
    cv.put(Ptanjali_item_name, dishname);
    cv.put(Ptanjali_item_variety_price, dvp);
    cv.put(Ptanjali_delivery_type, del_type);
    cv.put(Ptanjali_quantity, qty);
    cv.put(Ptanjali_totalprice, tp);
    Long k = db.insert(Ptanjali_order_Info, null, cv);
    Log.d("insert:", "" + k);
    db.close();
  }

  public void updateCart_Ptanjali(String qty, String new_dish_id) {
    SQLiteDatabase db = this.getWritableDatabase();
    String query = "update "
        + Ptanjali_order_Info
        + " set "
        + Ptanjali_quantity
        + " = "
        + qty
        + " where "
        + Ptanjali_item_id
        + "='"
        + new_dish_id
        + "'";
    db.execSQL(query);
    Cursor cursor = db.rawQuery(query, null);
    db.close();
  }

  public void deletecart_Ptanjali(String new_dish_id) {
    SQLiteDatabase db = this.getWritableDatabase();
    db.execSQL("DELETE FROM "
        + Ptanjali_order_Info
        + " WHERE "
        + Ptanjali_item_id
        + "='"
        + new_dish_id
        + "'");
    db.close();
  }

  public ArrayList<Cart_Model.Cart_Details> getCart_Details_CitySpecial() {
    ArrayList<Cart_Model.Cart_Details> cd = new ArrayList<>();
    try {
      SQLiteDatabase db = this.getReadableDatabase();
      String query = "SELECT  * FROM " + CitySpecial_order_Info + " ORDER BY " +
          CitySpecial_item_id + " DESC";
      Cursor cursor = db.rawQuery(query, null);
      while (cursor.moveToNext()) {
        Cart_Model.Cart_Details d = new Cart_Model().new Cart_Details();
        d.setId(cursor.getString(0));
        d.setIn_dish_id(cursor.getString(1));
        d.setSt_dish_name(cursor.getString(2));
        d.setMenu_price(cursor.getString(3));
        d.setDelivery_type(cursor.getString(4));
        d.setQuantity(cursor.getString(5));
        cd.add(d);
      }
      db.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return cd;
  }

  public int getQuantity_CitySpecial() {
    int status = 0;
    SQLiteDatabase db = this.getReadableDatabase();
    String query = "select count(1) from " + CitySpecial_order_Info;
    Cursor cursor = db.rawQuery(query, null);
    int cnt = cursor.getCount();
    while (cursor.moveToNext()) {
      status = cursor.getInt(0);
    }
    cursor.close();
    db.close();
    return status;
  }

  public void insertTable_dish_info_CitySpecial(String dishid, String dishname, float dvp,
      String del_type, int qty, float tp) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues cv = new ContentValues();
    cv.put(CitySpecial_item_id, dishid);
    cv.put(CitySpecial_item_name, dishname);
    cv.put(CitySpecial_item_variety_price, dvp);
    cv.put(CitySpecial_delivery_type, del_type);
    cv.put(CitySpecial_quantity, qty);
    cv.put(CitySpecial_totalprice, tp);
    Long k = db.insert(CitySpecial_order_Info, null, cv);
    Log.d("insert:", "" + k);
    db.close();
  }

  public void updateCart_CitySpecial(String qty, String new_dish_id) {
    SQLiteDatabase db = this.getWritableDatabase();
    String query = "update "
        + CitySpecial_order_Info
        + " set "
        + CitySpecial_quantity
        + " = "
        + qty
        + " where "
        + CitySpecial_item_id
        + "='"
        + new_dish_id
        + "'";
    db.execSQL(query);
    Cursor cursor = db.rawQuery(query, null);
    db.close();
  }

  public void deletecart_CitySpecial(String new_dish_id) {
    SQLiteDatabase db = this.getWritableDatabase();
    db.execSQL("DELETE FROM "
        + CitySpecial_order_Info
        + " WHERE "
        + CitySpecial_item_id
        + "='"
        + new_dish_id
        + "'");
    db.close();
  }

  public ArrayList<Cart_Model.Cart_Details> getCart_Details_Water() {
    ArrayList<Cart_Model.Cart_Details> cd = new ArrayList<>();
    try {
      SQLiteDatabase db = this.getReadableDatabase();
      String query = "SELECT  * FROM " + Water_order_Info + " ORDER BY " +
          Water_item_id + " DESC";
      Cursor cursor = db.rawQuery(query, null);
      while (cursor.moveToNext()) {
        Cart_Model.Cart_Details d = new Cart_Model().new Cart_Details();
        d.setId(cursor.getString(0));
        d.setIn_dish_id(cursor.getString(1));
        d.setSt_dish_name(cursor.getString(2));
        d.setMenu_price(cursor.getString(3));
        d.setDelivery_type(cursor.getString(4));
        d.setQuantity(cursor.getString(5));
        cd.add(d);
      }
      db.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return cd;
  }

  public int getQuantity_Water() {
    int status = 0;
    SQLiteDatabase db = this.getReadableDatabase();
    String query = "select count(1) from " + Water_order_Info;
    Cursor cursor = db.rawQuery(query, null);
    int cnt = cursor.getCount();
    while (cursor.moveToNext()) {
      status = cursor.getInt(0);
    }
    cursor.close();
    db.close();
    return status;
  }

  public void insertTable_dish_info_Water(String dishid, String dishname, float dvp,
      String del_type, int qty, float tp) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues cv = new ContentValues();
    cv.put(Water_item_id, dishid);
    cv.put(Water_item_name, dishname);
    cv.put(Water_item_variety_price, dvp);
    cv.put(Water_delivery_type, del_type);
    cv.put(Water_quantity, qty);
    cv.put(Water_totalprice, tp);
    Long k = db.insert(Water_order_Info, null, cv);
    Log.d("insert:", "" + k);
    db.close();
  }

  public void updateCart_Water(String qty, String new_dish_id) {
    SQLiteDatabase db = this.getWritableDatabase();
    String query = "update "
        + Water_order_Info
        + " set "
        + Water_quantity
        + " = "
        + qty
        + " where "
        + Water_item_id
        + "='"
        + new_dish_id
        + "'";
    db.execSQL(query);
    Cursor cursor = db.rawQuery(query, null);
    db.close();
  }

  public void deletecart_Water(String new_dish_id) {
    SQLiteDatabase db = this.getWritableDatabase();
    db.execSQL(
        "DELETE FROM " + Water_order_Info + " WHERE " + Water_item_id + "='" + new_dish_id + "'");
    db.close();
  }

  public void insertTable_dish_info(String rest_id, String dishid, String dishname, String dvn,
      float dvp, String del_type, String non_veg_flag, String cake_flag, int qty,int max_qty,
      String preferenceinsert, float tp, String res_name, String res_address, String suburb,
      String postcode, String amount, String desc,String res_image) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues cv = new ContentValues();
    cv.put(restaurant_id, rest_id);
    cv.put(dish_id, dishid);
    cv.put(dish_name, dishname);
    cv.put(dish_variety_name, dvn);
    cv.put(dish_variety_price, dvp);
    cv.put(delivery_type, del_type);
    cv.put(non_veg_status, non_veg_flag);
    cv.put(cake_flg, cake_flag);
    cv.put(quantity, qty);
    cv.put(max_qty_peruser, max_qty);
    cv.put(prefdetails, preferenceinsert);
    cv.put(totalprice, tp);
    cv.put(restaurant_name, res_name);
    cv.put(restaurant_address, res_address);
    cv.put(restaurant_suburb, suburb);
    cv.put(restaurant_postcode, postcode);
    cv.put(rest_charge_amount, amount);
    cv.put(rest_charge_desc, desc);
    cv.put(restaurant_image, res_image);
    Long k = db.insert(Table_Dish_Info, null, cv);
    Log.d("insert:", "" + k);
    db.close();
  }

  public boolean checkIfCartContainsSomeOtherRestaurantData(String rest_id) {
    boolean status = false;
    SQLiteDatabase db = this.getReadableDatabase();
    String query =
        "select * from " + Table_Dish_Info + " WHERE " + restaurant_id + " != " + rest_id;
    Cursor cursor = db.rawQuery(query, null);
    int cnt = cursor.getCount();
    if (cnt > 0) {
      status = true;
    }
    cursor.close();
    db.close();
    return status;
  }

  public String getRestaurantIdFromCart() {
    String restId = "";
    SQLiteDatabase db = this.getReadableDatabase();
    String query =
        "select * from " + Table_Dish_Info;
    Cursor cursor = db.rawQuery(query, null);
    int cnt = cursor.getCount();
    while (cursor.moveToNext()) {
      restId = cursor.getString(1);
    }
    cursor.close();
    db.close();
    return restId;
  }

  public int getQuantity(String rest_id) {
    int status = 0;
    SQLiteDatabase db = this.getReadableDatabase();
    /*String query = "select count(1) from "
        + Table_Dish_Info
        + " WHERE "
        + restaurant_id
        + "='"
        + rest_id
        + "'";*/
    String query = "select count(1) from " + Table_Dish_Info;
    Cursor cursor = db.rawQuery(query, null);
    int cnt = cursor.getCount();
    while (cursor.moveToNext()) {
      status = cursor.getInt(0);
    }
    cursor.close();
    db.close();
    return status;
  }


  public ArrayList<Cart_Model.Cart_Details> checkIfCartContainsSameDishWithPreference(
      String new_dish_id, String preference) {
    ArrayList<Cart_Model.Cart_Details> cd = new ArrayList<>();

    SQLiteDatabase db = this.getReadableDatabase();
    String query = "select * from "
        + Table_Dish_Info
        + " WHERE "
        + dish_id
        + " = '"
        + new_dish_id
        + "'"
        + " AND "
        + prefdetails
        + " ='"
        + preference
        + "'";
    Cursor cursor = db.rawQuery(query, null);
    while (cursor.moveToNext()) {
      Cart_Model.Cart_Details d = new Cart_Model().new Cart_Details();
      ArrayList<ArrayList<Preferencemodel>> prefmodellist = new ArrayList<>();
      d.setId(cursor.getString(0));
      d.setIn_restaurant_id(cursor.getString(1));
      d.setIn_dish_id(cursor.getString(2));
      d.setSt_dish_name(cursor.getString(3));
      d.setPrice_item(cursor.getString(4));
      d.setMenu_price(cursor.getString(5));
      d.setDelivery_type(cursor.getString(6));
      d.setNon_veg_status(cursor.getString(7));
      d.setCake_flg(cursor.getString(8));
      d.setQuantity(cursor.getString(9));
      d.setMax_qty_peruser(cursor.getString(18));
      d.setSt_restaurant_image(cursor.getString(19));
      String preflist = cursor.getString(10);

      if (preflist.length() > 0) {
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonarray = (JsonArray) jsonParser.parse(preflist);
        for (int i = 0; i < jsonarray.size(); i++) {
          JsonArray newarray = (JsonArray) jsonarray.get(i);
          String str = String.valueOf(newarray);
          JsonArray myarray = (JsonArray) jsonParser.parse(str);
          ArrayList<Preferencemodel> newChildArray = new ArrayList<>();

          for (int j = 0; j < myarray.size(); j++) {
            Preferencemodel preferencemodel = new Preferencemodel();

            String newstr = String.valueOf(myarray.get(j));
            JsonElement jsonElement = new JsonParser().parse(newstr);
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            //     String json=new Gson().toJson(jsonObject);
            Map<String, String> map = new HashMap<String, String>();
            map = (Map<String, String>) new Gson().fromJson(jsonObject, map.getClass());

            String id = map.get("id");
            String priceitem = map.get("priceitem");
            String menuprice = map.get("menuprice");
            String sectionname = map.get("sectionname");

            preferencemodel.setPriceitem(priceitem);
            preferencemodel.setMenuprice(menuprice);
            preferencemodel.setSectionname(sectionname);
            preferencemodel.setId(id);

            newChildArray.add(preferencemodel);
          }
          prefmodellist.add(newChildArray);
        }
      }
      d.setPreferencelfromdb(prefmodellist);
      d.setTotal_price(cursor.getString(11));
      cd.add(d);
    }
    cursor.close();
    db.close();
    return cd;
  }

  public ArrayList<Cart_Model.Cart_Details> getCart_Details(/*String rest_id*/) {
    ArrayList<Cart_Model.Cart_Details> cd = null;
    try {
      cd = new ArrayList<>();
      SQLiteDatabase db = this.getReadableDatabase();
      String query = "select * from " + Table_Dish_Info/*+" WHERE "+restaurant_id +" = "+ rest_id*/;
      Cursor cursor = db.rawQuery(query, null);
      while (cursor.moveToNext()) {
        Cart_Model.Cart_Details d = new Cart_Model().new Cart_Details();
        ArrayList<ArrayList<Preferencemodel>> prefmodellist = new ArrayList<>();
        d.setId(cursor.getString(0));
        d.setIn_restaurant_id(cursor.getString(1));
        d.setIn_dish_id(cursor.getString(2));
        d.setSt_dish_name(cursor.getString(3));
        d.setPrice_item(cursor.getString(4));
        d.setMenu_price(cursor.getString(5));
        d.setDelivery_type(cursor.getString(6));
        d.setNon_veg_status(cursor.getString(7));
        d.setCake_flg(cursor.getString(8));
        d.setQuantity(cursor.getString(9));
        d.setMax_qty_peruser(cursor.getString(18));
        d.setSt_restaurant_image(cursor.getString(19));
        String preflist = cursor.getString(10);

        if (preflist.length() > 0) {
          JsonParser jsonParser = new JsonParser();
          JsonArray jsonarray = (JsonArray) jsonParser.parse(preflist);
          for (int i = 0; i < jsonarray.size(); i++) {
            JsonArray newarray = (JsonArray) jsonarray.get(i);
            String str = String.valueOf(newarray);
            JsonArray myarray = (JsonArray) jsonParser.parse(str);
            ArrayList<Preferencemodel> newChildArray = new ArrayList<>();

            for (int j = 0; j < myarray.size(); j++) {
              Preferencemodel preferencemodel = new Preferencemodel();

              String newstr = String.valueOf(myarray.get(j));
              JsonElement jsonElement = new JsonParser().parse(newstr);
              JsonObject jsonObject = jsonElement.getAsJsonObject();

              Type type = new TypeToken<Map<String, String>>() {
              }.getType();
              Map<String, String> map = new Gson().fromJson(jsonObject, type);

              String id = map.get("id");
              String priceitem = map.get("priceitem");
              String menuprice = map.get("menuprice");
              String sectionname = map.get("sectionname");

              preferencemodel.setPriceitem(priceitem);
              preferencemodel.setMenuprice(menuprice);
              preferencemodel.setSectionname(sectionname);
              preferencemodel.setId(id);

              newChildArray.add(preferencemodel);
            }
            prefmodellist.add(newChildArray);
          }
        }
        d.setPreferencelfromdb(prefmodellist);
        d.setTotal_price(cursor.getString(11));
        d.setSt_restaurant_name(cursor.getString(12));
        d.setSt_street_address(cursor.getString(13));
        d.setSt_suburb(cursor.getString(14));
        d.setSt_postcode(cursor.getString(15));
        d.setSt_rest_charge_amount(cursor.getString(16));
        d.setSt_rest_charge_desc(cursor.getString(17));
        cd.add(d);
      }
      db.close();
    } catch (JsonSyntaxException e) {
      e.printStackTrace();
    }
    return cd;
  }

  public JSONArray getOrderDetails(String rest_id) {
    ArrayList<Payment_OrderDetail> cd = new ArrayList<>();
    SQLiteDatabase db = this.getReadableDatabase();

    String query = "select * from " + Table_Dish_Info + " WHERE " + restaurant_id + " = " + rest_id;
    Cursor cursor = db.rawQuery(query, null);
    while (cursor.moveToNext()) {
      double totprice = 0;
      Payment_OrderDetail d = new Payment_OrderDetail();
      String extraitem = "";
      String extraprice = "";
      String type = "";
      double qty = 0;
      ArrayList<ArrayList<Preferencemodel>> prefmodellist = new ArrayList<>();
      d.setId(cursor.getString(2));
      d.setName(cursor.getString(3));
      d.setActual_Price(cursor.getString(5));
      d.setQty(cursor.getString(9));
      qty = cursor.getDouble(9);
      String preflist = cursor.getString(10);
      totprice = totprice + cursor.getDouble(5);

      if (preflist.length() > 0) {
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonarray = (JsonArray) jsonParser.parse(preflist);
        for (int i = 1; i < jsonarray.size(); i++) {

          JsonArray newarray = (JsonArray) jsonarray.get(i);
          String str = String.valueOf(newarray);
          JsonArray myarray = (JsonArray) jsonParser.parse(str);
          ArrayList<Preferencemodel> newChildArray = new ArrayList<>();

          for (int j = 0; j < myarray.size(); j++) {
            Preferencemodel preferencemodel = new Preferencemodel();

            String newstr = String.valueOf(myarray.get(j));
            JsonElement jsonElement = new JsonParser().parse(newstr);
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            //     String json=new Gson().toJson(jsonObject);
            Map<String, String> map = new HashMap<String, String>();

            map = (Map<String, String>) new Gson().fromJson(jsonObject, map.getClass());

            String id = map.get("id");
            extraitem = extraitem + map.get("priceitem") + "@";
            extraprice = extraprice + map.get("menuprice") + "@";
            type = type + map.get("sectionname") + "@";
            if (!map.get("menuprice").equals("")) {
              try {
                totprice = totprice + Double.parseDouble(map.get("menuprice"));
              } catch (Exception e) {
                e.printStackTrace();
              }
            }
          }
        }
        if (extraitem.length() > 0) {
          extraitem = extraitem.substring(0, extraitem.length() - 1);
          d.setExtra_Item(extraitem);
        }
        if (extraprice.length() > 0) {
          extraprice = extraprice.substring(0, extraprice.length() - 1);
          d.setExtra_Price(extraprice);
        }
        if (type.length() > 0) {
          type = type.substring(0, type.length() - 1);
          d.setType(type);
        }
        d.setPrice(String.format("%.2f", totprice));
        double subtotalnew = totprice * qty;
        d.setSubtotal(String.format("%.2f", subtotalnew));
      } else {
        d.setPrice(String.format("%.2f", totprice));
        double subtotalnew = totprice * qty;
        d.setSubtotal(String.format("%.2f", subtotalnew));
      }
      cd.add(d);
    }
    db.close();

    JSONArray jarray = new JSONArray();
    for (int k = 0; k < cd.size(); k++) {
      JSONObject jobj = new JSONObject();
      Map<String, String> map = new HashMap<String, String>();
      map.put("Extra_Item", cd.get(k).getExtra_Item());
      map.put("name", cd.get(k).getName());
      map.put("id", cd.get(k).getId());
      map.put("qty", cd.get(k).getQty());
      map.put("Unit", cd.get(k).getUnit());
      map.put("Actual_Price", cd.get(k).getActual_Price());
      map.put("Extra_Price", cd.get(k).getActual_Price());
      map.put("subtotal", cd.get(k).getSubtotal());
      map.put("Type", cd.get(k).getType());
      map.put("price", cd.get(k).getPrice());

      jobj = new JSONObject(map);
      jarray.put(jobj);
    }
    return jarray;
  }

  public JSONArray getOrderDetails_patanjali() {
    ArrayList<Patanjali_product_details> cd = new ArrayList<>();
    SQLiteDatabase db = this.getReadableDatabase();

    String query = "select * from " + Ptanjali_order_Info;
    Cursor cursor = db.rawQuery(query, null);
    while (cursor.moveToNext()) {
      Patanjali_product_details d = new Patanjali_product_details();
      d.setPro_id(cursor.getString(1));
      d.setQty(cursor.getString(5));
      cd.add(d);
    }
    db.close();

    JSONArray jarray = new JSONArray();
    for (int k = 0; k < cd.size(); k++) {
      JSONObject jobj = new JSONObject();
      Map<String, String> map = new HashMap<String, String>();
      map.put("pro_id", cd.get(k).getPro_id());
      map.put("qty", cd.get(k).getQty());
      jobj = new JSONObject(map);
      jarray.put(jobj);
    }
    return jarray;
  }

  public JSONArray getOrderDetails_CitySpecial() {
    ArrayList<Patanjali_product_details> cd = new ArrayList<>();
    SQLiteDatabase db = this.getReadableDatabase();

    String query = "select * from " + CitySpecial_order_Info;
    Cursor cursor = db.rawQuery(query, null);
    while (cursor.moveToNext()) {
      Patanjali_product_details d = new Patanjali_product_details();
      d.setPro_id(cursor.getString(1));
      d.setQty(cursor.getString(5));
      cd.add(d);
    }
    db.close();

    JSONArray jarray = new JSONArray();
    for (int k = 0; k < cd.size(); k++) {
      JSONObject jobj = new JSONObject();
      Map<String, String> map = new HashMap<String, String>();
      map.put("pro_id", cd.get(k).getPro_id());
      map.put("qty", cd.get(k).getQty());
      jobj = new JSONObject(map);
      jarray.put(jobj);
    }
    return jarray;
  }

  public JSONArray getOrderDetails_Water() {
    ArrayList<Patanjali_product_details> cd = new ArrayList<>();
    SQLiteDatabase db = this.getReadableDatabase();

    String query = "select * from " + Water_order_Info;
    Cursor cursor = db.rawQuery(query, null);
    while (cursor.moveToNext()) {
      Patanjali_product_details d = new Patanjali_product_details();
      d.setPro_id(cursor.getString(1));
      d.setQty(cursor.getString(5));
      cd.add(d);
    }
    db.close();

    JSONArray jarray = new JSONArray();
    for (int k = 0; k < cd.size(); k++) {
      JSONObject jobj = new JSONObject();
      Map<String, String> map = new HashMap<String, String>();
      map.put("pro_id", cd.get(k).getPro_id());
      map.put("qty", cd.get(k).getQty());
      jobj = new JSONObject(map);
      jarray.put(jobj);
    }
    return jarray;
  }

  public void updateCart(String qty, String new_dish_id) {
    SQLiteDatabase db = this.getWritableDatabase();
    String query = "update "
        + Table_Dish_Info
        + " set "
        + quantity
        + " = "
        + qty
        + " where "
        + dish_id
        + "='"
        + new_dish_id
        + "'";
    db.execSQL(query);
    Cursor cursor = db.rawQuery(query, null);
    db.close();
  }

  public void deleteCart() {
    SQLiteDatabase db = this.getWritableDatabase();
    String query = "delete from " + Table_Dish_Info;
    db.execSQL(query);
    Cursor cursor = db.rawQuery(query, null);
    db.close();
  }

  public void deleteCart_patanjali() {
    SQLiteDatabase db = this.getWritableDatabase();
    String query = "delete from " + Ptanjali_order_Info;
    db.execSQL(query);
    Cursor cursor = db.rawQuery(query, null);
    db.close();
  }

  public void deleteCart_CitySpecial() {
    SQLiteDatabase db = this.getWritableDatabase();
    String query = "delete from " + CitySpecial_order_Info;
    db.execSQL(query);
    Cursor cursor = db.rawQuery(query, null);
    db.close();
  }

  public void deleteCart_Water() {
    SQLiteDatabase db = this.getWritableDatabase();
    String query = "delete from " + Water_order_Info;
    db.execSQL(query);
    Cursor cursor = db.rawQuery(query, null);
    db.close();
  }

  public void deletecart(String new_dish_id) {
    SQLiteDatabase db = this.getWritableDatabase();
    db.execSQL("DELETE FROM " + Table_Dish_Info + " WHERE " + dish_id + "='" + new_dish_id + "'");
    db.close();
  }

  public void deleteFruitCart() {
    SQLiteDatabase db = this.getWritableDatabase();
    String query = "delete from " + Fruit_Info;
    db.execSQL(query);
    Cursor cursor = db.rawQuery(query, null);
    db.close();
  }

  public void deleteFruitcart(String new_dish_id) {
    SQLiteDatabase db = this.getWritableDatabase();
    db.execSQL("DELETE FROM " + Fruit_Info + " WHERE " + dish_id + "='" + new_dish_id + "'");
    db.close();
  }

  public void updateFruitCart(String qty, String new_dish_id) {
    SQLiteDatabase db = this.getWritableDatabase();
    String query = "update "
        + Fruit_Info
        + " set "
        + quantity
        + " = "
        + qty
        + " where "
        + dish_id
        + "='"
        + new_dish_id
        + "'";
    db.execSQL(query);
    Cursor cursor = db.rawQuery(query, null);
    db.close();
  }

  public JSONArray getFruitsOrderDetails(String rest_id) {
    ArrayList<Payment_OrderDetail> cd = new ArrayList<>();
    SQLiteDatabase db = this.getReadableDatabase();

    String query = "select * from " + Fruit_Info + " WHERE " + restaurant_id + " = " + rest_id;
    Cursor cursor = db.rawQuery(query, null);
    while (cursor.moveToNext()) {
      double totprice = 0;
      Payment_OrderDetail d = new Payment_OrderDetail();
      String extraitem = "";
      String extraprice = "";
      String type = "";
      double qty = 0;
      ArrayList<ArrayList<Preferencemodel>> prefmodellist = new ArrayList<>();
      d.setId(cursor.getString(2));
      d.setName(cursor.getString(3));
      d.setActual_Price(cursor.getString(5));
      d.setQty(cursor.getString(9));
      qty = cursor.getDouble(9);
      String preflist = cursor.getString(10);
      totprice = totprice + cursor.getDouble(5);

      if (preflist.length() > 0) {
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonarray = (JsonArray) jsonParser.parse(preflist);
        for (int i = 1; i < jsonarray.size(); i++) {

          JsonArray newarray = (JsonArray) jsonarray.get(i);
          String str = String.valueOf(newarray);
          JsonArray myarray = (JsonArray) jsonParser.parse(str);
          ArrayList<Preferencemodel> newChildArray = new ArrayList<>();

          for (int j = 0; j < myarray.size(); j++) {
            Preferencemodel preferencemodel = new Preferencemodel();

            String newstr = String.valueOf(myarray.get(j));
            JsonElement jsonElement = new JsonParser().parse(newstr);
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            //     String json=new Gson().toJson(jsonObject);
            Map<String, String> map = new HashMap<String, String>();

            map = (Map<String, String>) new Gson().fromJson(jsonObject, map.getClass());

            String id = map.get("id");
            extraitem = extraitem + map.get("priceitem") + "@";
            extraprice = extraprice + map.get("menuprice") + "@";
            type = type + map.get("sectionname") + "@";
            if (!map.get("menuprice").equals("")) {
              try {
                totprice = totprice + Double.parseDouble(map.get("menuprice"));
              } catch (Exception e) {
                e.printStackTrace();
              }
            }
          }
        }
        if (extraitem.length() > 0) {
          extraitem = extraitem.substring(0, extraitem.length() - 1);
          d.setExtra_Item(extraitem);
        }
        if (extraprice.length() > 0) {
          extraprice = extraprice.substring(0, extraprice.length() - 1);
          d.setExtra_Price(extraprice);
        }
        if (type.length() > 0) {
          type = type.substring(0, type.length() - 1);
          d.setType(type);
        }
        d.setPrice(String.format("%.2f", totprice));
        double subtotalnew = totprice * qty;
        d.setSubtotal(String.format("%.2f", subtotalnew));
      } else {
        d.setPrice(String.format("%.2f", totprice));
        double subtotalnew = totprice * qty;
        d.setSubtotal(String.format("%.2f", subtotalnew));
      }
      cd.add(d);
    }
    db.close();

    JSONArray jarray = new JSONArray();
    for (int k = 0; k < cd.size(); k++) {
      JSONObject jobj = new JSONObject();
      Map<String, String> map = new HashMap<String, String>();
      map.put("Extra_Item", cd.get(k).getExtra_Item());
      map.put("name", cd.get(k).getName());
      map.put("id", cd.get(k).getId());
      map.put("qty", cd.get(k).getQty());
      map.put("Unit", cd.get(k).getUnit());
      map.put("Actual_Price", cd.get(k).getActual_Price());
      map.put("Extra_Price", cd.get(k).getActual_Price());
      map.put("subtotal", cd.get(k).getSubtotal());
      map.put("Type", cd.get(k).getType());
      map.put("price", cd.get(k).getPrice());

      jobj = new JSONObject(map);
      jarray.put(jobj);
    }
    return jarray;
  }

  public ArrayList<Cart_Model.Cart_Details> getFruitCart_Details() {
    ArrayList<Cart_Model.Cart_Details> cd = null;
    try {
      cd = new ArrayList<>();
      SQLiteDatabase db = this.getReadableDatabase();
      String query = "select * from " + Fruit_Info;
      Cursor cursor = db.rawQuery(query, null);
      while (cursor.moveToNext()) {
        Cart_Model.Cart_Details d = new Cart_Model().new Cart_Details();
        ArrayList<ArrayList<Preferencemodel>> prefmodellist = new ArrayList<>();
        d.setId(cursor.getString(0));
        d.setIn_restaurant_id(cursor.getString(1));
        d.setIn_dish_id(cursor.getString(2));
        d.setSt_dish_name(cursor.getString(3));
        d.setPrice_item(cursor.getString(4));
        d.setMenu_price(cursor.getString(5));
        d.setDelivery_type(cursor.getString(6));
        d.setNon_veg_status(cursor.getString(7));
        d.setCake_flg(cursor.getString(8));
        d.setQuantity(cursor.getString(9));
        d.setMax_qty_peruser(cursor.getString(16));
        d.setSt_restaurant_image(cursor.getString(17));
        String preflist = cursor.getString(10);

        if (preflist.length() > 0) {
          JsonParser jsonParser = new JsonParser();
          JsonArray jsonarray = (JsonArray) jsonParser.parse(preflist);
          for (int i = 0; i < jsonarray.size(); i++) {
            JsonArray newarray = (JsonArray) jsonarray.get(i);
            String str = String.valueOf(newarray);
            JsonArray myarray = (JsonArray) jsonParser.parse(str);
            ArrayList<Preferencemodel> newChildArray = new ArrayList<>();

            for (int j = 0; j < myarray.size(); j++) {
              Preferencemodel preferencemodel = new Preferencemodel();

              String newstr = String.valueOf(myarray.get(j));
              JsonElement jsonElement = new JsonParser().parse(newstr);
              JsonObject jsonObject = jsonElement.getAsJsonObject();

              Type type = new TypeToken<Map<String, String>>() {
              }.getType();
              Map<String, String> map = new Gson().fromJson(jsonObject, type);

              String id = map.get("id");
              String priceitem = map.get("priceitem");
              String menuprice = map.get("menuprice");
              String sectionname = map.get("sectionname");

              preferencemodel.setPriceitem(priceitem);
              preferencemodel.setMenuprice(menuprice);
              preferencemodel.setSectionname(sectionname);
              preferencemodel.setId(id);

              newChildArray.add(preferencemodel);
            }
            prefmodellist.add(newChildArray);
          }
        }
        d.setPreferencelfromdb(prefmodellist);
        d.setTotal_price(cursor.getString(11));
        d.setSt_restaurant_name(cursor.getString(12));
        d.setSt_street_address(cursor.getString(13));
        d.setSt_suburb(cursor.getString(14));
        d.setSt_postcode(cursor.getString(15));
        cd.add(d);
      }
      db.close();
    } catch (JsonSyntaxException e) {
      e.printStackTrace();
    }
    return cd;
  }

  public void insertTable_Fruit_info(String rest_id, String dishid, String dishname, String dvn,
      float dvp, String del_type, String non_veg_flag, String cake_flag, int qty,int max_qty,
      String preferenceinsert, float tp, String res_name, String res_address, String suburb,
      String postcode,String res_image) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues cv = new ContentValues();
    cv.put(restaurant_id, rest_id);
    cv.put(dish_id, dishid);
    cv.put(dish_name, dishname);
    cv.put(dish_variety_name, dvn);
    cv.put(dish_variety_price, dvp);
    cv.put(delivery_type, del_type);
    cv.put(non_veg_status, non_veg_flag);
    cv.put(cake_flg, cake_flag);
    cv.put(quantity, qty);
    cv.put(max_qty_peruser, max_qty);
    cv.put(prefdetails, preferenceinsert);
    cv.put(totalprice, tp);
    cv.put(restaurant_name, res_name);
    cv.put(restaurant_address, res_address);
    cv.put(restaurant_suburb, suburb);
    cv.put(restaurant_postcode, postcode);
    cv.put(restaurant_image, res_image);
    Long k = db.insert(Fruit_Info, null, cv);
    Log.d("insert:", "" + k);
    db.close();
  }

  public boolean checkIfCartContainsSomeOtherFruitData(String rest_id) {
    boolean status = false;
    SQLiteDatabase db = this.getReadableDatabase();
    String query =
        "select * from " + Fruit_Info + " WHERE " + restaurant_id + " != " + rest_id;
    Cursor cursor = db.rawQuery(query, null);
    int cnt = cursor.getCount();
    if (cnt > 0) {
      status = true;
    }
    cursor.close();
    db.close();
    return status;
  }

  public int getFruitQuantity(String rest_id) {
    int status = 0;
    SQLiteDatabase db = this.getReadableDatabase();
      /*String query = "select count(1) from "
        + Fruit_Info
        + " WHERE "
        + restaurant_id
        + "='"
        + rest_id
        + "'";*/
    String query = "select count(1) from " + Fruit_Info;
    Cursor cursor = db.rawQuery(query, null);
    int cnt = cursor.getCount();
    while (cursor.moveToNext()) {
      status = cursor.getInt(0);
    }
    cursor.close();
    db.close();
    return status;
  }
}


