package com.food.order.speedzy.screen.patanjali;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.food.order.speedzy.R;
import com.food.order.speedzy.api.response.grocery.SubCategoriesItem;

import java.util.ArrayList;
import java.util.List;

public class GrocerySubCategoriesAdapter extends RecyclerView.Adapter<GrocerySubCategoriesVH> {
  private final Callback mCallback;
  private List<GrocerySubcategoryList> mData;
  String category_id = "";

  public GrocerySubCategoriesAdapter(
      Callback callback) {
    this.mData = new ArrayList<>();
    mCallback = callback;
  }

  @NonNull @Override
  public GrocerySubCategoriesVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View view =
        LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.viewhokder_two, viewGroup, false);
    return new GrocerySubCategoriesVH(view, new GrocerySubCategoriesVH.Callback() {
      @Override public void onClickItem(int position) {
        mCallback.onClickItem(mData.get(position), category_id);
      }
    });
  }

  @Override
  public void onBindViewHolder(@NonNull GrocerySubCategoriesVH grocerySubCategoriesVH, int i) {
    grocerySubCategoriesVH.onBindView(mData.get(i));
  }

  @Override public int getItemCount() {
    return mData.size();
  }

  public void setData(
      List<GrocerySubcategoryList> data) {
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public void refreshData(
          List<GrocerySubcategoryList> data) {
    mData.clear();
    setData(data);
  }

  public void clearAdapter() {
    mData.clear();
  }

  public String getCategory_id() {
    return category_id;
  }

  public void setCategory_id(String category_id) {
    this.category_id = category_id;
  }

  public interface Callback {
    void onClickItem(GrocerySubcategoryList data, String categoryId);
  }
}
