package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Combo_Details_Response_Model implements Serializable {
    @SerializedName("status")
    @Expose
    public String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<RestaurantDetail> getRestaurantDetails() {
        return restaurantDetails;
    }

    public void setRestaurantDetails(List<RestaurantDetail> restaurantDetails) {
        this.restaurantDetails = restaurantDetails;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public RestaurantTimings getRestaurantTimings() {
        return restaurantTimings;
    }

    public void setRestaurantTimings(RestaurantTimings restaurantTimings) {
        this.restaurantTimings = restaurantTimings;
    }

    @SerializedName("restaurant_details")
    @Expose
    public List<RestaurantDetail> restaurantDetails = null;
    @SerializedName("delivery_charge")
    @Expose
    public String deliveryCharge;
    @SerializedName("Restaurant_timings")
    @Expose
    public RestaurantTimings restaurantTimings;

    public Combo_Details_Response_Model.open_close_status getOpen_close_status() {
        return open_close_status;
    }

    public void setOpen_close_status(Combo_Details_Response_Model.open_close_status open_close_status) {
        this.open_close_status = open_close_status;
    }

    @SerializedName("open_close_status")
    @Expose
    public open_close_status open_close_status;

    public class RestaurantDetail implements Serializable{

        public String getRestaurantLogoImage() {
            return restaurantLogoImage;
        }

        public void setRestaurantLogoImage(String restaurantLogoImage) {
            this.restaurantLogoImage = restaurantLogoImage;
        }

        public String getRestaurantCoverImage() {
            return restaurantCoverImage;
        }

        public void setRestaurantCoverImage(String restaurantCoverImage) {
            this.restaurantCoverImage = restaurantCoverImage;
        }

        public String getRestaurantMainImage() {
            return restaurantMainImage;
        }

        public void setRestaurantMainImage(String restaurantMainImage) {
            this.restaurantMainImage = restaurantMainImage;
        }

        public String getInRestaurantId() {
            return inRestaurantId;
        }

        public void setInRestaurantId(String inRestaurantId) {
            this.inRestaurantId = inRestaurantId;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getStRestaurantName() {
            return stRestaurantName;
        }

        public void setStRestaurantName(String stRestaurantName) {
            this.stRestaurantName = stRestaurantName;
        }

        public String getStSuburb() {
            return stSuburb;
        }

        public void setStSuburb(String stSuburb) {
            this.stSuburb = stSuburb;
        }

        public String getStStreetAddress() {
            return stStreetAddress;
        }

        public void setStStreetAddress(String stStreetAddress) {
            this.stStreetAddress = stStreetAddress;
        }

        public String getStMinOrder() {
            return stMinOrder;
        }

        public void setStMinOrder(String stMinOrder) {
            this.stMinOrder = stMinOrder;
        }

        public Object getStMinFreeOrder() {
            return stMinFreeOrder;
        }

        public void setStMinFreeOrder(Object stMinFreeOrder) {
            this.stMinFreeOrder = stMinFreeOrder;
        }

        public String getAvgRating() {
            return avgRating;
        }

        public void setAvgRating(String avgRating) {
            this.avgRating = avgRating;
        }

        public String getCountRating() {
            return countRating;
        }

        public void setCountRating(String countRating) {
            this.countRating = countRating;
        }

        public String getStreetAddress() {
            return streetAddress;
        }

        public void setStreetAddress(String streetAddress) {
            this.streetAddress = streetAddress;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getStGstCommission() {
            return stGstCommission;
        }

        public void setStGstCommission(String stGstCommission) {
            this.stGstCommission = stGstCommission;
        }

        @SerializedName("restaurant_logo_image")
        @Expose
        public String restaurantLogoImage;
        @SerializedName("restaurant_cover_image")
        @Expose
        public String restaurantCoverImage;
        @SerializedName("restaurant_main_image")
        @Expose
        public String restaurantMainImage;
        @SerializedName("in_restaurant_id")
        @Expose
        public String inRestaurantId;
        @SerializedName("distance")
        @Expose
        public String distance;
        @SerializedName("st_restaurant_name")
        @Expose
        public String stRestaurantName;
        @SerializedName("st_suburb")
        @Expose
        public String stSuburb;
        @SerializedName("st_street_address")
        @Expose
        public String stStreetAddress;
        @SerializedName("st_min_order")
        @Expose
        public String stMinOrder;
        @SerializedName("st_min_free_order")
        @Expose
        public Object stMinFreeOrder;
        @SerializedName("avg_rating")
        @Expose
        public String avgRating;
        @SerializedName("count_rating")
        @Expose
        public String countRating;
        @SerializedName("street_address")
        @Expose
        public String streetAddress;
        @SerializedName("flag")
        @Expose
        public String flag;
        @SerializedName("st_gst_commission")
        @Expose
        public String stGstCommission;

        public String getSt_latitude() {
            return st_latitude;
        }

        public void setSt_latitude(String st_latitude) {
            this.st_latitude = st_latitude;
        }

        public String getSt_longitude() {
            return st_longitude;
        }

        public void setSt_longitude(String st_longitude) {
            this.st_longitude = st_longitude;
        }

        @SerializedName("st_latitude")
        @Expose
        public String st_latitude;
        @SerializedName("st_longitude")
        @Expose
        public String st_longitude;

        public RestaurantDetail.restcharge getRestcharge() {
            return restcharge;
        }

        public void setRestcharge(RestaurantDetail.restcharge restcharge) {
            this.restcharge = restcharge;
        }

        @SerializedName("restcharge")
        @Expose
        public restcharge restcharge;
        public class restcharge implements Serializable{
            @SerializedName("amount")
            @Expose
            public String amount;

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            @SerializedName("desc")
            @Expose
            public String desc;
        }
    }
    public class RestaurantTimings {

        public String getLunchStart() {
            return lunchStart;
        }

        public void setLunchStart(String lunchStart) {
            this.lunchStart = lunchStart;
        }

        public String getLunchEnd() {
            return lunchEnd;
        }

        public void setLunchEnd(String lunchEnd) {
            this.lunchEnd = lunchEnd;
        }

        public String getDinnerStart() {
            return dinnerStart;
        }

        public void setDinnerStart(String dinnerStart) {
            this.dinnerStart = dinnerStart;
        }

        public String getDinnerEnd() {
            return dinnerEnd;
        }

        public void setDinnerEnd(String dinnerEnd) {
            this.dinnerEnd = dinnerEnd;
        }

        public String getBreakfastStart() {
            return breakfastStart;
        }

        public void setBreakfastStart(String breakfastStart) {
            this.breakfastStart = breakfastStart;
        }

        public String getBreakfastEnd() {
            return breakfastEnd;
        }

        public void setBreakfastEnd(String breakfastEnd) {
            this.breakfastEnd = breakfastEnd;
        }

        public String getTiffinStart() {
            return tiffinStart;
        }

        public void setTiffinStart(String tiffinStart) {
            this.tiffinStart = tiffinStart;
        }

        public String getTiffinEnd() {
            return tiffinEnd;
        }

        public void setTiffinEnd(String tiffinEnd) {
            this.tiffinEnd = tiffinEnd;
        }

        public String getFastfoodStart() {
            return fastfoodStart;
        }

        public void setFastfoodStart(String fastfoodStart) {
            this.fastfoodStart = fastfoodStart;
        }

        public String getFastfoodEnd() {
            return fastfoodEnd;
        }

        public void setFastfoodEnd(String fastfoodEnd) {
            this.fastfoodEnd = fastfoodEnd;
        }

        public String getSweetsStart() {
            return sweetsStart;
        }

        public void setSweetsStart(String sweetsStart) {
            this.sweetsStart = sweetsStart;
        }

        public String getSweetsEnd() {
            return sweetsEnd;
        }

        public void setSweetsEnd(String sweetsEnd) {
            this.sweetsEnd = sweetsEnd;
        }

        public String getFruitsStart() {
            return fruitsStart;
        }

        public void setFruitsStart(String fruitsStart) {
            this.fruitsStart = fruitsStart;
        }

        public String getFruitsEnd() {
            return fruitsEnd;
        }

        public void setFruitsEnd(String fruitsEnd) {
            this.fruitsEnd = fruitsEnd;
        }

        @SerializedName("lunch_start")
        @Expose
        public String lunchStart;
        @SerializedName("lunch_end")
        @Expose
        public String lunchEnd;
        @SerializedName("dinner_start")
        @Expose
        public String dinnerStart;
        @SerializedName("dinner_end")
        @Expose
        public String dinnerEnd;
        @SerializedName("breakfast_start")
        @Expose
        public String breakfastStart;
        @SerializedName("breakfast_end")
        @Expose
        public String breakfastEnd;
        @SerializedName("tiffin_start")
        @Expose
        public String tiffinStart;
        @SerializedName("tiffin_end")
        @Expose
        public String tiffinEnd;
        @SerializedName("fastfood_start")
        @Expose
        public String fastfoodStart;
        @SerializedName("fastfood_end")
        @Expose
        public String fastfoodEnd;
        @SerializedName("sweets_start")
        @Expose
        public String sweetsStart;
        @SerializedName("sweets_end")
        @Expose
        public String sweetsEnd;
        @SerializedName("fruits_start")
        @Expose
        public String fruitsStart;
        @SerializedName("fruits_end")
        @Expose
        public String fruitsEnd;

    }
    public class open_close_status implements Serializable{
        @SerializedName("message")
        @Expose
        public String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getOpenstatus() {
            return openstatus;
        }

        public void setOpenstatus(String openstatus) {
            this.openstatus = openstatus;
        }

        @SerializedName("openstatus")
        @Expose
        public String openstatus;
    }
}
