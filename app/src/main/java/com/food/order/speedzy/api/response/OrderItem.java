package com.food.order.speedzy.api.response;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class OrderItem implements Serializable {

  @SerializedName("order_no")
  private String orderNo;

  @SerializedName("restaurant_id")
  private String restaurantId;

  @SerializedName("delivery_schedule")
  private String deliverySchedule;

  @SerializedName("delivery_time")
  private Object deliveryTime;

  @SerializedName("order_detail")
  private List<OrderDetailItem> orderDetail;

  @SerializedName("suburb_id")
  private String suburbId;

  @SerializedName("avg_order_value")
  private String avgOrderValue;

  @SerializedName("lname")
  private String lname;

  @SerializedName("order_amount")
  private String orderAmount;

  @SerializedName("method_of_ordering")
  private Object methodOfOrdering;

  @SerializedName("st_gst_commission")
  private String stGstCommission;

  @SerializedName("email")
  private String email;

  @SerializedName("payment_method")
  private String paymentMethod;

  @SerializedName("transaction_id")
  private Object transactionId;

  @SerializedName("order_runningstatus")
  private OrderRunningstatus orderRunningstatus;

  @SerializedName("image")
  private String image;

  @SerializedName("fname")
  private String fname;

  @SerializedName("st_voucher_amount")
  private String stVoucherAmount;

  @SerializedName("res_address")
  private String resAddress;

  @SerializedName("address")
  private String address;

  @SerializedName("wallet")
  private String wallet;

  @SerializedName("orderid")
  private String orderid;
  @SerializedName("res_latitude")
  private String res_latitude;
  @SerializedName("res_longitude")
  private String res_longitude;
  @SerializedName("user_latitude")
  private String user_latitude;

  public String getRes_latitude() {
    return res_latitude;
  }

  public void setRes_latitude(String res_latitude) {
    this.res_latitude = res_latitude;
  }

  public String getRes_longitude() {
    return res_longitude;
  }

  public void setRes_longitude(String res_longitude) {
    this.res_longitude = res_longitude;
  }

  public String getUser_latitude() {
    return user_latitude;
  }

  public void setUser_latitude(String user_latitude) {
    this.user_latitude = user_latitude;
  }

  public String getUser_longitude() {
    return user_longitude;
  }

  public void setUser_longitude(String user_longitude) {
    this.user_longitude = user_longitude;
  }

  @SerializedName("user_longitude")
  private String user_longitude;

  @SerializedName("Close_open_st")
  private String closeOpenSt;

  @SerializedName("mobile")
  private String mobile;

  @SerializedName("streetnumber")
  private String streetnumber;

  @SerializedName("pickup_schedule")
  private String pickupSchedule;

  @SerializedName("restaurant_name")
  private String restaurantName;

  @SerializedName("special_offers")
  private List<Object> specialOffers;

  @SerializedName("delivery_charge")
  private String deliveryCharge;

  @SerializedName("tom_pickup_schedule")
  private String tomPickupSchedule;

  @SerializedName("res_mobile")
  private String resMobile;

  @SerializedName("total_amount")
  private String totalAmount;

  @SerializedName("companyname")
  private String companyname;

  @SerializedName("nearest_cross_city")
  private String nearestCrossCity;

  @SerializedName("voucher_id")
  private String voucherId;

  @SerializedName("flg_order_type")
  private String flgOrderType;

  @SerializedName("tom_delivery_schedule")
  private String tomDeliverySchedule;

  @SerializedName("flg_order_status")
  private String flgOrderStatus;

  @SerializedName("pre_order_date")
  private String preOrderDate;

  @SerializedName("payment_status")
  private String paymentStatus;

  public String getDicount_val() {
    return dicount_val;
  }

  public void setDicount_val(String dicount_val) {
    this.dicount_val = dicount_val;
  }

  @SerializedName("dicount_val")
  private String dicount_val;

  public String getRestaurant_charge() {
    return restaurant_charge;
  }

  public void setRestaurant_charge(String restaurant_charge) {
    this.restaurant_charge = restaurant_charge;
  }

  @SerializedName("restaurant_charge")
  private String restaurant_charge;

  public String getPaymentStatus() {
    return paymentStatus;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setRestaurantId(String restaurantId) {
    this.restaurantId = restaurantId;
  }

  public String getRestaurantId() {
    return restaurantId;
  }

  public void setDeliverySchedule(String deliverySchedule) {
    this.deliverySchedule = deliverySchedule;
  }

  public String getDeliverySchedule() {
    return deliverySchedule;
  }

  public void setDeliveryTime(Object deliveryTime) {
    this.deliveryTime = deliveryTime;
  }

  public Object getDeliveryTime() {
    return deliveryTime;
  }

  public void setOrderDetail(List<OrderDetailItem> orderDetail) {
    this.orderDetail = orderDetail;
  }

  public List<OrderDetailItem> getOrderDetail() {
    return orderDetail;
  }

  public void setSuburbId(String suburbId) {
    this.suburbId = suburbId;
  }

  public String getSuburbId() {
    return suburbId;
  }

  public void setAvgOrderValue(String avgOrderValue) {
    this.avgOrderValue = avgOrderValue;
  }

  public String getAvgOrderValue() {
    return avgOrderValue;
  }

  public void setLname(String lname) {
    this.lname = lname;
  }

  public String getLname() {
    return lname;
  }

  public void setOrderAmount(String orderAmount) {
    this.orderAmount = orderAmount;
  }

  public String getOrderAmount() {
    return orderAmount;
  }

  public void setMethodOfOrdering(Object methodOfOrdering) {
    this.methodOfOrdering = methodOfOrdering;
  }

  public Object getMethodOfOrdering() {
    return methodOfOrdering;
  }

  public void setStGstCommission(String stGstCommission) {
    this.stGstCommission = stGstCommission;
  }

  public String getStGstCommission() {
    return stGstCommission;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public void setPaymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public String getPaymentMethod() {
    return paymentMethod;
  }

  public void setTransactionId(Object transactionId) {
    this.transactionId = transactionId;
  }

  public Object getTransactionId() {
    return transactionId;
  }

  public void setOrderRunningstatus(OrderRunningstatus orderRunningstatus) {
    this.orderRunningstatus = orderRunningstatus;
  }

  public OrderRunningstatus getOrderRunningstatus() {
    return orderRunningstatus;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getImage() {
    return image;
  }

  public void setFname(String fname) {
    this.fname = fname;
  }

  public String getFname() {
    return fname;
  }

  public void setStVoucherAmount(String stVoucherAmount) {
    this.stVoucherAmount = stVoucherAmount;
  }

  public String getStVoucherAmount() {
    return stVoucherAmount;
  }

  public void setResAddress(String resAddress) {
    this.resAddress = resAddress;
  }

  public String getResAddress() {
    return resAddress;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getAddress() {
    return address;
  }

  public void setWallet(String wallet) {
    this.wallet = wallet;
  }

  public String getWallet() {
    return wallet;
  }

  public void setOrderid(String orderid) {
    this.orderid = orderid;
  }

  public String getOrderid() {
    return orderid;
  }

  public void setCloseOpenSt(String closeOpenSt) {
    this.closeOpenSt = closeOpenSt;
  }

  public String getCloseOpenSt() {
    return closeOpenSt;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getMobile() {
    return mobile;
  }

  public void setStreetnumber(String streetnumber) {
    this.streetnumber = streetnumber;
  }

  public String getStreetnumber() {
    return streetnumber;
  }

  public void setPickupSchedule(String pickupSchedule) {
    this.pickupSchedule = pickupSchedule;
  }

  public String getPickupSchedule() {
    return pickupSchedule;
  }

  public void setRestaurantName(String restaurantName) {
    this.restaurantName = restaurantName;
  }

  public String getRestaurantName() {
    return restaurantName;
  }

  public void setSpecialOffers(List<Object> specialOffers) {
    this.specialOffers = specialOffers;
  }

  public List<Object> getSpecialOffers() {
    return specialOffers;
  }

  public void setDeliveryCharge(String deliveryCharge) {
    this.deliveryCharge = deliveryCharge;
  }

  public String getDeliveryCharge() {
    return deliveryCharge;
  }

  public void setTomPickupSchedule(String tomPickupSchedule) {
    this.tomPickupSchedule = tomPickupSchedule;
  }

  public String getTomPickupSchedule() {
    return tomPickupSchedule;
  }

  public void setResMobile(String resMobile) {
    this.resMobile = resMobile;
  }

  public String getResMobile() {
    return resMobile;
  }

  public void setTotalAmount(String totalAmount) {
    this.totalAmount = totalAmount;
  }

  public String getTotalAmount() {
    return totalAmount;
  }

  public void setCompanyname(String companyname) {
    this.companyname = companyname;
  }

  public String getCompanyname() {
    return companyname;
  }

  public void setNearestCrossCity(String nearestCrossCity) {
    this.nearestCrossCity = nearestCrossCity;
  }

  public String getNearestCrossCity() {
    return nearestCrossCity;
  }

  public void setVoucherId(String voucherId) {
    this.voucherId = voucherId;
  }

  public String getVoucherId() {
    return voucherId;
  }

  public void setFlgOrderType(String flgOrderType) {
    this.flgOrderType = flgOrderType;
  }

  public String getFlgOrderType() {
    return flgOrderType;
  }

  public void setTomDeliverySchedule(String tomDeliverySchedule) {
    this.tomDeliverySchedule = tomDeliverySchedule;
  }

  public String getTomDeliverySchedule() {
    return tomDeliverySchedule;
  }

  public void setFlgOrderStatus(String flgOrderStatus) {
    this.flgOrderStatus = flgOrderStatus;
  }

  public String getFlgOrderStatus() {
    return flgOrderStatus;
  }

  public void setPreOrderDate(String preOrderDate) {
    this.preOrderDate = preOrderDate;
  }

  public String getPreOrderDate() {
    return preOrderDate;
  }

  @Override
  public String toString() {
    return
        "OrderItem{" +
            "order_no = '" + orderNo + '\'' +
            ",restaurant_id = '" + restaurantId + '\'' +
            ",delivery_schedule = '" + deliverySchedule + '\'' +
            ",delivery_time = '" + deliveryTime + '\'' +
            ",order_detail = '" + orderDetail + '\'' +
            ",suburb_id = '" + suburbId + '\'' +
            ",avg_order_value = '" + avgOrderValue + '\'' +
            ",lname = '" + lname + '\'' +
            ",order_amount = '" + orderAmount + '\'' +
            ",method_of_ordering = '" + methodOfOrdering + '\'' +
            ",st_gst_commission = '" + stGstCommission + '\'' +
            ",email = '" + email + '\'' +
            ",payment_method = '" + paymentMethod + '\'' +
            ",transaction_id = '" + transactionId + '\'' +
            ",order_runningstatus = '" + orderRunningstatus + '\'' +
            ",image = '" + image + '\'' +
            ",fname = '" + fname + '\'' +
            ",st_voucher_amount = '" + stVoucherAmount + '\'' +
            ",res_address = '" + resAddress + '\'' +
            ",address = '" + address + '\'' +
            ",wallet = '" + wallet + '\'' +
            ",orderid = '" + orderid + '\'' +
            ",close_open_st = '" + closeOpenSt + '\'' +
            ",mobile = '" + mobile + '\'' +
            ",streetnumber = '" + streetnumber + '\'' +
            ",pickup_schedule = '" + pickupSchedule + '\'' +
            ",restaurant_name = '" + restaurantName + '\'' +
            ",special_offers = '" + specialOffers + '\'' +
            ",delivery_charge = '" + deliveryCharge + '\'' +
            ",tom_pickup_schedule = '" + tomPickupSchedule + '\'' +
            ",res_mobile = '" + resMobile + '\'' +
            ",total_amount = '" + totalAmount + '\'' +
            ",companyname = '" + companyname + '\'' +
            ",nearest_cross_city = '" + nearestCrossCity + '\'' +
            ",voucher_id = '" + voucherId + '\'' +
            ",flg_order_type = '" + flgOrderType + '\'' +
            ",tom_delivery_schedule = '" + tomDeliverySchedule + '\'' +
            ",flg_order_status = '" + flgOrderStatus + '\'' +
            ",pre_order_date = '" + preOrderDate + '\'' +
            "}";
  }
}