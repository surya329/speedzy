package com.food.order.speedzy.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.food.order.speedzy.Model.AddUserAddressModel;
import com.food.order.speedzy.Model.CityListModel;
import com.food.order.speedzy.Model.UserAddressModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.fonts.MySpinnerAdapter;
import com.food.order.speedzy.root.BaseActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddresses_Manual extends BaseActivity
    implements  AdapterView.OnItemSelectedListener {
  String location_str = "";
  EditText location, name, mobile, landmark, pincode, other_name;
  LinearLayout home, work, other;
  TextView home_txt, work_txt, other_txt, add_address, change;
  Spinner city, area;
  MySpinnerAdapter CityAdapter;
  MySpinnerAdapter AreaAdapter;
  ImageView home_img, work_img, other_img;
  String type = "1";
  MySharedPrefrencesData mySharedPrefrencesData;
  LOcaldbNew lOcaldbNew;
  String userid = "", Veg_Flag;
  Toolbar toolbar;
  TextView title;
  String locality, flag;
  List<CityListModel.cityList> cityList = new ArrayList<>();
  ArrayList<String> cityList_name = new ArrayList<>();
  List<CityListModel.cityList.area_list> area_list_specific = new ArrayList<>();
  List<List<CityListModel.cityList.area_list>> areaList = new ArrayList<>();
  ArrayList<String> areaList_name = new ArrayList<>();
  int city_selected_pos = -1;
  String city_selected = "";
  int area_selected_pos = -1;
  String state, district = "", lati, longi;
  LinearLayout other_name_linear;
  private View viewToAttachDisplayerTo;
  boolean network_status = true;
  boolean change_address, edit_address = false;
  Activity context;
 /* ProgressBar progressBar1;
  RecyclerView saved_adds_recyclerview;
  LinearLayoutManager llm, llm1, llm2;
  Saved_Addresses_Adapter savedAddressAdapter;
  ArrayList<UserAddressModel.info> savedAddressList = new ArrayList<>();*/
  UserAddressModel.info address_data;
  TextView text;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_addresses__new);

    viewToAttachDisplayerTo = findViewById(R.id.add_address_activity);

    context = AddAddresses_Manual.this;
    change_address = getIntent().getBooleanExtra("change_address", false);
    edit_address = getIntent().getBooleanExtra("edit_address", false);
    locality = "";
    flag = "0";

    mySharedPrefrencesData = new MySharedPrefrencesData();
    lOcaldbNew = new LOcaldbNew(this);
    userid = mySharedPrefrencesData.getUser_Id(this);
    location_str = mySharedPrefrencesData.getLocation(this);
    Veg_Flag = mySharedPrefrencesData.getVeg_Flag(AddAddresses_Manual.this);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    title = findViewById(R.id.title);
    setSupportActionBar(toolbar);
    getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    if (location_str.equalsIgnoreCase("") || location_str.equalsIgnoreCase(null)) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    } else {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    if (Veg_Flag.equalsIgnoreCase("1")) {
      statusbar_bg(R.color.red);
    } else {
      statusbar_bg(R.color.red);
    }

    change = (TextView) findViewById(R.id.change);
    add_address = (TextView) findViewById(R.id.add_address);
    home_txt = (TextView) findViewById(R.id.home_txt);
    work_txt = (TextView) findViewById(R.id.work_txt);
    other_txt = (TextView) findViewById(R.id.other_txt);
    home_img = (ImageView) findViewById(R.id.home_img);
    work_img = (ImageView) findViewById(R.id.work_img);
    other_img = (ImageView) findViewById(R.id.other_img);
    home = (LinearLayout) findViewById(R.id.home);
    work = (LinearLayout) findViewById(R.id.work);
    other = (LinearLayout) findViewById(R.id.other);
    name = (EditText) findViewById(R.id.name);
    mobile = (EditText) findViewById(R.id.mobile);
    location = (EditText) findViewById(R.id.location);
    landmark = (EditText) findViewById(R.id.landmark);
    pincode = (EditText) findViewById(R.id.pincode);
    other_name = (EditText) findViewById(R.id.other_name);
    other_name_linear = (LinearLayout) findViewById(R.id.other_name_linear);
    city = (Spinner) findViewById(R.id.city);
    area = (Spinner) findViewById(R.id.area);
    /*saved_adds_recyclerview = (RecyclerView) findViewById(R.id.saved_adds_recyclerview);
    progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);*/
    text = findViewById(R.id.text);

    call_api_citylist();

    if (!edit_address) {
     // callapi_get_address();
    } else {
      text.setVisibility(View.GONE);
     /* saved_adds_recyclerview.setVisibility(View.GONE);
      progressBar1.setVisibility(View.GONE);*/
      title.setText("Save Address");
      add_address.setText("Save Address");
      address_data = (UserAddressModel.info) getIntent().getSerializableExtra("address_data");
      data_show();
    }

    home.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        type = "1";
        home_click();
      }
    });
    work.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        type = "2";
        work_click();
      }
    });
    other.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        type = "0";
        other_click();
      }
    });

    add_address.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (!location.getText().toString().equalsIgnoreCase("")) {
          if (!name.getText().toString().equalsIgnoreCase("")) {
            if (!city.getSelectedItem().toString().equalsIgnoreCase("Select City")) {
              if (!area.getSelectedItem().toString().equalsIgnoreCase("Select Area")) {
                if (type.equalsIgnoreCase("0")) {
                  if (!other_name.getText().toString().equalsIgnoreCase("")) {
                    apicall();
                  } else {
                    Toast.makeText(context, "Enter Other name", Toast.LENGTH_SHORT).show();
                  }
                } else {
                  apicall();
                }
              } else {
                Toast.makeText(context, "Select your Area", Toast.LENGTH_SHORT).show();
              }
            } else {
              Toast.makeText(context, "Select your City", Toast.LENGTH_SHORT).show();
            }
          } else {
            Toast.makeText(context, "Enter your Name", Toast.LENGTH_SHORT).show();
          }
        } else {
          Toast.makeText(context, "Enter your Address", Toast.LENGTH_SHORT).show();
        }
      }
    });
  }

  private void data_show() {
    location.setText(address_data.getComplete_address());
    name.setText(address_data.getName());
    mobile.setText(mySharedPrefrencesData.get_Party_mobile(AddAddresses_Manual.this));
    String select_city = address_data.getCityid();
    String select_area = address_data.getArea();
    for (int i = 0; i < cityList.size(); i++) {
      if (select_city.equalsIgnoreCase(cityList.get(i).getCity_id().toString())) {
        city.setSelection(i);
        city_selected_pos = i;
      }
    }
    if (city_selected_pos > 0) {
      for (int i = 0; i < areaList.get(city_selected_pos).size(); i++) {
        areaList_name.add(areaList.get(city_selected_pos).get(i).getSt_suburb());
        AreaAdapter.notifyDataSetChanged();
      }
    }
    for (int i = 0; i < areaList_name.size(); i++) {
      if (select_area.equalsIgnoreCase(areaList_name.get(i).toString())) {
        area.setSelection(i + 1);
      }
    }
    landmark.setText(address_data.getLandmark());
    pincode.setText(address_data.getPincode());
    if (address_data.getAddress_type()
        .equalsIgnoreCase("1")) {
      home_click();
    } else if (address_data.getAddress_type()
        .equalsIgnoreCase("2")) {
      work_click();
    } else if (address_data.getAddress_type()
        .equalsIgnoreCase("3")) {
      other_click();
    }
  }
  private void other_click() {
    background_change(other, other_txt,work, work_txt,home, home_txt);
    other_name_linear_fun(1);
  }

  private void home_click() {
    background_change(home, home_txt,other, other_txt,work, work_txt);
    other_name_linear_fun(0);
  }

  private void work_click() {
    background_change(work, work_txt,home, home_txt,other, other_txt);
    other_name_linear_fun(0);
  }
  private void background_change(LinearLayout select, TextView select_text,LinearLayout unselect1, TextView unselect_text1,
                                 LinearLayout unselect2, TextView unselect_text2) {

    select.setBackground(context.getResources().getDrawable(R.drawable.select_bg));
    select_text.setTextColor(context.getResources().getColor(R.color.white));

    unselect1.setBackground(context.getResources().getDrawable(R.drawable.unselect_bg));
    unselect_text1.setTextColor(context.getResources().getColor(R.color.red));

    unselect2.setBackground(context.getResources().getDrawable(R.drawable.unselect_bg));
    unselect_text2.setTextColor(context.getResources().getColor(R.color.red));
  }

  /*private void callapi_get_address() {
    saved_adds_recyclerview = (RecyclerView) findViewById(R.id.saved_adds_recyclerview);
    progressBar1.setVisibility(View.VISIBLE);
    saved_adds_recyclerview.setVisibility(View.GONE);
    final LoaderDiloag loaderDiloag = new LoaderDiloag(this);
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    Call<UserAddressModel> call = methods.get_address(userid);
    Log.d("url", "url=" + call.request().url().toString());
    loaderDiloag.displayDiloag();
    call.enqueue(new Callback<UserAddressModel>() {
      @Override
      public void onResponse(Call<UserAddressModel> call, Response<UserAddressModel> response) {
        loaderDiloag.dismissDiloag();
        int statusCode = response.code();
        Log.d("Response", "" + response);
        UserAddressModel response1 = response.body();
        savedAddressList.clear();
        if (response1.getStatus().equalsIgnoreCase("Success")) {
          savedAddressList.addAll(response1.getInfo_list());
          Collections.reverse(savedAddressList);

          saved_adds_recyclerview.setHasFixedSize(true);
          llm2 = new LinearLayoutManager(context);
          saved_adds_recyclerview.setLayoutManager(llm2);
          savedAddressAdapter = new Saved_Addresses_Adapter(context, savedAddressList);
          saved_adds_recyclerview.setAdapter(savedAddressAdapter);
          savedAddressAdapter.setOnItemClickListener(
              new Saved_Addresses_Adapter.OnItemClickListener() {
                public void onItemClick(String type, String cityid, String selected_location,
                    String longi,
                    String lati, String address_id, String name, String area, String pincode,
                    String address_type, String landmark) {
                  home_Activity_Intent(type, cityid, longi, lati, address_id, selected_location,
                      name,
                      area, pincode, address_type, landmark);
                }
              });
          progressBar1.setVisibility(View.GONE);
          saved_adds_recyclerview.setVisibility(View.VISIBLE);
        }
      }

      @Override
      public void onFailure(Call<UserAddressModel> call, Throwable t) {
        //Toast.makeText(getApplicationContext(), "internet not available..connect internet",
        //    Toast.LENGTH_LONG).show();
        loaderDiloag.dismissDiloag();
      }
    });
  }

  private void home_Activity_Intent(String type, String city_selected, String longitude_selected,
      String latitude_selected, String addressid_selected, String selected_location, String name,
      String area, String pincode, String address_type, String landmark) {
    if (mySharedPrefrencesData.getLocation(context) == null || mySharedPrefrencesData.getLocation(
        this).equalsIgnoreCase("")) {
      mySharedPrefrencesData.setLocation(context, selected_location);
      mySharedPrefrencesData.setSelectCity(context, city_selected);
      mySharedPrefrencesData.setSelectCity_latitude(context, latitude_selected);
      mySharedPrefrencesData.setSelectCity_longitude(context, longitude_selected);
      mySharedPrefrencesData.setSelectAddress_ID(context, addressid_selected);
      mySharedPrefrencesData.setSelectName(context, name);
      mySharedPrefrencesData.setSelectArea(context, area);
      mySharedPrefrencesData.setSelectMobile(context,
          mySharedPrefrencesData.get_Party_mobile(this));
      mySharedPrefrencesData.setSelectPincode(context, pincode);
      mySharedPrefrencesData.setSelectAddresstype(context, address_type);
      mySharedPrefrencesData.setSelectLandMark(context, landmark);
      Intent intent = new Intent(context, HomeActivityNew.class);
      intent.putExtra("flag", 1);
      intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      startActivity(intent);
      overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    } else {
      mySharedPrefrencesData.setLocation(context, selected_location);
      mySharedPrefrencesData.setSelectCity(context, city_selected);
      mySharedPrefrencesData.setSelectCity_latitude(context, latitude_selected);
      mySharedPrefrencesData.setSelectCity_longitude(context, longitude_selected);
      mySharedPrefrencesData.setSelectAddress_ID(context, addressid_selected);
      mySharedPrefrencesData.setSelectName(context, name);
      mySharedPrefrencesData.setSelectArea(context, area);
      mySharedPrefrencesData.setSelectMobile(context,
          mySharedPrefrencesData.get_Party_mobile(this));
      mySharedPrefrencesData.setSelectPincode(context, pincode);
      mySharedPrefrencesData.setSelectAddresstype(context, address_type);
      mySharedPrefrencesData.setSelectLandMark(context, landmark);
      Intent intent = new Intent();
      intent.putExtra("SELECTED_LOCATION", selected_location);
      intent.putExtra("type", type);
      intent.putExtra("lati", latitude_selected);
      intent.putExtra("longi", longitude_selected);
      setResult(1, intent);
      Commons.back_button_transition(context);
    }
    finish();
  }*/

  private void other_name_linear_fun(int i) {
    if (i == 1) {
      other_name_linear.setVisibility(View.VISIBLE);
    } else {
      other_name_linear.setVisibility(View.GONE);
    }
  }

  private void apicall() {
    callapi_add_address();
  }
  public void getLocationFromAddress(int loction_show, String strAddress) {
    String city_name = "";
    Geocoder coder = new Geocoder(this);
    List<Address> address;
    try {
      address = coder.getFromLocationName(strAddress, 1);
      if (address.size() > 0) {
        Address location1 = address.get(0);
        if (String.valueOf(location1.getLatitude()) != null) {
          lati = String.valueOf(location1.getLatitude());
        }
        if (String.valueOf(location1.getLongitude()) != null) {
          longi = String.valueOf(location1.getLongitude());
        }
        if (location1.getAdminArea() != null) {
          state = String.valueOf(location1.getAdminArea());
        }
        if (location1.getSubAdminArea() != null) {
          district = String.valueOf(location1.getSubAdminArea());
        } else {
          district = "";
        }
        if (location1.getPostalCode() != null) {
          //pincode.setText(location1.getPostalCode());
        }
        if (location1.getLocality() != null) {
          city_name = location1.getLocality();
        } else {
          city_name = city.getSelectedItem().toString();
        }
        if (!change_address) {
          if (cityList_name.contains(city_name)) {
            for (int i = 0; i < cityList.size(); i++) {
              if (city_name.toLowerCase()
                  .equalsIgnoreCase(cityList.get(i).getCityName().toLowerCase())) {
                city_selected = cityList.get(i).getCity_id();
                city.setSelection(i);
              }
            }
          }
        }
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  private void call_api_citylist() {
    final LoaderDiloag loaderDiloag = new LoaderDiloag(this);
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    Call<CityListModel> call = methods.get_city_list();
    Log.d("url", "url=" + call.request().url().toString());
    loaderDiloag.displayDiloag();
    call.enqueue(new Callback<CityListModel>() {
      @Override
      public void onResponse(@NonNull Call<CityListModel> call,
          @NonNull Response<CityListModel> response) {
        loaderDiloag.dismissDiloag();
        int statusCode = response.code();
        Log.d("Response", "" + response);
        CityListModel cityListModel = response.body();
        cityList.clear();
        cityList_name.clear();
        areaList.clear();
        area_list_specific.clear();
        if (!change_address) {
          area_list_specific.add(new CityListModel.cityList.area_list("", "Select Area"));
          areaList.add(area_list_specific);
          cityList.add(new CityListModel.cityList("", "Select City", area_list_specific));
          cityList.addAll(cityListModel.getCityList());
          for (int i = 0; i < cityList.size(); i++) {
            cityList_name.add(cityList.get(i).getCityName());
          }
          for (int i = 0; i < cityListModel.getCityList().size(); i++) {
            areaList.add(cityListModel.getCityList().get(i).getArea_list());
          }
          CityAdapter =
              new MySpinnerAdapter(context, android.R.layout.simple_spinner_item, cityList_name);
          CityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
          city.setAdapter(CityAdapter);
          city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position,
                long l) {
              city_selected_pos = position;
              city_selected = cityList.get(position).getCity_id();
              areaList_name.clear();
              areaList_name.add("Select Area");
              for (int i = 0; i < areaList.get(city_selected_pos).size(); i++) {
                areaList_name.add(areaList.get(city_selected_pos).get(i).getSt_suburb());
                AreaAdapter.notifyDataSetChanged();
              }
              if (mySharedPrefrencesData.getLocation(context) == null
                  || mySharedPrefrencesData.getLocation(AddAddresses_Manual.this)
                  .equalsIgnoreCase("")) {
                area.setSelection(0);
              }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
          });

          AreaAdapter =
              new MySpinnerAdapter(context, android.R.layout.simple_spinner_item, areaList_name);
          AreaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
          area.setAdapter(AreaAdapter);
          area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position,
                long l) {
              if (!city.getSelectedItem().toString().equalsIgnoreCase("Select City")) {
                area_selected_pos = position;
              } else {
                areaList_name.clear();
                areaList_name.add("Select Area");
                AreaAdapter.notifyDataSetChanged();
              }
              //area_selected = areaList.get(city_selected_pos).get(position).getIn_suburb_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
          });
        } else {
          cityList.addAll(cityListModel.getCityList());
          String city1 = mySharedPrefrencesData.getSelectCity(context);
          for (int i = 0; i < cityList.size(); i++) {
            if (city1.equalsIgnoreCase(cityList.get(i).getCity_id())) {
              city_selected_pos = cityList.indexOf(city1);
              cityList_name.add(cityList.get(i).getCityName());
              city_selected = city1;
            }
          }
          for (int i = 0; i < cityListModel.getCityList().size(); i++) {
            areaList.add(cityListModel.getCityList().get(i).getArea_list());
          }
          CityAdapter =
              new MySpinnerAdapter(context, android.R.layout.simple_spinner_item, cityList_name);
          CityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
          city.setAdapter(CityAdapter);

          areaList_name.clear();
          areaList_name.add("Select Area");
          for (int i = 0; i < areaList.get(city_selected_pos + 1).size(); i++) {
            areaList_name.add(areaList.get(city_selected_pos + 1).get(i).getSt_suburb());
          }
          area.setSelection(0);

          AreaAdapter =
              new MySpinnerAdapter(context, android.R.layout.simple_spinner_item, areaList_name);
          AreaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
          area.setAdapter(AreaAdapter);
          area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position,
                long l) {
              area_selected_pos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
          });
        }

        /*if (!locality.equalsIgnoreCase("")) {
          getLocationFromAddress(0, locality);
        }*/
        mySharedPrefrencesData_show();
      }

      @Override
      public void onFailure(Call<CityListModel> call, Throwable t) {
        loaderDiloag.dismissDiloag();
      }
    });
  }

  private void mySharedPrefrencesData_show() {
    if (mySharedPrefrencesData.getLocation(context) != null || !mySharedPrefrencesData.getLocation(
        AddAddresses_Manual.this).equalsIgnoreCase("")) {
      location.setText(mySharedPrefrencesData.getLocation(AddAddresses_Manual.this));
      name.setText(mySharedPrefrencesData.getSelectName(AddAddresses_Manual.this));
      mobile.setText(mySharedPrefrencesData.get_Party_mobile(AddAddresses_Manual.this));
      String select_city = mySharedPrefrencesData.getSelectCity(AddAddresses_Manual.this);
      String select_area = mySharedPrefrencesData.getSelectArea(AddAddresses_Manual.this);
      for (int i = 0; i < cityList.size(); i++) {
        if (select_city.equalsIgnoreCase(cityList.get(i).getCity_id().toString())) {
          city.setSelection(i);
          city_selected_pos = i;
        }
      }
      if (city_selected_pos > 0) {
        for (int i = 0; i < areaList.get(city_selected_pos).size(); i++) {
          areaList_name.add(areaList.get(city_selected_pos).get(i).getSt_suburb());
          AreaAdapter.notifyDataSetChanged();
        }
      }
      for (int i = 0; i < areaList_name.size(); i++) {
        if (select_area.equalsIgnoreCase(areaList_name.get(i).toString())) {
          area.setSelection(i + 1);
        }
      }
      landmark.setText(mySharedPrefrencesData.getSelectLandMark(AddAddresses_Manual.this));
      pincode.setText(mySharedPrefrencesData.getSelectPincode(AddAddresses_Manual.this));
      if (mySharedPrefrencesData.getSelectAddresstype(AddAddresses_Manual.this)
          .equalsIgnoreCase("1")) {
        home_click();
      } else if (mySharedPrefrencesData.getSelectAddresstype(AddAddresses_Manual.this)
          .equalsIgnoreCase("2")) {
        work_click();
      } else if (mySharedPrefrencesData.getSelectAddresstype(AddAddresses_Manual.this)
          .equalsIgnoreCase("3")) {
        other_click();
      }
    }
  }

  private void callapi_add_address() {
    final LoaderDiloag loaderDiloag = new LoaderDiloag(this);
    // if (complete_location_str.equalsIgnoreCase("")) {
    getLocationFromAddress(1, location.getText().toString()
        + ","
        + area.getSelectedItem().toString().toLowerCase()
        + ","
        + city.getSelectedItem().toString().toLowerCase()
        + pincode.getText().toString());
    // }
    String alternative_no = "";
    if (!mobile.getText().toString().equalsIgnoreCase("")) {
      alternative_no = mobile.getText().toString();
    } else {
      alternative_no = "NA";
    }
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    Call<AddUserAddressModel> call = null;
    if (!edit_address) {
      call = methods.add_address(userid, location.getText().toString(), state, district,
          pincode.getText().toString(), landmark.getText().toString(),
          "", type, longi, lati,
          other_name.getText().toString(), name.getText().toString(), alternative_no,
          area.getSelectedItem().toString(),
          city_selected);
    } else if (address_data != null && !GeneralUtil.isStringEmpty(address_data.getAddress_id())) {
      call = methods.edit_address(userid, location.getText().toString(), state, district,
          pincode.getText().toString(), landmark.getText().toString(),
          "", type, longi, lati,
          other_name.getText().toString(), name.getText().toString(), alternative_no,
          area.getSelectedItem().toString(),
          city_selected, address_data.getAddress_id());
    }
    Log.d("url", "url=" + call.request().url().toString());
    loaderDiloag.displayDiloag();
    call.enqueue(new Callback<AddUserAddressModel>() {
      @Override
      public void onResponse(Call<AddUserAddressModel> call,
          Response<AddUserAddressModel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + response);
        AddUserAddressModel response1 = response.body();
        if (response1.getStatus().equalsIgnoreCase("Success")) {

          if (edit_address) {
            Commons.back_button_transition(AddAddresses_Manual.this);
          } else {
            String complete_address = response1.getInfo().getComplete_address().toString();
            String city_selected1 = city_selected;
            String longitude_selected = response1.getInfo().getLongitude().toString();
            String latitude_selected = response1.getInfo().getLatitude().toString();
            String addressid_selected = response1.getInfo().getAddress_id().toString();
            String name = response1.getInfo().getName().toString();
            String mobile = response1.getInfo().getMobile().toString();
            String area = response1.getInfo().getArea().toString();
            String pincode = response1.getInfo().getPincode().toString();
            String address_type = response1.getInfo().getAddress_type().toString();
            String landmark = response1.getInfo().getLandmark().toString();

            if (city_selected1!=null &&
                    !city_selected1.equalsIgnoreCase("")&&
                    !city_selected1.equalsIgnoreCase("NA")) {
              mySharedPrefrencesData.setSelectCity(context, city_selected1);
            }else {
              mySharedPrefrencesData.setSelectCity(context, "FOODCITYBAM");
            }
            mySharedPrefrencesData.setSelectCity_latitude(context, latitude_selected);
            mySharedPrefrencesData.setSelectCity_longitude(context, longitude_selected);
            mySharedPrefrencesData.setSelectAddress_ID(context, addressid_selected);
            mySharedPrefrencesData.setSelectName(context, name);
            mySharedPrefrencesData.setSelectMobile(context, mobile);
            mySharedPrefrencesData.setSelectPincode(context, pincode);
            mySharedPrefrencesData.setSelectAddresstype(context, address_type);
            mySharedPrefrencesData.setSelectArea(context, area);
            mySharedPrefrencesData.setSelectLandMark(context, landmark);

                   /* lOcaldbNew.deleteCart();
                    mySharedPrefrencesData.cart_clearAllSharedData(AddAddresses_Manual.this);*/

            if (mySharedPrefrencesData.getLocation(context) == null
                || mySharedPrefrencesData.getLocation(context).equalsIgnoreCase("")) {
              mySharedPrefrencesData.setLocation(context, complete_address);
              loaderDiloag.dismissDiloag();
              Intent intent = new Intent(context, HomeActivityNew.class);
              intent.putExtra("flag", 1);
              intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
              startActivity(intent);
              overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            } else {
              String type_str = "";
              if (type.equalsIgnoreCase("0")) {
                type_str = "Other";
              }
              if (type.equalsIgnoreCase("1")) {
                type_str = "Home";
              }
              if (type.equalsIgnoreCase("2")) {
                type_str = "Work";
              }
              mySharedPrefrencesData.setLocation(context, complete_address);
              loaderDiloag.dismissDiloag();
              Intent intent = new Intent();
              intent.putExtra("SELECTED_LOCATION", complete_address);
              intent.putExtra("type", type_str);
              intent.putExtra("lati", latitude_selected);
              intent.putExtra("longi", longitude_selected);
              setResult(1, intent);
              Commons.back_button_transition(context);
            }
                  /*  if (!change_address) {
                        Intent intent = new Intent();
                        intent.putExtra("city_selected", city_selected1);
                        intent.putExtra("longitude_selected", longitude_selected);
                        intent.putExtra("latitude_selected", latitude_selected);
                        intent.putExtra("addressid_selected", addressid_selected);
                        intent.putExtra("name", name);
                        intent.putExtra("area", area);
                        intent.putExtra("ADD_ADDRESS", complete_address);
                        setResult(2, intent);
                    }else {
                            mySharedPrefrencesData.setLocation(context, complete_address);
                            Intent intent=new Intent();
                            intent.putExtra("SELECTED_LOCATION",complete_address);
                            setResult(1,intent);
                    }*/
          }
        }
      }

      @Override
      public void onFailure(Call<AddUserAddressModel> call, Throwable t) {
        loaderDiloag.dismissDiloag();
      }
    });
  }

  private void statusbar_bg(int color) {
    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
        .getColor(color)));
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case android.R.id.home:
        Commons.back_button_transition(this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    Commons.back_button_transition(this);
  }

 @Override
  public void onStart() {
    super.onStart();
  }

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

  }

  @Override
  public void onNothingSelected(AdapterView<?> parent) {

  }
}
