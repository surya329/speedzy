package com.food.order.speedzy.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.afollestad.materialdialogs.MaterialDialog;
import com.food.order.speedzy.Adapter.OrderAdapter;
import com.food.order.speedzy.Adapter.OrderImageAdapter;
import com.food.order.speedzy.FCM.Config;
import com.food.order.speedzy.FCM.NotificationUtils;
import com.food.order.speedzy.Model.OrderDataModel;
import com.food.order.speedzy.Model.Patanjali_order_deatail_model;
import com.food.order.speedzy.Model.StorepackagerModel;
import com.food.order.speedzy.Model.payment.PaymentModel;
import com.food.order.speedzy.NetworkConnectivity.NetworkStateReceiver;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.IntentUtils;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.Utils.reyclerviewutils.RecyclerViewUtils;
import com.food.order.speedzy.api.DeliveryApiService;
import com.food.order.speedzy.api.DeliveryServiceApiGenerator;
import com.food.order.speedzy.api.QueryBuilder;
import com.food.order.speedzy.api.response.OrderDetailItem;
import com.food.order.speedzy.api.response.OrderItem;
import com.food.order.speedzy.api.response.order.ListOrderStatusItem;
import com.food.order.speedzy.api.response.order.OrderStatusResponse;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.root.BaseActivity;
import com.food.order.speedzy.screen.TrackOrder.TrackOrderActivity;
import com.food.order.speedzy.screen.TrackOrder.TrackOrderDataModel;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.food.order.speedzy.Utils.SpeedzyConstants.OrderStatusType.DELIVERY_PARTNER_WAITING_AT_RESTURANT;

/**
 * Created by Sujata Mohanty.
 */

public class OrderDetails_New extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {
  public static final int ORDER_PAYMENT_REQUEST_CODE = 20;
  List<OrderDetailItem> order_detailArrayList;
  OrderItem orderdetailone;
  List<Patanjali_order_deatail_model.Grocery.ProductDetail> patanjali_order_detailArrayList;
  Patanjali_order_deatail_model.Grocery patanjaliorderdetailone;
  private Toolbar toolbar;
  RecyclerView recyclerView, recyclerViewOtherOrders;
  OrderAdapter orderDetailsAdapter;
  OrderImageAdapter orderImageAdapter;
  LinearLayoutManager lm, lmOtherOrders;
  LinearLayout order_status;
  TextView title, restaurant_name, restaurant_address, delivery_address, ord_phonenum, ord_name,
      paymenttype, del_type, order_id, order_time;
  TextView subtotaltxt, deliverychargetxt, grandtotal, gstamount, order_type, ordertittle,
      order_desc, discount_text, wallet_text, track;
  MySharedPrefrencesData mySharedPrefrencesData;
  String Veg_Flag;
  int flag;
  String oid = "", amount = "";
  List<String> orderImageList = new ArrayList<>();
  @BindView(R.id.order_payment_retry) TextView containerPayment;
  @BindView(R.id.paymentstatus) TextView txtOrderStatus;
  @BindView(R.id.container_pricing_details) LinearLayout containerPricingDetails;
  @BindView(R.id.txt_price_pending) TextView txtPricePending;
  private Apimethods mApimethods;
  Timer checkStatusTimer;
  LinearLayout activity_order_details;
  ImageView img_call;
  private static final int PERMISSIONS_REQUEST_PHONE_CALL = 100;
  String phone_digit;
  ImageView res_charge_tooltip;
  TextView restaurant__charges;
  LinearLayout restaurant__charges_lay;
  private DeliveryApiService mDeliveryApiService;
  private ListOrderStatusItem mListOrderStatusItem;
  TrackOrderDataModel trackOrderDataModel;


  private NetworkStateReceiver networkStateReceiver;
  Dialog dialog;
  private boolean mInBackground;
  private MaterialDialog mErrorDialog;
  LoaderDiloag loaderDiloag;

  @SuppressLint("SetTextI18n") @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_order_details__new_edited);
    ButterKnife.bind(this);

    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
    networkStateReceiver = new NetworkStateReceiver();
    networkStateReceiver.addListener(this);
    this.registerReceiver(networkStateReceiver, intentFilter);
    initView();
  }

  private void initView() {
    loaderDiloag=new LoaderDiloag(OrderDetails_New.this);
    trackOrderDataModel = new TrackOrderDataModel();
    mySharedPrefrencesData = new MySharedPrefrencesData();
    Veg_Flag = mySharedPrefrencesData.getVeg_Flag(this);
    flag = getIntent().getIntExtra("flag", 0);
    mApimethods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    toolbar = findViewById(R.id.toolbar);
    title = findViewById(R.id.title);
    activity_order_details = findViewById(R.id.activity_order_details);
    mDeliveryApiService =
            DeliveryServiceApiGenerator.provideRetrofit(this).create(DeliveryApiService.class);
    setSupportActionBar(toolbar);
    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    if (Veg_Flag.equalsIgnoreCase("1")) {
      statusbar_bg(R.color.red);
    } else {
      statusbar_bg(R.color.red);
    }
    if (flag == 1) {
      orderdetailone = (OrderItem) getIntent().getSerializableExtra("orderdetails");
      Log.e("recieved", orderdetailone.toString());
      order_detailArrayList = orderdetailone.getOrderDetail();
    } else {
      patanjaliorderdetailone =
              (Patanjali_order_deatail_model.Grocery) getIntent().getSerializableExtra(
                      "patanjalidetails");
      patanjali_order_detailArrayList = patanjaliorderdetailone.getProductDetails();
      if (!GeneralUtil.isStringEmpty(patanjaliorderdetailone.getOrderImage())) {
        orderImageList.addAll(Arrays.asList(patanjaliorderdetailone.getOrderImage().split(",")));
      }
    }
    subtotaltxt = findViewById(R.id.subtotal);
    res_charge_tooltip = findViewById(R.id.res_charge_tooltip);
    restaurant__charges = findViewById(R.id.restaurant__charges);
    restaurant__charges_lay = findViewById(R.id.restaurant__charges_lay);
    grandtotal = findViewById(R.id.grandtotal);
    deliverychargetxt = findViewById(R.id.deliverycharge_cart);
    gstamount = findViewById(R.id.gst_text);
    order_type = findViewById(R.id.order_type);
    ordertittle = findViewById(R.id.ordertittle);
    order_desc = findViewById(R.id.order_desc);
    img_call = findViewById(R.id.img_call);
    order_status = findViewById(R.id.order_status);
    restaurant_name = findViewById(R.id.restaurant_name);
    restaurant_address = findViewById(R.id.restaurant_address);
    delivery_address = findViewById(R.id.delivery_address);
    ord_phonenum = findViewById(R.id.ord_phonenum);
    ord_name = findViewById(R.id.ord_name);
    paymenttype = findViewById(R.id.paymenttype);
    del_type = findViewById(R.id.del_type);
    order_id = findViewById(R.id.order_id);
    order_time = findViewById(R.id.order_time);
    discount_text = findViewById(R.id.discount_text);
    wallet_text = findViewById(R.id.wallet_text);
    track = findViewById(R.id.track);

    recyclerView = findViewById(R.id.orderdetailslist);
    //lm = new LinearLayoutManager(this);
    //recyclerView.setLayoutManager(lm);
    //recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(RecyclerViewUtils.newLinearVerticalLayoutManager(this));
    recyclerView.addItemDecoration(
            RecyclerViewUtils.newVerticalSpacingItemDecoration(this, R.dimen.spacing_12, true, true));
    recyclerViewOtherOrders = findViewById(R.id.orderdetailslist_image);

    if (flag == 1) {
      recyclerView.setVisibility(View.VISIBLE);
      recyclerViewOtherOrders.setVisibility(View.GONE);
      orderDetailsAdapter = new OrderAdapter(1, this);
      //orderDetailsAdapter.shouldShowHeadersForEmptySections(true);
      recyclerView.setAdapter(orderDetailsAdapter);
      orderDetailsAdapter.refreshData(OrderDataModel.transform(order_detailArrayList));
      deliverychargetxt.setText("₹ " + orderdetailone.getDeliveryCharge());
      restaurant__charges_lay.setVisibility(View.VISIBLE);
      restaurant__charges.setText("₹ " + orderdetailone.getRestaurant_charge());
      subtotaltxt.setText("₹ " + orderdetailone.getOrderAmount());
      grandtotal.setText("₹ " + orderdetailone.getTotalAmount());
      order_type.setText(orderdetailone.getFlgOrderType());
      title.setText("Order ID : " + orderdetailone.getOrderid());
      //ordertittle.setText(orderdetailone.getOrderRunningstatus().getStatusTitle());
      //if (orderdetailone.getOrderRunningstatus()
      //    .getStatusDescription() != null && orderdetailone.getOrderRunningstatus()
      //    .getStatusDescription().toLowerCase()
      //    .contains("delivery partner")) {
      //  order_desc.setText(orderdetailone.getOrderRunningstatus().getStatusDescription()
      //      .replace("partner",
      //          "partner " + orderdetailone.getOrderRunningstatus().getDeliveryPersonName()));
      //  img_call.setVisibility(View.VISIBLE);
      //} else {
      //  order_desc.setText(orderdetailone.getOrderRunningstatus().getStatusDescription());
      //}
      //setFoodDeliveryStatus();
      img_call.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (orderdetailone.getOrderRunningstatus() != null
                  && !GeneralUtil.isStringEmpty(
                  orderdetailone.getOrderRunningstatus().getDeliveryPersonNumber())) {
            Dialog_show_call(orderdetailone.getOrderRunningstatus().getDeliveryPersonNumber());
          } else if (mListOrderStatusItem != null && !GeneralUtil.isStringEmpty(
                  mListOrderStatusItem.getDeliveryGuyMobileNo())) {
            Dialog_show_call(mListOrderStatusItem.getDeliveryGuyMobileNo());
          }
        }
      });
      checkOrderStatus(orderdetailone.getFlgOrderStatus());
      delivery_address.setText(orderdetailone.getStreetnumber() + ","
              + orderdetailone.getNearestCrossCity() + ","
              + orderdetailone.getAddress() + ","
              + orderdetailone.getCompanyname());
      ord_phonenum.setText(orderdetailone.getMobile());
      ord_name.setText(orderdetailone.getFname() + " " + orderdetailone.getLname());
      if (orderdetailone.getPaymentMethod().equalsIgnoreCase("2")) {
        paymenttype.setText("Cash on Delivery");
      } else if (orderdetailone.getPaymentMethod().equalsIgnoreCase("1")
              || orderdetailone.getPaymentMethod().equalsIgnoreCase("3")) {
        paymenttype.setText("Card Payment");
      } else if (orderdetailone.getPaymentMethod().equalsIgnoreCase("4")) {
        paymenttype.setText("UPI Payment");
      }
      del_type.setText(orderdetailone.getFlgOrderType());
      order_id.setText(orderdetailone.getOrderid());
      order_time.setText("" + getdate(orderdetailone.getPreOrderDate()));
      restaurant_name.setText(orderdetailone.getRestaurantName());
      restaurant_address.setText(orderdetailone.getResAddress());
      discount_text.setText("-₹ " + Double.parseDouble(orderdetailone.getDicount_val()));
      if (orderdetailone.getWallet() == null) {
        wallet_text.setText("-₹ 0.00");
      } else {
        wallet_text.setText("-₹ " + Double.parseDouble(orderdetailone.getWallet()));
      }
      if (orderdetailone.getFlgOrderStatus().equalsIgnoreCase("6")) {
        order_status.setVisibility(View.VISIBLE);
      } else {
        order_status.setVisibility(View.GONE);
      }
      if (orderdetailone.getPaymentStatus().equalsIgnoreCase("0")) {
        String s = "<font color=\"#ff0000\"><bold>" + "UnPaid" + "</font></bold>";
        txtOrderStatus.setText(Html.fromHtml(s));
      } else if (orderdetailone.getPaymentStatus().equalsIgnoreCase("1")) {
        String s = "<font color=\"#4caf50\"><bold>" + "Paid" + "</font></bold>";
        txtOrderStatus.setText(Html.fromHtml(s));
      }
      if (orderdetailone.getPaymentMethod().equalsIgnoreCase("4")
              && orderdetailone.getPaymentStatus().equalsIgnoreCase("0")) {
        containerPayment.setVisibility(View.VISIBLE);
      } else if (orderdetailone.getPaymentMethod().equalsIgnoreCase("4")) {
        containerPayment.setVisibility(View.GONE);
      }
      callApiToCheckPaymentStatus(orderdetailone.getOrderid());
    } else {
      //           orderDetailsAdapter=new OrderDetailsAdapter( patanjali_order_detailArrayList,this);
      //           orderDetailsAdapter.shouldShowHeadersForEmptySections(true);
      //           recyclerView.setAdapter(orderDetailsAdapter);
      //List<String> myList =
      //    new ArrayList<String>(Arrays.asList(patanjaliorderdetailone.getOrderImage().split(",")));
      recyclerView.setVisibility(View.GONE);
      recyclerViewOtherOrders.setVisibility(View.VISIBLE);
      if (orderImageList != null && orderImageList.size() > 0) {
        lmOtherOrders = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewOtherOrders.setLayoutManager(lmOtherOrders);
        recyclerViewOtherOrders.setHasFixedSize(true);
        orderImageAdapter = new OrderImageAdapter(this);
        orderImageAdapter.setOrderType(patanjaliorderdetailone.getOrderName());
        recyclerViewOtherOrders.setAdapter(orderImageAdapter);
        orderImageAdapter.refreshData(orderImageList);
      } else {
        lmOtherOrders = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewOtherOrders.setLayoutManager(lmOtherOrders);
        recyclerViewOtherOrders.setHasFixedSize(true);
        orderDetailsAdapter = new OrderAdapter(2, this);
        //orderDetailsAdapter.shouldShowHeadersForEmptySections(true);
        recyclerViewOtherOrders.setAdapter(orderDetailsAdapter);
        orderDetailsAdapter.refreshData(
                OrderDataModel.transformOrderDetails(patanjali_order_detailArrayList));
      }
      deliverychargetxt.setText("₹ " + patanjaliorderdetailone.getShippingCharge());
      restaurant__charges_lay.setVisibility(View.GONE);
      subtotaltxt.setText("₹ " + patanjaliorderdetailone.getTotalSalesprice());
      grandtotal.setText("₹ " + patanjaliorderdetailone.getOrderAmount());
      order_type.setText("Delivery");
      title.setText("Order ID : " + patanjaliorderdetailone.getOrderNumber());
      if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("1")) {
        ordertittle.setText("Order Received");
        ordertittle.setTextColor(getResources().getColor(R.color.black_transparent));
      } else if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("2")) {
        ordertittle.setText("Order accepted");
        ordertittle.setTextColor(getResources().getColor(R.color.black_transparent));
      } else if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("3")) {
        ordertittle.setText("Rejected");
        ordertittle.setTextColor(getResources().getColor(R.color.black_transparent));
        ordertittle.setVisibility(View.VISIBLE);
      } else if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("4")) {
        ordertittle.setText("Order Picked up");
        ordertittle.setTextColor(getResources().getColor(R.color.black_transparent));
      } else if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("5")) {
        ordertittle.setText("Delivery started");
        ordertittle.setTextColor(getResources().getColor(R.color.black_transparent));
      } else if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("6")) {
        ordertittle.setText("Delivery end");
        ordertittle.setTextColor(getResources().getColor(R.color.black_transparent));
      } else if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("7")) {
        ordertittle.setText("Item change in progress");
        ordertittle.setTextColor(getResources().getColor(R.color.black_transparent));
      }
      order_desc.setVisibility(View.GONE);
      String[] address_list =
              patanjaliorderdetailone.getShippingAddress().address1.split(",");
      String address = "";
      for (String name : address_list) {
        address = address + name + "\n";
      }
      delivery_address.setText(address);
      ord_phonenum.setText(patanjaliorderdetailone.getShippingAddress().getPhone());
      ord_name.setText(patanjaliorderdetailone.getShippingAddress().getFirstname()
              + " "
              + patanjaliorderdetailone.getShippingAddress().getLastname());
      if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("2")) {
        paymenttype.setText("Cash on Delivery");
      } else if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("1")
              || patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("3")) {
        paymenttype.setText("Card Payment");
      } else if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("4")) {
        paymenttype.setText("UPI Payment");
      }

      if (patanjaliorderdetailone.getPaymentStatus().equalsIgnoreCase("0")) {
        String s = "<font color=\"#ff0000\"><bold>" + "UnPaid" + "</font></bold>";
        txtOrderStatus.setText(Html.fromHtml(s));
      } else if (patanjaliorderdetailone.getPaymentStatus().equalsIgnoreCase("1")) {
        String s = "<font color=\"#4caf50\"><bold>" + "Paid" + "</font></bold>";
        txtOrderStatus.setText(Html.fromHtml(s));
      }
      if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("4")
              && patanjaliorderdetailone.getPaymentStatus().equalsIgnoreCase("0")) {
        containerPayment.setVisibility(View.VISIBLE);
      } else if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("4")) {
        containerPayment.setVisibility(View.GONE);
      }
      callApiToCheckPaymentStatus(patanjaliorderdetailone.getOrderNumber());
      del_type.setText("Delivery");
      order_id.setText(patanjaliorderdetailone.getOrderNumber());
      order_time.setText("" + getdate(patanjaliorderdetailone.getCreated()));
      restaurant_name.setText(patanjaliorderdetailone.getOrderName());
      restaurant_address.setText("Speedzy Store");
      discount_text.setText(
              "-₹ " + Double.parseDouble(patanjaliorderdetailone.getAdditionalDiscount()));
      wallet_text.setText("-₹ " + Double.parseDouble(patanjaliorderdetailone.getWalletAmount()));
      if (patanjaliorderdetailone.getOrderStatus().equalsIgnoreCase("6")) {
        order_status.setVisibility(View.VISIBLE);
      } else {
        order_status.setVisibility(View.GONE);
      }

      if (patanjaliorderdetailone.getOrderAmount().equals("0")
              || patanjaliorderdetailone.getOrderAmount().equals("0.00")) {
        containerPricingDetails.setVisibility(View.GONE);
        txtPricePending.setText(
                patanjaliorderdetailone != null && patanjaliorderdetailone.getOrderName()
                        .equalsIgnoreCase(
                                SpeedzyConstants.GROCERY_ORDER)
                        ? R.string.order_label_grocerypricing : R.string.order_label_medicinepricing);
        txtPricePending.setVisibility(View.VISIBLE);
      } else {
        containerPricingDetails.setVisibility(View.VISIBLE);
        txtPricePending.setVisibility(View.GONE);
      }
    }
    activity_order_details.setVisibility(View.VISIBLE);

    track.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (flag == 1) {
          trackOrderDataModel.setOrderId(orderdetailone.getOrderid());
          if (orderdetailone != null && !GeneralUtil.isStringEmpty(
                  orderdetailone.getRes_latitude())) {
            trackOrderDataModel.setPickup(
                    new LatLng(Double.parseDouble(orderdetailone.getRes_latitude()),
                            Double.parseDouble(orderdetailone.getRes_longitude())));
            trackOrderDataModel.setPickupAddress(orderdetailone.getResAddress());
            trackOrderDataModel.setDestination(
                    GeneralUtil.getLocationFromAddress(OrderDetails_New.this,
                            orderdetailone.getAddress(),
                            Double.parseDouble(orderdetailone.getUser_latitude()),
                            Double.parseDouble(orderdetailone.getUser_longitude())));
            trackOrderDataModel.setDestinationAddress(orderdetailone.getAddress());
            if (orderdetailone.getOrderRunningstatus() != null) {
              if (!GeneralUtil.isStringEmpty(
                      orderdetailone.getOrderRunningstatus().getStatusTitle())
                      && !GeneralUtil.isStringEmpty(
                      orderdetailone.getOrderRunningstatus().getStatusDescription())) {
                trackOrderDataModel.setOrderTitle(
                        orderdetailone.getOrderRunningstatus().getStatusTitle());
                trackOrderDataModel.setOrderStatusMessage(
                        orderdetailone.getOrderRunningstatus().getStatusDescription());
              }
              //if (!GeneralUtil.isStringEmpty(
              //    orderdetailone.getOrderRunningstatus().getDeliveryPersonName())) {
              //  trackOrderDataModel.setDeliveryBoyName(
              //      orderdetailone.getOrderRunningstatus().getDeliveryPersonName());
              //}
              //if (!GeneralUtil.isStringEmpty(
              //    orderdetailone.getOrderRunningstatus().getDeliveryPersonId())) {
              //  trackOrderDataModel.setDeliveryBoyId(
              //      orderdetailone.getOrderRunningstatus().getDeliveryPersonId());
              //}
              //if (!GeneralUtil.isStringEmpty(
              //    orderdetailone.getOrderRunningstatus().getDeliveryPersonNumber())) {
              //  trackOrderDataModel.setDeliveryBoyNumber(
              //      orderdetailone.getOrderRunningstatus().getDeliveryPersonNumber());
              //}
              updateStatusFromSpeedzyResponse();
            }
          } else if (orderdetailone != null) {
            trackOrderDataModel.setPickup(
                    GeneralUtil.getLocationFromAddress(OrderDetails_New.this,
                            orderdetailone.getResAddress()));
            trackOrderDataModel.setPickupAddress(orderdetailone.getResAddress());
            trackOrderDataModel.setDestination(
                    GeneralUtil.getLocationFromAddress(OrderDetails_New.this,
                            orderdetailone.getAddress()));
            trackOrderDataModel.setDestinationAddress(orderdetailone.getAddress());
            if (orderdetailone.getOrderRunningstatus() != null) {
              if (!GeneralUtil.isStringEmpty(
                      orderdetailone.getOrderRunningstatus().getStatusTitle())
                      && !GeneralUtil.isStringEmpty(
                      orderdetailone.getOrderRunningstatus().getStatusDescription())) {
                trackOrderDataModel.setOrderTitle(
                        orderdetailone.getOrderRunningstatus().getStatusTitle());
                trackOrderDataModel.setOrderStatusMessage(
                        orderdetailone.getOrderRunningstatus().getStatusDescription());
              }
              //if (!GeneralUtil.isStringEmpty(
              //    orderdetailone.getOrderRunningstatus().getDeliveryPersonName())) {
              //  trackOrderDataModel.setDeliveryBoyName(
              //      orderdetailone.getOrderRunningstatus().getDeliveryPersonName());
              //}
              //if (!GeneralUtil.isStringEmpty(
              //    orderdetailone.getOrderRunningstatus().getDeliveryPersonId())) {
              //  trackOrderDataModel.setDeliveryBoyId(
              //      orderdetailone.getOrderRunningstatus().getDeliveryPersonId());
              //}
              //if (!GeneralUtil.isStringEmpty(
              //    orderdetailone.getOrderRunningstatus().getDeliveryPersonNumber())) {
              //  trackOrderDataModel.setDeliveryBoyNumber(
              //      orderdetailone.getOrderRunningstatus().getDeliveryPersonNumber());
              //}
              updateStatusFromSpeedzyResponse();
            }
          }
          startActivity(
                  TrackOrderActivity.newIntent(OrderDetails_New.this, trackOrderDataModel));
          overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
      }
    });
  }

  private void updateStatusFromSpeedzyResponse() {
    if (GeneralUtil.isStringEmpty(trackOrderDataModel.getDeliveryBoyName())
        && !GeneralUtil.isStringEmpty(
        orderdetailone.getOrderRunningstatus().getDeliveryPersonName())) {
      trackOrderDataModel.setDeliveryBoyName(
          orderdetailone.getOrderRunningstatus().getDeliveryPersonName());
    }
    if (GeneralUtil.isStringEmpty(trackOrderDataModel.getDeliveryBoyId())
        && !GeneralUtil.isStringEmpty(
        orderdetailone.getOrderRunningstatus().getDeliveryPersonId())) {
      trackOrderDataModel.setDeliveryBoyId(
          orderdetailone.getOrderRunningstatus().getDeliveryPersonId());
    }
    if (GeneralUtil.isStringEmpty(trackOrderDataModel.getDeliveryBoyNumber())
        && !GeneralUtil.isStringEmpty(
        orderdetailone.getOrderRunningstatus().getDeliveryPersonNumber())) {
      trackOrderDataModel.setDeliveryBoyNumber(
          orderdetailone.getOrderRunningstatus().getDeliveryPersonNumber());
    }
  }

  private void Dialog_show_call(String mobile) {
    BottomSheetDialog dialog = new BottomSheetDialog(OrderDetails_New.this, R.style.SheetDialog);
    dialog.setContentView(R.layout.call_dialog);
    TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
    TextView mobile_no = (TextView) dialog.findViewById(R.id.mobile_no);
    LinearLayout mobile_no_linear = dialog.findViewById(R.id.mobile_no_linear);
    mobile_no.setText("Call " + mobile);
    cancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        dialog.dismiss();
      }
    });
    phone_digit = mobile.trim().replaceAll("[^\\.0123456789]", "").trim();
    mobile_no_linear.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        call();
        dialog.dismiss();
      }
    });
    if (!isFinishing()) {
      dialog.show();
    }
  }

  private void call() {
    if (!GeneralUtil.isStringEmpty(phone_digit)) {
      startActivity(IntentUtils.callDialerIntent(phone_digit));
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    if (requestCode == PERMISSIONS_REQUEST_PHONE_CALL) {
      if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        // Permission is granted
        call();
      } else {
        Toast.makeText(getApplicationContext(), "Sorry!!! Permission Denied", Toast.LENGTH_SHORT)
            .show();
      }
    }
  }

  private void checkOrderStatus(String flgOrderStatus) {
    if (flgOrderStatus.equalsIgnoreCase("3")
        || flgOrderStatus.equalsIgnoreCase("7")
        || flgOrderStatus.equalsIgnoreCase("2") || flgOrderStatus.equalsIgnoreCase("6")) {
      track.setVisibility(View.GONE);
    } else {
      track.setVisibility(View.VISIBLE);
    }
  }

  private String getdate(String deliveryDate) {
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
    Date d = null;
    String changedDate = "";
    try {
      d = dateFormatter.parse(deliveryDate);
      SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy H:mm:ss");
      changedDate = dateFormat.format(d);
    } catch (ParseException e) {
      e.printStackTrace();
      changedDate = deliveryDate;
    }
    return changedDate;
  }

  private void statusbar_bg(int color) {
    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
        .getColor(color)));
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case android.R.id.home:
        Commons.back_button_transition(OrderDetails_New.this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    Commons.back_button_transition(OrderDetails_New.this);
  }

  @OnClick(R.id.order_payment_retry) void onClickMakePayment() {
    BottomSheetDialog dialog = new BottomSheetDialog(OrderDetails_New.this, R.style.SheetDialog);
    dialog.setContentView(R.layout.payment_dialog);
    final ImageView google_pay = (ImageView) dialog.findViewById(R.id.google_pay);
    final ImageView phone_pe = (ImageView) dialog.findViewById(R.id.phone_pe);
    final ImageView paytm = (ImageView) dialog.findViewById(R.id.paytm);
    final ImageView bhim = (ImageView) dialog.findViewById(R.id.bhim);
    TextView text_upi = dialog.findViewById(R.id.text_upi);

    if (flag == 1 && orderdetailone != null && !GeneralUtil.isStringEmpty(
        orderdetailone.getOrderid()) && !GeneralUtil.isStringEmpty(
        orderdetailone.getOrderAmount())) {
      Commons.menu_id = "M1001";
      oid = orderdetailone.getOrderid();
      amount = orderdetailone.getTotalAmount();
      //upi_method_call(oid,amount);
    } else if (flag == 2 && patanjaliorderdetailone != null && !GeneralUtil.isStringEmpty(
        patanjaliorderdetailone.getOrderNumber()) && !GeneralUtil.isStringEmpty(
        patanjaliorderdetailone.getOrderAmount())) {
      Commons.menu_id = patanjaliorderdetailone.getId();
      oid = patanjaliorderdetailone.getOrderNumber();
      amount = patanjaliorderdetailone.getOrderAmount();
      //upi_method_call(oid,amount);
    }

    text_upi.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent browserIntent =
            new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.npci.org.in/upi-faq-s"));
        startActivity(browserIntent);
      }
    });
    google_pay.setOnClickListener(new View.OnClickListener() {
      @SuppressLint("DefaultLocale") @Override
      public void onClick(View v) {
        Commons.google_pay_call(1, OrderDetails_New.this,
            oid, amount);
      }
    });
    phone_pe.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Commons.google_pay_call(2, OrderDetails_New.this,
            oid, amount);
      }
    });
    paytm.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
       /* Intent intent = new Intent(OrderUPIPaymentActivity.this, PaymentWebView_food.class);
        intent.putExtra("order_id", mOrderUPIDataModel.getOrderNumber());
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);*/
        Commons.google_pay_call(4, OrderDetails_New.this,
            oid, amount);
      }
    });
    bhim.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Commons.google_pay_call(3, OrderDetails_New.this,
            oid, amount);
      }
    });
    dialog.show();
  }

  private void upi_method_call(String oid, String amount) {
    Uri uri =
        new Uri.Builder()
            .scheme("upi")
            .authority("pay")
            .appendQueryParameter("pa", "bharatpe09894621521@yesbankltd")
            .appendQueryParameter("pn", "Speedzy India")
            .appendQueryParameter("tr", oid)
            .appendQueryParameter("tn", "Speedzy Order")
            .appendQueryParameter("am", amount)
            .appendQueryParameter("cu", "INR")
            .build();
    final Intent intent = new Intent();
    intent.setAction(Intent.ACTION_VIEW);
    intent.setData(uri);
    Intent chooser = Intent.createChooser(intent, "Pay with...");
    startActivityForResult(chooser, ORDER_PAYMENT_REQUEST_CODE, null);
  }

  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    try {
      if ((requestCode == 11 && resultCode == RESULT_OK) ||
          (requestCode == 22 && resultCode == RESULT_OK) ||
          (requestCode == 33 && resultCode == RESULT_OK) ||
          (requestCode == 44 && resultCode == RESULT_OK)) {
        if (data != null) {
          payment_status(data);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
   /* if (requestCode == ORDER_PAYMENT_REQUEST_CODE && resultCode == RESULT_OK) {
      if (data != null) {
        payment_status(data);
      }
    }*/
    super.onActivityResult(requestCode, resultCode, data);
  }

  private void payment_status(Intent data) {
    Bundle bundle = data.getExtras();
    if (bundle != null) {
      for (String key : bundle.keySet()) {
        Log.e("UPI_txn=>", key + ":" + bundle.get(key));
      }
    }
    String orderid = data.getStringExtra("txnRef");
    Log.e("UPI_txn", orderid);
    Log.e("UPI_txn", data.getStringExtra("Status"));
    if (Objects.requireNonNull(data.getStringExtra("Status")).equalsIgnoreCase("SUCCESS")) {
      //String s = "<font color=\"#4caf50\"><bold>" + "Paid" + "</font></bold>";
      //txtOrderStatus.setText(Html.fromHtml(s));
      Toast.makeText(this, "Payment Successful", Toast.LENGTH_SHORT).show();
      containerPayment.setVisibility(View.GONE);
      apicall();
    } else {
      //Toast.makeText(this, "Payment Failed", Toast.LENGTH_SHORT).show();
      //String s = "<font color=\"#ff0000\"><bold>" + "UnPaid" + "</font></bold>";
      //txtOrderStatus.setText(Html.fromHtml(s));
      Toast.makeText(this, "Payment Failed", Toast.LENGTH_SHORT).show();
      containerPayment.setVisibility(View.VISIBLE);
    }
  }

  private void apicall() {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    Call<StorepackagerModel> call =
        methods.changepayment(orderdetailone.getOrderid(), Commons.menu_id);
    Log.e("url", "url=" + call.request().url().toString());
    call.enqueue(new Callback<StorepackagerModel>() {
      @Override
      public void onResponse(Call<StorepackagerModel> call, Response<StorepackagerModel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        StorepackagerModel deviceUpdateModel = response.body();
        if (deviceUpdateModel != null && deviceUpdateModel.getStatus()
            .equalsIgnoreCase("Success")) {
        }
      }

      @Override
      public void onFailure(@NonNull Call<StorepackagerModel> call, @NonNull Throwable t) {
        showErrorDialog(t.getLocalizedMessage());
      }
    });
  }

  //public void checkPaymentStatus(String orderId) {
  //  if (checkStatusTimer != null) {
  //    checkStatusTimer.cancel();
  //    checkStatusTimer.purge();
  //    checkStatusTimer = null;
  //  }
  //  long timeDelay;
  //  final Handler handler = new Handler();
  //  checkStatusTimer = new Timer();
  //  timeDelay = 5000;
  //  checkStatusTimer.schedule(new TimerTask() {
  //    @Override
  //    public void run() {
  //      handler.post(new Runnable() {
  //        @Override
  //        public void run() {
  //          callApiToCheckPaymentStatus(orderId);
  //        }
  //      });
  //    }
  //  }, 0, timeDelay);
  //}

  private void callApiToCheckPaymentStatus(String orderId) {
    if (orderId != null) {
      String orderIdNew = orderId.replaceAll("#", "").trim();
      mApimethods.getPaymentStatus(orderIdNew).subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<PaymentModel>() {
        @Override public void onSubscribe(Disposable d) {
        }

        @Override public void onNext(PaymentModel paymentModel) {
          if (paymentModel != null && paymentModel.getPaymentType() != null &&
              paymentModel.getPaymentType()
                  .equalsIgnoreCase("4")
              && paymentModel.getPaymentStatus() != null && paymentModel.getPaymentStatus()
              .equalsIgnoreCase("1")) {
            String s = "<font color=\"#4caf50\"><bold>" + "Paid" + "</font></bold>";
            txtOrderStatus.setText(Html.fromHtml(s));
            containerPayment.setVisibility(View.GONE);
          }
        }

        @Override public void onError(Throwable e) {
        }

        @Override public void onComplete() {
        }
      });
    }
  }


  private void getOrderStatus(String orderId) {
    loaderDiloag.displayDiloag();
    Log.e("query", QueryBuilder.getOrderStatus(orderId.replaceAll("#", "").trim()));
    mDeliveryApiService.getOrderStatus(
        QueryBuilder.getOrderStatus(orderId.replaceAll("#", "").trim()))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<OrderStatusResponse>() {
          @Override public void onSubscribe(Disposable d) {

          }

          @Override public void onNext(OrderStatusResponse response) {
            loaderDiloag.dismissDiloag();
            if (response.getData() != null
                && response.getData().getListOrderStatus() != null
                && response.getData().getListOrderStatus().size() > 0) {
              mListOrderStatusItem = response.getData().getListOrderStatus().get(0);
              showFoodStatusData(response.getData().getListOrderStatus().get(0));
            } else {
              setFoodDeliveryStatus();
            }
          }

          @Override public void onError(Throwable e) {
            loaderDiloag.dismissDiloag();
          }

          @Override public void onComplete() {

          }
        });
  }

  protected void showErrorDialog(String message) {
    if (mErrorDialog == null) {
      mErrorDialog = new MaterialDialog.Builder(this).title(R.string.general_label_error)
              .content(message)
              .positiveText(R.string.general_label_ok)
              .build();
    } else {
      mErrorDialog.setContent(message);
    }
    if (!isInBackground()) {
      mErrorDialog.show();
    }
  }

  private void setFoodDeliveryStatus() {
    ordertittle.setText(orderdetailone.getOrderRunningstatus().getStatusTitle());
    if (orderdetailone.getOrderRunningstatus()
        .getStatusDescription() != null && orderdetailone.getOrderRunningstatus()
        .getStatusDescription().toLowerCase()
        .contains("delivery partner")) {
      order_desc.setText(orderdetailone.getOrderRunningstatus().getStatusDescription()
          .replace("partner",
              "partner " + orderdetailone.getOrderRunningstatus().getDeliveryPersonName()));
      img_call.setVisibility(View.VISIBLE);
    } else {
      order_desc.setText(orderdetailone.getOrderRunningstatus().getStatusDescription());
    }
  }

  private void showFoodStatusData(ListOrderStatusItem listOrderStatusItem) {
    if (trackOrderDataModel == null) {
      trackOrderDataModel = new TrackOrderDataModel();
    }
    switch (listOrderStatusItem.getStatus()) {
      case SpeedzyConstants.OrderStatusType.ORDER_RECIEVED:
        ordertittle.setText(SpeedzyConstants.OrderStatusTitle.ORDER_RECIEVED);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.AWAITING_CONFIRMATION_RESTURANT);
        img_call.setVisibility(View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.ACCEPTED:
        ordertittle.setText(SpeedzyConstants.OrderStatusTitle.ACCEPTED);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.ACCEPTED);
        img_call.setVisibility(View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.CANCELLED:
        ordertittle.setText(SpeedzyConstants.OrderStatusTitle.CANCELLED);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.CANCELLED);
        img_call.setVisibility(View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.ORDER_CANCELLED:
        ordertittle.setText(SpeedzyConstants.OrderStatusTitle.CANCELLED);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.CANCELLED);
        img_call.setVisibility(View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.DELIVERY_PARTNER_ASSIGNED:
        setDeliveryGuyValues(listOrderStatusItem);
        ordertittle.setText(SpeedzyConstants.OrderStatusTitle.DELIVERY_PARTNER_ASSIGNED);
        if (!GeneralUtil.isStringEmpty(listOrderStatusItem.getDeliveryGuyName())) {
          order_desc.setText(SpeedzyConstants.OrderStatusMesssage.DELIVERY_PARTNER_ASSIGNED
              .replace("Delivery Partner", listOrderStatusItem.getDeliveryGuyName()));
        } else {
          order_desc.setText(SpeedzyConstants.OrderStatusMesssage.DELIVERY_PARTNER_ASSIGNED);
        }
        img_call.setVisibility(View.VISIBLE);
        break;
      case SpeedzyConstants.OrderStatusType.ORDER_PICKED_UP:
        setDeliveryGuyValues(listOrderStatusItem);
        ordertittle.setText(SpeedzyConstants.OrderStatusTitle.DELIVERY_PARTNER_ASSIGNED);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.DELIVERY_PARTNER_ASSIGNED);
        img_call.setVisibility(View.VISIBLE);
        break;
      case SpeedzyConstants.OrderStatusType.ORDER_DELIVERED:
        setDeliveryGuyValues(listOrderStatusItem);
        ordertittle.setText(SpeedzyConstants.OrderStatusTitle.ORDER_DELIVERED);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.ORDER_DELIVERED);
        img_call.setVisibility(View.VISIBLE);
        break;
      case SpeedzyConstants.OrderStatusType.ORDER_CANCEL:
        setDeliveryGuyValues(listOrderStatusItem);
        ordertittle.setText(SpeedzyConstants.OrderStatusTitle.ORDER_CANCEL);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.ORDER_CANCEL);
        img_call.setVisibility(View.GONE);
        break;
      case SpeedzyConstants.OrderStatusType.ORDER_READY_FOR_PICKUP:
        setDeliveryGuyValues(listOrderStatusItem);
        ordertittle.setText(SpeedzyConstants.OrderStatusTitle.ORDER_READY_FOR_PICKUP);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.ORDER_READY_FOR_PICKUP);
        img_call.setVisibility(View.VISIBLE);
        break;
      case DELIVERY_PARTNER_WAITING_AT_RESTURANT:
        setDeliveryGuyValues(listOrderStatusItem);
        ordertittle.setText(
            SpeedzyConstants.OrderStatusTitle.DELIVERY_PARTNER_WAITING_AT_RESTURANT);
        order_desc.setText(
            SpeedzyConstants.OrderStatusMesssage.DELIVERY_PARTNER_WAITING_AT_RESTURANT);
        img_call.setVisibility(View.VISIBLE);
        break;
      case SpeedzyConstants.OrderStatusType.WE_CANT_REACH_YOU:
        setDeliveryGuyValues(listOrderStatusItem);
        ordertittle.setText(
            SpeedzyConstants.OrderStatusTitle.WE_CANT_REACH_YOU);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.WE_CANT_REACH_YOU);
        img_call.setVisibility(View.VISIBLE);
        break;
      case SpeedzyConstants.OrderStatusType.AWAITING_CONFIRMATION_RESTURANT:
        ordertittle.setText(
            SpeedzyConstants.OrderStatusTitle.AWAITING_CONFIRMATION_RESTURANT);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.AWAITING_CONFIRMATION_RESTURANT);
        img_call.setVisibility(View.GONE);
        break;
      default:
        ordertittle.setText(
            SpeedzyConstants.OrderStatusTitle.AWAITING_CONFIRMATION_RESTURANT);
        order_desc.setText(SpeedzyConstants.OrderStatusMesssage.AWAITING_CONFIRMATION_RESTURANT);
        img_call.setVisibility(View.GONE);
        break;
    }
  }

  private void setDeliveryGuyValues(ListOrderStatusItem listOrderStatusItem) {
    if (!GeneralUtil.isStringEmpty(listOrderStatusItem.getDeliveryGuyId())) {
      trackOrderDataModel.setDeliveryBoyId(listOrderStatusItem.getDeliveryGuyId());
    }
    if (!GeneralUtil.isStringEmpty(listOrderStatusItem.getDeliveryGuyName())) {
      trackOrderDataModel.setDeliveryBoyName(listOrderStatusItem.getDeliveryGuyName());
    }
    if (!GeneralUtil.isStringEmpty(listOrderStatusItem.getDeliveryGuyMobileNo())) {
      trackOrderDataModel.setDeliveryBoyNumber(listOrderStatusItem.getDeliveryGuyMobileNo());
    }
  }

  @Override public void onResume() {
      super.onResume();
      mInBackground = false;
      if (orderdetailone != null && !GeneralUtil.isStringEmpty(orderdetailone.getOrderid())) {
        getOrderStatus(orderdetailone.getOrderid());
      }
    }
  @Override
  protected void onDestroy() {
    super.onDestroy();
    networkStateReceiver.removeListener(this);
    this.unregisterReceiver(networkStateReceiver);
  }

  @Override
  public void networkAvailable() {
    if (dialog != null && dialog.isShowing() && !isFinishing()) {
      dialog.dismiss();
    }
    if (orderdetailone != null && !GeneralUtil.isStringEmpty(orderdetailone.getOrderid())) {
      getOrderStatus(orderdetailone.getOrderid());
    }
  }
  @Override
  public void networkUnavailable() {
    popupshow();
  }
  private void popupshow() {
    if (dialog == null) {
      dialog = new Dialog(this);
      dialog.setContentView(R.layout.connection_popup);
      dialog.setCancelable(false);
      Objects.requireNonNull(dialog.getWindow())
              .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
    if (!isInBackground()) {
      dialog.show();
    }
  }

  protected boolean isInBackground() {
    return mInBackground;
  }
}
