package com.food.order.speedzy.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.order.speedzy.Model.DeliveryAddressModel;
import com.food.order.speedzy.R;

import java.util.ArrayList;

/**
 * Created by Sujata Mohanty.
 */


public class DeliveryAddAdapter extends RecyclerView.Adapter<DeliveryAddAdapter.MyViewHolder> {

    private ArrayList<DeliveryAddressModel> deliveryAddList;
    Context mContext;
    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout mParentLayout;
        public TextView mAddress,type;
        private MyViewHolder(View view) {
            super(view);
            mParentLayout = (RelativeLayout)itemView.findViewById(R.id.predictedRow);
            mAddress = (TextView)itemView.findViewById(R.id.landmark);
            type= (TextView)itemView.findViewById(R.id.type);
        }
    }

    public DeliveryAddAdapter(Context mContext, ArrayList<DeliveryAddressModel> deliveryAddList) {
        this.deliveryAddList = deliveryAddList;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_recent_search, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DeliveryAddressModel deliveryData = deliveryAddList.get(position);
        holder.mAddress.setText(deliveryData.getLocation());
        holder.mAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(holder.mAddress.getText().toString());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return deliveryAddList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(String address);

    }
}