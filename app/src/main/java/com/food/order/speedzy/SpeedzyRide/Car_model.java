package com.food.order.speedzy.SpeedzyRide;

import java.io.Serializable;

public class Car_model implements Serializable {
    int image;
    String name;
    String model;

    public Car_model(int image, String name, String model, String facility, String price) {
        this.image=image;
        this.name=name;
        this.model=model;
        this.facility=facility;
        this.price=price;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    String facility;
    String price;
}
