package com.food.order.speedzy.Model;

import java.io.Serializable;

/**
 * Created by Sujata Mohanty.
 */


public class Payment_OrderDetail implements Serializable {

    private String Extra_Item;
    private String name;
    private String id;
    private String qty;
    private String Unit;
    private String Actual_Price;
    private String Extra_Price;
    private String Type;
    private String price;
    private String subtotal;

    public String getExtra_Item() {
        return Extra_Item;
    }

    public void setExtra_Item(String extra_Item) {
        Extra_Item = extra_Item;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getActual_Price() {
        return Actual_Price;
    }

    public void setActual_Price(String actual_Price) {
        Actual_Price = actual_Price;
    }

    public String getExtra_Price() {
        return Extra_Price;
    }

    public void setExtra_Price(String extra_Price) {
        Extra_Price = extra_Price;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }
}
