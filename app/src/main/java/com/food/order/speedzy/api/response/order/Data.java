package com.food.order.speedzy.api.response.order;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Data{

	@SerializedName("listOrderStatus")
	private List<ListOrderStatusItem> listOrderStatus;

	public List<ListOrderStatusItem> getListOrderStatus(){
		return listOrderStatus;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"listOrderStatus = '" + listOrderStatus + '\'' + 
			"}";
		}
}