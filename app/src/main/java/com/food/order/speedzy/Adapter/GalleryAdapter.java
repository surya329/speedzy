package com.food.order.speedzy.Adapter;

import android.content.Context;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.food.order.speedzy.R;
import java.util.ArrayList;
import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
  private final Callback mCallback;
  private List<Uri> mData;
  Context context;

  public GalleryAdapter(
      Context context, Callback callback) {
    mCallback = callback;
    this.context = context;
    this.mData = new ArrayList<>();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery, parent, false);
    System.out.println("View Holder");
    // set the view's size, margins, paddings and layout parameters
    ViewHolder vh = new ViewHolder(v);
    return vh;
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, final int position) {
    //holder.itemImage.setImageURI(mData.get(position));
    Glide.with(context).load(mData.get(position)).into(holder.itemImage);
    holder.clear.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mCallback.onClickDeleteItem(mData.get(position));
        mData.remove(position);
        notifyDataSetChanged();
        //notifyItemRemoved(position);
        //notifyItemRangeChanged(position, mData.size());
      }
    });
  }

  @Override
  public int getItemCount() {
    return mData.size();
  }

  public void setData(List<Uri> data) {
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public void refreshData(List<Uri> data) {
    mData.clear();
    setData(data);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    ImageView itemImage;
    LinearLayout clear;

    public ViewHolder(View itemView) {
      super(itemView);
      itemImage = (ImageView) itemView.findViewById(R.id.itemImage);
      clear = (LinearLayout) itemView.findViewById(R.id.clear);
    }
  }

  public interface Callback {
    void onClickDeleteItem(Uri uri);
  }
}

