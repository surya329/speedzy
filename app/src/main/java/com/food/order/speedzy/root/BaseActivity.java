package com.food.order.speedzy.root;

import android.app.Dialog;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.ActionBar;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.food.order.speedzy.Activity.HomeActivityNew;
import com.food.order.speedzy.NetworkConnectivity.NetworkStateReceiver;
import com.food.order.speedzy.R;
import java.util.Objects;

public class BaseActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener{
  private MaterialDialog mErrorDialog;
  private MaterialDialog mExtraNullDialog;
  private boolean mInBackground;
  private boolean mViewDestroyed;
  private Dialog mProgressDialog;


  private NetworkStateReceiver networkStateReceiver;
  Dialog dialog;
  int first_time=0;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mViewDestroyed = false;

    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
    networkStateReceiver = new NetworkStateReceiver();
    networkStateReceiver.addListener(this);
    this.registerReceiver(networkStateReceiver,intentFilter);
  }
  @Override public void onResume() {
    super.onResume();
    mInBackground = false;
  }

  @Override protected void onPause() {
    super.onPause();
    mInBackground = true;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    mViewDestroyed = true;
    mProgressDialog = null;
    mErrorDialog = null;
    dialog=null;

    networkStateReceiver.removeListener(this);
    this.unregisterReceiver(networkStateReceiver);
  }

  //@Override public boolean onOptionsItemSelected(MenuItem item) {
  //  switch (item.getItemId()) {
  //    case android.R.id.home:
  //      onBackPressed();
  //      return true;
  //  }
  //  return super.onOptionsItemSelected(item);
  //}

  protected boolean isInBackground() {
    return mInBackground;
  }

  protected void showProgressDialog(String message) {
    if (mProgressDialog == null) {
      mProgressDialog = new Dialog(this);
      mProgressDialog.setContentView(R.layout.loader_layout);
      mProgressDialog.setCancelable(false);
      Objects.requireNonNull(mProgressDialog.getWindow())
          .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
    if (!isInBackground()) {
      mProgressDialog.show();
    }
  }

  protected void showProgressDialog() {
    showProgressDialog(R.string.general_label_pleasewait);
  }

  protected void showProgressDialog(@StringRes int messageResId) {
    showProgressDialog(getString(messageResId));
  }

  protected void dismissProgressDialog() {
    if (mProgressDialog != null && mProgressDialog.isShowing() && !isFinishing()) {
      mProgressDialog.dismiss();
    }
  }

  protected void showErrorDialog(String message) {
    if (mErrorDialog == null) {
      mErrorDialog = new MaterialDialog.Builder(this).title(R.string.general_label_error)
          .content(message)
          .positiveText(R.string.general_label_ok)
          .build();
    } else {
      mErrorDialog.setContent(message);
    }
    if (!isInBackground()) {
      mErrorDialog.show();
    }
  }

  protected void showErrorDialog(@StringRes int messageResId) {
    showErrorDialog(getString(messageResId));
  }

  protected void dismissErrorDialog() {
    if (mErrorDialog != null && mErrorDialog.isShowing() && !isFinishing()) {
      mErrorDialog.dismiss();
    }
  }

  protected void setBackButtonEnabled(boolean enabled) {
    final ActionBar actionBar = getSupportActionBar();
    //Assert.assertNotNull(actionBar);
    actionBar.setDisplayHomeAsUpEnabled(enabled);
    actionBar.setHomeButtonEnabled(enabled);
  }

  protected void setTitleVisible(boolean visible) {
    final ActionBar actionBar = getSupportActionBar();
    //Assert.assertNotNull(actionBar);
    actionBar.setDisplayShowTitleEnabled(visible);
  }

  //protected void setFragment(Fragment fragment, String tag) {
  //  setFragment(R.id.container_fragment, fragment, tag);
  //}
  //
  //protected void setFragment(Fragment fragment, String tag, boolean addToBackStack) {
  //  setFragment(R.id.container_fragment, fragment, tag, addToBackStack);
  //}
  //
  //protected void setFragment(int container, Fragment fragment, String tag) {
  //  setFragment(container, fragment, tag, true);
  //}

  //protected void setFragment(int container, Fragment fragment, String tag, boolean addToBackStack) {
  //  if (!ObjectUtils.equals(tag, getLastBackStackTag())) {
  //    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
  //    fragmentTransaction.replace(container, fragment, tag);
  //    if (addToBackStack) fragmentTransaction.addToBackStack(tag);
  //
  //    fragmentTransaction.commit();
  //  }
  //}

  //protected ApplicationComponent getInjection() {
  //  return ((App) getApplication()).getApplicationComponent();
  //}

  protected boolean isViewDestroyed() {
    return mViewDestroyed;
  }

  private String getLastBackStackTag() {
    final FragmentManager manager = getSupportFragmentManager();
    final int count = manager.getBackStackEntryCount();
    return count > 0 ? manager.getBackStackEntryAt(count - 1).getName() : "";
  }

  protected Fragment findFragmentByTag(String tag) {
    return getSupportFragmentManager().findFragmentByTag(tag);
  }

  protected void showExtraNullErrorDialog() {
    if (mExtraNullDialog == null) {
      mExtraNullDialog = new MaterialDialog.Builder(this).title(R.string.general_label_error)
          .content(R.string.general_error_message)
          .positiveText(R.string.general_label_ok)
          .cancelable(false)
          .onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
              onPositiveExtraNullDialog();
            }
          })
          .build();
    }

    mExtraNullDialog.show();
  }

  protected void onPositiveExtraNullDialog() {
    //override this if you want to do additional things
    finish();
  }

  protected void initToolbar(String title) {
    if (getSupportActionBar() != null) {
      getSupportActionBar().setTitle(title);
      //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
      //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      //getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
  }

  @Override public boolean onSupportNavigateUp() {
    onBackPressed();
    return true;
  }

  @Override
  public void networkAvailable() {
    if (dialog != null && dialog.isShowing() && !isFinishing()) {
      dialog.dismiss();
    }
    this.onResume();
  }

  @Override
  public void networkUnavailable() {
    popupshow();
  }

  private void popupshow() {
   /* dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setCancelable(false);
    dialog.setContentView(R.layout.connection_popup);
    dialog.show();*/

    if (dialog == null) {
      dialog = new Dialog(this);
      dialog.setContentView(R.layout.connection_popup);
      dialog.setCancelable(false);
      Objects.requireNonNull(dialog.getWindow())
              .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
    if (!isInBackground()) {
      dialog.show();
    }

  }
}
