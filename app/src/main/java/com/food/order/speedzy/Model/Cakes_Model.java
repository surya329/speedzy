package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Cakes_Model implements Serializable {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("app_openstatus")
    @Expose
    public String app_openstatus;
    @SerializedName("open_close_status")
    @Expose
    public open_close_status open_close_status;

    public String getApp_openstatus() {
        return app_openstatus;
    }

    public void setApp_openstatus(String app_openstatus) {
        this.app_openstatus = app_openstatus;
    }

    public Cakes_Model.open_close_status getOpen_close_status() {
        return open_close_status;
    }

    public void setOpen_close_status(Cakes_Model.open_close_status open_close_status) {
        this.open_close_status = open_close_status;
    }

    public Cakes_Model.Restaurant_timings getRestaurant_timings() {
        return Restaurant_timings;
    }

    public void setRestaurant_timings(Cakes_Model.Restaurant_timings restaurant_timings) {
        Restaurant_timings = restaurant_timings;
    }

    @SerializedName("Restaurant_timings")
    @Expose
    public Restaurant_timings Restaurant_timings;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Cake> getCakes() {
        return cakes;
    }

    public void setCakes(List<Cake> cakes) {
        this.cakes = cakes;
    }

    @SerializedName("Cakes")
    @Expose
    public List<Cake> cakes = null;
    public class Cake implements Serializable {
        public String getIn_restaurant_id() {
            return in_restaurant_id;
        }

        public void setIn_restaurant_id(String in_restaurant_id) {
            this.in_restaurant_id = in_restaurant_id;
        }

        @SerializedName("in_restaurant_id")
        @Expose
        public String in_restaurant_id;
        @SerializedName("in_dish_id")
        @Expose
        public String inDishId;
        @SerializedName("st_dish_name")
        @Expose
        public String stDishName;
        @SerializedName("dish_desription")
        @Expose
        public String dishDesription;

        public String getInDishId() {
            return inDishId;
        }

        public void setInDishId(String inDishId) {
            this.inDishId = inDishId;
        }

        public String getStDishName() {
            return stDishName;
        }

        public void setStDishName(String stDishName) {
            this.stDishName = stDishName;
        }

        public String getDishDesription() {
            return dishDesription;
        }

        public void setDishDesription(String dishDesription) {
            this.dishDesription = dishDesription;
        }

        public List<StPrice> getStPrice() {
            return stPrice;
        }

        public void setStPrice(List<StPrice> stPrice) {
            this.stPrice = stPrice;
        }

        public String getFlgAddChoices() {
            return flgAddChoices;
        }

        public void setFlgAddChoices(String flgAddChoices) {
            this.flgAddChoices = flgAddChoices;
        }

        public Object getDishCuisineId() {
            return dishCuisineId;
        }

        public void setDishCuisineId(Object dishCuisineId) {
            this.dishCuisineId = dishCuisineId;
        }

        public String getEggStatus() {
            return eggStatus;
        }

        public void setEggStatus(String eggStatus) {
            this.eggStatus = eggStatus;
        }

        public String getNonVegStatus() {
            return nonVegStatus;
        }

        public void setNonVegStatus(String nonVegStatus) {
            this.nonVegStatus = nonVegStatus;
        }

        public String getOutOfStock() {
            return outOfStock;
        }

        public void setOutOfStock(String outOfStock) {
            this.outOfStock = outOfStock;
        }

        public String getCakeFlg() {
            return cakeFlg;
        }

        public void setCakeFlg(String cakeFlg) {
            this.cakeFlg = cakeFlg;
        }

        public String getImagename() {
            return imagename;
        }

        public void setImagename(String imagename) {
            this.imagename = imagename;
        }

        @SerializedName("st_price")
        @Expose
        public List<StPrice> stPrice = null;
        @SerializedName("flg_add_choices")
        @Expose
        public String flgAddChoices;
        @SerializedName("dish_cuisine_id")
        @Expose
        public Object dishCuisineId;
        @SerializedName("egg_status")
        @Expose
        public String eggStatus;
        @SerializedName("non_veg_status")
        @Expose
        public String nonVegStatus;
        @SerializedName("out_of_stock")
        @Expose
        public String outOfStock;
        @SerializedName("cake_flg")
        @Expose
        public String cakeFlg;
        @SerializedName("imagename")
        @Expose
        public String imagename;
    }
    public class open_close_status implements Serializable{
        @SerializedName("message")
        @Expose
        public String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getOpenstatus() {
            return openstatus;
        }

        public void setOpenstatus(String openstatus) {
            this.openstatus = openstatus;
        }

        @SerializedName("openstatus")
        @Expose
        public String openstatus;
    }
    public class Restaurant_timings implements Serializable{
        @SerializedName("bakery_start")
        @Expose
        public String bakery_start;

        public String getBakery_start() {
            return bakery_start;
        }

        public void setBakery_start(String bakery_start) {
            this.bakery_start = bakery_start;
        }

        public String getBakery_end() {
            return bakery_end;
        }

        public void setBakery_end(String bakery_end) {
            this.bakery_end = bakery_end;
        }

        @SerializedName("bakery_end")
        @Expose
        public String bakery_end;
    }
}
