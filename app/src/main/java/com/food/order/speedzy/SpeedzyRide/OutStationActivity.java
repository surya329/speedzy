package com.food.order.speedzy.SpeedzyRide;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.Activity.Resturant_SearchActivity;
import com.food.order.speedzy.Adapter.LikedResturantsActivityAdapter1;
import com.food.order.speedzy.Fragment.DateTimePicker;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;

import java.util.ArrayList;
import java.util.List;

public class OutStationActivity extends AppCompatActivity implements DateTimePicker.TheListener {

    MySharedPrefrencesData mySharedPrefrencesData;
    TextView title;
    Toolbar toolbar;
    String Veg_Flag;
    RecyclerView recyclerView;
    LinearLayoutManager lm;
    CarAdapter carAdapter;
    List<Car_model> car_modelList = new ArrayList<>();
    LoaderDiloag loaderDiloag;
    String pickup_location,drop_location,leave_on;
    TextView leave_on_txt,drop,pickup,oneway,round,return_by_txt;
    RelativeLayout relative_return_by;
    int trip_type=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_station);
        
        pickup_location=getIntent().getStringExtra("pickup_location");
        drop_location=getIntent().getStringExtra("drop_location");
        leave_on=getIntent().getStringExtra("leave_on");

        mySharedPrefrencesData=new MySharedPrefrencesData();
        Veg_Flag = mySharedPrefrencesData.getVeg_Flag(OutStationActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title= findViewById(R.id.title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Veg_Flag.equalsIgnoreCase("1")) {
            statusbar_bg(R.color.red);
        } else {
            statusbar_bg(R.color.red);
        }
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_car);
        loaderDiloag = new LoaderDiloag(this);
        pickup= findViewById(R.id.pickup);
        drop= findViewById(R.id.drop);
        leave_on_txt= findViewById(R.id.leave_on_txt);
        return_by_txt=findViewById(R.id.return_by_txt);
        relative_return_by= findViewById(R.id.relative_return_by);
        oneway= findViewById(R.id.oneway);
        round=findViewById(R.id.round);
        
       pickup.setText(pickup_location);
       drop.setText(drop_location);
       leave_on_txt.setText(leave_on);
       oneway_click();
       oneway.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               trip_type=1;
               oneway_click();
           }
       });
       round.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trip_type=2;
                round_click();
            }
        });
       return_by_txt.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               dialog_show(3);
           }
       });
        leave_on_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_show(2);
            }
        });
        carlist();
        lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, 0));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        carAdapter = new CarAdapter(OutStationActivity.this,car_modelList);
        recyclerView.setAdapter(carAdapter);
        carAdapter.setOnItemClickListener(new CarAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Car_model car_model) {
                if (trip_type==2){
                    if (!return_by_txt.getText().toString().equalsIgnoreCase("SELECT")){
                        price_detail_intent(car_model);
                    }else {
                        Toast.makeText(getApplicationContext(),"Please Select return date and time",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    price_detail_intent(car_model);
                }
            }
        });
    }

    private void price_detail_intent(Car_model car_model) {
        Intent data = new Intent(OutStationActivity.this, OutStationPriceDetailsActivity.class);
        data.putExtra("pickup_location",pickup_location);
        data.putExtra("drop_location",drop_location);
        data.putExtra("leave_on",leave_on_txt.getText().toString());
        data.putExtra("return_by",return_by_txt.getText().toString());
        data.putExtra("car_model",car_model);
        startActivity(data);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    private void dialog_show(int flag) {
        DialogFragment newFragment = new DateTimePicker(flag,leave_on_txt.getText().toString(),return_by_txt.getText().toString());
        newFragment.show(getSupportFragmentManager(), "DatePicker");
    }

    private void oneway_click() {
        oneway.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_checked, 0, 0, 0);
        round.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_unchecked, 0, 0, 0);
        relative_return_by.setVisibility(View.GONE);
    }
    private void round_click() {
        round.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_checked, 0, 0, 0);
        oneway.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_unchecked, 0, 0, 0);
        relative_return_by.setVisibility(View.VISIBLE);
    }

    private void carlist() {
        Car_model car_model=new Car_model(R.drawable.mini,"Mini","WagonR, Indica, Ritz","Affordable AC cabs with free Wi-Fi","1277");
        car_modelList.add(car_model);
        car_model=new Car_model(R.drawable.micro,"Micro","Dzire, Etios, Xcent","Comfortable with extra legroom","1439");
        car_modelList.add(car_model);
        car_model=new Car_model(R.drawable.outstation,"Prime","WagonR, Indica, Ritz","Affordable AC cabs with free Wi-Fi","1277");
        car_modelList.add(car_model);
    }

    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(this);
    }

    @Override
    public void returnDate(int flag,String date, String time) {
        if (flag==2) {
            leave_on_txt.setText(date + "," + time);
        }else  if (flag==3) {
            return_by_txt.setText(date + "," + time);
        }
    }
}
