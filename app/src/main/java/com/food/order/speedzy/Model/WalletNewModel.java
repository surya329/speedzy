package com.food.order.speedzy.Model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class WalletNewModel {
  //@SerializedName("wallet")
  //private String wallet;

  @SerializedName("transactions")
  private List<WalletModel> transactions;

  //@SerializedName("status")
  //private String status;
  //private String status;
  //private String wallet;
  //private List<WalletModel> transactions;

  //public String getStatus() {
  //  return status;
  //}
  //
  //public String getWallet() {
  //  return wallet;
  //}

  public List<WalletModel> getTransactions() {
    return transactions;
  }
}
