package com.food.order.speedzy.api.response.home;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class TypeDetails{

	@SerializedName("id")
	private String id;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"TypeDetails{" + 
			"id = '" + id + '\'' + 
			"}";
		}
}