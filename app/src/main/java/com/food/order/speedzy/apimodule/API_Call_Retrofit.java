package com.food.order.speedzy.apimodule;

import android.content.Context;
import com.food.order.speedzy.BuildConfig;
import com.food.order.speedzy.Model.LoginUserResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by Sujata Mohanty.
 */

public class API_Call_Retrofit {
  public static String apiBaseUrl = "";
  static Gson gson = new GsonBuilder()
      .registerTypeAdapter(LoginUserResponse.class, new LoginUserResponse.DataStateDeserializer())
      .setLenient()
      .create();

  static String token = "";
  static OkHttpClient client;

  public static Retrofit changeApiBaseUrl(final Context ctx) {
    apiBaseUrl = "http://35.244.8.156/speedzy/";

    Interceptor interceptor = new Interceptor() {
      @Override
      public Response intercept(Chain chain) throws IOException {
        Request newRequest = chain.request().newBuilder().build();
        Response response = chain.proceed(chain.request());
        return chain.proceed(newRequest);
      }
    };
    OkHttpClient.Builder builder = new OkHttpClient.Builder();

    if (BuildConfig.DEBUG) {
      HttpLoggingInterceptor httpLogging = new HttpLoggingInterceptor();
      httpLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
      builder.addInterceptor(httpLogging);
      HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor((messages) ->
          Timber.i(messages));
      httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
    }

    client = builder.readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .build();

    builder.interceptors().add(interceptor);

    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(apiBaseUrl)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(client)
        .build();
    return retrofit;
  }

  public static void stopAllCalls() {
    client.dispatcher().cancelAll();
  }
}



