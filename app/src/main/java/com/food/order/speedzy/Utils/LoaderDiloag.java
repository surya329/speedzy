package com.food.order.speedzy.Utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.food.order.speedzy.R;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Circle;
import com.github.ybq.android.spinkit.style.DoubleBounce;

import java.util.Objects;

import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;

/**
 * Created by Sujata Mohanty.
 */

public class LoaderDiloag {
  Dialog ratingDialog;

  public LoaderDiloag(final Context mContext) {
    ratingDialog = new Dialog(mContext);
    ratingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    ratingDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    ratingDialog.setContentView(R.layout.loader_layout);
    ratingDialog.setCancelable(false);
    /*ProgressBar progressBar = (ProgressBar)ratingDialog.findViewById(R.id.spin_kit);
    Sprite doubleBounce = new Circle();
    progressBar.setIndeterminateDrawable(doubleBounce);*/
    ImageView imageView = ratingDialog.findViewById(R.id.gifImage);
    Glide.with(getApplicationContext())
            .asGif()
            .load(R.drawable.loader)
            .into(imageView);
  }

  public void dismissDiloag() {
    try {
      if (ratingDialog != null && ratingDialog.isShowing()) {
        ratingDialog.dismiss();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void displayDiloag() {
    if (ratingDialog != null) {
      ratingDialog.show();
    }
  }
}
