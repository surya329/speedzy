package com.food.order.speedzy.Model;

import java.util.ArrayList;

/**
 * Created by Sujata Mohanty.
 */


public class RestaurantsListModelNew {


    private String status;
    private ArrayList<Restaurant_model> Restaurants = null;
    private ArrayList<String> filter_cuisine = null;
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public ArrayList<Restaurant_model> getRestaurants() {
        return Restaurants;
    }

    public void setRestaurants(ArrayList<Restaurant_model> restaurants) {
        this.Restaurants = restaurants;
    }


    public ArrayList<String> getFilter_cuisine() {
        return filter_cuisine;
    }

    public void setFilter_cuisine(ArrayList<String> filter_cuisine) {
        this.filter_cuisine = filter_cuisine;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}

