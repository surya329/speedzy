package com.food.order.speedzy.screen.orderupi;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderUPIDataModel implements Parcelable {

  private String orderNumber;
  private String price;

  public OrderUPIDataModel() {
  }

  protected OrderUPIDataModel(Parcel in) {
    orderNumber = in.readString();
    price = in.readString();
  }

  public static final Creator<OrderUPIDataModel> CREATOR = new Creator<OrderUPIDataModel>() {
    @Override
    public OrderUPIDataModel createFromParcel(Parcel in) {
      return new OrderUPIDataModel(in);
    }

    @Override
    public OrderUPIDataModel[] newArray(int size) {
      return new OrderUPIDataModel[size];
    }
  };

  public String getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(orderNumber);
    parcel.writeString(price);
  }
}
