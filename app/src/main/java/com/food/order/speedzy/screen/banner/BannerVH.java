package com.food.order.speedzy.screen.banner;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;


import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.GeneralUtil;

public class BannerVH extends RecyclerView.ViewHolder {
  private final Callback mCallback;
  private final Context mContext;
  public static int sCorner = 30;
  public static int sMargin = 8;
  @BindView(R.id.itemImage) ImageView imgBanner;
  private final String base_url_thumb;
  private final String base_url_main;

  public BannerVH(@NonNull View itemView,
      Callback callback) {
    super(itemView);
    mCallback = callback;
    mContext = itemView.getContext();
    ButterKnife.bind(this, itemView);
  /*  ViewGroup.LayoutParams params1 = imgBanner.getLayoutParams();
    params1.width = mContext.getResources().getDimensionPixelSize(R.dimen._215sdp);
    params1.height = mContext.getResources().getDimensionPixelSize(R.dimen._150sdp);
    params1.height = mContext.getResources().getDimensionPixelSize(R.dimen._150sdp);*/
    base_url_thumb = "https://storage.googleapis.com/speedzy_data/resources/banner/small/";
    base_url_main = "https://storage.googleapis.com/speedzy_data/resources/banner/big/";
    imgBanner.requestLayout();
  }

  public void onBindView(BannersVM bannersItem) {
    if (!GeneralUtil.isStringEmpty(bannersItem.getImageName())) {
      RequestBuilder<Drawable> thumbnailRequest = Glide
              .with(mContext.getApplicationContext())
              .load(base_url_thumb + bannersItem.getImageName())
              .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder));
             //.override(pixels_height, pixels_width);

      Glide
              .with(mContext.getApplicationContext())
              .load(base_url_main + bannersItem.getImageName())
              // .override(pixels_height, pixels_width)
              .thumbnail(thumbnailRequest)
              .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder))
              .into(imgBanner);
    }
  }

  public interface Callback {
    void onClickItem(int position);
  }
}
