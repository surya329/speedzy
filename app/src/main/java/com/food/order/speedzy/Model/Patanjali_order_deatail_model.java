package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 14-12-2018.
 */

public class Patanjali_order_deatail_model implements Serializable {
  @SerializedName("status")
  @Expose
  public String status;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public List<Grocery> getGrocery() {
    return grocery;
  }

  public void setGrocery(List<Grocery> grocery) {
    this.grocery = grocery;
  }

  public String getMesage() {
    return mesage;
  }

  public void setMesage(String mesage) {
    this.mesage = mesage;
  }

  @SerializedName("grocery")
  @Expose
  public List<Grocery> grocery = null;
  @SerializedName("mesage")
  @Expose
  public String mesage;

  public class Grocery implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getOrderNumber() {
      return orderNumber;
    }

    public String getOrderName() {
      return orderName;
    }

    public void setOrderName(String orderName) {
      this.orderName = orderName;
    }

    public String getOrderImage() {
      return orderImage;
    }

    public void setOrderImage(String orderImage) {
      this.orderImage = orderImage;
    }

    public void setOrderNumber(String orderNumber) {
      this.orderNumber = orderNumber;
    }

    public String getTotalMrp() {
      return totalMrp;
    }

    public void setTotalMrp(String totalMrp) {
      this.totalMrp = totalMrp;
    }

    public String getTotalSalesprice() {
      return totalSalesprice;
    }

    public void setTotalSalesprice(String totalSalesprice) {
      this.totalSalesprice = totalSalesprice;
    }

    public String getAdditionalDiscount() {
      return additionalDiscount;
    }

    public void setAdditionalDiscount(String additionalDiscount) {
      this.additionalDiscount = additionalDiscount;
    }

    public String getCouponamount() {
      return couponamount;
    }

    public void setCouponamount(String couponamount) {
      this.couponamount = couponamount;
    }

    public String getCouponcode() {
      return couponcode;
    }

    public void setCouponcode(String couponcode) {
      this.couponcode = couponcode;
    }

    public String getGrandTotal() {
      return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
      this.grandTotal = grandTotal;
    }

    public String getUserID() {
      return userID;
    }

    public void setUserID(String userID) {
      this.userID = userID;
    }

    public String getWalletAmount() {
      return walletAmount;
    }

    public void setWalletAmount(String walletAmount) {
      this.walletAmount = walletAmount;
    }

    public String getShippingCharge() {
      return shippingCharge;
    }

    public void setShippingCharge(String shippingCharge) {
      this.shippingCharge = shippingCharge;
    }

    public String getOrderAmount() {
      return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
      this.orderAmount = orderAmount;
    }

    public String getPaymentStatus() {
      return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
      this.paymentStatus = paymentStatus;
    }

    public String getOrderStatus() {
      return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
      this.orderStatus = orderStatus;
    }

    public String getShippingStatus() {
      return shippingStatus;
    }

    public void setShippingStatus(String shippingStatus) {
      this.shippingStatus = shippingStatus;
    }

    public String getCreated() {
      return created;
    }

    public void setCreated(String created) {
      this.created = created;
    }

    public ShippingAddress getShippingAddress() {
      return shippingAddress;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
      this.shippingAddress = shippingAddress;
    }

    public List<ProductDetail> getProductDetails() {
      return productDetails;
    }

    public void setProductDetails(List<ProductDetail> productDetails) {
      this.productDetails = productDetails;
    }

    @SerializedName("ordername")
    @Expose
    private String orderName;

    @SerializedName("order_image")
    @Expose
    private String orderImage;

    @SerializedName("order_number")
    @Expose
    public String orderNumber;
    @SerializedName("total_mrp")
    @Expose
    public String totalMrp;
    @SerializedName("total_salesprice")
    @Expose
    public String totalSalesprice;
    @SerializedName("additional_discount")
    @Expose
    public String additionalDiscount;
    @SerializedName("couponamount")
    @Expose
    public String couponamount;
    @SerializedName("couponcode")
    @Expose
    public String couponcode;
    @SerializedName("grand_total")
    @Expose
    public String grandTotal;
    @SerializedName("userID")
    @Expose
    public String userID;
    @SerializedName("wallet_amount")
    @Expose
    public String walletAmount;
    @SerializedName("shipping_charge")
    @Expose
    public String shippingCharge;
    @SerializedName("order_amount")
    @Expose
    public String orderAmount;
    @SerializedName("payment_status")
    @Expose
    public String paymentStatus;
    @SerializedName("order_status")
    @Expose
    public String orderStatus;
    @SerializedName("shipping_status")
    @Expose
    public String shippingStatus;
    @SerializedName("created")
    @Expose
    public String created;
    @SerializedName("shipping_address")
    @Expose
    public ShippingAddress shippingAddress;
    @SerializedName("product_details")
    @Expose
    public List<ProductDetail> productDetails = null;

    public class ShippingAddress implements Serializable {
      @SerializedName("firstname")
      @Expose
      public String firstname;

      public String getFirstname() {
        return firstname;
      }

      public void setFirstname(String firstname) {
        this.firstname = firstname;
      }

      public String getLastname() {
        return lastname;
      }

      public void setLastname(String lastname) {
        this.lastname = lastname;
      }

      public String getEmail() {
        return email;
      }

      public void setEmail(String email) {
        this.email = email;
      }

      public String getPhone() {
        return phone;
      }

      public void setPhone(String phone) {
        this.phone = phone;
      }

      public String getAddress1() {
        return address1;
      }

      public void setAddress1(String address1) {
        this.address1 = address1;
      }

      public String getLandmark() {
        return landmark;
      }

      public void setLandmark(String landmark) {
        this.landmark = landmark;
      }

      public String getComment() {
        return comment;
      }

      public void setComment(String comment) {
        this.comment = comment;
      }

      public String getLat() {
        return lat;
      }

      public void setLat(String lat) {
        this.lat = lat;
      }

      public String get_long() {
        return _long;
      }

      public void set_long(String _long) {
        this._long = _long;
      }

      @SerializedName("lastname")
      @Expose
      public String lastname;
      @SerializedName("email")
      @Expose
      public String email;
      @SerializedName("phone")
      @Expose
      public String phone;
      @SerializedName("address1")
      @Expose
      public String address1;
      @SerializedName("landmark")
      @Expose
      public String landmark;
      @SerializedName("comment")
      @Expose
      public String comment;
      @SerializedName("lat")
      @Expose
      public String lat;
      @SerializedName("long")
      @Expose
      public String _long;
    }

    public class ProductDetail implements Serializable {
      public String getQty() {
        return qty;
      }

      public void setQty(String qty) {
        this.qty = qty;
      }

      public Data getData() {
        return data;
      }

      public void setData(Data data) {
        this.data = data;
      }

      @SerializedName("qty")
      @Expose
      public String qty;
      @SerializedName("data")
      @Expose
      public Data data;

      public class Data implements Serializable {
        @SerializedName("ProductID")
        @Expose
        public String productID;
        @SerializedName("title")
        @Expose
        public String title;

        public String getProductID() {
          return productID;
        }

        public void setProductID(String productID) {
          this.productID = productID;
        }

        public String getTitle() {
          return title;
        }

        public void setTitle(String title) {
          this.title = title;
        }

        public String getDescription() {
          return description;
        }

        public void setDescription(String description) {
          this.description = description;
        }

        public String getProductMrp() {
          return productMrp;
        }

        public void setProductMrp(String productMrp) {
          this.productMrp = productMrp;
        }

        public String getSalePrice() {
          return salePrice;
        }

        public void setSalePrice(String salePrice) {
          this.salePrice = salePrice;
        }

        public String getPurchasePrice() {
          return purchasePrice;
        }

        public void setPurchasePrice(String purchasePrice) {
          this.purchasePrice = purchasePrice;
        }

        public String getBrand() {
          return brand;
        }

        public void setBrand(String brand) {
          this.brand = brand;
        }

        public String getCurrentStock() {
          return currentStock;
        }

        public void setCurrentStock(String currentStock) {
          this.currentStock = currentStock;
        }

        public String getDiscount() {
          return discount;
        }

        public void setDiscount(String discount) {
          this.discount = discount;
        }

        public String getDiscountType() {
          return discountType;
        }

        public void setDiscountType(String discountType) {
          this.discountType = discountType;
        }

        public String getUnit() {
          return unit;
        }

        public void setUnit(String unit) {
          this.unit = unit;
        }

        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("product_mrp")
        @Expose
        public String productMrp;
        @SerializedName("sale_price")
        @Expose
        public String salePrice;
        @SerializedName("purchase_price")
        @Expose
        public String purchasePrice;
        @SerializedName("brand")
        @Expose
        public String brand;
        @SerializedName("current_stock")
        @Expose
        public String currentStock;
        @SerializedName("discount")
        @Expose
        public String discount;
        @SerializedName("discount_type")
        @Expose
        public String discountType;
        @SerializedName("unit")
        @Expose
        public String unit;
      }
    }


  }
}
