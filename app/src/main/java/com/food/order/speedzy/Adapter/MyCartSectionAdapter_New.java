package com.food.order.speedzy.Adapter;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.food.order.speedzy.Activity.CheckoutActivity_New;
import com.food.order.speedzy.Activity.MealCombo_Checkout_Activity;
import com.food.order.speedzy.Model.Cart_Model;
import com.food.order.speedzy.Model.Preferencemodel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.AllValidation;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GravityCompoundDrawable;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.database.LOcaldbNew;
import java.util.ArrayList;

/**
 * Created by Sujata Mohanty.
 */

public class MyCartSectionAdapter_New
    extends SectionedRecyclerViewAdapter<MyCartSectionAdapter_New.ViewHolder> {
  ArrayList<Cart_Model.Cart_Details> cart_detailsList;
  LOcaldbNew lOcaldbNew;
  CheckoutActivity_New context;
  MealCombo_Checkout_Activity mealCombo_checkout_activity;
  int flag=0;
  String inDishId;
  public MyCartSectionAdapter_New(CheckoutActivity_New context,
      ArrayList<Cart_Model.Cart_Details> cart_detailsList) {
    this.cart_detailsList = cart_detailsList;
    this.context = context;
  }

  public MyCartSectionAdapter_New(int flag, MealCombo_Checkout_Activity mealCombo_checkout_activity, ArrayList<Cart_Model.Cart_Details> cart_detailsList, String inDishId) {
    this.cart_detailsList = cart_detailsList;
    this.flag=flag;
    this.inDishId=inDishId;
    this.mealCombo_checkout_activity = mealCombo_checkout_activity;
  }

  @Override
  public int getSectionCount() {
    return cart_detailsList.size();
  }

  @Override
  public int getItemCount(int section) {
    int count = 0;
    if (Commons.flag_for_hta.equalsIgnoreCase("Patanjali") || Commons.flag_for_hta.equalsIgnoreCase(
        "City Special")
        || Commons.flag_for_hta == "Drinking Water") {

    } else {
      ArrayList<ArrayList<Preferencemodel>> myarray =
          cart_detailsList.get(section).getPreferencelfromdb();
      for (int i = 1; i < myarray.size(); i++) {
        ArrayList<Preferencemodel> mychildarray =
            cart_detailsList.get(section).getPreferencelfromdb().get(i);
        count = count + mychildarray.size();
      }
    }
    return count;
  }

  //
  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  @Override
  public void onBindHeaderViewHolder(final MyCartSectionAdapter_New.ViewHolder holder,
      final int section) {
    final Cart_Model.Cart_Details cart_details = cart_detailsList.get(section);
    if (flag==1 && inDishId.equalsIgnoreCase(cart_details.getIn_dish_id())){
      holder.deletelay.setVisibility(View.GONE);
      holder.deletelay.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
    }else {
      holder.itemname.setText(cart_detailsList.get(section).getSt_dish_name());
      String qnty_no = cart_detailsList.get(section).getQuantity();
      holder.qty.setText("" + qnty_no);
      double pricefloat = Double.parseDouble(cart_details.getMenu_price());
      Double totalprice = pricefloat * (Integer.parseInt(qnty_no));
      holder.price.setText("₹ " + String.format("%.2f", totalprice));
      holder.add.setVisibility(View.VISIBLE);
    }

    if (Commons.flag_for_hta.equalsIgnoreCase("Patanjali") || Commons.flag_for_hta.equalsIgnoreCase(
        "City Special")
        || Commons.flag_for_hta.equalsIgnoreCase("Drinking Water")) {
      veg_nonveg_iconset(context.getDrawable(R.drawable.ic_veg_item_indicator), holder);
    } else {
      if (flag==0) {
        if (cart_detailsList.get(section).getNon_veg_status().equalsIgnoreCase("1")) {
          veg_nonveg_iconset(context.getDrawable(R.drawable.ic_nonveg_item_indicator), holder);
          //holder.itemname.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_nonveg_item_indicator, 0, 0, 0);
        } else {
          veg_nonveg_iconset(context.getDrawable(R.drawable.ic_veg_item_indicator), holder);
          //holder.itemname.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_veg_item_indicator, 0, 0, 0);
        }
      }else {
        if (inDishId.equalsIgnoreCase(cart_details.getIn_dish_id())){
          holder.deletelay.setVisibility(View.GONE);
          holder.deletelay.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
        }else {
          if (cart_detailsList.get(section).getNon_veg_status().equalsIgnoreCase("1")) {
            veg_nonveg_iconset(mealCombo_checkout_activity.getDrawable(R.drawable.ic_nonveg_item_indicator), holder);
            //holder.itemname.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_nonveg_item_indicator, 0, 0, 0);
          } else {
            veg_nonveg_iconset(mealCombo_checkout_activity.getDrawable(R.drawable.ic_veg_item_indicator), holder);
            //holder.itemname.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_veg_item_indicator, 0, 0, 0);
          }
        }
      }
    }

    holder.add.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        int qty = Integer.parseInt(cart_detailsList.get(section).getQuantity());
        if (cart_detailsList.get(section).getMax_qty_peruser()!=null) {
          if (qty < Integer.parseInt(cart_detailsList.get(section).getMax_qty_peruser())) {
            qty++;
            cart_detailsList.get(section).setQuantity(String.valueOf(qty));
            holder.sub.setVisibility(View.VISIBLE);
            if (Commons.flag_for_hta.equalsIgnoreCase("Patanjali")) {
              lOcaldbNew.updateCart_Ptanjali(String.valueOf(qty),
                      cart_detailsList.get(section).getIn_dish_id());
              notifyDataSetChanged();
              context.setPriceDetailsPatanjali(cart_detailsList);
            } else if (Commons.flag_for_hta.equalsIgnoreCase("City Special")) {
              lOcaldbNew.updateCart_CitySpecial(String.valueOf(qty),
                      cart_detailsList.get(section).getIn_dish_id());
              notifyDataSetChanged();
              context.setPriceDetailsPatanjali(cart_detailsList);
            } else if (Commons.flag_for_hta.equalsIgnoreCase("Drinking Water")) {
              lOcaldbNew.updateCart_Water(String.valueOf(qty),
                      cart_detailsList.get(section).getIn_dish_id());
              notifyDataSetChanged();
              context.setPriceDetailsPatanjali(cart_detailsList);
            } else if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)) {
              lOcaldbNew.updateFruitCart(String.valueOf(qty),
                      cart_detailsList.get(section).getIn_dish_id());
              notifyDataSetChanged();
              context.setPriceDetails1(cart_detailsList);
            } else {
              lOcaldbNew.updateCart(String.valueOf(qty), cart_detailsList.get(section).getIn_dish_id());
              notifyDataSetChanged();
              if (flag == 0) {
                context.setPriceDetails1(cart_detailsList);
              } else {
                mealCombo_checkout_activity.setPriceDetails(cart_detailsList);
              }
            }
          } else {
            Toast.makeText(context.getApplicationContext(), "Maximum " + cart_detailsList.get(section).getMax_qty_peruser() + " Items Added",
                    Toast.LENGTH_SHORT).show();
          }
        }
      }
    });

    holder.sub.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int qty = Integer.parseInt(cart_detailsList.get(section).getQuantity());
        Log.e("id",Commons.flag_for_hta);
        if (qty > 1) {
          holder.sub.setVisibility(View.VISIBLE);
          qty--;
          cart_detailsList.get(section).setQuantity(String.valueOf(qty));
          if (Commons.flag_for_hta.equalsIgnoreCase("Patanjali")) {
            lOcaldbNew.updateCart_Ptanjali(String.valueOf(qty),
                cart_detailsList.get(section).getIn_dish_id());
            notifyDataSetChanged();
            context.setPriceDetailsPatanjali(cart_detailsList);
          } else if (Commons.flag_for_hta.equalsIgnoreCase("City Special")) {
            lOcaldbNew.updateCart_CitySpecial(String.valueOf(qty),
                cart_detailsList.get(section).getIn_dish_id());
            notifyDataSetChanged();
            context.setPriceDetailsPatanjali(cart_detailsList);
          } else if (Commons.flag_for_hta.equalsIgnoreCase("Drinking Water")) {
            lOcaldbNew.updateCart_Water(String.valueOf(qty),
                cart_detailsList.get(section).getIn_dish_id());
            notifyDataSetChanged();
            context.setPriceDetailsPatanjali(cart_detailsList);
          } else if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)) {
            lOcaldbNew.updateFruitCart(String.valueOf(qty),
                cart_detailsList.get(section).getIn_dish_id());
            notifyDataSetChanged();
            context.setPriceDetails1(cart_detailsList);
          } else {
            lOcaldbNew.updateCart(String.valueOf(qty),
                cart_detailsList.get(section).getIn_dish_id());
            notifyDataSetChanged();
            if (flag==0) {
              context.setPriceDetails1(cart_detailsList);
            }else {
              mealCombo_checkout_activity.setPriceDetails(cart_detailsList);
            }
          }
        } else {
          if (Commons.flag_for_hta.equalsIgnoreCase("Patanjali")) {
            lOcaldbNew.deletecart_Ptanjali(cart_detailsList.get(section).getIn_dish_id());
            AllValidation.myToast(context, "dish is successfully deleted from cart ");
            cart_detailsList.remove(section);
            notifyDataSetChanged();
            context.setPriceDetailsPatanjali(cart_detailsList);
          } else if (Commons.flag_for_hta.equalsIgnoreCase("City Special")) {
            lOcaldbNew.deletecart_CitySpecial(cart_detailsList.get(section).getIn_dish_id());
            AllValidation.myToast(context, "dish is successfully deleted from cart ");
            cart_detailsList.remove(section);
            notifyDataSetChanged();
            context.setPriceDetailsPatanjali(cart_detailsList);
          } else if (Commons.flag_for_hta.equalsIgnoreCase("Drinking Water")) {
            lOcaldbNew.deletecart_Water(cart_detailsList.get(section).getIn_dish_id());
            AllValidation.myToast(context, "Watercan is successfully deleted from cart ");
            cart_detailsList.remove(section);
            notifyDataSetChanged();
            context.setPriceDetailsPatanjali(cart_detailsList);
          } else if (Commons.flag_for_hta.equalsIgnoreCase(SpeedzyConstants.Fruits)) {
            lOcaldbNew.deleteFruitcart(cart_detailsList.get(section).getIn_dish_id());
            AllValidation.myToast(context, "Fruit is successfully deleted from cart ");
            cart_detailsList.remove(section);
            notifyDataSetChanged();
            context.setPriceDetails1(cart_detailsList);
          } else {
            lOcaldbNew.deletecart(cart_detailsList.get(section).getIn_dish_id());
            AllValidation.myToast(context, "dish is successfully deleted from cart ");
            cart_detailsList.remove(section);
            notifyDataSetChanged();
            if (flag==0) {
              context.setPriceDetails1(cart_detailsList);
            }else {
              mealCombo_checkout_activity.setPriceDetails(cart_detailsList);
            }
          }
        }
      }
    });
  }

  private void veg_nonveg_iconset(Drawable innerDrawable, ViewHolder holder) {
    GravityCompoundDrawable gravityDrawable = new GravityCompoundDrawable(innerDrawable);
    innerDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(),
        innerDrawable.getIntrinsicHeight());
    gravityDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(),
        innerDrawable.getIntrinsicHeight());
    holder.itemname.setCompoundDrawables(gravityDrawable, null, null, null);
  }

  @Override
  public void onBindViewHolder(final MyCartSectionAdapter_New.ViewHolder holder, int section,
      int relativePosition, int absolutePosition) {

    final Cart_Model.Cart_Details cart_details = cart_detailsList.get(section);
    ArrayList<ArrayList<Preferencemodel>> myarray = cart_details.getPreferencelfromdb();
    if (myarray.size() > 1) {
      ArrayList<Preferencemodel> mynewArray = new ArrayList();
      for (int i = 1; i < myarray.size(); i++) {
        ArrayList<Preferencemodel> mychildarray = cart_details.getPreferencelfromdb().get(i);
        for (int j = 0; j < mychildarray.size(); j++) {
          mynewArray.add(mychildarray.get(j));
        }
      }

      if (relativePosition < mynewArray.size()) {
        Preferencemodel mychildData = mynewArray.get(relativePosition);
        Log.e("abslute =", "" + absolutePosition);
        String type = mychildData.getSectionname();
        String pref_name = mychildData.getPriceitem();
        holder.txt_radioButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        holder.txt_radioButton.setText(" --> " + type + "," + pref_name);
        int qty = Integer.parseInt(cart_details.getQuantity());

        if (mychildData.getMenuprice().length() < 2) {
          mychildData.setMenuprice("0.00");
        }
        if (mychildData.getPriceitem().length() < 2) {
          mychildData.setPriceitem("Item");
        }
        double pricefloat = Double.parseDouble(mychildData.getMenuprice());
        double totalprice = pricefloat * qty;
        holder.priceradio.setText(String.valueOf(totalprice));
      }
    }
  }

  @Override
  public MyCartSectionAdapter_New.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    // Change inflated layout based on 'header'.
    View v;
    v = LayoutInflater.from(parent.getContext())
        .inflate(
            viewType == VIEW_TYPE_HEADER ? R.layout.mycart_listitem_layout_new : R.layout.list_item,
            parent, false);
    if (flag==0) {
      lOcaldbNew = new LOcaldbNew(context);
    }else {
      lOcaldbNew = new LOcaldbNew(mealCombo_checkout_activity);
    }
    MyCartSectionAdapter_New.ViewHolder vh = new MyCartSectionAdapter_New.ViewHolder(v);
    return vh;
  }

  // ItemViewHolder Class for Items in each Section
  public class ViewHolder extends RecyclerView.ViewHolder {

    TextView priceradio;
    TextView txt_radioButton;
    LinearLayout radiobtnlay;
    TextView itemname, qty, price;
    ImageView add, sub;
    LinearLayout deletelay;

    public ViewHolder(View itemView) {
      super(itemView);
      radiobtnlay = (LinearLayout) itemView.findViewById(R.id.card_view);
      txt_radioButton = (TextView) itemView.findViewById(R.id.check);
      priceradio = (TextView) itemView.findViewById(R.id.price);
      itemname = (TextView) itemView.findViewById(R.id.itemname);
      qty = (TextView) itemView.findViewById(R.id.itemqty);
      price = (TextView) itemView.findViewById(R.id.itemprice);
      add = itemView.findViewById(R.id.itemqty_add);
      sub = itemView.findViewById(R.id.itemqty_sub);
      deletelay = (LinearLayout) itemView.findViewById(R.id.deletelay1);
    }
  }
}
