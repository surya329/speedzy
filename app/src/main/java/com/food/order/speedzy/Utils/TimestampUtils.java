package com.food.order.speedzy.Utils;

import androidx.annotation.Nullable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class
TimestampUtils {
  private static final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs
  private static SimpleDateFormat sFormatter =
      new SimpleDateFormat(SpeedzyConstants.DATE_DEFAULT_FORMAT);

  /**
   * Return an ISO 8601 combined date and time string for current date/time
   *
   * @return String with format "yyyy-MM-dd'T'HH:mm:ss'Z'"
   */
  public static String getISO8601StringForCurrentDate() {
    Date now = new Date();
    return getISO8601StringForDate(now);
  }

  /**
   * Return an ISO 8601 combined date and time string for specified date/time
   *
   * @param date Date
   * @return String with format "yyyy-MM-dd'T'HH:mm:ss'Z'"
   */
  private static String getISO8601StringForDate(Date date) {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    return dateFormat.format(date);
  }

  /**
   * Private constructor: class cannot be instantiated
   */
  private TimestampUtils() {
  }

  public static String convertDateToString(Date date) {
    return sFormatter.format(date);
  }

  public static String convertDateToString(Date date, String toformat) {
    sFormatter.applyPattern(toformat);
    return convertDateToString(date);
  }

  @Nullable public static Date convertStringToDate(String date) {
    try {
      return sFormatter.parse(date);
    } catch (ParseException e) {
      e.printStackTrace();
      return null;
    }
  }

  @Nullable public static Date convertStringToDate(String date, String format) {
    sFormatter.applyPattern(format);
    return convertStringToDate(date);
  }

  public static String formatStringDate(String dateRaw, String formatResult) {
    return convertDateToString(convertStringToDate(dateRaw), formatResult);
  }

  public static String formatStringDate(String dateRaw, String formatRaw, String formatResult) {
    return convertDateToString(convertStringToDate(dateRaw, formatRaw), formatResult);
  }

  public static String convertMonth(int input) {
    int monthOfYear = input + 1;
    if (monthOfYear >= 10) {
      return String.valueOf(monthOfYear);
    } else {
      return "0".concat(String.valueOf(monthOfYear));
    }
  }

  public static String formatDateOrHour(int input) {
    if (input >= 10) {
      return String.valueOf(input);
    } else {
      return "0".concat(String.valueOf(input));
    }
  }

  public static String getCurrentDateTime() {
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd");
    return mdformat.format(calendar.getTime());
  }

  public static String getCurrentDateTimeForPhotoFormat() {
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat mdformat = new SimpleDateFormat("yyyyMMdd");
    return mdformat.format(calendar.getTime());
  }

  public static String getDateArrival() {
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd");
    calendar.add(Calendar.DATE, 5);
    return mdformat.format(calendar.getTime());
  }

  public static String getCurrentDateTimeWithAddedMinutes() {
    Calendar rightNow = Calendar.getInstance();
    Long t = rightNow.getTimeInMillis();
    Date timeAfterAdding = new Date(t + (15 * ONE_MINUTE_IN_MILLIS));
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String formattedDate = df.format(timeAfterAdding.getTime());
    return formattedDate;
  }

  public static Date getCurrentDateTimeInTimeFormat() {
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return calendar.getTime();
  }

  public static Date getCurrentDate() {
    Calendar calendar = Calendar.getInstance();
    return (calendar.getTime());
  }

  public static Date getDateBeforeMonthInDateFormat() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MONTH, -1);
    return calendar.getTime();
  }
}
