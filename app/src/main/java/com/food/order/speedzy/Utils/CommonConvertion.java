package com.food.order.speedzy.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by surya on 16-03-2018.
 */

public class CommonConvertion {

  public static String replace_point(String Replace_value) {
    String result = Replace_value.replaceAll("[lat/lng:,]", " ");
    return result;
  }

  public static String convert_to_base64(String stringvaue) {

    byte[] data;
    String base64 = null;
    try {

      data = stringvaue.getBytes("UTF-8");

      base64 = Base64.encodeToString(data, Base64.NO_WRAP).toString();

      Log.i("Base 64 ", base64);
    } catch (UnsupportedEncodingException e) {

      e.printStackTrace();
    }

    return base64;
  }

  public static String replace_linestring(String Replace_value) {
    String result = Replace_value.replaceAll("[lat/lng: \\[ \\] ]", "");

    String[] result1 = result.split("\\(");
    StringBuilder strbuild = new StringBuilder();
    strbuild.append("(");
    for (int i = 0; i < result1.length; i++) {

      strbuild.append(result1[i].replaceAll("[\\)]", "").replaceFirst("[,]", " "));
    }
    strbuild.append(")");

    return strbuild.toString();
  }

  public static String sRID_replace(String srid) {
    if (srid != null) {
      String[] result1 = srid.split("SRID=4326;POINT");
      String result = result1[1].replaceAll("[\\(\\)\"]", "");
      return result;
    }
    return "";
  }

  public static String sourceAddress_spec_replace(String Address_spec_replace) {
    return Address_spec_replace.replaceAll("[lat/lng: \\( \\ )]", "");
  }

  public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
    Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
    vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(),
        vectorDrawable.getIntrinsicHeight());
    Bitmap bitmap =
        Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(),
            Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    vectorDrawable.draw(canvas);
    return BitmapDescriptorFactory.fromBitmap(bitmap);
  }

  public int getDetour(String eta1, String eta2) {
    int detour = 0;

      /* if(eta1.contains("km")){

            detour = Integer.parseInt(eta1.replaceAll("[^0-9]", ""));
       }
        else if(eta2.contains("km")){
           detour = detour + Integer.parseInt(eta2.replaceAll("[^0-9]", ""));
       }*/
    if (eta1.contains("km")) {
      float eta = Float.parseFloat(eta1.replaceAll("[^.0-9]", ""));
      detour = (Math.round(eta) * 1000);
    } else if (eta2.contains("km")) {
      float eta = Float.parseFloat(eta2.replaceAll("[^.0-9]", ""));
      detour = detour + (Math.round(eta) * 1000);
    } else if (eta1.contains("m")) {
      detour = detour + Integer.parseInt(eta1.replaceAll("[^0-9]", ""));
    } else if (eta2.contains("m")) {
      detour = detour + Integer.parseInt(eta1.replaceAll("[^0-9]", ""));
    }
    return detour;
  }

  public static String replaceComma(String latLng) {
    return latLng.replaceAll("[,]", " ");
  }

  public static List<LatLng> getLatLngFromLineString(String lineString) {
    List<LatLng> decoded = new ArrayList<LatLng>();
    String[] result1 = lineString.split("SRID=4326;LINESTRING");
    String result = result1[1].replaceAll("[\\(\\)]", "");
    List<String> value = new ArrayList<>();
    StringTokenizer tokenizer = new StringTokenizer(result, ",");
    while (tokenizer.hasMoreTokens()) {
      value.add(tokenizer.nextToken());
    }
    try {
      for (int i = 0; i < value.size(); i++) {
        String temp = value.get(i);
        double lng_value = Double.parseDouble(temp.split(" ")[0]);
        double lat_value = Double.parseDouble(temp.split(" ")[1]);
        decoded.add(new LatLng(lat_value, lng_value));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return decoded;
  }

  public static int detour(String eta1) {
    int detour = 0;
    if (eta1.contains("km")) {
      String eta = eta1.replaceAll("[^.0-9]", "");
      Double temp = Double.parseDouble(eta) * 1000;
      detour = temp.intValue();
    } else {
      detour = Integer.parseInt(eta1.replaceAll("[^.0-9]", ""));
    }
    return detour;
  }

  public static int waitETA(String eta1) {
    int ETA = 0;
    if (eta1.contains("hours")) {
      String[] raw = eta1.split("h");
      int hr = Integer.parseInt(String.valueOf(Integer.parseInt(raw[0]) * 60));
      int mins = Integer.parseInt(raw[1].replaceAll("[^0-9]", ""));
      ETA = hr + mins;
    } else {
      ETA = Integer.parseInt(eta1.replaceAll("[^0-9]", ""));
    }
    return ETA;
  }

  public static int wayPointsHrs(String eta) {
    int detour = 0;
    String hour = eta.split("h")[0];
    detour = Integer.parseInt(hour.replaceAll("[^.0-9]", ""));
    return detour;
  }

  public static int wayPointsHrsMins(String eta) {
    int detour = 0;
    String hour = eta.split("h")[1];
    detour = Integer.parseInt(hour.replaceAll("[^.0-9]", ""));
    return detour;
  }

  public static List<LatLng> decodePolyLine(final String poly) {
    int len = poly.length();
    int index = 0;
    List<LatLng> decoded = new ArrayList<LatLng>();
    int lat = 0;
    int lng = 0;

    while (index < len) {
      int b;
      int shift = 0;
      int result = 0;
      do {
        b = poly.charAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lat += dlat;

      shift = 0;
      result = 0;
      do {
        b = poly.charAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lng += dlng;

      decoded.add(new LatLng(
          lat / 100000d, lng / 100000d
      ));
    }

    return decoded;
  }

  public static String point_Replace(String srid) {
    String[] result1 = srid.split("POINT");
    String result = result1[1].replaceAll("[\\(\\)\"]", "");
    return result;
  }

  public static LatLng convert_sRID_to_latLng(String srid) {
    String[] result1 = srid.split("SRID=4326;POINT");
    String result = result1[1].replaceAll("[\\(\\)\"]", "");
    double lat_value = Double.parseDouble(result.split(" ")[0]);
    double lng_value = Double.parseDouble(result.split(" ")[1]);
    return new LatLng(lat_value, lng_value);
  }

  public static LatLng convert_sRID_to_latLngReverse(String srid) {
    if (srid != null) {
      String[] result1 = srid.split("SRID=4326;POINT");
      String result = result1[1].replaceAll("[\\(\\)\"]", "");
      double lat_value = Double.parseDouble(result.split(" ")[1]);
      double lng_value = Double.parseDouble(result.split(" ")[0]);
      return new LatLng(lat_value, lng_value);
    }
    return null;
  }

  public static String convertLatLongReverse(double latitude, double longitude) {
    return String.valueOf(longitude) + " " + String.valueOf(latitude);
  }

  public static LatLng convertLatLngFromStringPoints(String point) {
    String result = point.replaceAll("[\\(\\)\"]", "");
    double lat_value = Double.parseDouble(result.split(" ")[0]);
    double lng_value = Double.parseDouble(result.split(" ")[1]);
    return new LatLng(lat_value, lng_value);
  }

  public static String convertLatLongToString(double latitude, double longitude) {
    return String.valueOf(latitude) + "," + String.valueOf(longitude);
  }
}
