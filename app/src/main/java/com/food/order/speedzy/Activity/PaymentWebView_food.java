package com.food.order.speedzy.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.database.LOcaldbNew;

import java.util.Timer;
import java.util.TimerTask;

public class PaymentWebView_food extends AppCompatActivity {

    MySharedPrefrencesData mySharedPrefrencesData;
    String Veg_Flag;
    WebView webview;
    ProgressDialog pd;
    String order_id;
    String url="http://35.244.8.156/speedzy/payment/payment.php?orderid=";
    //String url_resturant="http://foodinns.com/payment/paymenttest.php?orderid=";
    //String url_patanjali="http://foodinns.com/payment-grocery/payment.php?orderid=";
    LOcaldbNew lOcaldbNew;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_web_view_food);

        mySharedPrefrencesData = new MySharedPrefrencesData();
        Veg_Flag=mySharedPrefrencesData.getVeg_Flag(PaymentWebView_food.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (Veg_Flag.equalsIgnoreCase("1")){
            statusbar_bg(R.color.red);
        }else {
            statusbar_bg(R.color.red);
        }

        lOcaldbNew=new LOcaldbNew(this);
        order_id=getIntent().getStringExtra("order_id");
        pd = new ProgressDialog(this);
        pd.setMessage("Please wait Loading...");
        pd.show();
        webview=(WebView)findViewById(R.id.webview);
      /*  if (Commons.flag_for_hta == "Grocerry" || Commons.flag_for_hta == "Patanjali" || Commons.flag_for_hta=="City Special"
                || Commons.flag_for_hta=="Drinking Water") {
            url=url_patanjali;
        }else {
            url=url_resturant;
        }*/
        callpaymentWebviewApi(url+order_id);
    }
    private void callpaymentWebviewApi(String url) {
        this.setFinishOnTouchOutside(false);
        pd.dismiss();
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.setWebViewClient(new MyWebViewClient());
        webview.loadUrl(url);
    }
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            if(url.contains("success")){
                dialog_show();
            }
            if(url.contains("cancel")){

                Toast.makeText(PaymentWebView_food.this,"Transaction failed...",Toast.LENGTH_LONG).show();
                Timer t1 = new Timer();
                t1.scheduleAtFixedRate(new TimerTask() {

                    @Override
                    public void run() {
                        PaymentWebView_food.this.finish();
                    }

                },0,120000);

            }
            return true;
        }
    }

    private void dialog_show() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.order_successful_popup);
        TextView ok = (TextView) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int type=0;
                dialog.dismiss();
                if (Commons.flag_for_hta == "Grocerry" || Commons.flag_for_hta == "Patanjali") {
                    lOcaldbNew.deleteCart_patanjali();
                    type=2;
                } else if (Commons.flag_for_hta=="City Special"){
                    lOcaldbNew.deleteCart_CitySpecial();
                    type = 2;
                } else if (Commons.flag_for_hta=="Drinking Water"){
                    lOcaldbNew.deleteCart_Water();
                    type = 2;
                }else {
                    lOcaldbNew.deleteCart();
                    type=1;
                }
                Intent intent = new Intent(PaymentWebView_food.this, MyOrderActivity.class);
                intent.putExtra("flag",1);
                intent.putExtra("type",type);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
        dialog.show();
    }
    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this,color ));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Commons.back_button_transition(PaymentWebView_food.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Commons.back_button_transition(PaymentWebView_food.this);
    }
}
