package com.food.order.speedzy.SpeedzyRideApi;

import com.food.order.speedzy.SpeedzyRideApi.response.LoginRide.UserRegistration;

import io.reactivex.Observable;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RideApiService {

  @POST("graphql") Observable<UserRegistration> loginRide(@Query("query") String query);
}
