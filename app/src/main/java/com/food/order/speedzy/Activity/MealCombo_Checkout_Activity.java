package com.food.order.speedzy.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;

import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Adapter.RestaurantDetailsAdapterMenu;
import com.food.order.speedzy.Model.Dishdetail;
import com.food.order.speedzy.Utils.reyclerviewutils.RecyclerViewUtils;
import com.food.order.speedzy.api.response.coupon.CouponItem;
import com.food.order.speedzy.api.response.coupon.CouponsResponse;
import com.food.order.speedzy.screen.coupon.CouponAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.food.order.speedzy.Adapter.MyCartSectionAdapter_New;
import com.food.order.speedzy.Model.Cakes_Model;
import com.food.order.speedzy.Model.Cart_Model;
import com.food.order.speedzy.Model.Combo_Details_Response_Model;
import com.food.order.speedzy.Model.Combo_Response_Model;
import com.food.order.speedzy.Model.Delivery_Charge_Response_Model;
import com.food.order.speedzy.Model.Preferencemodel;
import com.food.order.speedzy.Model.RestaurantModel;
import com.food.order.speedzy.Model.StorepackagerModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import com.food.order.speedzy.api.response.home.HomePageAppConfigResponse;
import com.food.order.speedzy.api.response.home.ItemsItem;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.database.LOcaldbNew;
import com.food.order.speedzy.screen.home.SpeedzyOperationsClosedModel;
import com.food.order.speedzy.screen.operationsclosed.OperationsClosedActivity;
import com.food.order.speedzy.screen.ordersuccess.ActivityOrderDataModel;
import com.food.order.speedzy.screen.ordersuccess.ActivityOrderSuccess;
import com.food.order.speedzy.screen.orderupi.OrderUPIDataModel;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.michael.easydialog.EasyDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sujata Mohanty.
 */


public class MealCombo_Checkout_Activity extends AppCompatActivity {
    MySharedPrefrencesData mySharedPrefrencesData;
    TextView selected_location, change,add_address,add_item;
    ItemsItem items;
    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView res_imgview;
    TextView title;
    String Veg_Flag;
    String delivery_charge = "";
    ImageView itemqty_add, itemqty_sub;
    TextView itemname, res_name, combo_item, itemprice, itemqty, subtotal, deliverycharge_cart, wallet_text, grandtotal;
    LinearLayout checkout;
    LOcaldbNew lOcaldbNew;
    String cake_flag="0";
    Combo_Response_Model.combos.Itemlist Combo_Details;
    Cakes_Model.Cake Cake_Details;
    String image_url_thumb = "", image_url_main = "";
    int flag = 0;
    String amount;
    double grandtotalamount = 0.00, walletvalue = 0;
    TextView asap, later;
    Spinner later_time;
    String time_selected = "1";
    String[] arrayspinner = new String[0];
    ArrayList<String> time_new_with_cuurenttime = new ArrayList<>();
    ArrayList<String> deltime= new ArrayList<>();
    String start_time_lunch_list, end_time_lunch_list, start_time_dinner_list, end_time_dinner_list, app_openstatus;
    Date starttime_lunch, endtime_lunch, starttime_dinner, endtime_dinner, newtime;
    boolean istimecycle = true;
    double totalsum = 0.00,totalsum_combo=0.00,grand_total=0.00;
    boolean isActivityRunning = false;
    String tommorrow_del="0";
    Cakes_Model.open_close_status Cake_res_open_status;
    Cakes_Model.Restaurant_timings Cake_rest_timings;
    RecyclerView recyclerView;
    LinearLayoutManager lm;
    ArrayList<Cart_Model.Cart_Details> cd = new ArrayList<>();
    private SpeedzyOperationsClosedModel mSpeedzyOperationsClosedModel;
    Apimethods methods;
    static String city_id = "";
    private Disposable mDisposable;
    private OrderUPIDataModel mOrderUPIDataModel;
    ImageView res_charge_tooltip;
    TextView restaurant__charges;
    TextView  add_coupon_txt,apply;
    private List<CouponItem> mCouponItems= new ArrayList<>();
    String coupon_id = "voucherid", coupon_amount = "vaoucheramount", discount_type = "",
            discount_value = "0", max_amount = "0", min_order_value = "0";
    double coupon_discount_amount = 0.00, max_coupon_discount = 0.00;
    List<CouponItem.discount_slabs> discount_slabs=new ArrayList<>();
    boolean add_coupon_flag = false;
    String coupon_code = "voucherid";
    Dialog dialog_msg;
    TextView msg,ok,no;
    TextView discount_text;
    LinearLayout discount_lay;
    int max_qty_per_user=11;
    String lati="",longi="";
    String coupon_flag="",coupon_msg="";
    LinearLayout coupon_linear;
    String coupon_apply="0";
    RelativeLayout delivery_frag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_combo__checkout_);

        lOcaldbNew = new LOcaldbNew(MealCombo_Checkout_Activity.this);

        dialog_msg =
                new Dialog(MealCombo_Checkout_Activity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog_msg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_msg.setContentView(R.layout.logout_popup);
        msg = (TextView) dialog_msg.findViewById(R.id.warning);
        ok = (TextView) dialog_msg.findViewById(R.id.yes);
        no = (TextView) dialog_msg.findViewById(R.id.no);
        ok.setText("Ok");
        no.setVisibility(View.GONE);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_msg.dismiss();
            }
        });

        flag = getIntent().getIntExtra("flag", 0);
        Commons.menu_id = "M1021";
        if (flag == 1) {
            Combo_Details = (Combo_Response_Model.combos.Itemlist) getIntent().getSerializableExtra("Combo_Details");
            max_qty_per_user= Integer.parseInt(Combo_Details.getMaxQtyPeruser());
            coupon_apply=Combo_Details.getCoupon_active();
        } else if (flag == 3){
            Cake_Details= (Cakes_Model.Cake) getIntent().getSerializableExtra("Cake_Details");
            Cake_rest_timings= (Cakes_Model.Restaurant_timings) getIntent().getSerializableExtra("Cake_rest_timings");
            Cake_res_open_status= (Cakes_Model.open_close_status) getIntent().getSerializableExtra("Cake_res_open_status");
        }else {
            items = (ItemsItem) getIntent().getSerializableExtra("meal_combo");
            max_qty_per_user= Integer.parseInt(items.getMax_qty_peruser());
            coupon_apply=items.getCoupon_active();
        }
        mySharedPrefrencesData = new MySharedPrefrencesData();
        Veg_Flag = mySharedPrefrencesData.getVeg_Flag(this);
        methods =
                API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);

        itemname = (TextView) findViewById(R.id.itemname);
        res_name = (TextView) findViewById(R.id.res_name);
        combo_item = (TextView) findViewById(R.id.combo_item);
        itemqty = (TextView) findViewById(R.id.itemqty);
        itemprice = (TextView) findViewById(R.id.itemprice);
        itemqty_add = findViewById(R.id.itemqty_add);
        itemqty_sub = findViewById(R.id.itemqty_sub);
        add_coupon_txt = (TextView) findViewById(R.id.add_coupon_txt);
        apply= (TextView) findViewById(R.id.apply);
        res_charge_tooltip= findViewById(R.id.res_charge_tooltip);
        restaurant__charges=findViewById(R.id.restaurant__charges);
        discount_lay=findViewById(R.id.discount_lay);
        discount_text=findViewById(R.id.discount_text);
        subtotal = (TextView) findViewById(R.id.subtotal);
        deliverycharge_cart = (TextView) findViewById(R.id.deliverycharge_cart);
        wallet_text = (TextView) findViewById(R.id.wallet_text);
        grandtotal = (TextView) findViewById(R.id.grandtotal);
        checkout = (LinearLayout) findViewById(R.id.checkout_linear);
        selected_location = (TextView) findViewById(R.id.selected_location);
        change = (TextView) findViewById(R.id.change);
        add_address=findViewById(R.id.add_address);
        add_item=(TextView) findViewById(R.id.add_item);
        res_imgview = (ImageView) findViewById(R.id.res_img);
        asap = (TextView) findViewById(R.id.asap);
        later = (TextView) findViewById(R.id.later);
        later_time = (Spinner) findViewById(R.id.later_time);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        coupon_linear=findViewById(R.id.coupon_linear);
        delivery_frag=findViewById(R.id.delivery_frag);
        title = (TextView) findViewById(R.id.title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        // title.setText(name);

        if (coupon_apply.equalsIgnoreCase("1")){
            coupon_linear.setVisibility(View.VISIBLE);
            discount_lay.setVisibility(View.VISIBLE);
        }else {
            coupon_linear.setVisibility(View.GONE);
            discount_lay.setVisibility(View.GONE);
        }

        recyclerView = (RecyclerView) findViewById(R.id.cartlistItems);
        recyclerView.setHasFixedSize(true);
        lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);

        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(itemname.getText().toString());
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        if (Veg_Flag.equalsIgnoreCase("1")) {
            statusbar_bg(R.color.red);
        } else {
            statusbar_bg(R.color.red);
        }

        if (mySharedPrefrencesData.getLocation(this) == null
                || mySharedPrefrencesData.getLocation(this).equalsIgnoreCase("")) {
            delivery_frag.setVisibility(View.GONE);
            add_address.setVisibility(View.VISIBLE);
        }else {
            delivery_frag.setVisibility(View.VISIBLE);
            add_address.setVisibility(View.GONE);
            selected_location.setText(mySharedPrefrencesData.getLocation(this));
        }

        asap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (app_openstatus!=null && app_openstatus.equalsIgnoreCase("1")) {
                    time_selected = "1";
                    time_slect_fun();
                }
            }
        });
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time_selected = "2";
                time_slect_fun();
                if (Commons.flag_for_hta != "Patanjali" || Commons.flag_for_hta != "City Special") {
                    if (tommorrow_del.equalsIgnoreCase("0")) {
                        dialogshow_time(time_new_with_cuurenttime);
                    } else {
                        dialogshow_time(deltime);
                    }
                }
            }
        });

        if (flag == 3) {
            image_url_thumb = "https://storage.googleapis.com/speedzy_data/resources/cakes/thumb_image/" + Cake_Details.getImagename();
            image_url_main = "https://storage.googleapis.com/speedzy_data/resources/cakes/main_image/" + Cake_Details.getImagename();
            itemname.setText(Cake_Details.getStDishName());
            itemprice.setText("₹ " + Cake_Details.getStPrice().get(0).getMenuPrice());
            amount = itemprice.getText().toString();
            call_api(1,Cake_Details.getIn_restaurant_id(),"");
        }else if (flag == 0) {
            image_url_thumb = "https://storage.googleapis.com/speedzy_data/resources/mealcombo/thumb/" + items.getImagename();
            image_url_main = "https://storage.googleapis.com/speedzy_data/resources/mealcombo/main_image/" + items.getImagename();
            itemname.setText(items.getName());
            itemprice.setText("₹ " + items.getStPrice().get(0).getMenuPrice());
            amount = itemprice.getText().toString();
            call_api(0,items.getComboId(),items.getRestId());
        } else {
            image_url_thumb = "https://storage.googleapis.com/speedzy_data/resources/mealcombo/thumb/" + Combo_Details.getImagename();
            image_url_main = "https://storage.googleapis.com/speedzy_data/resources/mealcombo/main_image/" + Combo_Details.getImagename();
            itemname.setText(Combo_Details.getStDishName());
            itemprice.setText("₹ " + Combo_Details.getStPrice().get(0).getMenuPrice());
            amount = itemprice.getText().toString();
            call_api(0,Combo_Details.getComboId(),"");
        }
        RequestBuilder<Drawable> thumbnailRequest = Glide
                .with(this)
                .load(image_url_thumb);
        Glide
                .with(this)
                .load(image_url_main)
                .thumbnail(thumbnailRequest)
                .apply(new RequestOptions()
                .placeholder(R.drawable.grid_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(res_imgview);
        selected_location.setText( mySharedPrefrencesData.getLocation(this));
        Commons.order_type = "1";
        Commons.dicount_val = "0.00";
        itemqty_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(itemqty.getText().toString());
                if (qty < max_qty_per_user) {
                    qty++;
                    set_price_details_combo(qty);
                }
            }
        });

        itemqty_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(itemqty.getText().toString());
                if (qty > 1) {
                    qty--;
                    set_price_details_combo(qty);
                }
            }
        });
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                address_change_intent();
            }
        });
        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                address_change_intent();
            }
        });
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
                DateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd");
                String date_order = dateFormatter.format(c.getTime());
                Commons.order_date_from_cart = date_order;
                JSONArray order_detail = order_details(itemname.getText().toString(), Double.parseDouble(itemprice.getText().toString().replace("₹ ", "")), itemqty.getText().toString(), Commons.order_amount);
                if (totalsum >= Commons.min_order_rest) {
                    if (time_selected.equalsIgnoreCase("2")) {
                        String s = "";
                        if (tommorrow_del.equalsIgnoreCase("1")) {
                            c.add(Calendar.DAY_OF_YEAR, 1);
                        }
                        if (later_time.getSelectedItem() != null) {
                            if (mySharedPrefrencesData.getLocation(MealCombo_Checkout_Activity.this) == null
                                    || mySharedPrefrencesData.getLocation(MealCombo_Checkout_Activity.this).equalsIgnoreCase("")) {
                                msg.setText("Please add or select default Address");
                                dialog_msg.show();
                            }else {
                                s = dateFormatter1.format(c.getTime()) + " " + later_time.getSelectedItem().toString() + ":00";
                                Commons.Preorder_date_from_cart = s;
                                Commons.dialogshow_payment(MealCombo_Checkout_Activity.this, "1", Double.parseDouble(itemprice.getText().toString().replace("₹ ", "")), "1234", "Delivery", mySharedPrefrencesData.getUser_Id(MealCombo_Checkout_Activity.this), mySharedPrefrencesData, lOcaldbNew, delivery_charge, order_detail);

                            }
                        } else {
                            if (time_new_with_cuurenttime.isEmpty()) {
                                dialog_show(0);
                            }
                        }
                    } else {
                       /* if (!time_new_with_cuurenttime.isEmpty()) {
                            String s = dateFormatter1.format(c.getTime()) + " " + time_new_with_cuurenttime.get(0) + ":00";
                            Commons.Preorder_date_from_cart = s;
                            Commons.dialogshow_payment(MealCombo_Checkout_Activity.this, "1", Double.parseDouble(itemprice.getText().toString().replace("₹ ", "")), "1234", "Delivery", mySharedPrefrencesData.getUser_Id(MealCombo_Checkout_Activity.this), mySharedPrefrencesData, lOcaldbNew, delivery_charge, order_detail);
                        } else {
                            dialog_show(0);
                        }*/

                        String s;
                        if (time_new_with_cuurenttime.size() > 0) {
                            //s = dateFormatter1.format(c.getTime()) + " " + time_new_with_cuurenttime.get(0) + ":00";
                            s = dateFormatter.format(c.getTime());
                        } else {
                            s = dateFormatter1.format(c.getTime()) + " ";
                        }
                        if (Integer.parseInt(app_openstatus) == SpeedzyConstants.AppOpenStatus.OPEN) {
                            Commons.Preorder_date_from_cart = s;
                            Commons.dialogshow_payment(MealCombo_Checkout_Activity.this, "1", Double.parseDouble(itemprice.getText().toString().replace("₹ ", "")), "1234", "Delivery", mySharedPrefrencesData.getUser_Id(MealCombo_Checkout_Activity.this), mySharedPrefrencesData, lOcaldbNew, delivery_charge, order_detail);
                        } else if (mSpeedzyOperationsClosedModel != null) {
                            startActivity(OperationsClosedActivity.newIntent(MealCombo_Checkout_Activity.this, mSpeedzyOperationsClosedModel));
                        }
                    }
                }else {
                    dialog_show(1);
                }
            }
        });

        System.gc();
    }

    private void address_change_intent() {
        Intent intent = new Intent(MealCombo_Checkout_Activity.this, Saved_Addresses.class);
        intent.putExtra("change_address", true);
        startActivityForResult(intent, 1);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                /*Intent intent = new Intent(CheckoutActivity_New.this, LocationActivity.class);
                startActivityForResult(intent, 1);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);*/
    }

    private void getCouponList(String resturantId) {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);

        Call<CouponsResponse> call =methods.getCouponList(resturantId,mySharedPrefrencesData.getUser_Id(this),Commons.menu_id);
        Log.d("url","url="+call.request().url().toString());

        call.enqueue(new Callback<CouponsResponse>() {
            @Override
            public void onResponse(Call<CouponsResponse> call, Response<CouponsResponse> response) {
                int statusCode = response.code();
                Log.d("Response",""+statusCode);
                Log.d("respones",""+response);
                CouponsResponse couponsResponse=response.body();
                coupon_flag=couponsResponse.getFlag().toString();
                coupon_msg=couponsResponse.getMsg().toString();
                if (couponsResponse != null
                        && couponsResponse.getStatus()
                        .equalsIgnoreCase("Success")
                        && couponsResponse.getCoupon() != null
                        && couponsResponse.getCoupon().size() > 0) {
                    mCouponItems.clear();
                    mCouponItems.addAll(couponsResponse.getCoupon());
                    add_coupon_dialog(mCouponItems);
                } else {
                    add_coupon_dialog(mCouponItems);
                }
            }

            @Override
            public void onFailure(Call<CouponsResponse> call, Throwable t) {

                //             Toast.makeText(Reviews.this,"internet not available..connect internet",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void add_coupon_dialog(List<CouponItem> data) {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        Objects.requireNonNull(dialog.getWindow())
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.popup_coupon_view);
        final EditText etCoupon = (EditText) dialog.findViewById(R.id.et_coupon);
        final RecyclerView rvCoupon = dialog.findViewById(R.id.rv_coupon);
        final TextView btnApply = dialog.findViewById(R.id.btn_apply);
        final TextView txtAvailableCoupons = dialog.findViewById(R.id.txt_available_coupons);
        final TextView txtNoCouponsAvailable = dialog.findViewById(R.id.txt_no_coupons_available);
        CouponAdapter mAdapter = new CouponAdapter(new CouponAdapter.CallBack() {
            @Override public void onClickApplyButton(CouponItem couponItem) {
                call_voucher_data(couponItem.getStCouponCode(), dialog);
            }
        });
        rvCoupon.setLayoutManager(RecyclerViewUtils.newLinearVerticalLayoutManager(this));
        rvCoupon.addItemDecoration(
                RecyclerViewUtils.newVerticalSpacingItemDecoration(this, R.dimen.spacing_12, true, true));
        rvCoupon.setAdapter(mAdapter);
        if (data.size() > 0) {
            mAdapter.setData(data);
            txtNoCouponsAvailable.setVisibility(View.GONE);
            txtAvailableCoupons.setVisibility(View.VISIBLE);
        } else {
            txtNoCouponsAvailable.setVisibility(View.VISIBLE);
            txtAvailableCoupons.setVisibility(View.GONE);
        }
        if (coupon_flag.equalsIgnoreCase("1")) {
            if (coupon_flag.equalsIgnoreCase("1")) {
                txtNoCouponsAvailable.setVisibility(View.VISIBLE);
                txtAvailableCoupons.setVisibility(View.GONE);
                txtNoCouponsAvailable.setText(coupon_msg);
            }
        }
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                if (!etCoupon.getText().toString().trim().isEmpty()) {
                    call_voucher_data(etCoupon.getText().toString().trim(), dialog);
                } else {
                    Toast.makeText(MealCombo_Checkout_Activity.this, "Enter Coupon Code", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
        dialog.show();
    }
    private void call_voucher_data(final String code, final Dialog d) {
        CouponItem couponItem=null;
        for (int i=0;i<mCouponItems.size();i++){
            if (code.equalsIgnoreCase(mCouponItems.get(i).getStCouponCode())){
                couponItem=mCouponItems.get(i);
            }
        }
        if (code != null && couponItem!=null) {
            coupon_id = couponItem.getInCouponId().toString();
            discount_type = couponItem.getDiscountType().toString();
            discount_value = couponItem.getDiscountValue().toString();
            max_amount = couponItem.getMaxAmount().toString();
            min_order_value = couponItem.getMinOrderValue().toString();
            discount_slabs=couponItem.getDiscount_slabs();
            coupon_code = code;
            if (d != null) {
                    add_coupon_flag = true;
                ArrayList<Cart_Model.Cart_Details> cd1 =
                        lOcaldbNew.getCart_Details();
                setPriceDetails(cd1);
                d.dismiss();
            }
        } else {
            msg.setText("Wrong code Applied");
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    add_coupon_flag = false;
                    apply.setVisibility(View.VISIBLE);
                    add_coupon_txt.setText("Apply Coupon ");
                    add_coupon_txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
                            0);
                    dialog_msg.dismiss();
                }
            });
            dialog_msg.show();
        }
    }
    private void AddCoupon() {
        if (Double.parseDouble(min_order_value) <= totalsum) {
            if (discount_type.equalsIgnoreCase("0")) {
                coupon_discount_amount = Double.parseDouble(discount_value);
                if (coupon_discount_amount >= Double.parseDouble(max_amount)) {
                    max_coupon_discount = Double.parseDouble(max_amount);
                } else {
                    max_coupon_discount = coupon_discount_amount;
                }
            } else {
                coupon_discount_amount = totalsum * (Double.parseDouble(discount_value) / 100);
                if (!discount_slabs.isEmpty()){
                    for (int i=0;i<discount_slabs.size();i++){
                        if (!discount_slabs.get(i).getOrdervalue().equalsIgnoreCase("")) {
                            if (totalsum >= Double.parseDouble(discount_slabs.get(i).getOrdervalue())) {
                                if (coupon_discount_amount >= Double.parseDouble(discount_slabs.get(i).getDiscount())) {
                                    max_coupon_discount = Double.parseDouble(discount_slabs.get(i).getDiscount());
                                }else {
                                    max_coupon_discount = coupon_discount_amount;
                                }
                            }
                        }else {
                            if (coupon_discount_amount >= Double.parseDouble(max_amount)) {
                                max_coupon_discount = Double.parseDouble(max_amount);
                            } else {
                                max_coupon_discount = coupon_discount_amount;
                            }
                        }
                    }
                }
            }
        } else {
            coupon_discount_amount = 0.00;
            max_coupon_discount = 0.00;
            msg.setText("Sorry! this offer is applicable only on the orders above ₹ " + min_order_value);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    apply.setVisibility(View.VISIBLE);
                    add_coupon_txt.setText("Apply Coupon ");
                    add_coupon_txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
                            0);
                    dialog_msg.dismiss();
                }
            });
            dialog_msg.show();
        }
    }
    private void ToolTipView(Combo_Details_Response_Model combo_details_response_model) {
        View view = this.getLayoutInflater().inflate(R.layout.custom_tooltip, null);
        TextView amount=view.findViewById(R.id.amount);
        TextView desc=view.findViewById(R.id.desc);
        desc.setText(combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getDesc());
        amount.setText("₹ "+combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getAmount());
        new EasyDialog(MealCombo_Checkout_Activity.this)
                .setLayout(view)
                .setBackgroundColor(getResources().getColor(R.color.red))
                .setLocationByAttachedView(res_charge_tooltip)
                .setGravity(EasyDialog.GRAVITY_TOP)
                .setAnimationAlphaShow(100, 0.0f, 1.0f)
                .setAnimationAlphaDismiss(100, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(false)
                .setMarginLeftAndRight(24, 24)
                .setOutsideColor(Color.TRANSPARENT)
                .show();
    }

    private void dialogshow_time(ArrayList<String> delivery_time) {

        arrayspinner = delivery_time.toArray(new String[0]);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayspinner);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        later_time.setAdapter(dataAdapter);
        later_time.setSelection(0);
        dataAdapter.notifyDataSetChanged();
        later_time.setVisibility(View.VISIBLE);
        final Handler h = new Handler();
        new Thread(new Runnable() {
            public void run() {
                h.postDelayed(new Runnable() {
                    public void run() {
                        if (isActivityRunning) {
                            later_time.performClick();
                        }
                    }
                }, 1000);
            }
        }).start();

    }

    public void setimagefortdto(int ca) throws ParseException {
        if (ca == 0) {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = df.format(c.getTime());

            DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
            DateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd");
            String date_order = dateFormatter.format(c.getTime());
            Commons.order_date_from_cart = date_order;
            Commons.Preorder_date_from_cart = "";
            Commons.Preorder_date_from_cart = dateFormatter1.format(c.getTime());
            // if (ordertype == "1") {
            deltime = callfortime_del(start_time_lunch_list,
                    end_time_lunch_list, start_time_dinner_list, end_time_dinner_list);

            DateFormat dateFormatcuurenttime = new SimpleDateFormat("H:mm");
            Calendar date = Calendar.getInstance();
            long t = date.getTimeInMillis();
            String str = dateFormatcuurenttime.format(new Date(t + (30 * 60000)));
            Date cuurenttime = dateFormatcuurenttime.parse(str);

            time_new_with_cuurenttime.clear();
            for (int i = 0; i < deltime.size(); i++) {
                Date timecheck = dateFormatcuurenttime.parse(deltime.get(i));
                if (timecheck.compareTo(cuurenttime) > 0) {
                    time_new_with_cuurenttime.add(dateFormatcuurenttime.format(timecheck));
                }
            }
            if (app_openstatus.equalsIgnoreCase("0")) {
                time_selected = "2";
                time_slect_fun();
                asap.setTextColor(getResources().getColor(R.color.grey));
                if (time_new_with_cuurenttime.size() > 0) {
                    dialogshow_time(time_new_with_cuurenttime);
                } else {
                    dialog_show(0);
                }
            } else {
                if (!time_new_with_cuurenttime.isEmpty()) {
                    time_selected = "1";
                    time_slect_fun();
                    asap.setTextColor(getResources().getColor(R.color.black));
                }
            }
        }
    }

    private void dialog_show(int i){
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.wallet_popup);
        LinearLayout linear1=(LinearLayout) dialog.findViewById(R.id.linear1);
        TextView proceed = (TextView) dialog.findViewById(R.id.proceed);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        TextView dis = (TextView) dialog.findViewById(R.id.dismiss);
        TextView warn = (TextView) dialog.findViewById(R.id.warning);
        TextView msg = (TextView) dialog.findViewById(R.id.message);
        if (i==0) {
            linear1.setVisibility(View.VISIBLE);
            dis.setVisibility(View.GONE);
            warn.setText("Info!!");
            msg.setText("Sorry we are closed at this moment.please order for tomorrow");
        }else {
            linear1.setVisibility(View.GONE);
            dis.setVisibility(View.VISIBLE);
            warn.setText("Info!!");
            msg.setText("Please add more dishes to get total amount of ₹" + Commons.min_order_rest);
        }
        dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tommorrow_del="1";
                dialogshow_time(deltime);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private ArrayList<String> callfortime_del(String start_time_lunch_list, String end_time_lunch_list, String start_time_dinner_list, String end_time_dinner_list) {

        DateFormat dateFormat = new SimpleDateFormat("H:mm");
        DateFormat dateFormat2 = new SimpleDateFormat("H:mm");

        ArrayList<String> time_todaylist = new ArrayList<>();
        ArrayList<String> time_todaylist_lunch = new ArrayList<>();
        ArrayList<String> time_todaylist_dinner = new ArrayList<>();
        try {

            if (!(start_time_lunch_list == null)) {
                starttime_lunch = dateFormat.parse(start_time_lunch_list);
                endtime_lunch = dateFormat.parse(end_time_lunch_list);
                newtime = starttime_lunch;
                String str = dateFormat.format(new Date());
                Date cuurenttime = dateFormat.parse(str);
                time_todaylist_lunch.add(String.valueOf(dateFormat2.format(starttime_lunch)));
                while (newtime.compareTo(endtime_lunch) < 0 && istimecycle == true) {
                    Date timget = covert_time_for_add(newtime, endtime_lunch);
                    time_todaylist_lunch.add(String.valueOf(dateFormat2.format(timget)));
                }
                time_todaylist.addAll(time_todaylist_lunch);
            }

            if (!(start_time_dinner_list == null)) {
                starttime_dinner = dateFormat.parse(start_time_dinner_list);
                endtime_dinner = dateFormat.parse(end_time_dinner_list);
                System.out.println("Time: " + dateFormat.format(starttime_dinner));
                newtime = starttime_dinner;
                time_todaylist_dinner.add(String.valueOf(dateFormat2.format(starttime_dinner)));
                if (istimecycle == true) {
                    while (newtime.compareTo(endtime_dinner) < 0 && istimecycle == true) {
                        Date timget = covert_time_for_add(newtime, endtime_dinner);
                        time_todaylist_dinner.add(String.valueOf(dateFormat2.format(timget)));
                    }
                }
                time_todaylist.addAll(time_todaylist_dinner);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.d("lunch_del_arr", time_todaylist + "");
        return time_todaylist;

    }

    public Date covert_time_for_add(Date date, Date dateend) {

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, 30);
        String newTime1 = df.format(cal.getTime());
        Date addedtime = null;
        try {
            if (newTime1.equalsIgnoreCase("00:00")) {
                istimecycle = false;
                newtime = dateend;
            } else {
                addedtime = df.parse(newTime1);
                newtime = addedtime;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newtime;
    }


    private void time_slect_fun() {
        if (time_selected.equalsIgnoreCase("1")) {
            asap.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_checked, 0, 0, 0);
            later.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_unchecked, 0, 0, 0);
            later_time.setVisibility(View.GONE);
        } else {
            later.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_checked, 0, 0, 0);
            asap.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_unchecked, 0, 0, 0);
        }
    }

    private void set_price_details_combo(int qty) {
        if (flag == 1) {
            lOcaldbNew.updateCart(String.valueOf(qty),
                    Combo_Details.getInDishId());
        }
        if (flag == 0) {
            lOcaldbNew.updateCart(String.valueOf(qty),
                    items.getIn_dish_id());
        }
        if (flag == 3) {
            lOcaldbNew.updateCart(String.valueOf(qty),
                    Cake_Details.getInDishId());
        }
        itemqty.setText("" + qty);
         totalsum_combo = Double.parseDouble(amount.replace("₹ ", "")) * qty;
         grand_total=totalsum+totalsum_combo;
        itemprice.setText("₹ " + totalsum_combo);
        subtotal.setText("₹" + grand_total);
        cd = lOcaldbNew.getCart_Details();
        setPriceDetails(cd);
    }
    public void setPriceDetails(ArrayList<Cart_Model.Cart_Details> listdata) {
        if (listdata.size() > 0) {
            totalsum = 0.00;
            for (int k = 0; k < listdata.size(); k++) {
                final Cart_Model.Cart_Details cart_details = listdata.get(k);
                int qty = Integer.parseInt(cart_details.getQuantity());
                double dishPrice = Double.parseDouble(cart_details.getMenu_price()) * qty;
                ArrayList<ArrayList<Preferencemodel>> myarray = cart_details.getPreferencelfromdb();
                if (myarray.size() > 1) {
                    for (int i = 1; i < myarray.size(); i++) {
                        ArrayList<Preferencemodel> mychildarray = cart_details.getPreferencelfromdb().get(i);
                        for (int j = 0; j < mychildarray.size(); j++) {
                            Preferencemodel prefData = mychildarray.get(j);
                            if (prefData.getMenuprice().length() > 2) {
                                double itemPrice = Double.parseDouble(prefData.getMenuprice()) * qty;
                                dishPrice = dishPrice + itemPrice;
                            } else {
                                double itemPrice = 0.00;
                                dishPrice = dishPrice + itemPrice;
                            }
                        }
                    }
                }
                totalsum = totalsum + dishPrice;
                if (!GeneralUtil.isStringEmpty(cart_details.getCake_flg()) &&
                        cart_details.getCake_flg().equalsIgnoreCase("1")) {
                    cake_flag = "1";
                }
            }
            grand_total=totalsum;
            subtotal.setText("₹" + totalsum);
            Log.e("totalsum:", "" + totalsum);
            call_deliverychargeApi();
        }
    }

    private JSONArray order_details(String itemname, double itemprice, String itemqty, String order_amount) {
        JSONArray jarray = new JSONArray();
        JSONObject jobj = new JSONObject();
        Map<String, String> map = new HashMap<String, String>();
        map.put("Extra_Item", null);
        map.put("name", itemname);
        map.put("id", null);
        map.put("qty", itemqty);
        map.put("Unit", null);
        map.put("Actual_Price", String.valueOf((int) itemprice));
        map.put("Extra_Price", String.valueOf((int) itemprice));
        map.put("subtotal", order_amount);
        map.put("Type", null);
        map.put("price", String.valueOf((int) itemprice));
        jobj = new JSONObject(map);
        jarray.put(jobj);
        return jarray;
    }

    private void call_api(int i,String comboId,String rest_id) {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<Combo_Details_Response_Model> call = null;
        if (i==0){
            call = methods.get_combo_details(comboId,rest_id);
        }else {
            call = methods.get_cake_details(comboId);
        }
        Log.d("url", "url=" + call.request().url().toString());
        call.enqueue(new Callback<Combo_Details_Response_Model>() {
            @Override
            public void onResponse(Call<Combo_Details_Response_Model> call, Response<Combo_Details_Response_Model> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                Combo_Details_Response_Model combo_details_response_model = response.body();
                if (combo_details_response_model.getStatus().equalsIgnoreCase("Success")) {
                    restaurant__charges.setText("₹ "+combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getAmount());
                    Commons.resturant_charge_amount=combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getAmount();
                    res_charge_tooltip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ToolTipView(combo_details_response_model);
                        }
                    });
                    //delivery_charge=combo_details_response_model.getDeliveryCharge().toString();
                    lati=combo_details_response_model.getRestaurantDetails().get(0).getSt_latitude();
                    longi=combo_details_response_model.getRestaurantDetails().get(0).getSt_longitude();

                    Log.d("url", "url=" + lati+"\n"+longi);
                    res_name.setText("From :" + combo_details_response_model.getRestaurantDetails().get(0).getStRestaurantName());
                    Commons.restaurant_id = combo_details_response_model.getRestaurantDetails().get(0).getInRestaurantId();
                    //Commons.delivery_charge=combo_details_response_model.getDeliveryCharge();
                    Commons.min_order_rest= Double.parseDouble(combo_details_response_model.getRestaurantDetails().get(0).getStMinOrder());
                    Commons.gstamount = Double.parseDouble(combo_details_response_model.getRestaurantDetails().get(0).getStGstCommission());
                    set_price_details_combo(1);
                    if (flag==3){
                        start_time_lunch_list = Cake_rest_timings.getBakery_start();
                        end_time_lunch_list = Cake_rest_timings.getBakery_end();
                        start_time_dinner_list = "";
                        end_time_dinner_list ="";
                        app_openstatus = Cake_res_open_status.getOpenstatus();
                    }else {
                        start_time_lunch_list = combo_details_response_model.getRestaurantTimings().getLunchStart();
                        end_time_lunch_list = combo_details_response_model.getRestaurantTimings().getLunchEnd();
                        start_time_dinner_list = combo_details_response_model.getRestaurantTimings().getDinnerStart();
                        end_time_dinner_list = combo_details_response_model.getRestaurantTimings().getDinnerEnd();
                        app_openstatus = combo_details_response_model.getOpen_close_status().getOpenstatus();
                    }
                    try {
                        setimagefortdto(0);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (flag!=4) {
                        add_item.setVisibility(View.VISIBLE);
                        final boolean status = lOcaldbNew.checkIfCartContainsSomeOtherRestaurantData(combo_details_response_model.getRestaurantDetails().get(0).getInRestaurantId());
                        if (!status) {
                            if (flag==1) {
                                lOcaldbNew.deletecart(Combo_Details.getInDishId());
                                additem(Combo_Details, combo_details_response_model);
                            }
                            if (flag==0) {
                                lOcaldbNew.deletecart(items.getIn_dish_id());
                                additem_meal(items, combo_details_response_model);
                            }
                            if (flag==3) {
                                lOcaldbNew.deletecart(Cake_Details.getInDishId());
                                additem_Cake(Cake_Details, combo_details_response_model);

                            }
                            other_Function();
                        } else {
                            delete_cart_dialog(combo_details_response_model);
                        }
                    }
                    add_item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Commons.restname = combo_details_response_model.getRestaurantDetails().get(0).getStRestaurantName();
                            Commons.restadd = combo_details_response_model.getRestaurantDetails().get(0).getStreetAddress();
                            Commons.restsuburb = combo_details_response_model.getRestaurantDetails().get(0).getStSuburb();
                            Intent in = new Intent(MealCombo_Checkout_Activity.this, RestarantDetails.class);
                            in.putExtra("additem",true);
                            in.putExtra("res_id", combo_details_response_model.getRestaurantDetails().get(0).getInRestaurantId());
                            in.putExtra("res_name", combo_details_response_model.getRestaurantDetails().get(0).getStRestaurantName());
                            in.putExtra("res_image", combo_details_response_model.getRestaurantDetails().get(0).getRestaurantCoverImage());
                            in.putExtra("res_logo_image", combo_details_response_model.getRestaurantDetails().get(0).getRestaurantLogoImage());
                            in.putExtra("res_add", combo_details_response_model.getRestaurantDetails().get(0).getStStreetAddress());
                            in.putExtra("res_minorder", combo_details_response_model.getRestaurantDetails().get(0).getStMinOrder());
                            in.putExtra("res_rating", combo_details_response_model.getRestaurantDetails().get(0).getAvgRating());
                            in.putExtra("res_reviews", combo_details_response_model.getRestaurantDetails().get(0).getCountRating());
                            in.putExtra("Suburb", combo_details_response_model.getRestaurantDetails().get(0).getStSuburb());
                            in.putExtra("postcode", "");
                            in.putExtra("app_openstatus", combo_details_response_model.getOpen_close_status().getOpenstatus());
                            in.putExtra("menu_id", "M1001");
                            in.putExtra("nav_type", "3");
                            in.putExtra("extra_model", transform(combo_details_response_model));
                                in.putExtra("start_time_lunch", combo_details_response_model.getRestaurantTimings().getLunchStart());
                                in.putExtra("end_time_lunch", combo_details_response_model.getRestaurantTimings().getLunchEnd());
                                in.putExtra("start_time_dinner", combo_details_response_model.getRestaurantTimings().getDinnerStart());
                                in.putExtra("end_time_dinner", combo_details_response_model.getRestaurantTimings().getDinnerEnd());
                            Commons.gst_commision = combo_details_response_model.getRestaurantDetails().get(0).getStGstCommission();
                            if (!(combo_details_response_model.getRestaurantDetails().get(0).getStMinOrder() == null)) {

                                Commons.min_order_rest = Double.parseDouble(combo_details_response_model.getRestaurantDetails().get(0).getStMinOrder());
                            }
                            Commons.flag_for_hta = "7";
                            startActivityForResult(in, 99);
                            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        }
                    });

                    apply.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!apply.getText().toString().equalsIgnoreCase("Remove")) {
                                if (mCouponItems != null && mCouponItems.size() > 0) {
                                    add_coupon_dialog(mCouponItems);
                                }  else if (!GeneralUtil.isStringEmpty(Commons.restaurant_id)) {
                                    getCouponList(Commons.restaurant_id);
                                }
                            }else {
                                add_coupon_txt.setText("Apply Coupon ");
                                apply.setText("Apply");
                                add_coupon_flag = false;
                                ArrayList<Cart_Model.Cart_Details> cd1 =
                                        lOcaldbNew.getCart_Details();
                                setPriceDetails(cd1);
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<Combo_Details_Response_Model> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void other_Function() {
        cd = lOcaldbNew.getCart_Details();
        for (int i = 0; i < cd.size(); i++) {
            if (flag==1) {
                if (cd.get(i).getIn_dish_id().equalsIgnoreCase(Combo_Details.getInDishId())) {
                    set_price_details_combo(Integer.parseInt(cd.get(i).getQuantity()));
                } else {
                    MyCartSectionAdapter_New myCartAdapter = new MyCartSectionAdapter_New(1, MealCombo_Checkout_Activity.this, cd, Combo_Details.getInDishId());
                    recyclerView.setAdapter(myCartAdapter);
                    myCartAdapter.shouldShowHeadersForEmptySections(true);
                    myCartAdapter.notifyItemInserted(0);
                    myCartAdapter.notifyDataSetChanged();
                }
            }
            if (flag==0) {
                if (cd.get(i).getIn_dish_id().equalsIgnoreCase(items.getIn_dish_id())) {
                    set_price_details_combo(Integer.parseInt(cd.get(i).getQuantity()));
                } else {
                    MyCartSectionAdapter_New myCartAdapter = new MyCartSectionAdapter_New(1, MealCombo_Checkout_Activity.this, cd, items.getIn_dish_id());
                    recyclerView.setAdapter(myCartAdapter);
                    myCartAdapter.shouldShowHeadersForEmptySections(true);
                    myCartAdapter.notifyItemInserted(0);
                    myCartAdapter.notifyDataSetChanged();
                }
            }
            if (flag==3) {
                if (cd.get(i).getIn_dish_id().equalsIgnoreCase(Cake_Details.getInDishId())) {
                    set_price_details_combo(Integer.parseInt(cd.get(i).getQuantity()));
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    MyCartSectionAdapter_New myCartAdapter = new MyCartSectionAdapter_New(1, MealCombo_Checkout_Activity.this, cd, Cake_Details.getInDishId());
                    recyclerView.setAdapter(myCartAdapter);
                    myCartAdapter.shouldShowHeadersForEmptySections(true);
                    myCartAdapter.notifyItemInserted(0);
                    myCartAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void delete_cart_dialog(Combo_Details_Response_Model combo_details_response_model) {
        final BottomSheetDialog dialog_msg = new BottomSheetDialog(MealCombo_Checkout_Activity.this, R.style.SheetDialog);
        Objects.requireNonNull(dialog_msg.getWindow())
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_msg.setContentView(R.layout.layout_discard_cart);
        //TextView msg = (TextView) dialog_msg.findViewById(R.id.warning);
        TextView yes = (TextView) dialog_msg.findViewById(R.id.txt_yes);
        TextView no = (TextView) dialog_msg.findViewById(R.id.txt_no);
        ImageView imgClose = dialog_msg.findViewById(R.id.img_close);
        //msg.setText(
        //    "Your Cart has item(s) from other resturants.Do you want to discard it and add item(s) from this resturants?");
        if (no != null) {
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_msg.dismiss();
                    Commons.back_button_transition(MealCombo_Checkout_Activity.this);
                }
            });
        }

        if (imgClose != null) {
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    dialog_msg.dismiss();
                    Commons.back_button_transition(MealCombo_Checkout_Activity.this);
                }
            });
        }
        if (yes != null) {
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_msg.dismiss();
                    if (flag==1) {
                        lOcaldbNew.deleteCart();
                        lOcaldbNew.deletecart(Combo_Details.getInDishId());
                        additem(Combo_Details, combo_details_response_model);
                    }
                    if (flag==0) {
                        lOcaldbNew.deleteCart();
                        lOcaldbNew.deletecart(items.getIn_dish_id());
                        additem_meal(items, combo_details_response_model);
                    }
                    if (flag==3) {
                        lOcaldbNew.deleteCart();
                        lOcaldbNew.deletecart(Cake_Details.getInDishId());
                        additem_Cake(Cake_Details, combo_details_response_model);

                    }
                    other_Function();
                }
            });
        }
        dialog_msg.show();
    }

    private void additem_Cake(Cakes_Model.Cake dishdetail, Combo_Details_Response_Model combo_details_response_model) {

            String menupice;
            if (dishdetail.getStPrice().get(0).getMenuPrice().contains(",")) {
                menupice = dishdetail.getStPrice().get(0).getMenuPrice().replace(",", "");
            } else {
                menupice = dishdetail.getStPrice().get(0).getMenuPrice();
            }
            float dvp = Float.parseFloat(menupice);
            ArrayList<Cart_Model.Cart_Details> cd =
                    lOcaldbNew.checkIfCartContainsSameDishWithPreference(dishdetail.getInDishId(), "");
            if (cd.size() > 0) {
                int sqty = Integer.parseInt(cd.get(0).getQuantity());
                sqty = sqty + 1;
                lOcaldbNew.deletecart(dishdetail.getInDishId());
                lOcaldbNew.insertTable_dish_info(Commons.restaurant_id, dishdetail.getInDishId(), dishdetail.getStDishName(),
                        dishdetail.getStPrice().get(0).getPriceItem(), dvp,
                        "1", dishdetail.getNonVegStatus(), dishdetail.getCakeFlg(), sqty, max_qty_per_user,"", 0,
                        combo_details_response_model.getRestaurantDetails().get(0).getStRestaurantName(),
                        combo_details_response_model.getRestaurantDetails().get(0).getStreetAddress(),
                        combo_details_response_model.getRestaurantDetails().get(0).getStSuburb(),
                        "",
                        combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getAmount(),
                        combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getDesc(),
                        combo_details_response_model.getRestaurantDetails().get(0).getRestaurantLogoImage());
            }else {
                lOcaldbNew.insertTable_dish_info(Commons.restaurant_id, dishdetail.getInDishId(), dishdetail.getStDishName(),
                        dishdetail.getStPrice().get(0).getPriceItem(), dvp,
                        "1", dishdetail.getNonVegStatus(), dishdetail.getCakeFlg(), 1,max_qty_per_user, "", 0,
                        combo_details_response_model.getRestaurantDetails().get(0).getStRestaurantName(),
                        combo_details_response_model.getRestaurantDetails().get(0).getStreetAddress(),
                        combo_details_response_model.getRestaurantDetails().get(0).getStSuburb(),
                        "",
                        combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getAmount(),
                        combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getDesc(),
                        combo_details_response_model.getRestaurantDetails().get(0).getRestaurantLogoImage());
            }
        ArrayList<Cart_Model.Cart_Details> cd1 =
                lOcaldbNew.getCart_Details();
        setPriceDetails(cd1);

    }

    private void additem(Combo_Response_Model.combos.Itemlist dishdetail,Combo_Details_Response_Model combo_details_response_model) {

            String menupice;
            if (dishdetail.getStPrice().get(0).getMenuPrice().contains(",")) {
                menupice = dishdetail.getStPrice().get(0).getMenuPrice().replace(",", "");
            } else {
                menupice = dishdetail.getStPrice().get(0).getMenuPrice();
            }
            float dvp = Float.parseFloat(menupice);
            ArrayList<Cart_Model.Cart_Details> cd =
                    lOcaldbNew.checkIfCartContainsSameDishWithPreference(dishdetail.getInDishId(), "");
            if (cd.size() > 0) {
                int sqty = Integer.parseInt(cd.get(0).getQuantity());
                sqty = sqty + 1;
                lOcaldbNew.deletecart(dishdetail.getInDishId());
                lOcaldbNew.insertTable_dish_info(Commons.restaurant_id, dishdetail.getInDishId(), dishdetail.getStDishName(),
                        dishdetail.getStPrice().get(0).getPriceItem(), dvp,
                        "1", dishdetail.getNonVegStatus(), dishdetail.getCake_flg(), sqty,max_qty_per_user, "", 0,
                        combo_details_response_model.getRestaurantDetails().get(0).getStRestaurantName(),
                        combo_details_response_model.getRestaurantDetails().get(0).getStreetAddress(),
                        combo_details_response_model.getRestaurantDetails().get(0).getStSuburb(),
                        "",
                        combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getAmount(),
                        combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getDesc(),
                        combo_details_response_model.getRestaurantDetails().get(0).getRestaurantLogoImage());
            } else {
                lOcaldbNew.insertTable_dish_info(Commons.restaurant_id, dishdetail.getInDishId(), dishdetail.getStDishName(),
                        dishdetail.getStPrice().get(0).getPriceItem(), dvp,
                        "1", dishdetail.getNonVegStatus(), dishdetail.getCake_flg(), 1,max_qty_per_user, "", 0,
                        combo_details_response_model.getRestaurantDetails().get(0).getStRestaurantName(),
                        combo_details_response_model.getRestaurantDetails().get(0).getStreetAddress(),
                        combo_details_response_model.getRestaurantDetails().get(0).getStSuburb(),
                        "",
                        combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getAmount(),
                        combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getDesc(),
                        combo_details_response_model.getRestaurantDetails().get(0).getRestaurantLogoImage());
            }
            ArrayList<Cart_Model.Cart_Details> cd1 =
                    lOcaldbNew.getCart_Details();
            setPriceDetails(cd1);

    }
    private void additem_meal(ItemsItem dishdetail, Combo_Details_Response_Model combo_details_response_model) {
        String menupice;
        if (dishdetail.getStPrice().get(0).getMenuPrice().contains(",")) {
            menupice = dishdetail.getStPrice().get(0).getMenuPrice().replace(",", "");
        } else {
            menupice = dishdetail.getStPrice().get(0).getMenuPrice();
        }
        float dvp = Float.parseFloat(menupice);
        ArrayList<Cart_Model.Cart_Details> cd =
                lOcaldbNew.checkIfCartContainsSameDishWithPreference(dishdetail.getIn_dish_id(), "");
        if (cd.size() > 0) {
            int sqty = Integer.parseInt(cd.get(0).getQuantity());
            sqty = sqty + 1;
            lOcaldbNew.deletecart(dishdetail.getIn_dish_id());
            lOcaldbNew.insertTable_dish_info(Commons.restaurant_id, dishdetail.getIn_dish_id(), dishdetail.getName(),
                    dishdetail.getStPrice().get(0).getPriceItem(), dvp,
                    "1", dishdetail.getNon_veg_status(), dishdetail.getCake_flg(), sqty,max_qty_per_user, "", 0,
                    combo_details_response_model.getRestaurantDetails().get(0).getStRestaurantName(),
                    combo_details_response_model.getRestaurantDetails().get(0).getStreetAddress(),
                    combo_details_response_model.getRestaurantDetails().get(0).getStSuburb(),
                    "",
                    combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getAmount(),
                    combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getDesc(),
                    combo_details_response_model.getRestaurantDetails().get(0).getRestaurantLogoImage());
        } else {
            lOcaldbNew.insertTable_dish_info(Commons.restaurant_id, dishdetail.getIn_dish_id(), dishdetail.getName(),
                    dishdetail.getStPrice().get(0).getPriceItem(), dvp,
                    "1", dishdetail.getNon_veg_status(), dishdetail.getCake_flg(), 1, max_qty_per_user,"", 0,
                    combo_details_response_model.getRestaurantDetails().get(0).getStRestaurantName(),
                    combo_details_response_model.getRestaurantDetails().get(0).getStreetAddress(),
                    combo_details_response_model.getRestaurantDetails().get(0).getStSuburb(),
                    "",
                    combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getAmount(),
                    combo_details_response_model.getRestaurantDetails().get(0).getRestcharge().getDesc(),
                    combo_details_response_model.getRestaurantDetails().get(0).getRestaurantLogoImage());
        }
        ArrayList<Cart_Model.Cart_Details> cd1 =
                lOcaldbNew.getCart_Details();
        setPriceDetails(cd1);

    }

    private RestaurantModel transform(Combo_Details_Response_Model restaurant_model) {
        String base_url_logo =
                "https://storage.googleapis.com/speedzy_data/resources/restaurantlogo/";
        RestaurantModel obj = new RestaurantModel();
        obj.setmResPic(base_url_logo + "big/" + restaurant_model.getRestaurantDetails().get(0).getRestaurantLogoImage());
        obj.setmResName(restaurant_model.getRestaurantDetails().get(0).getStRestaurantName());
        obj.setmRating(restaurant_model.getRestaurantDetails().get(0).getAvgRating() + " Rating");
        obj.setmRatingBar(Float.parseFloat(restaurant_model.getRestaurantDetails().get(0).getAvgRating()));
        obj.setmMsg("");

        if (restaurant_model.getOpen_close_status().getOpenstatus().toString().equalsIgnoreCase("0")) {
            obj.setmOpenClose("Close");
        } else if (restaurant_model.getOpen_close_status().getOpenstatus().toString().equalsIgnoreCase("1")) {
            obj.setmOpenClose("Open");
        }
        obj.setmMinOrder("Min ₹" + restaurant_model.getRestaurantDetails().get(0).getStMinOrder());
            obj.setmMenu("Menu");
        return obj;
    }

    private void call_deliverychargeApi() {
        double distance=Commons.getDistance(lati,longi,mySharedPrefrencesData.getSelectCity_latitude(this),
                mySharedPrefrencesData.getSelectCity_longitude(this));
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<Delivery_Charge_Response_Model> call = methods.get_delivery_charge(mySharedPrefrencesData.getSelectCity_longitude(this),
                mySharedPrefrencesData.getSelectCity_latitude(this), mySharedPrefrencesData.getSelectAddress_ID(this),
                mySharedPrefrencesData.getSelectCity(this), String.valueOf(totalsum),
                mySharedPrefrencesData.getUser_Id(this), Commons.restaurant_id,cake_flag,Commons.menu_id, String.valueOf(distance));
        Log.d("url", "url=" + call.request().url().toString());
        call.enqueue(new Callback<Delivery_Charge_Response_Model>() {
            @Override
            public void onResponse(Call<Delivery_Charge_Response_Model> call, Response<Delivery_Charge_Response_Model> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                Delivery_Charge_Response_Model delivery_charge_response_model = response.body();
                if (delivery_charge_response_model.getStatus().equalsIgnoreCase("Success")) {
                    delivery_charge = delivery_charge_response_model.getDeliverycharge().toString();

                    if (add_coupon_flag) {
                        call_voucher_data(coupon_code, null);
                        AddCoupon();
                        discount_lay.setVisibility(View.VISIBLE);
                        apply.setVisibility(View.VISIBLE);
                        coupon_amount = String.format("%.2f", max_coupon_discount);
                        add_coupon_txt.setText("Coupon Applied (" + coupon_code+" )" );
                        apply.setText("Remove");
                        discount_text.setText("-₹" + coupon_amount);
                    } else {
                        max_coupon_discount=0.0;
                        discount_lay.setVisibility(View.GONE);
                        coupon_amount = String.format("%.2f", max_coupon_discount);

                        apply.setVisibility(View.VISIBLE);
                        add_coupon_txt.setText("Apply Coupon ");
                        add_coupon_txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
                                0);
                    }
                    NumberFormat f = NumberFormat.getInstance();
                    double res_charge = 0;
                    try {
                        res_charge = f.parse(cd.get(0).getSt_rest_charge_amount()).doubleValue();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    grandtotalamount = grand_total + Double.parseDouble(delivery_charge)
                            +res_charge- max_coupon_discount;
                    grandtotal.setText("₹" + String.format("%.2f", grandtotalamount));

                    if (Commons.wallet_static_value != null) {
                        if (grandtotalamount > Double.parseDouble(Commons.wallet_static_value)) {
                            walletvalue = Double.parseDouble(Commons.wallet_static_value);
                        } else {
                            walletvalue = grandtotalamount;
                        }
                    }
                    wallet_text.setText("-₹" + walletvalue);
                    grandtotalamount = grandtotalamount - walletvalue;
                    grandtotal.setText("₹" + String.format("%.2f", grandtotalamount));
                    String walletrupees = wallet_text.getText().toString();
                    walletrupees = walletrupees.replace("-₹", "");
                    Commons.wallet = String.valueOf(walletrupees);
                    deliverycharge_cart.setText("₹ " + delivery_charge);
                    Commons.delivery_charge = delivery_charge;
                    Commons.order_amount = String.valueOf(grand_total);
                    Commons.total_amount = String.format("%.2f", grandtotalamount);
                    Commons.coupon_id = coupon_id;
                    Commons.dicount_val = coupon_amount;
                    Commons.coupon_amount = coupon_amount;
                    Commons.voucherid = coupon_code;
                    Commons.vaoucheramount = coupon_amount;

                }

            }

            @Override
            public void onFailure(Call<Delivery_Charge_Response_Model> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void statusbar_bg(int color) {
        collapsingToolbarLayout.setContentScrimColor(getResources().getColor(color));
        collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(color));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Commons.back_button_transition(MealCombo_Checkout_Activity.this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(MealCombo_Checkout_Activity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        isActivityRunning = false;
    }
    public void callapi() {
        if (mySharedPrefrencesData == null) {
            mySharedPrefrencesData = new MySharedPrefrencesData();
        }
        if (methods == null) {
            methods =
                    API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        }
        city_id = mySharedPrefrencesData.getSelectCity(this);
        methods.get_home(city_id, mySharedPrefrencesData.getVeg_Flag(this))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<HomePageAppConfigResponse>() {
                    @Override public void onSubscribe(Disposable d) {
                        mDisposable = d;
                    }

                    @Override public void onNext(HomePageAppConfigResponse home_response_model) {
                        if (home_response_model != null) {
                            app_openstatus = home_response_model.getAppOpenstatus();
                            mSpeedzyOperationsClosedModel = SpeedzyOperationsClosedModel.transform(
                                    home_response_model.getAppclosestatus(),
                                    Integer.parseInt(home_response_model.getAppOpenstatus()),
                                    Integer.parseInt(home_response_model.getAppCloseType()));
                        }
                    }

                    @Override public void onError(Throwable e) {

                    }

                    @Override public void onComplete() {

                    }
                });
    }

    @Override public void onResume() {
        super.onResume();
        isActivityRunning=true;
        callapi();
    }
    @Override protected void onDestroy() {
        super.onDestroy();
        GeneralUtil.safelyDispose(mDisposable);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            // check if the request code is same as what is passed  here it is 2
            if (requestCode == 1 ) {
                if (mySharedPrefrencesData.getLocation(this).equalsIgnoreCase("")||
                        mySharedPrefrencesData.getLocation(this) == null) {
                    delivery_frag.setVisibility(View.GONE);
                    add_address.setVisibility(View.VISIBLE);
                }else {
                    delivery_frag.setVisibility(View.VISIBLE);
                    add_address.setVisibility(View.GONE);
                    String message = data.getStringExtra("SELECTED_LOCATION");
                    selected_location.setText(message);
                }
                int qty = Integer.parseInt(itemqty.getText().toString());
                set_price_details_combo(qty);
            }if (requestCode == 11 && resultCode == RESULT_OK) {
                payment_status(data);
            } else if (requestCode == 22 && resultCode == RESULT_OK) {
                payment_status(data);
            } else if (requestCode == 33 && resultCode == RESULT_OK) {
                payment_status(data);
            }else {
                if(resultCode == RESULT_OK){
                    cd = lOcaldbNew.getCart_Details();
                    for (int i = 0; i < cd.size(); i++) {
                        if (flag==1) {
                            if (cd.get(i).getIn_dish_id().equalsIgnoreCase(Combo_Details.getInDishId())) {
                                set_price_details_combo(Integer.parseInt(cd.get(i).getQuantity()));
                            } else {
                                MyCartSectionAdapter_New myCartAdapter = new MyCartSectionAdapter_New(1, MealCombo_Checkout_Activity.this, cd, Combo_Details.getInDishId());
                                recyclerView.setAdapter(myCartAdapter);
                                myCartAdapter.shouldShowHeadersForEmptySections(true);
                                myCartAdapter.notifyItemInserted(0);
                                myCartAdapter.notifyDataSetChanged();
                            }
                        }
                        if (flag==0) {
                            if (cd.get(i).getIn_dish_id().equalsIgnoreCase(items.getIn_dish_id())) {
                                set_price_details_combo(Integer.parseInt(cd.get(i).getQuantity()));
                            } else {
                                MyCartSectionAdapter_New myCartAdapter = new MyCartSectionAdapter_New(1, MealCombo_Checkout_Activity.this, cd, items.getIn_dish_id());
                                recyclerView.setAdapter(myCartAdapter);
                                myCartAdapter.shouldShowHeadersForEmptySections(true);
                                myCartAdapter.notifyItemInserted(0);
                                myCartAdapter.notifyDataSetChanged();
                            }
                        }
                        if (flag==3) {
                            if (cd.get(i).getIn_dish_id().equalsIgnoreCase(Cake_Details.getInDishId())) {
                                set_price_details_combo(Integer.parseInt(cd.get(i).getQuantity()));
                            } else {
                                MyCartSectionAdapter_New myCartAdapter = new MyCartSectionAdapter_New(1, MealCombo_Checkout_Activity.this, cd, Cake_Details.getInDishId());
                                recyclerView.setAdapter(myCartAdapter);
                                myCartAdapter.shouldShowHeadersForEmptySections(true);
                                myCartAdapter.notifyItemInserted(0);
                                myCartAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
                if (resultCode == RESULT_CANCELED) {
                    //Do nothing?
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    private void payment_status(Intent data) {
        Bundle bundle=data.getExtras();
        for (String key : bundle.keySet()) {
            Log.e("UPI_txn=>", key + ":" + bundle.get(key));
        }
        String orderid=data.getStringExtra("txnRef");
        mOrderUPIDataModel=new OrderUPIDataModel();
        mOrderUPIDataModel.setOrderNumber(orderid);
        Log.e("UPI_txn", orderid);
        Log.e("UPI_txn", data.getStringExtra("Status"));
        if (data.getStringExtra("Status").equalsIgnoreCase("SUCCESS")) {
            apicall();
        }else {
            Toast.makeText(this, "Payment Failed", Toast.LENGTH_SHORT).show();
        }
    }
    private void apicall() {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<StorepackagerModel> call = methods.changepayment(mOrderUPIDataModel.getOrderNumber(),Commons.menu_id);
        Log.d("url", "url=" + call.request().url().toString());
        call.enqueue(new Callback<StorepackagerModel>() {
            @Override
            public void onResponse(Call<StorepackagerModel> call, Response<StorepackagerModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + statusCode);
                Log.d("respones", "" + response);
                StorepackagerModel deviceUpdateModel = response.body();
                if (deviceUpdateModel.getStatus().equalsIgnoreCase("Success")) {
                    goToOrderSuccessPage();
                }
            }

            @Override
            public void onFailure(Call<StorepackagerModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet",
                        Toast.LENGTH_LONG).show();
            }
        });
    }
    private void goToOrderSuccessPage() {
        Toast.makeText(this, "Payment Successful", Toast.LENGTH_SHORT).show();
        ActivityOrderDataModel activityOrderDataModel = new ActivityOrderDataModel();
        activityOrderDataModel.setOrderId(mOrderUPIDataModel.getOrderNumber());
        finish();
        startActivity(ActivityOrderSuccess.newIntent(this, activityOrderDataModel));
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
