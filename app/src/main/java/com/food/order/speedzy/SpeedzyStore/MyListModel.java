package com.food.order.speedzy.SpeedzyStore;

import java.io.Serializable;

public class MyListModel implements Serializable {
    private String quantity;

    public boolean isAdd_item() {
        return add_item;
    }

    public void setAdd_item(boolean add_item) {
        this.add_item = add_item;
    }

    private boolean add_item=true;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    private String item_name;
}
