package com.food.order.speedzy.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by Sujata Mohanty.
 */


public class Product_Sans extends AppCompatTextView {
    public Product_Sans(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public Product_Sans(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public Product_Sans(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("ProductSansRegular.ttf", context);
        setTypeface(customFont);
    }
}
