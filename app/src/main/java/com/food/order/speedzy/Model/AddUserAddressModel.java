package com.food.order.speedzy.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Sujata Mohanty.
 */


public class AddUserAddressModel implements Serializable {
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @SerializedName("status")
    private String status;

    public AddUserAddressModel.info getInfo() {
        return info;
    }

    public void setInfo(AddUserAddressModel.info info) {
        this.info = info;
    }

    @SerializedName("info")
    private info info;

    @SerializedName("msg")
    private String msg;
    public class info implements Serializable {
        private String address_id;

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        private String area;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        private String name;

        public String getAddress_id() {
            return address_id;
        }

        public void setAddress_id(String address_id) {
            this.address_id = address_id;
        }

        public String getComplete_address() {
            return complete_address;
        }

        public void setComplete_address(String complete_address) {
            this.complete_address = complete_address;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getAddional_comments() {
            return addional_comments;
        }

        public void setAddional_comments(String addional_comments) {
            this.addional_comments = addional_comments;
        }

        public String getAddress_type() {
            return address_type;
        }

        public void setAddress_type(String address_type) {
            this.address_type = address_type;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getIsdefault() {
            return isdefault;
        }

        public void setIsdefault(String isdefault) {
            this.isdefault = isdefault;
        }

        public String getOther_name() {
            return other_name;
        }

        public void setOther_name(String other_name) {
            this.other_name = other_name;
        }

        private String complete_address;
        private String state;

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        private String mobile;
        private String district;
        private String pincode;
        private String landmark;
        private String addional_comments;
        private String address_type;
        private String longitude;
        private String latitude;
        private String user_id;
        private String isdefault;
        private String other_name;
    }
}
