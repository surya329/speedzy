package com.food.order.speedzy.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.appbar.AppBarLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import com.food.order.speedzy.Adapter.LikedResturantsActivityAdapter1;
import com.food.order.speedzy.Model.Restaurant_model;
import com.food.order.speedzy.Model.RestaurantsListModelNew;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.LoaderDiloag;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.root.BaseActivity;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Sujata Mohanty.
 */

public class Resturant_SearchActivity extends BaseActivity {
  private Toolbar toolbar;
  AppBarLayout appbar;

  int page = 1;
  RecyclerView recyclerView;
  LinearLayoutManager lm;
  LikedResturantsActivityAdapter1 hotelsAdapter;
  String restname_letter;
  RestaurantsListModelNew restaurantsListModelNew = new RestaurantsListModelNew();
  List<Restaurant_model> restaurantModelList = new ArrayList<>();
  LoaderDiloag loaderDiloag;
  EditText searchrest;
  String restname;
  boolean isloaderloaded = true;
  RelativeLayout norestlayout;

  String deliveryfee;
  String minimumorder;

  ArrayList<String> cuisinelist = new ArrayList<>();
  boolean isdatafilter = true;
  boolean isrestaurantlist = true;
  ArrayList<String> filtercuisineArrayList = new ArrayList<>();
  String ratingflag, booktableflag = "", bookroomflag = "";
  MySharedPrefrencesData mySharedPrefrencesData;
  String Veg_Flag;
  String start_time_lunch_list, end_time_lunch_list, start_time_dinner_list, end_time_dinner_list,
      app_openstatus;
  private ReactiveLocationProvider locationProvider;
  private Location location;
  private LatLng myLocationLatlng;
  private double latitude = 0.0;
  private double longitude = 0.0;
  public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_resturant__search);
    mySharedPrefrencesData = new MySharedPrefrencesData();
    Veg_Flag = mySharedPrefrencesData.getVeg_Flag(this);

    toolbar = (Toolbar) findViewById(R.id.toolbar);
    appbar = (AppBarLayout) findViewById(R.id.appbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    if (Veg_Flag.equalsIgnoreCase("1")) {
      statusbar_bg(R.color.red);
    } else {
      statusbar_bg(R.color.red);
    }
    app_openstatus = getIntent().getStringExtra("app_openstatus");
    start_time_lunch_list = getIntent().getStringExtra("start_time_lunch");
    end_time_lunch_list = getIntent().getStringExtra("end_time_lunch");
    start_time_dinner_list = getIntent().getStringExtra("start_time_dinner");
    end_time_dinner_list = getIntent().getStringExtra("end_time_dinner");

    norestlayout = (RelativeLayout) findViewById(R.id.norestlay);
    searchrest = (EditText) findViewById(R.id.searchrest);
    Commons.flag_for_hta = "7";
    recyclerView = (RecyclerView) findViewById(R.id.recyclerview_hotels);
    loaderDiloag = new LoaderDiloag(this);
    lm = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(lm);
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    recyclerView.addItemDecoration(new DividerItemDecoration(this, 0));
    recyclerView.setHasFixedSize(true);
    //        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
    recyclerView.setNestedScrollingEnabled(false);
    initLocationProvider();
    searchrest.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        restname = searchrest.getText().toString();
        // isloaderloaded=false;
        // restaurantList();
        restaurantModelList = getSelectedRestlist(restname);
        if (restaurantModelList.size() > 0) {
          norestlayout.setVisibility(View.GONE);
          recyclerView.setVisibility(View.VISIBLE);
          hotelsAdapter = new LikedResturantsActivityAdapter1(Resturant_SearchActivity.this,
              restaurantModelList, start_time_lunch_list, end_time_lunch_list,
              start_time_dinner_list, end_time_dinner_list, app_openstatus);
          recyclerView.setNestedScrollingEnabled(false);
          recyclerView.setAdapter(hotelsAdapter);
          hotelsAdapter.notifyDataSetChanged();
        } else {
          norestlayout.setVisibility(View.VISIBLE);
          recyclerView.setVisibility(View.GONE);
        }
      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });
  }

  @Override protected void onStart() {
    super.onStart();
  }

  private ArrayList<Restaurant_model> getSelectedRestlist(String restcharacter) {
    ArrayList<Restaurant_model> restlistnew = new ArrayList<>();
    System.out.print("restcharacter=" + restcharacter);
    if (!(Commons.restlist == null)) {

      for (int i = 0; i < Commons.restlist.size(); i++) {
        if (Pattern.compile(Pattern.quote(restcharacter), Pattern.CASE_INSENSITIVE)
            .matcher(Commons.restlist.get(i).getStRestaurantName())
            .find()) {
          restlistnew.add(Commons.restlist.get(i));
        }
      }
    }
    return restlistnew;
  }

  private void restaurantList(LatLng myLocation) {
    if (myLocation != null) {
      latitude = myLocation.latitude;
      longitude = myLocation.longitude;
    }
    Apimethods methods =
        API_Call_Retrofit.changeApiBaseUrl(Resturant_SearchActivity.this).create(Apimethods.class);
    Call<RestaurantsListModelNew> call =
        methods.setRestaurant(restname, String.valueOf(latitude), String.valueOf(longitude), 0,
            page,
            ratingflag, deliveryfee, minimumorder, cuisinelist, booktableflag, bookroomflag);

    Log.e("url", "url=" + call.request().url().toString());
    //Log.d("Latitude", Commons.latitude_str);
    //Log.d("Longitude", Commons.longitude_str);
    if (isloaderloaded == true) {
      loaderDiloag.displayDiloag();
    }
    call.enqueue(new Callback<RestaurantsListModelNew>() {
      @Override
      public void onResponse(@NonNull Call<RestaurantsListModelNew> call,
          @NonNull retrofit2.Response<RestaurantsListModelNew> response) {
        loaderDiloag.dismissDiloag();
        restaurantModelList.clear();
        restaurantsListModelNew = response.body();
        restaurantModelList.addAll(restaurantsListModelNew.getRestaurants());
        if (isrestaurantlist == true) {

          Commons.restlist = new ArrayList<Restaurant_model>();
          Commons.restlist = restaurantsListModelNew.getRestaurants();
        }

        if (isdatafilter == true) {
          filtercuisineArrayList = restaurantsListModelNew.getFilter_cuisine();
        }

        if (restaurantModelList.size() > 0) {
          norestlayout.setVisibility(View.GONE);
          recyclerView.setVisibility(View.VISIBLE);
          hotelsAdapter = new LikedResturantsActivityAdapter1(Resturant_SearchActivity.this,
              restaurantModelList, start_time_lunch_list, end_time_lunch_list,
              start_time_dinner_list, end_time_dinner_list, app_openstatus);
          recyclerView.setNestedScrollingEnabled(false);
          recyclerView.setAdapter(hotelsAdapter);
          hotelsAdapter.notifyDataSetChanged();
        } else {
          norestlayout.setVisibility(View.VISIBLE);
          recyclerView.setVisibility(View.GONE);
        }
      }

      @Override
      public void onFailure(@NonNull Call<RestaurantsListModelNew> call, @NonNull Throwable t) {
        t.printStackTrace();
        loaderDiloag.dismissDiloag();
        //Toast.makeText(Resturant_SearchActivity.this,"internet not available..connect internet", Toast.LENGTH_SHORT).show();
      }
    });
  }

  // region Listeners
  private RecyclerView.OnScrollListener recyclerViewOnScrollListener =
      new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
          super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
          super.onScrolled(recyclerView, dx, dy);
          int visibleItemCount = lm.getChildCount();
          int totalItemCount = lm.getItemCount();
          int firstVisibleItemPosition = lm.findFirstVisibleItemPosition();
            /*if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {}*/
          if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
              && firstVisibleItemPosition >= 0) {
            page++;
            restaurantList(myLocationLatlng);
                /*if(restaurantsListModelNew.getRestaurants().size()<1){
                    oops.setVisibility(View.VISIBLE);
                }else {
                    oops.setVisibility(View.GONE);
                }*/

               /* if(page<=5) {
                    restaurantList();
                }

                if(page==5){

                    oops.setVisibility(View.VISIBLE);
                }*/

          }
        }
      };

  private void statusbar_bg(int color) {
    Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(getResources()
        .getColor(color)));
    appbar.setBackgroundDrawable(new ColorDrawable(getResources()
        .getColor(color)));
    if (android.os.Build.VERSION.SDK_INT >= 21) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
      getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case android.R.id.home:
        Commons.back_button_transition(Resturant_SearchActivity.this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    Commons.back_button_transition(Resturant_SearchActivity.this);
  }

  @SuppressLint("CheckResult") private void initLocationProvider() {
    locationProvider = new ReactiveLocationProvider(this);
    checkLocationPermission();
    locationProvider.getLastKnownLocation().subscribe(newLocation -> {
      location = newLocation;
      myLocationLatlng = new LatLng(location.getLatitude(), location.getLongitude());
      restaurantList(myLocationLatlng);
      showLocationInMap();
    });
  }

  private void checkLocationPermission() {
    if (ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
        Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
      if (ActivityCompat.shouldShowRequestPermissionRationale(this,
          Manifest.permission.ACCESS_FINE_LOCATION)) {
        if (android.os.Build.VERSION.SDK_INT > 23) {
          requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
              MY_PERMISSIONS_REQUEST_LOCATION);
        }
      } else {
        if (android.os.Build.VERSION.SDK_INT > 23) {
          requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
              MY_PERMISSIONS_REQUEST_LOCATION);
        }
      }
    }
  }

  @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
      if (permissions.length == 1) {
        LocationManager locationManager =
            (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
          // TODO: Consider calling
          //    ActivityCompat#requestPermissions
          // here to request the missing permissions, and then overriding
          //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
          //                                          int[] grantResults)
          // to handle the case where the user grants the permission. See the documentation
          // for ActivityCompat#requestPermissions for more details.
          return;
        }
        //if (locationManager != null) {
        //  location = locationManager.getLastKnownLocation(
        //      Objects.requireNonNull(locationManager).getBestProvider(criteria, false));
        //}
        //if (location != null) {
        //  Commons.latitude = location.getLatitude();
        //  Commons.longitude = location.getLongitude();
        //  Commons.latitude_str = String.valueOf(location.getLatitude());
        //  Commons.longitude_str = String.valueOf(location.getLongitude());
        //}
      }
    }
  }

  private void showLocationInMap() {
    checkLocationPermission();
    if (location != null) {
      Commons.latitude = location.getLatitude();
      Commons.longitude = location.getLongitude();
      Commons.latitude_str = String.valueOf(location.getLatitude());
      Commons.longitude_str = String.valueOf(location.getLongitude());
    }
  }
}
