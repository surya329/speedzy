package com.food.order.speedzy.root;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.facebook.accountkit.AccountKit;
import com.food.order.speedzy.BuildConfig;
import timber.log.Timber;

public class App extends Application {

  @Override
  public void onCreate() {
    super.onCreate();
    AccountKit.initialize(getApplicationContext());
    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    }

  }

}
