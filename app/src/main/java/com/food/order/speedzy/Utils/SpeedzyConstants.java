package com.food.order.speedzy.Utils;

import android.widget.Switch;

import androidx.annotation.IntDef;
import androidx.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SpeedzyConstants {
  public static final String NOTIFICATION_URI_RESOURCE = "android.resource://";
  public static final String Fruits = "Fruits";
  public static final String CAKE_MENU_ID = "M1007";
  public static final String SWEET_MENU_ID = "M1006";
  public static final String FRUITS_MENU_ID = "M1005";
  public static final String RESTURANT_MENU_ID = "M1001";
  public static final String MEDICINE_ORDER_ID = "M1027";
  public static final String GROCERY_ORDER_ID = "M1010";
  public static final String SELECTED_PICTURE_MODE_GALLERY = "gallery";
  public static final String SELECTED_PICTURE_MODE_CAMERA = "camera";
  public static final String DATE_DEFAULT_FORMAT = "yyyy-MM-dd";
  public static final String DATE_API_FORMAT = "yyyyMMdd";
  public static final String DATE_ILOHO_FORMAT = "EEE,dd MMM";
  public static final String DATE_DAY_FORMAT = "dd-MMM-yyyy";
  public static final String DATE_SPACE_FORMAT = "yyyy-MM-dd HH:mm:ss";
  public static final String APP_OPEN_STATUS_IS_OPENED = "1";
  public static final String APP_OPEN_STATUS_IS_CLOSED = "0";

  //Firebase
  public static final String FIREBASE_SPEEDZY_BUCKET = "gs://speedzy_data/";
  public static final String FIREBASE_BUCKET_URL =
      "https://firebasestorage.googleapis.com/v0/b/speedzy_data/o/";
  public static final String FIREBASE_MEDICINE = "medicine";
  public static final String FIREBASE_ORIGINAL = "original";
  public static final String FIREBASE_GROCERY = "grocery";
  public static final String FIREBASE_USER = "user";

  public static final String GROCERY_ORDER = "Grocery Order";
  public static final String SOCKET_URL = "http://165.22.212.1:8010/socket/websocket";
  public static final String DELIVERY_SERVER_BASE_URL = "http://34.93.26.196:4005/";
  public static final String MEDICINE_ORDER = "Medicine Order";
  public static final int DRIVER_BIKE_HEIGHT = 90;
  public static final int DRIVER_BIKE_WIDTH = 50;
  public static final String MAPS_KEY = "AIzaSyB5RxynUiyJ80JWO4C1pFcVey0p-jHM0PE";


  public static final String RIDE_SERVER_BASE_URL = "http://34.93.26.196:4010/";

  //Image Urls
  public static String STORAGE_BASE_BANNER_URL =
      "https://storage.googleapis.com/speedzy_data/resources/banner/big/";

  @IntDef({
      PaymentType.CARD, PaymentType.CASH_ON_DELIVERY
  }) @Retention(RetentionPolicy.SOURCE) public @interface PaymentType {
    int CARD = 1;
    int CASH_ON_DELIVERY = 2;
    int UPI = 4;
  }

  @IntDef({
      PaymentStatus.UNPAID, PaymentStatus.PAID
  }) @Retention(RetentionPolicy.SOURCE) public @interface PaymentStatus {
    int UNPAID = 0;
    int PAID = 1;
  }

  @IntDef({
      WalletType.ADMIN_ADDED, WalletType.REFERRAL, WalletType.ORDER_REFUND,
      WalletType.JOINING_BONUS, WalletType.CANCEL_REFUND, WalletType.ORDER_FOOD,
      WalletType.ORDER_CASHBACK, WalletType.CASHBACK_EXPIRED
  }) @Retention(RetentionPolicy.SOURCE) public @interface WalletType {
    int ADMIN_ADDED = 1;
    int REFERRAL = 2;
    int ORDER_REFUND = 3;
    int JOINING_BONUS = 4;
    int CANCEL_REFUND = 5;
    int ORDER_FOOD = 6;
    int ORDER_CASHBACK = 7;
    int CASHBACK_EXPIRED = 8;
  }

  @IntDef({
      AppOpenStatus.CLOSED, AppOpenStatus.OPEN
  }) @Retention(RetentionPolicy.SOURCE) public @interface AppOpenStatus {
    int CLOSED = 0;
    int OPEN = 1;
  }

  @IntDef({
      AppCloseStatus.RESTURANT, AppCloseStatus.OTHERS
  }) @Retention(RetentionPolicy.SOURCE) public @interface AppCloseStatus {
    int RESTURANT = 1;
    int OTHERS = 2;
  }

  @IntDef({
      NetworkSignalType.UNDEFINED, NetworkSignalType.POOR, NetworkSignalType.GOOD,
      NetworkSignalType.EXCELLENT
  }) @Retention(RetentionPolicy.SOURCE) public @interface NetworkSignalType {
    int UNDEFINED = -1;
    int POOR = 0;
    int GOOD = 1;
    int EXCELLENT = 2;
  }

  @IntDef({
      OrderStatusType.ACCEPTED, OrderStatusType.CANCELLED,
      OrderStatusType.ORDER_CANCELLED, OrderStatusType.DELIVERY_PARTNER_ASSIGNED,
      OrderStatusType.ORDER_READY_FOR_PICKUP, OrderStatusType.ORDER_PICKED_UP,
      OrderStatusType.ORDER_CANCEL, OrderStatusType.ORDER_DELIVERED,
      OrderStatusType.ORDER_RECIEVED, OrderStatusType.DELIVERY_PARTNER_WAITING_AT_RESTURANT,
      OrderStatusType.WE_CANT_REACH_YOU, OrderStatusType.AWAITING_CONFIRMATION_RESTURANT
  }) @Retention(RetentionPolicy.SOURCE) public @interface OrderStatusType {
    int ORDER_RECIEVED = 0;
    int ACCEPTED = 1;
    int CANCELLED = 2;
    int ORDER_CANCELLED = 3;
    int DELIVERY_PARTNER_ASSIGNED = 4;
    int ORDER_PICKED_UP = 5;
    int ORDER_DELIVERED = 6;
    int ORDER_CANCEL = 7;
    int ORDER_READY_FOR_PICKUP = 8;
    int DELIVERY_PARTNER_WAITING_AT_RESTURANT = 9;
    int WE_CANT_REACH_YOU = 10;
    int AWAITING_CONFIRMATION_RESTURANT = 21;
  }

  @StringDef({
      OrderStatusMesssage.ORDER_RECIEVED, OrderStatusMesssage.ACCEPTED,
      OrderStatusMesssage.CANCELLED, OrderStatusMesssage.DELIVERY_PARTNER_ASSIGNED,
      OrderStatusMesssage.ORDER_PICKED_UP, OrderStatusMesssage.ORDER_DELIVERED,
      OrderStatusMesssage.ORDER_CANCEL, OrderStatusMesssage.ORDER_READY_FOR_PICKUP,
      OrderStatusMesssage.DELIVERY_PARTNER_WAITING_AT_RESTURANT,
      OrderStatusMesssage.AWAITING_CONFIRMATION_RESTURANT,
      OrderStatusMesssage.WE_CANT_REACH_YOU
  }) @Retention(RetentionPolicy.SOURCE) public @interface OrderStatusMesssage {
    String ORDER_RECIEVED = "We have received your order & confirming with the Restaurant.";
    String ACCEPTED = "Order is Confirmed by the Restaurant.";
    String CANCELLED = "Sorry! We Couldn't Serve You This Time!";
    String DELIVERY_PARTNER_ASSIGNED = "Delivery Partner has been assigned to your order and he is on the way to the restaurant.";
    String ORDER_PICKED_UP = "Order Picked Up by delivery partner and he is on the way!";
    String ORDER_DELIVERED = "We have delivered you order enjoy!";
    String ORDER_CANCEL = "Sorry! We Couldn't Serve You This Time!";
    String ORDER_READY_FOR_PICKUP = "Food is Prepared and we are looking for a delivery partner!";
    String DELIVERY_PARTNER_WAITING_AT_RESTURANT = "Delivery partner is waiting at the restaurant.";
    String WE_CANT_REACH_YOU = "Our Delivery person cant reach you please call him";
    String AWAITING_CONFIRMATION_RESTURANT =
        " Speedzy confirm your order waiting for restaurant confirmation";
  }

  @StringDef({
      OrderStatusTitle.ORDER_RECIEVED, OrderStatusTitle.ACCEPTED,
      OrderStatusTitle.CANCELLED, OrderStatusTitle.DELIVERY_PARTNER_ASSIGNED,
      OrderStatusTitle.ORDER_PICKED_UP, OrderStatusTitle.ORDER_DELIVERED,
      OrderStatusTitle.ORDER_CANCEL, OrderStatusTitle.ORDER_READY_FOR_PICKUP,
      OrderStatusTitle.DELIVERY_PARTNER_WAITING_AT_RESTURANT,
      OrderStatusTitle.AWAITING_CONFIRMATION_RESTURANT,
      OrderStatusTitle.WE_CANT_REACH_YOU
  }) @Retention(RetentionPolicy.SOURCE) public @interface OrderStatusTitle {
    String ORDER_RECIEVED = "Order Received by SpeedZy";
    String ACCEPTED = "Order Accepted!";
    String CANCELLED = "Order Cancelled!";
    String DELIVERY_PARTNER_ASSIGNED = "Delivery Partner assigned";
    String ORDER_PICKED_UP = "Order Picked Up";
    String ORDER_DELIVERED = "Order Delivered!";
    String ORDER_CANCEL = "Order Cancelled!";
    String ORDER_READY_FOR_PICKUP = "Order ready to pickup!";
    String DELIVERY_PARTNER_WAITING_AT_RESTURANT = "Delivery partner reached restaurant!";
    String WE_CANT_REACH_YOU = "We can't reach you!";
    String AWAITING_CONFIRMATION_RESTURANT =
        "Speedzy confirm your order!";
  }

  public static int getOrderStatus(String statusMessage){
    switch (statusMessage){
      case "We have received your order & confirming with the Restaurant.":
        return 0;
      case "Order is Confirmed by the Restaurant.":
        return 0;
      case "Sorry! We Couldn't Serve You This Time!":
        return 0;
      case "Delivery Partner has been assigned to your order and he is on the way to the restaurant.":
        return 1;
      case "Order Picked Up by delivery partner and he is on the way!":
        return 2;
      case "We have delivered you order enjoy!":
        return 0;
      case "Food is Prepared and we are looking for a delivery partner!":
        return 0;
      case "Delivery partner is waiting at the restaurant.":
        return 0;
      case "Our Delivery person cant reach you please call him":
        return 2;
      case " Speedzy confirm your order waiting for restaurant confirmation":
        return 0;
     default:
       return 0;
    }
  }
}
