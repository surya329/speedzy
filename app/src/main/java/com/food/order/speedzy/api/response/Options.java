package com.food.order.speedzy.api.response;

import java.io.Serializable;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Options implements Serializable {

	@SerializedName("Type")
	private String type;

	@SerializedName("Actual Price")
	private String actualPrice;

	@SerializedName("Extra Price")
	private String extraPrice;

	@SerializedName("Extra Item")
	private String extraItem;

	@SerializedName("Unit")
	private String unit;

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setActualPrice(String actualPrice){
		this.actualPrice = actualPrice;
	}

	public String getActualPrice(){
		return actualPrice;
	}

	public void setExtraPrice(String extraPrice){
		this.extraPrice = extraPrice;
	}

	public String getExtraPrice(){
		return extraPrice;
	}

	public void setExtraItem(String extraItem){
		this.extraItem = extraItem;
	}

	public String getExtraItem(){
		return extraItem;
	}

	public void setUnit(String unit){
		this.unit = unit;
	}

	public String getUnit(){
		return unit;
	}

	@Override
 	public String toString(){
		return 
			"Options{" + 
			"type = '" + type + '\'' + 
			",actual Price = '" + actualPrice + '\'' + 
			",extra Price = '" + extraPrice + '\'' + 
			",extra Item = '" + extraItem + '\'' + 
			",unit = '" + unit + '\'' + 
			"}";
		}
}