package com.food.order.speedzy.Model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Jayattama Prusty on 06-May-17.
 */

public class View_Rating_model implements Serializable{

    private String status;
    private List<Review> review = null;
    private Overall overal = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Review> getReview() {
        return review;
    }

    public void setReview(List<Review> review) {
        this.review = review;
    }

    public Overall getOveral() {
        return overal;
    }

    public void setOveral(Overall overal) {
        this.overal = overal;
    }

    public class Review {

        private String description;
        private String food_rating;
        private String value_rating;
        private String speed_rating;
        private String overal;
        private String added_date;
        private String firstname;
        private String lastname;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getFoodRating() {
            return food_rating;
        }

        public void setFoodRating(String foodRating) {
            this.food_rating = foodRating;
        }

        public String getValueRating() {
            return value_rating;
        }

        public void setValueRating(String valueRating) {
            this.value_rating = valueRating;
        }

        public String getSpeedRating() {
            return speed_rating;
        }

        public void setSpeedRating(String speedRating) {
            this.speed_rating = speedRating;
        }

        public String getOveral() {
            return overal;
        }

        public void setOveral(String overal) {
            this.overal = overal;
        }

        public String getAddedDate() {
            return added_date;
        }

        public void setAddedDate(String addedDate) {
            this.added_date = addedDate;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

    }
    public class Overall {

        private String food_rating;
        private String value_rating;
        private String speed_rating;
        private String overal;

        public String getFoodRating() {
            return food_rating;
        }

        public void setFoodRating(String foodRating) {
            this.food_rating = foodRating;
        }

        public String getValueRating() {
            return value_rating;
        }

        public void setValueRating(String valueRating) {
            this.value_rating = valueRating;
        }

        public String getSpeedRating() {
            return speed_rating;
        }

        public void setSpeedRating(String speedRating) {
            this.speed_rating = speedRating;
        }

        public String getOveral() {
            return overal;
        }

        public void setOveral(String overal) {
            this.overal = overal;
        }


    }
}
