package com.food.order.speedzy.SpeedzyRide;

import java.util.ArrayList;

/**
 * Created by Sujata on 29-08-2017.
 */
public interface SavedPlaceListener {
    public void onSavedPlaceClick(ArrayList<SavedAddress> mResultList, int position);
}
