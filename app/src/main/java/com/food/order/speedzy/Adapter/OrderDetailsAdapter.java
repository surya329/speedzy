package com.food.order.speedzy.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.food.order.speedzy.Model.Patanjali_order_deatail_model;
import com.food.order.speedzy.R;
import com.food.order.speedzy.api.response.Options;
import com.food.order.speedzy.api.response.OrderDetailItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */

public class OrderDetailsAdapter
    extends SectionedRecyclerViewAdapter<OrderDetailsAdapter.ViewHolder> {
  List<OrderDetailItem> mData;
  OrderDetailItem order_detail;
  List<Patanjali_order_deatail_model.Grocery.ProductDetail> patanjali_order_detailArrayList;
  Patanjali_order_deatail_model.Grocery.ProductDetail patanjaliorderdetailone;
  Context context;
  private String da = "";
  int flag;

  public OrderDetailsAdapter(int flag, Context context) {
    mData = new ArrayList<>();
    this.context = context;
    this.flag = flag;
  }

  //public OrderDetailsAdapter(
  //    List<Patanjali_order_deatail_model.Grocery.ProductDetail> patanjali_order_detailArrayList,
  //    Context context) {
  //  this.patanjali_order_detailArrayList = patanjali_order_detailArrayList;
  //  this.context = context;
  //}

  public void setData(
      List<OrderDetailItem> data) {
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public void refreshData(
      List<OrderDetailItem> data) {
    mData.clear();
    setData(data);
  }

  @Override
  public int getSectionCount() {
    if (flag == 1) {
      return mData.size();
    } else {
      return patanjali_order_detailArrayList.size();
    }
  }

  @Override
  public int getItemCount(int section) {
    if (flag == 1) {
      List<Options> optionsArrayList = new ArrayList<>();
      optionsArrayList.add(mData.get(section).getOptions());
      return optionsArrayList.size();
    } else {
      return 0;
    }
  }

  @Override
  public void onBindHeaderViewHolder(ViewHolder holder, int section) {
    if (flag == 1) {
      order_detail = mData.get(section);
      holder.dishname.setText(order_detail.getDishname() + " " + " X " + order_detail.getQty());
      holder.qtprice.setText("₹ " + order_detail.getPrice());
    } else {
      patanjaliorderdetailone = patanjali_order_detailArrayList.get(section);
      holder.dishname.setText(patanjaliorderdetailone.getData().getTitle()
          + " "
          + " X "
          + patanjaliorderdetailone.getQty());
      double total_price = Integer.parseInt(patanjaliorderdetailone.getQty()) * Double.parseDouble(
          patanjaliorderdetailone.getData().getSalePrice());
      holder.qtprice.setText("₹ " + total_price);
    }
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int section, int relativePosition,
      int absolutePosition) {
    if (flag == 1) {
      order_detail = mData.get(section);
      holder.pref.setVisibility(View.GONE);
      if (order_detail.getOptions().getType() != null) {
        if (order_detail.getOptions().getType().length() > 1) {
          holder.pref.setVisibility(View.VISIBLE);
          String preftype = order_detail.getOptions().getType();
          String preftypearr[] = preftype.split("@");
          String extraitem = order_detail.getOptions().getExtraItem();
          String extras[] = extraitem.split("@");
          for (int i = 0; i < preftypearr.length; i++) {
            for (int j = 0; j < extras.length; j++) {
              if (j == i) {
                da = da + "\n" + "--> " + preftypearr[i] + " : " + extras[j];
              }
            }
          }
          holder.pref.setText(da);
        }
      }
    }
  }

  @NonNull @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    View v = LayoutInflater.from(parent.getContext())
        .inflate(viewType == VIEW_TYPE_HEADER ? R.layout.orderdetailheader
            : R.layout.oderdtladapter_child, parent, false);
    return new ViewHolder(v);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView dishname, qtprice, pref;

    public ViewHolder(View itemView) {
      super(itemView);
      dishname = (TextView) itemView.findViewById(R.id.odrdishname);
      qtprice = (TextView) itemView.findViewById(R.id.qtyprice);
      pref = (TextView) itemView.findViewById(R.id.orderdtlpref);
    }
  }
}
