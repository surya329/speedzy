package com.food.order.speedzy.SpeedzyRideApi;

public class QueryBuilderRide {

    public static String loginRide(String name, String emailAddress, String mobileNumber, String speedzyId) {
        return "mutation{\n"
                + "  userRegistration(name:\"" + name + "\",\n"
                + "  emailAddress:\"" + emailAddress + "\",\n"
                + "  mobileNumber:\"" + mobileNumber + "\",\n"
                + "  speedzyId:\"" + speedzyId + "\",\n"
                + ")\n"
                + "  {\n"
                + "    id\n"
                + "    name\n"
                + "    mobileNumber\n"
                + "    emailAddress\n"
                + "  }\n"
                + "}";
    }
}
