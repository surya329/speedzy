package com.food.order.speedzy.api.response;

import java.io.Serializable;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DeliveryareaItem implements Serializable {

	@SerializedName("st_suburb")
	private String stSuburb;

	@SerializedName("st_city")
	private String stCity;

	@SerializedName("postcode_id")
	private String postcodeId;

	@SerializedName("in_suburb_id")
	private String inSuburbId;

	@SerializedName("flg_delivery_status")
	private String flgDeliveryStatus;

	@SerializedName("st_postcode")
	private String stPostcode;

	@SerializedName("flg_menu_choice")
	private String flgMenuChoice;

	@SerializedName("st_delivery_charge")
	private String stDeliveryCharge;

	public void setStSuburb(String stSuburb){
		this.stSuburb = stSuburb;
	}

	public String getStSuburb(){
		return stSuburb;
	}

	public void setStCity(String stCity){
		this.stCity = stCity;
	}

	public String getStCity(){
		return stCity;
	}

	public void setPostcodeId(String postcodeId){
		this.postcodeId = postcodeId;
	}

	public String getPostcodeId(){
		return postcodeId;
	}

	public void setInSuburbId(String inSuburbId){
		this.inSuburbId = inSuburbId;
	}

	public String getInSuburbId(){
		return inSuburbId;
	}

	public void setFlgDeliveryStatus(String flgDeliveryStatus){
		this.flgDeliveryStatus = flgDeliveryStatus;
	}

	public String getFlgDeliveryStatus(){
		return flgDeliveryStatus;
	}

	public void setStPostcode(String stPostcode){
		this.stPostcode = stPostcode;
	}

	public String getStPostcode(){
		return stPostcode;
	}

	public void setFlgMenuChoice(String flgMenuChoice){
		this.flgMenuChoice = flgMenuChoice;
	}

	public String getFlgMenuChoice(){
		return flgMenuChoice;
	}

	public void setStDeliveryCharge(String stDeliveryCharge){
		this.stDeliveryCharge = stDeliveryCharge;
	}

	public String getStDeliveryCharge(){
		return stDeliveryCharge;
	}

	@Override
 	public String toString(){
		return 
			"DeliveryareaItem{" + 
			"st_suburb = '" + stSuburb + '\'' + 
			",st_city = '" + stCity + '\'' + 
			",postcode_id = '" + postcodeId + '\'' + 
			",in_suburb_id = '" + inSuburbId + '\'' + 
			",flg_delivery_status = '" + flgDeliveryStatus + '\'' + 
			",st_postcode = '" + stPostcode + '\'' + 
			",flg_menu_choice = '" + flgMenuChoice + '\'' + 
			",st_delivery_charge = '" + stDeliveryCharge + '\'' + 
			"}";
		}
}