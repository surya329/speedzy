package com.food.order.speedzy.screen.patanjali;

import android.os.Parcel;
import android.os.Parcelable;
import com.food.order.speedzy.api.response.grocery.ChildSubcatItem;
import java.util.ArrayList;
import java.util.List;

public class GroceryChildList implements Parcelable {
  private String mName;
  private String mId;

  public GroceryChildList() {

  }

  protected GroceryChildList(Parcel in) {
    mName = in.readString();
    mId = in.readString();
  }

  public static final Creator<GroceryChildList> CREATOR = new Creator<GroceryChildList>() {
    @Override
    public GroceryChildList createFromParcel(Parcel in) {
      return new GroceryChildList(in);
    }

    @Override
    public GroceryChildList[] newArray(int size) {
      return new GroceryChildList[size];
    }
  };

  public static List<GroceryChildList> transform(List<ChildSubcatItem> childSubcat) {
    List<GroceryChildList> mData = new ArrayList<>();
    if (childSubcat != null && childSubcat.size() > 0) {
      for (ChildSubcatItem vm : childSubcat) {
        GroceryChildList model = new GroceryChildList();
        model.setId(vm.getId());
        model.setName(vm.getName());
        mData.add(model);
      }
    }
    return mData;
  }

  public String getName() {
    return mName;
  }

  public void setName(String name) {
    mName = name;
  }

  public String getId() {
    return mId;
  }

  public void setId(String id) {
    mId = id;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(mName);
    parcel.writeString(mId);
  }
}
