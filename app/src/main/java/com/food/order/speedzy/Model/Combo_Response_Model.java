package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Combo_Response_Model implements Serializable {
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<combos> getCombos() {
        return combos;
    }

    public void setCombos(List<combos> combos) {
        this.combos = combos;
    }

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("combos")
    @Expose
    private List<combos> combos = null;
    public class combos implements Serializable {
        public String getComboSection() {
            return comboSection;
        }

        public void setComboSection(String comboSection) {
            this.comboSection = comboSection;
        }

        public String getComboTitle() {
            return comboTitle;
        }

        public void setComboTitle(String comboTitle) {
            this.comboTitle = comboTitle;
        }

        public List<Itemlist> getItemlist() {
            return itemlist;
        }

        public void setItemlist(List<Itemlist> itemlist) {
            this.itemlist = itemlist;
        }

        @SerializedName("combo_section")
        @Expose
        private String comboSection;
        @SerializedName("combo_title")
        @Expose
        private String comboTitle;
        @SerializedName("itemlist")
        @Expose
        private List<Itemlist> itemlist = null;
        public class Itemlist implements Serializable {
            @SerializedName("combo_id")
            @Expose
            private String comboId;
            @SerializedName("combo_section")
            @Expose
            private String comboSection;
            @SerializedName("in_dish_id")
            @Expose
            private String inDishId;
            @SerializedName("st_dish_name")
            @Expose
            private String stDishName;

            public String getDish_desription() {
                return dish_desription;
            }

            public void setDish_desription(String dish_desription) {
                this.dish_desription = dish_desription;
            }

            @SerializedName("dish_desription")
            @Expose
            private String dish_desription;
            @SerializedName("st_price")
            @Expose
            private List<StPrice> stPrice = null;
            @SerializedName("flg_add_choices")
            @Expose
            private String flgAddChoices;

            public String getDish_cuisine_id() {
                return dish_cuisine_id;
            }

            public void setDish_cuisine_id(String dish_cuisine_id) {
                this.dish_cuisine_id = dish_cuisine_id;
            }

            @SerializedName("dish_cuisine_id")
            @Expose
            private String dish_cuisine_id;
            @SerializedName("egg_status")
            @Expose
            private String eggStatus;

            public String getCake_flg() {
                return cake_flg;
            }

            public void setCake_flg(String cake_flg) {
                this.cake_flg = cake_flg;
            }

            @SerializedName("cake_flg")
            @Expose
            private String cake_flg;
            @SerializedName("non_veg_status")
            @Expose
            private String nonVegStatus;
            @SerializedName("max_qty_peruser")
            @Expose
            private String maxQtyPeruser;
            @SerializedName("imagename")
            @Expose
            private String imagename;
            @SerializedName("imageURL")
            @Expose
            private String imageURL;

            public String getCoupon_active() {
                return coupon_active;
            }

            public void setCoupon_active(String coupon_active) {
                this.coupon_active = coupon_active;
            }

            @SerializedName("coupon_active")
            @Expose
            private String coupon_active;

            public String getComboId() {
                return comboId;
            }

            public void setComboId(String comboId) {
                this.comboId = comboId;
            }

            public String getComboSection() {
                return comboSection;
            }

            public void setComboSection(String comboSection) {
                this.comboSection = comboSection;
            }

            public String getInDishId() {
                return inDishId;
            }

            public void setInDishId(String inDishId) {
                this.inDishId = inDishId;
            }

            public String getStDishName() {
                return stDishName;
            }

            public void setStDishName(String stDishName) {
                this.stDishName = stDishName;
            }

            public List<StPrice> getStPrice() {
                return stPrice;
            }

            public void setStPrice(List<StPrice> stPrice) {
                this.stPrice = stPrice;
            }

            public String getFlgAddChoices() {
                return flgAddChoices;
            }

            public void setFlgAddChoices(String flgAddChoices) {
                this.flgAddChoices = flgAddChoices;
            }

            public String getEggStatus() {
                return eggStatus;
            }

            public void setEggStatus(String eggStatus) {
                this.eggStatus = eggStatus;
            }

            public String getNonVegStatus() {
                return nonVegStatus;
            }

            public void setNonVegStatus(String nonVegStatus) {
                this.nonVegStatus = nonVegStatus;
            }

            public String getMaxQtyPeruser() {
                return maxQtyPeruser;
            }

            public void setMaxQtyPeruser(String maxQtyPeruser) {
                this.maxQtyPeruser = maxQtyPeruser;
            }

            public String getImagename() {
                return imagename;
            }

            public void setImagename(String imagename) {
                this.imagename = imagename;
            }

            public String getImageURL() {
                return imageURL;
            }

            public void setImageURL(String imageURL) {
                this.imageURL = imageURL;
            }

            public String getOutOfStock() {
                return outOfStock;
            }

            public void setOutOfStock(String outOfStock) {
                this.outOfStock = outOfStock;
            }

            @SerializedName("out_of_stock")
            @Expose
            private String outOfStock;
        }
    }
}
