package com.food.order.speedzy.SpeedzyRide;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.food.order.speedzy.Activity.MyOrderActivity;
import com.food.order.speedzy.Fragment.DateTimePicker;
import com.food.order.speedzy.Model.CategoryModel;
import com.food.order.speedzy.Model.StorepackagerModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.SpeedzyRideApi.QueryBuilderRide;
import com.food.order.speedzy.SpeedzyRideApi.RideApiService;
import com.food.order.speedzy.SpeedzyRideApi.RideServiceApiGenerator;
import com.food.order.speedzy.SpeedzyRideApi.response.LoginRide.UserRegistration;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.MySharedPrefrencesData;
import com.food.order.speedzy.Utils.reyclerviewutils.RecyclerViewUtils;
import com.food.order.speedzy.api.response.coupon.CouponItem;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.root.BaseActivity;
import com.food.order.speedzy.screen.coupon.CouponAdapter;
import com.gauravbhola.ripplepulsebackground.RipplePulseLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity_New extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, DateTimePicker.TheListener {
    private View viewToAttachDisplayerTo;
    boolean network_status = true;
    MySharedPrefrencesData mySharedPrefrencesData;
    String Veg_Flag, userid = "";
    private Toolbar toolbar;
    AppBarLayout appbar;
    TextView tittle;
    TextView text_category, confirm;
    double pick_lati, drop_lati, pick_longi, drop_longi;
    String cat = "";
    EditText comment;
    List<CategoryModel.cat> categoryModels = new ArrayList<>();
    LinearLayout linear_packages, linear_ride;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private GoogleMap mGoogleMap;
    TouchSupportMapFragment mapFragment;
    LinearLayout pickup_layout, drop_layout;
    TextView pickup_location, drop_location, ride_now, ride_later;
    LinearLayout linear1;
    TextView mini_txt, micro_txt, auto_txt, sharing_txt, outstation_txt, bike_txt;
    ImageView mini, micro, auto, sharing, outstation, bike;
    String current_address = "";
    String pick_address = "", drop_address = "";
    LatLng pick_point, drop_point;
    String type = "6";
    int flag = 0, touch_flag = 0;
    private LatLng mCenterLatLong;
    ImageView imageMarker;
    boolean map_zoom = true;
    View mapView;
    ImageView my_location, back;
    int search_type;
    View view1, view2;
    ImageView image_arrow1, image_arrow2;
    LinearLayout linear_ride_paymet, appbar1;
    TextView text11, text12, title1, ride_person, payment_type, ride_price, apply_coupon, confirm_booking, search_vehical_text;
    int ride_page = 1;
    ShimmerFrameLayout mShimmerViewContainer;
    LinearLayout search_vehical, driver_detail_lin;
    View search;
    RipplePulseLayout mRipplePulseLayout;
    ImageView vehicle_img;
    TextView cancel_ride, call_driver;
    RelativeLayout linear_pickup;
    TextView pickup_time;
    RideApiService rideApiService;
    String name, emailAddress, mobileNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search__new);

        buildGoogleApiClient();
        viewToAttachDisplayerTo = findViewById(R.id.displayerAttachableView);

        mySharedPrefrencesData = new MySharedPrefrencesData();
        Veg_Flag = mySharedPrefrencesData.getVeg_Flag(this);
        userid = mySharedPrefrencesData.getUser_Id(this);
        name = mySharedPrefrencesData.getFName(this) + " " + mySharedPrefrencesData.getLName(this);
        emailAddress = "";
        mobileNumber = mySharedPrefrencesData.getSelectMobile(this);

        loginApiCall();

        search_type = getIntent().getIntExtra("search_type", 0);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        appbar = findViewById(R.id.appbar);
        tittle = findViewById(R.id.title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Veg_Flag.equalsIgnoreCase("1")) {
            statusbar_bg(R.color.red);
        } else {
            statusbar_bg(R.color.red);
        }

        imageMarker = findViewById(R.id.marker);
        my_location = findViewById(R.id.my_location);
        mapFragment = (TouchSupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

        pickup_layout = (LinearLayout) findViewById(R.id.pickup);
        drop_layout = (LinearLayout) findViewById(R.id.drop);
        pickup_location = (TextView) findViewById(R.id.pickup_location);
        drop_location = (TextView) findViewById(R.id.drop_location);
        ride_now = (TextView) findViewById(R.id.ride_now);
        ride_later = (TextView) findViewById(R.id.ride_later);
        linear1 = (LinearLayout) findViewById(R.id.linear1);
        mini = (ImageView) findViewById(R.id.mini);
        micro = (ImageView) findViewById(R.id.micro);
        auto = (ImageView) findViewById(R.id.auto);
        sharing = (ImageView) findViewById(R.id.sharing);
        outstation = (ImageView) findViewById(R.id.outstation);
        bike = (ImageView) findViewById(R.id.bike);
        linear_pickup = findViewById(R.id.linear_pickup);
        pickup_time = findViewById(R.id.pickup_time);
        mini_txt = findViewById(R.id.mini_txt);
        micro_txt = findViewById(R.id.micro_txt);
        auto_txt = findViewById(R.id.auto_txt);
        sharing_txt = findViewById(R.id.sharing_txt);
        outstation_txt = findViewById(R.id.outstation_txt);
        bike_txt = findViewById(R.id.bike_txt);
        linear_ride = findViewById(R.id.linear_ride);
        linear_packages = findViewById(R.id.linear_packages);
        text_category = findViewById(R.id.text_category);
        confirm = findViewById(R.id.confirm);
        comment = findViewById(R.id.comment);
        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);
        image_arrow1 = findViewById(R.id.image_arrow1);
        image_arrow2 = findViewById(R.id.image_arrow2);
        linear_ride_paymet = findViewById(R.id.linear_ride_paymet);
        appbar1 = findViewById(R.id.appbar1);
        text11 = findViewById(R.id.text11);
        text12 = findViewById(R.id.text12);
        title1 = findViewById(R.id.title1);
        back = findViewById(R.id.back);
        ride_person = findViewById(R.id.ride_person);
        payment_type = findViewById(R.id.payment_type);
        ride_price = findViewById(R.id.ride_price);
        apply_coupon = findViewById(R.id.apply_coupon);
        confirm_booking = findViewById(R.id.confirm_booking);
        search_vehical = findViewById(R.id.search_vehical);
        search_vehical_text = findViewById(R.id.search_vehical_text);
        search = findViewById(R.id.search);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mRipplePulseLayout = findViewById(R.id.layout_ripplepulse);
        driver_detail_lin = findViewById(R.id.driver_detail_lin);
        vehicle_img = findViewById(R.id.vehicle_img);
        cancel_ride = findViewById(R.id.cancel_ride);
        call_driver = findViewById(R.id.call_driver);

        if (search_type == 0) {
            tittle.setText("Send Packages");
            linear_packages.setVisibility(View.VISIBLE);
        } else if (search_type == 1) {
            tittle.setText("Speedzy Rides");
            linear_ride.setVisibility(View.VISIBLE);
            findcar_btn_visible();
        }
        linear_pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_time_dialog();
            }
        });

        mapFragment.setNonConsumingTouchListener(new TouchSupportMapFragment.NonConsumingTouchListener() {
            @Override
            public boolean onTouch(MotionEvent motionEvent) {
                switch (motionEvent.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        // map is touched
                        slideUpDown(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        // map touch ended
                        slideUpDown(false);
                        break;
                    default:
                        break;
                    // use more cases if needed, for example MotionEvent.ACTION_MOVE
                }
                return true;
            }
        });

        mini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "1";
                vehicle_bg(mini, micro, auto, sharing, outstation, bike, mini_txt, micro_txt, auto_txt, sharing_txt, outstation_txt, bike_txt);
            }
        });
        micro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "2";
                vehicle_bg(micro, mini, auto, sharing, outstation, bike, micro_txt, mini_txt, auto_txt, sharing_txt, outstation_txt, bike_txt);
            }
        });
        auto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "3";
                vehicle_bg(auto, mini, micro, sharing, outstation, bike, auto_txt, mini_txt, micro_txt, sharing_txt, outstation_txt, bike_txt);
            }
        });
        sharing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "4";
                vehicle_bg(sharing, mini, micro, auto, outstation, bike, sharing_txt, mini_txt, micro_txt, auto_txt, outstation_txt, bike_txt);
            }
        });
        outstation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "5";
                vehicle_bg(outstation, mini, micro, auto, sharing, bike, outstation_txt, mini_txt, micro_txt, auto_txt, sharing_txt, bike_txt);
            }
        });
        bike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "6";
                vehicle_bg(bike, micro, auto, sharing, outstation, mini, bike_txt, micro_txt, auto_txt, sharing_txt, outstation_txt, mini_txt);
            }
        });
        ride_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!pickup_location.getText().toString().equalsIgnoreCase("Enter Pickup Location") && !pickup_location.getText().toString().equalsIgnoreCase("")
                        && !drop_location.getText().toString().equalsIgnoreCase("Enter Drop Location") && !drop_location.getText().toString().equalsIgnoreCase("")) {
                    linear_pickup.setVisibility(View.VISIBLE);
                    date_time_dialog();
                }
            }
        });

        ride_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pickup_location.getText().toString().equalsIgnoreCase("Enter Pickup Location") && !pickup_location.getText().toString().equalsIgnoreCase("")
                        && !drop_location.getText().toString().equalsIgnoreCase("Enter Drop Location") && !drop_location.getText().toString().equalsIgnoreCase("")) {
                    ride_now_click();
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                marker_pin(false);
                ride_page = 1;
                imageMarker.setVisibility(View.VISIBLE);
                appbar1.setVisibility(View.GONE);
                appbar.setVisibility(View.VISIBLE);
                pickup_layout.setVisibility(View.VISIBLE);
                drop_layout.setVisibility(View.VISIBLE);
                linear_ride.setVisibility(View.VISIBLE);
                my_location.setVisibility(View.VISIBLE);
                linear_ride_paymet.setVisibility(View.GONE);
                search_vehical.setVisibility(View.GONE);
                driver_detail_lin.setVisibility(View.GONE);
                markerInvisible();
                map_marker_show(String.valueOf(drop_lati), String.valueOf(drop_longi), drop_address);
                touch_flag = 1;
            }
        });
        confirm_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linear_ride_paymet.setVisibility(View.GONE);
                search_ride_boy();
            }
        });
        cancel_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back.setClickable(true);
            }
        });
        ride_person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("content://contacts");
                Intent intent = new Intent(Intent.ACTION_PICK, uri);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, 2);
            }
        });
        payment_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_dialog_show();
            }
        });
        apply_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<CouponItem> mCouponItems = new ArrayList<>();
                add_coupon_dialog(mCouponItems);
            }
        });
        drop_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 1;
                if (drop_location.getText().toString().equalsIgnoreCase("Enter Drop Location") || touch_flag == 1) {
                    Intent data = new Intent(SearchActivity_New.this, PickupDropActivity.class);
                    data.putExtra("flag", 1);
                    startActivityForResult(data, 1);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                } else {
                    markerInvisible();
                    map_marker_show(String.valueOf(drop_lati), String.valueOf(drop_longi), drop_address);
                }
                touch_flag = 1;
            }
        });
        pickup_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 0;
                if (drop_location.getText().toString().equalsIgnoreCase("Enter Drop Location") || touch_flag == 0) {
                    Intent data = new Intent(SearchActivity_New.this, PickupDropActivity.class);
                    data.putExtra("flag", 0);
                    startActivityForResult(data, 0);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                } else {
                    markerInvisible();
                    map_marker_show(String.valueOf(pick_lati), String.valueOf(pick_longi), pick_address);
                }
                touch_flag = 0;
            }
        });
        my_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                current_location_show();
            }
        });
        BottomSheetDialog dialog_payment = new BottomSheetDialog(this, R.style.SheetDialog);
        dialog_payment.setContentView(R.layout.category_dialog);
        TextView cancel = dialog_payment.findViewById(R.id.cancel);
        TextView done = dialog_payment.findViewById(R.id.done);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_payment.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = "";
                for (int i = 0; i < categoryModels.size(); i++) {
                    if (categoryModels.get(i).isclicked) {
                        str = str + categoryModels.get(i).getName() + ",";
                        cat = cat + categoryModels.get(i).getId() + ",";
                    }
                }
                text_category.setText(str.substring(0, str.length() - 1));
                dialog_payment.dismiss();
            }
        });
        apicall(dialog_payment);
        text_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_payment.show();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pickup_location.getText().toString().equalsIgnoreCase("Enter Pickup Location") &&
                        !pickup_location.getText().toString().equalsIgnoreCase("")) {
                    if (!drop_location.getText().toString().equalsIgnoreCase("Enter Drop Location") &&
                            !drop_location.getText().toString().equalsIgnoreCase("")) {
                        if (!text_category.getText().toString().equalsIgnoreCase("Select category")) {
                            //apicall_confirm();
                            linear_packages.setVisibility(View.GONE);
                            search_ride_boy();
                        } else {
                            Toast.makeText(SearchActivity_New.this, "Please Select category", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SearchActivity_New.this, "Please Search drop location", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SearchActivity_New.this, "Please Search pickup location", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ride_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogshow();
            }
        });
    }

    public void loginApiCall() {
       if (!mySharedPrefrencesData.getRideLoginStatus(SearchActivity_New.this)) {
            rideApiService =
                    RideServiceApiGenerator.provideRetrofit(this).create(RideApiService.class);
            Log.e("create query", QueryBuilderRide.loginRide(name, emailAddress, mobileNumber, userid));
            showProgressDialog();
            rideApiService.loginRide(
                    QueryBuilderRide.loginRide(name, emailAddress, mobileNumber, userid))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<UserRegistration>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(UserRegistration response) {
                            if (response != null) {
                                mySharedPrefrencesData.setRideLoginStatus(SearchActivity_New.this, true);
                                dismissProgressDialog();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            dismissProgressDialog();
                            Log.e("error", e.getLocalizedMessage());
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }

    private void search_ride_boy() {
        marker_pin(false);
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(pick_lati, pick_longi), 13.0f));
        mRipplePulseLayout.startRippleAnimation();
        mShimmerViewContainer.startShimmerAnimation();
        search_anim();
        search_vehical.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShimmerViewContainer.stopShimmerAnimation();
                mRipplePulseLayout.stopRippleAnimation();
                driver_detail_lin.setVisibility(View.VISIBLE);
                search_vehical.setVisibility(View.GONE);
                if (type.equalsIgnoreCase("1")) {
                    vehicle_img.setImageResource(R.drawable.mini);
                } else if (type.equalsIgnoreCase("2")) {
                    vehicle_img.setImageResource(R.drawable.micro);
                } else if (type.equalsIgnoreCase("3")) {
                    vehicle_img.setImageResource(R.drawable.auto);
                } else if (type.equalsIgnoreCase("4")) {
                    vehicle_img.setImageResource(R.drawable.sharing);
                } else if (type.equalsIgnoreCase("5")) {
                    vehicle_img.setImageResource(R.drawable.outstation);
                } else if (type.equalsIgnoreCase("6")) {
                    vehicle_img.setImageResource(R.drawable.ic_bike);
                }
            }
        }, 9000);

    }

    private String ride_now_time() {
        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();
        Date afterAddingMins = new Date(t + (45 * 60000));
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String date1 = sdf.format(Calendar.getInstance().getTime());
        String strTime = null, strHour = null, strMinute = null, strAM_PM = null;
        strMinute = String.valueOf(afterAddingMins.getMinutes());
        if (afterAddingMins.getMinutes() < 10) {
            strMinute = "0" + strMinute;
        }
        if (afterAddingMins.getMinutes() > 12) {
            strHour = (afterAddingMins.getHours() - 12) + "";
            strAM_PM = "PM";
        } else if (afterAddingMins.getHours() == 12) {
            strHour = String.valueOf(afterAddingMins.getHours());
            strAM_PM = "PM";
        } else if (afterAddingMins.getHours() < 12) {
            strHour = String.valueOf(afterAddingMins.getHours());
            strAM_PM = "AM";
        }
        if (strHour != null && strAM_PM != null) {
            strTime = strHour + ":" + strMinute + " " + strAM_PM;
        }
        return date1 + "," + strTime;
    }

    private void dialogshow() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        Objects.requireNonNull(dialog.getWindow())
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.ride_price_popup);
        TextView got_it = (TextView) dialog.findViewById(R.id.got_it);
        TextView fare_detail = (TextView) dialog.findViewById(R.id.fare_detail);
        TextView got_it1 = (TextView) dialog.findViewById(R.id.got_it1);
        TextView fare_detail1 = (TextView) dialog.findViewById(R.id.fare_detail1);
        LinearLayout lin1 = dialog.findViewById(R.id.lin1);
        LinearLayout lin2 = dialog.findViewById(R.id.lin2);
        fare_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin1.setVisibility(View.GONE);
                lin2.setVisibility(View.VISIBLE);
            }
        });
        fare_detail1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin2.setVisibility(View.GONE);
                lin1.setVisibility(View.VISIBLE);
            }
        });
        got_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        got_it1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void ride_now_visible() {
        ride_page = 2;
        marker_pin(true);
        appbar.setVisibility(View.GONE);
        pickup_layout.setVisibility(View.GONE);
        drop_layout.setVisibility(View.GONE);
        linear_ride.setVisibility(View.GONE);
        my_location.setVisibility(View.GONE);
        appbar1.setVisibility(View.VISIBLE);
        linear_ride_paymet.setVisibility(View.VISIBLE);
        text11.setText(pick_address);
        text12.setText(drop_address);
        if (type.equalsIgnoreCase("1")) {
            title1.setText("Mini");
        } else if (type.equalsIgnoreCase("2")) {
            title1.setText("Micro");
        } else if (type.equalsIgnoreCase("3")) {
            title1.setText("Auto");
        } else if (type.equalsIgnoreCase("4")) {
            title1.setText("Share");
        } else if (type.equalsIgnoreCase("5")) {
            title1.setText("Outstation");
        } else if (type.equalsIgnoreCase("6")) {
            title1.setText("Bike");
        }
    }

    private void date_time_dialog() {
        DialogFragment newFragment = new DateTimePicker(1, "", "");
        newFragment.show(getSupportFragmentManager(), "DatePicker");
    }

    @Override
    public void returnDate(int flag, String date, String time) {
        pickup_time.setText(date + " " + time);
        if (ride_page == 1) {
                ride_later_click();
        }
    }

    private void ride_later_click() {
        if (type.equalsIgnoreCase("5")) {
            Intent data = new Intent(SearchActivity_New.this, OutStationActivity.class);
            data.putExtra("pickup_location", pickup_location.getText().toString());
            data.putExtra("drop_location", drop_location.getText().toString());
            data.putExtra("leave_on", pickup_time.getText().toString());
            startActivity(data);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
            ride_now_visible();
        }
    }

    private void ride_now_click() {
        if (type.equalsIgnoreCase("5")) {
            String leave_on = ride_now_time();
            Intent data = new Intent(SearchActivity_New.this, OutStationActivity.class);
            data.putExtra("pickup_location", pickup_location.getText().toString());
            data.putExtra("drop_location", drop_location.getText().toString());
            data.putExtra("leave_on", leave_on);
            startActivity(data);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else {
            linear_pickup.setVisibility(View.GONE);
            ride_now_visible();
        }
    }


    private void add_coupon_dialog(List<CouponItem> data) {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        Objects.requireNonNull(dialog.getWindow())
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.popup_coupon_view);
        final EditText etCoupon = (EditText) dialog.findViewById(R.id.et_coupon);
        final RecyclerView rvCoupon = dialog.findViewById(R.id.rv_coupon);
        final TextView btnApply = dialog.findViewById(R.id.btn_apply);
        final TextView txtAvailableCoupons = dialog.findViewById(R.id.txt_available_coupons);
        final TextView txtNoCouponsAvailable = dialog.findViewById(R.id.txt_no_coupons_available);
        CouponAdapter mAdapter = new CouponAdapter(new CouponAdapter.CallBack() {
            @Override
            public void onClickApplyButton(CouponItem couponItem) {
                //call_voucher_data(couponItem.getStCouponCode(), dialog);
            }
        });
        rvCoupon.setLayoutManager(RecyclerViewUtils.newLinearVerticalLayoutManager(this));
        rvCoupon.addItemDecoration(
                RecyclerViewUtils.newVerticalSpacingItemDecoration(this, R.dimen.spacing_12, true, true));
        rvCoupon.setAdapter(mAdapter);
        if (data.size() > 0) {
            mAdapter.setData(data);
            txtNoCouponsAvailable.setVisibility(View.GONE);
            txtAvailableCoupons.setVisibility(View.VISIBLE);
        } else {
            txtNoCouponsAvailable.setVisibility(View.VISIBLE);
            txtAvailableCoupons.setVisibility(View.GONE);
        }
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etCoupon.getText().toString().trim().isEmpty()) {
                    //call_voucher_data(etCoupon.getText().toString().trim(), dialog);
                } else {
                    Toast.makeText(SearchActivity_New.this, "Enter Coupon Code", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
        dialog.show();
    }

    private void payment_dialog_show() {
        BottomSheetDialog dialog_payment = new BottomSheetDialog(this, R.style.SheetDialog);
        dialog_payment.setContentView(R.layout.payment_ride);
        dialog_payment.setCanceledOnTouchOutside(true);
        LinearLayout cash = dialog_payment.findViewById(R.id.cash);
        LinearLayout bhim = dialog_payment.findViewById(R.id.bhim);
        LinearLayout phonepe = dialog_payment.findViewById(R.id.phonepe);
        LinearLayout paytm = dialog_payment.findViewById(R.id.paytm);
        LinearLayout gpay = dialog_payment.findViewById(R.id.google_pay);
        cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_type_set("Cash", dialog_payment);
            }
        });
        bhim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_type_set("Bhim", dialog_payment);
            }
        });
        phonepe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_type_set("PhonePe", dialog_payment);
            }
        });
        paytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_type_set("Paytm", dialog_payment);
            }
        });
        gpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_type_set("Google Pay", dialog_payment);
            }
        });
        dialog_payment.show();
    }

    private void payment_type_set(String tittle, BottomSheetDialog dialog_payment) {
        payment_type.setText(tittle);
        payment_type.setTextColor(getResources().getColor(R.color.make_gray));
        payment_type.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
        dialog_payment.dismiss();
    }

    private void search_anim() {
        Display display = getWindowManager().getDefaultDisplay();
        float width = display.getWidth();
        TranslateAnimation animation = new TranslateAnimation(0, width - 50, 0, 0); // new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation.setDuration(1000);
        animation.setRepeatCount(10); // animation repeat count
        animation.setRepeatMode(2); // repeat animation (left to right, right to
        // left )
        // animation.setFillAfter(true);
        search.startAnimation(animation);
    }

    private void marker_pin(boolean flag) {
        if (flag) {
            imageMarker.setVisibility(View.GONE);
            image_arrow1.setVisibility(View.GONE);
            image_arrow2.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
        }
        Marker pickup_mark, drop_mark;
        pickup_mark = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(pick_lati, pick_longi))
                .title("")
                .icon(bitmapDescriptorFromVector(this, R.drawable.ic_marker)));
        drop_mark = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(drop_lati, drop_longi))
                .title("")
                .icon(bitmapDescriptorFromVector(this, R.drawable.ic_marker_drop)));
        pickup_mark.setVisible(flag);
        drop_mark.setVisible(flag);
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void vehicle_bg(ImageView vehicle1, ImageView vehicle2, ImageView vehicle3, ImageView vehicle4, ImageView vehicle5,
                            ImageView vehicle6,
                            TextView vehicle1_txt, TextView vehicle2_txt, TextView vehicle3_txt, TextView vehicle4_txt,
                            TextView vehicle5_txt, TextView vehicle6_txt) {

        vehicle1.setBackground(getResources().getDrawable(R.drawable.home_image_background));
        vehicle1.setColorFilter(ContextCompat.getColor(SearchActivity_New.this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        vehicle1_txt.setTextColor(getResources().getColor(R.color.red));

        vehicle2_txt.setTextColor(getResources().getColor(R.color.make_gray));
        vehicle3_txt.setTextColor(getResources().getColor(R.color.make_gray));
        vehicle4_txt.setTextColor(getResources().getColor(R.color.make_gray));
        vehicle5_txt.setTextColor(getResources().getColor(R.color.make_gray));
        vehicle6_txt.setTextColor(getResources().getColor(R.color.make_gray));

        vehicle2.setBackgroundColor(Color.TRANSPARENT);
        vehicle2.setColorFilter(ContextCompat.getColor(SearchActivity_New.this, R.color.make_gray), android.graphics.PorterDuff.Mode.SRC_IN);
        vehicle3.setBackgroundColor(Color.TRANSPARENT);
        vehicle3.setColorFilter(ContextCompat.getColor(SearchActivity_New.this, R.color.make_gray), android.graphics.PorterDuff.Mode.SRC_IN);
        vehicle4.setBackgroundColor(Color.TRANSPARENT);
        vehicle4.setColorFilter(ContextCompat.getColor(SearchActivity_New.this, R.color.make_gray), android.graphics.PorterDuff.Mode.SRC_IN);
        vehicle5.setBackgroundColor(Color.TRANSPARENT);
        vehicle5.setColorFilter(ContextCompat.getColor(SearchActivity_New.this, R.color.make_gray), android.graphics.PorterDuff.Mode.SRC_IN);
        vehicle6.setBackgroundColor(Color.TRANSPARENT);
        vehicle6.setColorFilter(ContextCompat.getColor(SearchActivity_New.this, R.color.make_gray), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    private void apicall_confirm() {
        double distance = getdistance();
        String category = cat.substring(0, cat.length() - 1);
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<StorepackagerModel> call = methods.storepackager(userid, pickup_location.getText().toString(),
                drop_location.getText().toString(), category, String.valueOf(distance), comment.getText().toString());
        Log.d("url", "url=" + call.request().url().toString());
        call.enqueue(new Callback<StorepackagerModel>() {
            @Override
            public void onResponse(Call<StorepackagerModel> call, Response<StorepackagerModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + statusCode);
                Log.d("respones", "" + response);
                StorepackagerModel deviceUpdateModel = response.body();
                if (deviceUpdateModel.getStatus().equalsIgnoreCase("Success")) {
                    success_dialog_show();
                }
            }

            @Override
            public void onFailure(Call<StorepackagerModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "internet not available..connect internet",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void success_dialog_show() {
        final Dialog dialog = new Dialog(SearchActivity_New.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow())
                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.order_successful_popup);
        TextView ok = (TextView) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int type = 3;
                Intent intent = new Intent(SearchActivity_New.this, MyOrderActivity.class);
                intent.putExtra("flag", 1);
                intent.putExtra("type", type);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
        dialog.show();
    }

    private double getdistance() {
        Location startPoint = new Location("locationA");
        startPoint.setLatitude(pick_lati);
        startPoint.setLongitude(pick_longi);

        Location endPoint = new Location("locationA");
        endPoint.setLatitude(drop_lati);
        endPoint.setLongitude(drop_longi);

        double distance = startPoint.distanceTo(endPoint);
        return distance;
    }

    private void apicall(BottomSheetDialog dialog_payment) {
        Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
        Call<CategoryModel> call = methods.packagercat();
        Log.d("url", "url=" + call.request().url().toString());
        call.enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
                int statusCode = response.code();
                Log.d("Response", "" + response);
                CategoryModel response1 = response.body();
                if (response1.getStatus().equalsIgnoreCase("Success")) {
                    categoryModels.addAll(response1.getCat());
                    ListView listView = (ListView) dialog_payment.findViewById(R.id.listview);
                    listView.invalidate();
                    listView.setAdapter(new SearchActivity_New.MyAdapter());

                }
            }

            @Override
            public void onFailure(Call<CategoryModel> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), "internet not available..connect internet",
                //    Toast.LENGTH_LONG).show();
            }
        });
    }

    public class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return categoryModels.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View row = null;
            row = View.inflate(getApplicationContext(), R.layout.list_cat_row, null);
            TextView tvContent = (TextView) row.findViewById(R.id.tvContent);
            tvContent.setText(categoryModels.get(position).getName());

            final CheckBox cb = (CheckBox) row.findViewById(R.id.chbContent);
            tvContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    category_click(position);
                }
            });
            cb.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    category_click(position);
                }
            });

            if (categoryModels.get(position).isclicked) {

                cb.setChecked(true);
            } else {
                cb.setChecked(false);
            }
            return row;
        }

        private void category_click(int position) {
            if (categoryModels.get(position).isclicked) {
                categoryModels.get(position).isclicked = false;
            } else {
                categoryModels.get(position).isclicked = true;
            }
            notifyDataSetChanged();
        }

    }

    private void statusbar_bg(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }

    public void slideUpDown(boolean isPanelShown) {
        if (!isPanelShown) {
            // Show the panel
            slideUp(linear1);
            if (ride_page == 1) {
                marker_Show();
            } else {
                view2.setVisibility(View.GONE);
                view1.setVisibility(View.GONE);
                image_arrow1.setVisibility(View.GONE);
                image_arrow2.setVisibility(View.GONE);
                imageMarker.setVisibility(View.GONE);
            }
        } else {
            // Hide the Panel
            slideDown(linear1);
            view2.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
            image_arrow1.setVisibility(View.GONE);
            image_arrow2.setVisibility(View.GONE);
        }
    }

    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Commons.back_button_transition(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(SearchActivity_New.this)
                .addOnConnectionFailedListener(SearchActivity_New.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void findcar_btn_visible() {

        if (pick_address.equalsIgnoreCase("") || pick_address == null) {
            pickup_location.setText("Enter Pickup Location");
        } else {
            pickup_location.setText(pick_address);
        }

        if (drop_address.equalsIgnoreCase("") || drop_address == null) {
            drop_location.setText("Enter Drop Location");
        } else {
            drop_location.setText(drop_address);
        }

        if (!pickup_location.getText().toString().equalsIgnoreCase("Enter Pickup Location") && !pickup_location.getText().toString().equalsIgnoreCase("")
                && !drop_location.getText().toString().equalsIgnoreCase("Enter Drop Location") && !drop_location.getText().toString().equalsIgnoreCase("")) {
            ride_later.setBackgroundColor(Color.TRANSPARENT);
            ride_later.setTextColor(getResources().getColor(R.color.white));
            ride_now.setBackgroundColor(Color.TRANSPARENT);
            ride_now.setTextColor(getResources().getColor(R.color.white));
        } else {
            ride_later.setBackgroundColor(getResources().getColor(R.color.transparent_white));
            ride_later.setTextColor(getResources().getColor(R.color.ride_unselect));
            ride_now.setBackgroundColor(getResources().getColor(R.color.transparent_white));
            ride_now.setTextColor(getResources().getColor(R.color.ride_unselect));
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        current_location_show();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 0 || resultCode == 1) {
            markerInvisible();
            if (resultCode == 0) {
                if (data != null) {
                    String address = data.getStringExtra("address");
                    String latitude = data.getStringExtra("latitude");
                    String longitude = data.getStringExtra("longitude");
                    map_marker_show(latitude, longitude, address);
                    pick_address = address;
                    pick_point = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                    pick_lati = Double.parseDouble(latitude);
                    pick_longi = Double.parseDouble(longitude);
                }
            }
            if (resultCode == 1) {
                if (data != null) {
                    String address = data.getStringExtra("address");
                    String latitude = data.getStringExtra("latitude");
                    String longitude = data.getStringExtra("longitude");
                    map_marker_show(latitude, longitude, address);
                    drop_address = address;
                    drop_point = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                    drop_lati = Double.parseDouble(latitude);
                    drop_longi = Double.parseDouble(longitude);
                }
            }
            if (search_type == 1) {
                findcar_btn_visible();
            }
        }
        if (requestCode == 2 && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};

            Cursor cursor = getContentResolver().query(uri, projection,
                    null, null, null);
            cursor.moveToFirst();

            int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            String number = cursor.getString(numberColumnIndex);

            int nameColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            String name = cursor.getString(nameColumnIndex);
            ride_person.setText(name);
            setTextViewDrawableColor(ride_person, R.color.red);

        } else {
            ride_person.setText("Personal");
            setTextViewDrawableColor(ride_person, R.color.make_gray);
        }
    }

    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(textView.getContext(), color), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (ride_page == 1) {
            find_address(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
        }
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private void current_location_show() {
        markerInvisible();
        if (mGoogleApiClient.isConnected()) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(1000);
            mLocationRequest.setFastestInterval(1000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        }
    }

    private void map_marker_show(String lati, String longi, String str) {
        if (str != null) {
            String[] new_str = str.split(",");
            String s = "";
            for (int i = 0; i < new_str.length; i++) {
                s = s + new_str[i];
                if (i == 2) {
                    s = s + "\n";
                }
                if (flag == 0) {
                    pick_lati = Double.parseDouble(lati);
                    pick_longi = Double.parseDouble(longi);
                    pick_address = s;
                    pickup_location.setText(s);
                } else if (flag == 1) {
                    drop_lati = Double.parseDouble(lati);
                    drop_longi = Double.parseDouble(longi);
                    drop_address = s;
                    drop_location.setText(s);
                }
            }
        }
        if (ActivityCompat.checkSelfPermission(SearchActivity_New.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SearchActivity_New.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        if (map_zoom) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(lati), Double.parseDouble(longi)))      // Sets the center of the map to location user
                    .zoom(17f)                   // Sets the zoom
                    //.bearing(90)                // Sets the orientation of the camera to east
                    //.tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        marker_Show();
        map_zoom = false;
    }

    private void markerInvisible() {
        map_zoom = true;
        imageMarker.setVisibility(View.GONE);
        view1.setVisibility(View.GONE);
        view2.setVisibility(View.GONE);
    }

    private void marker_Show() {
        if (flag == 0) {
            findViewById(R.id.text1).setVisibility(View.VISIBLE);
            findViewById(R.id.text2).setVisibility(View.GONE);
            pickup_layout.setBackgroundResource(R.drawable.bg_lightblack);
            drop_layout.setBackgroundResource(R.drawable.bg_lightblack_grey);
            pickup_location.setTextColor(getResources().getColor(R.color.black));
            drop_location.setTextColor(getResources().getColor(R.color.poly2));
            image_arrow1.setVisibility(View.VISIBLE);
            image_arrow2.setVisibility(View.GONE);
        } else if (flag == 1) {
            findViewById(R.id.text2).setVisibility(View.VISIBLE);
            findViewById(R.id.text1).setVisibility(View.GONE);
            drop_layout.setBackgroundResource(R.drawable.bg_lightblack);
            pickup_layout.setBackgroundResource(R.drawable.bg_lightblack_grey);
            pickup_location.setTextColor(getResources().getColor(R.color.poly2));
            drop_location.setTextColor(getResources().getColor(R.color.black));
            image_arrow2.setVisibility(View.VISIBLE);
            image_arrow1.setVisibility(View.GONE);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (flag == 0) {
                    imageMarker.setVisibility(View.VISIBLE);
                    imageMarker.setImageResource(R.drawable.ic_marker);
                    view1.setVisibility(View.VISIBLE);
                    view2.setVisibility(View.GONE);
                } else if (flag == 1) {
                    imageMarker.setVisibility(View.VISIBLE);
                    imageMarker.setImageResource(R.drawable.ic_marker_drop);
                    view2.setVisibility(View.VISIBLE);
                    view1.setVisibility(View.GONE);
                }
            }
        }, 1000);
        marker_bounce();
    }

    private void marker_bounce() {
        Animation bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        imageMarker.startAnimation(bounce);
    }

    private void find_address(String latitude, String longitude) {
        try {
            Geocoder geocoder = new Geocoder(SearchActivity_New.this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1);
            if (addresses != null && addresses.size() > 0) {
                Address location1 = addresses.get(0);
                String str = location1.getAddressLine(0);
                current_address = str;
                map_marker_show(latitude, longitude, str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
     /*   try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mGoogleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            SearchActivity_New.this, R.raw.map_style));

            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }*/
        mGoogleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
            }
        });

        mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (ride_page == 1) {
                    mCenterLatLong = mGoogleMap.getCameraPosition().target;
                    mGoogleMap.clear();
                    find_address(String.valueOf(mCenterLatLong.latitude), String.valueOf(mCenterLatLong.longitude));
                }
            }
        });

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (ContextCompat.checkSelfPermission(SearchActivity_New.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
        }
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(SearchActivity_New.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(SearchActivity_New.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // SearchActivity_New.this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("SearchActivity_New.this app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(SearchActivity_New.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(SearchActivity_New.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(SearchActivity_New.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on SearchActivity_New.this permission.
                    Toast.makeText(SearchActivity_New.this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions SearchActivity_New.this app might request
        }
    }
}
