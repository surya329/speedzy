package com.food.order.speedzy.api.response.delivery;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ListDriverLocationItem{

	@SerializedName("location")
	private String location;

	public String getLocation(){
		return location;
	}

	@Override
 	public String toString(){
		return 
			"ListDriverLocationItem{" + 
			"location = '" + location + '\'' + 
			"}";
		}
}