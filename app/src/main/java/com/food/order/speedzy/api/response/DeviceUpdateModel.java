package com.food.order.speedzy.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DeviceUpdateModel implements Serializable {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    @SerializedName("info")
    @Expose
    private Info info;
    public class Info implements Serializable {
        @SerializedName("D_Status")
        @Expose
        private String dStatus;

        public String getdStatus() {
            return dStatus;
        }

        public void setdStatus(String dStatus) {
            this.dStatus = dStatus;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(String deviceName) {
            this.deviceName = deviceName;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceOS() {
            return deviceOS;
        }

        public void setDeviceOS(String deviceOS) {
            this.deviceOS = deviceOS;
        }

        public String getdOsVersion() {
            return dOsVersion;
        }

        public void setdOsVersion(String dOsVersion) {
            this.dOsVersion = dOsVersion;
        }

        public String getdAppVersion() {
            return dAppVersion;
        }

        public void setdAppVersion(String dAppVersion) {
            this.dAppVersion = dAppVersion;
        }

        @SerializedName("Device_token")
        @Expose
        private String deviceToken;
        @SerializedName("Device_name")
        @Expose
        private String deviceName;
        @SerializedName("Device_Type")
        @Expose
        private String deviceType;
        @SerializedName("Device_OS")
        @Expose
        private String deviceOS;
        @SerializedName("D_Os_Version")
        @Expose
        private String dOsVersion;
        @SerializedName("D_app_Version")
        @Expose
        private String dAppVersion;
    }
}
