package com.food.order.speedzy.Utils;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public class NoScrollRecyclerView extends RecyclerView {
  public NoScrollRecyclerView(Context context) {
    super(context);
  }

  public NoScrollRecyclerView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public NoScrollRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override protected void onMeasure(int widthSpec, int heightSpec) {
    int expandSpec =
        View.MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, View.MeasureSpec.AT_MOST);
    super.onMeasure(widthSpec, expandSpec);
  }
}
