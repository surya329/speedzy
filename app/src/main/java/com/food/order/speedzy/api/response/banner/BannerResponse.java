package com.food.order.speedzy.api.response.banner;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class BannerResponse{

	@SerializedName("Banners")
	private List<BannersItem> banners;

	@SerializedName("status")
	private String status;

	public List<BannersItem> getBanners(){
		return banners;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"BannerResponse{" + 
			"banners = '" + banners + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}