package com.food.order.speedzy.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.Activity.CateringActivity_New;
import com.food.order.speedzy.Activity.RestarantDetails;
import com.food.order.speedzy.Activity.RestarantDetails_New;
import com.food.order.speedzy.Model.RestaurantModel;
import com.food.order.speedzy.Model.ShopListModelNew;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.SpeedzyConstants;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Sujata Mohanty.
 */

public class HotelsAdapter_New extends RecyclerView.Adapter<HotelsAdapter_New.ViewHolder> {

  Activity context;
  // private List<Restaurant_model> restaurant_modelList;
  ArrayList<ShopListModelNew.Restaurants> restaurant_modelList;
  public static int sCorner = 30;
  public static int sMargin = 0;
  String nav_type, app_message, app_openstatus, menu_id;
  ShopListModelNew.Restaurant_timings restaurant_timings;
  ShopListModelNew.open_close_status open_close_status;

  public HotelsAdapter_New(Activity context,
      ArrayList<ShopListModelNew.Restaurants> restaurant_modelList, String app_message,
      String app_openstatus, ShopListModelNew.Restaurant_timings restaurant_timings,
      ShopListModelNew.open_close_status open_close_status, String nav_type, String menu_id) {
    this.context = context;
    this.restaurant_modelList = restaurant_modelList;
    this.nav_type = nav_type;
    this.app_message = app_message;
    this.app_openstatus = app_openstatus;
    this.menu_id = menu_id;
    this.open_close_status = open_close_status;
    this.restaurant_timings = restaurant_timings;
  }
    /*public HotelsAdapter_New(Activity context, List<Restaurant_model> restaurant_modelList, String nav_type) {
        this.context = context;
        this.restaurant_modelList = restaurant_modelList;
        this.nav_type = nav_type;
    }*/

  @NonNull @Override
  public HotelsAdapter_New.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.hotellay_adapter, parent, false);
    // set the view's size, margins, paddings and layout parameters
    return new ViewHolder(v);
  }

  private RestaurantModel transform(ShopListModelNew.Restaurants restaurant_model) {
    String base_url_logo =
        "https://storage.googleapis.com/speedzy_data/resources/restaurantlogo/";
    RestaurantModel obj = new RestaurantModel();
    obj.setmResPic(base_url_logo + "big/" + restaurant_model.getRestaurantLogoImage());
    obj.setmResName(restaurant_model.getStRestaurantName());
    obj.setmRating(restaurant_model.getCountRating());
    obj.setmRatingBar(Float.parseFloat(restaurant_model.getAvgRating()));
    obj.setmMsg(restaurant_model.getPromotion_msg());
    obj.setmInRestaurantId(restaurant_model.getInRestaurantId());
    obj.setmStStreetAddress(restaurant_model.getStStreetAddress());
    obj.setmStSuburb(restaurant_model.getStSuburb());

    if (restaurant_model.getFlg_ac_status().equalsIgnoreCase("2") ||
            open_close_status.getOpenstatus().equalsIgnoreCase("0")) {
      obj.setmOpenClose("Closed");
    } else if (restaurant_model.getFlg_ac_status().equalsIgnoreCase("1") ||
            open_close_status.getOpenstatus().equalsIgnoreCase("1")) {
      obj.setmOpenClose("Open");
    }
    obj.setmMinOrder("Min ₹" + restaurant_model.getStMinOrder());
    if (nav_type.equalsIgnoreCase("7") || menu_id.equalsIgnoreCase("M1014")) {
      obj.setmMenu("Book");
    } else {
      obj.setmMenu("Menu");
    }
    return obj;
  }

  @Override
  public void onBindViewHolder(HotelsAdapter_New.ViewHolder holder, final int position) {
    ShopListModelNew.Restaurants restaurant_model = restaurant_modelList.get(position);

    String base_url = "https://storage.googleapis.com/speedzy_data/resources/restaurant/very_small/" + restaurant_model.getRestaurantMainImage();
    String base_url_logo =
        "https://storage.googleapis.com/speedzy_data/resources/restaurantlogo/big/"+restaurant_model.getRestaurantLogoImage();
    RequestBuilder<Drawable> thumbnailRequest = Glide
            .with(context)
            .load(restaurant_model.getRestaurantMainImage())
            .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder));
    Glide
            .with(context)
            .load(restaurant_model.getRestaurantMainImage())
            .thumbnail(thumbnailRequest)
            .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner)).placeholder(R.drawable.combo_placeholder).diskCacheStrategy(DiskCacheStrategy.ALL))
            .into(holder.res_logo);
    holder.restname.setText(restaurant_model.getStRestaurantName());
    if (restaurant_model.getStStreetAddress()!=null) {
      holder.res_address.setText(restaurant_model.getStStreetAddress());
    }else if (restaurant_model.getStSuburb()!=null)  {
      holder.res_address.setText(restaurant_model.getStSuburb());
    }else {
      holder.res_address.setText("- - - - - - - - - -");
    }
    holder.rating.setText(restaurant_model.getCountRating() + " Ratings");
    holder.avg_rating.setText(restaurant_model.getAvgRating());
    holder.ratingbar.setRating(Float.parseFloat(restaurant_model.getAvgRating()));
    //holder.msg.setText(app_message);
    holder.msg.setText(restaurant_model.getPromotion_msg());
    holder.min_order.setText("Min ₹" + restaurant_model.getStMinOrder());
    //holder.open_close_status.setVisibility(View.VISIBLE);
    //holder.open_close_status.setText(open_close_status.getMessage());
    if (restaurant_model.getFlg_ac_status().equalsIgnoreCase("2") ||
            open_close_status.getOpenstatus().equalsIgnoreCase("0")) {
      holder.open_close_img.setImageResource(R.drawable.ic_closed);
    } else if (restaurant_model.getFlg_ac_status().equalsIgnoreCase("1") ||
            open_close_status.getOpenstatus().equalsIgnoreCase("1")) {
      holder.open_close_img.setImageResource(R.drawable.ic_open);;
    }

    if (restaurant_model.getFlag().length() > 0) {
      holder.offer_tag.setVisibility(View.VISIBLE);
      holder.offer_tag.setText(restaurant_model.getFlag());
    } else {
      holder.offer_tag.setVisibility(View.INVISIBLE);
    }
    if (open_close_status.getOpenstatus().equalsIgnoreCase("0") ||
            restaurant_model.getFlg_ac_status().equalsIgnoreCase("2")) {
      ColorMatrix matrix = new ColorMatrix();
      matrix.setSaturation(0);
      holder.res_logo.setColorFilter(new ColorMatrixColorFilter(matrix));
      holder.restname.setTextColor(context.getResources().getColor(R.color.make_gray));
      holder.res_address.setTextColor(context.getResources().getColor(R.color.make_gray));
      holder.offer_tag.setTextColor(context.getResources().getColorStateList(R.color.white));
      holder.offer_tag.setBackgroundTintList(context.getResources().getColorStateList(R.color.make_gray));
      holder.msg.setTextColor(context.getResources().getColor(R.color.make_gray));
      holder.linear.setBackgroundTintList(context.getResources().getColorStateList(R.color.light_gray50));
      holder.rating.setTextColor(context.getResources().getColor(R.color.make_gray));
      holder.avg_rating.setTextColor(context.getResources().getColor(R.color.white));
      holder.rating.setBackgroundTintList(context.getResources().getColorStateList(R.color.headerPrefColor));
      holder.avg_rating.setBackgroundTintList(context.getResources().getColorStateList(R.color.make_gray));
      holder.stars.getDrawable(2).setColorFilter(Color.parseColor("#878686"), PorterDuff.Mode.SRC_ATOP);
      holder.open_close_img.setImageResource(R.drawable.ic_closed);
      holder.open_close_img.setImageTintList(context.getResources().getColorStateList(R.color.make_gray));
      holder.min_order.setTextColor(context.getResources().getColor(R.color.make_gray));
    }else {
      holder.res_logo.setColorFilter(null);
      holder.restname.setTextColor(Color.parseColor("#383c3f"));
      holder.res_address.setTextColor(context.getResources().getColor(R.color.make_gray));
      holder.offer_tag.setTextColor(context.getResources().getColorStateList(R.color.white));
      holder.offer_tag.setBackgroundTintList(context.getResources().getColorStateList(R.color.red));
      holder.msg.setTextColor(context.getResources().getColor(R.color.orange));
      holder.linear.setBackgroundTintList(context.getResources().getColorStateList(R.color.white));
      holder.rating.setTextColor(Color.parseColor("#3aab25"));
      holder.avg_rating.setTextColor(context.getResources().getColor(R.color.white));
      holder.rating.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#dcf9d6")));
      holder.avg_rating.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#3aab25")));
      holder.stars.getDrawable(2).setColorFilter(Color.parseColor("#830E72"), PorterDuff.Mode.SRC_ATOP);
      holder.open_close_img.setImageResource(R.drawable.ic_open);
      holder.open_close_img.setImageTintList(context.getResources().getColorStateList(R.color.price_green));
      holder.min_order.setTextColor(Color.parseColor("#ed4a47"));
    }

    holder.linear.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (app_openstatus.equalsIgnoreCase("0")) {
          dialog_show();
        } else {
          if (nav_type.equalsIgnoreCase("7")) {
            Intent intent = new Intent(context, CateringActivity_New.class);
            context.startActivity(intent);
            context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
          } else {
            Commons.restname = restaurant_model.getStRestaurantName();
            Commons.restadd = restaurant_model.getStreetAddress();
            Commons.restsuburb = restaurant_model.getStSuburb();
            Intent in = new Intent(context, RestarantDetails.class);
            in.putExtra("additem",false);
            in.putExtra("res_id", restaurant_model.getInRestaurantId());
            in.putExtra("res_name", restaurant_model.getStRestaurantName());
            in.putExtra("res_image", restaurant_model.getRestaurantCoverImage());
            in.putExtra("res_logo_image", restaurant_model.getRestaurantLogoImage());
            in.putExtra("res_add", restaurant_model.getStStreetAddress());
            in.putExtra("res_minorder", restaurant_model.getStMinOrder());
            in.putExtra("res_rating", restaurant_model.getAvgRating());
            in.putExtra("res_reviews", restaurant_model.getCountRating());
            in.putExtra("Suburb", restaurant_model.getStSuburb());
            in.putExtra("Promotion_msg", restaurant_model.getPromotion_msg());
            in.putExtra("Flg_ac_status", restaurant_model.getFlg_ac_status());
            in.putExtra("postcode", "");
            in.putExtra("app_openstatus", open_close_status.getOpenstatus());
            in.putExtra("menu_id", menu_id);
            in.putExtra("nav_type", nav_type);
            in.putExtra("extra_model", transform(restaurant_model));
            in.putExtra("lati",restaurant_model.getSt_latitude());
            in.putExtra("longi",restaurant_model.getSt_longitude());
            if (menu_id.equalsIgnoreCase("M1001")) {
              in.putExtra("start_time_lunch", restaurant_timings.getLunch_start());
              in.putExtra("end_time_lunch", restaurant_timings.getLunch_end());
              in.putExtra("start_time_dinner", restaurant_timings.getDinner_start());
              in.putExtra("end_time_dinner", restaurant_timings.getDinner_end());
            } else if (menu_id.equalsIgnoreCase("M1002")) {
              in.putExtra("start_time_lunch", restaurant_timings.getDinner_start());
              in.putExtra("end_time_lunch", restaurant_timings.getDinner_end());
              in.putExtra("start_time_dinner", "");
              in.putExtra("end_time_dinner", "");
            } else if (menu_id.equalsIgnoreCase("M1003")) {
              in.putExtra("start_time_lunch", restaurant_timings.getBreakfast_start());
              in.putExtra("end_time_lunch", restaurant_timings.getBreakfast_end());
              in.putExtra("start_time_dinner", "");
              in.putExtra("end_time_dinner", "");
            } else if (menu_id.equalsIgnoreCase("M1012")) {
              in.putExtra("start_time_lunch", restaurant_timings.getFastfood_start());
              in.putExtra("end_time_lunch", restaurant_timings.getFastfood_end());
              in.putExtra("start_time_dinner", "");
              in.putExtra("end_time_dinner", "");
            } else if (menu_id.equalsIgnoreCase("M1006")) {
              in.putExtra("start_time_lunch", restaurant_timings.getSweets_start());
              in.putExtra("end_time_lunch", restaurant_timings.getSweets_end());
              in.putExtra("start_time_dinner", "");
              in.putExtra("end_time_dinner", "");
            } else if (menu_id.equalsIgnoreCase("M1005")) {
              in.putExtra("start_time_lunch", restaurant_timings.getFruits_start());
              in.putExtra("end_time_lunch", restaurant_timings.getFruits_end());
              in.putExtra("start_time_dinner", "");
              in.putExtra("end_time_dinner", "");
            } else if (menu_id.equalsIgnoreCase("M1014")) {
              in.putExtra("start_time_lunch", restaurant_timings.getLunch_start());
              in.putExtra("end_time_lunch", restaurant_timings.getLunch_end());
              in.putExtra("start_time_dinner", restaurant_timings.getDinner_start());
              in.putExtra("end_time_dinner", restaurant_timings.getDinner_end());
            } else if (menu_id.equalsIgnoreCase("M1007")) {
              in.putExtra("start_time_lunch", restaurant_timings.getBakery_start());
              in.putExtra("end_time_lunch", restaurant_timings.getBakery_end());
              in.putExtra("start_time_dinner", "");
              in.putExtra("end_time_dinner", "");
            } else if (menu_id.equalsIgnoreCase(SpeedzyConstants.FRUITS_MENU_ID)) {
              in.putExtra("start_time_lunch", restaurant_timings.getLunch_start());
              in.putExtra("end_time_lunch", restaurant_timings.getLunch_end());
              in.putExtra("start_time_dinner", restaurant_timings.getDinner_start());
              in.putExtra("end_time_dinner", restaurant_timings.getDinner_end());
            }
                   /* in.putExtra("postcode", restaurant_model.getStPostcode());
                    in.putExtra("booktableparameter", restaurant_model.getAgree_table_booking());
                    in.putExtra("deliverytime", (Serializable) restaurant_model.getDeliverySchedule());
                    in.putExtra("pickuptime", (Serializable) restaurant_model.getPickupSchedule());
                    in.putExtra("tom_deliverytime", (Serializable) restaurant_model.getTom_delivery_schedule());
                    in.putExtra("tom_pickuptime", (Serializable) restaurant_model.getTom_pickup_schedule());
                    in.putExtra("inner_image_list", (Serializable) restaurant_model.getInnerimg());
                    in.putExtra("stclose_open", restaurant_model.getClose_open_st());
                    in.putExtra("venuecuisine", restaurant_model.getStVenueCuisine());
                    Commons.closedtoday = restaurant_model.getClose_open_st();*/
            Commons.gst_commision = restaurant_model.getStGstCommission();
            if (!(restaurant_model.getStMinOrder() == null)) {

              Commons.min_order_rest = Double.parseDouble(restaurant_model.getStMinOrder());
            }
            Commons.flag_for_hta = "7";
            /*ActivityOptionsCompat option = ActivityOptionsCompat
                    .makeSceneTransitionAnimation(context, holder.linear, "card");*/
            context.startActivity(in/*,option.toBundle()*/);
            context.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
          }
        }
      }
    });
  }

  private boolean close_check(String start, String end) {
    boolean flag = false;
    try {
      String from = start + ":00";
      String to = end + ":00";

      Date date = new Date();
      String strDateFormat = "HH:mm:ss";
      DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
      String now = dateFormat.format(date);

      SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
      Date date_from = formatter.parse(from);
      Date date_to = formatter.parse(to);
      Date dateNow = formatter.parse(now);
      if (date_from.before(dateNow) && date_to.after(dateNow)) {
        System.out.println("Yes time between");
        flag = true;
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return flag;
  }

  private void dialog_show() {
    final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.setContentView(R.layout.wallet_popup);
    TextView dis = (TextView) dialog.findViewById(R.id.dismiss);
    TextView warn = (TextView) dialog.findViewById(R.id.warning);
    TextView msg = (TextView) dialog.findViewById(R.id.message);
    warn.setText("Info!!");
    msg.setText("Sorry we are closed at this moment");
    dis.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
      }
    });
    dialog.show();
  }

  @Override
  public int getItemCount() {
    return restaurant_modelList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView restname, rating,avg_rating, offer_tag, msg, min_order,res_address;
    ImageView res_logo,open_close_img;
    CardView linear;
    RatingBar ratingbar;
    LayerDrawable stars;
    public ViewHolder(View itemView) {
      super(itemView);
      restname = (TextView) itemView.findViewById(R.id.restname);
      offer_tag = (TextView) itemView.findViewById(R.id.offer_tag);
      msg = (TextView) itemView.findViewById(R.id.msg);
      res_logo = (ImageView) itemView.findViewById(R.id.res_logo);
      linear =  itemView.findViewById(R.id.linear);
      rating = (TextView) itemView.findViewById(R.id.rating);
      avg_rating=itemView.findViewById(R.id.avg_rating);
      ratingbar = (RatingBar) itemView.findViewById(R.id.ratingbar);
      stars = (LayerDrawable) ratingbar.getProgressDrawable();
      open_close_img = itemView.findViewById(R.id.open_close_img);
      min_order = (TextView) itemView.findViewById(R.id.min_order);
      res_address= itemView.findViewById(R.id.res_address);
    }
  }
}

