package com.food.order.speedzy.api.response.banner;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class BannersItem{

	@SerializedName("dish_id")
	private String dishId;

	@SerializedName("created")
	private String created;

	@SerializedName("ImageURL")
	private String imageURL;

	@SerializedName("del_status")
	private String delStatus;

	@SerializedName("banner_name")
	private String bannerName;

	@SerializedName("non_veg_status")
	private String nonVegStatus;

	@SerializedName("rest_id")
	private String restId;

	@SerializedName("image_name")
	private String imageName;

	@SerializedName("sequence")
	private String sequence;

	@SerializedName("category_id")
	private String categoryId;

	@SerializedName("sub_category_id")
	private String subCategoryId;

	@SerializedName("id")
	private String id;

	@SerializedName("nav_type")
	private String navType;

	@SerializedName("display_status")
	private String displayStatus;

	@SerializedName("menu_ID")
	private String menuID;

	public String getDishId(){
		return dishId;
	}

	public String getCreated(){
		return created;
	}

	public String getImageURL(){
		return imageURL;
	}

	public String getDelStatus(){
		return delStatus;
	}

	public String getBannerName(){
		return bannerName;
	}

	public String getNonVegStatus(){
		return nonVegStatus;
	}

	public String getRestId(){
		return restId;
	}

	public String getImageName(){
		return imageName;
	}

	public String getSequence(){
		return sequence;
	}

	public String getCategoryId(){
		return categoryId;
	}

	public String getSubCategoryId(){
		return subCategoryId;
	}

	public String getId(){
		return id;
	}

	public String getNavType(){
		return navType;
	}

	public String getDisplayStatus(){
		return displayStatus;
	}

	public String getMenuID(){
		return menuID;
	}

	@Override
 	public String toString(){
		return 
			"BannersItem{" + 
			"dish_id = '" + dishId + '\'' + 
			",created = '" + created + '\'' + 
			",imageURL = '" + imageURL + '\'' + 
			",del_status = '" + delStatus + '\'' + 
			",banner_name = '" + bannerName + '\'' + 
			",non_veg_status = '" + nonVegStatus + '\'' + 
			",rest_id = '" + restId + '\'' + 
			",image_name = '" + imageName + '\'' + 
			",sequence = '" + sequence + '\'' + 
			",category_id = '" + categoryId + '\'' + 
			",sub_category_id = '" + subCategoryId + '\'' + 
			",id = '" + id + '\'' + 
			",nav_type = '" + navType + '\'' + 
			",display_status = '" + displayStatus + '\'' + 
			",menu_ID = '" + menuID + '\'' + 
			"}";
		}
}