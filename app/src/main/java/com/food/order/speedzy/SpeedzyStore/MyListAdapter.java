package com.food.order.speedzy.SpeedzyStore;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import com.food.order.speedzy.R;

import java.util.ArrayList;

public class MyListAdapter
        extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {
    Activity context;
    ArrayList<MyListModel> myListModels=new ArrayList<MyListModel>();
    public MyListAdapter(Activity context) {
        this.context = context;
    }
    @Override
    public void onBindViewHolder(final MyListAdapter.ViewHolder holder,
                                       int section) {
        if (myListModels.get(section).getItem_name().equalsIgnoreCase("Type item one by one")||
                myListModels.get(section).getItem_name().equalsIgnoreCase("Add more items")){
            holder.itemname.setText("");
            holder.itemname.setHint(myListModels.get(section).getItem_name());
            holder.qty_linear.setVisibility(View.GONE);
        }else{
            holder.itemname.setText(myListModels.get(section).getItem_name());
            holder.qty_linear.setVisibility(View.VISIBLE);
        }
        holder.qty.setText(myListModels.get(section).getQuantity());
        holder.itemname.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length()==1 && myListModels.get(holder.getAdapterPosition()).isAdd_item()){
                    addItem(1);
                    myListModels.get(holder.getAdapterPosition()).setAdd_item(false);
                }
                if (!myListModels.get(holder.getAdapterPosition()).isAdd_item() && s.toString().length()>0) {
                    holder.qty_linear.setVisibility(View.VISIBLE);
                    myListModels.get(holder.getAdapterPosition()).setItem_name(s.toString());
                }

                if (s.toString().length()==0 && !myListModels.get(holder.getAdapterPosition()).isAdd_item()){
                    myListModels.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    notifyItemRangeChanged(holder.getAdapterPosition(), myListModels.size());
                    if (myListModels.size() == 1) {
                        myListModels.get(0).setItem_name("Type item one by one");
                        myListModels.get(0).setQuantity("1");
                        myListModels.get(0).setAdd_item(true);
                       notifyDataSetChanged();
                    }
                }
            }
        });

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int qty1 = Integer.parseInt(holder.qty.getText().toString());

                if (qty1 < 11) {
                    qty1++;
                    holder.qty.setText( ""+qty1);
                    myListModels.get(holder.getAdapterPosition()).setQuantity(String.valueOf(qty1));
                }else {
                    Toast.makeText(context.getApplicationContext(),"Maximum 11 Items Added",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty1 = Integer.parseInt(holder.qty.getText().toString());
                if (qty1 > 1) {
                    holder.sub.setVisibility(View.VISIBLE);
                    qty1--;
                    holder.qty.setText(""+qty1);
                    myListModels.get(holder.getAdapterPosition()).setQuantity(String.valueOf(qty1));
                }else {
                    myListModels.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    notifyItemRangeChanged(holder.getAdapterPosition(), myListModels.size());
                    if (myListModels.size() == 1) {
                        myListModels.get(0).setItem_name("Type item one by one");
                        myListModels.get(0).setQuantity("1");
                        myListModels.get(0).setAdd_item(true);
                        notifyDataSetChanged();
                    }
                }
            }
        });
    }
    public void addItem(int i) {
        MyListModel myListModel = new MyListModel();
        if (i==0) {
            myListModel.setItem_name("Type item one by one");
        }
        if (i==1) {
            myListModel.setItem_name("Add more items");
        }
        myListModel.setQuantity("1");
        myListModel.setAdd_item(true);
        myListModels.add(myListModel);
        notifyItemInserted(myListModels.size());
    }
    @Override
    public MyListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_edit, parent, false);
        return new MyListAdapter.ViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return myListModels.size();
    }



    // ItemViewHolder Class for Items in each Section
    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView  qty,  add, sub;
        EditText itemname;
        LinearLayout qty_linear;
        public ViewHolder(View itemView) {
            super(itemView);
            itemname = (EditText) itemView.findViewById(R.id.itemname);
            qty = (TextView) itemView.findViewById(R.id.itemqty);
            add = (TextView) itemView.findViewById(R.id.itemqty_add);
            sub = (TextView) itemView.findViewById(R.id.itemqty_sub);
            qty_linear=itemView.findViewById(R.id.qty_linear);
        }
    }
}

