package com.food.order.speedzy.CitySpecial;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.food.order.speedzy.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCategoriesVH extends RecyclerView.ViewHolder {
    private final SubCategoriesVH.Callback mCallback;
    private final Context mContext;
    @BindView(R.id.ivExample)
    ImageView ivExample;
    public static int sCorner = 15;
    public static int sMargin = 8;
    public SubCategoriesVH(@NonNull View itemView,
                               SubCategoriesVH.Callback callback) {
        super(itemView);
        mCallback = callback;
        mContext = itemView.getContext();
        ButterKnife.bind(this, itemView);
    }

    public void onBindView(CitySpecialCategoryModel.Category.SubCategory data) {
        Glide.with(mContext)
                .load(data.getImage())
                .apply(RequestOptions.bitmapTransform(new RoundedCorners( sCorner))
                        .placeholder(R.drawable.ic_product_coming_soon))
                .into(ivExample);
        ivExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    mCallback.onClickItem(getAdapterPosition());
                }
            }
        });
    }

    public interface Callback {
        void onClickItem(int position);
    }
}


