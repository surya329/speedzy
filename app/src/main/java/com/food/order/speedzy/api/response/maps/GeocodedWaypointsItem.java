package com.food.order.speedzy.api.response.maps;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class GeocodedWaypointsItem{

	@SerializedName("types")
	private List<String> types;

	@SerializedName("geocoder_status")
	private String geocoderStatus;

	@SerializedName("place_id")
	private String placeId;

	public List<String> getTypes(){
		return types;
	}

	public String getGeocoderStatus(){
		return geocoderStatus;
	}

	public String getPlaceId(){
		return placeId;
	}

	@Override
 	public String toString(){
		return 
			"GeocodedWaypointsItem{" + 
			"types = '" + types + '\'' + 
			",geocoder_status = '" + geocoderStatus + '\'' + 
			",place_id = '" + placeId + '\'' + 
			"}";
		}
}