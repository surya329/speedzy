package com.food.order.speedzy.api.response.createorder;

import com.google.gson.annotations.SerializedName;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CreateOrderResponse{

	@SerializedName("data")
	private Data data;

	public Data getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"UserRegistration{" +
			"data = '" + data + '\'' + 
			"}";
		}
}