package com.food.order.speedzy.screen.orderupi;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.food.order.speedzy.Activity.MyOrderActivity;
import com.food.order.speedzy.Activity.PaymentWebView_food;
import com.food.order.speedzy.Model.StorepackagerModel;
import com.food.order.speedzy.Model.payment.PaymentModel;
import com.food.order.speedzy.R;
import com.food.order.speedzy.Utils.Commons;
import com.food.order.speedzy.Utils.GeneralUtil;
import com.food.order.speedzy.apimodule.API_Call_Retrofit;
import com.food.order.speedzy.apimodule.Apimethods;
import com.food.order.speedzy.root.BaseActivity;
import com.food.order.speedzy.screen.ordersuccess.ActivityOrderDataModel;
import com.food.order.speedzy.screen.ordersuccess.ActivityOrderSuccess;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderUPIPaymentActivity extends BaseActivity {
  private static final String EXTRA_ORDER_UPI = "EXTRA_ORDER_UPI";
  public static final int PAYMENT_REQUEST_CODE = 1;
  private Disposable mDisposable;
  @BindView(R.id.txt_order_number) TextView txtOrderNumber;
  @BindView(R.id.txt_make_payment) TextView txtMakePayment;
  private OrderUPIDataModel mOrderUPIDataModel;
  private Apimethods mApimethods;
  Timer checkStatusTimer;
  private int type = 1;

  public static Intent newIntent(Context context, OrderUPIDataModel upiDataModel) {
    Intent intent = new Intent(context, OrderUPIPaymentActivity.class);
    intent.putExtra(EXTRA_ORDER_UPI, upiDataModel);
    return intent;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_order_upipayment_edited);
    ButterKnife.bind(this);
    initComponent();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    GeneralUtil.safelyDispose(mDisposable);
    if (checkStatusTimer != null) {
      checkStatusTimer.cancel();
      checkStatusTimer.purge();
      checkStatusTimer = null;
    }
  }

  /*@Override public void onStart() {
    super.onStart();
    if (mOrderUPIDataModel != null && mOrderUPIDataModel.getOrderNumber() != null) {
      Log.e("orderId", mOrderUPIDataModel.getOrderNumber());
      checkPaymentStatus(mOrderUPIDataModel.getOrderNumber());
    }
  }*/

  private void initComponent() {
    mApimethods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    if (getIntent().getExtras() != null) {
      mOrderUPIDataModel = getIntent().getExtras().getParcelable(EXTRA_ORDER_UPI);
    }
    if (mOrderUPIDataModel != null && mOrderUPIDataModel.getPrice() != null) {
      txtOrderNumber.setText(
          getString(R.string.upipayment_format_order, mOrderUPIDataModel.getOrderNumber(),
              ("₹ " + mOrderUPIDataModel.getPrice())));
    }

    final ImageView google_pay = (ImageView) findViewById(R.id.google_pay);
    final ImageView phone_pe = (ImageView) findViewById(R.id.phone_pe);
    final ImageView paytm = (ImageView) findViewById(R.id.paytm);
    final ImageView bhim = (ImageView) findViewById(R.id.bhim);
    TextView text_upi = findViewById(R.id.text_upi);

    text_upi.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent browserIntent =
            new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.npci.org.in/upi-faq-s"));
        startActivity(browserIntent);
      }
    });
    google_pay.setOnClickListener(new View.OnClickListener() {
      @SuppressLint("DefaultLocale") @Override
      public void onClick(View v) {
        Commons.google_pay_call(1, OrderUPIPaymentActivity.this,
            mOrderUPIDataModel.getOrderNumber(), Commons.total_amount);
      }
    });
    phone_pe.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Commons.google_pay_call(2, OrderUPIPaymentActivity.this,
            mOrderUPIDataModel.getOrderNumber(), Commons.total_amount);
      }
    });
    paytm.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
       /* Intent intent = new Intent(OrderUPIPaymentActivity.this, PaymentWebView_food.class);
        intent.putExtra("order_id", mOrderUPIDataModel.getOrderNumber());
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);*/
        Commons.google_pay_call(4, OrderUPIPaymentActivity.this,
                mOrderUPIDataModel.getOrderNumber(), Commons.total_amount);
      }
    });
    bhim.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Commons.google_pay_call(3, OrderUPIPaymentActivity.this,
            mOrderUPIDataModel.getOrderNumber(), Commons.total_amount);
      }
    });
  }

  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    try {
      if (requestCode == 11 && resultCode == RESULT_OK) {
        if (data != null) {
          payment_status(data);
        }
      } else if (requestCode == 22 && resultCode == RESULT_OK) {
        if (data != null) {
          payment_status(data);
        }
      } else if (requestCode == 33 && resultCode == RESULT_OK) {
        if (data != null) {
          payment_status(data);
        }
      } else if (requestCode == 44 && resultCode == RESULT_OK) {
        if (data != null) {
          payment_status(data);
        }
      } else if (requestCode == PAYMENT_REQUEST_CODE && resultCode == RESULT_OK) {
        goToOrderSuccessPage();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    super.onActivityResult(requestCode, resultCode, data);
  }

  private void payment_status(Intent data) {
    Bundle bundle = data.getExtras();
    if (bundle != null) {
      for (String key : bundle.keySet()) {
        Log.e("UPI_txn=>", key + ":" + bundle.get(key));
      }
    }
    String orderid = data.getStringExtra("txnRef");
    mOrderUPIDataModel = new OrderUPIDataModel();
    mOrderUPIDataModel.setOrderNumber(orderid);
    Log.e("UPI_txn", orderid);
    Log.e("UPI_txn", data.getStringExtra("Status"));
    if (Objects.requireNonNull(data.getStringExtra("Status")).equalsIgnoreCase("SUCCESS")) {
      apicall();
    } else {
      Toast.makeText(this, "Payment Failed", Toast.LENGTH_SHORT).show();
    }
  }

  private void apicall() {
    Apimethods methods = API_Call_Retrofit.changeApiBaseUrl(this).create(Apimethods.class);
    Call<StorepackagerModel> call =
        methods.changepayment(mOrderUPIDataModel.getOrderNumber(), Commons.menu_id);
    Log.e("url", "url=" + call.request().url().toString());
    call.enqueue(new Callback<StorepackagerModel>() {
      @Override
      public void onResponse(Call<StorepackagerModel> call, Response<StorepackagerModel> response) {
        int statusCode = response.code();
        Log.d("Response", "" + statusCode);
        Log.d("respones", "" + response);
        StorepackagerModel deviceUpdateModel = response.body();
        if (deviceUpdateModel != null && deviceUpdateModel.getStatus()
            .equalsIgnoreCase("Success")) {
          goToOrderSuccessPage1();
        }
      }

      @Override
      public void onFailure(@NonNull Call<StorepackagerModel> call, @NonNull Throwable t) {
        showErrorDialog(t.getLocalizedMessage());
      }
    });
  }

  private void goToOrderSuccessPage1() {
    Toast.makeText(this, "Payment Successful", Toast.LENGTH_SHORT).show();
    ActivityOrderDataModel activityOrderDataModel = new ActivityOrderDataModel();
    activityOrderDataModel.setOrderId(mOrderUPIDataModel.getOrderNumber());
    startActivity(ActivityOrderSuccess.newIntent(this, activityOrderDataModel));
    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    this.finish();
  }

  public void checkPaymentStatus(String orderId) {
    if (checkStatusTimer != null) {
      checkStatusTimer.cancel();
      checkStatusTimer.purge();
      checkStatusTimer = null;
    }
    long timeDelay;
    final Handler handler = new Handler();
    checkStatusTimer = new Timer();
    timeDelay = 5000;
    checkStatusTimer.schedule(new TimerTask() {
      @Override
      public void run() {
        handler.post(new Runnable() {
          @Override
          public void run() {
            callApiToCheckPaymentStatus(orderId);
          }
        });
      }
    }, 0, timeDelay);
  }

  private void callApiToCheckPaymentStatus(String orderId) {
    mApimethods.getPaymentStatus(orderId).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<PaymentModel>() {
      @Override public void onSubscribe(Disposable d) {
        mDisposable = d;
      }

      @Override public void onNext(PaymentModel paymentModel) {
        if (paymentModel != null && paymentModel.getPaymentType() != null &&
            paymentModel.getPaymentType()
                .equalsIgnoreCase("4")
            && paymentModel.getPaymentStatus() != null && paymentModel.getPaymentStatus()
            .equalsIgnoreCase("1")) {
          goToOrderSuccessPage();
        }
      }

      @Override public void onError(Throwable e) {
        Log.e("Testing", "Log");
        //        showErrorDialog(e.getLocalizedMessage());
      }

      @Override public void onComplete() {
        Log.e("Testing", "Log");
      }
    });
  }

  private void goToOrderSuccessPage() {
    if (checkStatusTimer != null) {
      checkStatusTimer.cancel();
      checkStatusTimer.purge();
      checkStatusTimer = null;
    }
    Toast.makeText(this, "Payment Successful", Toast.LENGTH_SHORT).show();
    ActivityOrderDataModel activityOrderDataModel = new ActivityOrderDataModel();
    activityOrderDataModel.setOrderId(mOrderUPIDataModel.getOrderNumber());
    finish();
    startActivity(ActivityOrderSuccess.newIntent(this, activityOrderDataModel));
    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
  }

  @Override public void onBackPressed() {
    super.onBackPressed();
    goToOrderPage();
  }

  private void goToOrderPage() {
    if (Commons.flag_for_hta != null) {
      switch (Commons.flag_for_hta) {
        case "Grocerry":
        case "Patanjali":
          type = 2;
          break;
        case "City Special":
          type = 2;
          break;
        case "Drinking Water":
          type = 2;
          break;
      }
      Intent intent = new Intent(this, MyOrderActivity.class);
      intent.putExtra("flag", 1);
      intent.putExtra("type", type);
      finish();
      startActivity(intent);
      overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    } else {
      Intent intent = new Intent(this, MyOrderActivity.class);
      intent.putExtra("flag", 1);
      intent.putExtra("type", type);
      finish();
      startActivity(intent);
      overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
  }
}

