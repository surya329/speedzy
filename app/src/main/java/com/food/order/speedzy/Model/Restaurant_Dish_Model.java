
package com.food.order.speedzy.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sujata Mohanty.
 */


public class Restaurant_Dish_Model {


    @SerializedName("deliveryarea")
    @Expose
    private List<DeliveryAreaList> deliveryAreaList = null;

    @SerializedName("offers")
    @Expose
    private List<NewOffers> offers = null;

    private String status;
    private String avg_order_value;
    private String order_no;

    public String getOrders_count() {
        return orders_count;
    }

    public void setOrders_count(String orders_count) {
        this.orders_count = orders_count;
    }

    private String orders_count;
    private String avada_date;
    private String avg_rating;
    private String count_rating;


    private String avada_start_time;
    private String avada_end_time;

    private String train_lunch_start;
    private String train_lunch_end;
    private String train_dinner_start;
    private String train_dinner_end;

    private List<Dishitem> dishitems = null;
    private List<Dishitem> Cuisines = null;
    private List<DeliveryFee> delivery_fee = null;
    private List<CuisineList> cuisine_list = null;
    private List<Extrasdetail> extras = null;
    private List<DevliverySuburb> devlivery_suburb = null;

    private List<Object> delivery_distinct = null;



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Dishitem> getDishitems() {
        return dishitems;
    }

    public void setDishitems(List<Dishitem> dishitems) {
        this.dishitems = dishitems;
    }

    public List<DeliveryFee> getDeliveryFee() {
        return delivery_fee;
    }

    public void setDeliveryFee(List<DeliveryFee> deliveryFee) {
        this.delivery_fee = deliveryFee;
    }

    public List<CuisineList> getCuisineList() {
        return cuisine_list;
    }

    public void setCuisineList(List<CuisineList> cuisineList) {
        this.cuisine_list = cuisineList;
    }

    public List<DevliverySuburb> getDevliverySuburb() {
        return devlivery_suburb;
    }

    public void setDevliverySuburb(List<DevliverySuburb> devliverySuburb) {
        this.devlivery_suburb = devliverySuburb;
    }

    public List<NewOffers> getOffers() {
        return offers;
    }

    public void setOffers(List<NewOffers> offers) {
        this.offers = offers;
    }

    public List<Object> getDeliveryDistinct() {
        return delivery_distinct;
    }

    public void setDeliveryDistinct(List<Object> deliveryDistinct) {
        this.delivery_distinct = deliveryDistinct;
    }

    public List<Dishitem> getCuisines() {
        return Cuisines;
    }

    public void setCuisines(List<Dishitem> cuisines) {
        Cuisines = cuisines;
    }

    public List<Extrasdetail> getExtras() {
        return extras;
    }

    public void setExtras(List<Extrasdetail> extras) {
        this.extras = extras;
    }

    public String getAvg_order_value() {
        return avg_order_value;
    }

    public void setAvg_order_value(String avg_order_value) {
        this.avg_order_value = avg_order_value;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }
    public List<DeliveryAreaList> getDeliveryAreaList() {
        return deliveryAreaList;
    }

    public void setDeliveryAreaList(List<DeliveryAreaList> deliveryAreaList) {
        this.deliveryAreaList = deliveryAreaList;
    }

    public String getAvada_date() {
        return avada_date;
    }

    public void setAvada_date(String avada_date) {
        this.avada_date = avada_date;
    }

    public String getAvada_start_time() {
        return avada_start_time;
    }

    public void setAvada_start_time(String avada_start_time) {
        this.avada_start_time = avada_start_time;
    }

    public String getAvada_end_time() {
        return avada_end_time;
    }

    public void setAvada_end_time(String avada_end_time) {
        this.avada_end_time = avada_end_time;
    }

    public String getTrain_lunch_start() {
        return train_lunch_start;
    }

    public void setTrain_lunch_start(String train_lunch_start) {
        this.train_lunch_start = train_lunch_start;
    }

    public String getTrain_lunch_end() {
        return train_lunch_end;
    }

    public void setTrain_lunch_end(String train_lunch_end) {
        this.train_lunch_end = train_lunch_end;
    }

    public String getTrain_dinner_start() {
        return train_dinner_start;
    }

    public void setTrain_dinner_start(String train_dinner_start) {
        this.train_dinner_start = train_dinner_start;
    }

    public String getTrain_dinner_end() {
        return train_dinner_end;
    }

    public void setTrain_dinner_end(String train_dinner_end) {
        this.train_dinner_end = train_dinner_end;
    }

    public String getAvg_rating() {
        return avg_rating;
    }

    public void setAvg_rating(String avg_rating) {
        this.avg_rating = avg_rating;
    }

    public String getCount_rating() {
        return count_rating;
    }

    public void setCount_rating(String count_rating) {
        this.count_rating = count_rating;
    }

    public class DeliveryAreaList implements Serializable {

        @SerializedName("in_suburb_id")
        @Expose
        private String inSuburbId;
        @SerializedName("postcode_id")
        @Expose
        private Object postcodeId;
        @SerializedName("st_suburb")
        @Expose
        private String stSuburb;
        @SerializedName("st_postcode")
        @Expose
        private String stPostcode;
        @SerializedName("st_city")
        @Expose
        private Object stCity;
        @SerializedName("st_delivery_charge")
        @Expose
        private String stDeliveryCharge;
        @SerializedName("flg_delivery_status")
        @Expose
        private String flgDeliveryStatus;
        @SerializedName("flg_menu_choice")
        @Expose
        private String flgMenuChoice;

        public String getNormal_del_charge() {
            return normal_del_charge;
        }

        public void setNormal_del_charge(String normal_del_charge) {
            this.normal_del_charge = normal_del_charge;
        }

        public String getMin_Order() {
            return min_Order;
        }

        public void setMin_Order(String min_Order) {
            this.min_Order = min_Order;
        }

        @SerializedName("normal_del_charge")
        @Expose
        private String normal_del_charge;
        @SerializedName("min_Order")
        @Expose
        private String min_Order;

        public String getInSuburbId() {
            return inSuburbId;
        }

        public void setInSuburbId(String inSuburbId) {
            this.inSuburbId = inSuburbId;
        }

        public Object getPostcodeId() {
            return postcodeId;
        }

        public void setPostcodeId(Object postcodeId) {
            this.postcodeId = postcodeId;
        }

        public String getStSuburb() {
            return stSuburb;
        }

        public void setStSuburb(String stSuburb) {
            this.stSuburb = stSuburb;
        }

        public String getStPostcode() {
            return stPostcode;
        }

        public void setStPostcode(String stPostcode) {
            this.stPostcode = stPostcode;
        }

        public Object getStCity() {
            return stCity;
        }

        public void setStCity(Object stCity) {
            this.stCity = stCity;
        }

        public String getStDeliveryCharge() {
            return stDeliveryCharge;
        }

        public void setStDeliveryCharge(String stDeliveryCharge) {
            this.stDeliveryCharge = stDeliveryCharge;
        }

        public String getFlgDeliveryStatus() {
            return flgDeliveryStatus;
        }

        public void setFlgDeliveryStatus(String flgDeliveryStatus) {
            this.flgDeliveryStatus = flgDeliveryStatus;
        }

        public String getFlgMenuChoice() {
            return flgMenuChoice;
        }

        public void setFlgMenuChoice(String flgMenuChoice) {
            this.flgMenuChoice = flgMenuChoice;
        }

    }
    public class NewOffers implements Serializable {
        private String offer_title;
        private String offer_description;
        private String offer_terms;
        private String st_del_pic_condition;

        private String flg_offer_type;
        private String second_offer_value;
        private String first_offer_value;
        private String third_offer_value;
        private float offerprice;

        public String getOffer_title() {
            return offer_title;
        }

        public void setOffer_title(String offer_title) {
            this.offer_title = offer_title;
        }

        public String getOffer_description() {
            return offer_description;
        }

        public void setOffer_description(String offer_description) {
            this.offer_description = offer_description;
        }

        public String getOffer_terms() {
            return offer_terms;
        }

        public void setOffer_terms(String offer_terms) {
            this.offer_terms = offer_terms;
        }

        public String getSt_del_pic_condition() {
            return st_del_pic_condition;
        }

        public void setSt_del_pic_condition(String st_del_pic_condition) {
            this.st_del_pic_condition = st_del_pic_condition;
        }

        public String getFlg_offer_type() {
            return flg_offer_type;
        }

        public void setFlg_offer_type(String flg_offer_type) {
            this.flg_offer_type = flg_offer_type;
        }

        public String getSecond_offer_value() {
            return second_offer_value;
        }

        public void setSecond_offer_value(String second_offer_value) {
            this.second_offer_value = second_offer_value;
        }

        public String getFirst_offer_value() {
            return first_offer_value;
        }

        public void setFirst_offer_value(String first_offer_value) {
            this.first_offer_value = first_offer_value;
        }

        public String getThird_offer_value() {
            return third_offer_value;
        }

        public void setThird_offer_value(String third_offer_value) {
            this.third_offer_value = third_offer_value;
        }

        public float getOfferprice() {
            return offerprice;
        }

        public void setOfferprice(float offerprice) {
            this.offerprice = offerprice;
        }
    }
}