package com.food.order.speedzy.api.response.grocery;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class GroceryResponse {

  @SerializedName("Banners")
  private List<Object> banners;

  @SerializedName("categories")
  private List<CategoriesItem> categories;

  @SerializedName("status")
  private String status;

  public List<Object> getBanners() {
    return banners;
  }

  public List<CategoriesItem> getCategories() {
    return categories;
  }

  public String getStatus() {
    return status;
  }

  @Override
  public String toString() {
    return
        "GroceryResponse{" +
            "banners = '" + banners + '\'' +
            ",categories = '" + categories + '\'' +
            ",status = '" + status + '\'' +
            "}";
  }
}