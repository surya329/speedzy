package com.food.order.speedzy.screen.patanjali;

import com.food.order.speedzy.api.response.grocery.SubCategoriesItem;
import java.util.ArrayList;
import java.util.List;

public class GrocerySubcategoryList {

  private String image;
  private List<GroceryChildList> childSubcat;
  private String name;
  private String id;

  public static List<GrocerySubcategoryList> transform(List<SubCategoriesItem> data) {
    List<GrocerySubcategoryList> mData = new ArrayList<>();
    if (data != null && data.size() > 0) {
      for (SubCategoriesItem vm : data) {
        GrocerySubcategoryList model = new GrocerySubcategoryList();
        model.setId(vm.getId());
        model.setChildSubcat(GroceryChildList.transform(vm.getChildSubcat()));
        model.setImage(vm.getImage());
        model.setName(vm.getName());
        mData.add(model);
      }
    }
    return mData;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public List<GroceryChildList> getChildSubcat() {
    return childSubcat;
  }

  public void setChildSubcat(
      List<GroceryChildList> childSubcat) {
    this.childSubcat = childSubcat;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
