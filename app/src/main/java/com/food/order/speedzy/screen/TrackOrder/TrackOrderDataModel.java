package com.food.order.speedzy.screen.TrackOrder;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;

public class TrackOrderDataModel implements Parcelable {
  private String mOrderId;
  private LatLng mPickup;
  private LatLng mDestination;
  private String mPickupAddress;
  private String mDestinationAddress;
  private String mDeliveryBoyId;
  private String mDeliveryBoyName;
  private String mDeliveryBoyNumber;
  private String mOrderTitle;
  private String mOrderStatusMessage;

  public TrackOrderDataModel() {

  }

  protected TrackOrderDataModel(Parcel in) {
    mOrderId = in.readString();
    mPickup = in.readParcelable(LatLng.class.getClassLoader());
    mDestination = in.readParcelable(LatLng.class.getClassLoader());
    mPickupAddress = in.readString();
    mDestinationAddress = in.readString();
    mDeliveryBoyId = in.readString();
    mDeliveryBoyName = in.readString();
    mDeliveryBoyNumber = in.readString();
    mOrderTitle = in.readString();
    mOrderStatusMessage = in.readString();
  }

  public static final Creator<TrackOrderDataModel> CREATOR = new Creator<TrackOrderDataModel>() {
    @Override
    public TrackOrderDataModel createFromParcel(Parcel in) {
      return new TrackOrderDataModel(in);
    }

    @Override
    public TrackOrderDataModel[] newArray(int size) {
      return new TrackOrderDataModel[size];
    }
  };

  public String getOrderId() {
    return mOrderId;
  }

  public void setOrderId(String orderId) {
    mOrderId = orderId;
  }

  public LatLng getPickup() {
    return mPickup;
  }

  public void setPickup(LatLng pickup) {
    mPickup = pickup;
  }

  public LatLng getDestination() {
    return mDestination;
  }

  public void setDestination(LatLng destination) {
    mDestination = destination;
  }

  public String getPickupAddress() {
    return mPickupAddress;
  }

  public void setPickupAddress(String pickupAddress) {
    mPickupAddress = pickupAddress;
  }

  public String getDestinationAddress() {
    return mDestinationAddress;
  }

  public void setDestinationAddress(String destinationAddress) {
    mDestinationAddress = destinationAddress;
  }

  public String getDeliveryBoyId() {
    return mDeliveryBoyId;
  }

  public void setDeliveryBoyId(String deliveryBoyId) {
    mDeliveryBoyId = deliveryBoyId;
  }

  public String getDeliveryBoyName() {
    return mDeliveryBoyName;
  }

  public void setDeliveryBoyName(String deliveryBoyName) {
    mDeliveryBoyName = deliveryBoyName;
  }

  public String getDeliveryBoyNumber() {
    return mDeliveryBoyNumber;
  }

  public void setDeliveryBoyNumber(String deliveryBoyNumber) {
    mDeliveryBoyNumber = deliveryBoyNumber;
  }

  public String getOrderTitle() {
    return mOrderTitle;
  }

  public void setOrderTitle(String orderTitle) {
    mOrderTitle = orderTitle;
  }

  public String getOrderStatusMessage() {
    return mOrderStatusMessage;
  }

  public void setOrderStatusMessage(String orderStatusMessage) {
    mOrderStatusMessage = orderStatusMessage;
  }



  @NonNull @Override public String toString() {
    return "TrackOrderDataModel{" +
        "mOrderId='" + mOrderId + '\'' +
        ", mPickup=" + mPickup +
        ", mDestination=" + mDestination +
        ", mPickupAddress='" + mPickupAddress + '\'' +
        ", mDestinationAddress='" + mDestinationAddress + '\'' +
        ", mDeliveryBoyId='" + mDeliveryBoyId + '\'' +
        ", mDeliveryBoyName='" + mDeliveryBoyName + '\'' +
        ", mDeliveryBoyNumber='" + mDeliveryBoyNumber + '\'' +
        '}';
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(mOrderId);
    parcel.writeParcelable(mPickup, i);
    parcel.writeParcelable(mDestination, i);
    parcel.writeString(mPickupAddress);
    parcel.writeString(mDestinationAddress);
    parcel.writeString(mDeliveryBoyId);
    parcel.writeString(mDeliveryBoyName);
    parcel.writeString(mDeliveryBoyNumber);
    parcel.writeString(mOrderTitle);
    parcel.writeString(mOrderStatusMessage);
  }
}
