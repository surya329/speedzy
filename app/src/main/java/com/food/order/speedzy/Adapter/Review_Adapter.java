package com.food.order.speedzy.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.food.order.speedzy.Model.View_Rating_model;
import com.food.order.speedzy.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by mathivanan on 11/03/17.
 */

public class Review_Adapter extends RecyclerView.Adapter<Review_Adapter.ViewHolder>{




    private Context ctx;
    ArrayList<View_Rating_model.Review> reviewArrayList;

    public Review_Adapter(Context ctx, ArrayList<View_Rating_model.Review> reviewArrayList) {
        this.ctx = ctx;
        this.reviewArrayList = reviewArrayList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
     public  TextView name,reviewdate,description,allrating,restRating;
     public RatingBar ratingBar;

        public ViewHolder(View v) {
            super(v);
            name=(TextView)v.findViewById(R.id.name);
            reviewdate=(TextView)v.findViewById(R.id.time);
            description=(TextView)v.findViewById(R.id.comment);
            allrating=(TextView)v.findViewById(R.id.allrating);
            ratingBar=(RatingBar) v.findViewById(R.id.rating);
            restRating=v.findViewById(R.id.restRating);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review, parent, false);
        System.out.println("View Holder");
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (reviewArrayList.get(position).getOveral() != null) {
            float ratingfloat = Float.parseFloat(reviewArrayList.get(position).getOveral());
            // ratingfloat/=5;
            holder.ratingBar.setRating(ratingfloat);
            holder.restRating.setText(String.format("%.1f", ratingfloat));
        }else {
            float ratingfloat=0;
            holder.ratingBar.setRating(ratingfloat);
            holder.restRating.setText("0.0");
        }
            holder.name.setText(reviewArrayList.get(position).getFirstname());
            holder.reviewdate.setText(getdate(reviewArrayList.get(position).getAddedDate()));
        if (reviewArrayList.get(position).getDescription().equalsIgnoreCase("") ||
                reviewArrayList.get(position).getDescription() == null) {
            holder.description.setVisibility(View.GONE);
        }else {
            holder.description.setText(reviewArrayList.get(position).getDescription());
        }
        double d = 0.0,d1= 0.0,d2= 0.0,d3= 0.0;
        if (reviewArrayList.get(position).getOveral() != null) {
             d = Double.parseDouble(reviewArrayList.get(position).getOveral());
        }
        if (reviewArrayList.get(position).getFoodRating() != null) {
             d1 = Double.parseDouble(reviewArrayList.get(position).getFoodRating());
        }
        if (reviewArrayList.get(position).getValueRating() != null) {
             d2 = Double.parseDouble(reviewArrayList.get(position).getValueRating());
        }
        if (reviewArrayList.get(position).getSpeedRating() != null) {
             d3 = Double.parseDouble(reviewArrayList.get(position).getSpeedRating());
        }

            holder.allrating.setText("Overall " + String.format("%.2f", d) + " | " + "Food " + String.format("%.2f", d1) + " | " + "Value " + String.format("%.2f", d2) + " | " + "Speed " + String.format("%.2f", d3));

        }

    // Here is the key method to apply the animation
    private int lastPosition = -1;
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(ctx, R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
    private String getdate(String deliveryDate) {
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
        Date d = null;
        String changedDate = "";
        try {
            d = dateFormatter.parse(deliveryDate);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy");
            changedDate = dateFormat.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
            changedDate = deliveryDate;
        }
        return changedDate;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return  reviewArrayList.size();
    }


}
